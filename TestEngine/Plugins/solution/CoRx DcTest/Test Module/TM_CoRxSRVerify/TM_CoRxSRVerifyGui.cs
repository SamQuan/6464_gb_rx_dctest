// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TM_CoRxSRVerifyGui.cs
//
// Author: alice.huang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class TM_CoRxSRVerifyGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TM_CoRxSRVerifyGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(GuiMsg.CautionOfLaserRqst))
            {
                gbCaution.Visible = true;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(GuiMsg.CancelOflLaserCautionRqst))
            {
                gbCaution.Visible = false;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(DatumString))
            {
                DatumString msg = payload as DatumString;

                if (msg.Name == "GuiTitle")
                {
                    lblTile.Text = msg.Value;
                    lblTile.Refresh();
                }
                else
                {
                    lblTestMsg.Text = msg.Value;
                    lblTestMsg.Refresh();
                }
            }
        }
    }
}
