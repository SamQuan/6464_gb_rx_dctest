// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TP_CoRxDcTest/SwitchFibreConnector.cs
// 
// Author: alice.huang
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    public partial class SwitchFibreConnector : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        TP_CoRxPATestGui parent;
        public SwitchFibreConnector(TP_CoRxPATestGui parentForm)
        {
            /* Call designer generated code. */
            this.parent = parentForm;
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            parent.SendToWorker(new GuiMsg.CoRxSwitchConnectorCompleted());
            parent.CtrlFinished();
        }

    }
}
