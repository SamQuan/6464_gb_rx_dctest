Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("MSAVendorCommands_NET.MSAVendorCommands")> Public Class MSAVendorCommands
	
	
	
	' Straight CAL commands - F4/F5  ###############################################################
	
	' Reg 0x01
	Public Function SetCalMzMod(ByRef what As Short, Optional ByRef nv As String = "defaultvalue") As Boolean
		If nv = "defaultvalue" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetCalMzMod = Comms.WriteCal(CALMZMOD, CDbl(what), 1)
		Else
			If nv = "EEPROM" Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetCalMzMod = Comms.WriteCal(CALMZMOD + EEPROMBIT, CDbl(what), 1)
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetCalMzMod = Comms.WriteCal(CALMZMOD, CDbl(what), 1)
			End If
		End If
	End Function
	
	
	Public Function GetCalMzMod(Optional ByRef nv As String = "defaultvalue") As Short
		If nv = "defaultvalue" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetCalMzMod = Comms.ReadCal(CALMZMOD, 0)
		Else
			If nv = "EEPROM" Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				GetCalMzMod = Comms.ReadCal(CALMZMOD + EEPROMBIT, 0)
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				GetCalMzMod = Comms.ReadCal(CALMZMOD, 0)
			End If
		End If
	End Function
	
	
	' Reg 0x02
	Public Function SetCalMzBiasN(ByRef what As Short, Optional ByRef nv As String = "defaultvalue") As Boolean
		If nv = "defaultvalue" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetCalMzBiasN = Comms.WriteCal(CALMZBIASN, CDbl(what), 1)
		Else
			If nv = "EEPROM" Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetCalMzBiasN = Comms.WriteCal(CALMZBIASN + EEPROMBIT, CDbl(what), 1)
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetCalMzBiasN = Comms.WriteCal(CALMZBIASN, CDbl(what), 1)
			End If
		End If
	End Function
	
	
	Public Function GetCalMzBiasN(Optional ByRef nv As String = "defaultvalue") As Short
		If nv = "defaultvalue" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			GetCalMzBiasN = Comms.ReadCal(CALMZBIASN, 0)
		Else
			If nv = "EEPROM" Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				GetCalMzBiasN = Comms.ReadCal(CALMZBIASN + EEPROMBIT, 0)
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				GetCalMzBiasN = Comms.ReadCal(CALMZBIASN, 0)
			End If
		End If
	End Function
	
	
	
	
	Public Function GetADCReading(ByRef indx As Byte) As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetADCReading = CShort(Comms.ReadCal(GETADC, 0, indx))
	End Function
	
	
	
	Public Function GetSerdesRegister(ByRef indx As Byte) As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetSerdesRegister = CShort(Comms.ReadCal(SERDESREGISTER, 0, indx))
	End Function
	
	
	
	Public Function SetSerdesRam(ByRef indx As Byte, ByRef what As Double) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSerdesRam = Comms.WriteCal(SERDESRAM, what, 1, indx)
	End Function
	
	
	
	Public Function SetSerdesRegister(ByRef indx As Byte, ByRef what As Double) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSerdesRegister = Comms.WriteCal(SERDESREGISTER, what, 1, indx)
	End Function
	
	
	
	Public Function SetSerdesEEPROM(ByRef indx As Byte, ByRef what As Double) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSerdesEEPROM = Comms.WriteCal(SERDESEEPROM, what, 1, indx)
	End Function
	
	
	
	Public Function SetRxPwrMonCalTable(ByRef indx As Byte) As Boolean
		' Sets the actual power for this level.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetRxPwrMonCalTable = Comms.WriteCal(RXPWRMONCALTABLE, 0, 1, indx)
	End Function
	
	
	
	Public Function GetRxPwrMonCalTable(ByRef indx As Byte) As Short
		' Reads the stored value of power for this level.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetRxPwrMonCalTable = CShort(Comms.ReadCal(RXPWRMONCALTABLE, 0, indx)) ' F7 00 indx
	End Function
	
	
	
	Public Function GetRxPwrMonCalTableSize() As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetRxPwrMonCalTableSize = CShort(Comms.ReadCal(RXPOWERMONSIZE, 0)) ' F5 0B
	End Function
	
	
	
	Public Function GetRxPwrPoint(ByRef indx As Byte) As Short
		' Reads the required power level for each point in the power monitor table.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetRxPwrPoint = CShort(Comms.ReadCal(RXPOWERPOINTS, 100, indx)) ' F7 01 indx
	End Function
	
	
	
	Public Function GetRawOpticalPower() As Single
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetRawOpticalPower = CSng(Comms.ReadCal(OPTPOWRAW, 0)) ' F5 18
	End Function
	
	
	
	Public Function GetModMonPower() As Integer
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetModMonPower = Comms.ReadCal(MODMONPOWER, -1)
	End Function
	
	
	
	Public Function GetMZModAmplitudeMon() As Integer
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetMZModAmplitudeMon = Comms.ReadCal(MZMODAMPLITUDEMON, -1)
	End Function
	
	
	
	
	Public Function SaveRxCal() As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		Send(1) = SAVERX
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then ' FA 13
			SaveRxCal = True
		Else
			SaveRxCal = False
		End If
	End Function
	
	
	
	
	Public Function RecallRxCal() As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		Send(1) = RECALLRX
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then ' FA 14
			RecallRxCal = True
		Else
			RecallRxCal = False
		End If
	End Function
	
	
	
	
	Public Function SetCalTxPowerMon(ByRef uw As Short) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetCalTxPowerMon = Comms.WriteCal(CALTXPOWERMON, CDbl(uw), 1)
	End Function
	
	
	
	
	Public Function SetBOLLaserPower(ByRef uw As Short) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetBOLLaserPower = Comms.WriteCal(BOLLASERPOWER, CDbl(uw), 1)
	End Function
	
	
	
	
	Public Function SetModulator(ByRef dac As Short) As Boolean
		' NOTE: This assumes negative chirp.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetModulator = Comms.WriteCal(CALMZBIASN, CDbl(dac), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetModulator Then Call Logger.logFATAL("Error: Failed to set modulator dac to " & CStr(dac))
	End Function
	
	
	
	
	Public Function GetModulatorDac() As Short
		' NOTE: This assumes negative chirp.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetModulatorDac = Comms.ReadCal(CALMZBIASN, 0)
	End Function
	
	
	
	Public Function SaveModulator(ByRef dac As Short) As Boolean
		' Saves the modulator dac value to the non-volatile dac register.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SaveModulator = Comms.WriteCal(SAVECALMZBIASN, CDbl(dac), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SaveModulator Then Logger.logFATAL("Error: Failed to set modulator dac.")
	End Function
	
	
	
	
	Public Function SetModMonGain(ByRef what As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		If what = "high" Then
			Send(1) = ENABLEMODMONHIGHGAIN
		Else
			Send(1) = DISABLEMODMONHIGHGAIN
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then
			SetModMonGain = True
		Else
			SetModMonGain = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set modulator monitor gain.")
		End If
	End Function
	
	
	
	
	Public Function SetMZLoop(ByRef what As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		If what = "open" Then
			Send(1) = MZOPENLOOP
		Else
			Send(1) = MZCLOSEDLOOP
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then
			SetMZLoop = True
		Else
			SetMZLoop = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set mzloop " & what)
		End If
	End Function
	
	
	
	
	Public Function SetChirp(ByRef what As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		If what = "positive" Then
			Send(1) = POSITIVECHIRP
		Else
			Send(1) = NEGATIVECHIRP
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(CHIRPCOMMAND, Send, 1, rcv, 0) Then
			SetChirp = True
		Else
			SetChirp = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set chirp " & what)
		End If
	End Function
	
	
	
	
	Public Function ModulatorDriver(ByRef what As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		If what = "enable" Then Send(1) = ENABLETXDRIVE Else Send(1) = DISABLETXDRIVE
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ModulatorDriver = Comms.SendMSA(DOACTION, Send, 1, rcv, 0)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not ModulatorDriver Then Logger.logFATAL("Error: Failed to " & what & " mod driver.")
	End Function
	
	
	
	
	Public Function SaveModulatorSettings() As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		Send(1) = SAVEMODSETTINGS
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then
			SaveModulatorSettings = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to save modulator settings.")
			SaveModulatorSettings = False
		End If
	End Function
	
	
	
	
	Public Function RecallModulatorSettings() As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		Send(1) = RECALLMODSETTINGS
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then
			RecallModulatorSettings = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to recall modulator settings.")
			RecallModulatorSettings = False
		End If
	End Function
	
	
	
	
	Public Function SetFirstChanBand(ByRef band As String) As Boolean
		If band = "C" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetFirstChanBand = Comms.WriteCal(FIRSTCHANBAND, CDbl(CBANDCODE), 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetFirstChanBand = Comms.WriteCal(FIRSTCHANBAND, CDbl(LBANDCODE), 1)
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetFirstChanBand Then Logger.logFATAL("Error: Failed to set First Channel Band.")
	End Function
	
	
	
	Public Function SetLastChanBand(ByRef band As String) As Boolean
		If band = "C" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLastChanBand = Comms.WriteCal(LASTCHANBAND, CDbl(CBANDCODE), 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLastChanBand = Comms.WriteCal(LASTCHANBAND, CDbl(LBANDCODE), 1)
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetLastChanBand Then Logger.logFATAL("Error: Failed to set Last Channel Band.")
	End Function
	
	
	
	Public Function SetPinModeBand(ByRef band As String) As Boolean
		If band = "C" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetPinModeBand = Comms.WriteCal(PINMODEBAND, CDbl(CBANDCODE), 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetPinModeBand = Comms.WriteCal(PINMODEBAND, CDbl(LBANDCODE), 1)
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPinModeBand Then Logger.logFATAL("Error: Failed to set Pin Mode Band.")
	End Function
	
	
	
	
	Public Function SetFirstChanFreq(ByRef freq As Double) As Boolean
		' Frequency must be in GHz.
		' NOTE: We are actually setting the ITU channel number here but this defines
		' the frequency.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetFirstChanFreq = Comms.WriteCal(FIRSTCHANFREQ, CDbl(THz2ITUChannel(freq / 1000#)), 16)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetFirstChanFreq Then Logger.logFATAL("Error: Failed to set First Channel Frequency.")
	End Function
	
	
	
	
	Public Function SetLastChanFreq(ByRef freq As Double) As Boolean
		' Frequency must be in GHz.
		' NOTE: We are actually setting the ITU channel number here but this defines
		' the frequency.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetLastChanFreq = Comms.WriteCal(LASTCHANFREQ, CDbl(THz2ITUChannel(freq / 1000#)), 16)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetLastChanFreq Then Logger.logFATAL("Error: Failed to set Last Channel Frequency.")
	End Function
	
	
	
	
	Public Function SetPinModeFreq(ByRef freq As Double) As Boolean
		' Frequency must be in GHz.
		' NOTE: We are actually setting the ITU channel number here but this defines
		' the frequency.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetPinModeFreq = Comms.WriteCal(PINMODEFREQ, CDbl(THz2ITUChannel(freq / 1000#)), 16)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPinModeFreq Then Logger.logFATAL("Error: Failed to set Pin Mode Frequency.")
	End Function
	
	
	
	
	'UPGRADE_NOTE: space was upgraded to space_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Public Function SetChanSpacing(ByRef space_Renamed As Short) As Boolean
		Select Case space_Renamed
			Case 25
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetChanSpacing = Comms.WriteCal(CHANSPACING, 8, 1)
			Case 50
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetChanSpacing = Comms.WriteCal(CHANSPACING, 16, 1)
			Case 100
				'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				SetChanSpacing = Comms.WriteCal(CHANSPACING, 32, 1)
		End Select
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetChanSpacing Then Logger.logFATAL("Error: Failed to set Channel Spacing.")
	End Function
	
	
	
	
	Public Function SaveBootData() As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		Send(1) = SAVETOEEPROM
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then
			SaveBootData = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to Save Boot Data.")
			SaveBootData = False
		End If
	End Function
	
	
	
	
	Public Function OIFLaser(ByRef what As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(1) As Byte
		If what = "enable" Then
			Send(1) = ENABLEOIFLASER
		Else
			Send(1) = DISABLEOIFLASER
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DOACTION, Send, 1, rcv, 0) Then
			OIFLaser = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Laser enable failed.")
			OIFLaser = False
		End If
	End Function
	
	
	
	Public Function SetTxCommandLsEnable(ByRef what As String) As Boolean
		' The LsEnable bit is bit 0 of byte 3 in the MSA Tx Command Register. If this bit
		' is clear then the laser is enabled.
		Dim b1 As Byte
		Dim b2 As Byte
		Dim b3 As Byte
		
		'UPGRADE_WARNING: Couldn't resolve default property of object MSA.ReadTxCommand. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not MSA.ReadTxCommand(b1, b2, b3) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: SetTxCommandLsEnable failed to read register.")
			SetTxCommandLsEnable = False
		Else
			b3 = b3 And &HFEs ' Clear the bit (laser enabled).
			If what = "off" Then b3 = b3 + 1 ' Laser disabled.
			'UPGRADE_WARNING: Couldn't resolve default property of object MSA.SetTxCommand. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If Not MSA.SetTxCommand(b1, b2, b3) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: SetTxCommandLsEnable failed to set register.")
				SetTxCommandLsEnable = False
			Else
				SetTxCommandLsEnable = True
			End If
		End If
		
	End Function
	
	
	
	Private Function SetCalString(ByRef cmd As Short, ByRef dat As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(MAX_MSA_PARAM_LENGTH + 2) As Byte
		'UPGRADE_WARNING: Lower bound of array rcv was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim rcv(MAX_MSA_PARAM_LENGTH + 2) As Byte
		Dim lenf As Short
		Dim maxlenf As Short
		Dim indx As Short
		
		On Error GoTo ErrorHandler
		
		Select Case cmd
			Case 0
				Send(1) = MODULESERIALNUMBER
				maxlenf = 16
			Case 1
				Send(1) = MODULEMANUFACTUREDATE
				maxlenf = 8
			Case 2
				Send(1) = MODULEHARDWAREREVISION
				maxlenf = 8
			Case 3
				Send(1) = MODULEPARTNUMBER
				maxlenf = 16
			Case Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logMESSAGE("Unknown command sent to SetCalString (" & CStr(cmd) & ")")
				GoTo Aborted
		End Select
		
		lenf = Len(dat)
		If lenf > maxlenf Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logMESSAGE("Data length too long in SetCalString (" & CStr(cmd) & ")")
			GoTo Aborted
		Else
			For indx = 1 To lenf
				Send(indx + 1) = Asc(Mid(dat, indx, 1))
			Next indx
			' Pad to max length with nulls if necessary.
			For indx = lenf + 1 To maxlenf
				Send(indx + 1) = 0
			Next indx
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If Comms.SendMSA(SETSTRING, Send, maxlenf + 1, rcv, 0) Then
				SetCalString = True
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to Set Cal String (" & dat & ")")
				SetCalString = False
			End If
			msDelay((200)) ' These commands take a long time, so wait.
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SetCalString")
Aborted: 
		SetCalString = False
	End Function
	
	
	
	Public Function SetSerialNumber(ByRef what As String) As Boolean
		SetSerialNumber = SetCalString(MODULESERIALNUMBER, what)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetSerialNumber Then Logger.logFATAL("Error: Failed to set module Serial Number.")
	End Function
	
	
	
	Public Function SetManufactureDate(ByRef what As String) As Boolean
		SetManufactureDate = SetCalString(MODULEMANUFACTUREDATE, what)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetManufactureDate Then Logger.logFATAL("Error: Failed to set module Manufacture Date.")
	End Function
	
	
	
	Public Function SetHardwareRevision(ByRef what As String) As Boolean
		SetHardwareRevision = SetCalString(MODULEHARDWAREREVISION, what)
	End Function
	
	
	
	Public Function SetPartNumber(ByRef what As String) As Boolean
		SetPartNumber = SetCalString(MODULEPARTNUMBER, what)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPartNumber Then Logger.logFATAL("Error: Failed to set module Part Number.")
	End Function
	
	Public Function SetLaserTempByAdc(ByRef adc As Double) As Boolean
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetLaserTempByAdc = Comms.WriteCal(&H3Fs, adc, 1)
	End Function
	
	Public Function GetLaserTempByAdc() As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadCal. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetLaserTempByAdc = Comms.ReadCal(&H3Fs, 1)
    End Function

    ''' <summary>
    ''' Perform an I2C transaction with byte arrays.
    ''' </summary>
    ''' <param name="Cmd">MSA command byte.</param>
    ''' <param name="CmdPayload">Command payload in a one-based array. (Item #0 must be included. It will be ignored.)</param>
    ''' <param name="RespPayload">One-based array to hold the response payload. (Item #0 should be ignored.)</param>
    ''' <param name="ActualLen">Length of resonse, not including item #0.</param>
    ''' <returns>True if successful. False on error.</returns>
    Public Function TransactionWithBytes(ByVal Cmd As Byte, ByVal CmdPayload As Byte(), ByRef RespPayload As Byte(), ByRef ActualLen As Byte) As Boolean
        Return Comms.SendMSA(Cmd, CmdPayload, CmdPayload.Length - 1, RespPayload, MAX_MSA_PARAM_LENGTH, ActualLen)
    End Function

End Class