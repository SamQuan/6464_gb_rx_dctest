using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace IxoliteGetNopTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        IxoliteLibDotNet.LibSetup ixoSetup;
        IxoliteLibDotNet.OIFStandardCommands ixoOifStd;

        private void Form1_Load(object sender, EventArgs e)
        {
            ixoSetup = new IxoliteLibDotNet.LibSetup();
            ixoSetup.IxoInitialise(new MyMsgLogger());

            //            short comPort = 1;
            //            string rate = "9600";
            //            short parPort = 0x378;
            //            ixoSetup.SetSerialCommsWithParallelPinControl(ref comPort, ref rate, ref parPort);
            System.IO.Ports.SerialPort sp = new System.IO.Ports.SerialPort("COM1", 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            ixoSetup.SetSerialCommsWithParallelPinControl(sp, 0x378);

            ixoOifStd = new IxoliteLibDotNet.OIFStandardCommands();

        }

        string sendNop()
        {
            int nop = ixoOifStd.GetNOP();
            return "NOP: " + nop + "\r\n" + readLog() + "\r\n";
        }

        string getSerNo()
        {
            string sn = ixoOifStd.GetSerialNumber();
            return "SN: " + sn + "\r\n" + readLog() + "\r\n";
        }

        string readLog()
        {
            string log = "";
            while (ixoSetup.IsLogEmpty() == false)
            {
                log += ixoSetup.LogRead() + "\r\n";
            }

            return log;
        }    

        private void setButton_Click(object sender, EventArgs e)
        {
            CheckBox[] boxes = { d0CheckBox, d1CheckBox, d2CheckBox, d3CheckBox,
                                 d4CheckBox, d5CheckBox, d6CheckBox, d7CheckBox };
            byte mask = 0;
            byte bits = 0;

            int i;
            for (i = 0; i < 8; ++i)
            {
                if (boxes[i].CheckState != CheckState.Indeterminate)
                {
                    mask |= (byte)(1U << i);
                }

                if (boxes[i].CheckState == CheckState.Checked)
                {
                    bits |= (byte)(1U << i);
                }
            }

            //            MessageBox.Show("mask=" + mask + " bits=" + bits);

            ixoSetup.SetParPortData(mask, bits);
        }

        private void getButton_Click(object sender, EventArgs e)
        {
            byte status = ixoSetup.GetParPortStatus();
            string labText = "";

            int i;
            for (i = 0; i < 8; ++i)
            {
                labText = (((status & (1 << i)) != 0) ? "1" : "0") + " " + labText;
            }

            parStatusLabel.Text = labText;
        }

        const int maxCountdown = 10;
        int countdown;

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            countdown = maxCountdown;
            progressBar1.Maximum = maxCountdown;
            button1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            countdown -= 1;

            if (countdown == 0)
            {
                countdown = maxCountdown;
                getButton_Click(null, null);

                //noptextBox.Text = getSerNo();
            }

            progressBar1.Value = countdown;

        }

        const int maxCountdownNop = 10;
        int countdownNop;


        private void timer2_Tick(object sender, EventArgs e)
        {
            countdownNop -= 1;

            if (countdownNop == 0)
            {
                countdownNop = maxCountdownNop;
                noptextBox.Text = sendNop();
            }
        }

        private void singleStatusReadButton_Click(object sender, EventArgs e)
        {
            noptextBox.Text =
                sendNop() +
                getSerNo() +
                "";
        }
    }
}