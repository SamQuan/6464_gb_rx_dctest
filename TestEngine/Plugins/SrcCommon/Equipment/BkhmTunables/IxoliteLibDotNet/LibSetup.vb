Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("LibSetup_NET.LibSetup")> Public Class LibSetup
	
	' [Copyright]
	'
	' Bookham Test Library
	' IxoliteLib
	'
	' LibSetup.cls
	'
	' Author: Bill Godfrey
	' Design: Tunable Module Drivers DD
	
	' Pre-constructed objetcs for use as the global Comms object.
	Private parCommsObj As ParallelComms
	Private serCommsObj As SerialBinaryComms
	
	' Class constructor. Does nothing.
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	' Set up Logger.
    Public Sub IxoInitialise(ByVal msgLoggerFromTestEngine As IMsgLogger)
        Logger = msgLoggerFromTestEngine
        'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMsgQueueInit. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Call Logger.logMsgQueueInit()
    End Sub
	
	' Set up the global Comms object and configure for I2C use.
	' parPortAddr is the address of the parallel port (0x378 = LPT1).
	' moduleAddr is the I2C address of the module. (Typically 0x41).
    Public Sub SetParallelComms(ByRef parPortAddr As Long, ByRef moduleAddr As Byte, _
        ByRef maxI2CRate_kHz As Integer)

        ' Use the parallel comms object as the Comms global variable.
        parCommsObj = New ParallelComms
        Comms = parCommsObj

        ' Setup parallel port and configure the underlying library.
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.PortAddress. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Comms.PortAddress = parPortAddr
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ApplyInterfaceSettings. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Comms.ApplyInterfaceSettings()

        ' Setup module address. It is shifted left by 1 bit so the 7 bits occupy the
        ' correct slots in the first I2C byte of a transaction.
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.DeviceAddress. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Comms.DeviceAddress = moduleAddr * 2
        If maxI2CRate_kHz > 0 Then
            Comms.MaxI2CRate_kHz = maxI2CRate_kHz
        End If
        ' Complete initailisation.
        'UPGRADE_WARNING: Couldn't resolve default property of object Comms.Init. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Call Comms.Init()

    End Sub

    ' Restore context of the  global Comms Object - needed when several devices 
    ' require the services of the Comm Object on different Ports/Device address/Max I2C rate.
    ' parPortAddr is the address of the parallel port (0x378 = LPT1).
    ' moduleAddr is the I2C address of the module. (Typically 0x41).
    Public Sub RestoreParallelCommsContext(ByRef parPortAddr As Long, ByRef moduleAddr As Byte, _
        ByRef maxI2CRate_kHz As Integer)
        Comms.PortAddress = parPortAddr
        Comms.DeviceAddress = moduleAddr * 2
        If maxI2CRate_kHz > 0 Then
            Comms.MaxI2CRate_kHz = maxI2CRate_kHz
        End If

    End Sub
    

    ' Set up the global Comms object and congure for OIF Serial use.
    ' comPortAddr is the address of the serial port. COM(n).
    ' rate is a string holding the port speed. "9600", "19200", "38400", "57600", "115200"
    Public Sub SetSerialComms(ByVal SerPort As System.IO.Ports.SerialPort)
        SerialPortComms = SerPort
        SerialPortComms.Open()
        GC.SuppressFinalize(SerialPortComms.BaseStream)
        SerialPortComms.DiscardNull = False
        SerialPortComms.DiscardOutBuffer()

        ' Use the SerialBinaryComms object as the Comms global variable.
        serCommsObj = New SerialBinaryComms
        Comms = serCommsObj
    End Sub

    ' As SetSerialComms, but also set up the parallel port for use as simple pin control.
    Public Sub SetSerialCommsWithParallelPinControl(ByVal SerPort As System.IO.Ports.SerialPort, ByVal parPortAddr As Long)
        ' Configure parallel comms, setting parCommsObj.
        Call SetParallelComms(parPortAddr, 1, 0)

        ' Set up serial port driver, going over the global Comms object set by the previous function.
        Call SetSerialComms(SerPort)
    End Sub

    ' Sets the masked bits of the parallel port data.
    Public Sub SetParPortData(ByVal mask As Byte, ByVal bits As Byte)
        Call parCommsObj.SetParPortData(mask, bits)
    End Sub

    ' Gets the parallel port status register.
    Public Function GetParPortStatus() As Byte
        GetParPortStatus = parCommsObj.GetParPortStatus()
    End Function

    ' Return flag indicating if the log queue is empty or not.
    Public Function IsLogEmpty() As Boolean
        IsLogEmpty = Logger.logMsgQueueEmpty()
    End Function

    ' Fetch a single line from the log queue. Only use if IsLogEmpty returned false.
    Public Function LogRead() As String
        LogRead = Logger.logMsgQueueRead()
    End Function
End Class