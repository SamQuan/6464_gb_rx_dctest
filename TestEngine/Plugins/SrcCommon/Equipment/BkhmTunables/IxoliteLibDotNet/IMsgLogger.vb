' [Copyright]
'
' Bookham Test Engine Library
' Tunable Modules Driver
'
' IMsgLogger.vb
' 
' Author: Bill Godfrey
' Design: Tunable Module Driver DD

''' <summary>
''' Generic interface replacing the Logger.frm class from VB6. C# wrapper code will implement this interface.
''' </summary>
Public Interface IMsgLogger
    ''' <summary>
    ''' Initialise the internal queue.
    ''' </summary>
    Sub logMsgQueueInit()

    ''' <summary>
    ''' Add an internal event log message to the internal queue.
    ''' </summary>
    ''' <param name="errnum">Eroor code.</param>
    ''' <param name="errsrc">Error source.</param>
    ''' <param name="ErrMsg">Message</param>
    ''' <param name="where">Location</param>
    Sub LogINTERNAL(ByRef errnum As Short, ByRef errsrc As String, ByRef ErrMsg As String, Optional ByRef where As String = "defaultvalue")

    ''' <summary>
    ''' Log a fatal error to the internal queue.
    ''' </summary>
    ''' <param name="strErrMsg">Error text.</param>
    Sub logFATAL(ByRef strErrMsg As String)

    ''' <summary>
    ''' Log a normal non-error event to the internal queue.
    ''' </summary>
    ''' <param name="strErrMsg">Message text.</param>
    Sub logMESSAGE(ByRef strErrMsg As String)

    ''' <summary>
    ''' Check if the internal queue is empty.
    ''' </summary>
    ''' <returns>True if empty.</returns>
    Function logMsgQueueEmpty() As Boolean

    ''' <summary>
    ''' Take off a string from the internal queue and return it.
    ''' </summary>
    ''' <returns>String from internal queue, prefixed with 'E'
    ''' indicating error or 'M' indicating a normal message.</returns>
    ''' <remarks>Only to be called when logMsgQueueEmpty returns false.</remarks>
    Function logMsgQueueRead() As String

    ''' <summary>
    ''' Add a byte to the current transaction, to be later returned by LastMsgSentStr.
    ''' </summary>
    ''' <param name="DataByte">Byte to add.</param>
    Sub logLastMsgRcvd(ByRef DataByte As Byte)

    ''' <summary>
    ''' Add a byte to the current transaction, to be later returned by LastMsgSentStr.
    ''' </summary>
    ''' <param name="DataByte">Byte to add.</param>
    Sub logLastMsgSent(ByRef DataByte As Byte)

    ''' <summary>
    ''' Clear current log of bytes.
    ''' </summary>
    Sub clearLastMsgSent()

    ''' <summary>
    ''' Clear current log of bytes.
    ''' </summary>
    Sub clearLastMsgRcvd()

    ''' <summary>
    ''' Return logged bytes as a string of hex digits, byte values separated by spaces.
    ''' </summary>
    ''' <returns>Hex string.</returns>
    Function LastMsgSentStr() As String

    ''' <summary>
    ''' Return logged bytes as a string of hex digits, byte values separated by spaces.
    ''' </summary>
    ''' <returns>Hex string.</returns>
    Function LastMsgRcvdStr() As String
End Interface

