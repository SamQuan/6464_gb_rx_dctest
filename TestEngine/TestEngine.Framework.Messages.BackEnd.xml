<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestEngine.Framework.Messages.BackEnd</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy">
            <summary>
            Delivery agent for all objects running in the UI thread and astraction layer for message
            centre.
            See Windows Forms Programming in C# by Chris Sells, chapter 14.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.#ctor(System.ComponentModel.ISynchronizeInvoke)">
            <summary>
            Constructor. Called by main form constructor.
            </summary>
            <remarks>Must only be constructed within UI thread.</remarks>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.invokeControlRef">
            <summary>
            Main form reference for calling BeginInvoke.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.uiThreadHashCode">
            <summary>
            Hash code of UI thread.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IReceiveMsg)">
            <summary>
            Register control as node with message centre.
            </summary>
            <param name="name">New node name.</param>
            <param name="ctl">Control implementing IReceiveMsg.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.RemoveNode(System.String)">
            <summary>
            Remove node from message centre.
            </summary>
            <param name="name">Name of node to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.RegisterRestrictedSignal(System.String,System.String)">
            <summary>
            Register restricted signal.
            </summary>
            <param name="sigId">New signal name.</param>
            <param name="nodeName">Restrict to this node.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.RegisterUnrestrictedSignal(System.String)">
            <summary>
            Register unrestricted signal.
            </summary>
            <param name="sigId">New signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.SubscribeSignal(System.String,System.String)">
            <summary>
            Subscribe named node to signal.
            </summary>
            <param name="nodeName">Node name to subscribe.</param>
            <param name="sigId">Signal name.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.UnsubscribeSignal(System.String,System.String)">
            <summary>
            Unsubscribe named node from signal.
            </summary>
            <param name="nodeName">Unsubscribing node name.</param>
            <param name="sigId">Signal name to unsubscribe from.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.SendAsync(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send a new asynchronous message. (Not a response.)
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Payload object reference.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.SendAsync(System.String,System.String,System.Object,System.Int64,System.Int64)">
            <summary>
            Send an asynchronous message as a response.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Payload object reference.</param>
            <param name="msgSeq">Message sequence number.</param>
            <param name="respSeq">Sequence number fo message being responded to.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.SendSignal(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Broadcast a signal to all subscribed nodes.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="signalId">Signal name.</param>
            <param name="payload">Payload object reference.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.DeliverMsg(Bookham.TestEngine.Framework.Messages.MsgContainer)">
            <summary>
            Implements this class as delivery agent. Called within the sender's thread.
            </summary>
            <param name="msgCont">Message container to deliver.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.DeliverWithinUiThread(Bookham.TestEngine.Framework.Messages.MsgContainer)">
            <summary>
            Called by the DeliverMsg, passing the message container on to the final recipient.
            </summary>
            <param name="msgCont">Message container.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.BackEnd.GuiProxy.DeliverWithinUiThreadDlg">
            <summary>
            Delegate to hold reference to the DeliverWithinUiThread function.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry">
            <summary>
            The core of the message centre, including the registry and the means
            to send messages and signals.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.mutexTimeout_ms">
            <summary>
            Mutex timeout delay. (10 seconds.)
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.#ctor">
            <summary>
            Construct message centre instance.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object,System.String)">
            <summary>
            Register a node.
            </summary>
            <param name="nodeName">Name of node to register. '*' characters will be replaced with digits.</param>
            <param name="delivery">Link to delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
            <param name="parentNodeName">Node which will contain nodeName's node as stand-in.</param>
            <returns>New node name, including '*' replacement.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.RemoveNode(System.String,System.String)">
            <summary>
            Remove the named node.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
            <param name="parentNodeName">Node which will contain nodeName's node as stand-in.></param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object)">
            <summary>
            Register a node.
            </summary>
            <param name="nodeName">Name of node to register. '*' characters will be replaced with digits.</param>
            <param name="delivery">Link to delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
            <returns>New node name, including '*' replacement.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.SecureNodeToThread(System.String)">
            <summary>
            Set the named node's thread to the current thread.
            </summary>
            <param name="nodeName">Name of node to secure.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.RemoveNode(System.String)">
            <summary>
            Remove the named node.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.checkThreadSecurityAndThrow(System.String)">
            <summary>
            Check the current thread is permitted to identify itself as this node. Throw if it is
            not.
            </summary>
            <param name="nodeName">Name of node to check.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.RegisterRestrictedSignal(System.String,System.String)">
            <summary>
            Register a signal which may only be sent by the named node.
            </summary>
            <param name="sigId">Name of new signal.</param>
            <param name="nodeName">Name of node who may send signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.RegisterUnrestrictedSignal(System.String)">
            <summary>
            Register a signal which may be sent by any node.
            </summary>
            <param name="sigId">Name of new signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.SubscribeSignal(System.String,System.String)">
            <summary>
            Subscribe node to the named signal.
            </summary>
            <param name="nodeName">Node subscribing...</param>
            <param name="sigId">... to this signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.UnsubscribeSignal(System.String,System.String)">
            <summary>
            Unscribe node from the named signal.
            </summary>
            <param name="nodeName">Node to unsubscribe.</param>
            <param name="sigId">From this signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.SendAsync(System.String,System.String,System.Object,System.Int64,System.Int64)">
            <summary>
            Send response message via delivery agent.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">This message sequence number.</param>
            <param name="respSeq">Sequence number of message being responded to.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.SendAsync(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send new message via delivery agent.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.SendSignal(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send signal to many nodes via delivery agent.
            </summary>
            <param name="sender">Sender node.</param>
            <param name="signalId">Signal id.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.mutex">
            <summary>
            Registry mutex.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.nodeRegistry">
            <summary>
            The node registry.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.nextStar">
            <summary>
            Star characters will be replaced with numbers. Use this value which increases with
            each use.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.BackEnd.MsgCentreRegistry.signalRegistry">
            <summary>
            Signal registry, holds signals, restriction and subscriptions.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.BackEnd.NamespaceDoc">
            <summary>
            Test Engine Messaging Framework Backend code
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.NodeRegistry">
            <summary>
            A node registry for use by the message centre.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.#ctor">
            <summary>
            Construct node registry.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.AddNode(Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData)">
            <summary>
            Add node.
            </summary>
            <param name="newNode">Name for new node.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.IsNodeRegistered(System.String)">
            <summary>
            Check if a node name is already registered.
            </summary>
            <param name="nodeName">Node name to check for.</param>
            <returns>True if registered. False if available.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.GetNode(System.String)">
            <summary>
            Look up and retrieve a node by the name.
            </summary>
            <param name="nodeName">Node name to find.</param>
            <returns>Node data for named node.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.RemoveNode(System.String)">
            <summary>
            Remove a node from the registry.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.checkNodeRegisteredAndThrow(System.String)">
            <summary>
            Check if node is registered and throw if it is not.
            </summary>
            <param name="nodeName">Name of node to check for.</param>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.NodeRegistry.nodes">
            <summary>
            Map of NodeData instances, indexed by node name.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData">
            <summary>
            Stores all information about one node.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.name">
            <summary>
            Name of node. (Key field)
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.deliveryAgent">
            <summary>
            Reference to delivery agent.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.deliveryTag">
            <summary>
            Delivery agent's tag object.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.securityThreadHashCode">
            <summary>
            If non-zero, this node is secured to the thread which yields this hash code.
            If zero, any thread may identify itself as this node.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.standin">
            <summary>
            standin will be used in fact to replace the instant which contains it.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.#ctor(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object)">
            <summary>
            Construct node data.
            </summary>
            <param name="name">Name of node.</param>
            <param name="deliveryAgent">Node's delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.Standin">
            <summary>
            Gets and sets standin which is used in MsgCentreRegistry.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.Name">
            <summary>
            Gets the node name.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.DeliveryAgent">
            <summary>
            Gets the delivery agent.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.DeliveryTag">
            <summary>
            Gets the delivery tag.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.NodeRegistry.NodeData.SecurityThreadHashCode">
            <summary>
            Gets and sets the security thread.
            </summary>
            <remarks>Zero means any node may identify itself as this node.</remarks>
            <exception>Thrown when on second call to set. (Only set once.)</exception>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.SignalRegistry">
            <summary>
            The SignalRegistry is used by the Message centre to keep track of signal registrations
            and subscriptions.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.SignalRegistry.signals">
            <summary>
            Collection of SignalData instances, indexed by signal id.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.AddSignal(Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData)">
            <summary>
            Add a new signal to the registry.
            </summary>
            <param name="newSig">New signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalExists(System.String)">
            <summary>
            Checks if stated signal name exists.
            </summary>
            <param name="sigId">Name to check for.</param>
            <returns>True if signal id exists. False if not.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.GetSignalData(System.String)">
            <summary>
            Get signal data, or null if signal does not exist.
            </summary>
            <param name="sigId">Name of sigal to get.</param>
            <returns>Signal data, or null if it does not exist.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.IsNodePermitted(System.String,System.String)">
            <summary>
            Check if a signal is restricted to the named node.
            </summary>
            <param name="sigId">Signal in question.</param>
            <param name="nodeName">Name of node to check for.</param>
            <returns>
            True if node is restricted to signal or unrestricted.
            False if signal is restructed to another node.
            </returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.UnsubscribeAllByNode(System.String)">
            <summary>
            Remove named node from all subscription lists.
            </summary>
            <param name="nodeName">Name of node to unsubscribe.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData">
            <summary>
            SignalData stores information about a single signal, including all subscribers.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.sigId">
            <summary>
            Signal id
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.restrictNode">
            <summary>
            Restricted to named node or empty string for unrestricted.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.subscribers">
            <summary>
            Set of subscriber node name strings. Only LHS is used as a set collection.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.#ctor(System.String)">
            <summary>
            Construct an unrestricted signal record.
            </summary>
            <param name="sigId">New signal id.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.#ctor(System.String,System.String)">
            <summary>
            Construct a rrestricted signal record.
            </summary>
            <param name="sigId">New signal id.</param>
            <param name="restrictNode">Owner node name.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.AddSubscriber(System.String)">
            <summary>
            Add named node to subscriber list.
            </summary>
            <param name="nodeName">Name of node to add.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.DelSubscriber(System.String)">
            <summary>
            Delete subscriber.
            </summary>
            <param name="nodeName">Subscriber to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.GetSubscribers">
            <summary>
            Return a new arraylist of subscribers to this signal.
            </summary>
            <returns>ArrayList of strings. Each item is a subscriber.</returns>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.SigId">
            <summary>
            Gets signal Id.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.Restricted">
            <summary>
            Gets restricted state.
            </summary>
            <value>True if restricted. False if not.</value>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.SignalRegistry.SignalData.RestrictedToNode">
            <summary>
            Gets restriction node name, or empty string.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.BackEnd.Startup">
            <summary>
            Message centre initialisation tools. Public BackEnd interface.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.Startup.#ctor">
            <summary>
            Private constructor to prevent construction.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.Startup.InitialiseMsgCentre">
            <summary>
            Initialise message centre.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.BackEnd.Startup.NewGuiProxy(System.ComponentModel.ISynchronizeInvoke)">
            <summary>
            Construct a new gui proxy class as a restricted interface.
            </summary>
            <returns>New instance of a GUI Proxy.</returns>
        </member>
    </members>
</doc>
