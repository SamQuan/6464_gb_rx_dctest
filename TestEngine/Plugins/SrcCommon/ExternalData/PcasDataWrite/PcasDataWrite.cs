// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasDataWrite\PcasDataWrite.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Collections.Specialized;	// StringDictionary
using System.Globalization;				// CultureInfo
using System.Text;						// StringBuilder
using System.IO;                        // System Input/Output
using System.Xml;						// XML Reader
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Generic;


namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// Implementation of IDataWrite for PCAS.
	/// </summary>
    public class PcasDataWrite : IDataWrite
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="logger">Reference to logging component</param>
        /// <param name="dropDirectory">Path to the location of the drop directory. The directory 'pcasdrop' will be created in the application base directory if this parameter is empty.</param>		
        public PcasDataWrite(ILogging logger, string dropDirectory)
        {
            if (logger == null)
            {
                throw new BookhamExternalDataException("'logger' is null");
            }
            this.logger = logger;

            if (dropDirectory == null || dropDirectory.Length == 0)
            {
                dropArea = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory.ToString(),
                    "pcasdrop");
            }
            else
            {
                dropArea = dropDirectory;
            }
            // If the output area is not writeable this will throw a fatal exception when 
            // the component is initialised.
            createDirectory(dropArea, this);

            // Read the location of the Blob data drop directory from configuration, if present.
            SetBlobDataDropDirectory();
        }

        /// <summary>
        /// Creates a directory.
        /// </summary>
        /// <param name="directoryName">Name of directory to create.</param>
        /// <param name="thisObject">This Object</param>
        private static void createDirectory(string directoryName, PcasDataWrite thisObject)
        {
            if (!System.IO.Directory.Exists(directoryName))
            {
                System.IO.Directory.CreateDirectory(directoryName); 
                // Write log entry
                thisObject.logger.DatabaseTransactionWrite(PcasDataWrite.LogLabel, "Created Results drop area : " + directoryName);
            }
        }

        /// <summary>
        /// Sets the location of the Blob data streamer drop directory.
        /// This is found in configuration file PcasWriteConfig.xml, if present.
        /// If no alternative configuration is given, then the Blob data streamer drop directory 
        /// is left at the default of ./pcasBlobDataStreamerDrop.
        /// </summary>
        private void SetBlobDataDropDirectory()
        {
            const string configDataFileName = "Configuration/PcasWriteConfig.xml";

            if (File.Exists(configDataFileName))
            {
                XmlTextReader configData = new XmlTextReader(configDataFileName);

                try
                {
                    // Read the file
                    while (configData.Read())
                    {
                        // Start of element.
                        if (configData.NodeType == XmlNodeType.Element)
                        {
                            if (configData.LocalName == "RawDataDropDirectory")
                            {
                                blobDataStreamerDropDirectory = configData.ReadString();
                            }
                            if (configData.LocalName == "RenameBlobDataFiles")
                            {
                                string renameBlobData = configData.ReadString();
                                renameBlobDataFiles = Boolean.Parse(renameBlobData);
                            }
                        }
                    } // Finished reading
                }
                catch (UnauthorizedAccessException uae)
                {
                    throw new BookhamExternalDataException("Error accessing config file '" + System.IO.Path.GetFileName(configDataFileName) + "'", uae);
                }
                catch (System.IO.FileNotFoundException fnfe)
                {
                    throw new BookhamExternalDataException("Unable to locate config file '" + System.IO.Path.GetFileName(configDataFileName) + "'", fnfe);
                }
                catch (System.IO.DirectoryNotFoundException dnfe)
                {
                    throw new BookhamExternalDataException("Unable to locate configuration directory", dnfe);
                }
                catch (System.IO.IOException ioe)
                {
                    throw new BookhamExternalDataException("IO error accessing config file '" + System.IO.Path.GetFileName(configDataFileName) + "'", ioe);
                }
                catch (System.Xml.XmlException xe)
                {
                    throw new BookhamExternalDataException("XML error with config file '" + System.IO.Path.GetFileName(configDataFileName) + "'", xe);
                }
                finally
                {
                    configData.Close();
                }
            }
        }

		#region IDataWrite Member
		/// <summary>
		/// Extracts test data that matches specification specName. Creates a drop file in pcas format.
		/// </summary>
		/// <param name="testData">The set of testData gathered so far.</param>
		/// <param name="keyData">Must contain SCHEMA, SERIAL_NUMBER, DEVICE_TYPE and TEST_STAGE</param>
		/// <param name="specName">A name of a specification within TestData</param>
		/// <param name="ignoreMissing">Whether to ignore null result values within the test data.</param>
        /// <param name="usingUtcDateTime">If true,use UTC Date Time,otherwise,use local date time.</param>
        /// <returns>Was there missing data? Always false if "ignoreMissing" is true</returns>
        public bool Write(TestData testData, StringDictionary keyData, string specName, bool ignoreMissing, bool usingUtcDateTime)
		{
            bool nodeIdFound = false;
            // flag that indicates whether there is any data missing from the specification...
            bool dataMissingFlag = false;

			if ( testData == null )
			{
				logger.ErrorInExternalDataPlugin("Write called with null testData");
				return false;			// unreachable
			}

			// Check that the required key data is present
			CheckKeyData(keyData);
			
			// CI note :
			// System.IO.Path does not provide a means of creating a temp directory.
			// We could use System.IO.Path.GetTempFileName(), but for debugging purposes 
			// I would prefer to see temp files dropped in their own subdirectory.
			
			// Make a temporary area in the system temp path
			string localDir = Path.Combine(	Path.GetTempPath(), "tempDrop") ;
			createDirectory(localDir, this);

			StringDictionary resultsList = new StringDictionary();

			Specification specData = testData.Specifications[specName];

			if (specData != null)
			{
                //First extract the Test Set NODE identifier for this data.
                // Loop through each param in the spec to find it.
                foreach (ParamLimit parmRef in specData)
                {
                    if (parmRef != null)
                    {
                        // Set a flag if the NODE parameter has been found in the data.
                        if (parmRef.ExternalName == "NODE")
                        {
                            if (parmRef.MeasuredData == null)
                            {
                                //Trying to save data where no NODE is specified. This is not allowed.
                                throw new BookhamExternalDataException("Parameter NODE must contain data");
                            }

                            
                            if (parmRef.MeasuredData.Type != DatumType.Sint32)
                            {
                                //Trying to save data where no NODE is specified. This is not allowed.
                                throw new BookhamExternalDataException("Parameter NODE must be of type DatumSint32");
                            }


                            nodeIdFound = true;
                            DatumSint32 nodeNumber = (DatumSint32)parmRef.MeasuredData;
                            this.nodeId = Convert.ToString(nodeNumber.Value);
                            break;
                        }
                    }
                }

                if (nodeIdFound == false)
                {
                    //Trying to save data where no NODE is specified. This is not allowed.
                    throw new BookhamExternalDataException("Attempting to store data in PCAS, without specifying Test Set NODE Identifier.");
                }

                List<string> autoCreatableParamList = new System.Collections.Generic.List<string>();
                autoCreatableParamList.Add("DAY");
                autoCreatableParamList.Add("HOUR");
                autoCreatableParamList.Add("TIME");
                autoCreatableParamList.Add("WEEK_NUMBER");
                autoCreatableParamList.Add("DATE_1900");
                List<string> autoCreatingParamList = new List<string>();
                

				// Loop through each param in the spec
				foreach ( ParamLimit parmRef in specData )
				{ 
					if (parmRef != null)
					{
						bool paramIsPcasPrimaryKey = (parmRef.ExternalName=="SERIAL_NO" || parmRef.ExternalName=="TIME_DATE");
                        
						if ( parmRef.MeasuredData != null )
						{
							string valAsString = parmRef.MeasuredDataRounded.ValueToString();

                            if (parmRef.MeasuredData.GetType() == typeof(DatumFileLink))
                            {
                                //Need to copy the plot data to the streamer directory.
                                DatumFileLink datumFileLink = (DatumFileLink)parmRef.MeasuredData;

                                string blobDataStreamerDropDirectoryForNode = blobDataStreamerDropDirectory + @"\" + this.nodeId;

                                if (!Directory.Exists(blobDataStreamerDropDirectoryForNode))
                                {
                                    Directory.CreateDirectory(blobDataStreamerDropDirectoryForNode);
                                    // Write log entry for creating BLOB directory
                                    this.logger.DatabaseTransactionWrite(PcasDataWrite.LogLabel, "Created BLOB drop area : " + this.blobDataStreamerDropDirectory); 
                                }

                                //Get the full path to the blob data in local storage.
                                string originalBlobFileFullPath = datumFileLink.GetDirectoryPath() + @"\" + datumFileLink.GetFileName();

                                if (!File.Exists(originalBlobFileFullPath))
                                {
                                    //The blob file doesn't exist! Raise an error
                                    throw new BookhamExternalDataException("Attempting to store Blob data from a Filelink that doesn't exist! : " + originalBlobFileFullPath);
                                }

                                bool copiedOK = false;
                                string droppedBlobFilename;
                                string droppedBlobFileFullPath;
                                bool preserveFilename = !this.renameBlobDataFiles;

                                do
                                {
                                    //Construct the full path to where the Blob Data is to be stored in the PCAS Blob Data Streamer 
                                    //drop directory. The file will be renamed during this operation, to conform to the following format:
                                    //       YYYYMMDDHHNNSSUUU.ZZZ
                                    //
                                    // where YYYY is the four digit year
                                    //       MM is the month
                                    //       DD is the day
                                    //       HH is the hour
                                    //       NN is the minute
                                    //       SS is the second
                                    //       UUU is the milliseconds
                                    //       ZZZ is the file suffix

                                    System.DateTime now;
                                    if (usingUtcDateTime)
                                    {
                                        now = DateTime.UtcNow;
                                    }
                                    else
                                    {
                                        now = DateTime.Now;
                                    }

                                    if (preserveFilename)
                                    {
                                        droppedBlobFilename = Path.GetFileName(originalBlobFileFullPath);
                                    }
                                    else
                                    {
                                        //string tempaa = Path.GetExtension(originalBlobFileFullPath);
                                        //droppedBlobFilename = Path.GetFileNameWithoutExtension(originalBlobFileFullPath);
                                        //droppedBlobFilename = droppedBlobFilename.Replace(tempaa, now.Millisecond.ToString() + tempaa);

                                        droppedBlobFilename = now.ToString("yyyyMMddHHmmssfff") + Path.GetExtension(originalBlobFileFullPath);
                                    }
                                    droppedBlobFileFullPath = blobDataStreamerDropDirectoryForNode + @"\" + droppedBlobFilename;

                                    //if (File.Exists(droppedBlobFileFullPath))
                                    //{
                                    //    //File already exists - wait a ms so that we generate a free filename
                                    //    System.Threading.Thread.Sleep(10);
                                    //    // Need to allow renaming
                                    //    preserveFilename = false;
                                    //}
                                    //else
                                    //{
                                        //Copy blob data file it to the Streamer directory, with new filename.
                                        File.Copy(originalBlobFileFullPath, droppedBlobFileFullPath,true);
                                        copiedOK = true;
                                    //}
                                } while (!copiedOK);

                                // Write log entry
                                this.logger.DatabaseTransactionWrite(PcasDataWrite.LogLabel, "Copied BLOB drop-file to : " + droppedBlobFileFullPath);

                                // Add parameter to list of params that we want to write
                                resultsList.Add(parmRef.ExternalName, droppedBlobFilename);
                            }
                            else if (parmRef.MeasuredData.GetType() == typeof(DatumBool))
                            {
                                //Special handling for booleans required, as PCAS doesn't actually support them.
                                //Convert False to 0, and True to 1.
                                if (String.Compare("True", valAsString, true) == 0)
                                {
                                    resultsList.Add(parmRef.ExternalName, "1");
                                }
                                else if (String.Compare("False", valAsString, true) == 0)
                                {
                                    resultsList.Add(parmRef.ExternalName, "0");
                                }
                                else
                                {
                                    throw new BookhamExternalDataException("Attempting to store invalid Boolean Data to PCAS");
                                }
                            }
                            else if (parmRef.MeasuredData.GetType() == typeof(DatumString))
                            {
                                //Special handling for strings required, as PCAS doesn't support CR or LF characters
                                //Convert to "  ".
                                string output = valAsString.Replace("\n", " ");
                                output = output.Replace("\r", " ");
                                resultsList.Add(parmRef.ExternalName, output);
                            }
                            else if (parmRef.MeasuredData.GetType() == typeof(DatumDouble))
                            {
                                // Allowing for Not-a-Number and Infinity
                                string valAsStringAllowingNaNs = valAsString;

                                // Floating point types (doubles) can have special values of +/- infinity and not-a-number.
                                if ((String.Compare(valAsString, "infinity", true) == 0) || // ignore case 
                                    (String.Compare(valAsString, "NaN", true) == 0))
                                {
                                    // Use value 1E+38 as expected by Ian Gurney's PCAS code.
                                    valAsStringAllowingNaNs = PcasDataWrite.PositiveInfinityTokenString;
                                }
                                else if (String.Compare(valAsString, "-infinity", true) == 0)
                                {
                                    // Use value -1E+38 as expected by Ian Gurney's PCAS code.
                                    valAsStringAllowingNaNs = PcasDataWrite.NegativeInfinityTokenString;
                                }

                                // Add parameter to list of params that we want to write
                                resultsList.Add(parmRef.ExternalName, valAsStringAllowingNaNs);
                            }
                            else // for a DatumSint32 etc.
                            {
                                // Add parameter to list of params that we want to write
                                resultsList.Add(parmRef.ExternalName, valAsString);
                            }
						}
						else
						{
                            
                            if (autoCreatableParamList.Contains(parmRef.ExternalName.ToUpper()))
                            {
                                resultsList.Add(parmRef.ExternalName, null);
                                autoCreatingParamList.Add(parmRef.ExternalName.ToUpper());
                            }
                            else if (ignoreMissing || paramIsPcasPrimaryKey)
                            {
                                logger.DatabaseTransactionWrite(PcasDataWrite.LogLabel, "Ignoring null value for parameter : " + parmRef.ExternalName);
                            }
                            else
                            {
                                string errStr = "Missing data for parameter '" +
                                    parmRef.ExternalName + "' ( IgnoreMissing is FALSE )";
                                logger.ErrorWrite(errStr);
                                dataMissingFlag = true;
                            }
						}
					}
				} // parmRef in specData

                if (dataMissingFlag)
                {
                    //Mark this device as failed in PCAS to avoid it being shipped with incomplete data.
                    resultsList["TEST_STATUS"] = "DATA_MISSING";
                }

                PCASdropFile dropFile = new PCASdropFile(localDir, dropArea, nodeId);

                // Create local file
                dropFile.createLocal(keyData["DEVICE_TYPE"].ToString(),
                    keyData["TEST_STAGE"].ToString(),
                    keyData["SPECIFICATION"].ToString(),
                    keyData["SERIAL_NO"].ToString(),
                    resultsList,
                    true,
                    autoCreatingParamList,
                    usingUtcDateTime);

                // send it on its way
                string finalDropFilePath = dropFile.MoveToDropDir();
 
                // Write log entry for writing test results drop file
                this.logger.DatabaseTransactionWrite(PcasDataWrite.LogLabel, "Wrote Results drop-file : " + finalDropFilePath); 
                
			} // specData == null			
            // return the data missing flag
            return dataMissingFlag;
		}

		#endregion
		
		
		
		#region Private Members

		/// <summary>
		/// Location of the drop area used for writing PCAS data.
		/// </summary>
		internal string dropArea;

        /// <summary>
        /// Location of the drop directory for Blob data. Set to pcasBlobDataStreamerDrop subdirectory 
        /// from the TestEngine.Core.exe by default. This value can be changed if a 
        /// PcasWriteconfig.xml file is present, and contains the RawDataDropDirectory tag with 
        /// a new Blob Data streamer directory specified.
        /// </summary>
        private string blobDataStreamerDropDirectory = @".\pcasBlobDataStreamerDrop";

        /// <summary>
        /// It is usually a good idea to name blob data files uniquely when
        /// moving them to their final destination directory.
        /// However, there are some circumstances where use of certain data viewing 
        /// tools is made easier when the original name is preserved.
        /// </summary>
        private bool renameBlobDataFiles = true;

		/// <summary>
		/// Checks that the schema exists in PCASschema
		/// </summary>
		/// <param name="schemaName">The name of the schema to check.</param>
		/// <returns>TRUE if the schema exists.</returns>
		private static Boolean CheckSchema(string schemaName)
		{
			foreach(string s in Enum.GetNames(typeof(PCASschema)))
			{
				if ( String.Compare(s, schemaName, true, CultureInfo.InvariantCulture ) == 0 )
					return true;
			}
			throw new BookhamExternalDataException("ExternalData - Invalid schema '" + schemaName +  "'");
		}

		/// <summary>
		/// Checks for existence of keys.
		/// </summary>		
		/// <param name="keyData">Collection of key information</param>
		private static void CheckKeyData(StringDictionary keyData)
		{
			StringBuilder missingKeys = new StringBuilder();
			
			CheckSchema(keyData["Schema"]);
			
			// Each of the following keys must be present
			string[] requiredKeys = {"test_stage","device_type","schema","specification","serial_no"};
			
			foreach ( string requiredKey in requiredKeys )
			{
				if ( ! keyData.ContainsKey(requiredKey) || requiredKey.Length == 0 )
				{
					if ( missingKeys.Length > 0 )
					{
						missingKeys.Append(", ");
					}
					missingKeys.Append(requiredKey);
				}
			}

			if ( missingKeys.Length > 0 )
			{
				throw new BookhamExternalDataException("ExternalData - Missing key data for '" + missingKeys.ToString() +  "'. Set key information before use.");
			}
		}

		#endregion
		/// <summary>
		/// Reference to component providing logging functionality.
		/// </summary>
		private ILogging logger;

        /// <summary>
        /// Log application label string to "mark" log entry as originating from this domain.
        /// </summary>
        private const string LogLabel = "PcasDataWrite"; 

        /// <summary>
        /// Node Id for the Test Set, for the current Test Stage.
        /// </summary>
        private string nodeId;


        /// <summary>
        /// Constant special token string value to represent +ve Infinity and NaN.
        /// <para>
        /// The PCAS software (written by Ian Gurney) expects this value.
        /// The value depended on VB 6.0 requirements and hence fits in a Single float type.
        /// </para>
        /// </summary>
        private const String PositiveInfinityTokenString = "1E+38";

        /// <summary>
        /// Constant special token string value to represent -ve Infinity.
        /// <para>
        /// The PCAS software (written by Ian Gurney) expects this value.
        /// The value depended on VB 6.0 requirements and hence fits in a Single float type.
        /// </para>
        /// </summary>
        private const String NegativeInfinityTokenString = "-1E+38";

       
    }
}
