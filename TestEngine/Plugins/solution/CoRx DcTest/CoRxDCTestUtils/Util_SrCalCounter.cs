using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to deal with splitter ration calbration data usage counter,
    /// </summary>
    public class Util_SrCalCounter
    {
        /// <summary>
        /// Configure file path for splitter ration calbration data usage counter
        /// </summary>
        public static string SrCalCounterFilePath = "";

        
        /// <summary>
        /// Check SR config file and sr data outdate or not
        /// </summary>
        /// <param name="isCreateFile"> If create config file when it is not exist </param>
        /// <returns></returns>
        public static bool CheckSrCalCounter(bool isCreateFile)
        {
            bool isCounterOK = false;
            if (File.Exists(SrCalCounterFilePath))
            {
                try
                {
                    XmlDocument counterDoc = new XmlDocument();
                    counterDoc.Load(SrCalCounterFilePath);
                    XmlNode rootNode = counterDoc.SelectSingleNode("SplitterRatioCalCounter");
                    if (rootNode == null)
                        throw new XmlException(" the node " + "SplitterRatioCalCounter" +
                            "does not exist in file @" + SrCalCounterFilePath);
                    XmlNodeList settingNodes = rootNode.ChildNodes;

                    bool isCountByCounter = false;
                    bool bOk = bool.TryParse(settingNodes[0].InnerText, out isCountByCounter); // node "IsCountByCounter";
                    if (!bOk)
                        throw new XmlException(" the text in node @ " + settingNodes[0].Name + " can't parse to bool");
                    
                    DateTime lastCalDate = DateTime.ParseExact(settingNodes[1].InnerText, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture); // node "LastCalDate"
                    int counter = int.Parse(settingNodes[3].InnerText); // node "Counter"

                    if (isCountByCounter)  // limit be counter
                    {
                        int maxCount = int.Parse(settingNodes[2].InnerText); // node "MaxCalTerm"
                        if (counter >= maxCount)
                            isCounterOK = false;
                        else
                            isCounterOK = true;
                    }
                    else  // limite by cal interval
                    {
                        TimeSpan MaxcalSpan = TimeSpan.Parse(settingNodes[2].InnerText);

                        TimeSpan calSpan = DateTime.Now - lastCalDate;

                        if (calSpan >= MaxcalSpan)
                            isCounterOK = false;
                        else
                            isCounterOK = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
 
            }
            else if (isCreateFile)
            {
                createCounterFile();
            }
            return isCounterOK;
        }

        /// <summary>
        /// Increase test counter
        /// </summary>
        public static void UpdateSrCalCounter()
        {
           
            try
            {
                XmlDocument counterDoc = new XmlDocument();
                counterDoc.Load(SrCalCounterFilePath);
                XmlNode rootNode = counterDoc.SelectSingleNode("SplitterRatioCalCounter");
                if (rootNode == null)
                    throw new XmlException(" the node " + "SplitterRatioCalCounter" +
                        "does not exist in file @" + SrCalCounterFilePath);
                XmlNodeList settingNodes = rootNode.ChildNodes;
                bool isCountByCounter = false;
                bool bOk = bool.TryParse(settingNodes[0].InnerText, out isCountByCounter); // node "IsCountByCounter";
                if (!bOk)
                    throw new XmlException(" the text in node @ " + settingNodes[0].Name + " can't parse to bool");
                if (!isCountByCounter) return;
                int counter = int.Parse(settingNodes[3].InnerText); // node "Counter"
                settingNodes[3].InnerText = (++counter).ToString();

                counterDoc.Save(SrCalCounterFilePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update last calbrate time and reset test counter to 0
        /// </summary>
        public static void UpdateNewSrCal()
        {
            try
            {
                XmlDocument counterDoc = new XmlDocument();
                counterDoc.Load(SrCalCounterFilePath); 
                XmlNode rootNode = counterDoc.SelectSingleNode("SplitterRatioCalCounter");
                if (rootNode == null)
                    throw new XmlException(" the node " + "SplitterRatioCalCounter" +
                        "does not exist in file @" + SrCalCounterFilePath);
                XmlNodeList settingNodes = rootNode.ChildNodes;
                settingNodes[1].InnerText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); // node "LastCalDate"
                settingNodes[3].InnerText ="0"; // node "Counter"

                counterDoc.Save(SrCalCounterFilePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Create counter config file
        /// </summary>
        /// <returns></returns>
        private static bool createCounterFile()
        {
            if (!SrCalCounterFilePath.EndsWith(".xml"))
                throw new FileLoadException("Incorrected Splitter ratio cal file name, it should be xml file!");
            try
            {
                //define XDocument file                 
                XmlDocument myXDoc = new XmlDocument();
                XmlElement rootElem = myXDoc.CreateElement("SplitterRatioCalCounter"); 
                myXDoc .AppendChild(rootElem );

                XmlElement settingNode =  myXDoc.CreateElement( "IsCountByCounter");
                settingNode.InnerText = "True";
                rootElem.AppendChild(settingNode);

                settingNode = myXDoc.CreateElement("LastCalDate");
                settingNode .InnerText =  DateTime .Now.ToString("yyyy-MM-dd HH:mm:ss");
                settingNode.AppendChild(myXDoc.CreateComment("'yyyy-MM-dd HH:mm:SS'"));
                rootElem .AppendChild(settingNode );

                settingNode = myXDoc.CreateElement("MaxCalTerm");
                settingNode.InnerText = "5";
                settingNode.AppendChild(myXDoc.CreateComment("'HH:MM:SS OR yyyy-MM-dd HH:mm:SS'"));
                rootElem.AppendChild(settingNode );

                settingNode = myXDoc.CreateElement("Counter");
                settingNode.InnerText = "0";
                rootElem.AppendChild (settingNode );

                // save to xml file
                myXDoc.Save(SrCalCounterFilePath);
            }
            
            catch (Exception ex)
            {
                throw ex;
            }

            return true ;
        }
    }
}
