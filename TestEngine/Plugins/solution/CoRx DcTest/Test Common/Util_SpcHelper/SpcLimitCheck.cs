﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.Program;
using System.Collections.Specialized;

namespace Bookham.TestLibrary.Utilities
{
    public static class SpcLimitCheck
    {

        public static string dailySpcLimitFile = @"Configuration\SpcLimitFile\daily\";

        public static string weeklySpcLimitFile = @"Configuration\SpcLimitFile\weekly\";

        public static bool Check(ITestEngine engine, string node, string spcStage, string[] timeSpan = null, bool useWeekly = false)
        {

            DatumList spcUnitTestResult = ReadPcasData(engine, node, spcStage, timeSpan, useWeekly);

            if (spcUnitTestResult==null)
            {
                return false;
            }

            CsvReader cr = new CsvReader();
             List<string[]> spcLimitList=new List<string[]> ();
            int week = SpcConfigReader.ReadInt("DayOfWeekly");

            if ((int)DateTime.Now.DayOfWeek == week)
            {
                spcLimitList = cr.ReadFile(weeklySpcLimitFile + spcStage + "_Node" + node + ".csv");
            }
            else
            {
                spcLimitList = cr.ReadFile(dailySpcLimitFile + spcStage + "_Node" + node + ".csv");   //pre-lidding_spc_Node2449
            }

            bool limitCheckPass = false;
            for (int i = 1; i < spcLimitList[0].Length; i++)
            {
                string paramName = spcLimitList[0][i];
                double meanValue=Convert.ToDouble( spcLimitList[1][i]);
                double limitRange=Convert.ToDouble(spcLimitList[2][i]);
                if (spcUnitTestResult.IsPresent(paramName))
                {
                   double testResult= spcUnitTestResult.ReadDouble(paramName);
                   if (Math.Abs(testResult - meanValue) < limitRange)
                   {
                       limitCheckPass = true;
                   }
                   else
                   {
                       engine.ShowContinueUserQuery(TestControlTab.TestControl, "标准件SPC监控异常，联系技术员！");
                       limitCheckPass = false;
                       break;
                   }
                }
                else
                {
                    return false;
                }
            }

            return limitCheckPass;
        }

        private static DatumList ReadPcasData(ITestEngine engine, string node, string spcStage, string[] timeSpan = null, bool useWeekly = false)
        {
            IDataRead read = engine.GetDataReader();

            StringDictionary mapKeys = new StringDictionary();
            string PcasSchema = SpcConfigReader.ReadString("Schema");
            string SerialNumber = string.Empty;
            string PartCode = SpcConfigReader.ReadString("DeviceType");

            int week = SpcConfigReader.ReadInt("DayOfWeekly");

            if ((int)DateTime.Now.DayOfWeek == week)
                SerialNumber = SpcConfigReader.ReadString("WeeklySerialNo");
            else
                SerialNumber = SpcConfigReader.ReadString("DailySerialNo");

            mapKeys.Add("SCHEMA", PcasSchema);
            mapKeys.Add("SERIAL_NO", SerialNumber);
            mapKeys.Add("TEST_STAGE", spcStage);

            DatumList list = null;
            DatumList[] lists = null;
            if (timeSpan == null)
            {
                lists = read.GetResults(mapKeys, false, string.Format("node={0} and TIME_DATE >= {1}{2} order by TIME_DATE desc",
                            node, DateTime.Now.AddDays(-1).ToString("yyyyMMdd"), "235960"));
                if (lists != null && lists.Length > 0)
                {
                    list = lists[0];
                }
            }
            else
                list = read.GetLatestResults(mapKeys, false, string.Format("node={0} and TIME_DATE BETWEEN {1} and {2}", node, timeSpan[0], timeSpan[1]));

            return list;
        }

    }
}
