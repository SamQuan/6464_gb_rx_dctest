// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// Instr_ITLA.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for the ITLA.
    /// </summary>
    public class Instr_ITLA : Instrument,
                                BlackBoxes.IMSA_ITLA,
                                BlackBoxes.IDsdbrLaserSetup,
                      BlackBoxes.IBlackBoxIdentity
    {
        #region Private data
        /// <summary>
        /// Reference to chassis object, wrappimng JC's Ixolite VB code.
        /// </summary>
        public Chassis_IxoliteWrapper ixoliteChassis;
        #endregion

        /// <summary>
        /// Construct ITLA instrument driver.
        /// </summary>
        /// <param name="instrumentName">Name of instrument.</param>
        /// <param name="driverName">Name of driver.</param>
        /// <param name="slotId">Slot id. (Ignored)</param>
        /// <param name="subSlotId">Sub slot id. (Ignored)</param>
        /// <param name="chassis">Reference to Ixolite wrapper chassis.</param>
        public Instr_ITLA(
            string instrumentName,
            string driverName,
            string slotId,
            string subSlotId,
            Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            /* Add hardware record. */
            ValidHardwareData.Add(
                "ITLA",
                new InstrumentDataRecord("Unknown", "Unknown", "Unknown"));

            /* Add chassis record. */
            ValidChassisDrivers.Add(
                "Chassis_IxoliteWrapper",
                new InstrumentDataRecord("Chassis_IxoliteWrapper", "0.0.0.0", "2.0.0.0"));

            /* Save chassis object reference. */
            ixoliteChassis = (Chassis_IxoliteWrapper)base.InstrumentChassis;
        }


        /// <summary>
        /// Construct ITLA Instrument. This Constructor should be used 
        /// when this class is being used as a base class for another instrument e.g. 
        /// TTA.
        /// </summary>
        /// <param name="instrumentName">Name of instrument.</param>
        /// <param name="driverName">Name of driver.</param>
        /// <param name="slotId">Slot id. (Ignored)</param>
        /// <param name="subSlotId">Sub slot id. (Ignored)</param>
        /// <param name="chassis">Reference to Ixolite wrapper chassis.</param>
        /// <param name="baseForOtherInstrument">True if being used as base class for another instrument.</param>
        public Instr_ITLA(
            string instrumentName,
            string driverName,
            string slotId,
            string subSlotId,
            Chassis chassis,
            bool baseForOtherInstrument
            )
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            if (!baseForOtherInstrument)
            {
                /* Add hardware record. */
                ValidHardwareData.Add(
                    "ITLA",
                    new InstrumentDataRecord("Unknown", "Unknown", "Unknown"));
            }

            /* Add chassis record. */
            ValidChassisDrivers.Add(
                "Chassis_IxoliteWrapper",
                new InstrumentDataRecord("Chassis_IxoliteWrapper", "0.0.0.0", "2.0.0.0"));

            /* Save chassis object reference. */
            ixoliteChassis = (Chassis_IxoliteWrapper)base.InstrumentChassis;
        }

        /// <summary>
        /// Gets the version of the ixolite driver from the chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Gest the hardware serial number.
        /// </summary>
        public override string HardwareIdentity
        {
            get { return "Unknown"; }
        }

        /// <summary>
        /// Set Default State (Does nothing.)
        /// </summary>
        public override void SetDefaultState()
        {
            /* Left empty for future expansion. Required by base class abstract. */
        }


        #region IBlackBoxIdentity Members

        public string GetUnitSerialNumber()
        {
            return this.GetSerialNumber();
        }

        public void SetUnitSerialNumber(string serno)
        {
            this.ixoliteChassis.SetUnitSerialNumber_OIF(serno);
        }

        public DateTime GetUnitBuildDate()
        {
            /* Get date from ITLA. */
            string bdate = this.GetManufacturingDate();

            /* Check length of string. */
            if (bdate.Length != 11)
            {
                throw new TunableModuleException(
                    "GetBuildDate returned " + bdate.Length + " characters. (Expected 11.)",
                    TunableProtocol.OIF);
            }

            /* Extract values. */
            string dateStr = bdate.Substring(0, 2);
            string monStr = bdate.Substring(3, 3);
            string yearStr = bdate.Substring(7, 4);

            int dateNum, monNum, yearNum;

            try
            {
                /* Parse numeric parts. */
                dateNum = int.Parse(dateStr);
                yearNum = int.Parse(yearStr);
            }
            catch (ArgumentException)
            {
                throw new TunableModuleException(
                    "GetBuildDate failed. A numeric part of \"" + bdate + "\" did not parse correctly.",
                    TunableProtocol.OIF);
            }

            /* Find month number. */
            switch (monStr)
            {
                case "JAN": monNum = 1; break;
                case "FEB": monNum = 2; break;
                case "MAR": monNum = 3; break;
                case "APR": monNum = 4; break;
                case "MAY": monNum = 5; break;
                case "JUN": monNum = 6; break;
                case "JUL": monNum = 7; break;
                case "AUG": monNum = 8; break;
                case "SEP": monNum = 9; break;
                case "OCT": monNum = 10; break;
                case "NOV": monNum = 11; break;
                case "DEC": monNum = 12; break;
                default:
                    {
                        throw new TunableModuleException(
                            "GetBuildDate returned bad month. \"" + monStr + "\".",
                            TunableProtocol.OIF);
                    }
            }

            try
            {
                /* Generate DateTime as noon on stated day. */
                return new DateTime(yearNum, monNum, dateNum, 12, 0, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new TunableModuleException(
                    "GetBuildDate failed. Bad date. \"" + bdate + "\".",
                    TunableProtocol.OIF);
            }
        }

        public void SetUnitBuildDate(DateTime bdate)
        {
            this.ixoliteChassis.SetUnitBuildDate_OIF(bdate.ToString("dd-MMM-yyyy").ToUpper());
        }

        public string GetLaserSerialNumber()
        {
            return this.ixoliteChassis.GetLaserSerialNumber_OIF();
        }

        public void SetLaserSerialNumber(string serno)
        {
            this.ixoliteChassis.SetLaserSerialNumber_OIF(serno);
        }

        public string GetBoardSerialNumber()
        {
            return this.ixoliteChassis.GetBoardSerialNumber_OIF();
        }

        public void SetBoardSerialNumber(string serno)
        {
            this.ixoliteChassis.SetBoardSerialNumber_OIF(serno);
        }

        public string GetPartNumber()
        {
            return this.ixoliteChassis.GetModelNumber_OIF();
        }

        public void SetPartNumber(string partno)
        {
            this.ixoliteChassis.SetModuleNumber_OIF(partno);
        }

        public string GetSupplier()
        {
            return this.ixoliteChassis.GetManufacturer_OIF();
        }

        /// <summary>
        /// Set the Laser Technology String. Not Supported by ITLA.
        /// </summary>
        /// <param name="mzTechnology"> Laser technology string </param>
        public virtual void SetLaserTechnology(string mzTechnology)
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Get the laser Technology String. Not Supported by ITLA.
        /// </summary>
        /// <returns>The Laser Technology String</returns>
        public virtual string GetLaserTechnology()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }


        #endregion

        #region IMSA_ITLA Members

        public int GetNOP()
        {
            return this.ixoliteChassis.GetNOP();
        }

        public string GetDeviceType()
        {
            return this.ixoliteChassis.GetDeviceType();
        }

        public string GetManufacturer()
        {
            return this.ixoliteChassis.GetManufacturer_OIF();
        }

        public string GetModelNumber()
        {
            return this.ixoliteChassis.GetModelNumber_OIF();
        }

        public string GetSerialNumber()
        {
            return this.ixoliteChassis.GetSerialNumber_OIF();
        }

        public string GetManufacturingDate()
        {
            return this.ixoliteChassis.GetManufacturingDate_OIF();
        }

        public string GetReleaseCode()
        {
            return this.ixoliteChassis.GetReleaseCode();
        }

        public string GetReleaseBackwards()
        {
            return this.ixoliteChassis.GetReleaseBackwards();
        }

        public void SaveConfig()
        {
            this.ixoliteChassis.SaveConfig();
        }

        public int GetIOCap()
        {
            return this.ixoliteChassis.GetIOCap();
        }

        public void SetIOCap(int value)
        {

            int a = GetIOCap();
            this.ixoliteChassis.SetIOCap(value);
            if (a != value)
                switch (value)
                {
                    case 0x04:

                        this.ixoliteChassis.SerialPort.Close();
                        this.ixoliteChassis.SerialPort.BaudRate = 9600;
                        this.ixoliteChassis.SerialPort.Open();
                        break;
                    case 0x44:
                        this.ixoliteChassis.SerialPort.Close();
                        this.ixoliteChassis.SerialPort.BaudRate = 115200;
                        this.ixoliteChassis.SerialPort.Open();
                        break;
                }

        }

        public OifStatus GetStatusF()
        {
            return this.ixoliteChassis.GetStatusF();
        }

        public void ClearStatusF()
        {
            this.ixoliteChassis.ClearStatusF();
        }

        public OifStatus GetStatusW()
        {
            return this.ixoliteChassis.GetStatusW();
        }

        public void ClearStatusW()
        {
            this.ixoliteChassis.ClearStatusW();
        }

        public double GetFPowTh_dB()
        {
            return this.ixoliteChassis.GetFPowTh();
        }

        public void SetFPowTh_dB(double thresh)
        {
            this.ixoliteChassis.SetFPowTh(thresh);
        }

        public double GetWPowTh_dB()
        {
            return this.ixoliteChassis.GetWPowTh();
        }

        public void SetWPowTh_dB(double th)
        {
            this.ixoliteChassis.SetWPowTh(th);
        }

        public double GetFFreqTh_GHz()
        {
            return this.ixoliteChassis.GetFFreqTh();
        }

        public void SetFFreqTh_GHz(double th)
        {
            this.ixoliteChassis.SetFFreqTh(th);
        }

        public double GetWFreqTh_GHz()
        {
            return this.ixoliteChassis.GetWFreqTh();
        }

        public void SetWFreqTh_GHz(double th)
        {
            this.ixoliteChassis.SetWFreqTh(th);
        }

        public double GetFThermTh_degC()
        {
            return this.ixoliteChassis.GetFThermTh();
        }

        public void SetFThermTh_degC(double th)
        {
            this.ixoliteChassis.SetFThermTh(th);
        }

        public double GetWThermTh_degC()
        {
            return this.ixoliteChassis.GetWThermTh();
        }

        public void SetWThermTh_degC(double th)
        {
            this.ixoliteChassis.SetWThermTh(th);
        }

        public OifStatus GetSRQT()
        {
            return this.ixoliteChassis.GetSRQT();
        }

        public void SetSRQT(OifStatus mask)
        {
            this.ixoliteChassis.SetSRQT(mask);
        }

        public OifStatus GetFATALT()
        {
            return this.ixoliteChassis.GetFATALT();
        }

        public void SetFATALT(OifStatus mask)
        {
            this.ixoliteChassis.SetFATALT(mask);
        }

        public OifStatus GetALMT()
        {
            return this.ixoliteChassis.GetALMT();
        }

        public void SetALMT(OifStatus mask)
        {
            this.ixoliteChassis.SetALMT(mask);
        }

        public void SelectChannel(int chan)
        {
            this.ixoliteChassis.SelectChannel(checked((short)chan));
        }

        public int GetChannel()
        {
            return this.ixoliteChassis.GetChannel();
        }

        public void SetPowerSetpoint_dBm(double pwr)
        {
            this.ixoliteChassis.SetPowerSetpoint(pwr);
        }

        public double GetPowerSetpoint_dBm()
        {
            return this.ixoliteChassis.GetPowerSetpoint();
        }

        public void SoftwareEnableOutput(bool sena)
        {
            this.ixoliteChassis.SoftwareEnableOutput(sena);
        }

        public void ModuleReset()
        {
            this.ixoliteChassis.ModuleReset();
        }

        public void SoftReset()
        {
            this.ixoliteChassis.SoftReset();
        }

        public bool GetSoftwareEnableOutput()
        {
            return this.ixoliteChassis.GetSoftwareEnableOutput();
        }

        public bool GetADT()
        {
            return this.ixoliteChassis.GetADT();
        }

        public void SetADT(bool adt)
        {
            this.ixoliteChassis.SetADT(adt);
        }

        public bool GetSDF()
        {
            return this.ixoliteChassis.GetSDF();
        }

        public void SetSDF(bool Sdf)
        {
            this.ixoliteChassis.SetSDF(Sdf);
        }

        public void SetFrequencyGrid_GHz(double GHz)
        {
            this.ixoliteChassis.SetFrequencyGrid(checked((short)GHz));
        }

        public double GetFrequencyGrid_GHz()
        {
            return this.ixoliteChassis.GetFrequencyGrid();
        }

        public void SetFirstChanFreq_GHz(double freq)
        {
            this.ixoliteChassis.SetFirstChanFreq_OIF(freq);
        }

        public double GetFirstChanFreq_GHz()
        {
            return this.ixoliteChassis.GetFirstChanFreq_OIF();
        }

        public double GetLaserFrequency_GHz()
        {
            return this.ixoliteChassis.GetLaserFrequency();
        }

        public double GetLaserOutputPower_dBm()
        {
            return this.ixoliteChassis.GetLaserOutputPower();
        }

        public double GetCurrentTemperature_degC()
        {
            return this.ixoliteChassis.GetCurrentTemperature();
        }

        public double GetOpticalPowerMin_dBm()
        {
            return this.ixoliteChassis.GetOpticalPowerMin();
        }

        public double GetOpticalPowerMax_dBm()
        {
            return this.ixoliteChassis.GetOpticalPowerMax();
        }

        public double GetLaserFirstFreq_GHz()
        {
            return this.ixoliteChassis.GetLaserFirstFreq();
        }

        public void SetLaserFirstFreq_GHz(double freq_GHz)
        {
            this.ixoliteChassis.SetLaserFirstFreq(freq_GHz);
        }

        public double GetLaserLastFreq_GHz()
        {
            return this.ixoliteChassis.GetLaserLastFreq();
        }

        public void SetLaserLastFreq_GHz(double freq_GHz)
        {
            this.ixoliteChassis.SetLaserLastFreq(freq_GHz);
        }

        public void SetMinFrequencyGrid_GHz(double GHz)
        {
            this.ixoliteChassis.SetMinFrequencyGrid(GHz);
        }

        public double GetMinFrequencyGrid_GHz()
        {
            return this.ixoliteChassis.GetMinFrequencyGrid();
        }

        public DitherState GetDitherEnable()
        {
            return this.ixoliteChassis.GetDitherEnable();
        }

        public void SetDitherEnable(DitherState state)
        {
            this.ixoliteChassis.SetDitherEnable(state);
        }

        public double GetDitherR_KHz()
        {
            return this.ixoliteChassis.GetDitherR();
        }

        public void SetDitherR_KHz(double KHz)
        {
            this.ixoliteChassis.SetDitherR(checked((short)KHz));
        }

        public double GetDitherF_GHz()
        {
            return this.ixoliteChassis.GetDitherF();
        }

        public void SetDitherF_GHz(double GHz)
        {
            this.ixoliteChassis.SetDitherF(GHz);
        }

        public double GetDitherA_percent()
        {
            return this.ixoliteChassis.GetDitherA();
        }

        public void SetDitherA_percent(double percent)
        {
            this.ixoliteChassis.SetDitherA(percent);
        }

        public double GetTCaseL_degC()
        {
            return this.ixoliteChassis.GetTCaseL();
        }

        public void SetTCaseL_degC(double degC)
        {
            this.ixoliteChassis.SetTCaseL(checked((short)degC));
        }

        public double GetTCaseH_degC()
        {
            return this.ixoliteChassis.GetTCaseH();
        }

        public void SetTCaseH_degC(double degC)
        {
            this.ixoliteChassis.SetTCaseH(degC);
        }

        public int GetFAgeTh()
        {
            return this.ixoliteChassis.GetFAgeTh();
        }

        public void SetFAgeTh(int percent)
        {
            this.ixoliteChassis.SetFAgeTh(checked((short)percent));
        }

        public int GetWAgeTh()
        {
            return this.ixoliteChassis.GetWAgeTh();
        }

        public void SetWAgeTh(int percent)
        {
            this.ixoliteChassis.SetWAgeTh(checked((short)percent));
        }

        #endregion

        #region IDsdbrLaserSetup Members

        public void EnterLaserSetupMode()
        {
            this.UnlockModule();
        }

        public int GetSOADAC()
        {
            return this.ixoliteChassis.GetSOADAC();
        }

        public void SetSOADAC(int dac)
        {
            this.ixoliteChassis.SetSOADAC(dac);
        }

        public int GetGainDAC()
        {
            return this.ixoliteChassis.GetGainDAC();
        }

        public void SetGainDAC(int dac)
        {
            this.ixoliteChassis.SetGainDAC(dac);
        }

        public int GetFrontSectionPair()
        {
            return this.ixoliteChassis.GetControlRegFrontSection();
        }

        public void SetFrontSectionPair(int i)
        {
            this.ixoliteChassis.SetControlRegFrontSection(i);
        }

        public int GetFrontEvenDAC()
        {
            return this.ixoliteChassis.GetFrontEvenDAC();
        }

        public void SetFrontEvenDAC(int dac)
        {
            this.ixoliteChassis.SetFrontEvenDAC(dac);
        }

        public int GetFrontOddDAC()
        {
            return this.ixoliteChassis.GetFrontOddDAC();
        }

        public void SetFrontOddDAC(int dac)
        {
            this.ixoliteChassis.SetFrontOddDAC(dac);
        }

        public int GetRearDAC()
        {
            return this.ixoliteChassis.GetRearDAC();
        }

        public void SetRearDAC(int dac)
        {
            this.ixoliteChassis.SetRearDAC(dac);
        }

        public int GetPhaseDAC()
        {
            return this.ixoliteChassis.GetPhaseDAC();
        }

        public void SetPhaseDAC(int dac)
        {
            this.ixoliteChassis.SetPhaseDAC(dac);
        }

        public int GetModeCentrePhaseDAC()
        {
            return this.ixoliteChassis.GetModeCentrePhaseDAC_OifBkhm();
        }

        public void SetModeCentrePhaseDAC(int dac)
        {
            this.ixoliteChassis.SetModeCentrePhaseDAC_OifBkhm(dac);
        }

        public int GetBOLPhaseDAC()
        {
            return this.ixoliteChassis.GetBOLPhaseDAC_OifBkhm();
        }

        public void SetBOLPhaseDAC(int dac)
        {
            this.ixoliteChassis.SetBOLPhaseDAC_OifBkhm(dac);
        }

        public int GetDitherDAC()
        {
            return this.ixoliteChassis.GetDitherDAC();
        }

        public void SetDitherDAC(int dac)
        {
            this.ixoliteChassis.SetDitherDAC(checked((short)dac));
        }

        public void SaveChannel(int chan)
        {
            this.ixoliteChassis.SaveChannel(checked((short)chan));
        }

        public void DoAutoLockerSetup()
        {
            this.ixoliteChassis.SetControlRegAutoLockStart();
        }

        public bool GetLockerSlopePositive()
        {
            return this.ixoliteChassis.GetControlRegLockerSlopeOn();
        }

        public void SetLockerSlopePositive(bool on)
        {
            this.ixoliteChassis.SetControlRegLockerSlopeOn(on);
        }

        public int GetTxCoarsePot()
        {
            return this.ixoliteChassis.GetTxCoarsePot();
        }

        public void SetTxCoarsePot(int pot)
        {
            this.ixoliteChassis.SetTxCoarsePot(checked((short)pot));
        }

        public int GetRefCoarsePot()
        {
            return this.ixoliteChassis.GetRefCoarsePot();
        }

        public void SetRefCoarsePot(int rcp)
        {
            this.ixoliteChassis.SetRefCoarsePot(checked((short)rcp));
        }

        public int GetLockerErrorFactor()
        {
            return this.ixoliteChassis.GetLockerErrorFactor();
        }

        public void SetLockerErrorFactor(int lef)
        {
            this.ixoliteChassis.SetLockerErrorFactor(lef);
        }

        public bool GetLockerOn()
        {
            return this.ixoliteChassis.GetControlRegLockerOn();
        }

        public void SetLockerOn(bool on)
        {
            this.ixoliteChassis.SetControlRegLockerOn(on);
        }

        public int GetLockerTxMon_ADC()
        {
            return this.ixoliteChassis.GetLockerTxMon();
        }

        public int GetLockerRefMon_ADC()
        {
            return this.ixoliteChassis.GetLockerRefMon();
        }

        public int GetLaserPowerMon_ADC(int n)
        {
            return this.ixoliteChassis.GetLaserPowerMon(checked((short)n));
        }

        public int GetLsPhaseSense()
        {
            return this.ixoliteChassis.GetLsPhaseSense();
        }

        public int GetEOLPhaseADC_Min()
        {
            return this.ixoliteChassis.GetPhaseMin();
        }

        public void SetEOLPhaseADC_Min(int pmin)
        {
            this.ixoliteChassis.SetPhaseMin(pmin);
        }

        public int GetEOLPhaseADC_Max()
        {
            return this.ixoliteChassis.GetPhaseMax();
        }

        public void SetEOLPhaseADC_Max(int pmax)
        {
            this.ixoliteChassis.SetPhaseMax(pmax);
        }

        public int GetPowerMonFactor()
        {
            return this.ixoliteChassis.GetPowerMonFactor();
        }

        public void SetPowerMonFactor(int pmf)
        {
            this.ixoliteChassis.SetPowerMonFactor(pmf);
        }

        public int GetPowerMonPot()
        {
            return this.ixoliteChassis.GetPowerMonPot();
        }

        public void SetPowerMonPot(int pot)
        {
            this.ixoliteChassis.SetPowerMonPot(checked((short)pot));
        }

        public int GetPowerMonOffset()
        {
            return this.ixoliteChassis.GetPowerMonOffset();
        }

        public void SetPowerMonOffset(int pmo)
        {
            this.ixoliteChassis.SetPowerMonOffset(pmo);
        }

        public double GetLaserPowerEstimate_uW()
        {
            return this.ixoliteChassis.GetLaserPowerEstimate();
        }

        public int GetSBSDitherMultiplier()
        {
            return this.ixoliteChassis.GetSBSDitherMultiplier();
        }

        public void SetSBSDitherMultiplier(int mult)
        {
            this.ixoliteChassis.SetSBSDitherMultiplier(checked((byte)mult));
        }

        public bool GetSBSDitherOn()
        {
            return this.ixoliteChassis.GetControlRegDitherOn();
        }

        public void SetSBSDitherOn(bool on)
        {
            this.ixoliteChassis.SetControlRegDitherOn(on);
        }

        public bool GetPowerProfilingOn()
        {
            return this.ixoliteChassis.GetControlRegProfilingOn();
        }

        public void SetPowerProfilingOn(bool on)
        {
            this.ixoliteChassis.SetControlRegProfilingOn(on);
        }

        public bool GetPowerControlLoopOn()
        {
            return this.ixoliteChassis.GetPowerControlStateOn();
        }

        public void SetPowerControlLoopOn(bool enabled)
        {
            this.ixoliteChassis.SetPowerControlStateOn(enabled);
        }
        #endregion

        #region ITLA only
        public double GetLaserTecCurrent_mA()
        {
            this.ixoliteChassis.GetOifRegisterAsUint(0x57);
            //System.Threading.Thread.Sleep(10);
            short tempReg = this.ixoliteChassis.GetOifRegisterAsInt(0x0B);
            return (double)tempReg / 10.0;
        }
        public int GetLaserAge()
        {
            return this.ixoliteChassis.GetOifRegisterAsUint(0x61);
        }
        public double GetPCBTemp_degC()
        {
            return this.ixoliteChassis.GetPCBTemp();
        }

        public int GetWaveCode()
        {
            return checked((int)this.ixoliteChassis.GetWaveCode());
        }

        public void SetWaveCode(int wc)
        {
            this.ixoliteChassis.SetWaveCode(checked((short)wc));
        }

        public void SetChannelNumber(int chan)
        {
            this.ixoliteChassis.SaveChannel(checked((short)chan));
        }

        public void SelectInternalChannel(int chan)
        {
            this.ixoliteChassis.SelectChannel_OIFVen(checked((short)chan));
        }

        public int GetCurrentSrcNumber()
        {
            return this.ixoliteChassis.GetSrcNumber(SrcMode.Current);
        }

        public void SetCurrentSrcNumber(int pot)
        {
            this.ixoliteChassis.SetSrcNumber(SrcMode.Current, checked((short)pot));
        }

        public int GetCurrentSrcIndex()
        {
            return this.ixoliteChassis.GetSrcIndex(SrcMode.Current);
        }

        public void SetCurrentSrcIndex(int pot)
        {
            this.ixoliteChassis.SetSrcIndex(SrcMode.Current, checked((short)pot));
        }

        public double GetCurrentSrcValue()
        {
            return this.ixoliteChassis.GetSrcValue(SrcMode.Current);
        }

        public void SetCurrentSrcValue(double value)
        {
            this.ixoliteChassis.SetSrcValue(SrcMode.Current, checked((float)value));
        }

        public int GetBaseDac()
        {
            return this.ixoliteChassis.GetBaseDac();
        }

        public void SetBaseDac(int dac)
        {
            this.ixoliteChassis.SetBaseDac(dac);
        }

        public int GetBaseDac2()
        {
            return this.ixoliteChassis.GetBaseDac2();
        }

        public void SetBaseDac2(int dac)
        {
            this.ixoliteChassis.SetBaseDac2(dac);
        }

        public double GetPlateauPower()
        {
            return this.ixoliteChassis.GetPlateauPower();
        }

        public void SetPlateauPower(double pwr)
        {
            this.ixoliteChassis.SetPlateauPower(pwr);
        }

        public double GetSinkSlope()
        {
            return this.ixoliteChassis.GetSinkSlope();
        }

        public void SetSinkSlope(double slp)
        {
            this.ixoliteChassis.SetSinkSlope(slp);
        }

        public double GetSourceSlope()
        {
            return this.ixoliteChassis.GetSourceSlope();
        }

        public void SetSourceSlope(double slp)
        {
            this.ixoliteChassis.SetSourceSlope(slp);
        }

        public bool GetControlRegLaserTecOn()
        {
            return this.ixoliteChassis.GetControlRegLaserTecOn();
        }

        public void SetControlRegLaserTecOn(bool on)
        {
            this.ixoliteChassis.SetControlRegLaserTecOn(on);
        }

        public double GetOffGridTuning()
        {
            return this.ixoliteChassis.GetOffGridTuning();
        }

        public void SetOffGridTuning(double offGrid)
        {
            this.ixoliteChassis.SetOffGridTuning(offGrid);
        }

        public void UnlockModule()
        {
            this.ixoliteChassis.UnlockModule();
        }

        public int GetOffGridPositive()
        {
            return this.ixoliteChassis.GetOffGridPositive();
        }

        public void SetOffGridPositive(int pot)
        {
            this.ixoliteChassis.SetOffGridPositive(checked((short)pot));
        }

        public int GetOffGridNegative()
        {
            return this.ixoliteChassis.GetOffGridNegative();
        }

        public void SetOffGridNegative(int pot)
        {
            this.ixoliteChassis.SetOffGridNegative(checked((short)pot));
        }

        public int GetChannelCentre()
        {
            return this.ixoliteChassis.GetChannelCentre();
        }

        public void SetChannelCentre(int pot)
        {
            this.ixoliteChassis.SetChannelCentre(checked((short)pot));
        }

        public int GetOpenLoopCalibrationPointTableSize()
        {
            return this.ixoliteChassis.GetOifRegisterAsUint(0xDE);
        }

        public void SetOpenLoopCalibrationPointIndex(int index)
        {
            this.ixoliteChassis.SetOifRegisterAsUint(0xDE, index);
        }

        public int GetOpenLoopCalibrationPoint_uW()
        {
            return this.ixoliteChassis.GetOifRegisterAsUint(0xDF);
        }

        public void SetOpenLoopCalibrationPoint_uW(int uW)
        {
            this.ixoliteChassis.SetOifRegisterAsUint(0xDF, uW);
        }


        public double GetLowPowerEtalonSlope()
        {
            return this.ixoliteChassis.GetLowPowerEtalonSlope();
        }

        public void SetLowPowerEtalonSlope(double lef)
        {
            this.ixoliteChassis.SetLowPowerEtalonSlope(lef);
        }

        #endregion


        #region IBlackBoxIdentity Members


        public string GetRxSerialNumber()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetRxSerialNumber(string serno)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDsdbrLaserSetup Members

        /// <summary>
        /// 
        /// </summary>
        public int SettlingTime_ms
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion
    }
}
