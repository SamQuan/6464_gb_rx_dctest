// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TM_CoRxPinCheck.cs
//
// Author: alice.huang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class TM_CoRxPinCheck : ITestModule
    {
        #region ITestModule Members

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            // Get Pin Bias leval from config data
            double Ipd_A = configData.ReadDouble("PdCheckCurrent_A");
            double Vpd_Compl_V = configData.ReadDouble("PdVoltageCompliance_V");
            double VauxPd_Compl_V = configData.ReadDouble("AuxPdVoltageCompliance_V");
            double Vpd_Max_V = configData.ReadDouble("MaxPdVolt_V");
            double Vpd_Min_V = configData.ReadDouble("MinPdVolt_V");
            double VauxPd_Max_V = configData.ReadDouble("MaxAuxPdVolt_V");
            double VauxPd_Min_V = configData.ReadDouble("MinAuxPdVolt_V");
 
            // switch source connected to aux pd
            CoRxDCTestInstruments.SetSource2AuxPD(true);

            CoRxDCTestInstruments.PdSource_AUX_XR.InitSourceIMeasureV_MeasurementAccuracy(
                VauxPd_Compl_V, VauxPd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_AUX_XR.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_AUX_XL.InitSourceIMeasureV_MeasurementAccuracy(
                VauxPd_Compl_V, VauxPd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_AUX_XL.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_AUX_YR.InitSourceIMeasureV_MeasurementAccuracy(
                VauxPd_Compl_V, VauxPd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_AUX_YR.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_AUX_YL.InitSourceIMeasureV_MeasurementAccuracy(
                VauxPd_Compl_V, VauxPd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_AUX_YL.CurrentRange_Amp = Ipd_A;
            
            CoRxDCTestInstruments.PdSource_AUX_XR .CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_AUX_XR.OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_AUX_XL.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_AUX_XL.OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_AUX_YR.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_AUX_YR.OutputEnabled = true;
            CoRxDCTestInstruments.PdSource_AUX_YL.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_AUX_YL .OutputEnabled = true ;

            double VauxPd_Ln_V = CoRxDCTestInstruments .PdSource_AUX_XR.VoltageActual_Volt;
            double VauxPd_Lp_V = CoRxDCTestInstruments .PdSource_AUX_XL.VoltageActual_Volt ;
            double VauxPd_Rn_V = CoRxDCTestInstruments .PdSource_AUX_YR.VoltageActual_Volt ;
            double VauxPd_Rp_V = CoRxDCTestInstruments .PdSource_AUX_YL.VoltageActual_Volt ;

            // Switch source connected to PD
            CoRxDCTestInstruments.SetSource2AuxPD(false);

            CoRxDCTestInstruments.PdSource_XI1.InitSourceIMeasureV_MeasurementAccuracy(
                Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_XI1.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_XI2.InitSourceIMeasureV_MeasurementAccuracy(
                Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_XI2.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_XQ1.InitSourceIMeasureV_MeasurementAccuracy(
            Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_XQ1 .CurrentRange_Amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_XQ2. InitSourceIMeasureV_MeasurementAccuracy(
                Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_XQ2.CurrentRange_Amp = Ipd_A;

            CoRxDCTestInstruments.PdSource_YI1.InitSourceIMeasureV_MeasurementAccuracy(
                Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_YI1.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_YI2.InitSourceIMeasureV_MeasurementAccuracy(
                Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_YI2.CurrentRange_Amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_YQ1.InitSourceIMeasureV_MeasurementAccuracy(
            Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_YQ1 .CurrentRange_Amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_YQ2. InitSourceIMeasureV_MeasurementAccuracy(
                Vpd_Compl_V, Vpd_Compl_V, true, 0, 3, 5, true);
            CoRxDCTestInstruments.PdSource_YQ2.CurrentRange_Amp = Ipd_A;

            CoRxDCTestInstruments.PdSource_XI1.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_XI1.OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_XI2.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_XI2.OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_XQ1.CurrentSetPoint_amp = Ipd_A;
            CoRxDCTestInstruments.PdSource_XQ1 .OutputEnabled = true;
            CoRxDCTestInstruments.PdSource_XQ2.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_XQ2 .OutputEnabled = true ;

            CoRxDCTestInstruments.PdSource_YI1.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_YI1.OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_YI2.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_YI2.OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_YQ1.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_YQ1 .OutputEnabled = true ;
            CoRxDCTestInstruments.PdSource_YQ2.CurrentSetPoint_amp = Ipd_A ;
            CoRxDCTestInstruments.PdSource_YQ2 .OutputEnabled = true ;

            double Vpd_Xi1_V = CoRxDCTestInstruments.PdSource_XI1.VoltageActual_Volt;
            double Vpd_Xi2_V = CoRxDCTestInstruments.PdSource_XI2.VoltageActual_Volt;
            double Vpd_Xq1_V = CoRxDCTestInstruments.PdSource_XQ1.VoltageActual_Volt;
            double Vpd_Xq2_V = CoRxDCTestInstruments.PdSource_XQ2.VoltageActual_Volt;

            double Vpd_Yi1_V = CoRxDCTestInstruments.PdSource_YI1.VoltageActual_Volt;
            double Vpd_Yi2_V = CoRxDCTestInstruments.PdSource_YI2.VoltageActual_Volt;
            double Vpd_Yq1_V = CoRxDCTestInstruments.PdSource_YQ1.VoltageActual_Volt;
            double Vpd_Yq2_V = CoRxDCTestInstruments.PdSource_YQ2.VoltageActual_Volt;

            bool isPinOK = false;

            if (VauxPd_Ln_V >= VauxPd_Min_V && VauxPd_Ln_V <= VauxPd_Max_V &&
                VauxPd_Lp_V >= VauxPd_Min_V && VauxPd_Lp_V <= VauxPd_Max_V &&
                VauxPd_Rn_V >= VauxPd_Min_V && VauxPd_Rn_V <= VauxPd_Max_V &&
                VauxPd_Rp_V >= VauxPd_Min_V && VauxPd_Rp_V <= VauxPd_Max_V &&
                Vpd_Xi1_V >= Vpd_Min_V && Vpd_Xi1_V <= Vpd_Max_V &&
                Vpd_Xi2_V >= Vpd_Min_V && Vpd_Xi2_V <= Vpd_Max_V &&
                Vpd_Xq1_V >= Vpd_Min_V && Vpd_Xq1_V <= Vpd_Max_V &&
                Vpd_Xq2_V >= Vpd_Min_V && Vpd_Xq2_V <= Vpd_Max_V &&
                Vpd_Yi1_V >= Vpd_Min_V && Vpd_Yi1_V <= Vpd_Max_V &&
                Vpd_Yi2_V >= Vpd_Min_V && Vpd_Yi2_V <= Vpd_Max_V &&
                Vpd_Yq1_V >= Vpd_Min_V && Vpd_Yq1_V <= Vpd_Max_V &&
                Vpd_Yq2_V >= Vpd_Min_V && Vpd_Yq2_V <= Vpd_Max_V)
                isPinOK = true;

            // return data
            DatumList modData = new DatumList();
            modData.AddSint32("IsPinCheckOK", isPinOK ? 1 : 0);
            modData.AddDouble("VauxPd_Ln_V", VauxPd_Ln_V);
            modData.AddDouble("VauxPd_Lp_V", VauxPd_Lp_V);
            modData.AddDouble("VauxPd_Rn_V", VauxPd_Rn_V);
            modData.AddDouble("VauxPd_Rp_V", VauxPd_Rp_V);

            modData.AddDouble("Vpd_Xi1_V", Vpd_Xi1_V);
            modData.AddDouble("Vpd_Xi2_V", Vpd_Xi2_V);
            modData.AddDouble("Vpd_Xq1_V", Vpd_Xq1_V);
            modData.AddDouble("Vpd_Xq2_V", Vpd_Xq2_V);

            modData.AddDouble("Vpd_Yi1_V", Vpd_Yi1_V);
            modData.AddDouble("Vpd_Yi2_V", Vpd_Yi2_V);
            modData.AddDouble("Vpd_Yq1_V", Vpd_Yq1_V);
            modData.AddDouble("Vpd_Yq2_V", Vpd_Yq2_V);
            return modData;
        }

        public Type UserControl
        {
            get { return (typeof(TM_CoRxPinCheckGui)); }
        }

        #endregion
    }
}
