using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to measure power in mW from a power meter and
    /// correct for a power offset.
    /// Can return values in dBm or mW.
    /// </summary>
    public class Util_CalPowerMeasure
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="powMeter">Power meter</param>
        /// <param name="powerOffset_dB">Power offset</param>
        public Util_CalPowerMeasure(InstType_OpticalPowerMeter powMeter,
            double powerOffset_dB)
        {
            this.powMeter = powMeter;
            this.powerOffset_dB = powerOffset_dB;
        }

        /// <summary>
        /// get power offset in dB
        /// </summary>
        public double PowerOffset_dB
        {
            get { return powerOffset_dB; }
        }

        /// <summary>
        /// Convert a corrected power in dBm to a raw power in mW
        /// </summary>
        /// <param name="pow_dBm"></param>
        /// <returns></returns>
        public double Corrected_dBm_to_Raw_mW(double pow_dBm)
        {
            double powRaw_dBm = pow_dBm + powerOffset_dB;
            double powRaw_mW = Alg_PowConvert_dB.Convert_dBmtomW(powRaw_dBm);
            return powRaw_mW;
        }

        /// <summary>
        /// Measure power, correct and return value in dBm
        /// </summary>
        /// <returns>Corrected power in dBm</returns>
        public double MeasPower_dBm()
        {
            //System.Threading.Thread.Sleep(100);
            double pow_dBm = double.NegativeInfinity;
            double powUncorr_dBm = double.NegativeInfinity;

            double reading = powMeter.ReadPower();
            if (powMeter.Mode == InstType_OpticalPowerMeter.MeterMode.Absolute_mW)
            {
                // Convert to dBm
                double powUncorr_mW = reading;
                powUncorr_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powUncorr_mW);
            }
            else
                powUncorr_dBm = reading;

            // Power correction
            pow_dBm = powUncorr_dBm + powerOffset_dB;

            return pow_dBm;
        }


        /// <summary>
        /// Measure power, correct and return value in mW
        /// </summary>
        /// <returns>Corrected power in mW</returns>
        public double MeasPower_mW()
        {
            double pow_dBm = MeasPower_dBm();
            double pow_mW = Alg_PowConvert_dB.Convert_dBmtomW(pow_dBm);
            return pow_mW;
        }

        #region Private data
        InstType_OpticalPowerMeter powMeter;
        double powerOffset_dB;
        #endregion
    }
}
