// [Copyright]
//
// Bookham [Coherence Rx DC Test]
// Bookham.TestSolution.TestModules
//
// TM_InitialiseTest.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module for auliary pd leakeage current test
    /// </summary>
    public class TM_AuxPdDarkCurrentTest : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        ///  Read aux pd leakage current
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, 
            InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            
            double VauxPD_X_V = configData.ReadDouble("AuxPDBias_X_V");
            double IauxPD_X_A = configData.ReadDouble("AuxPDCompCurrent_X_A");
            double VauxPD_Y_V = configData.ReadDouble("AuxPDBias_Y_V");
            double IauxPD_Y_A = configData.ReadDouble("AuxPDCompCurrent_Y_A");
           
            // Initialise source for auxilary pd bias
            CoRxDCTestInstruments.SetSource2AuxPD(true);
            CoRxDCTestInstruments.InitAllAuxPDSourcesAsVoltSource(VauxPD_X_V, VauxPD_Y_V , IauxPD_X_A,IauxPD_Y_A );
            CoRxDCTestInstruments.SetAllAuxPdBias(VauxPD_X_V,VauxPD_Y_V , IauxPD_X_A, IauxPD_Y_A );
            CoRxDCTestInstruments.SetAllAuxPdSourceOutput(false);

            int delayTime = 10000;
            CoRxDCTestInstruments.PdSource_AUX_XR.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double IauxPD_XR_A = CoRxDCTestInstruments.PdSource_AUX_XR.CurrentActual_amp;
            CoRxDCTestInstruments.PdSource_AUX_XR.OutputEnabled = false;
            CoRxDCTestInstruments.PdSource_AUX_XL.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double IauxPD_XL_A = CoRxDCTestInstruments.PdSource_AUX_XL.CurrentActual_amp;
            CoRxDCTestInstruments.PdSource_AUX_XL.OutputEnabled = false;
            CoRxDCTestInstruments.PdSource_AUX_YR.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double IauxPD_YR_A = CoRxDCTestInstruments.PdSource_AUX_YR.CurrentActual_amp;
            CoRxDCTestInstruments.PdSource_AUX_YR.OutputEnabled = false;
            CoRxDCTestInstruments.PdSource_AUX_YL.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double IauxPD_YL_A = CoRxDCTestInstruments.PdSource_AUX_YL.CurrentActual_amp;
            CoRxDCTestInstruments.PdSource_AUX_YL.OutputEnabled = false;

            
            DatumList returnData = new DatumList();            
            
            returnData.AddDouble("IauxPD_XR_nA", Math .Abs(IauxPD_XR_A) * 1000000000);
            returnData.AddDouble("IauxPD_XL_nA", Math .Abs (IauxPD_XL_A) * 1000000000);
            returnData.AddDouble("IauxPD_YR_nA", Math .Abs (IauxPD_YR_A) * 1000000000);
            returnData.AddDouble("IauxPD_YL_nA", Math.Abs(IauxPD_YL_A) * 1000000000);

            return returnData;
        }

        /// <summary>
        /// Module GUI
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(TM_AuxPdDarkCurrentTestGui)); }
        }

        #endregion
    }
}
