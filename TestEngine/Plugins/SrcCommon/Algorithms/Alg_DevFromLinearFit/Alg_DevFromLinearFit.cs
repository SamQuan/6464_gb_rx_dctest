// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
//
// Alg_DevFromLinearFit.cs
//
// Author: kpillar, 2007
// Design: [Reference design documentation]

using System;
using Bookham.TestLibrary.Algorithms;


namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// An algorithm class for calculating the deviation from a linear fit
    /// </summary>
    public sealed class Alg_DevFromLinearFit
    {
        /// <summary>
        /// Empty private constructor.
        /// </summary>
        private Alg_DevFromLinearFit()
        {
        }

        /// <summary>
        /// Function to calculate the deviation from a linear fit, with array index arguments being provided
        /// </summary>
        /// <param name="x">Our array of x values</param>
        /// <param name="y">Our array of y values</param>
        /// <param name="fromIndex">The array index indicating the START of the range we wish to calculate deviation from a linear fit from</param>
        /// <param name="toIndex">The array index indicating the END of the range we wish to calculate deviation from a linear fit from</param>
        /// <returns>A devFromLinearFitResults structure</returns>
        public static DevFromLinearFitResults DevFromLinearFit(double[] x, double[] y, int fromIndex, int toIndex )
        {
            if (x == null || y == null)
            {
                throw new AlgorithmException("DevFromLinearFit - input array is null");
            }
            double xlength = x.Length;
            double ylength = y.Length;

            if (xlength != ylength)
            {
                throw new AlgorithmException("DevFromLinearFit - input arrays must be the same size");
            }
            if (xlength < 3)
            {
                throw new AlgorithmException("DevFromLinearFit - need at least 3 data points");
            }

            if (toIndex >= x.Length)
            {
                throw new AlgorithmException("DevFromLinearFit - cannot have toIndex bigger than array size");
            }

            if (fromIndex >= toIndex)
            {
                throw new AlgorithmException("DevFromLinearFit - cannot have fromIndex bigger than toIndex");
            }

            //do a least squares fit on the array data over the range stipulated
           
            LinearLeastSquaresFit myLSF = LinearLeastSquaresFitAlgorithm.Calculate(x, y, fromIndex, toIndex);

            //create a results structure
            DevFromLinearFitResults myResultsStruct = new DevFromLinearFitResults();
            
            //populate the fromIndex and ToIndex as used, (important to feed this back as the Alg user may have originally stipulated only
            //x values, not indexes, and i only wish to have 1 distinct results structure in place for all devFromLinearFit_xxx functions)
            myResultsStruct.FromIndexValueUsed = fromIndex;
            myResultsStruct.ToIndexValueUsed = toIndex;
            myResultsStruct.LinearFittedYAxisData = new double[x.Length];
            //create an array to hold the calculated deviation from linear values
            double[] deviationFromFittedYArray = new double[x.Length];

            // Populate the whole of the fitted line array
	        for(  int i = 0; i < x.Length; i++ )
	        {
                myResultsStruct.LinearFittedYAxisData[i] = (myLSF.Slope * x[i] + myLSF.YIntercept);
                //seems a good time to calculate the deviation from the fitted slope as well
                deviationFromFittedYArray[i] = myResultsStruct.LinearFittedYAxisData[i] - y[i];
            }

            //need to cut this 'deviation' array down to the correct range we wish to examine for max, min values
            deviationFromFittedYArray = Alg_ArrayFunctions.ExtractSubArray(deviationFromFittedYArray,fromIndex,toIndex);


            myResultsStruct.MaxPosDevFromLinearValue = Alg_PointSearch.FindMaxValueInArray(deviationFromFittedYArray);
            myResultsStruct.MaxNegDevFromLinearValue = Alg_PointSearch.FindMinValueInArray(deviationFromFittedYArray);
            
            //need to establish the abs maximum of the 2 values now
            double a = Math.Abs( myResultsStruct.MaxPosDevFromLinearValue );
            double b = Math.Abs( myResultsStruct.MaxNegDevFromLinearValue );


            myResultsStruct.MaxAbsDeviationFromLinearValue = (a > b) ? a : b;

            return (myResultsStruct);
            
        }

        /// <summary>
        /// Function to calculate the deviation from a linear fit, with x values being provided for FROM and TO, and this function calculating the array index's to use
        /// </summary>
        /// <param name="x">Our array of x values</param>
        /// <param name="y">Our array of y values</param>
        /// <param name="fitFromNearestXvalue">Function will search for the x value nearest to that specified and will use the index of this as the fromIndex array value</param>
        /// <param name="fitToNearestXvalue">Function will search for the x value nearest to that specified and will use the index of this as the toIndex array value</param>
        /// <returns>devFromLinearFitResults structure</returns>
        public static DevFromLinearFitResults DevFromLinearFit_xValues(double[] x, double[] y, double fitFromNearestXvalue, double fitToNearestXvalue)
        {
            if (x == null || y == null)
            {
                throw new AlgorithmException(" DevFromLinearFit - input array is null");
            }

            int fromIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(x, fitFromNearestXvalue);
            int toIndex = Alg_ArrayFunctions.FindIndexOfNearestElement(x, fitToNearestXvalue);

            return(DevFromLinearFit( x, y, fromIndex, toIndex )); 
        
        }

        /// <summary>
        /// A structure to encapsulate the results we will get back from our Alg_DevFromLinearFit functions
        /// </summary>
        public struct DevFromLinearFitResults
        {
            /// <summary>
            /// This is our original y axis data, that has now been adjusted based on results
            /// from using Alg_linearLeastSqFit.  Note that the WHOLE y axis array is adjusted, not just the subArray as stipulated
            /// with From and To index values
            /// </summary>
            public double[] LinearFittedYAxisData;
            /// <summary>
            /// The maximum positive deviation from the adjusted linearly fitted value.  
            /// This is calculated using only the values within the subArray as stipulated
            /// with From and To index values.
            /// </summary>
            public double MaxPosDevFromLinearValue;
            /// <summary>
            /// The maximum negative deviation from the adjusted linearly fitted value.  
            /// This is calculated using only the values within the subArray as stipulated
            /// with From and To index values.
            /// </summary>
            public double MaxNegDevFromLinearValue;
            /// <summary>
            /// The 'absolute' maximum deviation from the adjusted linearly fitted value.  
            /// This is calculated using only the values within the subArray as stipulated
            /// with From and To index values.
            /// </summary>
            public double MaxAbsDeviationFromLinearValue;
            /// <summary>
            /// The 'from' array index value we have used for our calculations
            /// </summary>
            public int FromIndexValueUsed;
            /// <summary>
            /// The 'to' array index value we have used for our calculations 
            /// </summary>
            public int ToIndexValueUsed;
        }
    }
}