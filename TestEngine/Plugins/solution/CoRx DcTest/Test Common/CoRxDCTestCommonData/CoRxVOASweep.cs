﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.CoRxDCTestCommonData
{
    public class CoRxVOASweep
    {
        private List<CoRxVOASweepData> VoaSweepdataList = new List<CoRxVOASweepData>();
        private CoRxVOASweepData FirstPt;
        private double mMaxXAttenuation = -double.MaxValue;
        private double mMaxYAttenuation = -double.MaxValue;
        private double mMaxMPDAttenuation = -double.MaxValue;
        private double mMinXAttenuation = +double.MaxValue;
        private double mMinYAttenuation = +double.MaxValue;
        private PolyFit mPdlFit;

        public static bool UseFixedMaxAttenForGradientCalculation = false;
        public static double FixedAttenuationForGradientCalc = 0;

        public static double AverageAttenuation(double attenuationX_db, double attenuationY_db)
        {
            double LinearAttenuationX = Math.Pow(10.0, attenuationX_db / 10);
            double LinearAttenuationY = Math.Pow(10.0, attenuationY_db / 10);

            return (10 * Math.Log10(Math.Abs(LinearAttenuationX + LinearAttenuationY) / 2));
        }

        public CoRxVOASweep()
        {

        }

        public void Add(CoRxVOASweepData SweepPt)
        {
            if (VoaSweepdataList.Count == 0) FirstPt = SweepPt;

            //if (VOAVolts == VOA_Volts_Start)
            //{
            //    FirstPt = TestData;             // Keep as reference for attenuation calculations
            //}
            SweepPt.XIPAttenuation_dB = 10 * Math.Log10(Math.Abs(FirstPt.XIPCurrent_A / SweepPt.XIPCurrent_A));
            SweepPt.YIPAttenuation_dB = 10 * Math.Log10(Math.Abs(FirstPt.YIPCurrent_A / SweepPt.YIPCurrent_A));
            SweepPt.MPDAttenuation_dB = 10 * Math.Log10(Math.Abs(FirstPt.MPDCurrent_A / SweepPt.MPDCurrent_A));
            SweepPt.XAttenuation_dB = SweepPt.X_Sum_A < 0 ? 9999 :  10 * Math.Log10(Math.Abs(FirstPt.X_Sum_A / SweepPt.X_Sum_A));
            SweepPt.YAttenuation_dB = SweepPt.Y_Sum_A < 0 ? 9999 : 10 * Math.Log10(Math.Abs(FirstPt.Y_Sum_A / SweepPt.Y_Sum_A));

            SweepPt.AverageAttenuation_db = 10 * Math.Log10(Math.Abs(FirstPt.XIPCurrent_A / SweepPt.XIPCurrent_A + FirstPt.YIPCurrent_A / SweepPt.YIPCurrent_A) / 2);

            if (SweepPt.XIPAttenuation_dB > mMaxXAttenuation) mMaxXAttenuation = SweepPt.XIPAttenuation_dB;
            if (SweepPt.YIPAttenuation_dB > mMaxYAttenuation) mMaxYAttenuation = SweepPt.YIPAttenuation_dB;
            if (SweepPt.XIPAttenuation_dB < mMinXAttenuation) mMinXAttenuation = SweepPt.XIPAttenuation_dB;
            if (SweepPt.YIPAttenuation_dB < mMinYAttenuation) mMinYAttenuation = SweepPt.YIPAttenuation_dB;
            if (SweepPt.MPDAttenuation_dB > mMaxMPDAttenuation) mMaxMPDAttenuation = SweepPt.MPDAttenuation_dB;

            VoaSweepdataList.Add(SweepPt);
            if (SweepPt.XIPAttenuation_dB > 2.5)
            {
                if (VoaSweepdataList[VoaSweepdataList.Count - 1].XIPAttenuation_dB < VoaSweepdataList[VoaSweepdataList.Count - 2].XIPAttenuation_dB)
                {
                    int iStop = 100;
                    iStop++;
                }
            }
        }

        public PolyFit PdlFit
        {

            get { return mPdlFit; }

        }

        public double MaxXAttenuation
        {
            get { return mMaxXAttenuation; }
        }

        public double MaxYAttenuation
        {
            get { return mMaxYAttenuation; }
        }

        /// <summary>
        /// Finds the PDL at a given average attenuation
        /// </summary>
        /// <param name="targetAttenuation_db">Target attenuation in dB</param>
        /// <returns>Polarisation dependant loss at this attenuation level</returns>
        /// <remarks>Search the average attenution to find the tartget value
        /// Calculate the correspnding PDL. Use linear fit between adjact points and see if that 
        /// gives us the same answer as the complicated curve fit used in Paignton.</remarks>
        public double PDLAtAvgAttenuation(double targetAttenuation_db)
        {
            if (targetAttenuation_db == 0) return 0.0;
            double pdl = -999.0;
            for (int iPtIndex = 0; iPtIndex < VoaSweepdataList.Count; ++iPtIndex)
            {
                if (VoaSweepdataList[iPtIndex].AverageAttenuation_db > targetAttenuation_db)
                {
                    double denom = VoaSweepdataList[iPtIndex].AverageAttenuation_db - VoaSweepdataList[iPtIndex - 1].AverageAttenuation_db;
                    double numerator1 = VoaSweepdataList[iPtIndex].PDL_db - VoaSweepdataList[iPtIndex - 1].PDL_db;
                    double numerator2 = targetAttenuation_db - VoaSweepdataList[iPtIndex - 1].AverageAttenuation_db;
                    pdl = VoaSweepdataList[iPtIndex - 1].PDL_db + numerator1 * numerator2 / denom;
                    break;
                }
            }
            return pdl;
        }

        public void CreatePdlPolyFit(double uptoAvgAtt_db, int PolyOrder)
        {

            List<double> AvgAttList = new List<double>();
            List<double> PdlList = new List<double>();

            for (int index = 0; index < VoaSweepdataList.Count; ++index)
            {
                AvgAttList.Add(VoaSweepdataList[index].AverageAttenuation_db);
                PdlList.Add(VoaSweepdataList[index].PDL_db);
                if (VoaSweepdataList[index].AverageAttenuation_db > uptoAvgAtt_db) break;
            }

            mPdlFit = Alg_PolyFit.PolynomialFit(AvgAttList.ToArray(), PdlList.ToArray(), PolyOrder);

        }

       

        public double GetMaxPdl(double UpToAttenuation)
        {


            //start by finding closest index for the attenuation for X and for Y
            int X_idx = 0;
            int Y_idx = 0;
            double X_diff = double.MaxValue;
            double Y_diff = double.MaxValue;


            for (int index = 0; index < VoaSweepdataList.Count; ++index)
            {
                CoRxVOASweepData pt = VoaSweepdataList[index];
                double currentXDiff = Math.Abs(pt.XAttenuation_dB - UpToAttenuation);
                if (currentXDiff < X_diff) {

                    X_idx = index;
                    X_diff = currentXDiff;
                
                }

                double currentYDiff = Math.Abs(pt.YAttenuation_dB - UpToAttenuation);
                if (currentYDiff < Y_diff)
                {

                    Y_idx = index;
                    Y_diff = currentYDiff;

                }

            }

            //take the higher index
            int UpToIdx = Math.Max(X_idx, Y_idx);
            

            double Result = double.MinValue;

            for (int index = 0; index <= UpToIdx; ++index)
            {
                CoRxVOASweepData pt = VoaSweepdataList[index];
                if (pt.PDL_db > Result)
                {

                    Result = pt.PDL_db;
                
                }

            }
            return Result;


        }


        /// <summary>
        /// Sanity check to ensure that the fitted curve is reasonable compare to the real data
        /// </summary>
        /// <returns></returns>
        public double MaximumDeviationFromPdlFit(double uptoAvgAtt_db)
        {

            double maxDiff = double.MinValue;

            for (int i = 0; i < VoaSweepdataList.Count; ++i)
            {

                double actualPdl = VoaSweepdataList[i].PDL_db;
                double AvgAttenuation_db = VoaSweepdataList[i].AverageAttenuation_db;
                double fittedPdl = GetPdlValueFromPolyFit(AvgAttenuation_db);
                double diff = Math.Abs(actualPdl - fittedPdl);
                maxDiff = Math.Max(diff, maxDiff);

                if (AvgAttenuation_db > uptoAvgAtt_db) break;

            }
            return maxDiff;
        }


        public double MaximumDeviationFromAttenuationSlop(double uptoAvgAtt_db)
        {
            double maxSlopDeviation = double.MinValue;

             List<double> AvgAttenuationSlopList = new List<double>();
            //calculated the AvgAttenuation slop
             for (int i = 1; i < VoaSweepdataList.Count; ++i)
             {
                 double AvgAttenuation_db = VoaSweepdataList[i].AverageAttenuation_db; 
                 if (AvgAttenuation_db > uptoAvgAtt_db) break;
                 AvgAttenuationSlopList.Add(AvgAttenuation_db - VoaSweepdataList[i - 1].AverageAttenuation_db);
             }

            //find the max AvgAttenuation slop deviation
             for (int i = 1; i < AvgAttenuationSlopList.Count; i++)
             {
                 if ((AvgAttenuationSlopList[i] - AvgAttenuationSlopList[i - 1]) > maxSlopDeviation)
                 {
                     maxSlopDeviation = Math.Abs(AvgAttenuationSlopList[i] - AvgAttenuationSlopList[i - 1]);
                 }
             }

             return maxSlopDeviation;
        }


        /// <summary>
        /// Get maximum from the polynomial fit given a certain resolution
        /// </summary>
        /// <param name="uptoAvgAtt_db"></param>
        /// <param name="resolution_db"></param>
        /// <returns></returns>
        public double GetMaxFromFit(double uptoAvgAtt_db, double resolution_db)
        {

            double max = double.MinValue;

            for (double AvgAtt_db = 0; AvgAtt_db <= uptoAvgAtt_db; AvgAtt_db = AvgAtt_db + resolution_db)
            {

                max = Math.Max(max, GetPdlValueFromPolyFit(AvgAtt_db));
            }
            return max;

        }




        public string WritePdlBlobData(string filePrefix, string fileDirectory, double uptoAvgAtt_db, double resolution_db)
        {

            if (PdlFit.FittedYArray.Length < 1) throw new Exception("Error for attempting to write pdl blob data before a polynomial fit has been generated");

            List<string> data = new List<string>();
            string header = "Attenuation (dB),PDL (dB),Coefficients";
            data.Add(header);
            int coeffIndex = 0;

            for (double AvgAtt_db = 0; AvgAtt_db <= uptoAvgAtt_db; AvgAtt_db = AvgAtt_db + resolution_db)
            {

                string csvLine = AvgAtt_db + "," + GetPdlValueFromPolyFit(AvgAtt_db);
                if (coeffIndex < PdlFit.Coeffs.Length) csvLine += "," + PdlFit.Coeffs[coeffIndex++];
                data.Add(csvLine);
            }

            string fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix, "", "CSV");
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(fileName, data);
            }

            return fileName;

        }





        /// <summary>
        /// Use the current polynomial fit of the PDL to get the value for a specified attenuation
        /// </summary>
        /// <param name="AvgAttenuation_dB"></param>
        /// <returns></returns>
        private double GetPdlValueFromPolyFit(double AvgAttenuation_dB)
        {

            double[] Coeffs = mPdlFit.Coeffs;
            double value = 0d;
            int power = 0;

            foreach (double c in Coeffs)
            {
                value += c * Math.Pow(AvgAttenuation_dB, power);
                power++;
            }

            return value;
        }

        public double VoltageAtAttenuation_X(double attenuationValue)
        {
            double Voltage = -999.0;
            for (int idx = 0; idx < VoaSweepdataList.Count; ++idx)
            {
                if (VoaSweepdataList[idx].XIPAttenuation_dB > attenuationValue)
                {
                    Voltage = VoaSweepdataList[idx - 1].VoaVoltage_V + (attenuationValue - VoaSweepdataList[idx - 1].XIPAttenuation_dB) *
                            (VoaSweepdataList[idx].VoaVoltage_V - VoaSweepdataList[idx - 1].VoaVoltage_V) / (VoaSweepdataList[idx].XIPAttenuation_dB - VoaSweepdataList[idx - 1].XIPAttenuation_dB);
                    break;
                }
            }

            return Voltage;
        }

        public double VoltageAtAttenuation_Y(double attenuationValue)
        {
            double Voltage = -999.0;
            for (int idx = 0; idx < VoaSweepdataList.Count; ++idx)
            {
                if (VoaSweepdataList[idx].YIPAttenuation_dB > attenuationValue)
                {
                    Voltage = VoaSweepdataList[idx - 1].VoaVoltage_V + (attenuationValue - VoaSweepdataList[idx - 1].YIPAttenuation_dB) *
                            (VoaSweepdataList[idx].VoaVoltage_V - VoaSweepdataList[idx - 1].VoaVoltage_V) / (VoaSweepdataList[idx].YIPAttenuation_dB - VoaSweepdataList[idx - 1].YIPAttenuation_dB);
                    break;
                }
            }

            return Voltage;
        }

        public double AttenuationGradient_X
        {
            get
            {

                double[] AttLevels = new double[] { 2, 4, 6, 8, 10, 12, 14, 16, 18 };
                double MaxSlope = double.MinValue;

                for (int j = 0; j < AttLevels.Length; j++ )
                {

                    double InstSlope = double.MinValue;

                    for (int idx = 0; idx < VoaSweepdataList.Count; ++idx)
                    {
                        if (VoaSweepdataList[idx].XIPAttenuation_dB > AttLevels[j])
                        {
                            if (idx < 1) throw new ArgumentOutOfRangeException("Cannot calculate voa instantaneous slope! index is zero");

                            double DeltaV = VoaSweepdataList[idx].VoaVoltage_V - VoaSweepdataList[idx - 1].VoaVoltage_V;
                            double DeltaAtt = VoaSweepdataList[idx].XIPAttenuation_dB - VoaSweepdataList[idx - 1].XIPAttenuation_dB;
                            InstSlope = DeltaAtt / DeltaV;
                            break;
                        }

                    }

                    if (InstSlope == double.MinValue)
                    { 
                      // throw new Exception(string.Format("Could not calculate instantaneous slope. Attenuation level {0} was not reached!", AttLevels[j].ToString()));
                       InstSlope = -999;
                       MaxSlope = Math.Max(MaxSlope, InstSlope);
                    }
                    else
                    {
                        MaxSlope = Math.Max(MaxSlope, InstSlope);
                    }

                }
                
                return MaxSlope;
            }
        }

        public double AttenuationGradient_Y
        {
            get
            {

                double[] AttLevels = new double[] { 2, 4, 6, 8, 10, 12, 14, 16, 18 };
                double MaxSlope = double.MinValue;

                for (int j = 0; j < AttLevels.Length; j++)
                {

                    double InstSlope = double.MinValue;

                    for (int idx = 0; idx < VoaSweepdataList.Count; ++idx)
                    {
                        if (VoaSweepdataList[idx].YIPAttenuation_dB > AttLevels[j])
                        {
                            if (idx < 1) throw new ArgumentOutOfRangeException("Cannot calculate voa instantaneous slope! index is zero");

                            double DeltaV = VoaSweepdataList[idx].VoaVoltage_V - VoaSweepdataList[idx - 1].VoaVoltage_V;
                            double DeltaAtt = VoaSweepdataList[idx].YIPAttenuation_dB - VoaSweepdataList[idx - 1].YIPAttenuation_dB;
                            InstSlope = DeltaAtt / DeltaV;
                            break;
                        }

                    }

                    if (InstSlope == double.MinValue)
                    {
                        // throw new Exception(string.Format("Could not calculate instantaneous slope. Attenuation level {0} was not reached!", AttLevels[j].ToString()));
                        InstSlope = -999;
                        MaxSlope = Math.Max(MaxSlope, InstSlope);
                    }
                    else
                    {
                        MaxSlope = Math.Max(MaxSlope, InstSlope);
                    }

                }

                return MaxSlope;
            }
        }

        public double AttenuationAtVoltage_X(double voltage)
        {
            double Answer = -999.0;
            foreach (CoRxVOASweepData ThisPoint in VoaSweepdataList)
            {
                if (Math.Round(ThisPoint.VoaVoltage_V, 2) >= voltage)
                {
                    Answer = ThisPoint.XIPAttenuation_dB;
                    break;
                }
            }
            return Answer;
        }

        public double AttenuationAtVoltage_Y(double voltage)
        {
            double Answer = -999.0;
            foreach (CoRxVOASweepData ThisPoint in VoaSweepdataList)
            {
                if (Math.Round(ThisPoint.VoaVoltage_V, 2) >= voltage)
                {
                    Answer = ThisPoint.YIPAttenuation_dB;
                    break;
                }
            }
            return Answer;

        }

        public double AttenuationAtVoltage_MPD(double voltage)
        {
            double Answer = -999.0;
            foreach (CoRxVOASweepData ThisPoint in VoaSweepdataList)
            {
                if (Math.Round(ThisPoint.VoaVoltage_V, 2) >= voltage)
                {
                    Answer = ThisPoint.MPDAttenuation_dB;
                    break;
                }
            }
            return Answer;

        }

        public string WriteData(string filePrefix, string fileDirectory)
        {

            if (VoaSweepdataList == null || VoaSweepdataList.Count < 1)
                return "";

            List<string> lstVoaSweep = new List<string>();

            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("VOA Bias (V),");
            strHeader.Append("VOA Current (mA),");
            strHeader.Append("PD XIP (mA),");
            strHeader.Append("PD XIN (mA),");
            strHeader.Append("PD YIP (mA),");
            strHeader.Append("PD YIN (mA),");
            strHeader.Append("MPD (mA),");
            strHeader.Append("X Attenuation (dB),");
            strHeader.Append("Y Attenuation (dB),");
            strHeader.Append("Average Attenuation (XI and YI) (dB),");
            strHeader.Append("PDL (dB),");
            strHeader.Append("MPD Attenuation (dB)");
            lstVoaSweep.Add(strHeader.ToString());

            //CoRxVOASweepData FirstPt = VoaSweepdataList[0];

            CoRxVOASweepData WorkingData;
            for (int idx = 0; idx < VoaSweepdataList.Count; idx++)
            {
                WorkingData = VoaSweepdataList[idx];
                StringBuilder dataSb = new StringBuilder();
                dataSb.Append(WorkingData.VoaVoltage_V);
                dataSb.Append(",");
                dataSb.Append(WorkingData.VOACurrent_mA);
                dataSb.Append(",");
                dataSb.Append(Math.Abs(WorkingData.XIPCurrent_mA));
                dataSb.Append(",");
                dataSb.Append(Math.Abs(WorkingData.XIN_Or_XQ_Current_mA));
                dataSb.Append(",");
                dataSb.Append(Math.Abs(WorkingData.YIPCurrent_mA));
                dataSb.Append(",");
                dataSb.Append(Math.Abs(WorkingData.YIN_Or_YQ_Current_mA));
                dataSb.Append(",");
                dataSb.Append(WorkingData.MPDCurrent_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.XIPAttenuation_dB);
                dataSb.Append(",");
                dataSb.Append(WorkingData.YIPAttenuation_dB);
                dataSb.Append(",");
                dataSb.Append(WorkingData.AverageAttenuation_db);
                dataSb.Append(",");
                dataSb.Append(WorkingData.PDL_db);
                dataSb.Append(",");
                dataSb.Append(WorkingData.MPDAttenuation_dB);
                //dataSb.Append(",");
                lstVoaSweep.Add(dataSb.ToString());
            }

            string fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix, "", "CSV");
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(fileName, lstVoaSweep);
            }
            return fileName;
        }

    

        public string WriteData6464(string filePrefix, string fileDirectory)
        {

            if (VoaSweepdataList == null || VoaSweepdataList.Count < 1)
                return "";

            List<string> lstVoaSweep = new List<string>();

            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("VOA Bias (V),");
            strHeader.Append("VOA Current (mA),");
            strHeader.Append("PD XI (mA),");
            strHeader.Append("PD XQ (mA),");
            strHeader.Append("PD YI (mA),");
            strHeader.Append("PD YQ (mA),");
            strHeader.Append("MPD (mA),");
            strHeader.Append("X Attenuation (dB),");
            strHeader.Append("Y Attenuation (dB),");
            strHeader.Append("Average Attenuation (XI and YI) (dB),");
            strHeader.Append("PDL (dB),");
            strHeader.Append("MPD Attenuation (dB)");
            lstVoaSweep.Add(strHeader.ToString());

            //CoRxVOASweepData FirstPt = VoaSweepdataList[0];

            CoRxVOASweepData WorkingData;
            for (int idx = 0; idx < VoaSweepdataList.Count; idx++)
            {
                WorkingData = VoaSweepdataList[idx];
                StringBuilder dataSb = new StringBuilder();
                dataSb.Append(WorkingData.VoaVoltage_V);
                dataSb.Append(",");
                dataSb.Append(WorkingData.VOACurrent_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.XIPCurrent_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.XIN_Or_XQ_Current_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.YIPCurrent_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.YIN_Or_YQ_Current_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.MPDCurrent_mA);
                dataSb.Append(",");
                dataSb.Append(WorkingData.XAttenuation_dB);
                dataSb.Append(",");
                dataSb.Append(WorkingData.YAttenuation_dB);
                dataSb.Append(",");
                dataSb.Append(WorkingData.AverageAttenuation_db);
                dataSb.Append(",");
                dataSb.Append(WorkingData.PDL_db);
                dataSb.Append(",");
                dataSb.Append(WorkingData.MPDAttenuation_dB);
                //dataSb.Append(",");
                lstVoaSweep.Add(dataSb.ToString());
            }

            string fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix, "", "CSV");
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(fileName, lstVoaSweep);
            }
            return fileName;
        }
    }


}
