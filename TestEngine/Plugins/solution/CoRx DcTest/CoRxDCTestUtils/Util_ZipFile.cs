using System;
using System.Collections.Generic;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to create and add to a zip file.
    /// </summary>
    public class Util_ZipFile : IDisposable
    {
        private ZipFile zipFile;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="zipFileName">The name of the zip archive file.</param>
        public Util_ZipFile(string zipFileName)
        {
            FileInfo fi = new FileInfo(zipFileName);

            if (fi.Exists && fi.Length > 0)
                zipFile = new ZipFile(zipFileName);
            else
                zipFile = ZipFile.Create(zipFileName);
        }

        ///// <summary>
        ///// Sets the compression level ( the default is 9 )
        ///// </summary>
        ///// <param name="compressionLevel">0=fast, store only to 9=slow, maximum compression</param>
        //public void SetCompressionLevel(int compressionLevel)
        //{
        //    zipStream.SetLevel(compressionLevel);
        //}

        /// <summary>
        /// Adds a file to our zip file
        /// </summary>
        /// <param name="fileToAdd">file name to add (relative path)</param>
        public void AddFileToZip(string fileToAdd)
        {
            if (!File.Exists(fileToAdd)) return;
            zipFile.BeginUpdate();
            zipFile.Add(fileToAdd);
            zipFile.CommitUpdate();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (zipFile != null)
            {
                zipFile.Close();
                zipFile = null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        ~Util_ZipFile()
        {
            Dispose();
        }
    }
    /// <summary>
    /// Class to de-compress a zip file
    /// </summary>
    public class Util_UnZipFile : IDisposable
    {
        private ZipInputStream zipInput;
        byte[] buffer;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="zipFilename"></param>
        public Util_UnZipFile(string zipFilename)
        {
            zipInput = new ZipInputStream(File.OpenRead(zipFilename));
            buffer = new byte[4096];
        }

        /// <summary>
        /// De-compress the zip file to the destination folder
        /// </summary>
        /// <param name="desPath"></param>
        public void UnZipFile(string desPath)
        {
            ZipEntry theEntry;
            while ((theEntry = zipInput.GetNextEntry()) != null)
            {
                string directoryName = desPath + "\\";
                string fileName = Path.GetFileName(theEntry.Name);

                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);

                if (fileName != String.Empty)
                {
                    string curPath = directoryName + theEntry.Name;
                    string curDir = Path.GetDirectoryName(curPath);
                    if (!Directory.Exists(curDir))
                        Directory.CreateDirectory(curDir);

                    FileStream streamWriter = File.Create(curPath);

                    int size = 2048;
                    while (true)
                    {
                        size = zipInput.Read(buffer, 0, buffer.Length);
                        if (size > 0)
                        {
                            streamWriter.Write(buffer, 0, size);
                        }
                        else
                        {
                            break;
                        }
                    }

                    streamWriter.Close();
                }
            }
            zipInput.Close();
        }

        #region IDisposable Members

        /// <summary>
        /// Dispose function
        /// </summary>
        public void Dispose()
        {
            if (zipInput != null)
            {
                zipInput.Close();
            }
        }

        #endregion
    }
}
