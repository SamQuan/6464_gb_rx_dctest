// [Copyright]
//
// Bookham Test Engine
// TM_CoRxSRVerify
//
// Bookham.TestSolution.TestModules/TM_CoRxSRVerifyGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class TM_CoRxSRVerifyGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TM_CoRxSRVerifyGui));
            this.lblTile = new System.Windows.Forms.Label();
            this.lblTestMsg = new System.Windows.Forms.Label();
            this.gbCaution = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gbCaution.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTile
            // 
            this.lblTile.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTile.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblTile.Location = new System.Drawing.Point(34, 18);
            this.lblTile.Name = "lblTile";
            this.lblTile.Size = new System.Drawing.Size(307, 114);
            this.lblTile.TabIndex = 8;
            this.lblTile.Text = "Tile";
            // 
            // lblTestMsg
            // 
            this.lblTestMsg.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestMsg.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblTestMsg.Location = new System.Drawing.Point(68, 185);
            this.lblTestMsg.Name = "lblTestMsg";
            this.lblTestMsg.Size = new System.Drawing.Size(700, 95);
            this.lblTestMsg.TabIndex = 7;
            this.lblTestMsg.Text = "label1";
            // 
            // gbCaution
            // 
            this.gbCaution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCaution.BackColor = System.Drawing.Color.Yellow;
            this.gbCaution.CausesValidation = false;
            this.gbCaution.Controls.Add(this.label2);
            this.gbCaution.Controls.Add(this.pictureBox1);
            this.gbCaution.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.gbCaution.Location = new System.Drawing.Point(403, 18);
            this.gbCaution.Name = "gbCaution";
            this.gbCaution.Size = new System.Drawing.Size(374, 123);
            this.gbCaution.TabIndex = 6;
            this.gbCaution.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.BackColor = System.Drawing.Color.Yellow;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 104);
            this.label2.TabIndex = 5;
            this.label2.Text = "caution:  laser is on , don\'t move DUT or fibre connector!";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(187, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(187, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // TM_CoRxSRVerifyGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Controls.Add(this.lblTile);
            this.Controls.Add(this.lblTestMsg);
            this.Controls.Add(this.gbCaution);
            this.Name = "TM_CoRxSRVerifyGui";
            this.Size = new System.Drawing.Size(840, 352);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.gbCaution.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTile;
        private System.Windows.Forms.Label lblTestMsg;
        private System.Windows.Forms.GroupBox gbCaution;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;

    }
}
