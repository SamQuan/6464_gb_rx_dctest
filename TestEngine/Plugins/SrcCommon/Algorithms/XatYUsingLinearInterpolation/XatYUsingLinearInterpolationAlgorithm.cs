// [Copyright]
//
// Bookham Test Engine Library
//
// XatYUsingLinearInterpolation.cs
//
// Author: Paul.Worsey, March 2006
// Design: As per Alg_XatYUsingLinearInterpolation DD


using System;


namespace Bookham.TestLibrary.Algorithms
{
	/// <summary>
    /// Class containing XatY algorithm
	/// </summary>
	public sealed class XatYAlgorithm
	{
        /// <summary>
        /// Private Constructor protects class
        /// </summary>
        private XatYAlgorithm()
        {

        }

        /// <summary>
        /// Find x given y and arrays defining x vs y
        /// Note: Due to using the BoundingIndicesAlgorithm this only works for single-valued data.
        ///       If two or more instances of y then algorithm only return the first 
        /// </summary>
        /// <param name="xArray">Array of x values</param>
        /// <param name="yArray">Array of y values</param>
        /// <param name="y">Value of y to find x at</param>
        /// <returns>Value of x</returns>
        /// 

		public static double Calculate( double[] xArray, double[] yArray, double y )
		{
			// PRECONDITIONS - check for empty arrays, same size etc ..

            if (xArray == null || yArray == null)
            {
                throw new AlgorithmException("Null input array");
            }

            if (xArray.Length != yArray.Length)
            {
                throw new AlgorithmException("Input arrays must be of Same Size");
            }

            if (xArray.Length < 2)
            {
                throw new AlgorithmException("Input array must have 2 or more elements");
            }

            


			// Find y within yArray
			BoundingIndices yboundingIndices = BoundingIndicesAlgorithm.Calculate( yArray, y );

			// Interpolate between the array values bounding y
			double x = -999;
			if( yboundingIndices.IndexOfArrayValueLessThan == yboundingIndices.IndexOfArrayValueGreaterThan )
			{
				x = xArray[yboundingIndices.IndexOfArrayValueGreaterThan];
			}
			else
			{
				double xArrayValueLessThanY = xArray[ yboundingIndices.IndexOfArrayValueLessThan ];
				double xArrayValueGreaterThanY = xArray[ yboundingIndices.IndexOfArrayValueGreaterThan ];
				double yArrayValueLessThanY = yArray[ yboundingIndices.IndexOfArrayValueLessThan ];
				double yArrayValueGreaterThanY = yArray[ yboundingIndices.IndexOfArrayValueGreaterThan ];

				x = LinearInterpolateAlgorithm.Calculate( yArrayValueLessThanY, yArrayValueGreaterThanY, xArrayValueLessThanY, xArrayValueGreaterThanY, y );
			}

			return x;
		}
	}
}
