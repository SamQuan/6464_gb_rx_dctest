// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasLimitsRead\PcasLimitsRead.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.


using System;
using System.Collections.Specialized;	// StringDictionary
using System.Globalization;				// CultureInfo
using System.Text;						// StringBuilder
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;


namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// Implementation of ILimitRead for PCAS
	/// </summary>
	public class PcasLimitsRead : ILimitRead
	{
		/// <summary>
		/// Constructor that allows data source name to be specified
		/// </summary>
		/// <param name="dataSource">Data source name ( e.g. 'PCAS' )</param>
		/// <param name="logger">The component providing logging functionality.</param>
		public PcasLimitsRead(  ILogging logger, string dataSource )
		{
			if ( dataSource == null || dataSource.Length == 0) 
			{
				throw new BookhamExternalDataException("A dataSource must be specified");
			}
			else
			{
				this.dataSource = dataSource;
			}
			this.logger = logger;
		}

	
		#region Private members

		/// <summary>
		/// Key information relating to one device.
		/// </summary>
		private deviceInfo keyInfo;
		/// <summary>
		/// A where clause constructed from any 'extra' keys found in the key object.
		/// </summary>
		private StringBuilder keyDataWhereClause;
		/// <summary>
		/// The Data source name
		/// </summary>
		private string dataSource;

		/// <summary>
		/// Extracts key parameters from the key Object
		/// </summary>
		/// <param name="keyTable">A collection of key/value pairs.</param>
		private void ProcessKeyData(StringDictionary keyTable)
		{
			// Clear any existing data
			keyInfo = new deviceInfo();
			keyDataWhereClause = new StringBuilder();

			foreach ( string key in keyTable.Keys )
			{
				string KeyName = key.ToUpper(CultureInfo.InvariantCulture);
				switch ( KeyName )
				{	
					case "DEVICE_TYPE":
						keyInfo.deviceType = keyTable[key].ToString().ToLower(CultureInfo.InvariantCulture);
						break;
					case "TEST_STAGE":
						keyInfo.testStage = keyTable[key].ToString().ToLower(CultureInfo.InvariantCulture);
						break;
					case "SPECIFICATION":
					case "SPEC_ID":
						keyInfo.specification = keyTable[key].ToString().ToLower(CultureInfo.InvariantCulture);
						break;
					case "SERIAL_NO":
						keyInfo.serialNo = keyTable[key].ToString().ToUpper(CultureInfo.InvariantCulture);	// Always Uppercase in PCAS
						break;
					case "SCHEMA":
						keyInfo.schema = keyTable[key].ToString().ToLower(CultureInfo.InvariantCulture);		// Always Lowercase in PCAS
						break;

					default:
						// Use any extra key data for a where clause
						if ( keyDataWhereClause.Length > 0 )
							keyDataWhereClause.Append(" and ");

						keyDataWhereClause.Append( KeyName + "=" + keyTable[key].ToString() );
						//BugOut.WriteLine("PcasRead","Extra key information : " + keyDataWhereClause.ToString());
						break;
				}
			}
			// Check that the required keys were present
			CheckKeyData();
		}



		/// <summary>
		/// Checks for existence of keys.
		/// SCHEMA must be present. Others are optional.
		/// </summary>		
		private void CheckKeyData()
		{
			StringBuilder missingKeys = new StringBuilder();

			if ( keyInfo.schema == null || keyInfo.schema.Length == 0 ) 
				missingKeys.Append("SCHEMA ");

			if ( keyInfo.specification == null || keyInfo.specification.Length == 0 ) 
			{
				// No spec info. Need DEVICE TYPE and TEST STAGE
				if ( keyInfo.deviceType == null || keyInfo.deviceType.Length == 0 )
					missingKeys.Append("DEVICE_TYPE ");

				if ( keyInfo.testStage == null || keyInfo.testStage.Length == 0 )
				{
					if ( missingKeys.Length > 0 )
						missingKeys.Append("SPECIFICATION ");
					missingKeys.Append("TEST_STAGE ");
				}
			} 

			if ( missingKeys.Length > 0 )
			{
				logger.ErrorInExternalDataPlugin("Missing key data for '" + missingKeys.ToString() );
			}
			
			// Check that the schema is one of our permitted values.
			CheckSchema(keyInfo.schema);
		}

		
		/// <summary>
		/// Checks whether the schema exists within the PCASschema enum
		/// </summary>
		/// <param name="schemaName">The schema name to be checked.</param>
		private void CheckSchema(string schemaName)
		{
			foreach(string name in Enum.GetNames(typeof(PCASschema)))
			{
				if ( String.Compare( name, keyInfo.schema, true, CultureInfo.InvariantCulture) == 0 )
					return;
			}
			logger.ErrorInExternalDataPlugin("Invalid schema '" + schemaName +  "'");
		}


		#endregion



		#region ILimitRead Members

		/// <summary>
		/// Returns a specification list from PCAS for device types matching keyData.
		/// </summary>
		/// <param name="keyData">Must contain SCHEMA and either SPECIFICATION, or DEVICE_TYPE and TEST_STAGE</param>
		/// <returns>A SpecList containing the matching specification.</returns>
		public SpecList GetLimit(StringDictionary keyData)
		{
			
			ProcessKeyData(keyData);
			PCAS_limitReader pcasRead = null;

			SpecList specList = new SpecList();
			pcasRead = new PCAS_limitReader(this.logger, dataSource);
			
			try
			{
				// Latest spec for device type / test stage
				PCASspec pcasSpec = pcasRead.getSpec( keyInfo.schema, keyInfo.deviceType , keyInfo.testStage, keyInfo.specification );

				// Create actualDatabaseKeySet for limits.
				StringDictionary actualDatabaseKeySet = new StringDictionary();
				actualDatabaseKeySet.Add("SPEC_ID",keyInfo.specification);
				actualDatabaseKeySet.Add("DEVICE_TYPE",keyInfo.deviceType);
				actualDatabaseKeySet.Add("TEST_STAGE",keyInfo.testStage);

				// Everything is priority 1
				int priority = 1;
				
				Specification specification = new Specification(pcasSpec.name, actualDatabaseKeySet, priority);

				foreach ( PCASlimit limit in pcasSpec )
				{
					LimitOperand operand;
					DatumType paramType;
					Datum lowLimit;
					Datum highLimit;
					
					// Default to "no limit" with null high and low limit datums unless the data type is numeric
					switch ( limit.dataType )
					{
						case PCASdataType.PCAS_FLOAT_126:
						{
							operand = LimitOperand.InRange;
							paramType = DatumType.Double;
							lowLimit = new DatumDouble(limit.shortDesc,limit.min);
							highLimit = new DatumDouble(limit.shortDesc,limit.max);
							break;
						}
						case PCASdataType.PCAS_NUMBER_38:
						{
							operand = LimitOperand.InRange;
                            // Look to see if we have a Pseudo PCAS Boolean, which is a PCAS Type of PCAS_NUMBER_38
                            // with units = "bool", and a Short Descriptor that starts with "BOOL_".
                            // Note that this is a Test Engine specific format.
                            if ((string.Compare(limit.units, "bool", true) == 0) && (limit.shortDesc.IndexOf("BOOL_") == 0))
                            {
                                if ((((int)limit.min != 0) && ((int)limit.min != 1)) ||
                                    (((int)limit.max != 0) && ((int)limit.max != 1)))
                                {
                                    //Error - this is supposed to be a pseudo boolean, where  0  = false, and 1 = true.                                    
                                    throw new BookhamExternalDataException("Invalid Boolean Limit For Parameter: " + limit.shortDesc);
                                }
                                else
                                {
                                    paramType = DatumType.BoolType;
                                    lowLimit = new DatumBool(limit.shortDesc, Convert.ToBoolean(limit.min));
                                    highLimit = new DatumBool(limit.shortDesc, Convert.ToBoolean(limit.max));
                                }
                            }
                            else
                            {
                                paramType = DatumType.Sint32;
                                lowLimit = new DatumSint32(limit.shortDesc, checked((int)limit.min));	// Use a checked block to ensure that an overflow is raised if values are outside the acceptable range
                                highLimit = new DatumSint32(limit.shortDesc, checked((int)limit.max));
                            }					
							break;
						}
						default:
						{
							operand = LimitOperand.NoLimits;
                            paramType = PcasLimitConvertUtils.GetDatumTypeFromStringShortDesc(limit.shortDesc);                    							
							lowLimit = null;
							highLimit = null;
							break;
						}
					}

					RawParamLimit rawLimit = new RawParamLimit(limit.shortDesc, 
						paramType, lowLimit, highLimit, operand, limit.priority, 
						limit.accuracy(), limit.units);
										
					specification.Add(rawLimit);
				}
				specList.Add(specification);
			}
			finally
			{
				// Always call Disconnect
				pcasRead.Disconnect();
			}

			return specList;
		}
		#endregion

		/// <summary>
		/// A reference to the component providing the logging functionality.
		/// </summary>
		private ILogging logger;
	}

	/// <summary>
	/// A structure to hold key data relating to one device.
	/// </summary>
	internal struct deviceInfo
	{
		internal string serialNo;
		internal string deviceType;
		internal string testStage;
		internal string specification;
		internal string schema;
	}
}
