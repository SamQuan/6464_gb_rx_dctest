// [Copyright]
//
// Bookham Modular Test Engine
// Default Test Control Plug-in
//
// UpdateTestStatuses.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.TestLibrary.TestControl.GuiMessages
{
	/// <summary>
	/// Message sent by Test Control Plug-in worker thread to the Batch View GUI to 
	/// Update Test Status of all devices in the currently loaded batch.  
	/// </summary>
	internal sealed class UpdateTestStatuses
	{
		internal UpdateTestStatuses(params TestStatus [] testStatuses)
		{
			// Initialise the internal testStatuses array by copying 
			this.testStatuses = new TestStatus[testStatuses.Length];
			Array.Copy(testStatuses, 0, this.testStatuses, 0, testStatuses.Length);
		}

		/// <summary>
		/// Test Status for each device in the currently loaded batch.
		/// </summary>
		internal readonly TestStatus [] testStatuses;
	}
}
