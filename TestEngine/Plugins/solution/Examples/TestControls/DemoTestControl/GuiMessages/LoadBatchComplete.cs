// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// LoadBatchComplete.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Load Batch Complete Message.
	/// </summary>
	internal sealed class LoadBatchComplete
	{
		internal LoadBatchComplete()
		{
		}
	}
}
