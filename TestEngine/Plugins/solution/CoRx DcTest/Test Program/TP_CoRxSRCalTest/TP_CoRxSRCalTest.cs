// [Copyright]
//
// Bookham [Coherence Rx DC Test ]
// Bookham.TestSolution.TestPrograms
//
// TP_CoRxSRCalTest.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Config;
using System.IO;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.CoRxDCTestCommonData;
namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// Program for splitter calibration 
    /// </summary>
    public class TP_CoRxSRCalTest : ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private string specName;
        Specification mainSpec;

        TestParamConfigAccessor testParmConfig;
        TestParamConfigAccessor tnstrParamConfig;

        TempTableConfigAccessor tempParamConfig;

        ConfigDataAccessor instrumentsCalData;
        
        Inst_CoRxITLALaserSource itla_sig = null;
        Inst_CoRxITLALaserSource itla_loc = null;
        DateTime time_Start;
        #endregion

        #region ITestProgram Members
        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return typeof(TP_CoRxSRCalTestGui); }
        }

        /// <summary>
        /// Initialise instrument, config files and test modules
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SetVersionControl(false, false);
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui("Initialising ...");
            foreach (Chassis chassisTemp in chassis.Values )
            {
                chassisTemp.EnableLogging = false;
            }

            foreach (Instrument instrTemp in instrs.Values )
            {
                instrTemp.EnableLogging = false;
            }

            LoadConfigFiles(engine, dutObject);
            // load specification
            SpecList specs = this.LoadSpecs(engine,dutObject);
            InitialiseInstrs(engine, instrs);        
            
            //Initialising test modules
            engine.SendStatusMsg("Initialising Test modules ");
            InitSRMeasModules(engine);

            if (!engine.IsSimulation)  // if testset on line, initialise instruments
            {
                // Shut down laser source for safe
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.OutputEnable = false;
                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.OutputEnable = false;
                // Zero all power meter modules' dark current
                try
                {
                    Inst_Ag816x_OpticalPowerMeter inst_8163 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon as Inst_Ag816x_OpticalPowerMeter;
                    //inst_8163.ZeroDarkCurrent_End();
                    Inst_Ag816x_OpticalPowerMeter inst_8163_1 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref  as Inst_Ag816x_OpticalPowerMeter;
                    //inst_8163_1.ZeroDarkCurrent_End();
                }
                catch
                { }

            }
            
        }
        /// <summary>
        /// Test run phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void Run(ITestEngineRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            bool isModuleRunOk = false;
            DatumList tmData = null;
            
            string modname = "";
            engine.GuiToFront();
            

            if (engine.IsSimulation) return;
            time_Start = DateTime.Now;
            #region Verifying splitter ratio
            engine.GuiToFront();
            CoRxDCTestInstruments.SelectItla(itla_loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(1000);
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.SelectItla(itla_sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            modname = "Measuring Signal Path Splitter Ratio";
            engine.SendToGui(modname);
            engine .SendToGui(new GuiMsg .CoRxConnectorSwitchRqst(" Please insert the Signal laser to its monitor power head then continue"));
            GuiMsg.CoRxSwitchConnectorCompleted rspFiberSwitch = null;                
            do 
            {
                rspFiberSwitch  = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
            }
            while ( rspFiberSwitch == null );
            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());

            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = true;
            tmData = engine.RunModule(modname).ModuleRunData .ModuleData;

            isModuleRunOk = (tmData.ReadSint32("IsSrCalOk")==1);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            engine.GuiToFront();
            engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
            engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the Signal laser output Connector to DUT then continue"));
            
            do
            {
                rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
            }
            while (rspFiberSwitch == null);

            CoRxDCTestInstruments.SelectItla(itla_loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            modname = "Measuring Local Osc Path Splitter Ratio";
            engine.SendToGui(modname);
            engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the local OSC laser to its monitor power head then continue"));
            
            do
            {
                rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
            }
            while (rspFiberSwitch == null);
            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = true;
            tmData = engine.RunModule(modname).ModuleRunData .ModuleData;
            isModuleRunOk = (tmData.ReadSint32("IsSrCalOk") ==1);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;

            // Reset the splitte counter in config file
            Util_SrCalCounter.UpdateNewSrCal();

            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            engine.GuiToFront();
            engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
            engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the local OSC laser output Connector to DUT then continue"));
            
            do
            {
                rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
            }
            while (rspFiberSwitch == null);
            #endregion

            engine.GuiToFront();
            engine.SendToGui("Splitter calibaration completed!");
        }
        /// <summary>
        /// post run, shut down insturments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;
            // TODO: shutdown actions
            CoRxDCTestInstruments.SelectItla(itla_sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.SelectItla(itla_loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
                        
        }
        /// <summary>
        /// data write phase, collect all information to fill drop file
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, 
                    DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            // Write keys required for external data (example below for PCAS)
            engine.BringTestResultsTabToFront();
            StringDictionary keys = new StringDictionary();
            // TODO: MUST Add real values below!
            keys.Add("SCHEMA", testParmConfig .GetStringParam("PCASSchema"));
            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", this.specName);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...
            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)
            // !(beware, all these parameters MUST exist in the specification)!...
            
            DatumList traceData = new DatumList();
            traceData.AddSint32("NODE", dutObject.NodeID);

            DateTime testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(time_Start);            
            traceData.AddString("TIME_DATE", time_Start.ToString("yyyyMMddHHmmss"));
            
            traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            if (engine.GetProgramRunStatus() == ProgramStatus.Success)
            {
                string Fred = engine.GetOverallPassFail().ToString();
                
                traceData.AddString("TEST_STATUS", Fred.ToUpper());
            }
            else
            {
                string msg = engine.GetProgramRunStatus().ToString();
                string runInfo = msg.Substring(0, (msg.Length > 14 ? 14 : msg.Length));
                traceData.AddString("TEST_STATUS", runInfo);
            }

            traceData.AddString("OPERATOR", userList.UserListString);
            // pick the specification to add this data to...
            engine.SetTraceData(specName, traceData);
        }

        #endregion


        #region Initialise function

        /// <summary>
        /// Load config file for test parameters, temperature setting and instrument calibration
        /// </summary>
        /// <param name="dutObj"></param>
        void LoadConfigFiles(ITestEngineInit engine, DUTObject dutObj)
        {
            engine.SendStatusMsg("Load configuration files");
            string configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\TempTable.xml", dutObj.NodeID);
            tempParamConfig = new TempTableConfigAccessor(dutObj, 1, configFilePath);

            testParmConfig = new TestParamConfigAccessor(dutObj,
               @"Configuration\CoRxDcTest\DCTestParams.xml", "ALL", "CoRxDCTestParams");

            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrumentParams.xml", dutObj.NodeID);
            tnstrParamConfig = new TestParamConfigAccessor(dutObj, configFilePath,"ALL", "InstrumentParams");

            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrsCalibration.xml", dutObj.NodeID);
            instrumentsCalData = new ConfigDataAccessor(configFilePath, "Calibration");
            
        }

        /// <summary>
        /// Load specs
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        private SpecList LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (testParmConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification
                
                #region Get loccal Limit file path
                // initialise limit file reader
                string limitFileName = "";
                string localLimitFileDir = testParmConfig.GetStringParam("PcasLimitFileDirectory");
                
                StringDictionary keys = new StringDictionary();                
                keys.Add("TestStage", dutObject.TestStage);
                keys.Add("DeviceType", dutObject.PartCode);
                ConfigDataAccessor localLimitConfig = new ConfigDataAccessor(
                    Path.Combine(localLimitFileDir, testParmConfig.GetStringParam("LocalLimitStragety")),
                    testParmConfig.GetStringParam("LocalLimitTableName"));
                
                // Get file name from data base
                try
                {
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }
                catch
                {
                    keys.Remove("DeviceType");
                    System.Threading.Thread.Sleep(100);
                    keys.Add("DeviceType", "ALL");
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }
                
                if (limitFileName != "")
                {
                    limitFileName = Path.Combine(localLimitFileDir, limitFileName);
                    if (File.Exists(limitFileName))
                        mainSpecKeys.Add("Filename", limitFileName);
                    else
                        engine.ErrorInProgram("Can't find limit file at " + limitFileName);
                   
                }  //LimitFileName!= ""
                else
                {
                    engine.ErrorInProgram("No local limit file available for device type " + dutObject .PartCode);
                }                
                #endregion
                
            }
            else  // get pcas limit file
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            // Get our specification object (so we can initialise modules with appropriate limits)
            mainSpec = tempSpecList[0];

            SpecList specList = new SpecList();
            specName = mainSpec.Name;
            specList.Add(mainSpec);
            engine.SetSpecificationList(specList);
            return specList;
        }
        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        private void InitialiseInstrs(ITestEngineInit engine, InstrumentCollection instrs)
        {
            engine.SendToGui("Initialising instruments ...");
            engine.SendStatusMsg("Initialising instruments ...");
            #region Assign instruments
             
            CoRxDCTestInstruments.SignalInputInstrs = new CoRxSourceInstrsChain();

            itla_sig = new Inst_CoRxITLALaserSource("iTLA_Sig",(Instr_ITLA)instrs["iTLA_Sig"]);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource = itla_sig ;
            CoRxDCTestInstruments.SignalInputInstrs.Voa = null;// (InstType_OpticalAttenuator)instrs["VOA_Sig"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_SigMon"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_SigRef"];

            CoRxDCTestInstruments.LocalOscInputInstrs = new CoRxSourceInstrsChain();
            itla_loc = new Inst_CoRxITLALaserSource("iTLA_Loc",(Instr_ITLA)instrs["iTLA_Loc"]);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource =   itla_loc;
            CoRxDCTestInstruments.LocalOscInputInstrs.Voa = null;//(InstType_OpticalAttenuator)instrs["VOA_Loc"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_LocMon"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_LocRef"];

            CoRxDCTestInstruments.TopCaseTec = (Inst_Nt10a)instrs["CaseTec"];
            CoRxDCTestInstruments.IsDualCaseTecControl = tnstrParamConfig.GetBoolParam("IsUseBottomTEC");
            if (CoRxDCTestInstruments.IsDualCaseTecControl)
            {
                CoRxDCTestInstruments.BottomCaseTec = (Inst_Nt10a)instrs["BottomCaseTec"];
            }
            else
                CoRxDCTestInstruments.BottomCaseTec = null;

            CoRxDCTestInstruments.PdSource_AUX_XR = (Inst_Ke24xx)instrs["AuxPdSource_XR"];
            CoRxDCTestInstruments.PdSource_AUX_XL = (Inst_Ke24xx)instrs["AuxPdSource_XL"];
            CoRxDCTestInstruments.PdSource_AUX_YL = (Inst_Ke24xx)instrs["AuxPdSource_YL"];
            CoRxDCTestInstruments.PdSource_AUX_YR = (Inst_Ke24xx)instrs["AuxPdSource_YP"];
            CoRxDCTestInstruments.IsAuxPDOnFront = tnstrParamConfig.GetBoolParam("IsAuxPdOnFrontTernimal");

            CoRxDCTestInstruments.PdSource_XI1 = (Inst_Ke24xx)instrs["PdSource_XI1"];
            CoRxDCTestInstruments.PdSource_XI2 = (Inst_Ke24xx)instrs["PdSource_XI2"];
            CoRxDCTestInstruments.PdSource_XQ1 = (Inst_Ke24xx)instrs["PdSource_XQ1"];
            CoRxDCTestInstruments.PdSource_XQ2 = (Inst_Ke24xx)instrs["PdSource_XQ2"];

            CoRxDCTestInstruments.PdSource_YI1 = (Inst_Ke24xx)instrs["PdSource_YI1"];
            CoRxDCTestInstruments.PdSource_YI2 = (Inst_Ke24xx)instrs["PdSource_YI2"];
            CoRxDCTestInstruments.PdSource_YQ1 = (Inst_Ke24xx)instrs["PdSource_YQ1"];
            CoRxDCTestInstruments.PdSource_YQ2 = (Inst_Ke24xx)instrs["PdSource_YQ2"];
            CoRxDCTestInstruments.TriggerInLine = tnstrParamConfig.GetIntParam("SlaveSourceTriggerInLine");
            CoRxDCTestInstruments.TriggerOutLine = tnstrParamConfig.GetIntParam("SlaveSourceTriggerOutputLine");
            CoRxDCTestInstruments.TriggerNoUsedLine = tnstrParamConfig.GetIntParam("SlaveSourceTriggerNoUseLine");

            CoRxDCTestInstruments.TiaSource_X = (Inst_Ke24xx)instrs["TIASource_X"];
            CoRxDCTestInstruments.TiaSource_Y = (Inst_Ke24xx)instrs["TIASource_Y"];

            try
            {
                CoRxDCTestInstruments.GCSource_X = (InstType_ElectricalSource)instrs["GCSource_X"];
                CoRxDCTestInstruments.GCSource_Y = (InstType_ElectricalSource)instrs["GCSource_Y"];
                CoRxDCTestInstruments.MCSource_X = (InstType_ElectricalSource)instrs["MCSource_X"];
                CoRxDCTestInstruments.MCSource_Y = (InstType_ElectricalSource)instrs["MCSource_Y"];
                CoRxDCTestInstruments.OaSource_X = (InstType_ElectricalSource)instrs["OaSource_X"];
                CoRxDCTestInstruments.OaSource_Y = (InstType_ElectricalSource)instrs["OaSource_Y"];
                CoRxDCTestInstruments.OCSource_X = (InstType_ElectricalSource)instrs["OcSource_X"];
                CoRxDCTestInstruments.OCSource_Y = (InstType_ElectricalSource)instrs["OcSource_Y"];
                CoRxDCTestInstruments.ThermPhaseSource_L = (InstType_ElectricalSource)instrs["ThermPhaseSource_L"];
                CoRxDCTestInstruments.ThermPhaseSource_R = (InstType_ElectricalSource)instrs["ThermPhaseSource_R"];
            }
            catch
            { }
            #endregion

            #region setting measurement and calibration parameters
                        
            CoRxDCTestInstruments.LocalOscInputInstrs.sngVoaDefAtt_dB = testParmConfig.GetDoubleParam("InitAtt_LocVoa_dB");
            CoRxDCTestInstruments.SignalInputInstrs.sngVoaDefAtt_dB = testParmConfig.GetDoubleParam("InitAtt_SigVoa_dB");

            CoRxDCTestInstruments.SignalInputInstrs.PowerSetTolerance_dB = testParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.PowerSetTolerance_dB = testParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.MaxPowerTuningCount = testParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");
            CoRxDCTestInstruments.SignalInputInstrs.MaxPowerTuningCount = testParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");

            string configFilePath = tnstrParamConfig.GetStringParam("SplitterCalDataBaseConfigPath");
            string configTableName = tnstrParamConfig.GetStringParam("SplitterCalDataBaseConfigTableName");
            string sigSplitterName = tnstrParamConfig.GetStringParam("SignalPathSplitterName");
            string locSplitterName = tnstrParamConfig.GetStringParam("LocPathSplitterName");

            try
            {
                CoRxDCTestInstruments.SignalInputInstrs.SetupSplitterRatioData(
                    sigSplitterName, configFilePath, configTableName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.SignalInputInstrs.SplitterRatiosCalData.IsDBConfigOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Signal path, Test have to Abort!");

            try
            {
                CoRxDCTestInstruments.LocalOscInputInstrs.SetupSplitterRatioData(
                    locSplitterName, configFilePath, configTableName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.LocalOscInputInstrs.SplitterRatiosCalData.IsDBConfigOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Loc oscilatior path, Test have to Abort!");
            
            // Check if the splitter ratio cal data is outdate or exceed maximun allowed count

            #endregion

            #region Initialise instruments

            if (!engine.IsSimulation)  // if testset on line, initialise instruments
            {
                double calFactor, calOffset;
                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.ReferencePower = calOffset;

                CoRxDCTestInstruments.SignalInputInstrs.MaxShutDownPower_dBm = tnstrParamConfig.GetDoubleParam("LocPathMaxShutDownPower_dBm");
                CoRxDCTestInstruments.LocalOscInputInstrs.MaxShutDownPower_dBm = tnstrParamConfig.GetDoubleParam("SigPathMaxShutDownPower_dBm");

                // Shut down laser source for safe
                CoRxDCTestInstruments.SelectItla(itla_sig);
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.OutputEnable = false;
                CoRxDCTestInstruments.SelectItla(itla_loc);
                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.OutputEnable = false;
                // Zero all power meter modules' dark current
                try
                {
                    Inst_Ag816x_OpticalPowerMeter inst_8163 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon as Inst_Ag816x_OpticalPowerMeter;
                    if (inst_8163 != null)
                    {
                        inst_8163.SafeMode = false;
                        //inst_8163.Range = tnstrParamConfig.GetDoubleParam("OpmRange_LocPath Mon_dBM");
                        inst_8163.Range = double.NaN;           // Set to Auto range
                        //inst_8163.ZeroAllModuleDarkCurrent();
                    }
                                        
                    Inst_Ag816x_OpticalPowerMeter inst_8163_3 = CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                    if (inst_8163_3 != null)
                    {
                        //inst_8163_3.Range = tnstrParamConfig.GetDoubleParam("OpmRange_SigPathRef_dBM");
                        inst_8163_3.Range = double.NaN;
                        inst_8163_3.SafeMode = false;
                    }

                    Inst_Ag816x_OpticalPowerMeter inst_8163_4 = CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon as Inst_Ag816x_OpticalPowerMeter;
                    if (inst_8163_4 != null)
                    {
                        //inst_8163_2.Range = tnstrParamConfig.GetDoubleParam("OpmRange_LocPath Ref_dBM");
                        inst_8163_4.Range = double.NaN;
                        inst_8163_4.SafeMode = false;
                    }

                    Inst_Ag816x_OpticalPowerMeter inst_8163_2 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                    if (inst_8163_2 != null)
                    {
                        //inst_8163_2.Range = tnstrParamConfig.GetDoubleParam("OpmRange_LocPath Ref_dBM");
                        inst_8163_2.Range = double.NaN;
                        inst_8163_2.SafeMode = false;
                        //if (!object.ReferenceEquals(inst_8163.InstrumentChassis, inst_8163_2.InstrumentChassis))
                        //    inst_8163_2.ZeroAllModuleDarkCurrent();
                    }
                }
                catch
                { }
                                
            }
            #endregion
        }
                

        /// <summary>
        /// Initialise modules to verify Splitters' ratio
        /// </summary>
        /// <param name="engine"></param>
        void InitSRMeasModules(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising splitter ratio verify module ...");
            
            #region Sig path verify module
            ModuleRun module = engine.AddModuleRun("Measuring Signal Path Splitter Ratio", "TM_CoRxSplitterRatioCal", "");
            
            module.ConfigData.AddDouble("SR_Max", tnstrParamConfig.GetDoubleParam("SR_Sig_Max"));
            module.ConfigData.AddDouble("SR_Min", tnstrParamConfig.GetDoubleParam("SR_Sig_Min"));
            module.ConfigData.AddDouble("VoaInitAtt", tnstrParamConfig.GetDoubleParam("Att_Sig@SRMeas_dB"));
            module.ConfigData.AddDouble("LaserSourcePower_dBm", tnstrParamConfig.GetDoubleParam("SignalSourcePower@SRMeas_dBm"));
            
            module.ConfigData.AddReference("OpcChainInstrs", CoRxDCTestInstruments.SignalInputInstrs);

            module.ConfigData.AddString("GuiTitle", "Measuring Signal Path Splitter Ratio");

            module.ConfigData.AddDouble("Freq_Start_GHz", testParmConfig.GetDoubleParam("StartFreq@SRMeas_GHz"));
            module.ConfigData.AddDouble("Freq_Stop_GHz", testParmConfig.GetDoubleParam("StopFreq@SRMeas_GHz"));
            module.ConfigData.AddDouble("Freq_Step_GHz", testParmConfig.GetDoubleParam("StepFreq@SRMeas_GHz"));
            module.Limits.AddParameter(mainSpec, "SPLITTER_SIG_CAL", "IsSrCalOk");
            
            #endregion

            #region Loc path verify module
            module = engine.AddModuleRun("Measuring Local Osc Path Splitter Ratio", "TM_CoRxSplitterRatioCal", "");
            
            module.ConfigData.AddDouble("SR_Max", tnstrParamConfig.GetDoubleParam("SR_loc_Max"));
            module.ConfigData.AddDouble("SR_Min", tnstrParamConfig.GetDoubleParam("SR_Loc_Min"));
            module.ConfigData.AddDouble("VoaInitAtt", tnstrParamConfig.GetDoubleParam("Att_Loc@SRMeas_dB"));
            module.ConfigData.AddDouble("LaserSourcePower_dBm", tnstrParamConfig.GetDoubleParam("LocSourcePower@SRMeas_dBm"));
            
            module.ConfigData.AddReference("OpcChainInstrs", CoRxDCTestInstruments.LocalOscInputInstrs);

            module.ConfigData.AddString("GuiTitle", "Measuring Local Osc Path Splitter Ratio");

            module.ConfigData.AddDouble("Freq_Start_GHz", testParmConfig.GetDoubleParam("StartFreq@SRMeas_GHz"));
            module.ConfigData.AddDouble("Freq_Stop_GHz", testParmConfig.GetDoubleParam("StopFreq@SRMeas_GHz"));
            module.ConfigData.AddDouble("Freq_Step_GHz", testParmConfig.GetDoubleParam("StepFreq@SRMeas_GHz"));

            module.Limits.AddParameter(mainSpec, "SPLITTER_LOC_CAL", "IsSrCalOk");
            #endregion
        }

        
        #endregion
        #region Helper functions
        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void ReadCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = instrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }
            

        #endregion
    }
}
