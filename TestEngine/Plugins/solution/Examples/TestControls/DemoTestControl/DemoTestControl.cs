// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// DemoTestControl.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.DemoTestControlPlugin.GuiMessages;
using System.Collections.Specialized;

namespace Bookham.DemoTestControlPlugin
{
	/// <summary>
	/// Test Control Plug-in for first Test Engine Demonstration.
	/// 
	/// This Test Control Plug-in has the following features:
	/// - Separate GUIs for Load Batch and Batch Display.
	/// - Supports 14-pin Laser device used for the demo.
	/// - Integrated with engineering batches for the demo in FactoryWorks.
	/// - Uses internal batch processing if in Simulation mode (doesn't try to interact with 
	///   FactoryWorks.
	/// </summary>
	public class DemoTestControl : ITestControl
	{

		/// <summary>
		/// Constructor. Initialises User control lists.
		/// </summary>
		public DemoTestControl ()
		{
			this.batchCtlList = new Type[1];
			this.batchCtlList[0] = typeof (DemoBatchViewCtl);

			this.testCtlList =  new Type[2];
			this.testCtlList[0] = typeof (DemoLoadBatchDialogueCtl);
			this.testCtlList[1] = typeof (DemoRetryAbortDialogue);
		}

		#region ITestControl Implementation.

		/// <summary>
		/// Get Property.
		/// The type of the Batch View GUI User Control
		/// </summary>
		public Type [] BatchContentsCtlTypeList
		{
			get
			{
				return (batchCtlList);
			}
		}

		/// <summary>
		/// Get Property.
		/// The type of the Test Control GUI User Control. Used here for Load Batch operations.
		/// </summary>
		public Type [] TestControlCtlTypeList
		{
			get
			{
				return (testCtlList);
			}
		}

		/// <summary>
		/// Get Property.
		/// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
		/// This is a Manual Mode Plug-in.
		/// </summary>
		public AutoManual TestType
		{
			get
			{
				return AutoManual.MANUAL;
			}
		}

		/// <summary>
		/// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
		/// Test Control Server.
		/// </summary>
		/// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
		/// <param name="mes">Object implementing the IMES interface.</param>
		/// <param name="operatorName">The name of the currently logged in user.</param>
		/// <param name="operatorType">The privilege level of the currently logged in user.</param>
		/// <returns>The BatchId of the loaded batch.</returns>
		public string LoadBatch( ITestEngine testEngine, IMES mes, string operatorName, TestControlPrivilegeLevel operatorType )
		{
			
			batchID = "";
			this.mes = mes;

			if (!this.configFileRead)
			{
				// Read the MES Simulation mode flag from configuration file, if this hasn't been done yet.
				readMesSimulationModeConfiguration(testEngine);
			}
			
			// Bring the Test Control Tab to the front of the GUI, and attract the user's attention.
			testEngine.PageToFront( TestControlCtl.TestControl);
			testEngine.SendStatusMsg( "Load a batch" );
			testEngine.GetUserAttention( TestControlCtl.TestControl);
			
			//Instruct the Test Control User Control to get user input to load a batch.
			testEngine.SendToCtl( TestControlCtl.TestControl, typeof (DemoLoadBatchDialogueCtl), new LoadBatchRequest() );

			//We haven't yet got a valid batchId.  Clear the got valid BatchId Flag.
			this.gotValidBatchId = false;

			//First time attempting to load current batch. set rerying flag to false, so that
			// we wait for a LoadBatchResponse message from the GUI.
			this.retryingMesLoadBatch = false;

			// Keep looping until a Batch is loaded.
			while(true )
			{
				if (retryingMesLoadBatch == false)
				{
					// Wait for a message from the Test Control GUI.
					CtlMsg msg = testEngine.WaitForCtlMsg();

					// Process the recieved message.
					Type messageType = msg.Payload.GetType();

					if (messageType == typeof (LoadBatchResponse))
					{
						// The user has specified the identifier of the batch that is to be loaded.
						// Attempt to load the batch specified .

						//Extract the BatchId from the recieved message.
						LoadBatchResponse loadBatchResponse = (LoadBatchResponse)msg.Payload;
						batchID = loadBatchResponse.batchId;

						//Stop attracting the user's attention.
						testEngine.CancelUserAttention( TestControlCtl.TestControl );

						//Set flag to indicate that there is a valid batchId.
						this.gotValidBatchId = true;
					}
					else
					{
						//Invalid message recieved.
						testEngine.ErrorRaise ("Invalid message recieved: " + msg.Payload.GetType().ToString());
					}
				}
				else
				{
					//Retrying Load Batch operation
					this.gotValidBatchId = true;
				}

				if (this.gotValidBatchId == true)
				{
					// Attempt to load the batch. Expect to catch exceptions if there are errors.
					try
					{
						// Use internal processing for the batch if in Simulation mode. Load a virtual bacth of 3 devices.
						if (inSimulationMode)
						{
							loadedBatch = mesLoadBatch(batchID);

						}
						else
						{
							// Load the Batch from the MES.
							loadedBatch = mes.LoadBatch( batchID, true );
					

							// Lookup node based on configuration
							this.mes.Node = this.nodeID;
						

							// Load the batch, but dont bother with component level attributes
							this.mes.TrackIn( batchID );
						}

                        // Set the NodeID for all DUTObjects in the Batch.
                        foreach (DUTObject dut in loadedBatch)
                        {
                            dut.NodeID = this.nodeID;
                        }

						// Initialise a new Test Statuses array.
						this.testStatuses = new TestStatus [loadedBatch.Count];

						// Initialise each device to untested status.
						int index = 0; 
											
						foreach( DUTObject dut in loadedBatch )
						{
							testEngine.SendToCtl( TestControlCtl.BatchView, typeof (DemoBatchViewCtl), dut );
							testStatuses [index++] = TestStatus.Untested; 
						}
		
						// Send a load batch complete message to the Test Control GUI, so that it can hide 
						// the load batch dialogue.
						testEngine.SendToCtl( TestControlCtl.TestControl, typeof (DemoLoadBatchDialogueCtl), new LoadBatchComplete() );
						
						//Update the Batch View GUI with the devices' test statuses within the batch.
						UpdateTestStatuses message = new UpdateTestStatuses (testStatuses);
						testEngine.SendToCtl( TestControlCtl.BatchView, typeof (DemoBatchViewCtl), message );
						
						//Batch has now loaded so we can now exit the while loop.
						retryingMesLoadBatch = false;
						break;
					}
					catch( MESTimeoutException )
					{
						// Log the fact that there was an MES Timeout exception.
						testEngine.SendStatusMsg ("MES Load Batch Operation timedout.");

						// Ask the user what to do about the error, then act accordingly.
						dealWithLoadBatchFromMesError (testEngine);
					}
					catch( MESInvalidOperationException e )
					{
						// Log the fact that there was an MES Invalid operation exception.
						testEngine.SendStatusMsg ("MES Invalid operation exception." + e.Message + e.StackTrace);
						
						// Ask the user what to do about the error, then act accordingly.
						dealWithLoadBatchFromMesError (testEngine);					
					}
					catch (MESCommsException e)
					{
						testEngine.SendStatusMsg ("MES Communications exception." + e.Message + e.StackTrace);
						// Ask the user what to do about the error, then act accordingly.
						dealWithLoadBatchFromMesError (testEngine);	
					}
				} //if (gotBatchId == true)
				else
				{
					//Invalid message recieved or no valid BatchId
					testEngine.ErrorRaise ("Invalid response to Batch ID Request.");
				}
			}// while( true )
			return batchID;
		}

		/// <summary>
		/// Get the next available DUT from the Batch.
		/// </summary>
		/// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
		/// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
		/// <param name="operatorName">The name of the currently logged in user.</param>
		/// <param name="operatorType">Privilege level of the currently logged in user.</param>
		/// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
		public DUTObject GetNextDUT( ITestEngine testEngine, ITestJig testJigArg, string operatorName, TestControlPrivilegeLevel operatorType)
		{
			bool found  = false;
			string receivedSerialNumber = " ";

			//Grab the user's attention, and instruct them to select a DUT.
			testEngine.PageToFront( TestControlCtl.BatchView );
			testEngine.SendStatusMsg( "Select a DUT" );
			testEngine.GetUserAttention( TestControlCtl.BatchView );
			testEngine.SendToCtl( TestControlCtl.BatchView, typeof (DemoBatchViewCtl), new SelectNextDUTRequest());

			// Select DUT - wait for message from BatchDisplay user ctl

			while (true)
			{
				CtlMsg msg = testEngine.WaitForCtlMsg();

				Type messageType = msg.Payload.GetType();
			
				if (messageType == typeof (EndOfBatchNotification))
				{
					// Exit here with a Null DUT object if we have reached the end of the batch.
					testEngine.CancelUserAttention( TestControlCtl.BatchView );
					return null;
				}
				else if (messageType == typeof (MarkedForRetestNotification))
				{
					// The user marked a device for retest. Update the internal test status for the device.
					MarkedForRetestNotification retestMessage = (MarkedForRetestNotification)msg.Payload;

					for (int i = 0; i < testStatuses.Length; i++)
					{
						if (loadedBatch[i].SerialNumber == retestMessage.serialNumber)
						{
							testStatuses[i] = TestStatus.MarkForRetest;
							// Break out of for loop, as we've found the deivce and updated it's status.
							break;
						}
					}
				}
				else if (messageType == typeof (SelectNextDUTResponse)) 
				{
					// The user has specified the serial number of the next device to be tested.
					SelectNextDUTResponse response = (SelectNextDUTResponse)msg.Payload;

					receivedSerialNumber = response.serialNumber;

					//Find the index of the DUT Object in the loaded batch. Store this in the 
					// currentDutIndex class private variable.
					int index = 0;
					

					foreach( DUTObject dutObject in loadedBatch )
					{
						if (dutObject.SerialNumber == receivedSerialNumber)
						{
							currentDutIndex = index; 
							found = true;
							// Found the DUT Object for the next device to be tested. Break out of the For loop.
							break;
						}

						index++;
					}	
			
					// Break out of the message recieve while (true) loop.
					break;
				}
				else
				{
					testEngine.ErrorRaise ("Test Control Plug-in Get Next DUT Operation - Invalid Message received: " + messageType.ToString());
				}
			} // While (true)
			
			if (!found)
			{
				//The required DUT is not in the bacth. This is a fatal error.
				testEngine.ErrorRaise("Get Next DUT: Invalid DUT Specified " + receivedSerialNumber);
			}


			// Get the DUT object.
			DUTObject dut = loadedBatch[currentDutIndex];

			// Stop attracting user attention
			testEngine.CancelUserAttention( TestControlCtl.BatchView );

			// Check if DUT is testable on current jig. This Test Control Plug-in will allow 
            // either the example Test Jig (Test Jig ID = "A") or the Default Test Engine 
            // Test Jig (Test Jig ID = "DefaulTestJigID")
			string jigID = testJigArg.GetJigID( testEngine.InDebugMode, testEngine.InSimulationMode );

            if ((jigID != "A") && (jigID != "DefaultTestJigID"))
			{
				testEngine.ErrorRaise ("Invalid Test Jig for device!!");
			}

            // Set Test Jig ID
            dut.TestJigID = jigID;

			// "Lookup" which test program to run
			dut.ProgramPluginName = "UnitTestProgram";
			dut.ProgramPluginVersion = "";
			return dut;
		}


		/// <summary>
		/// Called after the test program has concluded, to decide what the outgoing 
		/// Part ID and other attributes are, having inspected the test results.
		/// </summary>
		/// <param name="testEngine">Reference to object implmenting ITestEngine</param>
		/// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
		/// <param name="testResults">Detailed test results</param>
		/// <returns>Outcome of the results analysis.</returns>
		public DUTOutcome PostDevice( ITestEngine testEngine,  ProgramStatus programStatus, TestResults testResults )
		{

			// This Test Control Plug-in only supports one specification. The Part Id 
			// is not transformed based on results. 

			// Store the results for use in the Post Device commit Phase.
			testEngine.SendStatusMsg( "Post Device" );
			this.testResults = testResults;
			this.programStatus = programStatus;

			// Extract the Specification name from the test results. Fill this in the DUToutcome.
			SpecResultsList specs = testResults.SpecResults;
			string []specNames  = new string[specs.Count];

			int i = 0;
			foreach (SpecResults specRes in specs)
			{
				specNames [i++] = specRes.Name;
			}


			DUTOutcome dutOutcome = new DUTOutcome ();

			// Fill in the first specification name from the results for the DUToutcome.
			dutOutcome.OutputSpecificationNames = new string[1];
			dutOutcome.OutputSpecificationNames[0] = specNames[0];


			return dutOutcome;
		}

		/// <summary>
		/// Commit the test status for the current DUT in the MES.
		/// </summary>
		/// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit( ITestEngine testEngine, bool dataWriteError )
		{
			testEngine.SendStatusMsg( "Post Device Commit" );
            //Mark the Device as Tested. Also record it's pass fail status.
            if ((this.programStatus == ProgramStatus.Success) && (!dataWriteError))
            {
                //The Test Passed. Mark the device as completed.
                testStatuses[currentDutIndex] = TestStatus.Passed;
            }
            else if ((this.programStatus == ProgramStatus.Success) && (dataWriteError))
            {
                //Problem writing Test Data. Mark for retest.
                testStatuses[currentDutIndex] = TestStatus.MarkForRetest;
            }
			else if (this.programStatus == ProgramStatus.Failed ||
                     this.programStatus == ProgramStatus.NonParametricFailure)
            // We only want to condemn a device which is definitely duff, so assume all
            // other statuses leave the device as "untested".
			{
				testStatuses [currentDutIndex] = TestStatus.Failed;

				//Update the Component level MES Data for the device if not in Simulation mode.
				if (!inSimulationMode)
				{
					mes.SetComponentAttribute (batchID, loadedBatch[currentDutIndex].SerialNumber, "PartId", "FAILED");
				}
			}
			else
			{
				testStatuses [currentDutIndex] = TestStatus.Untested;
			}

			UpdateTestStatuses message = new UpdateTestStatuses (testStatuses);
			testEngine.SendToCtl( TestControlCtl.BatchView, typeof (DemoBatchViewCtl), message );
		}

		/// <summary>
		/// Trackout the Batch in the MES.
		/// </summary>
		/// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
		public void PostBatch( ITestEngine testEngine )
		{
			testEngine.SendStatusMsg( "Post Batch" );

			//Trackout the Batch if we are not in Simulation Mode.
			if ((!this.inSimulationMode) && (!trackoutInhibit))
			{
				mes.TrackOutBatchPass (this.batchID, loadedBatch.Stage, "Passed");
			}
		}

		/// <summary>
		/// Mark all devices in the Batch for retest.
		/// </summary>
		/// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
		public void RestartBatch( ITestEngine testEngine )
		{
			testEngine.SendStatusMsg( "Restart Batch" );

			for (int i = 0; i < testStatuses.Length; i++)
			{
				if (testStatuses[i] != TestStatus.Untested)
				{
					testStatuses[i] = TestStatus.MarkForRetest;
				}

				UpdateTestStatuses message = new UpdateTestStatuses (testStatuses);
				testEngine.SendToCtl( TestControlCtl.BatchView, typeof (DemoBatchViewCtl), message );
			}
		}


		# endregion

		#region Private Methods

		/// <summary>
		/// If this is the first Load batch, read the configuration file to see if the Plug-in is in 
		/// MES simulation mode or not. The configuration file is called called TestControlDemoPluginConfig.xml 
        /// and is stored in the Plugins/solution/Examples/ExampleSolution sub-directory.
		/// </summary>
		/// <param name="testEngine">Reference to Object that implements the ITestEngine interface.</param>
		private void readMesSimulationModeConfiguration(ITestEngine testEngine)
		{
			int result;
			string commonErrorMessageString = "Unable to read Demo Test Control Plug-in configuration File: ";

			//Read Configuration file.
			/* Open user data XML file. This constructor call only conducts a scant validation of the
				 * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader("Plugins/solution/Examples/ExampleSolution/TestControlDemoPluginConfig.xml");

			try
			{
				/* Scan file, one element a time. */
				while (config.Read())
				{
					if (config.NodeType == XmlNodeType.Element)
					{
						if (config.LocalName == "SimulateMes")
						{
							string simulationModeEnable = config.ReadString();

							result = String.Compare (simulationModeEnable.Trim(), "true", true, System.Globalization.CultureInfo.InvariantCulture);
							if (result == 0)
							{
								this.inSimulationMode = true;
							}
							else
							{
								result = String.Compare (simulationModeEnable.Trim(), "false", true, System.Globalization.CultureInfo.InvariantCulture);

								if (result == 0)
								{
									this.inSimulationMode = false;
								}
								else
								{
									//Invalid configuration file contents.
									testEngine.ErrorRaise ("Invalid SimulateMes field contents in Configuration/TestControlDemoPluginConfig.xml configuration file.");
								}
							}
						}

						if (config.LocalName == "InhibitTrackout")
						{
							string trackoutInhibitString = config.ReadString();

							result = String.Compare (trackoutInhibitString.Trim(), "true", true);
							if (result == 0)
							{
								this.trackoutInhibit = true;
							}
							else
							{
								result = String.Compare (trackoutInhibitString.Trim(), "false", true);

								if (result == 0)
								{
									this.trackoutInhibit = false;
								}
							}
						}

						if (config.LocalName == "NodeId")
						{
							string nodeIdString = config.ReadString().Trim();
							nodeID = Convert.ToInt32(nodeIdString);
							break;
						}
					}
				}

				// Set flag to indicate that configuration has been read.
				this.configFileRead = true;
			}
			catch (UnauthorizedAccessException uae)
			{
				
				/* User did not have read permission for file. */
				testEngine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
			}
			catch (System.IO.FileNotFoundException fnfe)
			{
				/* File not found error. */
				testEngine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
			}
			catch (System.IO.IOException ioe)
			{
				/* General IO failure. Not file not found. */
				testEngine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
			}
			catch (System.Xml.XmlException xe)
			{
				/* XML parse error. */
				testEngine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
			}
			finally
			{
				/* Close file. */
				config.Close();
			}	
		}

		/// <summary>
		/// Loads an internal Batch if in Simulation Mode.
		/// </summary>
		/// <param name="batchId">The Batch Id</param>
		/// <returns>The Batch Object.</returns>
		private MESbatch mesLoadBatch (string batchId)
		{
			MESbatch batch = new MESbatch ();
			batch.PartCode = "Generic Bookham 14 Pin Laser";
			batch.Stage = "Final Test";

			for (int i = 0; i < BatchSize; i++)
			{
				DUTObject dut = new DUTObject();
				dut.PartCode = "Generic Bookham 14 Pin Laser";
				dut.OverrideSimulationMode = false;
				dut.IsSimulation = false;
				dut.ContinueOnFail = false;
				dut.BatchID = batchId;
				dut.EquipmentID = "Test Set 007";
				if (i == 0)
				{
					dut.FirstInBatch = true;
				}
				else
				{
					dut.FirstInBatch = false;
				}

				if  (i == (BatchSize - 1))
				{
					dut.LastInBatch = true;
				}
				else
				{
					dut.LastInBatch = false;
				}
			
				dut.SerialNumber = "000" + i;

				dut.TestStage = "Final Test";

				dut.TrayPosition = i.ToString();

				dut.Attributes.AddString("UnitTestType", "Core Integration");
			

				batch.Add( dut );
			}

			return (batch);
		}

		/// <summary>
		/// Load Batch from MES error handler. Asks User Whether to retry or abort. 
		/// If the response is retry, . If Abort, log this, and request the user select another 
		/// batch to load.
		/// </summary>
		private void dealWithLoadBatchFromMesError (ITestEngine testEngine)
		{
			// Display the Retry/Abort Dialogue on the Test Control screen.
			testEngine.SelectTestControlCtlToDisplay( typeof (DemoRetryAbortDialogue));

			// Ask retry/abort
			testEngine.SendToCtl( TestControlCtl.TestControl, typeof (DemoRetryAbortDialogue), new QueryRetryAbortRequest("MES Load Batch operation Timed Out.") );
						
			// wait for response.
			CtlMsg msg = testEngine.WaitForCtlMsg();

			
			if (msg.Payload.GetType() == typeof (QueryRetryAbortResponse))
			{

				// Display the normal Load Batch Dialogue on the Test Control screen.
				testEngine.SelectTestControlCtlToDisplay( typeof (DemoLoadBatchDialogueCtl));

				// Get the message contents.
				QueryRetryAbortResponse responseMsg = (QueryRetryAbortResponse)msg.Payload;

				if (responseMsg.response == QueryRetryAbort.Abort)
				{
					// The User elected to abort. Request that the user select another batch for loading.
					testEngine.SendStatusMsg ( "User aborted load batch operation" );
					//Instruct the Test Control User Control to get user input to load a batch.
					testEngine.SendToCtl( TestControlCtl.TestControl, typeof (DemoLoadBatchDialogueCtl), new LoadBatchRequest() );
					retryingMesLoadBatch = false;
				}
				else
				{
					retryingMesLoadBatch = true;
				}
			}
			else
			{
				// Something very wrong - Log an error.
				testEngine.ErrorRaise( "Invalid message recieved" );
			} 
		}

		#endregion

		#region Private data

		Type [] batchCtlList;

		Type [] testCtlList;

		//Batch size for internally generated batches.
		private const int BatchSize = 3;
		
		// Object to hold the loaded batch.
		MESbatch loadedBatch; 

		// Reference to Object passed by the Test Control Server that implements the 
		// IMES interface.
		IMES mes;

		// Internal store of Test Statuses for the current batch.
		TestStatus [] testStatuses;

		// The current BatchID
		string batchID = "";

		// The Index of the current DUT within the batch & Test Statuses array.
		int currentDutIndex;

		// The Program status, as stored in the Post Device operation.
		ProgramStatus programStatus;

		// Test results for the current DUT.
		TestResults testResults;

		//Temporary simulation flag.
		bool inSimulationMode;

		//True if the Config file has been read
		bool configFileRead;

		//True if Trackout is to be inhibited. Read from the internal configuration file.
		bool trackoutInhibit;

		// When True, the Plugin is retrying loading a Batch from the MES.
		bool retryingMesLoadBatch;

		//When True, we have a valid BatchId. 
		bool gotValidBatchId;

		//Node ID as read from the configuration file.
		int nodeID;

		#endregion

	}
}
