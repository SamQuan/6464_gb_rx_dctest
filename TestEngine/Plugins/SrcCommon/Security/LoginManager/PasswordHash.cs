// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/PasswordHash.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;
using System.Security.Cryptography;
using Bookham.TestEngine.PluginInterfaces.Security;

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Utility class holding public static functions for hashing and checking passwords.
	/// </summary>
	public class PasswordHash
	{
		/// <summary>
		/// Secret salt added to all hashes. Do not reveal to operators.
		/// </summary>
		private const string salt = "\u7336\u4684IWasOnceATreehouse\u6772ILivedInACake\u3528";

		/// <summary>
		/// Transform user information into a salted hash.
		/// </summary>
		/// <param name="user">User name.</param>
		/// <param name="priv">Privilege level.</param>
		/// <param name="pass">Plain text password.</param>
		/// <returns>Base64 string of hash.</returns>
		public static string Hash(string user, PrivilegeLevel priv, string pass)
		{
			/* Check params. */
			if ((user == null) || (pass == null))
			{
				throw new SecurityException(
					"PasswordHash.Hash called with null string.");
			}

			/* Create plain text */
            string plainText =
				":" +
				user.Length + ":" +
				pass.Length + ":" +
				user + ":" +
				pass + ":" +
				priv.ToString() + ":" +
				PasswordHash.salt + ":";

			/* Convert string into byte array.
			 * UTF8 is an algorithm to encode a unicode array into a byte array. */
			byte[] plainArray = System.Text.Encoding.UTF8.GetBytes(plainText);

			/* Hash plainArray using SHA256.
			 * SHA256 is a secure one-way hash algorithm, turning any number of bytes into
			 * 256 bits of hash. In theory, the hash cannot be reversed. */
            HashAlgorithm hasher = SHA256.Create();
			byte[] hashArray = hasher.ComputeHash(plainArray);
			hasher.Clear();

			/* Encode the byte array into a friendly text string.
			 * Base64 is an encoding, transforming chunks of 6 bits into a character
			 * selected from letters, numerals and a couple of punctuation marks.
			 * A base64 string can be stored in an plain ascii text file. */
			return Convert.ToBase64String(hashArray);
		}

		/// <summary>
		/// Test login credentials against a loaded hash.
		/// </summary>
		/// <param name="user">User name.</param>
		/// <param name="priv">User privilege.</param>
		/// <param name="pass">Typed in password plaintext.</param>
		/// <param name="hash">Hash loaded from user data.</param>
		/// <returns>True if password correct. False on no match.</returns>
		public static bool TestPassword(string user, PrivilegeLevel priv, string pass, string hash)
		{
            return (Hash(user, priv, pass) == hash);
		}

		/// <summary>
		/// Private constructor to prevent construction.
		/// </summary>
		private PasswordHash()
		{}
	}
}
