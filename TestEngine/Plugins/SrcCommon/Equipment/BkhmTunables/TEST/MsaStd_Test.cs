using System;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using System.Threading;
using NUnit.Framework;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS;

namespace TunableModulesTester
{
    /// <exclude />	
    [TestFixture]
    public class MsaStd_Test
    {
        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise logging domain
//            Bookham.TestEngine.Framework.Logging.UnhandledExceptionsHandler.Initialise();
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** FCU over Ixolite Test Initialising ***");

            // create equipment objects
            this.ixloiteChassis = new Chassis_IxoliteWrapper("Chassis_IxoliteWrapper", "Ixolite", "LPT,378,40,5");
            this.fcu = new Instr_FCU("FCU", "Ixolite", "", "", this.ixloiteChassis);

            ixloiteChassis.EnableLogging = true;
            fcu.EnableLogging = true;
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
        }        
       
        [Test]
        public void T01_UnitSerialNumber()
        {
            TestOutput("Unit serial number = " + this.fcu.GetUnitSerialNumber());
            ((IDsdbrLaserSetup)fcu).EnterLaserSetupMode();
            this.fcu.SetUnitSerialNumber("0123456789abcdeF");
            TestOutput("Max I2C Rate = " + this.fcu.ReadMaxI2CRate_kbps());
        }

        [Test]
        public void T02_MiscTests()
        {
#if TESTING_AN_FCU
            this.fcu.EnterProtectMode();
            this.fcu.EnterVendorProtectMode("THURSDAY");
            TestOutput("Entered Vendor protect mode.");
            double thresh = this.fcu.GetWPowTh_dB();
            TestOutput("GetWPowTH() = " + thresh);
            this.fcu.SetWPowTh_dB(thresh * 0.2 + 1);
            TestOutput("GetWPowTH() after set = " + this.fcu.GetWPowTh_dB());
            this.fcu.SetWPowTh_dB(thresh);
            TestOutput("GetWPowTH() after set = " + this.fcu.GetWPowTh_dB());

            TestOutput("GetLeftImbalance() = " + this.fcu.GetLeftImbalance());
#endif
        }

        [Test]
        public void T03_TempTests()
        {
//            this.fcu.SetLaserTemperature_ohms(10000);

//            bool tempok = this.fcu.WaitForTempToSettle(5, 3.5, 5.0, 1.0);
//            TestOutput("WaitForTempToSettle returned " + tempok.ToString());

//            TestOutput("GetLaserTemperature_ohms ret " + this.fcu.GetLaserTemperature_ohms());
        }

#if XXX
        [Test]
        public void T04_ControlRegTests()
        {
            TestOutput("GetControlReg ret 0x" + this.fcu.GetControlRegister().ToString("X"));
            
            int frontSect = this.fcu.GetControlRegFrontSection();
            TestOutput("GetControlRegFrontSection ret " + frontSect);

            for (frontSect = 1; frontSect <= 7; ++frontSect)
            {
                this.fcu.SetControlRegFrontSection(frontSect);
                TestOutput("GetControlRegFrontSection ret " + frontSect);
            }

            bool oldDith = this.fcu.GetControlRegDitherOn();
            TestOutput("ControlReg dith is " + (oldDith ? "1" : "0"));
            this.fcu.SetControlRegDitherOn(!oldDith);
            TestOutput("ControlReg dith is " + (this.fcu.GetControlRegDitherOn() ? "1" : "0"));
            this.fcu.SetControlRegDitherOn(oldDith);
            TestOutput("ControlReg dith is " + (this.fcu.GetControlRegDitherOn() ? "1" : "0"));
        }
#endif

        [Test]
        public void T05_TransactionWithBytes()
        {
            string sn = ((IBlackBoxIdentity)fcu).GetUnitSerialNumber();
            TestOutput("GetUnitSerialNumber returned \"" + sn + "\" (" + sn.Length + ").");

            if (Char.IsUpper(sn[0]))
            {
                sn = sn.ToLower() + "A";    
            }
            else
            {
                sn = sn.ToUpper() + "b";
            }

            if (sn.Length > 14)
            {
                sn = sn.Substring(0, 7);
            }

            TestOutput("New serno = \"" + sn + "\".");
            ((IBlackBoxIdentity)fcu).SetUnitSerialNumber(sn);

            sn = ((IBlackBoxIdentity)fcu).GetUnitSerialNumber();
            TestOutput("GetUnitSerialNumber returned \"" + sn + "\" (" + sn.Length + ").");            
        }


        [Test]
        public void T06_FiddleSOADac()
        {
            IDsdbrLaserSetup dsdbr = (IDsdbrLaserSetup)fcu;
            dsdbr.EnterLaserSetupMode();

            //fcu.AnnotateInstLog("Turning Power control off.");
            dsdbr.SetPowerControlLoopOn(false);

            //fcu.AnnotateInstLog("Modifying SOA DAC.");
            TestOutput("GetSOADAC() = " + dsdbr.GetSOADAC());
            dsdbr.SetSOADAC(20000);
            Thread.Sleep(TimeSpan.FromSeconds(1));
            TestOutput("GetSOADAC() = " + dsdbr.GetSOADAC());

            //fcu.AnnotateInstLog("Restoring power control.");
            dsdbr.SetPowerControlLoopOn(true);
            TestOutput("GetSOADAC() = " + dsdbr.GetSOADAC());
        }

        public void T07_BlackBoxId()
        {
            IBlackBoxIdentity bbi = (IBlackBoxIdentity)fcu;
            Random rnd = new Random();

            /* Get build date */
            DateTime bdate = bbi.GetUnitBuildDate();
            TestOutput("Old build date = " + bdate.ToString());

            bdate += TimeSpan.FromDays(rnd.NextDouble()*20 - 10);
            bbi.SetUnitBuildDate(bdate);
//            Thread.Sleep(5);
            TestOutput("New build Date = " + bbi.GetUnitBuildDate().ToString());
        }

        public void T08_TsffTemperature()
        {
            IMSA300pin msa = (IMSA300pin)fcu;
            msa.EnterVendorProtectMode("THURSDAY");

            TestOutput("Module temp = " + msa.GetModuleTemp_degC() + " degC");
            //TestOutput("Laser temp = " + msa.GetLaserAbsoluteTemperature_degC() + " degC");
            //TestOutput("APD temp = " + msa.APDTemp_degC() + " degC");
            //TestOutput("Submount temp = " + msa.SubMountTemp_degC() + " degC");
        }

        #region Private helper fns and data
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        Chassis_IxoliteWrapper ixloiteChassis;
        
        Instr_FCU fcu;
        
        #endregion

    }

}
