// [Copyright]
//
// Bookham Test Engine
// TM_CoRxResponsivity
//
// Bookham.TestSolution.TestModules/TM_CoRxResponsivityGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestSolution.TestModules
{
    partial class TM_CoRxResponsivityGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TM_CoRxResponsivityGui));
            this.gbCaution = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.pbRespProgress = new System.Windows.Forms.ProgressBar();
            this.gbProgress = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbCaution.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCaution
            // 
            this.gbCaution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCaution.BackColor = System.Drawing.Color.Yellow;
            this.gbCaution.CausesValidation = false;
            this.gbCaution.Controls.Add(this.label3);
            this.gbCaution.Controls.Add(this.label2);
            this.gbCaution.Controls.Add(this.pictureBox1);
            this.gbCaution.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.gbCaution.Location = new System.Drawing.Point(221, 17);
            this.gbCaution.Name = "gbCaution";
            this.gbCaution.Size = new System.Drawing.Size(558, 135);
            this.gbCaution.TabIndex = 3;
            this.gbCaution.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.BackColor = System.Drawing.Color.Yellow;
            this.label2.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(16, 16);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(308, 55);
            this.label2.TabIndex = 5;
            this.label2.Text = "Caution:  laser is on.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(365, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(187, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.MediumAquamarine;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 173);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(656, 55);
            this.label1.TabIndex = 5;
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMessage.AutoSize = true;
            this.lblMessage.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessage.Location = new System.Drawing.Point(26, 19);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(162, 24);
            this.lblMessage.TabIndex = 8;
            this.lblMessage.Text = "Sweep progress";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblMessage.Visible = false;
            // 
            // pbRespProgress
            // 
            this.pbRespProgress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbRespProgress.Location = new System.Drawing.Point(223, 19);
            this.pbRespProgress.Name = "pbRespProgress";
            this.pbRespProgress.Size = new System.Drawing.Size(515, 38);
            this.pbRespProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbRespProgress.TabIndex = 7;
            // 
            // gbProgress
            // 
            this.gbProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbProgress.Controls.Add(this.pbRespProgress);
            this.gbProgress.Controls.Add(this.lblMessage);
            this.gbProgress.Location = new System.Drawing.Point(20, 231);
            this.gbProgress.Name = "gbProgress";
            this.gbProgress.Size = new System.Drawing.Size(759, 73);
            this.gbProgress.TabIndex = 9;
            this.gbProgress.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.BackColor = System.Drawing.Color.Yellow;
            this.label3.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(18, 71);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(286, 56);
            this.label3.TabIndex = 6;
            this.label3.Text = "Don\'t move DUT or fibre connector!";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TM_CoRxResponsivityGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Controls.Add(this.gbProgress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbCaution);
            this.Name = "TM_CoRxResponsivityGui";
            this.Size = new System.Drawing.Size(800, 329);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.ModuleGui_MsgReceived);
            this.gbCaution.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbProgress.ResumeLayout(false);
            this.gbProgress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCaution;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.ProgressBar pbRespProgress;
        private System.Windows.Forms.GroupBox gbProgress;
        private System.Windows.Forms.Label label3;
    }
}
