@echo off
set TE_ROOT=%cd%
echo TE_ROOT set to %TE_ROOT%
set SOLN_ROOT=%TE_ROOT%\Plugins\solution
set CLEANSOLN=%TE_ROOT%\MSBuildScript.proj /t:CleanSolutionTree
set CLEANSRCCOMMON=%TE_ROOT%\MSBuildScript.proj /t:CleanSrcCommon

echo Adding SDK 2.0 to path
IF EXIST "C:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="C:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "D:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="D:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "E:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="E:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "F:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="F:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "G:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="G:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "H:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="H:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "I:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin" SET VS_PATH="I:\Program Files\Microsoft Visual Studio 8\SDK\v2.0\Bin"
IF EXIST "C:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="C:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
IF EXIST "D:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="D:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
IF EXIST "E:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="E:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
IF EXIST "F:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="F:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
IF EXIST "G:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="G:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
IF EXIST "H:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="H:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
IF EXIST "I:\Program Files\Microsoft.NET\SDK\v2.0\Bin" SET VS_PATH="I:\Program Files\Microsoft.NET\SDK\v2.0\Bin"
PATH=%PATH%;%VS_PATH%

echo Adding .NET Framework 2.0 bin to path
set DOTNET2_PATH=%windir%\Microsoft.NET\Framework\v2.0.50727
PATH=%PATH%;%DOTNET2_PATH%

echo ...

cmd
