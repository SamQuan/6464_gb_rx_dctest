// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_SMUTI_DMM.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_DMM
    /// </summary>
    public class Inst_SMUTI_DMM : Instrument
    {
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_DMM(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 

            // 8 Channel High Current Low Voltage card (DSDR High Current )
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord HighCurrentLowVoltageCard = new InstrumentDataRecord(
                "Bookham Tech PLC",				// hardware name 
                "FW:0001",  			// minimum valid firmware version 
                "FW:1001");			// maximum valid firmware version 
            ValidHardwareData.Add("HighCurrentLowVoltageCard", HighCurrentLowVoltageCard);

            //TODO: add other type card

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_SMUTI",								// chassis driver name  
                "FW:0001",									// minimum valid chassis driver version  
                "FW:1001");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);
            instChassis = (Chassis_SMUTI)chassisInit;
        }

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instChassis.FirmwareVersion;
            }
        }
        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instChassis.HardwareIdentity;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public override void SetDefaultState()
        {

        }
       /// <summary>
       /// 
       /// </summary>
       /// <param name="senseCompliance_volt"></param>
        /// <param name="adc">if true,retrue </param>
       /// <returns></returns>
        public double SenseCurrent(double senseCompliance_volt)
        {
            string command;
            command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
            string current_A = instChassis.Query(command, this);
            return double.Parse(current_A);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="senseCompliance_volt"></param>
        /// <param name="adc"></param>
        public void SenseCurrent(double senseCompliance_volt, out int adc)
        {
            string command;
            command = string.Format("SOURce{0}:CHANnel{1}:CURRent?ADC", this.Slot, this.SubSlot);
            adc = int.Parse(instChassis.Query(command, this));
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="senseCompliance_current"></param>
        /// <returns></returns>
        public double SenseVoltage(double senseCompliance_current)
        {
            string command;
            //set compliance current
            command = string.Format("SOURce{0}:CHANnel{1}:VOLTage" + senseCompliance_current, this.Slot, this.SubSlot);
            instChassis.Write(command,this);
            //get sense current
            command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
            string voltage_V = instChassis.Query(command, this);
            return double.Parse(voltage_V);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="senseCompliance_current"></param>
        /// <param name="adc"></param>
        public void SenseVoltage(double senseCompliance_current, out int adc)
        {
            string command;
            //set compliance current
            command = string.Format("SOURce{0}:CHANnel{1}:VOLTage" + senseCompliance_current+"adc", this.Slot, this.SubSlot);
            instChassis.Write(command, this);
            //get sense current
            command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?adc", this.Slot, this.SubSlot);
            adc = int.Parse(instChassis.Query(command, this));
            
        }

        #region private data

        Chassis_SMUTI instChassis;

        #endregion

    }
}
