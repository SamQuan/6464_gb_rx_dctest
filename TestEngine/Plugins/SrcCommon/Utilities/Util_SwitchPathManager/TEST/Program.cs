using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Equipment;

namespace TEST
{
    class Program
    {
        static void Main(string[] args)
        {
            InstrumentCollection instrColl = new InstrumentCollection();
            Util_SwitchPathManager switchDef = new Util_SwitchPathManager("../../test.xml", instrColl);
            switchDef.SetToPosn("DUT_TO_DUT");
            switchDef.SetToPosn("DUT_TO_REF1");
        }
    }
}
