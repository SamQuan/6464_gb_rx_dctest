// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.ChassisNS
//
// Chassis_Ag662x.cs
//
// Author: K Pillar
// Design: As specified in Ag662x Driver DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisNS
{

	/// <summary>
	/// The chassis Ag662x driver class, where x indicates 1 to 9.
	/// </summary>
	public class Chassis_Ag662x : ChassisType_Visa
	{

		#region Public Functions

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
		public Chassis_Ag662x(string chassisName, string driverName, string resourceStringId)
			: base(chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information

			// Add details of Ag6624 chassis
			ChassisDataRecord Ag6624aData = new ChassisDataRecord(
				"HP6624A",		// hardware name 
				"0",										// minimum valid firmware version 
				"3833");										// maximum valid firmware version 
			ValidHardwareData.Add("Ag6624a", Ag6624aData);

			// Add details of Ag6626 chassis
			ChassisDataRecord Ag6626aData = new ChassisDataRecord(
				"HP6626A",		// hardware name 
				"0",										// minimum valid firmware version 
				"3833");										// maximum valid firmware version 
			ValidHardwareData.Add("Ag6626a", Ag6626aData);

			// Add details of Ag6629 chassis
			ChassisDataRecord Ag6629aData = new ChassisDataRecord(
				"HP6629A",		// hardware name 
				"0",			// minimum valid firmware version 
				"3833");		// maximum valid firmware version 
			ValidHardwareData.Add("Ag6629a", Ag6629aData);


		}


		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				// Read the chassis firmware string 
				string ver = this.EmptyString_QueryUnchecked("ROM?", null);
				return ver;
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				// Read the chassis ID string
				string idn = this.EmptyString_QueryUnchecked("ID?", null);
				return idn;
			}
		}


		/// <summary>
		/// Checks the error register status (slot independent)
		/// </summary>
		public void CheckErrorStatus()
		{
			//the error register is independent of slot
			string errorRegister = this.EmptyString_QueryUnchecked("ERR?", null);
			if ((errorRegister != "0") && (errorRegister != "6"))
			{
				throw new ChassisException("Error Register indicates error " + errorRegister);
			}
		}

		/// <summary>
		/// Specific command to try and catch empty string responses from the 6626
		/// (put in as a result of integration test issue)
		/// </summary>
		/// <param name="cmd">Our string command</param>
		/// <param name="instr">the instrument</param>
		/// <returns>the string response</returns>
		public string EmptyString_QueryUnchecked(string cmd, Instrument instr)
		{
			string result;
			bool valid = false;
			int count = 0;
			do
			{
				result = this.Query_Unchecked(cmd, instr);

				if (result == "")
				{
					// Sleep for 500ms waiting for the DUT to stabilise
					System.Threading.Thread.Sleep(500);
					count++;
				}
				else
				{
					valid = true;
				}
			} while ((!valid) && (count < 3));

			return (result);
		}

		#endregion

	}

}
