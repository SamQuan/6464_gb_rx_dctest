﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.Instruments
{


    public partial class Instr_SPI : Instrument
    {
         

        public enum DIO_Channels
        {

            InvalidChannel,
         
            Mz1RightBiasDac,
            Mz1LeftBiasDac,
            Mz2RightBiasDac,
            Mz2LeftBiasDac,
            Mz3LeftBiasDac,
            Mz3RightBiasDac,
            Mz4LeftBiasDac,
            Mz4RightBiasDac,
            Mz1RightBiasImonAdc,
            Mz1LeftBiasImonAdc,
            Mz2RightBiasImonAdc,
            Mz2LeftBiasImonAdc,
            Mz3LeftBiasImonAdc,
            Mz3RightBiasImonAdc,
            Mz4LeftBiasImonAdc,
            Mz4RightBiasImonAdc,
            MzVoaDemand,
            VoaXDac,
            MzXOuterRfTapAdc,
            MzYOuterRfTapAdc,
            Mz1CTapAdc,
            Mz2CTapAdc,
            Mz3CTapAdc,
            Mz4CTapAdc,
            MzXInlineTapBiasDac,
            //MzNegTapBiasDac, //Legacy no longer used
            MzXInlineTapAdc,
            MzYInlineTapBiasDac,
            MzYInlineTapAdc,
            DsdbrTecPlus,
            DsdbrTecMinus,
            LaserTecIMon,
            //LaserTecVMon, //not present in old driver?
            MzTecP,
            MzTecN,
            MzTecIMon,
            //MzTecVMon, , //not present in old driver?
            EtalonTecP,
            EtalonTecN,
            EtalonTecIMon,
            //EtalonTecVMon, , //not present in old driver?
            DsdbrSoaVirtualSubZeroDac,
            DsdbrSoaVirtualDac,
            FrontSoaVMon,
            FrontSoaVSubZeroMon,
            DsdbrGainDac,
            GbGainVMon,
            DsdbrRearDac,
            GbRearVMon,
            DsdbrPhaseDac,
            GbPhaseVMon,
            DsdbrRearSoaDac,
            GbRearSoaVMon,         
            GbFront4VMon,  
            GbFront3VMon, 
            GbFront2VMon,
            GbFront1VMon,    
            GbFront7VMon,           
            GbFront5VMon,     
            GbFront6VMon,     
            GbFront8VMon,
            DsdbrFrontEvenDac,
            DsdbrFrontOddDac,
            Mz1LeftImbDac,
            Mz1LeftImbVMon,
            Mz1RightImbDac,
            Mz1RightImbVMon,
            Mz1OuterImbDac,
            Mz1OuterImbVMon,
            Mz2LeftImbDac,
            Mz2LeftImbVMon,
            Mz2RightImbDac,
            Mz2RightImbVMon,
            Mz2OuterImbDac,
            Mz2OuterImbVMon,
            Mz3LeftImbDac,
            Mz3LeftImbVMon,
            Mz3RightImbDac,
            Mz3RightImbVMon,
            Mz3OuterImbDac,
            Mz3OuterImbVMon,
            Mz4LeftImbDac,
            Mz4LeftImbVMon,
            Mz4RightImbDac,
            Mz4RightImbVMon,
            Mz4OuterImbDac,
            Mz4OuterImbVMon,
            MzXPreSoaSubZeroDac,
            MzXPreSoaDac,
            MzXPreSoaVMonIAdc,
            MzXPreSoaVMonISubZeroAdc,
            MzXPostSoaSubZeroDac,
            MzXPostSoaDac,
            MzXPostSoaVMonIAdc,
            MzXPostSoaVMonISubZeroAdc,
            MzYPreSoaSubZeroDac,
            MzYPreSoaDac,
            MzYPreSoaVMonIAdc,
            MzYPreSoaVMonISubZeroAdc,
            MzYPostSoaSubZeroDac,
            MzYPostSoaDac,
            MzYPostSoaVMonIAdc,
            MzYPostSoaVMonISubZeroAdc,
            MzXPreSoaVmodeDac,
            MzXPreSoaIMonVAdc,
            MzXPostSoaVmodeDac,
            MzXPostSoaIMonVAdc,
            MzYPreSoaVmodeDac,
            MzYPreSoaIMonVAdc,
            MzYPostSoaVmodeDac,
            MzYPostSoaIMonVAdc,                    
            DsdbrAltEtalonMon, //same as DsdbrEtalonMon should be removed - feedback to calibration people
            DsdbrEtalonMon,
            DsdbrAltEtalonReflMon,//same as DsdbrEtalonReflMon should be removed - feedback to calibration people
            DsdbrEtalonReflMon,
            DsdbrAltPowerMon,//same as MzPolmuxMon should be removed - feedback to calibration people
            PostVoaPMon,                                                       
            M6V6CTapYMon,
            M6V6CTapXMon,
            DsdbrFrontSectionPairDac,
            UV_LED_Drive,
            //UVLEDTrig, //Moved to DE0_Commands as this enum should only contain channesl that used 16 bit integers                                         
  
            //Built-in test instruments
            Photodiode1,
            Photodiode2,
            Photodiode3,
            Photodiode4,
            Photodiode5,
            Photodiode6,
            PowerMeter1_Lo,      
            PowerMeter1_Hi,     
            PowerMeter2_Lo,       
            PowerMeter2_Hi,
            //PowerMeter3_Lo, //Not defined in driver
            //PowerMeter3_Hi,     
            //PowerMeter4_Lo,    
            //PowerMeter4_Hi,
            PowerMeter1,
            PowerMeter2,
            PowerMeter3,
            PowerMeter4                
                    

        }



        //Not accessable to user
        //these commands often have complex data strutures so we need to hide this from the users

        protected enum DE0_Commands
        {
            /// <summary>Invalid Command - Default state</summary>
            InvalidCommand,
            AccessIoDevice,
            GroundDacs,
            UVLEDTrig,

            MzSoaSetVMode,
            MzSoaSetIMode,
          
            #region Device Info.

            NvTestBrdType,
            NvTestBrdSerialNumber,         
            MfcFwVersion,          
            MfcFirmwareBuilt,    
            FpgaHwVersion0,
            FpgaHwVersion1,
            FpgaHwVersion2,
            FpgaHwVersion3,
            FpgaHwVersion4,
            FpgaHwVersionNumber,          
            SerialNumberEvalBoard,       
            DeviceSelVerNbr,       
            DeviceSelMfcBuildNbr,     
            DeviceSelHardwareVersions,        
            DeviceSelEchoMessage,         
            DeviceDioVerNbr,         
            SweepVersionNumber,            
            DsdbrVersionNumber,           
            PowerMeterVersionNumber,          
            WaveMeterVersionNumber,

            #endregion              

            #region Temperature Controls.

            DsdbrTempSetPoint,          
            DsdbrTempControl,     
            DsdbrTempPropGain,        
            DsdbrTempIntegralGain,       
            DsdbrTempDiffGain,      
            DsdbrTemperature,              
            MzTempSetPoint,                             
            MzTempControl,      
            MzTempPropGain,      
            MzTempIntegralGain,       
            MzTempDiffGain,      
            MzTemperature,     
            EtalonTempSetPoint,               
            EtalonTempControl,           
            EtalonTempPropGain,          
            EtalonTempIntegralGain,           
            EtalonTempDiffGain,             
            EtalonTemperature, 
           
            #endregion

            #region Sweep Settings

          
            SweepState,         
            SweepSrcConfig,        
            SweepSensConfig,   
            SweepPts,     
            SweepSrcMeasDelay,     
            SweepEnable,   
            SweepDisableAll,    
            SweepFetchData,    
            SweepSrcNonLinConfig,           
            SweepSrcNonLinSetPts,           
            SweepClearEntry,            
            SweepClearAllEntries,
            /// <summary>Enable and configure the sweep trigger output
            /// signal line. When enabled, a trigger pulse will be 
            /// output on the trigger signal line at the beginning of 
            /// every read cycle during a sweep. The polarity of the 
            /// pulse signal can be configured as either negative or 
            /// positive going; i.e. trigger line idles low and pulses
            /// high (DAQ_SWP_TRIG_POL = 0) or trigger line idles high
            /// and pulses low (DAQ_SWP_TRIG_POL = 1.) 3.3V Pin 39 of 
            /// the DE2 expansion board connector J2                    Write  : Len = 0x0003, Payload = 0x40</summary>
            //SweepExtTriggerOut1,

            #endregion        
      
            #region Internal Instruments.
       
            PowerMeter1CalTable,         
            PowerMeter2CalTable,         
            PowerMeter3CalTable,    
            PowerMeter4CalTable,          
            PowerMeter1Read,       
            PowerMeter2Read,        
            PowerMeter3Read,                     
            PowerMeter4Read,            

            #endregion           

     


        }



        public enum DE0_SubDeviceAccessId : byte
        {
            /// <summary>default - no sub region</summary>
            NO_SUB_ROUTE = 0x00,
            /// <summary>DeviceSelector sub region EEPROM</summary>
            DEVSEL_CMD_E0_CAL_PARAM = 0xE0
        }

        /// <summary>
        /// Defines the routing through the device selector
        /// </summary>      
        public enum DE0_DeviceAccessId
        {
            /// <summary>Invalid Selection - Default state</summary>
            InvalidDeviceId = 0x00,
            /// <summary>Command router</summary>
            DeviceSelector = 0x01,
            /// <summary>IO Access ( Dac's, Adc's, etc )</summary>
            DioAccess = 0x02,
            /// <summary>Temperature and other controls</summary>
            TemperatureControl = 0x03,
            /// <summary>Data Acquistion (sweeps ) configuration</summary>
            SweepControl = 0x04,
            /// <summary>Dsdbr access</summary>
            DsdbrControl = 0x06,
            /// <summary>Power meter controller access. Upto 16 instruments</summary>
            PowerMtrControl = 0x10,
            /// <summary>Wave meter controller access. Upto 16 instruments</summary>
            WaveMtrControl = 0x14,
            /// <summary>Top Gen Memory Access</summary>
            TopGenMemAccess = 0x1E,
            /// <summary>Broadcast Channel - Debug</summary>
            BroadcastChannel = 0x1F
        }

        /// <summary>
        /// Defines the number of bytes used for simple numerical Tx/Rx commands
        /// </summary> 
        public enum DE0_NumericalDataBytesLength : ushort
        {        
            NONE_OR_CUSTOM = 0,
            BYTE_1 = 1,
            BYTE_2 = 2,
            BYTE_3 = 3,
            BYTE_4 = 4,
            
        }


        public class DE0_RegisterItem
        {

            //Use for complex commands and non-AIO functions
            public DE0_RegisterItem(DE0_DeviceAccessId routing, DE0_SubDeviceAccessId subrouting, UInt16 Command, DE0_NumericalDataBytesLength NumericalDataBytesLength, double FloatConversionFactor)
            {

                this.routing = routing;
                this.subrouting = subrouting;
                this.command = Command;
                this.txdatabyteslength = (ushort)NumericalDataBytesLength;               
                this.floatconversionFactor = FloatConversionFactor;         
            

            }

            // Use for AIO addresses 
            public DE0_RegisterItem(UInt16 AIO_Address)
            {


                this.routing = DE0_DeviceAccessId.DioAccess;
                this.subrouting = DE0_SubDeviceAccessId.NO_SUB_ROUTE;
                this.command = AIO_Address;
                this.txdatabyteslength = 2;            
                this.floatconversionFactor = 1.0;            


            }

            /// <summary>Command routing code</summary>
            public DE0_DeviceAccessId Routing
            {
                get { return this.routing; }
            }

            /// <summary>Command sub routing code</summary>
            public DE0_SubDeviceAccessId SubRouting
            {
                get { return this.subrouting; }
            }


             /// <summary> Command identifer code</summary>
            public UInt16 Command
             {
                 get { return this.command; }
             }
            
            /// <summary>Total number of bytes expected to be transmitted</summary>
            public UInt16 TxDataBytesLength
            {
                get { return this.txdatabyteslength; }
            }      

            /// <summary>Register contents conversion factor</summary>
            public double FloatConversionFactor
            {
                get { return (floatconversionFactor); }
            }             
            
            /// <summary>MSA command, or other command such as 0xF1, 0xF2, etc</summary>
            private readonly DE0_DeviceAccessId routing;
            /// <summary>Sub route into a device</summary>
            private readonly DE0_SubDeviceAccessId subrouting;
            /// <summary>MPCB write command identifer</summary>
            private readonly UInt16 command;
            /// <summary>Total number of data bytes to transmit</summary>
            private readonly UInt16 txdatabyteslength;
            /// <summary>multiplier requried to convert from float to counts</summary>           
            private readonly double floatconversionFactor;
       

        }


        private Dictionary<string, DE0_RegisterItem> MemoryMap;



        public DE0_RegisterItem GetRegisterInfo(string ChannelName)
        {
            if (!MemoryMap.ContainsKey(ChannelName))
                throw new InstrumentException("MemoryMap does not contain an entry for " + ChannelName);

            return MemoryMap[ChannelName];

        }

        public DE0_RegisterItem GetRegisterInfo(DIO_Channels DIO_Channel)
        {
            return GetRegisterInfo(DIO_Channel.ToString());

        }


        protected DE0_RegisterItem GetRegisterInfo(DE0_Commands DE0_Command)
        {
            return GetRegisterInfo(DE0_Command.ToString());

        }

        private void Init_MemoryMap()
        {


            MemoryMap = new Dictionary<string, DE0_RegisterItem>();

            //***********************
            // LASER
            //***********************

            //Laser Front SOA is connected to MzXPreSOA channel 0x6069
            //DsdbrSoaVirtualDac
            //MemoryMap.Add(DIO_Channels.DsdbrSoaVirtualDac.ToString(), new DE0_RegisterItem(0x604E));
            MemoryMap.Add(DIO_Channels.DsdbrSoaVirtualDac.ToString(), new DE0_RegisterItem(0x6069));
            //DsdbrSoaVirtualSubZeroDac
            //MemoryMap.Add(DIO_Channels.DsdbrSoaVirtualSubZeroDac.ToString(), new DE0_RegisterItem(0x604E));
            MemoryMap.Add(DIO_Channels.DsdbrSoaVirtualSubZeroDac.ToString(), new DE0_RegisterItem(0x6069));



            //DsdbrGainDac
            MemoryMap.Add(DIO_Channels.DsdbrGainDac.ToString(), new DE0_RegisterItem(0x6047));
            //DsdbrFrontEvenDac
            MemoryMap.Add(DIO_Channels.DsdbrFrontEvenDac.ToString(), new DE0_RegisterItem(0x6044));
            //DsdbrFrontOddDac
            MemoryMap.Add(DIO_Channels.DsdbrFrontOddDac.ToString(), new DE0_RegisterItem(0x6043));
           
            //Laser Rear is connected to Laser Front SOA
            //DsdbrRearDac
            //MemoryMap.Add(DIO_Channels.DsdbrRearDac.ToString(), new DE0_RegisterItem(0x6049));
            MemoryMap.Add(DIO_Channels.DsdbrRearDac.ToString(), new DE0_RegisterItem(0x604E));
            
            //DsdbrRearSoaDac
            MemoryMap.Add(DIO_Channels.DsdbrRearSoaDac.ToString(), new DE0_RegisterItem(0x6046));
            //DsdbrFrontSectionPairDac
            MemoryMap.Add(DIO_Channels.DsdbrFrontSectionPairDac.ToString(), new DE0_RegisterItem(0x604F));
            
            //Laser Phase is connected to Laser Rear
            //DsdbrPhaseDac
            MemoryMap.Add(DIO_Channels.DsdbrPhaseDac.ToString(), new DE0_RegisterItem(0x6049));


            //GbFront1VMon
            MemoryMap.Add(DIO_Channels.GbFront1VMon.ToString(), new DE0_RegisterItem(0x802D));
            //GbFront2VMon
            MemoryMap.Add(DIO_Channels.GbFront2VMon.ToString(), new DE0_RegisterItem(0x802E));
            //GbFront3VMon
            MemoryMap.Add(DIO_Channels.GbFront3VMon.ToString(), new DE0_RegisterItem(0x802F));
            //GbFront4VMon
            MemoryMap.Add(DIO_Channels.GbFront4VMon.ToString(), new DE0_RegisterItem(0x8030));
            //GbFront5VMon
            MemoryMap.Add(DIO_Channels.GbFront5VMon.ToString(), new DE0_RegisterItem(0x8031));
            //GbFront6VMon
            MemoryMap.Add(DIO_Channels.GbFront6VMon.ToString(), new DE0_RegisterItem(0x8032));
            //GbFront7VMon
            MemoryMap.Add(DIO_Channels.GbFront7VMon.ToString(), new DE0_RegisterItem(0x8033));
            //GbFront8VMon
            MemoryMap.Add(DIO_Channels.GbFront8VMon.ToString(), new DE0_RegisterItem(0x8034));


            //GbGainVMon
            MemoryMap.Add(DIO_Channels.GbRearSoaVMon.ToString(), new DE0_RegisterItem(0x8036));
            //GbGainVMon
            MemoryMap.Add(DIO_Channels.GbGainVMon.ToString(), new DE0_RegisterItem(0x8037));
         
            //GbRearVMon
            //MemoryMap.Add(DIO_Channels.GbRearVMon.ToString(), new DE0_RegisterItem(0x8039));
             MemoryMap.Add(DIO_Channels.GbRearVMon.ToString(), new DE0_RegisterItem(0x8038));

             //GbPhaseVMon            
             MemoryMap.Add(DIO_Channels.GbPhaseVMon.ToString(), new DE0_RegisterItem(0x8039));

            //Laser Front SOA is connected to MzXPreSOA channel 0x805E
            //FrontSoaVSubZeroMon
            //FrontSoaVMon
            //MemoryMap.Add(DIO_Channels.FrontSoaVMon.ToString(), new DE0_RegisterItem(0x8038));
            MemoryMap.Add(DIO_Channels.FrontSoaVMon.ToString(), new DE0_RegisterItem(0x805E));
            //MemoryMap.Add(DIO_Channels.FrontSoaVSubZeroMon.ToString(), new DE0_RegisterItem(0x8038));
            MemoryMap.Add(DIO_Channels.FrontSoaVSubZeroMon.ToString(), new DE0_RegisterItem(0x805E));


            //***********************
            // MZ IMB
            //***********************
            //Mz1LeftImbDac
            MemoryMap.Add(DIO_Channels.Mz1LeftImbDac.ToString(), new DE0_RegisterItem(0x6009));
            //Mz1RightImbDac
            MemoryMap.Add(DIO_Channels.Mz1RightImbDac.ToString(), new DE0_RegisterItem(0x600A));
            //Mz1OuterImbDac
            MemoryMap.Add(DIO_Channels.Mz1OuterImbDac.ToString(), new DE0_RegisterItem(0x600B));
            //Mz2OuterImbDac
            MemoryMap.Add(DIO_Channels.Mz2OuterImbDac.ToString(), new DE0_RegisterItem(0x600C));
            //Mz2LeftImbDac
            MemoryMap.Add(DIO_Channels.Mz2LeftImbDac.ToString(), new DE0_RegisterItem(0x600D));
            //Mz2RightImbDac
            MemoryMap.Add(DIO_Channels.Mz2RightImbDac.ToString(), new DE0_RegisterItem(0x600E));
            //Mz3LeftImbDac
            MemoryMap.Add(DIO_Channels.Mz3LeftImbDac.ToString(), new DE0_RegisterItem(0x600F));
            //Mz3RightImbDac
            MemoryMap.Add(DIO_Channels.Mz3RightImbDac.ToString(), new DE0_RegisterItem(0x6010));
            //Mz3OuterImbDac
            MemoryMap.Add(DIO_Channels.Mz3OuterImbDac.ToString(), new DE0_RegisterItem(0x6012));           
            //Mz4OuterImbDac
            MemoryMap.Add(DIO_Channels.Mz4OuterImbDac.ToString(), new DE0_RegisterItem(0x6013));
            //Mz4LeftImbDac
            MemoryMap.Add(DIO_Channels.Mz4LeftImbDac.ToString(), new DE0_RegisterItem(0x6014));
            //Mz4RightImbDac
            MemoryMap.Add(DIO_Channels.Mz4RightImbDac.ToString(), new DE0_RegisterItem(0x6015));

            //Mz1RightImbVMon,0x803F
            MemoryMap.Add(DIO_Channels.Mz1RightImbVMon.ToString(), new DE0_RegisterItem(0x803F));
            //Mz1LeftImbVMon,0x8040
            MemoryMap.Add(DIO_Channels.Mz1LeftImbVMon.ToString(), new DE0_RegisterItem(0x8040));
            //Mz1OuterImbVMon,0x8041
            MemoryMap.Add(DIO_Channels.Mz1OuterImbVMon.ToString(), new DE0_RegisterItem(0x8041));

            //Mz2RightImbVMon,0x8042
            MemoryMap.Add(DIO_Channels.Mz2RightImbVMon.ToString(), new DE0_RegisterItem(0x8042));
            //Mz2LeftImbVMon,0x8043
            MemoryMap.Add(DIO_Channels.Mz2LeftImbVMon.ToString(), new DE0_RegisterItem(0x8043));
            //Mz2OuterImbVMon,0x8044
            MemoryMap.Add(DIO_Channels.Mz2OuterImbVMon.ToString(), new DE0_RegisterItem(0x8044));

            //Mz3RightImbVMon,0x8045
            MemoryMap.Add(DIO_Channels.Mz3RightImbVMon.ToString(), new DE0_RegisterItem(0x8045));
            //Mz3LeftImbVMon,0x8046
            MemoryMap.Add(DIO_Channels.Mz3LeftImbVMon.ToString(), new DE0_RegisterItem(0x8046));
            //Mz3OuterImbVMon,0x8047
            MemoryMap.Add(DIO_Channels.Mz3OuterImbVMon.ToString(), new DE0_RegisterItem(0x8048));

            //Mz4RightImbVMon,0x8048
            MemoryMap.Add(DIO_Channels.Mz4RightImbVMon.ToString(), new DE0_RegisterItem(0x8049));
            //Mz4LeftImbVMon,0x8049
            MemoryMap.Add(DIO_Channels.Mz4LeftImbVMon.ToString(), new DE0_RegisterItem(0x804A));
            //Mz4OuterImbVMon,0x804A
            MemoryMap.Add(DIO_Channels.Mz4OuterImbVMon.ToString(), new DE0_RegisterItem(0x804B));


            //************************
            // MZ BIAS
            //************************ 
            
            //Mz1LeftBiasDac
            MemoryMap.Add(DIO_Channels.Mz1LeftBiasDac.ToString(), new DE0_RegisterItem(0x6024));
            //Mz1RightBiasDac
            MemoryMap.Add(DIO_Channels.Mz1RightBiasDac.ToString(), new DE0_RegisterItem(0x6025));          
            //Mz2RightBiasDac
            MemoryMap.Add(DIO_Channels.Mz2RightBiasDac.ToString(), new DE0_RegisterItem(0x6026));
            //Mz2LeftBiasDac
            MemoryMap.Add(DIO_Channels.Mz2LeftBiasDac.ToString(), new DE0_RegisterItem(0x6027));
            //Mz3LeftBiasDac
            MemoryMap.Add(DIO_Channels.Mz3LeftBiasDac.ToString(), new DE0_RegisterItem(0x6028));
            //Mz3RightBiasDac
            MemoryMap.Add(DIO_Channels.Mz3RightBiasDac.ToString(), new DE0_RegisterItem(0x6029));
            //Mz4LeftBiasDac
            MemoryMap.Add(DIO_Channels.Mz4LeftBiasDac.ToString(), new DE0_RegisterItem(0x602A));
            //Mz4RightBiasDac
            MemoryMap.Add(DIO_Channels.Mz4RightBiasDac.ToString(), new DE0_RegisterItem(0x602B));

            //Mz1LeftBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz1LeftBiasImonAdc.ToString(), new DE0_RegisterItem(0x801B));
            //Mz1RightBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz1RightBiasImonAdc.ToString(), new DE0_RegisterItem(0x801C));
            //Mz2LeftBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz2LeftBiasImonAdc.ToString(), new DE0_RegisterItem(0x801D));               
            //Mz2RightBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz2RightBiasImonAdc.ToString(), new DE0_RegisterItem(0x801E));
            //Mz3LeftBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz3LeftBiasImonAdc.ToString(), new DE0_RegisterItem(0x801F));
            //Mz3RightBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz3RightBiasImonAdc.ToString(), new DE0_RegisterItem(0x8020));
            //Mz4LeftBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz4LeftBiasImonAdc.ToString(), new DE0_RegisterItem(0x8021));                      
            //Mz4RightBiasImonAdc.
            MemoryMap.Add(DIO_Channels.Mz4RightBiasImonAdc.ToString(), new DE0_RegisterItem(0x8022));
       
                    
       

            //*************************
            // MZ TAPS
            //*************************   

            //MzXInlineTapBiasDac
            MemoryMap.Add(DIO_Channels.MzXInlineTapBiasDac.ToString(), new DE0_RegisterItem(0x6016));
            //MzYInlineTapBiasDac
            MemoryMap.Add(DIO_Channels.MzYInlineTapBiasDac.ToString(), new DE0_RegisterItem(0x6017));         
            //MzXInlineTapAdc
            MemoryMap.Add(DIO_Channels.MzXInlineTapAdc.ToString(), new DE0_RegisterItem(0x8012));
            //MzYInlineTapAdc
            MemoryMap.Add(DIO_Channels.MzYInlineTapAdc.ToString(), new DE0_RegisterItem(0x8013));
            //Mz1CTapAdc
            MemoryMap.Add(DIO_Channels.Mz1CTapAdc.ToString(), new DE0_RegisterItem(0x800D));
            //Mz2CTapAdc
            MemoryMap.Add(DIO_Channels.Mz2CTapAdc.ToString(), new DE0_RegisterItem(0x800E));
            //Mz3CTapAdc
            MemoryMap.Add(DIO_Channels.Mz3CTapAdc.ToString(), new DE0_RegisterItem(0x800F));
            //Mz4CTapAdc
            MemoryMap.Add(DIO_Channels.Mz4CTapAdc.ToString(), new DE0_RegisterItem(0x8010));
            //MzXOuterRfTapAdc
            MemoryMap.Add(DIO_Channels.MzXOuterRfTapAdc.ToString(), new DE0_RegisterItem(0x8029));
            //MzYOuterRfTapAdc
            MemoryMap.Add(DIO_Channels.MzYOuterRfTapAdc.ToString(), new DE0_RegisterItem(0x802A));
            //M6V6CTapXMon
            MemoryMap.Add(DIO_Channels.M6V6CTapXMon.ToString(), new DE0_RegisterItem(0x8026));                         
            //M6V6CTapYMon
            MemoryMap.Add(DIO_Channels.M6V6CTapYMon.ToString(), new DE0_RegisterItem(0x8027));
            //DsdbrEtalonMon
            MemoryMap.Add(DIO_Channels.DsdbrEtalonMon.ToString(), new DE0_RegisterItem(0x8009));
            MemoryMap.Add(DIO_Channels.DsdbrAltEtalonMon.ToString(), new DE0_RegisterItem(0x8009)); //duplicate of DsdbrEtalonMon , here for legacy. should remove
            //DsdbrEtalonReflMon
            MemoryMap.Add(DIO_Channels.DsdbrEtalonReflMon.ToString(), new DE0_RegisterItem(0x800A));
            MemoryMap.Add(DIO_Channels.DsdbrAltEtalonReflMon.ToString(), new DE0_RegisterItem(0x800A));//duplicate of DsdbrEtalonReflMon , here for legacy. should removee
            //MzPolmuxMon
            MemoryMap.Add(DIO_Channels.PostVoaPMon.ToString(), new DE0_RegisterItem(0x800B));
            MemoryMap.Add(DIO_Channels.DsdbrAltPowerMon.ToString(), new DE0_RegisterItem(0x800B));   //duplicate of MzPolmuxMon , here for legacy. should remove
            
            //*************************
            // MZ SOAS  CURRENT MODE        
            //************************             
            
            //Channel Disconnected
            //MzXPreSoaDac
            //MemoryMap.Add(DIO_Channels.MzXPreSoaDac.ToString(), new DE0_RegisterItem(0x6069));
            //MemoryMap.Add(DIO_Channels.MzXPreSoaSubZeroDac.ToString(), new DE0_RegisterItem(0x6069));


            //MzXPostSoaDac
            MemoryMap.Add(DIO_Channels.MzXPostSoaDac.ToString(), new DE0_RegisterItem(0x6084));
            MemoryMap.Add(DIO_Channels.MzXPostSoaSubZeroDac.ToString(), new DE0_RegisterItem(0x6084));
            //MzYPreSoaDac
            MemoryMap.Add(DIO_Channels.MzYPreSoaDac.ToString(), new DE0_RegisterItem(0x609F));
            MemoryMap.Add(DIO_Channels.MzYPreSoaSubZeroDac.ToString(), new DE0_RegisterItem(0x609F));
            //MzYPostSoaDac
            MemoryMap.Add(DIO_Channels.MzYPostSoaDac.ToString(), new DE0_RegisterItem(0x60BA));
            MemoryMap.Add(DIO_Channels.MzYPostSoaSubZeroDac.ToString(), new DE0_RegisterItem(0x609F));

            //Channel Disconnected
            //MzXPreSoaIMonVAdc   
            //MemoryMap.Add(DIO_Channels.MzXPreSoaIMonVAdc.ToString(), new DE0_RegisterItem(0x805A));       
            //MzXPostSoaIMonVAdc
            MemoryMap.Add(DIO_Channels.MzXPostSoaIMonVAdc.ToString(), new DE0_RegisterItem(0x805B));
            //MzYPreSoaIMonVAdc
            MemoryMap.Add(DIO_Channels.MzYPreSoaIMonVAdc.ToString(), new DE0_RegisterItem(0x805C));
            //MzYPostSoaIMonVAdc
            MemoryMap.Add(DIO_Channels.MzYPostSoaIMonVAdc.ToString(), new DE0_RegisterItem(0x805D));

            //*************************
            // MZ SOAS  VOLTAGE MODE        
            //************************ 
                        
            //Channel Disconnected
            //MzXPreSoaVmodeDac
            //MemoryMap.Add(DIO_Channels.MzXPreSoaVmodeDac.ToString(), new DE0_RegisterItem(0x602D));
            
            //MzXPostSoaVmodeDac
            MemoryMap.Add(DIO_Channels.MzXPostSoaVmodeDac.ToString(), new DE0_RegisterItem(0x602E));
            //MzYPostSoaVmodeDac
            MemoryMap.Add(DIO_Channels.MzYPostSoaVmodeDac.ToString(), new DE0_RegisterItem(0x602F));
            //MzYPreSoaVmodeDac      
            MemoryMap.Add(DIO_Channels.MzYPreSoaVmodeDac.ToString(), new DE0_RegisterItem(0x6030));

            //Channel Disconnected
            //MzXPreSoaVMonIAdc   
            //MemoryMap.Add(DIO_Channels.MzXPreSoaVMonIAdc.ToString(), new DE0_RegisterItem(0x805E));
            //MemoryMap.Add(DIO_Channels.MzXPreSoaVMonISubZeroAdc.ToString(), new DE0_RegisterItem(0x805E));
           
            //MzXPostSoaVMonIAdc
            MemoryMap.Add(DIO_Channels.MzXPostSoaVMonIAdc.ToString(), new DE0_RegisterItem(0x805F));
            MemoryMap.Add(DIO_Channels.MzXPostSoaVMonISubZeroAdc.ToString(), new DE0_RegisterItem(0x805F));
            //MzYPreSoaVMonIAdc
            MemoryMap.Add(DIO_Channels.MzYPreSoaVMonIAdc.ToString(), new DE0_RegisterItem(0x8060));
            MemoryMap.Add(DIO_Channels.MzYPreSoaVMonISubZeroAdc.ToString(), new DE0_RegisterItem(0x8060));
            //MzYPostSoaVMonIAdc
            MemoryMap.Add(DIO_Channels.MzYPostSoaVMonIAdc.ToString(), new DE0_RegisterItem(0x8061));
            MemoryMap.Add(DIO_Channels.MzYPostSoaVMonISubZeroAdc.ToString(), new DE0_RegisterItem(0x8061));
        

            //************************
            // TEMP DAC/ADC
            //************************ 
            //DsdbrTecPlus
            MemoryMap.Add(DIO_Channels.DsdbrTecPlus.ToString(), new DE0_RegisterItem(0x601B));
            //DsdbrTecMinus
            MemoryMap.Add(DIO_Channels.DsdbrTecMinus.ToString(), new DE0_RegisterItem(0x601C));
           //MzTecP
            MemoryMap.Add(DIO_Channels.MzTecP.ToString(), new DE0_RegisterItem(0x601D));
            //MzTecN
            MemoryMap.Add(DIO_Channels.MzTecN.ToString(), new DE0_RegisterItem(0x601E));
            //EtalonTecP
            MemoryMap.Add(DIO_Channels.EtalonTecP.ToString(), new DE0_RegisterItem(0x601F));
            //EtalonTecN
            MemoryMap.Add(DIO_Channels.EtalonTecN.ToString(), new DE0_RegisterItem(0x6020));
            
       
            //LaserTecIMon
            MemoryMap.Add(DIO_Channels.LaserTecIMon.ToString(), new DE0_RegisterItem(0x803A));
            //MzTecIMon
            MemoryMap.Add(DIO_Channels.MzTecIMon.ToString(), new DE0_RegisterItem(0x803B));
            //EtalonTecIMon
            MemoryMap.Add(DIO_Channels.EtalonTecIMon.ToString(), new DE0_RegisterItem(0x803C));

            //none of these are in the old driver?
            //LaserTecVMon    
            //LaserTecVMon
            //MzTecVMon
            //EtalonTecVMon

            //UV_LED_Drive
            MemoryMap.Add(DIO_Channels.UV_LED_Drive.ToString(), new DE0_RegisterItem(0x6031));         
            //VoaXDac
            MemoryMap.Add(DIO_Channels.VoaXDac.ToString(), new DE0_RegisterItem(0x6018));
            //MzVoaDemand
            MemoryMap.Add(DIO_Channels.MzVoaDemand.ToString(), new DE0_RegisterItem(0x6018));                       


            //************************
            // POWER METER DIO access - for use in sweeps etc , not calibrated as per electrical channel! so use direct adc/dac functions.
            //************************ 

            MemoryMap.Add(DIO_Channels.PowerMeter1_Lo.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x8200, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DIO_Channels.PowerMeter1_Hi.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x8201, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DIO_Channels.PowerMeter2_Lo.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x8202, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DIO_Channels.PowerMeter2_Hi.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x8203, DE0_NumericalDataBytesLength.BYTE_2, 1.0));

            MemoryMap.Add(DIO_Channels.PowerMeter1.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0xC000, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            MemoryMap.Add(DIO_Channels.PowerMeter2.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0xC001, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            MemoryMap.Add(DIO_Channels.PowerMeter3.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0xC002, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            MemoryMap.Add(DIO_Channels.PowerMeter4.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.DioAccess, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0xC003, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            
            //**********************
            // NONE AIO COMMANDS - These are not directly accessable to user.
            //**********************

            // return new MemoryAddress_MPCB(Mpcb_DeviceAccessId.TemperatureControl, 0x8E, 0x0001, 0x0001);
            MemoryMap.Add(DE0_Commands.GroundDacs.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x8E, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 0.0));

            //UVLEDTrig
            MemoryMap.Add(DE0_Commands.UVLEDTrig.ToString(), new DE0_RegisterItem(0x2021));

            MemoryMap.Add(DE0_Commands.MzSoaSetVMode.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x91, DE0_NumericalDataBytesLength.BYTE_1, 1.0));
            MemoryMap.Add(DE0_Commands.MzSoaSetIMode.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x90, DE0_NumericalDataBytesLength.BYTE_1, 1.0));

            MemoryMap.Add(DE0_Commands.DsdbrTempSetPoint.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x10, DE0_NumericalDataBytesLength.BYTE_3, 1000.0));
            MemoryMap.Add(DE0_Commands.DsdbrTempControl.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x11, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.DsdbrTempPropGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x12, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.DsdbrTempIntegralGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x13, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.DsdbrTempDiffGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x14, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.DsdbrTemperature.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x15, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1000.0));

            MemoryMap.Add(DE0_Commands.MzTempSetPoint.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x20, DE0_NumericalDataBytesLength.BYTE_3, 1000.0));
            MemoryMap.Add(DE0_Commands.MzTempControl.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x21,  DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.MzTempPropGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x22,  DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.MzTempIntegralGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE,  0x23, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.MzTempDiffGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x24,DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.MzTemperature.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x25,  DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1000.0));

            MemoryMap.Add(DE0_Commands.EtalonTempSetPoint.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x30,  DE0_NumericalDataBytesLength.BYTE_3,1000.0));
            MemoryMap.Add(DE0_Commands.EtalonTempControl.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x31, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.EtalonTempPropGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x32, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.EtalonTempIntegralGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE,  0x33,DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.EtalonTempDiffGain.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x34,  DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.EtalonTemperature.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.TemperatureControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x35, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1000.0));
            

            MemoryMap.Add(DE0_Commands.PowerMeter1CalTable.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x80,  DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.PowerMeter2CalTable.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x81, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.PowerMeter3CalTable.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x82, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.PowerMeter4CalTable.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x83, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));


            MemoryMap.Add(DE0_Commands.PowerMeter1Read.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x10, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            MemoryMap.Add(DE0_Commands.PowerMeter2Read.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x11, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            MemoryMap.Add(DE0_Commands.PowerMeter3Read.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x12, DE0_NumericalDataBytesLength.BYTE_2, 100.0));
            MemoryMap.Add(DE0_Commands.PowerMeter4Read.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.PowerMtrControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x13, DE0_NumericalDataBytesLength.BYTE_2, 100.0));


            MemoryMap.Add(DE0_Commands.SweepState.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x20,DE0_NumericalDataBytesLength.BYTE_1, 1.0));
            MemoryMap.Add(DE0_Commands.SweepSrcConfig.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x21, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepSensConfig.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x22,DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepPts.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x23, DE0_NumericalDataBytesLength.BYTE_2, 1.0));
            MemoryMap.Add(DE0_Commands.SweepSrcMeasDelay.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x24, DE0_NumericalDataBytesLength.BYTE_3, 1.0));
            MemoryMap.Add(DE0_Commands.SweepEnable.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x25, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepDisableAll.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x26, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepFetchData.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x27, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepSrcNonLinConfig.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x28, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepSrcNonLinSetPts.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x29, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepClearEntry.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x2E, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
            MemoryMap.Add(DE0_Commands.SweepClearAllEntries.ToString(), new DE0_RegisterItem(DE0_DeviceAccessId.SweepControl, DE0_SubDeviceAccessId.NO_SUB_ROUTE, 0x2F, DE0_NumericalDataBytesLength.NONE_OR_CUSTOM, 1.0));
       
        }
        


      


    }







}