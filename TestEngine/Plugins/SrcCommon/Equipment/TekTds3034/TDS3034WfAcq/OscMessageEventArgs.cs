using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Instruments
{
    public class OscWaveformDataEventArgs : EventArgs 
    {
        Inst_Tds3034.ClsWaveformData wfData;
        Inst_Tds3034.ClsWaveformDataScale scaleInfo;
        string dataFileName;

        public OscWaveformDataEventArgs(Inst_Tds3034.ClsWaveformData waveformData, string fileName)
        {
            wfData = waveformData;
            dataFileName = fileName;
        }

        

        public Inst_Tds3034.ClsWaveformData WaveformData
        {
            get
            {
               
                return wfData;
            }
        }

        
        public string DataFileName
        {
            get
            {
                return dataFileName;
            }
        }
    }

    public class OscChannelDisplayStatusEventArgs : EventArgs
    {
        bool isDisplay ;

        public OscChannelDisplayStatusEventArgs(bool displayStatus)
        {
            isDisplay = displayStatus;
        }

        public bool DisplayStatus
        {
            get
            {
                return isDisplay;
            }
        }
    }

    public class oscStartAcquisitionEventArgs : EventArgs
    { }

    public class oscAcqusitionCompletedEventArgs : EventArgs
    {
        bool isAquisitionCompleted;

        public oscAcqusitionCompletedEventArgs(bool aquisitionStatus)
        {
            isAquisitionCompleted = aquisitionStatus;
        }

        public bool AcquisitionSatus
        {
            get
            {
                return isAquisitionCompleted;
            }
        }
    }
}
