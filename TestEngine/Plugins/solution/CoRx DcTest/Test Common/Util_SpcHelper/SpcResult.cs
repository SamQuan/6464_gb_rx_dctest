﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Collections;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using Excel = Microsoft.Office.Interop.Excel;

namespace Bookham.TestLibrary.Utilities
{
    public class SpcExcel
    { 
        public static void Open(string excelFile, string resultFile)
        {
            Excel._Application application = null;
            Excel._Workbook workBook = null;
            int loop = 0;
            bool runOK = false;
            while (!runOK&& loop++ < 3)
            {
                try
                {
                    application = new Excel.Application();

                    application.DisplayAlerts = false;
                    workBook = application.Workbooks.Open(excelFile, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                        Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    while (!File.Exists(resultFile))
                    {
                        System.Threading.Thread.Sleep(1000);
                    }
                    runOK = true;
                }
                catch
                {

                }
                finally
                {
                    if (workBook != null)
                    {
                        workBook.Save();
                        workBook.Close();
                        Marshal.ReleaseComObject(workBook);
                        workBook = null;
                    }
                    if (application != null)
                    {
                        application.DisplayAlerts = true;
                        application.Quit();
                        Marshal.ReleaseComObject(application);
                        application = null;
                    }
                }
            }
        }
    }
    public enum Master
    { 
        CreateTime,
        TestStage,
        SerialNo,
        Schema,
        Node,
        TableName
    }
    public enum Record
    { 
        DateTimeString,
        SerialNo,
        Node,
        TestStatus
    }
    public enum TestStatus
    { 
        Pass,
        Fail,
        Warning,
        Nothing
    }
    public class SpcResult
    {
        private OleDbConnection conn = null;

        public SpcResult(string fileName)
        {
            string connString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"{0}\"", fileName);
            conn = new OleDbConnection(connString);
            try
            {
                conn.Open();
            }
            catch
            {
                if (conn != null)
                {
                    ReleaseObject();
                }
            }
        }
        ~SpcResult()
        {
            ReleaseObject();
        }
        private void ReleaseObject()
        {
            if (conn.State == ConnectionState.Open)
                conn.Close();
            Marshal.ReleaseComObject(conn);
            conn = null;
        }
        private string TypeMapping(string type)
        {
            string ret = default(string);
            switch (type.ToLower())
            {
                case "int":
                case "uint":
                case "uint32":
                case "short":
                case "long":
                case "uint16":
                case "uint64":
                    ret = "int";
                    break;
                case "double":
                case "float":
                    ret = "decimal";
                    break;
                case "string":
                case "char[]":
                    ret = "varchar(255)";
                    break;
                case "datetime":
                    ret = "date(yyyymmdd)";
                    break;
                default:
                    break;
            }
            return ret;
        }
        private OleDbDataReader GetDataReader(string sql)
        {
            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteReader();
        }
        private bool TableExists(string tableName)
        {
            bool retValue=false;
            DataTable table = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

            if (table.Rows.Count > 0)
            {
                foreach (DataRow row in table.Rows)
                {
                    string name = row["TABLE_NAME"].ToString();
                    if (name == tableName)
                    {
                        retValue = true;
                        break;
                    }
                }
            }
            return retValue;
        }
        private TestStatus QueryRecord( string serial, string time, int node)
        {
            string sql = string.Format("select {0} from {1} where {2}='{3}' and {4} like '{5}%' and  {6}= {7}",
                Record.TestStatus, node.ToString(), Record.SerialNo,serial, Record.DateTimeString,time, Record.Node,node);
            OleDbDataReader reader = GetDataReader(sql);

            if (!reader.Read())
                return TestStatus.Nothing;
            else
                return (TestStatus)Enum.Parse(typeof(TestStatus), reader.GetString(0), true);
        }
        private bool QueryMaster(string serial, string stage, string node, string schema)
        {
            string sql = string.Format("select * from test_master where {0}='{1}' and {2}='{3}' and {4}={5} and {6}='{7}'",
                                                                Master.SerialNo,serial,
                                                                Master.TestStage,stage,
                                                                Master.Node,Convert.ToInt32(node),
                                                                Master.Schema,schema);

            OleDbDataReader reader = GetDataReader(sql);

            if (!reader.Read())
                return false;

            return true;
        }
        //CREATE TABLE 表名称(列名称1 数据类型,列名称2 数据类型,列名称3 数据类型,....)
        private int CreateMasterTable()
        {
            string sql = string.Format("create table test_master ([{0}] {1}, [{2}] {3}, [{4}] {5}, [{6}] {7}, [{8}] {9}, [{10}] {11})",
                                Master.CreateTime, TypeMapping("string"),
                                Master.SerialNo, TypeMapping("string"),
                                Master.TestStage, TypeMapping("string"),
                                Master.Node, TypeMapping("int"),
                                Master.Schema, TypeMapping("string"),
                                Master.TableName, TypeMapping("string"));
            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }
        private int CreateRecordTable(string tableName)
        {
            string sql = string.Format("create table {8} ([{0}] {1}, [{2}] {3}, [{4}] {5}, [{6}] {7})",
                             Record.DateTimeString, TypeMapping("string"),
                             Record.SerialNo,TypeMapping( "string"),
                             Record.Node, TypeMapping("int"),
                             Record.TestStatus, TypeMapping("string"),
                             tableName);
            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }
        //INSERT INTO table_name (c1, c2,...) VALUES (v1, v2,....)
        private int InsertMaster( Dictionary<Master, string> what)
        {
            string sql = string.Format("insert into {0} values ('{1}','{2}','{3}',{4},'{5}','{6}')","test_master",
                                        what[Master.CreateTime],
                                        what[ Master.SerialNo],
                                        what[Master.TestStage],
                                        Convert.ToInt32(what[Master.Node]),
                                        what[Master.Schema],
                                        what[Master.TableName]);

            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }
        private int InsertMaster(string serial, string stage, string node, string schema)
        {
            string sql = string.Format("insert into test_master values('{0}','{1}','{2}',{3},'{4}','{5}')",
                                                              DateTime.Now.ToString("yyyyMMddhhmmss"),
                                                              serial,
                                                              stage,
                                                              Convert.ToInt32(node),
                                                              schema,
                                                              node);

            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }
        private int InsertRecord(Dictionary<Record, string> what)
        { 
             string sql = string.Format("insert into {0} values ('{1}','{2}',{3},'{4}')",what[Record.Node],
                                        what[Record.DateTimeString],
                                        what[Record.SerialNo],
                                        Convert.ToInt32(what[Record.Node]),
                                        what[Record.TestStatus]);

             OleDbCommand cmd = new OleDbCommand(sql, conn);

             return cmd.ExecuteNonQuery();
        }
        private int InsertRecord(string time, string serial, string node, string status)
        {
            string sql = string.Format("insert into {0} values ('{1}','{2}',{3},'{4}')", node,
                                      time,
                                      serial,
                                      Convert.ToInt32(node),
                                      status);

            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }

        //UPDATE t SET c = v WHERE c = v
        private int UpdateRecord(Dictionary<Record, string> what)
        {
            string sql = string.Format("update {0} set {1}='{2}' where {3}='{4}' and {5}='{6}' and {7}={8}",
                                                            what[Record.Node],
                                                            Record.TestStatus,what[Record.TestStatus],
                                                            Record.SerialNo,what[Record.SerialNo],
                                                            Record.DateTimeString,what[Record.DateTimeString],
                                                            Record.Node,what[Record.Node]);

            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }
        private int UpdateRecord(string node, string serial, string time, string status)
        {
            string sql = string.Format("update {0} set {1}='{2}' where {3}='{4}' and {5}='{6}' and {7}={8}",
                                                            node,
                                                            Record.TestStatus, status,
                                                            Record.SerialNo, node,
                                                            Record.DateTimeString, time,
                                                            Record.Node, node);

            OleDbCommand cmd = new OleDbCommand(sql, conn);

            return cmd.ExecuteNonQuery();
        }

        private TestStatus QueryRecord(Dictionary<Record, string> cond)
        {
            string sql = string.Format("select TEST_STATUS from {0} where {1} like '{2}%' and {3}='{4}' and {5}={6}", cond[Record.Node],
                                        Record.DateTimeString,cond[Record.DateTimeString],
                                        Record.SerialNo,cond[Record.SerialNo],
                                        Record.Node,Convert.ToInt32(cond[Record.Node]));

            sql = string.Format("seect TEST_STATUS from {0} where {1} = (select max({1}) from {0} where {1} like '{2}%' and {3}='{4}' and {5}={6})",
                                                    cond[Record.Node],Record.DateTimeString,cond[Record.DateTimeString],
                                                    Record.SerialNo, cond[Record.SerialNo],
                                                    Record.Node, Convert.ToInt32(cond[Record.Node]));

            OleDbDataReader reader = GetDataReader(sql);
            if (!reader.Read()) return TestStatus.Nothing;

            return (TestStatus)Enum.Parse(typeof(TestStatus), reader.GetString(0), true);
        }

        private void ReadTxtResult(string file, out TestStatus status, out string timeSpan, out string serial, out string schema, out string stage, out string node)
        {
            status = TestStatus.Nothing;
            timeSpan = serial = schema = stage = node = default(string);

            using (StreamReader sr=new StreamReader (file))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    /*
                     * X:
                     * TimeSpan:20160122065600
                     * SerialNo:HG108980.001
                     * Schema:HIBERDB
                     * Node:1592
                     * TestStage:hitt_spc
                     * */
                    if (line.StartsWith("X"))
                    {
                        if (line.ToLower().Contains("fail"))
                            status = TestStatus.Fail;
                        else if (line.ToLower().Contains("warning"))
                        {
                            if (status != TestStatus.Fail)
                                status = TestStatus.Warning;
                        }
                        else if (line.ToLower().Contains("pass"))
                        {
                            if (status != TestStatus.Fail && status != TestStatus.Warning)
                                status = TestStatus.Pass;
                        }
                        else
                        {
                            sr.Close();
                            throw new Exception("The line does not contains the valid information about test status!");
                        }
                    }
                    else if(!string.IsNullOrEmpty(line))
                    {
                        string temp = line.Split(new char[] { ':' })[1];
                        if (line.StartsWith("TimeSpan"))
                            timeSpan = temp;
                        else if (line.StartsWith("SerialNo"))
                            serial = temp;
                        else if (line.StartsWith("Node"))
                            node = temp;
                        else if (line.StartsWith("Schema"))
                            schema = temp;
                        else if (line.StartsWith("TestStage"))
                            stage = temp;
                        else
                        {
                            sr.Close();
                            throw new Exception("Formate Text File is Modified by someone!");
                        }
                    }
                }
            }
            try
            {
                File.Delete(file);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool Check(string serial, string stage, string node, string schema,string fileName,string dir)
        {
            if (conn == null) return true;
            else if (conn.State != ConnectionState.Open) return true;

            TestStatus txtstatus = TestStatus.Nothing;
            string txttimeSpan, txtserial, txtschema, txtstage, txtnode;

            if (!this.TableExists("test_master"))
                CreateMasterTable();
            if (!this.QueryMaster(serial, stage, node, schema))
                InsertMaster(serial, stage, node, schema);
            if (!this.TableExists(node))
                CreateRecordTable(node);
            TestStatus recordStatus=this.QueryRecord(serial,DateTime.Now.ToString("yyyyMMdd"),int.Parse(node));

            if (!dir.EndsWith("\\")) dir += "\\";

            string resultFile = string.Format("{0}Results{1}.txt", dir, node);

            if(recordStatus== TestStatus.Nothing)
            {
                if (!File.Exists(resultFile))
                    //open
                    SpcExcel.Open(string.Format("{0}{1}", dir, fileName), resultFile);

                ReadTxtResult(resultFile, out txtstatus, out txttimeSpan, out txtserial, out txtschema, out txtstage, out txtnode);

                if (!string.Equals(txttimeSpan.Substring(0, 8), DateTime.Now.ToString("yyyyMMdd")))
                    recordStatus = TestStatus.Nothing;
                else
                {
                    //Debug.Assert(string.Equals(serial, txtserial, StringComparison.OrdinalIgnoreCase) &&
                    //        string.Equals(stage, txtstage, StringComparison.OrdinalIgnoreCase) &&
                    //        string.Equals(node, txtnode, StringComparison.OrdinalIgnoreCase) &&
                    //        string.Equals(schema, txtschema, StringComparison.OrdinalIgnoreCase), "Parameters not coordinate!");

                    InsertRecord(txttimeSpan, txtserial, txtnode, txtstatus.ToString());

                    recordStatus = txtstatus;
                }
            }
            else if (recordStatus!= TestStatus.Pass)
            {
                if (File.Exists(resultFile))
                {
                    ReadTxtResult(resultFile, out txtstatus, out txttimeSpan, out txtserial, out txtschema, out txtstage, out txtnode);
                    //Debug.Assert(serial == txtserial && stage == txtstage && node == txtnode && schema == txtschema, "Parameters not coordinate!");

                    UpdateRecord(node, serial, txttimeSpan, txtstatus.ToString());

                    recordStatus = txtstatus;
                }
            }

            if (recordStatus == TestStatus.Pass || recordStatus == TestStatus.Warning)
                return true;
            return false;
        }
    }


}
