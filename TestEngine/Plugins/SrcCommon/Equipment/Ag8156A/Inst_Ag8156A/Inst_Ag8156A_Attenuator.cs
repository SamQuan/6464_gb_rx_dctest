// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ag8156A.cs
//
// Author: simon.ede, 2006
// Design: As specified in Ag8156 driver design document.
//
// ian.webb, 12Jun2007: modified OutputEnabled property as some instruments return +0 and +1.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 8156A optical attenuator driver
    /// </summary>
    public class Inst_Ag8156A_Attenuator : InstType_OpticalAttenuator
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag8156A_Attenuator(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Configure valid hardware information

            // Add all Ag8156A details
            InstrumentDataRecord Ag8156AInstData = new InstrumentDataRecord(
                "HEWLETT-PACKARD HP8156A",      // hardware name 
                "0",  			                // minimum valid firmware version 
                "1.05");			            // maximum valid firmware version 

            Ag8156AInstData.Add("MinWavelength_nm", "1200");    // minimum wavelength
            Ag8156AInstData.Add("MaxWavelength_nm", "1650");    // maximum wavelength
            Ag8156AInstData.Add("MaxAttenuation_dB", "60");     // maximum attenuation

            ValidHardwareData.Add("Ag8156A", Ag8156AInstData);

            // Configure valid chassis information
            // Add Chassis_Ag8156A chassis details
            InstrumentDataRecord Ag8156AChassisData = new InstrumentDataRecord(
                "Chassis_Ag8156A",				// chassis driver name  
                "0",							// minimum valid chassis driver version  
                "2.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag8156A", Ag8156AChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag8156A)base.InstrumentChassis;

            // Generate the common command stem that most commands use
            this.commandAttStem = "INP:ATT";
            this.commandWavStem = "INP:WAV";
            this.commandOutpStem = "OUTP:STAT";
            this.commandOffsStem = "INP:OFFS";

            // default to safe mode
            this.safeMode = true;

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the chassis.
            get
            {
                string ver = this.InstrumentChassis.FirmwareVersion;
                return ver;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware details.
            get
            {
                string id = this.instrumentChassis.HardwareIdentity;
                return id;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST, and a cls
            chassisWrite("*RST");
            chassisWrite("*CLS");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Optical Attenuator functions

        /// <summary>
        /// Set/Return attenuation level (in dB)
        /// </summary>
        public override double Attenuation_dB
        {
            get
            {
                // Read the current attenuation level (dB)
                string cmd = commandAttStem + "?";
                string resp = chassisQuery(cmd);

                double result = getDblFromString(cmd, resp);
                return result;
            }
            set
            {
                // Set the attenuation level (dB)
                string cmd = commandAttStem + (char)32 + value;
                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Set/Return wavelength (in nm)
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                // Read the current wavelength setting (nm)
                string cmd = commandWavStem + "?";
                string resp = chassisQuery(cmd);

                double result = getDblFromString(cmd, resp);
                return result;
            }
            set
            {
                // Set the wavelength (nm)
                string cmd = commandWavStem + (char)32 + value + "nm";
                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Set/Return output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Read the current output state
                bool result;
                string cmd = commandOutpStem + "?";
                string resp = chassisQuery(cmd);
                if (resp == "1" || resp == "+1")
                {
                    result = true;
                }
                else if (resp == "0" || resp == "+0")
                {
                    result = false;
                }
                else
                {
                    throw new InstrumentException("Invalid response to '" + cmd + "': " + resp);
                }
                return result;
            }
            set
            {
                //Set the output state
                string cmd;

                if (value)
                {
                    cmd = commandOutpStem + (char)32 + "ON";
                }
                else
                {
                    cmd = commandOutpStem + (char)32 + "OFF";
                }

                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Set/Return calibration factor (in dB)
        /// </summary>
        public override double CalibrationFactor_dB
        {
            get
            {
                // Read the current calibration factor (dB)
                string cmd = commandOffsStem + "?";
                string resp = chassisQuery(cmd);

                double result = getDblFromString(cmd, resp);
                return result;
            }
            set
            {
                // Set the calibration factor (dB)
                string cmd = commandOffsStem + (char)32 + value;
                chassisWrite(cmd);
            }
        }

        #endregion

        #region Ag8156A Optical Attenuator Specific functions

        /// <summary>
        /// SafeMode flag. If true, instrument will always wait for completion after every command and check
        /// the error registers. If false it will do neither.
        /// </summary>
        public bool SafeMode
        {
            get
            {
                return this.safeMode;
            }
            set
            {
                safeMode = value;
            }
        }
        #endregion

        #region Helper functions

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            if (safeMode)
            {
                this.instrumentChassis.Write(command, this);
            }
            else
            {
                // if not in safe mode commands are async and no error checking
                this.instrumentChassis.Write(command, this, true, false);
                //this.instrumentChassis.Write_Unchecked(command, null);
            }
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            if (safeMode)
            {
                return this.instrumentChassis.Query(command, this);
            }
            else
            {
                // if not in safe mode, don't check for errors
                return this.instrumentChassis.Query(command, this, false);
                //return this.instrumentChassis.Query_Unchecked(command, null);
            }
        }

        /// <summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// </summary>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag8156A instrumentChassis;

        private bool safeMode;

        // Common command stems for most commands
        private string commandAttStem;
        private string commandWavStem;
        private string commandOutpStem;
        private string commandOffsStem;



        #endregion
    }
}
