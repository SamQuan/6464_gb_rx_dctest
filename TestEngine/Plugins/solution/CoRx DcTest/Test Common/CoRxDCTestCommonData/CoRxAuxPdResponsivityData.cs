using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.CoRxDCTestCommonData
{
    /// <summary>
    /// Coupling Efficiency measured data,
    /// it includes inputed optical power,wavelen, current readings for 
    /// photodiodes for AUX_LN, AUX_LP, AUX_RN , AUX_RP, and tap
    /// </summary>
    public struct StcCouplingEffData
    {
        /// <summary>
        /// optical input power
        /// </summary>
        public double OpcPower_DBm;
        /// <summary>
        /// optical wanlen
        /// </summary>
        public double Wavelen_nm;
        /// <summary>
        /// 
        /// </summary>
        public double I_AUX_XR_mA;
        /// <summary>
        /// 
        /// </summary>
        public double I_AUX_XL_mA;
        /// <summary>
        /// 
        /// </summary>
        public double I_AUX_YR_mA;
        /// <summary>
        /// 
        /// </summary>
        public double I_AUX_YL_mA;
    }
    /// <summary>
    /// Class to process Co Rx auxilary pd responsivity data
    /// </summary>
    public class CoRxAuxPdResponsivityData
    {
        private readonly StcCouplingEffData TestData;
        /// <summary>
        /// auxilary pd coupling efficiency
        /// </summary>
        public readonly double CouplingEff;
        /// <summary>
        /// XR Auxilary PD  Responsivity 
        /// </summary>
        public readonly double Res_Aux_XR;
        /// <summary>
        /// XL Auxilary PD  Responsivity 
        /// </summary>
        public readonly double Res_Aux_XL;
        /// <summary>
        /// YR Auxilary PD  Responsivity 
        /// </summary>
        public readonly double Res_Aux_YR;
        /// <summary>
        /// YL Auxilary PD  Responsivity 
        /// </summary>
        public readonly double Res_Aux_YL;
                        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="opcPower_dBm"></param>
        /// <param name="wavelen_nm"></param>
        /// <param name="I_XR_A"></param>
        /// <param name="I_XL_A"></param>
        /// <param name="I_YR_A"></param>
        /// <param name="I_YL_A"></param>
        public CoRxAuxPdResponsivityData(double opcPower_dBm, double wavelen_nm,
                                double I_XR_A, double I_XL_A, double I_YR_A, double I_YL_A /*, double I_Tap_A*/)
        {
            if (opcPower_dBm == double.NegativeInfinity) // If no power input or low enough, set all data to 0
            {
                CouplingEff = 0;
                Res_Aux_XR = 0;
                Res_Aux_XL = 0;
                Res_Aux_YR = 0;
                Res_Aux_YL = 0;
                throw new Exception("Coupling Efficiecy Error: Test Coupling Efficiecy with no optical power input");
            }
            else
            {
                TestData = new StcCouplingEffData();
                TestData.OpcPower_DBm = opcPower_dBm;
                TestData.Wavelen_nm = wavelen_nm;
                TestData.I_AUX_XR_mA = 1000 * I_XR_A;
                TestData.I_AUX_XL_mA = 1000 * I_XL_A;
                TestData.I_AUX_YR_mA = 1000 * I_YR_A;
                TestData.I_AUX_YL_mA = 1000 *  I_YL_A;

                double power_mW = Alg_PowConvert_dB.Convert_dBmtomW(TestData.OpcPower_DBm);

                if (power_mW != 0)
                {
                    CouplingEff = (TestData.I_AUX_XR_mA + TestData.I_AUX_XL_mA +
                        TestData.I_AUX_YR_mA + TestData.I_AUX_YL_mA) / power_mW;
                    Res_Aux_XR = TestData.I_AUX_XR_mA / power_mW;
                    Res_Aux_XL = TestData.I_AUX_XL_mA / power_mW;
                    Res_Aux_YR = TestData.I_AUX_YR_mA / power_mW;
                    Res_Aux_YL = TestData.I_AUX_YL_mA / power_mW;
                }
                else
                    CouplingEff = 0;
            }
          
        }
                
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="measData"> <see cref="StcCouplingEffData"/></param>
        public CoRxAuxPdResponsivityData(StcCouplingEffData measData)
        {
            if (measData.OpcPower_DBm == double.NegativeInfinity)  // If no power input or low enough, set all data to 0
            {
                CouplingEff = 0;
                Res_Aux_XR = 0;
                Res_Aux_XL = 0;
                Res_Aux_YR = 0;
                Res_Aux_YL = 0;
                throw new Exception("Coupling Efficiecy Error: Test Coupling Efficiecy with no optical power input");
            }
            else
            {
                TestData = measData;
                double power_mW = Alg_PowConvert_dB.Convert_dBmtomW(TestData.OpcPower_DBm);

                if (power_mW != 0)
                {
                    CouplingEff = (TestData.I_AUX_XR_mA + TestData.I_AUX_XL_mA +
                        TestData.I_AUX_YR_mA + TestData.I_AUX_YL_mA) / power_mW;
                    Res_Aux_XR = TestData.I_AUX_XR_mA / power_mW;
                    Res_Aux_XL = TestData.I_AUX_XL_mA / power_mW;
                    Res_Aux_YR = TestData.I_AUX_YR_mA / power_mW;
                    Res_Aux_YL = TestData.I_AUX_YL_mA / power_mW;
                }
                else
                    CouplingEff = 0;
            } 
        }


        /// <summary>
        /// Return auxilary pd Responsivity member header in string ( without Tap)  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public static string GetPdRespStringHeader()
        {
            StringBuilder header = new StringBuilder();
            header.Append("Wavelength (nm),");

            header.Append("Responsivity Aux XL (A/W),");
            header.Append("Responsivity Aux XR (A/W),");
            header.Append("Responsivity Aux YL (A/W),");
            header.Append("Responsivity Aux YR (A/W),");
            return header.ToString();
        }

        /// <summary>
        /// Return auxilary pd responsivity value in string ( without Tap)  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public string GetPdRespString()
        {
            StringBuilder datastr = new StringBuilder();
            datastr.Append(TestData.Wavelen_nm);
            datastr.Append(",");
                        
            datastr.Append(Res_Aux_XL);
            datastr.Append(",");
            datastr.Append(Res_Aux_XR);
            datastr.Append(",");
            datastr.Append(Res_Aux_YL);
            datastr.Append(",");
            datastr.Append(Res_Aux_YR);
            datastr.Append(",");
            
            return datastr.ToString();
        }
        
        /// <summary>
        /// Return auxilary pd data member header in string ( without Tap)  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public static string GetAuxPdDataStringHeader()
        {
            StringBuilder header = new StringBuilder();
            header.Append("Wavelength (nm),");            
            header.Append("Power (dBm),");
            header.Append("Aux XL (mA),");
            header.Append("Aux XR (mA),");
            header.Append("Aux YL (mA),");
            header.Append("Aux YR (mA),");
            
            header.Append("Responsivity Aux XL (A/W),");
            header.Append("Responsivity Aux XR (A/W),");            
            header.Append("Responsivity Aux YL (A/W),");
            header.Append("Responsivity Aux YR (A/W),");
            header.Append("Coupling Efficiency (A/W),");
            return header.ToString();
        }
        /// <summary>
        /// Return auxilary pd data value in string ( without Tap)  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public string AuxPdDataToString()
        {
            StringBuilder datastr = new StringBuilder();
            datastr.Append(TestData.Wavelen_nm);
            datastr.Append(",");
            
            datastr.Append(TestData.OpcPower_DBm);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_XL_mA);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_XR_mA);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_YL_mA);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_YR_mA);
            datastr.Append(",");
            datastr.Append(Res_Aux_XL);
            datastr.Append(",");
            datastr.Append(Res_Aux_XR);
            datastr.Append(",");
            datastr.Append(Res_Aux_YL);
            datastr.Append(",");
            datastr.Append(Res_Aux_YR);
            datastr.Append(",");
            datastr.Append(CouplingEff);
            datastr.Append(",");
            return datastr.ToString();
           
        }

        /// <summary>
        /// Return all data meber header in string ( with Tap)  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public static string GetDataStringHeader()
        {
            StringBuilder header = new StringBuilder();
            header.Append("Wavelength (nm),");        
            header.Append("Power (dBm),");
            header.Append("Aux XL (mA),");
            header.Append("Aux XR (mA),");
            header.Append("Aux YL (mA),");
            header.Append("Aux YR (mA),");
           
            header.Append("Responsivity Aux XL (A/W),");
            header.Append("Responsivity Aux XR (A/W),");
            header.Append("Responsivity Aux YL (A/W),");
            header.Append("Responsivity Aux YR (A/W),");
            header.Append("Responsivity PD Tap1 (A/W,");
            header.Append("Coupling Efficiency (A/W),");

            return header.ToString();
        }
        /// <summary>
        /// Return all data value in string ( with Tap)  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public string DataToString()
        {
            StringBuilder datastr = new StringBuilder();
            datastr.Append(TestData.Wavelen_nm);
            datastr.Append(",");

            datastr.Append(TestData.OpcPower_DBm);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_XL_mA);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_XR_mA);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_YL_mA);
            datastr.Append(",");
            datastr.Append(TestData.I_AUX_YR_mA);
            datastr.Append(",");

            datastr.Append(Res_Aux_XL);
            datastr.Append(",");
            datastr.Append(Res_Aux_XR);
            datastr.Append(",");
            datastr.Append(Res_Aux_YL);
            datastr.Append(",");
            datastr.Append(Res_Aux_YR);
            datastr.Append(",");            
            datastr.Append(CouplingEff);
            
            return datastr.ToString();

        }
    }
}
