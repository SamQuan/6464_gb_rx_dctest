// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag816x_OpticalSwitch.cs
//
// Author: mark.fullalove, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// 816x chassis containing 8159x optical switch modules
    /// </summary>
    public class Inst_Ag816x_OpticalSwitch : InstType_SimpleSwitch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the switch module</param>
        /// <param name="subSlotInit">unused</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag816x_OpticalSwitch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
                "81591B",				        // hardware name 
                "0",  			                // minimum valid firmware version 
                "V4.50(37756)");			    // maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant1", instrVariant1);

            InstrumentDataRecord instrVariant2 = new InstrumentDataRecord(
                "81595B",				        // hardware name 
                "0",  			                // minimum valid firmware version 
                "V4.50(37756)");			    // maximum valid firmware version 
            ValidHardwareData.Add("InstrVariant2", instrVariant2);

            // Configure valid chassis information
            // Add 816x chassis details
            InstrumentDataRecord ag816xData = new InstrumentDataRecord(
                "Chassis_Ag816x",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag816x", ag816xData);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_Ag816x)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the SLOT IDN? string which has 4 comma seperated substrings                
                string command = String.Format(":SLOT{0}:IDN{1}?", this.Slot, this.SubSlot);
                string resp = instrumentChassis.Query_Unchecked(command, this);
                if (resp.Length == 0)
                {
                    string errStr = String.Format("No module available in '{0}' at slot {1}:{2}",
                        this.ChassisName, this.Slot, this.SubSlot);
                    throw new InstrumentException(errStr);
                }
                // Model number after 1st comma
                return resp.Split(',')[3];
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the SLOT IDN? string which has 4 comma seperated substrings                
                string command = String.Format(":SLOT{0}:IDN{1}?", this.Slot, this.SubSlot);
                string resp = instrumentChassis.Query_Unchecked(command, this);
                if (resp.Length == 0)
                {
                    string errStr = String.Format("No module available in '{0}' at slot {1}:{2}",
                        this.ChassisName, this.Slot, this.SubSlot);
                    throw new InstrumentException(errStr);
                }
                // Model number after 1st comma
                return resp.Split(',')[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write("*RST", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag816x instrumentChassis;
        #endregion

        /// <summary>
        /// Assumes a simple 1 x N switch
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                //Query the instrument to get the Switch Configuration

                // example: rout1:conf? -> A,B;1,2 (2 left and 2 right ports)

                /* response: <j>,<k>;<l>,<m> as text string where:
                    <j> is the first port character on the left
                    <k> is the last port character on the left
                    <l> is the minmimum port number on the right
                    <m> is the maximum port number on the right
                */

                string resp = instrumentChassis.Query( string.Format("ROUT{0}:CONF{1}?",this.Slot,this.SubSlot), this);
                string[] info = resp.Split(',');
                return Convert.ToInt32(info[2]);
            }
        }

        /// <summary>
        /// Assumes a simple 1 x N switch
        /// </summary>
        public override int MinimumSwitchState
        {
            get
            {
                //Query the instrument to get the Switch Configuration

                // example: rout1:conf? -> A,B;1,2 (2 left and 2 right ports)

                /* response: <j>,<k>;<l>,<m> as text string where:
                    <j> is the first port character on the left
                    <k> is the last port character on the left
                    <l> is the minmimum port number on the right
                    <m> is the maximum port number on the right
                */

                string resp = instrumentChassis.Query(string.Format("ROUT{0}:CONF{1}?", this.Slot, this.SubSlot), this);
                string[] info = resp.Split(';');
                return Convert.ToInt32(info[1].Remove(1,2));
            }
        }

        /// <summary>
        /// Assumes a simple 1 x N switch
        /// </summary>
        public override int SwitchState
        {
            get
            {
                string resp = instrumentChassis.Query(string.Format("ROUT{0}:CHAN{1}?", this.Slot, this.SubSlot), this);
                return Convert.ToInt32(resp);
            }
            set
            {
                //Validate switch position.
                if ((value >= this.MinimumSwitchState) && (value <= this.MaximumSwitchState))
                {
                    instrumentChassis.Write(string.Format("ROUT{0}:CHAN{1} A,{2}", this.Slot, this.SubSlot, value), this);
                }
                else
                {
                    throw new InstrumentException("Invalid switch Position Specified: " + value);
                }
            }
        }
    }
}
