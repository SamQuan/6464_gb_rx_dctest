// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// OpticalPowerCal.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class holding the optical power calibration data and 
    /// calculation methods for Tuneable modules.
    /// </summary>
    public class OpticalPowerCal
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="calTable">Calibration Table accessor</param>
        public OpticalPowerCal(CalTableConfigAccessor calTable)
        {
            this.calConfig = calTable;
            this.isValid = false;
            this.splitCal = null;
            this.dutSerialNumber = null;
        }

        
        /// <summary>
        /// Load Calibration from config file
        /// </summary>
        /// <param name="dutSerialNumber">Dut serial number</param>
        public void LoadConfig(string dutSerialNumber)
        {
            this.dutSerialNumber = dutSerialNumber;

            // load splitter calibration
            string splitCalFile = calConfig.GetCalibrationEntry<string>("SplitterCalFile");
            this.splitCal = OpticalOffsetVsFrequencyReader.LoadSplitterCal(splitCalFile);
            
            // load reference power meter calibration factors
            string powerMeterRefCalFile = calConfig.GetCalibrationEntry<string>("PowerMeterRefCalFile");
            this.powerMeterReferenceCal = OpticalOffsetVsFrequencyReader.LoadSplitterCal(powerMeterRefCalFile);
            
            // load system calibration
            bool calValid;
            double systemOffset_dB = calConfig.GetCalibrationEntry<double>
                ("SystemOffset_dB", out calValid);
            double systemOffsetAtFreqGHz = calConfig.GetCalibrationEntry<double>
                            ("SystemOffsetAtFreqGHz", out calValid);
            string systemOffsetSerialNumber = calConfig.GetCalibrationEntry<string>
                            ("SystemOffsetSerialNumber", out calValid);
            if ((systemOffsetSerialNumber == dutSerialNumber) &&
                calValid)
            {
                // set system loss
                this.SetSystemOffset(systemOffset_dB, systemOffsetAtFreqGHz);
                this.isValid = true;
            }
            else this.isValid = false;
        }

        /// <summary>
        /// Save ITLA system offset calibration
        /// </summary>
        public void SaveConfig()
        {
            calConfig.WriteCalibrationEntry("SystemOffset_dB", fullSystemOffset_dB);
            calConfig.WriteCalibrationEntry("SystemOffsetAtFreqGHz", systemOffsetAtFreqGHz);
            calConfig.WriteCalibrationEntry("SystemOffsetSerialNumber", dutSerialNumber);

        }

        /// <summary>
        /// Splitter Calibration object
        /// </summary>
        public OpticalOffsetVsFrequency SplitCal
        {
            get { return splitCal; }            
        }

        private OpticalOffsetVsFrequency splitCal;

        /// <summary>
        /// Reference power meter calibration factors
        /// </summary>
        public OpticalOffsetVsFrequency PowerMeterReferenceCal
        {
            get { return powerMeterReferenceCal; }            
        }

        private OpticalOffsetVsFrequency powerMeterReferenceCal;

        /// <summary>
        /// Set the system offset to use. This will be a loss (i.e. a NEGATIVE number in dB).
        /// </summary>
        /// <param name="fullSystemOffset_dB">Full system offset, including splitter loss</param>
        /// <param name="systemOffsetAtFreqGHz">Frequency loss was measured at</param>
        public void SetSystemOffset(double fullSystemOffset_dB, double systemOffsetAtFreqGHz)
        {
            // record the values read from the file
            this.fullSystemOffset_dB = fullSystemOffset_dB;
            this.systemOffsetAtFreqGHz = systemOffsetAtFreqGHz;

            // calculate the splitter loss at this frequency
            double splitterOffset_dB = splitCal.GetOffset_dB(systemOffsetAtFreqGHz);
            // record the additional loss, not due to the splitter
            systemOffsetNoSplitter_dB = fullSystemOffset_dB - splitterOffset_dB;
            this.isValid = true;
        }

        

        /// <summary>
        /// Calculate the offset correction at a given frequency
        /// </summary>
        /// <param name="freqGHz">Frequency in GHz</param>
        /// <returns>Returns the offset in dB</returns>
        public double GetOffsetAtFreqGHz(double freqGHz)
        {
            double splitterOffset_dB = splitCal.GetOffset_dB(freqGHz);
            double offsetAtFreq = splitterOffset_dB + systemOffsetNoSplitter_dB;
            return offsetAtFreq;
        }

        /// <summary>
        /// Is the optical calibration valid: i.e. is there a splitter cal and 
        /// a system offset...
        /// </summary>
        public bool IsValid
        {
            get { return isValid; }
        }

        #region Private data

        private CalTableConfigAccessor calConfig;
        private string dutSerialNumber;
        private bool isValid;
        /// <summary>
        /// Full splitter offset, including the splitter
        /// </summary>
        private double fullSystemOffset_dB;

        /// <summary>
        /// Frequency at which the system offset was measured
        /// </summary>
        private double systemOffsetAtFreqGHz;

        /// <summary>
        /// System offset, not including the splitter
        /// </summary>
        private double systemOffsetNoSplitter_dB;        
        
        #endregion
        
    }
}
