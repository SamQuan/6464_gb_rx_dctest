using System;
using System.Diagnostics;



namespace Bookham.TestLibrary.Algorithms
{
	/// <summary>
	/// Class containing LinearLeastSquaresFit algorithm
	/// </summary>
	public sealed class LinearLeastSquaresFitAlgorithm
	{
	
		/// <summary>
		/// Calculate fit over specified x range
		/// </summary>
		/// <param name="x">Array of x values to fit</param>
		/// <param name="y">Array of y values to fit</param>
		/// <param name="fromXvalue">X value to fit from - uses index of val greater than this</param>
		/// <param name="toXvalue">X value to fit up to - uses index of val greater than this</param>
		public static LinearLeastSquaresFit Calculate_ByXValue( double[] x, double[] y, double fromXvalue, double toXvalue )
		{
			BoundingIndices fromValBI = BoundingIndicesAlgorithm.Calculate( x, fromXvalue );
			BoundingIndices toValBI = BoundingIndicesAlgorithm.Calculate( x, toXvalue );

			// Have to choose one of the two bounding indicies for each end of the fit range
			// Without prior knowledge of the contents of x we can do no better than choose arbitrarily
			return Calculate( x, y, fromValBI.IndexOfArrayValueGreaterThan, toValBI.IndexOfArrayValueGreaterThan );
		}

		/// <summary>
		/// Calculate fit over specified x range
		/// </summary>
		/// <param name="x">Array of x values to fit</param>
		/// <param name="y">Array of y values to fit</param>
		/// <param name="fromYvalue">Y value to fit from - uses index of val greater than this</param>
		/// <param name="toYvalue">Y value to fit up to - uses index of val greater than this</param>
		public static LinearLeastSquaresFit Calculate_ByYValue( double[] x, double[] y, double fromYvalue, double toYvalue )
		{
			BoundingIndices fromValBI = BoundingIndicesAlgorithm.Calculate( y, fromYvalue );
			BoundingIndices toValBI = BoundingIndicesAlgorithm.Calculate( y, toYvalue );

			// Have to choose one of the two bounding indicies for each end of the fit range
			// Without prior knowledge of the contents of x we can do no better than choose arbitrarily
			return Calculate( x, y, fromValBI.IndexOfArrayValueGreaterThan, toValBI.IndexOfArrayValueGreaterThan );
		}


		/// <summary>
		/// Calculate fit over specified index range
		/// </summary>
		/// <param name="x">Array of x values to fit</param>
		/// <param name="y">Array of y values to fit</param>
		/// <param name="fromIndex">Array index to fit from</param>
		/// <param name="toIndex">Array index to fit up to</param>
		public static LinearLeastSquaresFit Calculate( double[] x, double[] y, int fromIndex, int toIndex )
		{
			// PRECONDITIONS
			if( fromIndex == toIndex ) 
			{
				throw new InvalidOperationException( "fromIndex == toIndex" );
			}
			
			if( x.Length==0 || y.Length==0 )
			{
				throw new InvalidOperationException( "empty array" );
			}

			if( x.Length != y.Length)
			{
				throw new InvalidOperationException( "arrays must be the same size" );
			}

			// Elements go from 0 -> n, numpts will always be +1 more
			if( (toIndex + 1) > x.Length )
			{
				throw new InvalidOperationException( "toIndex is greater than number of array points" );
			}



			// Ensure fromIndex < to Index
			if( fromIndex > toIndex ) 
			{
				int fromIndexCopy = fromIndex;

				fromIndex = toIndex;
				toIndex   = fromIndexCopy;
			}
	

			// Calculate the mean of x, and the mean of y
			double xBar = 0.0, yBar = 0.0;	
			for( int i = fromIndex; i <= toIndex; i++ )
			{
				xBar += x[i];
				yBar += y[i]; // Have already asserted that x and y are the same size
			}
			xBar /= ( toIndex - fromIndex + 1 );
			yBar /= ( toIndex - fromIndex + 1 );
			

			// Calculate the sums of squares
			double SSxy = 0.0;
            double SSxx = 0.0;
            double SSyy = 0.0;

			for( int i = fromIndex; i <= toIndex; i++ ) 
			{
				SSxy += (x[i] - xBar) * (y[i] - yBar);
				SSxx += (x[i] - xBar) * (x[i] - xBar);
                SSyy += (y[i] - yBar) * (y[i] - yBar);
			}


			if( SSxx == 0 )
			{
				// This can only happen if all x[i] are the same value
				throw new InvalidOperationException( "SSxx==0 => all x values are the same" );
			}
            if ( SSyy == 0 )
            {
                // This can only happen if all y[i] are the same value
                throw new InvalidOperationException("SSyy==0 => all y values are the same");
            }

			double slope = SSxy / SSxx;
			double yIntercept = yBar - slope * xBar;
			double correlationCoefficient = Math.Sqrt( (SSxy * SSxy) / (SSxx * SSyy) );

			return new LinearLeastSquaresFit( slope, yIntercept, correlationCoefficient );
		}
	}
}
