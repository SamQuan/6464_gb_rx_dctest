// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// DemoBatchViewCtl.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.DemoTestControlPlugin.GuiMessages;

namespace Bookham.DemoTestControlPlugin
{
	/// <summary>
	/// Demo Batch View User Control
	/// </summary>
	public class DemoBatchViewCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
	{
		#region Automatically generated private data

		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button endBatchButton;
		private System.Windows.Forms.Button testDeviceButton;
		private System.Windows.Forms.PictureBox legendPictureBox;
		private System.Windows.Forms.Button markForRetestButton;

		#endregion
		private System.Windows.Forms.Label messagesLabel;
		private System.Windows.Forms.ListBox devicesListBox;
		private System.Windows.Forms.PictureBox logoPictureBox;
		private System.Windows.Forms.Label screenTitleLabel;

		#region User Defined Data
		// Test Statuses for the currently loaded batch.
		private TestStatus [] testStatuses;

		#endregion

		/// <summary>
		/// Constructor.
		/// </summary>
		public DemoBatchViewCtl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// Set the DrawMode property to draw fixed sized items.
			devicesListBox.DrawMode = DrawMode.OwnerDrawFixed;

			// Disable all the buttons.
			disableButtons();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoBatchViewCtl));
            this.messagesLabel = new System.Windows.Forms.Label();
            this.devicesListBox = new System.Windows.Forms.ListBox();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.screenTitleLabel = new System.Windows.Forms.Label();
            this.endBatchButton = new System.Windows.Forms.Button();
            this.testDeviceButton = new System.Windows.Forms.Button();
            this.legendPictureBox = new System.Windows.Forms.PictureBox();
            this.markForRetestButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.legendPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // messagesLabel
            // 
            this.messagesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messagesLabel.Location = new System.Drawing.Point(8, 256);
            this.messagesLabel.Name = "messagesLabel";
            this.messagesLabel.Size = new System.Drawing.Size(312, 16);
            this.messagesLabel.TabIndex = 2;
            // 
            // devicesListBox
            // 
            this.devicesListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.devicesListBox.ItemHeight = 20;
            this.devicesListBox.Location = new System.Drawing.Point(8, 64);
            this.devicesListBox.Name = "devicesListBox";
            this.devicesListBox.Size = new System.Drawing.Size(544, 184);
            this.devicesListBox.TabIndex = 3;
            this.devicesListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.devicesListBox_DrawItem);
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.Image")));
            this.logoPictureBox.Location = new System.Drawing.Point(0, 16);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(128, 40);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logoPictureBox.TabIndex = 4;
            this.logoPictureBox.TabStop = false;
            // 
            // screenTitleLabel
            // 
            this.screenTitleLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.screenTitleLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.screenTitleLabel.Location = new System.Drawing.Point(144, 24);
            this.screenTitleLabel.Name = "screenTitleLabel";
            this.screenTitleLabel.Size = new System.Drawing.Size(304, 32);
            this.screenTitleLabel.TabIndex = 5;
            this.screenTitleLabel.Text = "Test Engine Demo Batch View";
            // 
            // endBatchButton
            // 
            this.endBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.endBatchButton.Location = new System.Drawing.Point(560, 200);
            this.endBatchButton.Name = "endBatchButton";
            this.endBatchButton.Size = new System.Drawing.Size(144, 48);
            this.endBatchButton.TabIndex = 6;
            this.endBatchButton.Text = "End Batch";
            this.endBatchButton.Click += new System.EventHandler(this.endBatchButton_Click);
            // 
            // testDeviceButton
            // 
            this.testDeviceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testDeviceButton.Location = new System.Drawing.Point(560, 64);
            this.testDeviceButton.Name = "testDeviceButton";
            this.testDeviceButton.Size = new System.Drawing.Size(144, 56);
            this.testDeviceButton.TabIndex = 7;
            this.testDeviceButton.Text = "Test Device";
            this.testDeviceButton.Click += new System.EventHandler(this.testDeviceButton_Click);
            // 
            // legendPictureBox
            // 
            this.legendPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("legendPictureBox.Image")));
            this.legendPictureBox.Location = new System.Drawing.Point(720, 64);
            this.legendPictureBox.Name = "legendPictureBox";
            this.legendPictureBox.Size = new System.Drawing.Size(120, 112);
            this.legendPictureBox.TabIndex = 8;
            this.legendPictureBox.TabStop = false;
            // 
            // markForRetestButton
            // 
            this.markForRetestButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markForRetestButton.Location = new System.Drawing.Point(560, 128);
            this.markForRetestButton.Name = "markForRetestButton";
            this.markForRetestButton.Size = new System.Drawing.Size(144, 56);
            this.markForRetestButton.TabIndex = 9;
            this.markForRetestButton.Text = "Mark Device For Retest";
            this.markForRetestButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.markForRetestButton.Click += new System.EventHandler(this.markForRetestButton_Click);
            // 
            // DemoBatchViewCtl
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.markForRetestButton);
            this.Controls.Add(this.legendPictureBox);
            this.Controls.Add(this.testDeviceButton);
            this.Controls.Add(this.endBatchButton);
            this.Controls.Add(this.screenTitleLabel);
            this.Controls.Add(this.logoPictureBox);
            this.Controls.Add(this.devicesListBox);
            this.Controls.Add(this.messagesLabel);
            this.Name = "DemoBatchViewCtl";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.DemoBatchViewCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.legendPictureBox)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Handle incoming messages.
		/// </summary>
		/// <param name="payload">Message payload</param>
		/// <param name="inMsgSeq">Input message sequence number.</param>
		/// <param name="respSeq">Outgoing message sequence number.</param>
		private void DemoBatchViewCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
		{
			if( payload is string )
			{

				string message = (string)payload;
				// Just print a message to screen.
				this.messagesLabel.Text = message;											 
			}
			else if ( payload is SelectNextDUTRequest )
			{
				// Ensure that the buttons are enabled, Test Control Plug-in wants another DUT.
				enableButtons();
			}
			else if( payload is DUTObject )
			{
				// The Test Control Plug-in is adding a new device to the batch view display.
				DUTObject dut = (DUTObject)payload;
				this.devicesListBox.Items.Add( dut.SerialNumber );
			}
			else if( payload is UpdateTestStatuses )
			{
				// Refresh the Batch View Test Statuses.
				UpdateTestStatuses updateMsg = (UpdateTestStatuses)payload;
				this.testStatuses = new TestStatus[updateMsg.testStatuses.Length];
				Array.Copy(updateMsg.testStatuses, 0, this.testStatuses, 0, updateMsg.testStatuses.Length);

				// Enable the GUI buttons. Ready to carry out next operation at this point.
				enableButtons();

			}
			else
			{
				//Invalid message.
			}
		}

		/// <summary>
		/// Update the Devices listbox display colours.
		/// </summary>
		/// <param name="sender">The system</param>
		/// <param name="e">event parameters.</param>
		private void devicesListBox_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
		{
			// Set the DrawMode property to draw fixed sized items.
			devicesListBox.DrawMode = DrawMode.OwnerDrawFixed;


			if (this.devicesListBox.Items.Count != 0)
			{
				int index = e.Index;
				TestStatus status = this.testStatuses[index];				
					
				int penWidth = 1;

				// Create a new Brush and initialize to a Black colored brush by default.
				Brush foreBrush = Brushes.Black;
				Brush backBrush = new SolidBrush(e.BackColor);
				Pen highlightPen = new Pen(SystemColors.Highlight, penWidth);

				// Determine the color of the brush to draw each item based on the index and Test Status of the item to draw.
				if (status == TestStatus.Failed)
				{
					// Display red if failed.
					foreBrush = Brushes.White;
					backBrush = Brushes.Red;
					highlightPen = new Pen(Color.Yellow, penWidth);
				}
				else if (status == TestStatus.Passed)
				{
					// Display green if failed.
					foreBrush = Brushes.White;
					backBrush = Brushes.Green;
					highlightPen = new Pen(Color.Yellow, penWidth);
				}
				else if (status == TestStatus.MarkForRetest)
				{
					// Display Purple if marked for retest.
					foreBrush = Brushes.Purple;
					backBrush = Brushes.Yellow;
					// highlightPen as before
				}
			
				// Draw the background of the ListBox control for each item.
				e.Graphics.FillRectangle(backBrush, e.Bounds);
				
				// Draw the current item text based on the current Font and the custom brush settings.
				e.Graphics.DrawString(devicesListBox.Items[index].ToString(), e.Font, foreBrush,e.Bounds,StringFormat.GenericDefault);
				
				if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
				{
					e.Graphics.DrawRectangle(highlightPen, e.Bounds.Left + penWidth,
						e.Bounds.Top + penWidth, 
						e.Bounds.Width - 2 - penWidth,
						e.Bounds.Height - 2 - penWidth);
				}
				
//				// If the ListBox has focus, draw a focus rectangle around the selected item.
//				e.DrawFocusRectangle();
			}

		}

		/// <summary>
		/// The user has clicked the Test Device Button. Send a message with the serial number of the 
		///  device highlighted in the devices list box to the Test Control plug-in.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">event parameters</param>
		private void testDeviceButton_Click(object sender, System.EventArgs e)
		{
			if (this.devicesListBox.SelectedItem != null)
			{
				// Send the message.
				SelectNextDUTResponse response = new SelectNextDUTResponse (this.devicesListBox.SelectedItem.ToString());
				this.sendToWorker( response);

				// Disable the GUI buttons. They will be re-enabled when 
				// it is time to choose the next device to test.
				disableButtons();
			}
		}

		/// <summary>
		/// The user has clicked the End Batch button. Send a message to the Test Control Plug-in.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">event parameters.</param>
		private void endBatchButton_Click(object sender, System.EventArgs e)
		{
			// Send the message.
			this.sendToWorker( new EndOfBatchNotification() );

			//Disable the GUI Buttons. The will be re-enabled when it is time to select another device to test.
			disableButtons();
		}


		/// <summary>
		/// The User has decided to mark a device for retest. Update the Display and 
		/// send a message to the Test Control Plug-in to inform it of the device marked.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event parameters.</param>
		private void markForRetestButton_Click(object sender, System.EventArgs e)
		{
			if (this.devicesListBox.SelectedItem != null)
			{
				// Only mark the device if it hasn't already passed it's tests or is untested.
				if ((testStatuses[this.devicesListBox.SelectedIndex] != TestStatus.Untested) &&
					(testStatuses[this.devicesListBox.SelectedIndex] != TestStatus.MarkForRetest))
				{
					// send the message.
					MarkedForRetestNotification message = new MarkedForRetestNotification (this.devicesListBox.SelectedItem.ToString());
					this.sendToWorker( message );

					// update the internal Test statuses array.
					testStatuses[this.devicesListBox.SelectedIndex] = TestStatus.MarkForRetest;

					// Refresh the display.
					this.devicesListBox.Refresh();
				}
			}		
		}

		/// <summary>
		/// Enable GUI Buttons.
		/// </summary>
		private void enableButtons()
		{
			this.testDeviceButton.Enabled = true;
			this.endBatchButton.Enabled = true;
			this.markForRetestButton.Enabled = true;
		}

		/// <summary>
		/// Disable GUI Buttons.
		/// </summary>
		private void disableButtons()
		{
			this.testDeviceButton.Enabled = false;
			this.endBatchButton.Enabled = false;
			this.markForRetestButton.Enabled = false;
		}
	}
}

