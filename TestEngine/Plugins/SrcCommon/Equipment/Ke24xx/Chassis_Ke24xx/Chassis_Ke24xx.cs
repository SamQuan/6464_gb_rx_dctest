// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Ke24xx.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    #region Public Functions

    /// <summary>
    /// Ke24xx Chassis Driver
    /// </summary>
    public class Chassis_Ke24xx : ChassisType_Visa488_2
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="chassisName">Chassis name</param>
        /// <param name="driverName">Chassis driver name</param>
        /// <param name="resourceStringId">Resource data</param>
        public Chassis_Ke24xx(string chassisName, string driverName, string resourceStringId)
            : base(chassisName, driverName, resourceStringId)
        {
            // Configure valid hardware information

            // Add details of Ke2400 chassis
            ChassisDataRecord ke2400Data = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2400",		// hardware name 
                "0",										// minimum valid firmware version 
                "C50");										// maximum valid firmware version 
            ValidHardwareData.Add("Ke2400", ke2400Data);
            // Add details of Ke2400-C chassis
            ChassisDataRecord ke2400CData = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2400-C",		// hardware name 
                "0",										// minimum valid firmware version 
                "C39");										// maximum valid firmware version 
            ValidHardwareData.Add("Ke2400C", ke2400CData);

            ChassisDataRecord ke2440Data = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2440",		// hardware name 
                "0",										// minimum valid firmware version 
                "C30");										// maximum valid firmware version 
            ValidHardwareData.Add("Ke2440", ke2440Data);

            ChassisDataRecord ke2440cData = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2440-C",		// hardware name 
                "0",										// minimum valid firmware version 
                "C30");										// maximum valid firmware version 
            ValidHardwareData.Add("Ke2440c", ke2440cData);
        }


        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                this.Clear();
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*cls;*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                // The first 3 characters are the firmware version
                if (idn.Length == 4)
                return idn[3].Substring(0, 3);
                else
                {
                    string[] mystrings = idn[2].Split(' ');

                    return mystrings[1].Trim();
                }
            }
        }

        /// <summary>
        /// Hardware identity string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                this.Clear();
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*cls;*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Override IsOnline to provide error check settings
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    // needed for error detection routines
                    this.Clear();
                    this.Write_Unchecked("*CLS", null);
                    this.StandardEventRegisterMask = 60;
                }
            }
        }
    #endregion
    }

}
