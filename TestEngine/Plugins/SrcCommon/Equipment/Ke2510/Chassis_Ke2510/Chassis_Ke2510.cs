// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Ke2510.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    #region Public Functions

    /// <summary>
    /// Ke2510 Chassis Driver
    /// </summary>
    public class Chassis_Ke2510 : ChassisType_Visa488_2
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="chassisName">Chassis name</param>
        /// <param name="driverName">Chassis driver name</param>
        /// <param name="resourceStringId">Resource data</param>
        public Chassis_Ke2510(string chassisName, string driverName, string resourceStringId)
            : base(chassisName, driverName, resourceStringId)
        {
            // Configure valid hardware information

            // Add details of valid chassis(s)
            ChassisDataRecord ke2510Data = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2510",		// hardware name
                "A00",										// minimum valid firmware version													
                "A20");										// maximum valid firmware version							 
            ValidHardwareData.Add("Ke2510", ke2510Data);

            ChassisDataRecord ke2510Data_1 = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2510   ",		// hardware name
                "A00",										// minimum valid firmware version													
                "A20");										// maximum valid firmware version							 
            ValidHardwareData.Add("Ke2510_1", ke2510Data_1);

            ChassisDataRecord ke2510ATData = new ChassisDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2510-AT",		// hardware name
                "A00",										// minimum valid firmware version													
                "A20");										// maximum valid firmware version							 
            ValidHardwareData.Add("Ke2510AT", ke2510ATData);
        }


        /// <summary>
        /// Firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return the firmware version in the 4th comma seperated field
                return idn[3].Substring(0, 3);
            }
        }

        /// <summary>
        /// Hardware identity string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Override IsOnline to setup StandardEventRegisterMask
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                if (value)
                {
                    this.StandardEventRegisterMask = 60;
                }
            }
        }

    #endregion
    }

}
