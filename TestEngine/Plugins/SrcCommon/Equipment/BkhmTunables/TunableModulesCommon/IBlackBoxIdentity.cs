using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Interface for black box units allowing reading and writing of that
    /// unit's identity along with the laser and PCB components.
    /// </summary>
    public interface IBlackBoxIdentity
    {
        /// <summary>
        /// Gets the completed unit serial number.
        /// </summary>
        /// <returns>Serial number.</returns>
        string GetUnitSerialNumber();

        /// <summary>
        /// Sets the completed unit serial number.
        /// </summary>
        /// <param name="serno">Serial number.</param>
        void SetUnitSerialNumber(string serno);

        /// <summary>
        /// Get's the unit's build date.
        /// </summary>
        /// <returns>Build Date.</returns>
        DateTime GetUnitBuildDate();

        /// <summary>
        /// Sets the unit's build date.
        /// </summary>
        /// <param name="bdate">Build Date.</param>
        void SetUnitBuildDate(DateTime bdate);

        /// <summary>
        /// Gets the laser serial number.
        /// </summary>
        /// <returns>Serial Number.</returns>
        string GetLaserSerialNumber();

        /// <summary>
        /// Sets the laser serial number.
        /// </summary>
        /// <param name="serno">Serial number.</param>
        void SetLaserSerialNumber(string serno);

        /// <summary>
        /// Gets the board serial number.
        /// </summary>
        /// <returns>Serial Number.</returns>
        string GetBoardSerialNumber();

        /// <summary>
        /// Sets the board serial number.
        /// </summary>
        /// <param name="serno">Serial number.</param>
        void SetBoardSerialNumber(string serno);

        /// <summary>
        /// Gets the unit part number.
        /// </summary>
        /// <returns>Part number.</returns>
        string GetPartNumber();

        /// <summary>
        /// Sets the unit part number.
        /// </summary>
        /// <param name="partno">Part number.</param>
        void SetPartNumber(string partno);

        /// <summary>
        /// Gets the supplier name.
        /// </summary>
        /// <returns>Supplier name.</returns>
        string GetSupplier();

        /// <summary>
        /// Gets the laser technology identifier string
        /// </summary>
        /// <returns>Laser technology identifier</returns>
        string GetLaserTechnology();

        /// <summary>
        /// Sets the laser technology identifier string
        /// </summary>
        /// <param name="laserTech">Laser technology identifier</param>
        void SetLaserTechnology(string laserTech);

        /// <summary>
        /// Get the Reciever Serial Number
        /// </summary>
        /// <returns>Reciever Serial Number</returns>
        string GetRxSerialNumber();

        /// <summary>
        /// Set the Reciever Serial Number
        /// </summary>
        /// <param name="serno">The serail number</param>
        void SetRxSerialNumber(string serno);

    }
}
