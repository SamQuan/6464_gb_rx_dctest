// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Tds420.cs
//
// Author: alice.huang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Algorithms;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_Tds420 : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Tds420(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrTds420 = new InstrumentDataRecord(
                "TEKTRONIX TDS 420A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "v2.0.2e");			// maximum valid firmware version 
            instrTds420.Add("MaxStorageSize","30000");
            instrTds420.Add("MaxBandwidth_MHz", "200");
            instrTds420.Add("MinHorizontalScale_S", "0.000000001");
            instrTds420.Add("MaxHorizontalScale_S", "20");
            instrTds420.Add("MaxMeasurementNum", "4");
            ValidHardwareData.Add("TDS420A", instrTds420);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisTds420 = new InstrumentDataRecord(
                "Chassis_Tds420",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("ChassisTDS420A", chassisTds420);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Tds420)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string command = "*IDN?";
                string res = instrumentChassis. Query_Unchecked(command, null);
                // TEKTRONIX,TDS 420A,0,CF:91.1CT FV:v1.0.2e
                string[] idn = res.Split(',');
                string[] verStr = idn[3].Split(' ');
                return verStr[1].Trim().TrimStart("FV:".ToCharArray());
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TEKTRONIX,TDS 420A,0,CF:91.1CT FV:v1.0.2e
                string command = "*IDN?";
                string res = instrumentChassis.Query_Unchecked(command, null);
                string[] idn = res.Split(',');
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            VerticalPosition_Div = 0;
            OffsetLevel_V = 0;
            Impedance = EnumImpedance.MEG;
            BandWidth = EnumBandWidth.FUL;
            
            //AutoSet();
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Oscillascope function

        /// <summary>
        /// disables all front panel buttons and knobs.
        /// </summary>
        public void DisableFrontPannel()
        {
            instrumentChassis.Write_Unchecked(":LOC ALL",this);
        }
        
        /// <summary>
        /// Enables all front panel buttons and knobs
        /// </summary>
        public void EnableFrontPannel()
        {
            instrumentChassis.Write_Unchecked(":UNL ALL",this);
        }
        
        /// <summary>
        /// Clear stored reference waveforms from memory respected to the channel
        /// </summary>
        public void ClearWaveformStorage()
        {
            string cmd = string.Format(":DELE:WAVE REF{0}", Slot);
            instrumentChassis.Write_Unchecked(cmd ,this);
        }
        
        /// <summary>
        /// Clear all stored reference waveforms from memory
        /// </summary>
        public void ClearAllWaveformStorage()
        {
            string cmd = ":DELE:WAVE ALL";
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Save current channel waveform to respected REF(x)
        /// </summary>
        public void SaveWaveform()
        {
            string cmd = string.Format(":SAV:WAV CH{0},REF{0}", Slot);
        }

        /// <summary>
        /// Get/Set waveform stored in REF(x) respected to this channel display status
        /// </summary>
        public bool IsDisplaySavedWaveform
        {
            set
            {
                string status = value ? "ON" : "OFF";
                string cmd = string.Format(":SEL:REF{0} {1}", Slot,status);
                instrumentChassis.Write_Unchecked(cmd, this);
                if (value)
                {
                    cmd = string.Format(":SEL:CONTRO REF{0}", Slot);
                    instrumentChassis.Write_Unchecked(cmd, this);
                }
            }
            get
            {
                string cmd = string.Format(":SEL:REF{0}?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                int sel = 0;
                try
                {
                    sel = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query channel saved waveform display status");
                }
                return rsp.Contains("1");
            }
        }

        /// <summary>
        ///  Set/Get the display status for this channel waveforms.
        /// </summary>
        public bool IsChannelWaveformDisplay
        {
            get
            {
                string cmd = string.Format(":SEL:CH{0}?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                int sel = 0;
                try
                {
                    sel = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query channel waveform display status");
                }
                return rsp.Contains("1");

            }
            set
            {
                string sel = value ? "ON" : "OFF";
                string cmd = string.Format(":SEL:CH{0} {1}", Slot, sel);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        
        /// <summary>
        /// Sets or queries this channel's waveform that is currently affected by
        /// the cursor and vertical settings
        /// </summary>
        public bool IsChannelSelected
        {
            get
            {
                string cmd = string.Format(":SEL:CONTRO?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                int sel = 0;
                try
                {
                    sel = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query channel waveform display status");
                }
                return rsp.Contains("1");

            }
            set
            {
                
                string cmd = string.Format(":SEL:CONTRO CH{0}", Slot);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }


        /// <summary>
        /// Sets or Gets the wavform display format.
        /// </summary>
        public EnumDisplayFormat DisplayFormat
        {
            get
            {
                string cmd = ":DIS:FORM?";
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                EnumDisplayFormat format = EnumDisplayFormat.YT;
                try
                {
                    format = (EnumDisplayFormat)Enum.Parse(typeof(EnumDisplayFormat), rsp, true);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query acquisition display format");
                }
                return format;
            }
            set
            {
                instrumentChassis.Write_Unchecked(":DIS:FORM " + value.ToString(),this);
 
            }
        }
        
        /// <summary>
        /// Causes the digitizing oscilloscope to adjust its vertical, horizontal, and trigger
        /// controls to provide a stable display of the selected waveform
        /// </summary>
        public void AutoSet()
        {
            instrumentChassis.Write_Unchecked(":AUTOS EXEC",this);
        }
        
        /// <summary>
        /// Get/Set the status to autosaves waveforms in reference memory when acquisition completes
        /// </summary>
        public bool AutoSaveAcquisition
        {
            set
            {
                string stateStr = value ? "ON" : "OFF";
                instrumentChassis.Write_Unchecked(":ACQ:AUTOSA " + stateStr, this);
            }
            get
            {
                string rsp = instrumentChassis.Query_Unchecked(":ACQ:AUTOSA?", this);
                
                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query acquisition auto sabe status");
                }
                return rsp.Contains("1") ;

            }
        }

        /// <summary>
        /// Get/Set waveform acquirement mode
        /// </summary>
        public EnumAcquisitionMode AcquisitionMode
        {
            get
            {
                string rsp = instrumentChassis.Query_Unchecked(":ACQ:MODE?", this);
                EnumAcquisitionMode mode = EnumAcquisitionMode.SAM;

                try
                {
                    mode = (EnumAcquisitionMode)Enum.Parse( typeof (EnumAcquisitionMode), rsp, true);
                }
                catch
                {
                    throw new InstrumentException("cant parse " + rsp  + " to acquirement mode");
                }
                return mode;
            }
            set
            {
                string cmd = ":ACQ:MODE " + value.ToString();
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// Get/Set acquisition status
        /// if start a acquisition, this command uses a write unchecked... so you need to
        /// call a WaitForOperationToComplete() if you wish to ensure operation has finished.
        /// That, or GetESRStatus for yourself for your specific requirements        
        /// </summary>
        public bool AcquisitionState
        {
            set
            {
                string stateStr = value ? "ON" : "OFF";
                instrumentChassis.Write_Unchecked(":ACQ:STATE " + stateStr, this);
            }
            get 
            {
                string rsp = instrumentChassis.Query_Unchecked(":ACQ:STATE?", this);
                                
                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query acquisition state");
                }
                return (rsp.Contains("1") || rsp.Contains("RUN") || num != 0);

            }
        }
        /// <summary>
        /// Restart the acquisition
        /// </summary>
        public void RestartAcquisition()
        {
            instrumentChassis.Write_Unchecked(":ACQ:STATE RUN", this);
        }

        /// <summary>
        /// Get/Set the digitizing oscilloscope when to stop taking acquisitions.
        /// This is equivalent to setting Stop After in the Acquire menu.
        /// </summary>
        public EnumAcquisitionStopCondition AcquisitionStopCondition
        {
            get
            {
                string rsp = instrumentChassis.Query_Unchecked(":ACQ:STOPA?", this);
                EnumAcquisitionStopCondition cond = EnumAcquisitionStopCondition.RUNS;

                try
                {
                    cond = (EnumAcquisitionStopCondition)Enum.Parse(
                        typeof(EnumAcquisitionStopCondition), rsp,true);
                }
                catch
                {
                    throw new InstrumentException("Oscillascope returm " + 
                        rsp + "while query the acquisition stop condition");
                }
                return cond;

            }
            set
            {
                string cmd = ":ACQ:STOPA " + value.ToString();
                instrumentChassis.Write_Unchecked(cmd, this);

            }
        }
        
        /// <summary>
        /// Sets/Gets the number of waveform acquisitions that make up an averaged waveform.
        /// </summary>
	    public int AcquireAvgNum
	    {
		    get 
            {
                string cmd = ":ACQ:NUMAV?";
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("Oscillascope returm " +
                        rsp + "while query the average count for waveform acquire.");
                }
                return num;

                }
		    set 
            {
                string cmd = string.Format(":ACQ:NUMAV {0}", value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
	    }
        /// <summary>
        /// Get/Set the behavior of the acquisition system during equivalent-time (ET) operation. When repetitive mode is on, the
        /// acquisition system will continue to acquire waveform data until the waveform
        /// record is filled with acquired data. When repetitive mode is off and you specify
        /// single acquisition operation, only some of the waveform data points will be set
        /// with acquired data, and the displayed waveform shows interpolated values for the unsampled data points.
        /// </summary>
        public bool IsRepeatAcquireModeOn
        {
            set
            {
                string stateStr = value ? "ON" : "OFF";
                instrumentChassis.Write_Unchecked(":ACQ:REPE " + stateStr, this);
            }
            get
            {
                string rsp = instrumentChassis.Query_Unchecked(":ACQ:REPE?", this);

                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query repetitive mode state");
                }
                return (rsp.Contains("1") );

            }
        }
        /// <summary>
        /// Get/Set the data points of storage for the specified waveform to store in reference location.
        /// </summary>
        public int WaveformStorageSize
        {
            set
            {
                value = (value>int .Parse(this .HardwareData["MaxStorageSize"]))? 
                    int.Parse( this .HardwareData["MaxStorageSize"]):value ;
                string cmd = string.Format(":ALLO:WAVE:REF{0} {1}", Slot, value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
            get
            {
                string cmd = string.Format(":ALLO:WAVE:REF{0}?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("Oscillascope returm " +
                        rsp + "while query the size of waveform storage for the specified reference location.");
                }
                return num;
            }
        }

        /// <summary>
        /// Get/Set channel bandwidth
        /// </summary>
        public EnumBandWidth  BandWidth
        {
            get 
            {
                EnumBandWidth bandwidth = EnumBandWidth.FUL;
                string cmd = string .Format(":CH{0}:BAN?", Slot );
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                try
                {
                    bandwidth = (EnumBandWidth)Enum.Parse(typeof(EnumBandWidth), rsp,true);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query bandwidth");
                }
                return bandwidth; 
            }
            set 
            {
                string cmd = string.Format(":CH{0}:BAN {1}", Slot, value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        
        /// <summary>
        /// Sets or Gets the input attenuator coupling setting
        /// </summary>
        public EnumCouplingMode  Coupling
        {
            get
            {
                EnumCouplingMode couple = EnumCouplingMode.DC;
                string cmd = string.Format(":CH{0}:COUP?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                try
                {
                    couple = (EnumCouplingMode)Enum.Parse(typeof(EnumCouplingMode), rsp, true);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query coupling");
                }
                return couple;
            }
            set 
            {
                string cmd = string.Format(":CH{0}:COUP {1}", Slot,value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        
        /// <summary>
        /// Sets or Gets the impedance setting
        /// </summary>       
        public EnumImpedance Impedance
        {
            get 
            {
                EnumImpedance imp = EnumImpedance.MEG;
                string cmd = string.Format(":CH{0}:IMP?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                try
                {
                    imp = (EnumImpedance)Enum.Parse(typeof(EnumImpedance), rsp, true);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query impedance");
                }
                return imp; 

            }
            set
            {
                string cmd = string.Format(":CH{0}:IMP {1}", Slot, value);
                instrumentChassis.Write_Unchecked(cmd, this); 
            }
        }
        
        /// <summary>
        /// Sets or queries the offset, in volts, that is subtracted from the specified input
        /// channel before it is acquired
        /// </summary>
        public double OffsetLevel_V
        {
            get 
            {
                double offsetLevel;
                string cmd = string.Format(":CH[0}:OFFS?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);

                try
                {
                    offsetLevel = double.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query offset level");
                }
                return offsetLevel; 
            }
            set 
            {
                string cmd = string.Format(":CH[0}:OFFS {1}", Slot,value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// Sets or Gets the vertical position in divisions from the center graticule of the specified channel.
        /// the value should be within -5 ~ 5
        /// </summary>
        public double VerticalPosition_Div
        {
            set
            {
                double div = value % 5;
                string cmd = string.Format(":CH{0}:POS {1}", Slot, div);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
            get
            {
                string cmd = string.Format(":CH{0}:POS?", Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                double pos = 0;
                try
                {
                    pos = double.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query waveform position");
                }
                return pos;
            }
        }

        /// <summary>
        /// Sets or gets the vertical gain (in volts per division) of the specified channel.
        /// </summary>
        public double VeticalScale_V

        {
            get
            {
                string cmd = string.Format(":CH{0}:SCAL?",Slot);
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                double scale = 0;
                try
                {
                    scale = double.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query vetical scale");
                }
                return scale;
            }
            set
            {
                string cmd = string.Format(":CH{0}:SCAL {1}", Slot, value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// Sets the time per division for the main time base(in Sec per division).
        /// </summary>
        public double HorizontalScale_S
        {
            get
            {
                string cmd = ":HOR:MAI:SCAL?";
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                double scale = 0;
                try
                {
                    scale = double.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                        " while query Horizontal scale");
                }
                return scale;
            }
            set
            {
                double Tscale_S = value > double.Parse(this.HardwareData["MinHorizontalScale_S"]) ? value : double.Parse(this.HardwareData["MinHorizontalScale_S"]);
                Tscale_S = Tscale_S < double .Parse( this .HardwareData["MaxHorizontalScale_S"])? Tscale_S : double .Parse( this.HardwareData["MaxHorizontalScale_S"]);
                string cmd = string.Format(":HOR:MAI:SCAL {0}", Tscale_S);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// Sets or Gets the starting data point for waveform transfer.
        /// </summary>
        public int WavformDataStartPoint
        {
            get
            {
                string rsp = instrumentChassis.Query_Unchecked(":DAT:STAR?", this );

                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("Oscillascope returm " +
                        rsp + "while query the start point for waveform data.");
                }
                return num;

            }
            set
            {
                int maxNum = DataPoints;
                if (value >= maxNum)
                    throw new InstrumentException(value.ToString() + "exceeds the data length. There is " + maxNum.ToString() + " points in this waveform ");
                
                string cmd = string .Format (":DAT:STAR {0}",value );
                instrumentChassis .Write_Unchecked (cmd ,this);
            }
        }
        
        /// <summary>
        /// Sets or Gets the last data point that will be transferred when using the CURVe? query
        /// </summary>
        public int WaveformDataStopPoint
        {
            get
            {
                string rsp = instrumentChassis .Query_Unchecked ( ":DAT:STOP?",this );

                int num = 0;
                try
                {
                    num = int.Parse(rsp);
                }
                catch
                {
                    throw new InstrumentException("Oscillascope returm " +
                        rsp + "while query the  stop point for waveform data.");
                }
                return num;

            }
            set
            {
                string cmd = string.Format(":DAT:STOP {0}", value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        /// <summary>
        /// Get/Sets data points in a waveform
        /// THE VALUE SHOULD BE 500,1000,2500,5000.15000 or 30000
        /// </summary>
        public int DataPoints
        {
            get
            {
                string cmd = ":HOR:RECO?";
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                int points = 0;

                try
                { points = int.Parse(rsp); }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                          " while query Horizontal data points");
                }
                return points;
            }
            set
            {
                value = (value > int.Parse(this.HardwareData["MaxStorageSize"])) ?
                    int.Parse(this.HardwareData["MaxStorageSize"]) : value;
                //if (AutoSaveAcquisition)
                //    value = (value > WaveformStorageSize ? WaveformStorageSize : value);
                string cmd = ":HOR:RECO " + value.ToString();

                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// Save channels waveform to coresponse REF
        /// </summary>
        public void SaveWaveform2Ref()
        {
            string cmd = string.Format(":SAV:WAVE CH{0},REF{0}", Slot);
            instrumentChassis.Write_Unchecked(cmd, this);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ClsWaveformData GetWaveform()
        {
            IsChannelWaveformDisplay = true;
            // Set the channel wavform to be the source waiting for transfer
            string cmd = string.Format(":DAT:SOU CH{0}", Slot);
            instrumentChassis.Write_Unchecked(cmd, this);            
            cmd = ":DAT:WID 2";
            instrumentChassis.Write_Unchecked(cmd, this);

            // ASCIi specifies the ASCII representation of signed integer (RIBinary) data.
            // RIBinary specifies signed integer data-point representation with the most significant byte transferred first.
            // SRIbinaty is the same as RIBinary except that the byte order is swapped,
            // meaning that the least significant byte is transferred first. 
            // This format is useful when transferring data to IBM compatible PCs.
            cmd = ":DAT:ENC ASCI";

            
            instrumentChassis.Write_Unchecked(cmd, this);
            // INIT initializes the waveform data parameters to their factory defaults.
            // SNAp sets DATa:STARt and DATa:STOP to match the current vertical bar cursor positions.
            //cmd = ":DAT INIT";
            //instrumentChassis.Write_Unchecked(cmd, this);

            // specifies a normal waveform where one ASCII or binary data point is
            // transmitted for each point in the waveform record. Only y values are explicitly
            // transmitted. Absolute coordinates are given by:
            // Xn = XZEro + XINcr (n �C PT_Off)
            // Yn = YZEro + YMUlt (yn - YOFf)
            cmd = string.Format(":WFMP:CH{0}:PT_F Y", Slot);
            instrumentChassis.Write_Unchecked(cmd, this);

            // Returns the waveform formatting data for the first ordered waveform as specified
            // by the DATa:SOUrce command. The channel and math waveforms selected by
            // the DATa:SOUrce command must be displayed.
            cmd = string.Format(":WFMP:CH{0}?", Slot);
            string rsp = instrumentChassis.Query_Unchecked(cmd, this);
            ClsWaveformDataScale myScale = new ClsWaveformDataScale(rsp);

            // Transfers waveform data to and from the digitizing oscilloscope in binary or
            // ASCII format. Each waveform that is transferred has an associated waveform
            // preamble which contains information such as data format and scale

            cmd = ":CURV?";
            rsp = instrumentChassis.Query_Unchecked(cmd ,this );
                        
            if (rsp.Length < 1)
                throw new InstrumentException("The oscillator returms empty  while querying waveform");
            //while (!rsp.EndsWith("\n"))
            //{
            //    try
            //    {
            //        rsp += instrumentChassis.Read_Unchecked(this);
            //    }
            //    catch
            //    { }
            //}

            //int startPos =  dataArr[0] + 1;
            //byte[] Ytemp = Alg_ArrayFunctions.ExtractSubArray(dataArr, startPos, dataArr.Length - 1);
            //if ((Ytemp.Length % 2) != 0)
            //    throw new InstrumentException(" the wave form y data bytes has " + Ytemp.ToString() +
            //        " Bytes, to make it convert to INT16 correctly,it should be an even number"); 
            // Convert.ToInt32(data, base);
            string[] Ytemp = rsp.Split(',');
            int[] YDac = new int[Ytemp.Length];
            
            for (int ind = 0; ind < Ytemp.Length; ind++)
            {
                YDac[ind] = int .Parse (Ytemp[ind ]);                
            }

            ClsWaveformData waveformData = new ClsWaveformData(myScale, YDac);
            return waveformData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ClsWaveformData GetSavedWaveForm()
        {
            IsDisplaySavedWaveform = true;
            // Set the waveform saved in ref to be the source waiting for transfer
            string cmd = string.Format(":DAT:SOU REF{0}", Slot);
            instrumentChassis.Write_Unchecked(cmd, this);            
            cmd = ":DAT:WID 2";
            instrumentChassis.Write_Unchecked(cmd, this);
            // ASCIi specifies the ASCII representation of signed integer (RIBinary) data.
            // RIBinary specifies signed integer data-point representation with the most significant byte transferred first.
            // SRIbinaty is the same as RIBinary except that the byte order is swapped,
            // meaning that the least significant byte is transferred first. 
            // This format is useful when transferring data to IBM compatible PCs.
            cmd = ":DAT:ENC ASCI";
            instrumentChassis.Write_Unchecked(cmd, this);
            // INIT initializes the waveform data parameters to their factory defaults.
            // SNAp sets DATa:STARt and DATa:STOP to match the current vertical bar cursor positions.
            cmd = ":DAT INIT";
            instrumentChassis.Write_Unchecked(cmd, this);
            // specifies a normal waveform where one ASCII or binary data point is
            // transmitted for each point in the waveform record. Only y values are explicitly
            // transmitted. Absolute coordinates are given by:
                // Xn = XZEro + XINcr (n �C PT_Off)
                // Yn = YZEro + YMUlt (yn - YOFf)
            cmd = string.Format(":WFMP:REF{0}:PT_F Y", Slot);
            instrumentChassis.Write_Unchecked(cmd, this);
            // Returns the waveform formatting data for the first ordered waveform as specified
            // by the DATa:SOUrce command. The channel and math waveforms selected by
            // the DATa:SOUrce command must be displayed.
            cmd = string.Format(":WFMP:REF{0}?", Slot);
            string rsp = instrumentChassis.Query_Unchecked(cmd, this);
            ClsWaveformDataScale myScale = new ClsWaveformDataScale(rsp);
            // Transfers waveform data to and from the digitizing oscilloscope in binary or
            // ASCII format. Each waveform that is transferred has an associated waveform
            // preamble which contains information such as data format and scale

            cmd = ":CURV?";
            rsp = instrumentChassis.Query_Unchecked(cmd, this);
            if (rsp.Length < 1)
                throw new InstrumentException("The oscillator returms empty  while querying waveform");
            while (!rsp.EndsWith("\x0A"))
            {
                try
                {
                    rsp += instrumentChassis.Read_Unchecked(this);
                }
                catch
                { }
            }

            //int startPos =  dataArr[0] + 1;
            //byte[] Ytemp = Alg_ArrayFunctions.ExtractSubArray(dataArr, startPos, dataArr.Length - 1);
            //if ((Ytemp.Length % 2) != 0)
            //    throw new InstrumentException(" the wave form y data bytes has " + Ytemp.ToString() +
            //        " Bytes, to make it convert to INT16 correctly,it should be an even number"); 
            // Convert.ToInt32(data, base);
            string[] Ytemp = rsp.Split(',');
            int[] YDac = new int[Ytemp.Length];

            for (int ind = 0; ind < Ytemp.Length; ind++)
            {
                YDac[ind] = int.Parse(Ytemp[ind]);
            }

            ClsWaveformData waveformData = new ClsWaveformData(myScale, YDac);
            return waveformData;
        }

        /// <summary>
        /// Creates a trigger event. If TRIGger:STATE is REAdy, 
        /// the acquisition will complete, otherwise this command will be ignored.
        /// </summary>
        public void Trigger()
        {
            string cmd = ":TRIG FORC";
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Sets SPECIFIED trigger levle  TO 50% Min-Max
        /// </summary>
        /// <param name="triggerType"> Main/Delay trigger </param>
        public void SetTrigger2HalfLevel(EnumTriggerType triggerType)
        {
            string cmd = string.Format(":TRIG:{0} SETL" , triggerType);
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Set Edge trigger for specified trigger        
        /// </summary>
        /// <param name="triggerType"> Specify which trigger to be set, main or delay</param>
        /// <param name="triggerSource"> Specify which channel that the trigger source comes from </param>
        /// <param name="slope"></param>
        /// <param name="coupling"> the type of coupling for trigger.</param>
        public void SetTriggerEdge(EnumTriggerType triggerType, EnumTrigerEdgeSource triggerSource,
            EnumTiggerEdgeSlopeType slope, EnumTriggerEdgeCouplingType coupling)
        {
            string cmd = string.Format(":TRIG:{0}:TYPE EDGE", triggerType);
            instrumentChassis.Write_Unchecked(cmd, this);
            //cmd = string .Format (":TRIG:{0}:EDGE {1}",Slot ,);
            //instrumentChassis .Write_Unchecked (cmd ,this );
            cmd = string .Format(":TRIG:{0}:EDGE:SOU {1};SLO {2};COUP {3}",
                triggerType, triggerSource ,slope ,coupling);
            instrumentChassis .Write_Unchecked(cmd ,this );
            

        }
        
        /// <summary>
        /// Removes the measurement snapshot display.
        /// </summary>
        public void ClearUpMeasurementDisplay()
        {
            string cmd = ":MEASU:CLEARSN";
            instrumentChassis.Write_Unchecked( cmd ,this );
        }

        /// <summary>
        /// Get specified measurement turn on status
        /// </summary>
        /// <returns></returns>
        public bool  GetMeasurementState( EnumMeasurementNum measNum)
        {
            if (measNum != EnumMeasurementNum.IMM)
            {
                string cmd = string.Format("MEASU:{0}:STATE?", measNum);

                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                
                int state = 0;

                try
                { state = int.Parse(rsp); }
                catch
                {
                    throw new InstrumentException("The oscillascope response with " + rsp +
                          " while query measurement turn on state ");
                }
                return state==1;


            }
            else   // Immediate measurement is always turn on
                return true;
        }

        /// <summary>
        /// Controls the measurement system. The source specified by MEASUrement:
        /// MEAS<x>:SOURCE1 must be selected for the measurement to be
        /// displayed. The source can be selected using the SELect:CH<x> command.
        /// </summary>
        /// <param name="measNum"></param>
        /// <param name="status"> Measurement status </param>
        public void SetMeasurementState(EnumMeasurementNum measNum, bool status)
        {
            // Immediate measurement is always on and can't be turn off 
            if (measNum == EnumMeasurementNum.IMM) return;

            string state = status? "ON" : "OFF";

            string cmd = string.Format(":MEASU:{0}:STATE {1}", measNum, state);
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Sets  the measurement type for the measurement specified by MEAS<x>
        /// </summary>
        /// <param name="measNum"></param>
        /// <param name="measType"></param>
        public void SetMeasurementType ( EnumMeasurementNum measNum, EnumMeasurementType measType)
        {
            string cmd = string.Format(":MEASU:{0}:TYP {1}", measNum, measType);
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Gets the measurement type for the measurement specified byMEAS<x>
        /// </summary>
        /// <param name="measNum"></param>
        /// <returns></returns>
        public EnumMeasurementType GetMeasurementType(EnumMeasurementNum measNum)
        {
            // MEASUrement:MEAS<x>:TYPe
            string cmd = string.Format(":MEASU:{0}:TYP?");
            string rsp = instrumentChassis.Query_Unchecked(cmd, this);

            EnumMeasurementType measType;
            try
            {
                measType = (EnumMeasurementType)Enum.Parse(typeof(EnumMeasurementType), rsp, true);
            }
            catch
            {
                throw new InstrumentException(string .Format ( "The oscillascope response with " + rsp +
                    " while query measurement type for {0} .", measNum ));
            }
            return measType;

        }

        /// <summary>
        /// Get the source for  single channel measurements 
        /// </summary> 
        public EnumMeasurementSource GetMeasurementSource(EnumMeasurementNum measNum)
        {
            // MEASUrement:MEAS<x>:SOURCE[1]
            string cmd = string.Format("MEASU:{0}:SOURCE1?", measNum);
            string rsp = instrumentChassis.Query_Unchecked(cmd, this);

            EnumMeasurementSource source;
            try
            {
                source = (EnumMeasurementSource)Enum.Parse(typeof(EnumMeasurementSource), rsp, true);
            }
            catch
            {
                throw new InstrumentException( string .Format ("The oscillascope response with " + rsp +
                    " while query measurement source for {0}", measNum ));
            }
            return source;

        }

        /// <summary>
        /// Sets s the source for all single channel measurements to this channel and specifies this channel to be the 
        /// source to measure ��from�� when taking a delay measurement or phase measurement.
        /// </summary>
        /// <param name="measNum"></param>
        public void SetupChannelMeasurement(EnumMeasurementNum measNum)
        {
            // MEASUrement:MEAS<x>:SOURCE[1]
            string cmd = string .Format( ":MEASU:{0}:SOURCE1 CH{1}", measNum ,Slot);
            this.instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Sets s the source for all single channel measurements to the REF coresponding to this channel and 
        /// specifies REF coresponding to this channel to be the  source to measure ��from�� when taking a delay measurement or phase measurement.
        /// </summary>
        public void SetupSavedWaveformMeasurement(EnumMeasurementNum measNum)
        {
            // MEASUrement:MEAS<x>:SOURCE[1]
            string cmd = string.Format(":MEASU:{0}:SOURCE1 REF{1}", measNum, Slot);
            this.instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Gets the value that has been calculated for the measurement(x).
        /// </summary>
        /// <param name="measNum"></param>
        /// <returns></returns>
        public double GetMeasurementValue(EnumMeasurementNum measNum)
        {
            // MEASUrement:MEAS<x>:VALue?
            string cmd = string.Format(":MEASU:{0}:VAL?", measNum);

            string rsp = instrumentChassis.Query_Unchecked(cmd, this);
            
            double val = 0;

            try
            { 
                val = int.Parse(rsp); 
            }
            catch
            {
                throw new InstrumentException( string .Format ("The oscillascope response with " + rsp +
                      " while query measurement value for {0} "));
            }
            return val ;

        }

        /// <summary>
        /// Making use of the chassis OPC* command
        /// </summary>
        public void WaitForOperationToComplete()
        {
            instrumentChassis.WaitForOperationComplete();
        }

        /// <summary>
        /// Wait for waveform acquirement to finish
        /// </summary>
        /// <returns>True when Acquire ends</returns>
        public  bool WaitForAcquisitionToFinish()
        {
            //int oldTimeout = instrumentChassis.Timeout_ms;
            //instrumentChassis.Timeout_ms = this.Timeout_ms;
            instrumentChassis.WaitForOperationComplete();
            //instrumentChassis.Timeout_ms = oldTimeout;
            return true;
        }

        /// <summary>
        /// Gets the esr status, (hooking into the instrument chassis)
        /// </summary>
        /// <returns>the standard event register byte</returns>
        public byte GetESRStatus()
        {
            return (instrumentChassis.StandardEventRegister);
        }

        /// <summary>
        /// Enable the OPC bit in the Device Event Status Enable
        /// Register (DESER) and the Event Status Enable Register (ESER) using the DESE
        /// and *ESE commands. When the operation is complete, the OPC bit in the
        /// Standard Event Status Register (SESR) will be enabled and the Event Status Bit
        /// (ESB) in the Status Byte Register will be enabled.
        /// 
        /// If anywhere you want to get serial poll after an operation completed, 
        /// you have to send *OPC follow it
        /// </summary>
        public void EnableSerialPoll()
        {
            instrumentChassis.Write_Unchecked("DESE 1",this);
            instrumentChassis.Write_Unchecked("*ESE 1",this);
            instrumentChassis.Write_Unchecked("*SRE 0",this);
        }

        /// <summary>
        /// Enable the OPC bit in the Device Event Status Enable
        /// Register (DESER) and the Event Status Enable Register (ESER) using the DESE
        /// and *ESE commands. You can also enable service requests by setting the ESB
        /// bit in the Service Request Enable Register (SRER) using the *SRE command.
        /// When the operation is complete, a Service Request will be generated.
        /// 
        /// If anywhere you want to get service request after an operation completed, 
        /// you have to send *OPC follow it
        /// </summary>
        public void EnableServiceRequest()
        {
            instrumentChassis.Write_Unchecked("DESE 1", this);
            instrumentChassis.Write_Unchecked("*ESE 1", this);
            instrumentChassis.Write_Unchecked("*SRE 32", this);
        }
        #endregion
        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Tds420 instrumentChassis;
        #endregion

        #region Enum data define
        
        /// <summary>
        /// Define Acquisition mode type available
        /// </summary>
        public enum EnumAcquisitionMode
        {
            /// <summary>
            /// SAMple: the default mode. , the displayed data point value is simply the first sampled 
            /// value that was taken during the acquisition interval. In sample mode, all 
            /// waveform data has 8 bits of precision.
            /// </summary>
            SAM,
            /// <summary>
            ///  PEAKdetect: the display of the high-low range of the samples taken 
            /// from a single waveform acquisition. The high-low range is displayed as a 
            /// vertical column that extends from the highest to the lowest value sampled during 
            /// the acquisition interval
            /// </summary>
            PEAK,
            /// <summary>
            ///  HIRes: the displayed data point value is the average of all the samples taken during the
            /// acquisition interval
            /// </summary>
            HIR,
            /// <summary>
            /// AVErage: the resulting waveform shows anaverage of sample data points from several separate waveform acquisitions.
            /// </summary>
            AVE,
            /// <summary>
            /// ENVelope: the resulting waveform shows the peakdetect range of data points from several separate waveform acquisitions.
            /// </summary>
            ENVE,
        }
        
        /// <summary>
        /// defines the digitizing oscilloscope when to stop taking acquisitions.
        /// </summary>
        public enum EnumAcquisitionStopCondition
        {
           /// <summary>
            /// RUNStop: run and stop state should be determined by the user
           /// pressing the front-panel RUN/STOP button
           /// </summary> 
            RUNS,
            /// <summary>
            /// SEQuence: specifies ��single sequence�� operation, where the digitizing oscilloscope
            /// stops after it has acquired enough waveforms to satisfy the conditions of
            /// the acquisition mode
            /// </summary>
            SEQ,
            /// <summary>
            /// LIMit: the digitizing oscilloscope stops after the limit test condition is met
            /// </summary>
            LIM,
        }

        
        /// <summary>
        /// enum the bandwidth setting
        /// </summary>
        public enum EnumBandWidth
        {
            /// <summary>
            /// TWEnty: 20 MHZ
            /// </summary>
            TWE,
            /// <summary>
            /// HUNdred: 100 MHZ
            /// </summary>
            HUN,
            //TWOfifty,

            /// <summary>
            /// FULl: Full bandwidth of the digitizing oscilloscope
            /// </summary>
            FUL,
            //NR3

        }

        /// <summary>
        /// Enum impedance setting
        /// </summary>
        public enum EnumImpedance
        {
            /// <summary>
            /// FIFty: 50 Ohms
            /// </summary>
            FIF,
            // SEVENTYFive,
            /// <summary>
            /// MEG: 1 MOhms
            /// </summary>
            MEG,
            //NR3
        }

        /// <summary>
        /// Enum Coupling Mode available for the channel
        /// </summary>
        public enum EnumCouplingMode
        {
            /// <summary>
            /// DC coupling.
            /// </summary>
            DC,
            /// <summary>
            /// AC coupling.
            /// </summary>
            AC,
            /// <summary>
            /// ground. Only a flat ground-level waveform will be displayed
            /// </summary>
            GND,
        }

        /// <summary>
        /// Enum the way to display the waveform
        /// </summary>
        public enum EnumDisplayFormat
        {
            /// <summary>
            /// display waveform with a voltage versus time format and is the normal mode.
            /// </summary>
            YT,
            /// <summary>
            ///  displays one waveform against another between 2 channels.
            /// channel 1 & 3 data will be the X-axis and 2&4 data will be Y-Axis 
            /// </summary>
            XY,
        }

        /// <summary>
        /// Enum the file format available for saved waveforms.
        /// </summary>
        public enum EnumWaveformFileFormat
        {
            /// <summary>
            /// INTERNal: the internal format. Internal format files have a .wfm extension.
            /// </summary>
            INTERN,
            /// <summary>
            /// SPREADSheet: specifies the spreadsheet format. Spreadsheet format files have a .CSV extension.
            /// </summary>
            SPREADS,
            /// <summary>
            /// MathCad format. MathCad format files have a .DAT extension.
            /// </summary>
            MATHC,
        }

        /// <summary>
        /// Enum Triggers available
        /// </summary>
        public enum EnumTriggerType
        {
            /// <summary>
            /// main trigger
            /// </summary>
            MAIn,
            /// <summary>
            /// delayed trigger
            /// </summary>
            DELay,
        }

        /// <summary>
        /// Enum Edge slope types available for trigger
        /// </summary>
        public enum EnumTiggerEdgeSlopeType
        { 
            /// <summary>
            /// RISe: trigger on the rising or positive edge of a signal.
            /// </summary>
            RIS,
            /// <summary>
            /// FALLz: trigger on the rising or positive edge of a signal.
            /// </summary>
            FALL,
        }

        /// <summary>
        /// Enum avialable edge trigger  source
        /// </summary>
        public enum EnumTrigerEdgeSource
        { 
            /// <summary>
            /// 
            /// </summary>
            CH1,
            /// <summary>
            /// 
            /// </summary>
            CH2,
            /// <summary>
            /// 
            /// </summary>
            CH3,
            /// <summary>
            /// 
            /// </summary>
            CH4,
            /// <summary>
            /// an external trigger using the Auxiliary Trigger Input connector that is located on the rear panel of the instrument.
            /// </summary>
            AUXiliary,
        }
        
        /// <summary>
        /// Enum the type of coupling available for edge trigger.
        /// </summary>
        public enum EnumTriggerEdgeCouplingType
        {
            /// <summary>
            /// AC trigger coupling.
            /// </summary>
            AC,
            /// <summary>
            /// DC trigger coupling.
            /// </summary>
            DC,
            /// <summary>
            /// removes the high frequency components of the DC signal.
            /// </summary>
            HFRej,
            /// <summary>
            /// removes the low frequency components of the AC signal.
            /// </summary>
            LFRej,
            /// <summary>
            /// DC low sensitivity. It requires added signal amplitude for more stable, less false triggering.
            /// </summary>
            NOISErej,
        }

        /// <summary>
        /// Enum measurement available on oscilator
        /// </summary>
        public enum EnumMeasurementNum
        {
            /// <summary>
            /// immediate measurement.
            /// </summary>
            IMM,

            /// <summary>
            /// 
            /// </summary>
            MEAS1,
            
            /// <summary>
            /// 
            /// </summary>
            MEAS2,

            /// <summary>
            /// 
            /// </summary>
            MEAS3,

            /// <summary>
            /// 
            /// </summary>
            MEAS4

        }

        /// <summary>
        /// Enum  source available for measurements
        /// </summary>
        public enum EnumMeasurementSource
        {
            /// <summary>
            /// Select Ch1 as measurement source
            /// </summary>
            CH1,

            /// <summary>
            /// Select Ch2 as measurement source
            /// </summary>
            CH2,

            /// <summary>
            /// Select Ch3 as measurement source
            /// </summary>
            CH3,

            /// <summary>
            /// Select Ch4 as measurement source
            /// </summary>
            CH4,

            /// <summary>
            /// Select waveform in REF1 as measurement source
            /// </summary>
            REF1,

            /// <summary>
            /// Select waveform in REF2 as measurement source
            /// </summary>
            REF2,

            /// <summary>
            /// Select waveform in REF3 as measurement source
            /// </summary>
            REF3,

            /// <summary>
            /// Select waveform in REF4 as measurement source
            /// </summary>
            REF4,


            /// <summary>
            /// 
            /// </summary>
            MATH1,

            /// <summary>
            /// 
            /// </summary>
            MATH2,

            /// <summary>
            /// 
            /// </summary>
            MATH3,
            
            /// <summary>
            /// 
            /// </summary>
            MATH4
        }

        /// <summary>
        /// Enum measurement type for the measurement
        /// </summary>
        public enum EnumMeasurementType
        {
            /// <summary>
            /// AMPlitude is the high value minus the low value.
            /// </summary>
            AMP,
            
            /// <summary>
            /// AREa is the area between the curve and ground over the entire waveform.
            /// </summary>
            ARE,
            
            /// <summary>
            /// CARea (cycle area) is the area between the curve and ground over one cycle.
            /// </summary>
            CAR,
            
            /// <summary>
            /// CMEan is the arithmetic mean over one cycle.
            /// </summary>
            CME,
            /// <summary>
            /// CRMs is the true Root Mean Square voltage over one cycle.
            /// </summary>
            CRM,
            
            /// <summary>
            /// FALL is the time that it takes for the falling edge of a pulse to fall from a HighRef value to a LowRef value of its final value.
            /// </summary>
            FALL,
            
            /// <summary>
            ///  FREQuency is the reciprocal of the period measured in hertz.
            /// </summary>
            FREQ,

            /// <summary>
            /// HIGH is the 100% reference level.
            /// </summary>
            HIGH,

            /// <summary>
            /// LOW is the 0% reference level.
            /// </summary>
            LOW,

            /// <summary>
            /// MAXinum is the highest amplitude (voltage).
            /// </summary>
            MAX,

            /// <summary>
            /// MEAN is the arithmetic mean over the entire waveform.
            /// </summary>
            MEAN,

            /// <summary>
            /// MINImun is the lowest amplitude (voltage).
            /// </summary>
            MINI,

            /// <summary>
            /// NDUty is the ratio of the negative pulse width to the signal period expressed as a percentage.
            /// </summary>
            NDU,

            /// <summary>
            /// NOVershoot is the negative overshoot, expressed as:
            /// NOVershoot = 100 * (Low - Minimum)/ Amplitude
            /// </summary>
            NOV,

            /// <summary>
            /// NWIdth is the distance (time) between MidRef (usually 50%) amplitude points of a negative pulse.
            /// </summary>
            NWI,
            /// <summary>
            /// NDUty is the ratio of the positive pulse width to the signal period expressed as a percentage.
            /// </summary>
            PDU,
            
            /// <summary>
            /// PERIod is the time, in seconds, it takes for one complete signal cycle to happen.
            /// </summary>
            PERI,
            
            /// <summary>
            /// PK2pk is the absolute difference between the maximum and minimum amplitude.
            /// </summary>
            PK2,

            /// <summary>
            /// POVershoot is the positive overshoot, expressed
            /// POVershoot = 100 * ( Maximun - High )/ Amplitude
            /// </summary>
            POV,

            /// <summary>
            /// PWIdth  is the distance (time) between MidRef (usually 50%) amplitude points of a positive pulse.
            /// </summary>
            PWI,
            
            /// <summary>
            /// RISe is the time that it takes for the leading edge of a pulse to rise from a low reference value to a high reference value of its final value.
            /// </summary>
            RIS,

            /// <summary>
            /// RMS is the true Root Mean Square voltage.
            /// </summary>
            RMS,
        }

        /// <summary>
        /// Define waveform data
        /// </summary>
        public struct StrcWaveform
        {
            public double[] Xdata;
            public double[] Ydata;
        }
        #endregion
        /// <summary>
        /// Class containts X axis and Y axis scale information 
        /// </summary>
        public class ClsWaveformDataScale:ICloneable

        {
            #region Scale information member

            public readonly int BYT_Nr;
            public readonly int BIT_Nr;
            public readonly string EncodingType;
            public readonly string BinaryFormat;
            public readonly string BinaryOrder;
            /// <summary>
            /// information about the waveform such as input coupling, volts per
            /// division, time per division, acquisition mode, and record length.
            /// </summary>
            public readonly string WaformInfo;
            /// <summary>
            /// Number of waveform points
            /// </summary>
            public readonly int NR_Pt;
            /// <summary>
            /// the data point format for the first ordered waveform as selected by the DATa:SOUrce command
            /// </summary>
            public readonly string PointFormat;
            /// <summary>
            /// the horizontal (X-axis) units of the waveform data at the time of creation
            /// </summary>
            public readonly string XAxisUnit;
            /// <summary>
            /// the horizontal sampling interval
            /// </summary>
            public readonly double XINcr;
            /// <summary>
            /// the horizontal (X-axis) origin offset.
            /// </summary>
            public readonly double XZEro;
            /// <summary>
            /// the trigger point within the waveform record.
            /// </summary>
            public readonly double PT_Off;
            /// <summary>
            /// the vertical (Y-axis) units of the waveform data at the time of creation.
            /// </summary>
            public readonly string YUNit;
            /// <summary>
            /// the vertical scale factor, in YUNit per unscaled data point value.
            /// </summary>
            public readonly double YMUlt;
            /// <summary>
            /// the vertical position of the waveform.
            /// </summary>
            public readonly double YOFf;
            /// <summary>
            /// the vertical (Y-axis) offset voltage
            /// </summary>
            public readonly double YZEro;

            public readonly string ScaleInfromationString;
            #endregion

            /// <summary>
            /// Constructor, set up x & y scale information for oscillascope
            /// </summary>
            /// <param name="scaleInfo"> String contains all scales' information, 
            /// should have 15 or 10 fields and sperated with ';'</param>
            public  ClsWaveformDataScale(string scaleInfo)
            {
                ScaleInfromationString = scaleInfo;
                string[] infoArr = scaleInfo.Split(';');

                if (infoArr.Length == 16-1)
                { 
                    BYT_Nr = int.Parse(infoArr[0]);
                    BIT_Nr = int.Parse(infoArr[1]);
                    EncodingType = infoArr[2];
                    BinaryFormat = infoArr[3];
                    BinaryOrder = infoArr[4];
                    WaformInfo = infoArr[5];
                    NR_Pt = int .Parse(infoArr[6]);
                    PointFormat = infoArr[7];
                    XAxisUnit = infoArr[8];
                    XINcr = double .Parse(infoArr[9]);
                    XZEro = 0; // double.Parse(infoArr[10]);
                    PT_Off = double .Parse (infoArr[11-1]);
                    YUNit = infoArr[12-1];
                    YMUlt = double .Parse (infoArr[13-1]);
                    YOFf = double .Parse(infoArr[14-1]);
                    YZEro = double .Parse(infoArr[15-1]);
                }
                else if (infoArr.Length == 11-1)
                {
                    WaformInfo = infoArr[0];
                    NR_Pt = int.Parse(infoArr[1]);
                    PointFormat = infoArr[2];
                    XAxisUnit = infoArr[3];
                    XINcr = double.Parse(infoArr[4]);
                    XZEro = 0; // double.Parse(infoArr[5]);
                    PT_Off = double.Parse(infoArr[6 -1 ]);
                    YUNit = infoArr[7 -1 ];
                    YMUlt = double.Parse(infoArr[8 -1 ]);
                    YOFf = double.Parse(infoArr[9 -1 ]);
                    YZEro = double.Parse(infoArr[10 -1 ]);
                }
                else
                {
                    throw new InstrumentException(
                        " try to construct scale information with illege information as \n" + scaleInfo +
                        "\nthe string should contain 15 or 10 fields, please make reference to the mannual");
                }
            }

            /// <summary>
            /// Return all scale information in string list, each element is combine by the name and value splited with comma
            /// </summary>
            /// <returns></returns>
            public List<string > ScaleInfoToStringList()
            {
                List<string> lstScale = new List<string>();
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(" Waveform information,{0}\n", WaformInfo);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" Data Points, {0}\n", NR_Pt);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" X Axis Unit, {0}", XAxisUnit);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" X Axis interval, {0}", XINcr);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat("X Axis offset, {0}", XZEro);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" Trigger Point, {0}", PT_Off);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" Y Axis Unit, {0}", YUNit);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" Y Axis factor, {0}", YMUlt);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" Y Axis Offset, {0}", YOFf);
                lstScale.Add(sb.ToString());
                sb = new StringBuilder();
                sb.AppendFormat(" Y Data Offset, {0}", YZEro);
                lstScale.Add(sb.ToString());
                return lstScale;
            }

            #region ICloneable Members
            /// <summary>
            /// Create a new instance with current scale information
            /// </summary>
            /// <returns></returns>
            public object Clone()
            {
                return new ClsWaveformDataScale(ScaleInfromationString);
            }

            #endregion
        }
        
        /// <summary>
        /// Waveform data class
        /// </summary>
        public class ClsWaveformData
        {
            /// <summary>
            /// X and Y scale information to construct X and Y data
            /// </summary>
            public readonly ClsWaveformDataScale ScaleInfomation;
            /// <summary>
            /// Y Axis DAC data for waveform
            /// </summary>
            public readonly int[] YRawData;
            /// <summary>
            /// X Axis data for waveform
            /// </summary>
            public readonly double[] Xdata;
            /// <summary>
            /// Y Axis data for waveform
            /// </summary>
            public readonly double[] Ydata;

            /// <summary>
            /// Consructor, use Y raw data and scale information to construct X-Y real data
            /// </summary>
            /// <param name="scaleInfo"> X and Y information to construct X and Y data </param>
            /// <param name="yData"> Y Axis DAC data for waveform </param>
            public ClsWaveformData(ClsWaveformDataScale scaleInfo, int[] yData)
            {
                // Check if waveform data is available and sacal infomation is ok
                if ( yData == null )
                    throw new InstrumentException("Waveform data array is null ! ");

                int dataPoint = yData .Length ;
                if (dataPoint <1)
                {
                    throw new InstrumentException ("waveform data point is 0");
                }
                if (scaleInfo == null)
                    throw new InstrumentException("Can't set up wavefrom data without avalilable scale information");

                ScaleInfomation = (ClsWaveformDataScale)scaleInfo.Clone();

                YRawData = new int[dataPoint];
                Xdata = new double[dataPoint];
                Ydata = new double[dataPoint];

                for (int indx = 0; indx < dataPoint; indx++)
                {
                    YRawData[indx] = yData[indx];
                    // Xn = XZEro + XINcr (n �C PT_Off)
                    // Yn = YZEro + YMUlt (yn - YOFf)
                    Xdata[indx] = ScaleInfomation.XZEro + (indx + 1 - ScaleInfomation.PT_Off) * ScaleInfomation.XINcr;
                    Ydata[indx] = ScaleInfomation.YZEro + (YRawData[indx] - ScaleInfomation.YOFf) * ScaleInfomation.YMUlt;
                }
            }
            
            /// <summary>
            /// Tranfer all data point to string list, each element is combined by x data and y data and splited with comma
            /// </summary>
            /// <returns></returns>
            public List<string > XYdataToStringList()
            {
                if (Xdata == null || Xdata.Length < 1)
                    return null;

                StringBuilder sb;
                List<string> wfData = new List<string>();
                wfData.Add("X Data,Y Data");
                for (int indx = 0; indx < Xdata.Length; indx++)
                {
                    sb = new StringBuilder();
                    sb .AppendFormat( "{0},{1}", Xdata[indx],Ydata[indx ]);
                    wfData .Add (sb .ToString());
                }
                return wfData;
            }

            /// <summary>
            /// Tranfer all data point to string list, each element is combined by x data ,y data and Y's raw dac data and splited with comma
            /// </summary>
            /// <returns></returns>
            public List<string > AllDataToStringList()
            {
                if (Xdata == null || Xdata.Length < 1)
                    return null;

                StringBuilder sb;
                List<string> wfData = new List<string>();
                wfData.Add("X Data,Y Data, Y Raw Data");
                for (int indx = 0; indx < Xdata.Length; indx++)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("{0},{1},{2}", Xdata[indx], Ydata[indx], YRawData[indx ]);
                    wfData.Add(sb.ToString());
                }
                
                return wfData;
            }

            
        }
    }
}
