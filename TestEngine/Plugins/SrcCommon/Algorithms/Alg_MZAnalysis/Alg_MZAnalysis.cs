// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_MZAnalysis.cs
//
// Author: Mark Fullalove
// Design: Gen4 MZ Gold Box Test DD

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Analysis class for MZ data
    /// </summary>        
    /// <remarks>
    /// Rules for zero chirp :
    ///   VQuad is located on the positive slope closest to 0v
    ///   VPI and ER are taken from the min/max pair closest to 
    ///   0v, regardless of the sign of the slope.
    /// In some instances the data will not contain data at either
    /// end of the array to allow detection of turning points. 
    /// If no MAX or MIN points are found the calculation should 
    /// use data from the ends of the array.
    /// 
    /// Rules for negative chirp :
    ///   VQuad, VPI and ER are located on the negative slope closest to 0v
    ///</remarks>
    public sealed class Alg_MZAnalysis
    {
        /// <summary>
        /// Empty private constructor
        /// </summary>
        private Alg_MZAnalysis()
        {
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a ZERO CHIRP MZ characteristic.
        /// </summary>
        /// <remarks>
        /// Use this method if you wish to calculate the MIN and MAX turning points
        /// outside this algorithm.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="turning_Points">Structure containing the turning points within the Y data</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpAnalysis(double[] voltageArray, double[] powerArray, Alg_MZLVMinMax.LVMinMax turning_Points, bool power_in_dBm)
        {
            // PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }

            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            if (power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            }


            // max or a min at the ends of the array are not true turning points, but
            // if the scan has not collected sufficient data it is valid to use one of them for VQuad
            // however it is a bad idea to add these points when 0v is the end point of the scan
            Alg_MZLVMinMax.LVMinMax turningPoints;
            if (voltageArray[0] != 0 && voltageArray[voltageArray.Length - 1] != 0)
            {
                turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turning_Points);
            }
            else
            {
                turningPoints = turning_Points;
            }


            MZAnalysis results = new MZAnalysis();
            
            double[] peakTPs = turningPoints.VoltagesForAllPeaks();
            double[] valleyTPs = turningPoints.VoltagesForAllValleys();
            if (peakTPs.Length == 0)
            {
                throw new AlgorithmException("Unable to analyse MZ data. No peaks found in data.");
            } 
            if (valleyTPs.Length == 0)
            {
                throw new AlgorithmException("Unable to analyse MZ data. No valleys found in data.");
            }

            int closestMAXTo0v = Alg_ArrayFunctions.FindIndexOfNearestElement(peakTPs, 0);
            int closestMINTo0v = Alg_ArrayFunctions.FindIndexOfNearestElement(valleyTPs, 0);

            // ER & VPi
            results.ExtinctionRatio_dB = turningPoints.PeakData[closestMAXTo0v].Power_dBm - turningPoints.ValleyData[closestMINTo0v].Power_dBm;
            results.Vpi = Math.Abs(turningPoints.PeakData[closestMAXTo0v].Voltage - turningPoints.ValleyData[closestMINTo0v].Voltage);
            results.VoltageAtMax = turningPoints.PeakData[closestMAXTo0v].Voltage;
            results.PowerAtMax_dBm = turningPoints.PeakData[closestMAXTo0v].Power_dBm;
            results.VoltageAtMin = turningPoints.ValleyData[closestMINTo0v].Voltage;
            results.PowerAtMin_dBm = turningPoints.ValleyData[closestMINTo0v].Power_dBm;

            // Calculate all potential VQuad points
            ArrayList quadPointsbyInflection = new ArrayList();
            ArrayList quadPointsByPower = new ArrayList();
            foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
            {
                bool found = false;
                int maxIndex = 0;
                double maxPower = Double.MinValue;

                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    // Search for a positive slope
                    if (maxPoint.Voltage > minPoint.Voltage)
                    {
                        maxIndex = maxPoint.Index;
                        maxPower = maxPoint.Power_dBm;
                        found = true;
                        break;
                    }
                }

                // If we found a positive slope that uses this MIN we can analyse it.
                if (found)
                {
                    // METHOD 1
                    // Use point of inflection to find the point at which the gradient is at a MAX
                    // Power must be in mW
                    double[] power_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray_dBm);
                    Alg_FindPointOfInflection.PointOfInflection potentialQuadPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, power_mW, minPoint.Index, maxIndex);
                    if (potentialQuadPoint.Found)
                    {
                        quadPointsbyInflection.Add(potentialQuadPoint);
                    }

                    // METHOD 2
                    // Use half power point, 3dB down from Peak
                    Alg_MZLVMinMax.DataPoint QuadPoint3dB = new Alg_MZLVMinMax.DataPoint();
                    QuadPoint3dB.Power_dBm = maxPower - 3;
                    double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, minPoint.Index, maxIndex);
                    double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, minPoint.Index, maxIndex);
                    // False peaks at the extremes of the sweep may not have a 3dB point
                    double minPwr = Alg_PointSearch.FindMinValueInArray(subArray_Pwr);
                    if (QuadPoint3dB.Power_dBm < minPwr)
                    {
                        // Not found within data
                        QuadPoint3dB.Voltage = Double.NaN;
                    }
                    else
                    {
                        QuadPoint3dB.Voltage = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, QuadPoint3dB.Power_dBm);
                    }
                    quadPointsByPower.Add(QuadPoint3dB);
                }
            }

            // Check all method 1 data
            // Copy the candidate quad points into an array.
            double[] quadPointVoltages = new double[quadPointsbyInflection.Count];
            for (int i = 0; i < quadPointsbyInflection.Count; i++ )
            {
                Alg_FindPointOfInflection.PointOfInflection poi = (Alg_FindPointOfInflection.PointOfInflection)quadPointsbyInflection[i];
                quadPointVoltages[i] = poi.XValue;
            }
            int closestQUADTo0v = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltages, 0);

            // Get details of the closest one to 0v
            Alg_FindPointOfInflection.PointOfInflection quadPoint = (Alg_FindPointOfInflection.PointOfInflection)quadPointsbyInflection[closestQUADTo0v];
            results.VImb = quadPoint.XValue;



            // Repeat for method 2 data
            // Copy the candidate quad points into an array.
            double[] quadPointVoltagesByPower = new double[quadPointsByPower.Count];
            for (int i = 0; i < quadPointsByPower.Count; i++)
            {
                Alg_MZLVMinMax.DataPoint poi = (Alg_MZLVMinMax.DataPoint)quadPointsByPower[i];
                quadPointVoltagesByPower[i] = poi.Voltage;
            }
            int closestQUADTo0v_byPower = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltagesByPower, 0);

            // Get details of the closest one to 0v
            Alg_MZLVMinMax.DataPoint quadPointByPower = (Alg_MZLVMinMax.DataPoint)quadPointsByPower[closestQUADTo0v_byPower];
            results.VQuad = quadPointByPower.Voltage;

            return results;
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, powerArray, 5, power_in_dBm);
            return ZeroChirpAnalysis(voltageArray, powerArray, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a zero chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return ZeroChirpAnalysis(bothArmsVoltage, bothArmsPower, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from ZERO CHIRP MZ power tap data.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpTapAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return ZeroChirpTapAnalysis(bothArmsVoltage, bothArmsPower, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from ZERO CHIRP MZ power tap data.
        /// </summary>
        /// <remarks>
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis ZeroChirpTapAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            // Tap analysis works on current rather than power.
            // Shift power array so that there are no negative numbers.
            double min = Alg_PointSearch.FindMinValueInArray(powerArray);
            double[] normalisedPower = Alg_ArrayFunctions.SubtractFromEachArrayElement(powerArray, min);
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, normalisedPower, 5, power_in_dBm);
            return ZeroChirpAnalysis(voltageArray, normalisedPower, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// Use this method if you wish to calculate the MIN and MAX turning points
        /// outside this algorithm.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="turning_Points">Structure containing the turning points within the Y data</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpAnalysis(double[] voltageArray, double[] powerArray, Alg_MZLVMinMax.LVMinMax turning_Points, bool power_in_dBm)
        {
            // PRECONDITIONS
            if (voltageArray.Length != powerArray.Length)
            {
                throw new AlgorithmException("Voltage and power data must contain the same number of elements. Voltage data contains " + voltageArray.Length + " and power data contains " + powerArray.Length);
            }
            if (voltageArray.Length == 0)
            {
                throw new AlgorithmException("Voltage and power arrays are empty");
            }
            if (voltageArray[0] > voltageArray[voltageArray.Length - 1])
            {
                throw new AlgorithmException("Voltage data must be in ascending order");
            }

            // Convert power to dBm, if necessary
            double[] powerArray_dBm = null;
            if (power_in_dBm)
            {
                powerArray_dBm = (double[])powerArray.Clone();
            }
            else
            {
                powerArray_dBm = Alg_PowConvert_dB.Convert_mWtodBm(powerArray);
            }


            // max or a min at the ends of the array are not true turning points, but
            // if the scan has not collected sufficient data it is valid to use one of them for VQuad
            // however it is a bad idea to add these points when 0v is the end point of the scan
            Alg_MZLVMinMax.LVMinMax turningPoints;
            if (voltageArray[0] != 0 && voltageArray[voltageArray.Length - 1] != 0)
            {
                turningPoints = addArrayEndPoints(voltageArray, powerArray_dBm, turning_Points);
            }
            else
            {
                turningPoints = turning_Points;
            }

            // Calculate all potential results combinations
            ArrayList candidateResults = new ArrayList();
            foreach (Alg_MZLVMinMax.DataPoint minPoint in turningPoints.ValleyData)
            {
                bool found = false;
                Alg_MZLVMinMax.DataPoint localMax = new Alg_MZLVMinMax.DataPoint();
                localMax.Index = 0;
                localMax.Power_dBm = Double.MinValue;
                localMax.Voltage = Double.MinValue;

                foreach (Alg_MZLVMinMax.DataPoint maxPoint in turningPoints.PeakData)
                {
                    // Search for a negative slope
                    if (maxPoint.Voltage < minPoint.Voltage)
                    {
                        localMax = maxPoint;
                        found = true;
                        break;
                    }
                }

                // If we found a negative slope that uses this MIN we can analyse it.
                if (found)
                {
                    MZAnalysis results = new MZAnalysis();

                    // METHOD 1
                    // Use point of inflection to find the point at which the gradient is at a MAX
                    // Power must be in mW
                    double[] power_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerArray_dBm);
                    Alg_FindPointOfInflection.PointOfInflection potentialQuadPoint = Alg_FindPointOfInflection.FindPointOfInflection(voltageArray, power_mW, localMax.Index, minPoint.Index );
                    if (potentialQuadPoint.Found)
                    {
                        results.VImb = potentialQuadPoint.XValue;
                    }

                    // METHOD 2
                    // Use half power point, 3dB down from Peak
                    double[] subArray_Pwr = Alg_ArrayFunctions.ExtractSubArray(powerArray_dBm, localMax.Index, minPoint.Index);
                    double[] subArray_V = Alg_ArrayFunctions.ExtractSubArray(voltageArray, localMax.Index, minPoint.Index);

                    results.VQuad = XatYAlgorithm.Calculate(subArray_V, subArray_Pwr, localMax.Power_dBm - 3);

                    // Populate the rest of the struct
                    results.PowerAtMax_dBm = localMax.Power_dBm;
                    results.PowerAtMin_dBm = minPoint.Power_dBm;
                    results.VoltageAtMax = localMax.Voltage;
                    results.VoltageAtMin = minPoint.Voltage;
                    results.Vpi = Math.Abs(localMax.Voltage - minPoint.Voltage);
                    results.ExtinctionRatio_dB = localMax.Power_dBm - minPoint.Power_dBm;

                    candidateResults.Add(results);
                }
            }

            // Check all method 1 data
            // Copy the candidate quad points into an array.
            double[] quadPointVoltages = new double[candidateResults.Count];
            for (int i = 0; i < candidateResults.Count; i++)
            {
                MZAnalysis results = (MZAnalysis)candidateResults[i];
                quadPointVoltages[i] = results.VImb;
            }
            // return the closest one to 0v
            int closestQUADTo0v = Alg_ArrayFunctions.FindIndexOfNearestElement(quadPointVoltages, 0);
            return (MZAnalysis)candidateResults[closestQUADTo0v];
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, powerArray, 5, power_in_dBm);
            return NegChirpAnalysis(voltageArray, powerArray, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from a negative chirp MZ characteristic.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return NegChirpAnalysis(bothArmsVoltage, bothArmsPower, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from negative chirp MZ tap data.
        /// </summary>
        /// <remarks>
        /// This method performs no additional processing of the data.
        /// </remarks>
        /// <param name="voltageArray">Array of voltage</param>
        /// <param name="powerArray">Array of power</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpTapAnalysis(double[] voltageArray, double[] powerArray, bool power_in_dBm)
        {
            // Tap analysis works on current rather than power.
            // Shift power array so that there are no negative numbers.
            double min = Alg_PointSearch.FindMinValueInArray(powerArray);
            double[] normalisedPower = Alg_ArrayFunctions.SubtractFromEachArrayElement(powerArray, min);
            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(voltageArray, normalisedPower, 5, power_in_dBm);
            return NegChirpAnalysis(voltageArray, normalisedPower, turningPoints, power_in_dBm);
        }

        /// <summary>
        /// Calculates ER, VPI and VQuad from negative chirp MZ power tap data.
        /// </summary>
        /// <remarks>
        /// This method joins data from two single ended sweeps before analysis.
        /// </remarks>
        /// <param name="leftArmVoltageArray">X axis data from the single ended left arm sweep.</param>
        /// <param name="rightArmVoltageArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="leftArmPowerArray">Power data from the single ended left arm sweep.</param>
        /// <param name="rightArmPowerArray">X axis data from the single ended right arm sweep.</param>
        /// <param name="power_in_dBm">True if power is in dBm, false if linear units</param>
        /// <returns>A structure containing all of the analysis data</returns>
        public static MZAnalysis NegChirpTapAnalysis(double[] leftArmVoltageArray, double[] rightArmVoltageArray, double[] leftArmPowerArray, double[] rightArmPowerArray, bool power_in_dBm)
        {
            // Stitch the single ended sweeps together
            double[] reversedRightArmPower = Alg_ArrayFunctions.ReverseArray(rightArmPowerArray);
            double[] reversedRightArmVoltage = Alg_ArrayFunctions.ReverseArray(rightArmVoltageArray);
            double[] positiveReversedRightArmVoltage = Alg_ArrayFunctions.MultiplyEachArrayElement(reversedRightArmVoltage, -1);


            double[] bothArmsVoltage = Alg_ArrayFunctions.JoinArrays(leftArmVoltageArray, positiveReversedRightArmVoltage);
            double[] bothArmsPower = Alg_ArrayFunctions.JoinArrays(leftArmPowerArray, reversedRightArmPower);

            Alg_MZLVMinMax.LVMinMax turningPoints = Alg_MZLVMinMax.MZLVMinMax(bothArmsVoltage, bothArmsPower, 5, power_in_dBm);
            return NegChirpTapAnalysis(bothArmsVoltage, bothArmsPower, power_in_dBm);
        }

        /// <summary>
        /// Adds false turning points at either end of the array to enable the analysis to work
        /// within the end regions of the data.
        /// </summary>
        /// <param name="voltageArray">Voltage data</param>
        /// <param name="powerArray_dBm">Power data in dBm</param>
        /// <param name="turningPoints">A structure holding turning points</param>
        /// <returns>A new structure containing the original turning points plus a maxima or a mimima as appropriate at each end if the dataset</returns>
        private static Alg_MZLVMinMax.LVMinMax addArrayEndPoints(double[] voltageArray, double[] powerArray_dBm, Alg_MZLVMinMax.LVMinMax turningPoints)
        {
            ArrayList peaks = new ArrayList(turningPoints.PeakData.Length);
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.PeakData)
            {
                peaks.Add(dataPoint);
            }

            ArrayList valleys = new ArrayList(turningPoints.ValleyData.Length);
            foreach (Alg_MZLVMinMax.DataPoint dataPoint in turningPoints.ValleyData)
            {
                valleys.Add(dataPoint);
            }

            int lastElement = voltageArray.Length - 1;
            
            // End point
            Alg_MZLVMinMax.DataPoint endPoint = new Alg_MZLVMinMax.DataPoint();
            endPoint.Index = lastElement;
            endPoint.Power_dBm = powerArray_dBm[lastElement];
            endPoint.Voltage = voltageArray[lastElement];

            // Start point
            Alg_MZLVMinMax.DataPoint startPoint = new Alg_MZLVMinMax.DataPoint();
            startPoint.Index = 0;
            startPoint.Power_dBm = powerArray_dBm[0];
            startPoint.Voltage = voltageArray[0];

            // Determine gradient at each end of the data
            bool positiveSlopeAtStart = false; ;
            bool positiveSlopeAtEnd = false;

            // Check whether there are sufficient turning points detected
            if (turningPoints.PeakData.Length < 1 && turningPoints.ValleyData.Length < 1)
            {   // No peaks or valleys
                if (endPoint.Power_dBm > startPoint.Power_dBm)
                {
                    positiveSlopeAtEnd = true;
                    positiveSlopeAtStart = true;
                }
            }
            else
            {
                if (turningPoints.PeakData.Length < 1)
                {   // No peaks with a valley between means that data is "U" shaped.
                    positiveSlopeAtEnd = true;
                }
                if ( turningPoints.ValleyData.Length < 1 )
                {    // No valleys with a peak in the middle means that data is "^" shaped.
                     positiveSlopeAtStart = true;
                }
                // This should be the "usual" case
                if ( turningPoints.PeakData.Length >= 1 && turningPoints.ValleyData.Length >= 1 )
                {   // To find the sign of the gradient we can draw a straight line from the end of the array to the nearest turning point.
                    double firstTurningPoint_V = turningPoints.PeakData[0].Voltage < turningPoints.ValleyData[0].Voltage ? turningPoints.PeakData[0].Voltage : turningPoints.ValleyData[0].Voltage;
                    double lastTurningPoint_V = turningPoints.PeakData[turningPoints.PeakData.Length - 1].Voltage > turningPoints.ValleyData[turningPoints.ValleyData.Length - 1].Voltage ? turningPoints.PeakData[turningPoints.PeakData.Length - 1].Voltage : turningPoints.ValleyData[turningPoints.ValleyData.Length - 1].Voltage;

                    LinearLeastSquaresFit startFitCoeffs = LinearLeastSquaresFitAlgorithm.Calculate_ByXValue(voltageArray, powerArray_dBm, voltageArray[0], firstTurningPoint_V);
                    LinearLeastSquaresFit endFitCoeffs = LinearLeastSquaresFitAlgorithm.Calculate_ByXValue(voltageArray, powerArray_dBm, lastTurningPoint_V, voltageArray[lastElement]);
                    // Check gradient
                    if (endFitCoeffs.Slope > 0)
                    {
                        positiveSlopeAtEnd = true;
                    }
                    if (startFitCoeffs.Slope > 0)
                    {
                        positiveSlopeAtStart = true;
                    }
                }
            }


           
            // Deal with end point
            if ( positiveSlopeAtEnd )
            {
                peaks.Add(endPoint);            // Positive gradient near end (peak)
            }
            else
            {
                valleys.Add(endPoint);          // Negative gradient near end (valley)
            } 
            
            // Deal with start point
            if ( positiveSlopeAtStart )
            {
                valleys.Insert(0, startPoint);     // Positive gradient near start. (valley)
            }
            else
            {
                peaks.Insert(0, startPoint);   // Negative gradient near start (peak)
            }

            // Create return structure
            Alg_MZLVMinMax.DataPoint[] peakData = (Alg_MZLVMinMax.DataPoint[])peaks.ToArray(typeof(Alg_MZLVMinMax.DataPoint));
            Alg_MZLVMinMax.DataPoint[] valleyData = (Alg_MZLVMinMax.DataPoint[])valleys.ToArray(typeof(Alg_MZLVMinMax.DataPoint));
            
            Alg_MZLVMinMax.LVMinMax returnData = new Alg_MZLVMinMax.LVMinMax();
            returnData.PeakData = peakData;
            returnData.ValleyData = valleyData;
            
            return returnData;
        }

        /// <summary>
        /// Structure to hold the MZ Analysis return values
        /// </summary>
        public struct MZAnalysis
        {
            /// <summary>
            /// VPi
            /// </summary>
            public double Vpi;
            /// <summary>
            /// Imbalance correction voltage. 
            /// This will be similar to the quadrature voltage for some device families.
            /// </summary>
            public double VImb;
            /// <summary>
            /// Quadrature voltage calculated by using 3dB power level.
            /// </summary>
            public double VQuad;
            /// <summary>
            /// Extinction Ratio
            /// </summary>
            public double ExtinctionRatio_dB;
            /// <summary>
            /// Voltage for max power
            /// </summary>
            public double VoltageAtMax;
            /// <summary>
            /// Voltage for min power
            /// </summary>
            public double VoltageAtMin;
            /// <summary>
            /// Peak power value
            /// </summary>
            public double PowerAtMax_dBm;
            /// <summary>
            /// Minimum power value
            /// </summary>
            public double PowerAtMin_dBm;
        }
    }
}
