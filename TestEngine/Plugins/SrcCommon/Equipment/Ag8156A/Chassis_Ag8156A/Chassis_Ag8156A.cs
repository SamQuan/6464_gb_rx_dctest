// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestSolution.Chassis
//
// Chassis_8156A.cs
//
// Author: simon.ede, 2006
// As per 8156 driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Chassis for Agilent 8156A Optical Attenuator.
    /// </summary>
    public class Chassis_Ag8156A : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="visaResourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_Ag8156A(string chassisNameInit, string driverNameInit,
            string visaResourceString)
            : base(chassisNameInit, driverNameInit, visaResourceString)
        {
            // Setup expected valid hardware variants 
            // Add Ag8156 details.
            ChassisDataRecord ag8156AData = new ChassisDataRecord(
                "HEWLETT-PACKARD HP8156A",			// hardware name 
                "0",			// minimum valid firmware version 
                "1.05");		// maximum valid firmware version 
            ValidHardwareData.Add("Ag8156A", ag8156AData);

            // This chassis doesn't support reading the Status Byte in low level GPIB.
            // Inform the base class so it can adapt its calls
            this.ErrorCheckUse_488_2_Command = true;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string idn = "";

                //Attempt to get the IDN up to 3 times. (slow responding instrument)
                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        // Read the chassis ID string
                        idn = Query_Unchecked("*IDN?", null);
                        break;
                    }
                    catch
                    {
                        if (i == 2)
                        {
                            throw new ChassisException("Unable to obtain firmware version info from Ag8156A");
                        }
                        else
                        {
                            //Wait for 2 seconds
                            System.Threading.Thread.Sleep(2000);
                        }
                    }
                }

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string idn = "";

                //Attempt to get the IDN up to 3 times. (slow responding instrument)
                for (int i = 0; i < 3; i++)
                {
                    try
                    {
                        // Read the chassis ID string
                        idn = Query("*IDN?", null);

                        break;
                    }
                    catch
                    {
                        if (i == 2)
                        {
                            throw new ChassisException("Unable to obtain hardware identity info from Ag8156A");
                        }
                        else
                        {
                            //Wait for 2 seconds
                            System.Threading.Thread.Sleep(2000);
                        }
                    }
                }

                // Return field1, the manufacturer name and field 2, the model number
                string[] fields = idn.Split(',');
                return fields[0].Trim() + " " + fields[1].Trim();
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // NOTHING TO DO
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // Set up Chassis standard event register mask
                    // 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //clear the status registers
                    this.Write("*CLS", null);
                }
            }
        }
        #endregion
    }
}
