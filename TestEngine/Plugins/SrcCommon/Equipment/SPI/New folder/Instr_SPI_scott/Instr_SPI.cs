// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_MPCB.cs
//
// Author: scott alexander, 2017
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;


namespace Bookham.TestSolution.Instruments
{
    /// <summary>Multi Purpose Control Board Instrument driver</summary>
    public partial class Instr_SPI : Instrument
    {
        #region Constructor

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_SPI(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
                chassisInit)
        {
            // Setup expected valid hardware variants 
            InstrumentDataRecord instrMpcb = new InstrumentDataRecord(
                "Oclaro MPCB",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 

            ValidHardwareData.Add("instrMpcb", instrMpcb);

            // Configure valid chassis driver information
            InstrumentDataRecord chassisMpcb = new InstrumentDataRecord(
                "Chassis_DE0",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version

            ValidChassisDrivers.Add("chassisMpcb", chassisMpcb);

            // initialise this instrument's chassis
            this.instrumentChassis = (Chassis_DE0)chassisInit;
            
            // Default to 1s
            writeReadDelay_ms = 1;
           // Default to 30s
            commsTimeout_ms = 30000;

            //Initialisation
            Init_MemoryMap();
            UserSweepConfigData = new Dictionary<SweepTypeCfgValues, List<ushort>>();
            //Init empty cal table
            if(CalibrationTable == null)
                CalibrationTable = new Dictionary<DIO_Channels, CalibrationItem>();

            if (PowerMeterCalFactor == null)
            {
                PowerMeterCalFactor = new Dictionary<PowerMeterChannel, double>();
                PowerMeterCalFactor.Add(PowerMeterChannel.POWERMETER_1, 0.0);
                PowerMeterCalFactor.Add(PowerMeterChannel.POWERMETER_2, 0.0);
                PowerMeterCalFactor.Add(PowerMeterChannel.POWERMETER_3, 0.0);
                PowerMeterCalFactor.Add(PowerMeterChannel.POWERMETER_4, 0.0);
            }


        }

        #endregion

        #region Instrument overrides

        private string firmwareVersion;
        /// <summary>
        /// Firmware version of this instrument.
        /// Leave it as Firmware_Unknown to allow the driver to load.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return "Firmware_Unknown";
            }
        }

     
        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return "Oclaro MPCB";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
           
        }


        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;

                if (value)
                {                 
                    

                }

            }
        }


        #endregion
                          

        //****************************************************************
        //
        //          DIO - DAC/ADC ACCESS COMMANDS
        //
        //**************************************************************

    
        /// <summary>Write the data to the DIO </summary>
        /// <param name="Channel">Device Channel</param>
        /// <param name="value">data to write in SI unit A or V</param>
        public void WriteDIO_Real(DIO_Channels Channel, double value)
        {

            if (Channel == DIO_Channels.InvalidChannel)
                return;

            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Channel);

            //Get Calibration Info
            CalibrationItem CalInfo = GetCalibrationItem(Channel);

            //Check the value is within bounds
            if ((value < CalInfo.MinReal) || (value > CalInfo.MaxReal) )
                throw new InstrumentException("WriteDIO_Real: Real value out of bounds for " + Channel.ToString() + Environment.NewLine +
                                              "Value = " + value.ToString() + " Max/Min = " + CalInfo.MaxReal.ToString() + "/" + CalInfo.MinReal.ToString());
      
            //Convert real value to adc/dac counts
            UInt16 Counts = CalInfo.ConvertRealToCounts(value);                      

            WriteDIO_AdcDac(Channel, Counts);
        
        }

        /// <summary>Write the data to the DIO </summary>
        /// <param name="Channel">Device Channel</param>
        /// <param name="value">data to write in SI unit A or V</param>
        public void WriteDIO(DIO_Channels Channel, double value)
        {

 
            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Channel);

            //Get Calibration Info
            CalibrationItem CalInfo = GetCalibrationItem(Channel);

            //Check the value is within bounds
            if ((value < CalInfo.MinReal) || (value > CalInfo.MaxReal))
                throw new InstrumentException("WriteDIO_Real: Real value out of bounds for " + Channel.ToString() + Environment.NewLine +
                                              "Value = " + value.ToString() + " Max/Min = " + CalInfo.MaxReal.ToString() + "/" + CalInfo.MinReal.ToString());

            //Convert real value to adc/dac counts
            UInt16 Counts = CalInfo.ConvertRealToCounts(value);

            WriteDIO_AdcDac(Channel, Counts);

        }

        /// <summary>
        /// Read a DIO Channel and return a calibrated double
        /// </summary>
        /// <param name="Channel">Device Channel</param>
        /// <returns>double in SI units A or V</returns>
        public double ReadDIO_Real(DIO_Channels Channel)
        {
            if (Channel == DIO_Channels.InvalidChannel)
                return -999.0;
            
            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Channel);

            //Get Calibration Info
            CalibrationItem CalInfo = GetCalibrationItem(Channel);

            //Read DAC from DE0
            UInt16 RegVal = ReadDIO_AdcDac(Channel);

            //Convert to calibrated double and return
            return CalInfo.ConvertCountsToReal(RegVal);

        }

        /// <summary>Write command via SPI </summary>
        /// <param name="Addr">Address to write </param>
        /// <param name="value">data to write</param>
        public void WriteDIO_SPI(UInt16 Addr, UInt16 value)
        {



            //Check the value is with bounds
 //           if ((value < CalInfo.MinCounts) || (value > CalInfo.MaxCounts))
 //               throw new InstrumentException("WriteDIO_AdcDac: Real value out of bounds for " + Channel.ToString() + Environment.NewLine +
  //                                            "Value = " + value.ToString() + " Max/Min = " + CalInfo.MinCounts.ToString() + "/" + CalInfo.MaxCounts.ToString());


            //Send Command to DE0
            byte[] AddressBytes = BitConverter.GetBytes(Addr);
            byte[] DataBytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(AddressBytes);
                Array.Reverse(DataBytes);

            }

            List<byte> Payload = new List<byte>();
            Payload.AddRange(AddressBytes);
            Payload.AddRange(DataBytes);

            instrumentChassis.SendCommandToDE0((byte)1, Payload.ToArray());

        }

        /// <summary>Write the data to the DIO </summary>
        /// <param name="Channel">Device Channel</param>
        /// <param name="value">data to write</param>
        public void WriteDIO_AdcDac(DIO_Channels Channel, UInt16 value)
        {

            if (Channel == DIO_Channels.InvalidChannel)
                return;
            
            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Channel);
            

            //Get Calibration Info to check bounds
            CalibrationItem CalInfo = GetCalibrationItem(Channel);


            //Check the value is with bounds
            if ((value < CalInfo.MinCounts) || (value > CalInfo.MaxCounts))
                throw new InstrumentException("WriteDIO_AdcDac: Real value out of bounds for " + Channel.ToString() + Environment.NewLine +
                                              "Value = " + value.ToString() + " Max/Min = " + CalInfo.MinCounts.ToString() + "/" + CalInfo.MaxCounts.ToString());
        

            //Send Command to DE0
            byte[] AddressBytes = BitConverter.GetBytes(RegInfo.Command);
            byte[] DataBytes = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(AddressBytes);
                Array.Reverse(DataBytes);

            }

            List<byte> Payload = new List<byte>();
            Payload.AddRange(AddressBytes);
            Payload.AddRange(DataBytes);

            instrumentChassis.SendCommandToDE0((byte)RegInfo.Routing, Payload.ToArray());

        }
        /// <summary>
        /// Read a DIO Channel and return a calibrated double
        /// </summary>
        /// <param name="Channel">Device Channel</param>
        /// <returns>UInt16</returns>
        public UInt16 ReadDIO_AdcDac(DIO_Channels Channel)
        {

            if (Channel == DIO_Channels.InvalidChannel)
                return 0;

            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Channel);      

            //Read DAC from DE0

            //Send Command to DE0
            byte[] AddressBytes = BitConverter.GetBytes(RegInfo.Command);           

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(AddressBytes);           
            }

            List<byte> Payload = new List<byte>();
            Payload.AddRange(AddressBytes);   

            byte[] RxBytes = instrumentChassis.SendCommandToDE0((byte)RegInfo.Routing, Payload.ToArray());
            
            //Check length is only 2 bytes!
            if (RxBytes.Length != 2)
                throw new InstrumentException("ReadDIO_AdcDac: " + RxBytes.Length + " bytes returned! Expected 2");

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(RxBytes);
            }
            
            return BitConverter.ToUInt16(RxBytes, 0);

        }


        //****************************************************************
        //                 DE0 COMMANDS
        //
        // All none DIO commands are to be wrapped since the user can't
        // be expected to know/look up the various command structures
        // 
        //**************************************************************

        //**************************************************************
        //          DE0 - Numerical Command Access
        //Commands that write/read only numerical values represented by a number of bytes.
        //***************************************************************


        private byte[] FormatNumericalDataBytes(UInt32 value, DE0_RegisterItem RegInfo)
        {          
               
            if(RegInfo.TxDataBytesLength < 1)
                throw new InstrumentException("FormatTxDataBytes: This command cannot be used for none numerical commands");

            //Check for max
            uint MaxScaledValue = (uint)Math.Pow(2, (RegInfo.TxDataBytesLength * 8));
            if (value > MaxScaledValue)
                throw new InstrumentException("FormatTxDataBytes: Value exceeds maximum that can be represented using " + RegInfo.TxDataBytesLength.ToString() + " bytes");

            //Convert value to byte array - this will default to little endian on an windows PC
            byte[] AllDataByte = BitConverter.GetBytes(value);

            //We have checked the size of the integer so we need to bin the empty bytes
            byte[] TxDataByte = new byte[RegInfo.TxDataBytesLength];

            //Reverse bytes if needed
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(AllDataByte);
                Array.Copy(AllDataByte, AllDataByte.Length - RegInfo.TxDataBytesLength, TxDataByte, 0, RegInfo.TxDataBytesLength);           

            }
            else
            {
                Array.Copy(AllDataByte, 0, TxDataByte, 0, RegInfo.TxDataBytesLength);
            }

            return TxDataByte;

        }



        private byte[] FormatAddressBytes(DE0_RegisterItem RegInfo)
        {            

            byte[] AddressBytes = BitConverter.GetBytes(RegInfo.Command);  
         
            if (BitConverter.IsLittleEndian) Array.Reverse(AddressBytes);

            List<byte> AddressByteList = new List<byte>();

            //Check for 1 or 2 byte command, two byte commands are DIO and top gen access
            //for 1 byte commands dump the last byte in the array.

            if ((RegInfo.Routing == DE0_DeviceAccessId.DioAccess) || (RegInfo.Routing == DE0_DeviceAccessId.TopGenMemAccess))
            {
                AddressByteList.AddRange(AddressBytes);
            }
            else //singe byte - dump what is now the 0 index byte
            {
                //Check Size of address 
                if (RegInfo.Command > 256)
                    throw new InstrumentException("FormatAddressBytes: Value exceeds single byte");

                AddressByteList.Add(AddressBytes[1]);

            }                        

            //If needed insert sub routing byte at start of list
            if (RegInfo.SubRouting != DE0_SubDeviceAccessId.NO_SUB_ROUTE)
            {         
                AddressByteList.Insert(0,(byte)RegInfo.SubRouting);
                          
            }

            return AddressByteList.ToArray();
        }
                          


        protected void WriteDE0Cmd(DE0_Commands Command, uint value)
        {
                     
            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Command);

            byte[] TxData = FormatNumericalDataBytes(value, RegInfo);
            byte[] Address = FormatAddressBytes(RegInfo);

            List<byte> Payload = new List<byte>();
            Payload.AddRange(Address);
            Payload.AddRange(TxData);

            byte[] CmdRx = instrumentChassis.SendCommandToDE0((byte)RegInfo.Routing, Payload.ToArray());
                        
        }

        /// <summary>Write the data to the DE0 </summary>
        /// <param name="Channel">Device Channel</param>
        /// <param name="value">data to write</param>
        protected void WriteDE0Cmd(DE0_Commands Command, double value)
        {
            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Command);    
        
            //2s Comp Coversion Here

            UInt32 IntValue = (UInt32)(value * RegInfo.FloatConversionFactor);
            
            WriteDE0Cmd(Command, IntValue);          

        }


        /// <summary>Write the data to the DE0 </summary>
        /// <param name="Channel">Device Channel</param>
        /// <param name="value">data to write</param>
        protected uint ReadDE0Cmd_UInt32(DE0_Commands Command)
        {
            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Command);
            byte[] Address = FormatAddressBytes(RegInfo);
            byte[] RxBytes = instrumentChassis.SendCommandToDE0((byte)RegInfo.Routing, Address);


            //Pad bytes if less than 4
            byte[] FourByteArray = new byte[] { 0, 0, 0, 0 };
            Array.Copy(RxBytes, 0, FourByteArray, 4 - RxBytes.Length, RxBytes.Length);

            //Reverse if needed            
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(FourByteArray);

            }                       

            //Convert to unsigned integer
            UInt32 UIntegerValue = BitConverter.ToUInt32(FourByteArray, 0);
            return UIntegerValue;
            
        }

        protected double ReadDE0Cmd_double(DE0_Commands Command)
        {

            //Define max bytes
            UInt16 MaxBytes = sizeof(Int32);

            //GetRegsiterInfo
            DE0_RegisterItem RegInfo = GetRegisterInfo(Command);
            byte[] Address = FormatAddressBytes(RegInfo);
            byte[] RxBytes = instrumentChassis.SendCommandToDE0((byte)RegInfo.Routing, Address);

            //Reverse if needed            
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(RxBytes);
            }

            //Pad bytes if less than 4
            byte[] FourByteArray = new byte[] {0,0,0,0};
            Array.Copy(RxBytes, 0, FourByteArray, MaxBytes - RxBytes.Length, RxBytes.Length);                     

            //Convert to signed integer
            Int32 IntegerValue = BitConverter.ToInt32(FourByteArray, 0);
            //Bit shift according to RxBytes length
            IntegerValue >>= 8 * (MaxBytes - RxBytes.Length);

            return IntegerValue / RegInfo.FloatConversionFactor;


        }

        //****************************************************************
        //
        //          POWER METER COMMANDS
        //
        //**************************************************************

        public enum PowerMeterChannel
        {
            POWERMETER_1,
            POWERMETER_2,
            POWERMETER_3,
            POWERMETER_4
        }

        static private Dictionary<PowerMeterChannel, double> PowerMeterCalFactor;

        public void SetPowerMeterCalFactor_dB(PowerMeterChannel Channel, double CalFactor_dB)
        {
            PowerMeterCalFactor[Channel] = CalFactor_dB;
        }

        public double GetPowerMeterCalFactor_dB(PowerMeterChannel Channel)
        {
            return PowerMeterCalFactor[Channel];
        }

        public double ReadPowerMeter_dBm(PowerMeterChannel Channel)
        {

            DE0_Commands PowerMeterCommand = DE0_Commands.PowerMeter1Read; 

            switch (Channel)
            {

                case PowerMeterChannel.POWERMETER_1:
                    PowerMeterCommand = DE0_Commands.PowerMeter1Read;
                    break;

                case PowerMeterChannel.POWERMETER_2:
                    PowerMeterCommand = DE0_Commands.PowerMeter2Read;
                    break;

                case PowerMeterChannel.POWERMETER_3:
                    PowerMeterCommand = DE0_Commands.PowerMeter3Read;
                    break;

                case PowerMeterChannel.POWERMETER_4:
                    PowerMeterCommand = DE0_Commands.PowerMeter4Read;
                    break;

            }

            //Get RegInfo
            //DE0_RegisterItem RegInfo = GetRegisterInfo(PowerMeterCommand);

            ////Get Unsigned value
            //UInt32 ChannelValue = ReadDE0Cmd_UInt32(PowerMeterCommand);

            ////Check for 2 compliment based on number of data bits
            //UInt32 TwosCompBit = Convert.ToUInt32(Math.Pow(2, 8 * RegInfo.TxDataBytesLength - 1));
            //UInt32 FullRange = Convert.ToUInt32(Math.Pow(2, 8 * RegInfo.TxDataBytesLength));
            //bool IsTwosComp = ((ChannelValue & TwosCompBit) != 0) ? true : false;
            //double RetValue = (IsTwosComp) ? (FullRange - ChannelValue) / (RegInfo.FloatConversionFactor * -1.0) : ChannelValue / RegInfo.FloatConversionFactor;

            double RetValue = ReadDE0Cmd_double(PowerMeterCommand);
            RetValue += PowerMeterCalFactor[Channel];
           
            return RetValue;


        }
        
        //****************************************************************
        //
        //          TEC CONTROL COMMANDS
        //
        //**************************************************************

        private enum TemperatureStatusMask
        {
            ///<summary>Bit0</summary>
            Enabled = 0x01,
            /// <summary>Bit1</summary>
            Ready = 0x02,
            /// <summary>Bit2</summary>
            Warmup = 0x04,
            /// <summary>Bit3</summary>
            Fault = 0x08,
            /// <summary>Bit4</summary>
            ProtectEnabled = 0x10,
            /// <summary>Bit5</summary>
            UnderTemperature = 0x20,
            /// <summary>Bit6</summary>
            OverTemperature = 0x40,
            /// <summary>Bit7</summary>
            Shutdown = 0x80
        }

        public struct TemperatureStatus
        {
            public bool Enabled;
            public bool Ready;
            public bool Warmup;
            public  bool Fault;
            public  bool ProtectEnabled;
            public bool UnderTemperature;
            public  bool OverTemperature;
            public bool Shutdown;
        }



        public enum TecControl
        {         
            LASER,
            MZ,
            ETALON
        }


        public void SetTecTemperature(TecControl Tec, double Temperature_C)
        {
            if (Temperature_C < 0)
                throw new InstrumentException("SetTecTemperature: Temperture cannot be less that 0C");

            DE0_Commands TecCommand = DE0_Commands.DsdbrTempSetPoint;

            switch (Tec)
            {
                case TecControl.LASER:
                    TecCommand = DE0_Commands.DsdbrTempSetPoint;
                    break;

                case TecControl.MZ:
                    TecCommand = DE0_Commands.MzTempSetPoint;
                    break;

                case TecControl.ETALON:
                    TecCommand = DE0_Commands.EtalonTempSetPoint;
                    break;
            }
            
            DE0_RegisterItem RegInfo = GetRegisterInfo(TecCommand);  
            
            //Assume no twos comp
            UInt32 ValueToSend = Convert.ToUInt32(Temperature_C * RegInfo.FloatConversionFactor);
                    
            WriteDE0Cmd(TecCommand, ValueToSend);

        }

        public double GetTecTempSetPoint(TecControl Tec)
        {
       

            DE0_Commands TecCommand = DE0_Commands.DsdbrTempSetPoint;

            switch (Tec)
            {
                case TecControl.LASER:
                    TecCommand = DE0_Commands.DsdbrTempSetPoint;
                    break;

                case TecControl.MZ:
                    TecCommand = DE0_Commands.MzTempSetPoint;
                    break;

                case TecControl.ETALON:
                    TecCommand = DE0_Commands.EtalonTempSetPoint;
                    break;
            }

            DE0_RegisterItem RegInfo = GetRegisterInfo(TecCommand);          

            double RetVal = ReadDE0Cmd_double(TecCommand);

            return RetVal;


        }

        public double ReadTecTemperature(TecControl Tec)
        {

            DE0_Commands TecCommand = DE0_Commands.DsdbrTemperature;

            switch (Tec)
            {
                case TecControl.LASER:
                    TecCommand = DE0_Commands.DsdbrTemperature;
                    break;

                case TecControl.MZ:
                    TecCommand = DE0_Commands.MzTemperature;
                    break;

                case TecControl.ETALON:
                    TecCommand = DE0_Commands.EtalonTemperature;
                    break;
            }

            DE0_RegisterItem RegInfo = GetRegisterInfo(TecCommand);          

            double RetValue = ReadDE0Cmd_double(TecCommand);

            return RetValue;

        }


        public void SetTecEnable(TecControl Tec, bool EnableControl)
        {
            DE0_Commands TecCommand = DE0_Commands.DsdbrTempSetPoint;

            switch (Tec)
            {
                case TecControl.LASER:
                    TecCommand = DE0_Commands.DsdbrTempControl;
                    break;

                case TecControl.MZ:
                    TecCommand = DE0_Commands.MzTempControl;
                    break;

                case TecControl.ETALON:
                    TecCommand = DE0_Commands.EtalonTempControl;
                    break;
            }

            WriteDE0Cmd(TecCommand, ((EnableControl) ? (uint)1 : (uint)0));

        }


        public TemperatureStatus GetTecStatus(TecControl Tec)
        {
            DE0_Commands TecCommand = DE0_Commands.DsdbrTempSetPoint;

            switch (Tec)
            {
                case TecControl.LASER:
                    TecCommand = DE0_Commands.DsdbrTempControl;
                    break;

                case TecControl.MZ:
                    TecCommand = DE0_Commands.MzTempControl;
                    break;

                case TecControl.ETALON:
                    TecCommand = DE0_Commands.EtalonTempControl;
                    break;
            }


            UInt32 TECStatus = ReadDE0Cmd_UInt32(TecCommand);


            TemperatureStatus TempStatus = new TemperatureStatus();
            TempStatus.Enabled = ((TECStatus &  (uint)TemperatureStatusMask.Enabled) ==  (uint)TemperatureStatusMask.Enabled)? true: false;
            TempStatus.Ready = ((TECStatus & (uint)TemperatureStatusMask.Ready) == (uint)TemperatureStatusMask.Ready) ? true : false;
            TempStatus.OverTemperature = ((TECStatus & (uint)TemperatureStatusMask.OverTemperature) == (uint)TemperatureStatusMask.OverTemperature) ? true : false;
            TempStatus.Fault = ((TECStatus & (uint)TemperatureStatusMask.Fault) == (uint)TemperatureStatusMask.Fault) ? true : false;
            TempStatus.ProtectEnabled = ((TECStatus & (uint)TemperatureStatusMask.ProtectEnabled) == (uint)TemperatureStatusMask.ProtectEnabled) ? true : false;
            TempStatus.Shutdown = ((TECStatus & (uint)TemperatureStatusMask.Shutdown) == (uint)TemperatureStatusMask.Shutdown) ? true : false;
            TempStatus.UnderTemperature = ((TECStatus & (uint)TemperatureStatusMask.UnderTemperature) == (uint)TemperatureStatusMask.UnderTemperature) ? true : false;
            TempStatus.Warmup = ((TECStatus & (uint)TemperatureStatusMask.Warmup) == (uint)TemperatureStatusMask.Warmup) ? true : false;

            return TempStatus;

        }

        
      

        public void SetTecPID(TecControl Tec, UInt16 PropGain, UInt16 IntGain, UInt16 DiffGain)
        {
            DE0_Commands PropCommand = DE0_Commands.DsdbrTempPropGain;
            DE0_Commands IntCommand = DE0_Commands.DsdbrTempIntegralGain;
            DE0_Commands DiffCommand = DE0_Commands.DsdbrTempDiffGain;

            switch (Tec)
            {
                case TecControl.LASER:
                    PropCommand = DE0_Commands.DsdbrTempPropGain;
                    IntCommand = DE0_Commands.DsdbrTempIntegralGain;
                    DiffCommand = DE0_Commands.DsdbrTempDiffGain;
                    break;
                case TecControl.MZ:
                    PropCommand = DE0_Commands.DsdbrTempPropGain;
                    IntCommand = DE0_Commands.DsdbrTempIntegralGain;
                    DiffCommand = DE0_Commands.DsdbrTempDiffGain;
                    break;
   
                case TecControl.ETALON:
                    PropCommand = DE0_Commands.DsdbrTempPropGain;
                    IntCommand = DE0_Commands.DsdbrTempIntegralGain;
                    DiffCommand = DE0_Commands.DsdbrTempDiffGain;
                    break;
                    
            }


            WriteDE0Cmd(PropCommand, PropGain);
            WriteDE0Cmd(IntCommand, IntGain);
            WriteDE0Cmd(DiffCommand, DiffGain);

        }

        public void GetTecPID(TecControl Tec,  out UInt16 PropGain, out UInt16 IntGain, out UInt16 DiffGain)
        {
            
            
            
            DE0_Commands PropCommand = DE0_Commands.DsdbrTempPropGain;
            DE0_Commands IntCommand = DE0_Commands.DsdbrTempIntegralGain;
            DE0_Commands DiffCommand = DE0_Commands.DsdbrTempDiffGain;

            switch (Tec)
            {
                case TecControl.LASER:
                    PropCommand = DE0_Commands.DsdbrTempPropGain;
                    IntCommand = DE0_Commands.DsdbrTempIntegralGain;
                    DiffCommand = DE0_Commands.DsdbrTempDiffGain;
                    break;
                case TecControl.MZ:
                    PropCommand = DE0_Commands.DsdbrTempPropGain;
                    IntCommand = DE0_Commands.DsdbrTempIntegralGain;
                    DiffCommand = DE0_Commands.DsdbrTempDiffGain;
                    break;

                case TecControl.ETALON:
                    PropCommand = DE0_Commands.DsdbrTempPropGain;
                    IntCommand = DE0_Commands.DsdbrTempIntegralGain;
                    DiffCommand = DE0_Commands.DsdbrTempDiffGain;
                    break;

            }

            PropGain = Convert.ToUInt16(ReadDE0Cmd_UInt32(PropCommand));
            IntGain = Convert.ToUInt16(ReadDE0Cmd_UInt32(IntCommand));
            DiffGain = Convert.ToUInt16(ReadDE0Cmd_UInt32(DiffCommand));
           

        }



        //****************************************************************
        //
        //          SWEEP COMMANDS
        //
        //**************************************************************




        public void SetupLinearSourceSweep(DIO_Channels ioId, double startVal_real, double stopVal_real, UInt32 numPts)
        {

            //Get config item
            CalibrationItem CalInfo = GetCalibrationItem(ioId);
            UInt16 startVal_dac = CalInfo.ConvertRealToCounts(startVal_real);
            UInt16 stopVal_dac = CalInfo.ConvertRealToCounts(stopVal_real);

            SetupLinearSourceSweep(ioId, startVal_dac, stopVal_dac, numPts);

        }
        
        /// <summary>
        /// Configure a linear sweep     
        /// </summary>
        /// <param name="ioId">Channel to sweep</param>
        /// <param name="startVal">start value</param>
        /// <param name="stopVal">stop value</param>
        /// <param name="numPts">number of sweep points</param>
        /// <param name="delay_us">Source-Delay-Measure value in uSecs</param>
        /// 
        public void SetupLinearSourceSweep(DIO_Channels ioId, UInt16 startVal_dac, UInt16 stopVal_dac, UInt32 numPts)
        {          

                    // We are in the DAC domain, so we need to make sure that the DAC step size is never less than 1
                    double sweepLen = Math.Abs(startVal_dac - stopVal_dac);

                    if ((sweepLen / numPts) < 1)
                        throw new InstrumentException("ConfigureSrcSweep : Calculated DAC stepsize < 1\r\n Please correct settings.");


                    DE0_RegisterItem Cmd_LinSourceSweepConfig = GetRegisterInfo(DE0_Commands.SweepSrcConfig);
                    DE0_RegisterItem Cmd_NumSweepPoints = GetRegisterInfo(DE0_Commands.SweepPts);
                    DE0_RegisterItem Cmd_SweepEnable = GetRegisterInfo(DE0_Commands.SweepEnable);    
                    DE0_RegisterItem ChannelReg =  GetRegisterInfo(ioId);      
                            
                    ushort chanId = ChannelReg.Command;                 
                             
                    byte[] ioIdArray = BitConverter.GetBytes(chanId);
                    byte[] startArray = BitConverter.GetBytes(Convert.ToUInt16(startVal_dac));
                    byte[] stopArray = BitConverter.GetBytes(Convert.ToUInt16(stopVal_dac));   
          
                    if (BitConverter.IsLittleEndian)
                    {
                        Array.Reverse(ioIdArray);
                        Array.Reverse(startArray);
                        Array.Reverse(stopArray);                      
                    }

                    List<byte> SweepConfigPayload = new List<byte>();
                    SweepConfigPayload.AddRange(FormatAddressBytes(Cmd_LinSourceSweepConfig));
                    SweepConfigPayload.AddRange(ioIdArray);
                    SweepConfigPayload.AddRange(startArray);
                    SweepConfigPayload.AddRange(stopArray);

                     //Send source sweep command
                    byte[] Rx = instrumentChassis.SendCommandToDE0((byte)Cmd_LinSourceSweepConfig.Routing, SweepConfigPayload.ToArray());                                              
                                           

                     //Send Points Command                  
                     WriteDE0Cmd(DE0_Commands.SweepPts, numPts);            

                     List<byte> SweepEnablePayload = new List<byte>();
                     SweepEnablePayload.AddRange(FormatAddressBytes(Cmd_SweepEnable));
                     SweepEnablePayload.Add(1);
                     SweepEnablePayload.AddRange(ioIdArray);                        

                    //Enable Sweep Channel
                     Rx = instrumentChassis.SendCommandToDE0((byte)Cmd_SweepEnable.Routing, SweepEnablePayload.ToArray());                                                         
                                                        
            
        }


        public ushort NumberofSweepPoints
        {

            set
            {
                WriteDE0Cmd(DE0_Commands.SweepPts, value);        
            }

            get
             {
                return Convert.ToUInt16(ReadDE0Cmd_UInt32(DE0_Commands.SweepPts));        
            }

        }


         /// 
        public void SetGlobalSourceMeasureDelay(UInt32 delay_us)
        {
            WriteDE0Cmd(DE0_Commands.SweepSrcMeasDelay, delay_us);
          
        }

        public UInt32 GetGlobalSourceMeasureDelay_us()
        {
            
            return ReadDE0Cmd_UInt32(DE0_Commands.SweepSrcMeasDelay);

        }

           


        public void SetupCustomSourceSweep(DIO_Channels ioId, List<double> sweepSteps_real)
        {

            //Get Cal info
            CalibrationItem CalInfo = GetCalibrationItem(ioId);

            List<UInt16> sweepSteps_dac = new List<UInt16>();

            foreach (double SweepPoint in sweepSteps_real)
            {
                sweepSteps_dac.Add(CalInfo.ConvertRealToCounts(SweepPoint));

            }

            SetupCustomSourceSweep(ioId, sweepSteps_dac);

        }


        /// <summary>      
        /// Setup the user sweep config
        /// </summary>      
        /// <param name="sweepType">What type of non-linear sweep are we doing</param>
        /// <param name="sweepSteps">DAC sweep pts</param>
        /// <param name="delay_us">Source-Delay-Measure value in uSecs</param>
        public void SetupCustomSourceSweep(DIO_Channels ioId, List<UInt16> sweepSteps_dac)
        {                              
                //Assign custom sweep ID                                  
                SweepTypeCfgValues sweepType = SweepTypeCfgValues.UserDefined0;
                bool SweepConfigAllocated = false;

                //Check if our sweepuserconfig already exists by comparing the sweepSteps_dac list element by element
                foreach (KeyValuePair<SweepTypeCfgValues, List<UInt16>> KV in UserSweepConfigData)
                {
                    if (CheckListElementsAreEqual(KV.Value, sweepSteps_dac))
                    {
                        sweepType = KV.Key;
                        SweepConfigAllocated = true;
                        break;
                    }
                }        
                                    
                //if not assign a new one
                //Get next free config               
                if (SweepConfigAllocated == false)
                {
                    foreach (SweepTypeCfgValues SweepConfig in Enum.GetValues(typeof(SweepTypeCfgValues)))
                    {
                        //May need to add a constraint here based on int value of SweepTypeCfgValues if we add some special sweep configs.
                        if (!UserSweepConfigData.ContainsKey(SweepConfig))
                        {
                            sweepType = SweepConfig;
                            //Setup new config
                            this.UserSweepConfig(sweepType, sweepSteps_dac);               
                            SweepConfigAllocated = true;
                            break;
                        }
                    }
                }

                if(!SweepConfigAllocated)
                    throw new InstrumentException("Maximum Number of custom sweeps already defined, clear sweep configs to add more");
                
                //Set DIO channel to use custom sweep
                this.AssignUserSweepToChannel(ioId, sweepType);                                                              
        }

        private Dictionary<SweepTypeCfgValues, List<UInt16>> UserSweepConfigData;

        //Crappy .NET 2.0 with no Linq hack
        private bool CheckListElementsAreEqual(List<UInt16> List1, List<UInt16> List2)
        {

            if (List1.Count != List2.Count)
                return false;

            bool ElementsAreEqual = true;

            for(int i = 0; i < List1.Count; ++i)
            {
                if(List1[i] != List2[i]) ElementsAreEqual = false;
            }

            return ElementsAreEqual;
        }





        /// <summary>
        /// Special command to enable the user to configure a sweep 
        /// monitor channel in one go
        /// </summary>
        /// <param name="ioId">Channel to monitor</param>
        /// <param name="samples">Number of samples (averaging) per measurement point</param>
        public void ConfigureSenseSweep(DIO_Channels ioId, UInt16 samples)
        {

            DE0_RegisterItem Cmd_ConfigSenseSweep = GetRegisterInfo(DE0_Commands.SweepSensConfig);
            DE0_RegisterItem ChannelReg = GetRegisterInfo(ioId);

            ushort chanId = ChannelReg.Command;

            byte[] ioIdArray = BitConverter.GetBytes(chanId);
            byte[] SamplesArray = BitConverter.GetBytes(Convert.ToUInt16(samples));


            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ioIdArray);
                Array.Reverse(SamplesArray);                
            }
            
            List<byte> payload = new List<byte>();
            payload.AddRange(  FormatAddressBytes(Cmd_ConfigSenseSweep));
            payload.AddRange(ioIdArray);
            payload.AddRange(SamplesArray);            

            //Send source sweep command
           byte[] Rx = instrumentChassis.SendCommandToDE0((byte)Cmd_ConfigSenseSweep.Routing,   payload.ToArray());                                          
                                            
            //Enable RxSweep
           DE0_RegisterItem Cmd_SweepEnable = GetRegisterInfo(DE0_Commands.SweepEnable);    

           List<byte> SweepEnablePayload = new List<byte>();
           SweepEnablePayload.AddRange(FormatAddressBytes(Cmd_SweepEnable));
           SweepEnablePayload.Add(1);
           SweepEnablePayload.AddRange(ioIdArray);

           //Enable Sweep Channel
           Rx = instrumentChassis.SendCommandToDE0((byte)Cmd_SweepEnable.Routing, SweepEnablePayload.ToArray());  
   
        }

      

        /// <summary>
        /// Special command to enable the user to associate the sweep type location with a device channel
        /// </summary>
        /// <param name="ioId">Channel to sweep</param>
        /// <param name="sweepType">What type of non-linear sweep are we doing</param>
        public void AssignUserSweepToChannel(DIO_Channels ioId, SweepTypeCfgValues sweepType)
        {

            //Check that user sweep is defined
             if (!UserSweepConfigData.ContainsKey(sweepType))
                 throw new InstrumentException("UserSweepConfig must be defined before assigning to a DIO channel");                   
            

            DE0_RegisterItem Cmd_ConfigSweep = GetRegisterInfo(DE0_Commands.SweepSrcNonLinConfig);
            DE0_RegisterItem Cmd_SweepEnable = GetRegisterInfo(DE0_Commands.SweepEnable);
            DE0_RegisterItem ChannelReg = GetRegisterInfo(ioId);

            ushort chanId = ChannelReg.Command;
            byte[] ioIdArray = BitConverter.GetBytes(chanId);


            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ioIdArray);

            }

            List<byte> payload = new List<byte>();
            payload.AddRange(FormatAddressBytes(Cmd_ConfigSweep));
            payload.AddRange(ioIdArray);
            payload.Add((byte)sweepType);

            //Send source sweep command
            instrumentChassis.SendCommandToDE0((byte)Cmd_ConfigSweep.Routing,payload.ToArray());                                             
                                     
            
            List<byte> SweepEnablePayload = new List<byte>();
            SweepEnablePayload.AddRange(FormatAddressBytes(Cmd_SweepEnable));
            SweepEnablePayload.Add(1);
            SweepEnablePayload.AddRange(ioIdArray);

            //Enable Sweep Channel
            instrumentChassis.SendCommandToDE0((byte)Cmd_SweepEnable.Routing,SweepEnablePayload.ToArray());                                                
                                               

        }



        /// <summary>      
        /// Setup the user sweep config
        /// </summary>      
        /// <param name="sweepType">What type of non-linear sweep are we doing</param>
        /// <param name="sweepSteps">DAC sweep pts</param>
        public void UserSweepConfig(SweepTypeCfgValues sweepType, List<UInt16> sweepSteps_dac)
        {
            

            //limit to 32766 points since that must be enough for anyone!
            if (sweepSteps_dac.Count > 32766)
                throw new InstrumentException("UserSweepConfig : sweepSteps_dac limit is 32766");


            DE0_RegisterItem Cmd_NonLinSourceSweepConfig = GetRegisterInfo(DE0_Commands.SweepSrcNonLinConfig);
            DE0_RegisterItem Cmd_NonLinNumSweepPoints = GetRegisterInfo(DE0_Commands.SweepSrcNonLinSetPts);

            List<byte> payload = new List<byte>();

            // Sweep Type
            byte swpType = Convert.ToByte(sweepType);

            payload.AddRange(FormatAddressBytes(Cmd_NonLinNumSweepPoints));
            payload.Add(swpType);

            //index 0 since the sweeps will start here
            //Note this index could be used to extend the number of points beyond 32766
            payload.Add(0); //Byte1
            payload.Add(0); //Byte2

            //sweepPts
            foreach (UInt16 swpPt in sweepSteps_dac)
            {
                byte[] swpPtArray = BitConverter.GetBytes(swpPt);

                if (BitConverter.IsLittleEndian)
                    Array.Reverse(swpPtArray);

                payload.AddRange(swpPtArray);

            }

            //Send source sweep command
            instrumentChassis.SendCommandToDE0((byte)Cmd_NonLinNumSweepPoints.Routing,  payload.ToArray());

            //assign to our internal lookup
            if (UserSweepConfigData.ContainsKey(sweepType))
                UserSweepConfigData[sweepType] = sweepSteps_dac;
            else
                UserSweepConfigData.Add(sweepType, sweepSteps_dac);
            

        }



        /// <summary>Clear all sweep config</summary>     
        public void ClearAllSweepConfig()
        {
            //Clear internal lookup
            UserSweepConfigData.Clear();

            DE0_RegisterItem Cmd_DisableAll = GetRegisterInfo(DE0_Commands.SweepDisableAll);
            DE0_RegisterItem Cmd_ClearAll = GetRegisterInfo(DE0_Commands.SweepClearAllEntries);

            byte[] Rx = instrumentChassis.SendCommandToDE0((byte)Cmd_DisableAll.Routing,
                                                   FormatAddressBytes(Cmd_DisableAll));



            Rx = instrumentChassis.SendCommandToDE0((byte)Cmd_ClearAll.Routing,
                                                   FormatAddressBytes(Cmd_ClearAll));                           
                                                

        }

  
        /// <summary>Enable the IoId Sweep channels</summary>
        /// <param name="ioIdList">List of IoIds to be cleared</param>
        public void ClearSweepConfig(DIO_Channels ioID)
        {         
            
            DE0_RegisterItem Cmd_NonLinSourceSweepConfig = GetRegisterInfo(DE0_Commands.SweepSrcNonLinConfig);
            DE0_RegisterItem ChannelReg = GetRegisterInfo(ioID);
            DE0_RegisterItem Cmd_SweepEnable = GetRegisterInfo(DE0_Commands.SweepEnable);
            DE0_RegisterItem Cmd_ClearSweep = GetRegisterInfo(DE0_Commands.SweepClearEntry);


            ushort chanId = ChannelReg.Command;
            byte[] ioIdArray = BitConverter.GetBytes(chanId);            

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ioIdArray);               
            }


            List<byte> CustomSweepPayload = new List<byte>();
            CustomSweepPayload.AddRange(FormatAddressBytes(Cmd_NonLinSourceSweepConfig));
            CustomSweepPayload.AddRange(ioIdArray);

            //Check if custom sweep
            byte[] RxBytes = instrumentChassis.SendCommandToDE0((byte)Cmd_NonLinSourceSweepConfig.Routing,CustomSweepPayload.ToArray());          
                        
            List<byte> SweepEnablePayload = new List<byte>();           
            SweepEnablePayload.AddRange(FormatAddressBytes(Cmd_SweepEnable));
            SweepEnablePayload.Add(0);
            SweepEnablePayload.AddRange(ioIdArray);
            //Enable Sweep Channel
            instrumentChassis.SendCommandToDE0((byte)Cmd_SweepEnable.Routing, SweepEnablePayload.ToArray());
                             
                                                                
             //Clear Sweep
             List<byte> SweepClearPayload = new List<byte>();
             SweepClearPayload.AddRange(FormatAddressBytes(Cmd_ClearSweep));
             SweepClearPayload.AddRange(ioIdArray);
             instrumentChassis.SendCommandToDE0((byte)Cmd_SweepEnable.Routing,SweepClearPayload.ToArray());
                      

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="senseIoIds"></param>
        /// <param name="numPts"></param>
        /// <returns></returns>
        public void RunSweep(int SweepMaxTimeOut_ms)
        {           
            //Start Sweep
            WriteDE0Cmd(DE0_Commands.SweepState, 0x01);

            System.Diagnostics.Stopwatch SweepTimer = new System.Diagnostics.Stopwatch();
            SweepTimer.Reset();
            SweepTimer.Start();

            while  ((SweepTimer.ElapsedMilliseconds < SweepMaxTimeOut_ms) && ((ReadDE0Cmd_UInt32(DE0_Commands.SweepState) != 0)))
            {
                System.Threading.Thread.Sleep(100);
            } 
      
            //Stop Sweep
            WriteDE0Cmd(DE0_Commands.SweepState, 0x00);

        }


        public bool SweepInProgress              
        {
            get
            {
                return (ReadDE0Cmd_UInt32(DE0_Commands.SweepState) == 1) ? true : false;

            }
        }



        public double[] ReadSweepData_Real(DIO_Channels ioId)
        {

            UInt16[] SweepData = ReadSweepData_ADC(ioId);
            double[] RealSweepData = new double[SweepData.Length];

            CalibrationItem CalInfo = GetCalibrationItem(ioId);

            for (int i = 0; i < SweepData.Length; i++)
            {

                RealSweepData[i] = CalInfo.ConvertCountsToReal(SweepData[i]);
            }

            return RealSweepData;

        }

        /// <summary>       
        /// </summary>
        /// <param name="ioId">IO_ID being monitored during the sweep</param>    
        /// <returns>List of UInt16 (ADC) values</returns>
        public UInt16[] ReadSweepData_ADC(DIO_Channels ioId)
        {


            //Get Number of sweep points                 
            UInt16 NumberOfPoints =  Convert.ToUInt16(ReadDE0Cmd_UInt32(DE0_Commands.SweepPts));            

            DE0_RegisterItem ChannelReg = GetRegisterInfo(ioId);
            ushort chanId = ChannelReg.Command;
            byte[] ioIdArray = BitConverter.GetBytes(chanId);
            byte[] NumOfPointArray = BitConverter.GetBytes(NumberOfPoints);

            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ioIdArray);
                Array.Reverse(NumOfPointArray);
             
            }
            
             DE0_RegisterItem Cmd_SweepRead = GetRegisterInfo(DE0_Commands.SweepFetchData);


            List<byte> payload = new List<byte>();
         
            payload.AddRange(FormatAddressBytes(Cmd_SweepRead));
            payload.AddRange(ioIdArray);
            //Index 0
            payload.Add(0);
            payload.Add(0);
            //Number of points (Read all)
            payload.AddRange(NumOfPointArray);

            byte[] RxBytes = instrumentChassis.SendCommandToDE0((byte)Cmd_SweepRead.Routing, payload.ToArray());


            if (RxBytes.Length < NumberOfPoints)
                throw new InstrumentException("ReadSweepData_ADC: Incorrect number of bytes returned");

            List<UInt16> SweepData = new List<UInt16>();            
            //So we should have an array of 8 bit values, we need to reverse each pair and convert to Uint16
            for (int i = 0; i < RxBytes.Length; i += 2)
            {
                UInt16 newVal = BitConverter.ToUInt16(new byte[] { RxBytes[i], RxBytes[i + 1] }, 0);

                if (BitConverter.IsLittleEndian)
                    newVal = (UInt16)(newVal >> 8 | newVal << 8);

                SweepData.Add(newVal);
            }
                                                        

            return  SweepData.ToArray();
        }

      

        


        public enum SweepTypeCfgValues
        {
            ///// <summary>Linear sweep. See register 0x21. Read only.</summary>
            //Linear = 0x00,
            ///// <summary>Internal REAR nonlinear sweep</summary>
            //IntNonLinRear = 0x10,
            ///// <summary>Internal FRONT SECTION nonlinear sweep</summary>
            //IntNonLinFsPair = 0x11,
            /// <summary>User uploaded sweep 0</summary>
            UserDefined0 = 0x20,
            /// <summary>User uploaded sweep 1</summary>
            UserDefined1 = 0x21,
            /// <summary>User uploaded sweep 2</summary>
            UserDefined2 = 0x22,
            /// <summary>User uploaded sweep 3</summary>
            UserDefined3 = 0x23,
            /// <summary>User uploaded sweep 4</summary>
            UserDefined4 = 0x24,
            /// <summary>User uploaded sweep 5</summary>
            UserDefined5 = 0x25,
            /// <summary>User uploaded sweep 6</summary>
            UserDefined6 = 0x26,
            /// <summary>User uploaded sweep 3</summary>
            UserDefined7 = 0x27
        }



        //****************************************************************
        //
        //         MISC COMMANDS
        //
        //**************************************************************

        public void TriggerUV(bool UVOn)
        {
            UInt32 State = Convert.ToUInt32((UVOn) ? 1 : 0);
            WriteDE0Cmd(DE0_Commands.UVLEDTrig, State);
        }

        public void SetUVLevel(double Voltage)
        {
            WriteDIO_Real(DIO_Channels.UV_LED_Drive, Voltage);
        }


        public enum SOA_TYPE
        {
            MzX_PRE,
            MzX_POST,
            MzY_PRE,
            MzY_POST
        }

        public enum SOA_MODE
        {
            CURRENT,
            VOLTAGE,
          
        }

        public void SetSOA_VImode(SOA_TYPE SOA, SOA_MODE mode)
        {


            DE0_Commands Cmd_SOAVImode;
            
            switch(mode)
            {
                case SOA_MODE.VOLTAGE:
                    Cmd_SOAVImode = DE0_Commands.MzSoaSetVMode;
                    break;
                case SOA_MODE.CURRENT:
                    Cmd_SOAVImode = DE0_Commands.MzSoaSetIMode;
                    break;
                default:
                    Cmd_SOAVImode = DE0_Commands.MzSoaSetIMode;
                    break;
            }       
         

            switch (SOA)
            {
                case SOA_TYPE.MzX_PRE:
                    WriteDE0Cmd(Cmd_SOAVImode, Convert.ToUInt32(1));
                    break;

                case SOA_TYPE.MzX_POST:
                    WriteDE0Cmd(Cmd_SOAVImode, Convert.ToUInt32(2));
                    break;

                case SOA_TYPE.MzY_PRE:
                    WriteDE0Cmd(Cmd_SOAVImode, Convert.ToUInt32(3));
                    break;

                case SOA_TYPE.MzY_POST:
                    WriteDE0Cmd(Cmd_SOAVImode, Convert.ToUInt32(4));
                    break;

            }

            //Timing issue when switching mode, advise to use reverse current dac setting unless photocurrent monitoring is needed.
            System.Threading.Thread.Sleep(5000);

        }
        

        public void GroundAllDacs()
        {

            DE0_RegisterItem Reg = GetRegisterInfo(DE0_Commands.GroundDacs);
            ushort chanId = Reg.Command;                            
            List<byte> payload = new List<byte>();
            payload.AddRange(FormatAddressBytes(Reg));         
            byte[] RxBytes = instrumentChassis.SendCommandToDE0((byte)Reg.Routing, payload.ToArray());

        }


        //****************************************************************
        //**************************************************************

        /// <summary>Instrument's chassis</summary>
        private Chassis_DE0 instrumentChassis;
        private UInt32 commsTimeout_ms;
        /// <summary>Delay to allow the FW time to process the send command before receiving another command</summary>
        private readonly int writeReadDelay_ms;
        /// <summary>In the event of a comms error with the test board we will wait and try once more</summary>
        private int retryTimeout_ms = 1000;


    }
}
