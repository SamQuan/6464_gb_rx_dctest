// [Copyright]
//
// Bookham [Coherence RX DC Test]
// Bookham.TestSolution.TestControl
//
// TC_CoRxDcSPCTest.cs
//
// Author: alice.huang, 2011
// Design: [Reference design documentation]

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;
using System.IO;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Generic;
using Bookham.TestLibrary.Utilities;


namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test control for Corx spc test
    /// Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_CoRxDcSPCTest : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_CoRxDcSPCTest()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);
            
            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[1];
            this.testCtlList[0] = typeof(ManualLoadBatchDialogueCtl);  // mes offline            
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.MANUAL;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes,
            string operatorName, TestControlPrivilegeLevel operatorType)
        {
            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.inSimulationMode = testEngine.InSimulationMode;            
            this.node = mes.Node;
            DutInfo = null;
            ReadTestControlConfig(testEngine);
            if (!Util_SrCalCounter.CheckSrCalCounter(true))
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, 
                    " The splitter cal data is expired, \nto go on any test you have to run splitter calibration first"); 
             
            }

            testEngine.SendStatusMsg(" Loading batch...");
            List<string> stageList = null;
            try
            {
                if (operatorName == "EngDebug")
                {
                    stageList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "TestStageList.csv");
                }
                else
                {
                    stageList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "TestStageNoFW.csv");
                }

                //if (operatorType!= TestControlPrivilegeLevel.Technician)
                //{
                //    if (stageList.Contains("Sr Cal"))
                //    {
                //        stageList.Remove("Sr Cal");
                //    }
                //}
                
            }
            catch (Exception ex)
            {
                testEngine.ErrorRaise("Can't get test stage infomation available for this test control for " + ex.Message);
            }

            while (true)
            {
                // SHOW THE USER CONTROL
                testEngine.PageToFront(TestControlCtl.TestControl);
           
                testEngine.SelectTestControlCtlToDisplay(typeof(ManualLoadBatchDialogueCtl));
                List<string> codeList = null;
                try
                {
                    if (operatorName == "EngDebug")
                    {
                        codeList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "PartCodeList.csv");
                    }
                    else
                    {
                        codeList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "PartCodeList_SPC.csv");
                    }
                }
                catch (Exception ex)
                {
                    testEngine.ErrorRaise("Can't get test stage infomation available for this test control for " + ex.Message);
                }

                testEngine .SendToCtl (TestControlCtl .TestControl , typeof (ManualLoadBatchDialogueCtl ),
                            new DatumStringArray("PartCodeInfo",codeList.ToArray() ));
                testEngine.SendToCtl(TestControlCtl.TestControl,
                    typeof(ManualLoadBatchDialogueCtl),
                    new DatumStringArray("StageInfo", stageList.ToArray()));

              
                testEngine.GetUserAttention(TestControlCtl.TestControl);

                // wait for the message to say "OK" has been clicked...
                CtlMsg msg = testEngine.WaitForCtlMsg();
               
                this.DutInfo = (CoRxDCDutLoadInfo)msg.Payload;
                
                // if the GUI Msg is not available dut information, reload batch
                if (this.DutInfo == null)
                {
                    testEngine.SendStatusMsg(" invalid batch information!\n reloading batch information ...");
                    continue;
                }
                // If DutInfo is ok, set BatchId to SN
                batchID = DutInfo.DutSerialNumber;

                break;
            }  // while ( true) to get available batch information

            // ... Clear up GUI.
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine

            testEngine.SendStatusMsg("Batch load completed!");
            return DutInfo.DutSerialNumber;
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg,
            string operatorName, TestControlPrivilegeLevel operatorType)
        {

            DUTObject currentDut = dut;
            DatumList spcList = new DatumList();
            if (currentDut != null && currentDut.Attributes.IsPresent("SPC_RESULTS"))
            {
                spcList = currentDut.Attributes.ReadReference("SPC_RESULTS") as DatumList;
                spcList.AddOrUpdateString("TIME_DATE", DateTime.Now.ToString("yyyyMMddHHmmss"));
                SpcConfigReader.Initialise(currentDut.NodeID.ToString(), currentDut.TestStage);
                SpcHelper.SPC_Post_Checks(testEngine, currentDut.NodeID.ToString(), spcList);

            }

            if (this.programRanOnce) return null; // end of batch after one run!

            if ((!Util_SrCalCounter.CheckSrCalCounter(false)) && (DutInfo.TestStage != SrCalStageName ))
            {
                Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId optUser=
                    testEngine.ShowYesNoUserQuery(TestControlTab.TestControl, 
                    " The splitter cal data is expired,"+
                    "\nto go on any test you have to run splitter calibration first" +
                    "Do you want to run splitter calibration now?");
                if (optUser == Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId.Yes)
                {
                    DutInfo.TestStage = SrCalStageName;
                }
                else
                {
                    testEngine.ErrorRaise("Splitter calibration data expired, test can't go on");
                    return null;
                }
               
            }

            testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            testEngine.PageToFront(TestControlCtl.BatchView);

            //Comstruct DUTObject - test engine get required program, serial no etc. from here
            dut = new DUTObject();
            dut.BatchID = batchID;  //from Load Batch
            dut.SerialNumber = DutInfo.DutSerialNumber;
            // Equipment ID is based on network computer name
            //dut.EquipmentID = System.Net.Dns.GetHostName();
            // Set results node
            dut.NodeID = this.node;
            // Get Test Jig ID
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);

            //dut.EquipmentID = "SZN-CFP2-RX-DC01";
            dut.EquipmentID = EquitpmentReader.ReadEquipment(testEngine, dut);

            // TODO: CHANGE THE FOLLOWING LINES FOR REQUIRED PROGRAM AND DEVICE
            // Set generic code to be Part code
            dut.PartCode = DutInfo.PartCode;            
            dut.TestStage = DutInfo.TestStage;
            dut.Attributes.AddString("pcasStage", dut.TestStage);

            //if (!dut.TestStage.ToLower().Contains("phase") && !dut.TestStage.ToLower().Contains("sr cal") && lastTestStageName != "pincheck" && this.programStatus == ProgramStatus.Success)
            //{
            //    lastTestStageName = "pincheck";
            //    dut.TestStage = "PinCheck";
            //    //dut.PartCode = "pa014138";
            //}
            //else
            //{
            //    if (this.programStatus != ProgramStatus.Success)
            //    {
            //        return null;
            //    }
            //}
          
            if (dut.TestStage == SrCalStageName)
                dut.ProgramPluginName = "TP_CoRxSRCalTest";
            else if (dut.TestStage == DCTestStageName)
                dut.ProgramPluginName = "TP_CoRxDcTest";
            else if ((dut.TestStage == PhaseAngleStageName) || (dut.TestStage == PhaseAngleSPCStageName))
                dut.ProgramPluginName = "TP_CoRxPATest";
            else if ((dut.TestStage == DCStageName) || (dut.TestStage == DcTestSPCStageName) || (dut.TestStage == DcPreLidSPCStageName)||(dut.TestStage == DcPreLidStageName))
                dut.ProgramPluginName = "TP_CoRxDcTest_NewLimit";
            else if (dut.TestStage == pinCheckStageName)
                dut.ProgramPluginName = "TP_CoRxDcTest_PinCheck";

            dut.ProgramPluginVersion = "";            
            dut.Attributes.AddOrUpdateString("DeviceType", dut.PartCode);
            dut.Attributes.AddOrUpdateString("lot_type", "Standard");


            testEngine.GetUserAttention(TestControlCtl.BatchView);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);

            CtlMsg msg = testEngine.WaitForCtlMsg();
            testEngine.CancelUserAttention(TestControlCtl.BatchView);

            DutTestOption testOption = null;
            try
            {
                testOption = (DutTestOption)msg.Payload;
            }
            catch
            { }

            if (testOption == null) return null;

            // only allowed though here once!
            if (testOption.TestOption == enmTestOption.GoOnTest)
            {
                if (!dut.TestStage.ToLower().Contains("pincheck"))
                {
                    this.programRanOnce = true;
                }

                return dut;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            //TODO: Modify DUTOutcome object, 
            //If required: add details based on analysis of the test results, specify 
            //what specifications the test results are to be stored against plus any other supporting data.

            List<String> specsToUse = new List<String>();
            foreach (SpecResults specRes in testResults.SpecResults)
            {
                specsToUse.Add(specRes.Name);
            }
            DUTOutcome dutOutcome = new DUTOutcome();
            dutOutcome.OutputSpecificationNames = specsToUse.ToArray();
            if (programStatus == ProgramStatus.Success &&
                testResults.SpecResults[dutOutcome.OutputSpecificationNames[0]].Status.Status == PassFail.Fail  )
                programStatus = ProgramStatus.Failed;
            this.programStatus = programStatus;
            
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");

            try
            {
                var dropFiles = Directory.GetFiles(@"pcasc\temp\" + dut.NodeID, "*.pcasDrop");

                int filesCounts = dropFiles.Length;

                if (dut.TestStage.ToLower().Contains("phase") || dut.TestStage.ToLower().Contains("pincheck"))
                {
                    for (int i = 0; i < filesCounts; i++)
                    {
                        string dropFile = dropFiles[i];
                        string strName = System.IO.Path.GetFileName(dropFile); //返回 e.jpg 
                        File.Move(dropFile, @"pcasc\" + dut.NodeID + "\\" + strName);
                    }
                }
                else
                {
                    for (int i = 0; i < filesCounts; i++)
                    {
                        string dropFile = dropFiles[i];
                        string partCode = "";
                        using (StreamReader sr = new StreamReader(dropFile))
                        {
                            do
                            {
                                if (sr.ReadLine().Contains("~DEVICE TYPE"))
                                {
                                    partCode = sr.ReadLine();
                                    break;
                                }

                            } while (true);
                        }

                        if (partCode.ToUpper() != dut.Attributes.ReadString("TransCode"))
                        {
                            File.Delete(dropFile);
                        }
                        else
                        {
                            string strName = System.IO.Path.GetFileName(dropFile); //返回 e.jpg 
                            File.Move(dropFile, @"pcasc\" + dut.NodeID + "\\" + strName);
                        }
                    }

                    if (dut.Attributes.ReadString("TransCode") != dut.PartCode)
                    {
                        testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "此器件已经转CODE为：" + dut.Attributes.ReadString("TransCode"));
                    }

                }
            }
            catch (Exception)
            {

                int aa = 0;
            }


            
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");
                        
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), DutInfo);
            testEngine.SendStatusMsg("Restart Batch");
        }
        # endregion

        /// <summary>
        /// Read test control setting
        /// </summary>
        /// <param name="engine"></param>
        private void ReadTestControlConfig(ITestEngine engine)
        {
            if (isTCPliginConfigRead)
                return;

            //int result;
            string commonErrorMessageString = "Unable to read Test Control Plug-in configuration File: ";

            //Read Configuration file.
            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader(CONFIGFilePath + "Node" + node.ToString() + "/TestControlPluginConfig.xml");

            try
            {
                /* Scan file, one element a time. */
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                                                
                        if (config.LocalName == "SrCalCounterFilePath")
                        {
                            string filePath = config.ReadString().Trim();
                            
                            Util_SrCalCounter.SrCalCounterFilePath = filePath;
                        }
                        if (config.LocalName == "SrCalStageName")
                        {
                            SrCalStageName = config.ReadString().Trim();
                        }
                        if ( config .LocalName == "PhaseAngleStageName")
                        {
                            PhaseAngleStageName = config.ReadString().Trim();
                        }
                        if ( config .LocalName == "DcTestStageName")
                        {
                            DCTestStageName = config .ReadString().Trim();
                        }
                        if (config.LocalName == "DcMeasurementStageName")
                        {
                            DCStageName = config.ReadString().Trim();
                        }

                        if (config.LocalName == "DcSpcStageName")
                        {
                            DcTestSPCStageName  = config.ReadString().Trim();
                        }
                        if (config.LocalName == "PhaseAngleSpcStageName")
                        {
                            PhaseAngleSPCStageName = config.ReadString().Trim();
                        }
                        if (config.LocalName == "DcPreLidSPCStageName")
                        {
                            DcPreLidSPCStageName = config.ReadString().Trim();
                        }
                        if (config.LocalName == "DcPreLidStageName")
                        {
                            DcPreLidStageName = config.ReadString().Trim();
                        }
                        if (config.LocalName == "PinCheckStageName")
                        {
                            pinCheckStageName = config.ReadString().Trim();
                        }

                    } // end if config.NodeType == XmlNodeType.Element: get available text nod
                }  // end while loop

                // Set flag to indicate that configuration has been read.
                isTCPliginConfigRead = true;
            }
            catch (UnauthorizedAccessException uae)
            {

                /* User did not have read permission for file. */
                engine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                /* File not found error. */
                engine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
            }
            catch (System.IO.IOException ioe)
            {
                /* General IO failure. Not file not found. */
                engine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
            }
            catch (System.Xml.XmlException xe)
            {
                /* XML parse error. */
                engine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
            }
            finally
            {
                /* Close file. */
                if (config != null) config.Close();
            }
        }
        
        /// <summary>
        /// Get Device type code by combination of stage and generic code
        /// </summary>
        /// <param name="stage"></param>
        /// <param name="partCode"></param>
        /// <returns></returns>
        private string GetDeviceType(string stage, string partCode)
        {
            ConfigDataAccessor configData = new ConfigDataAccessor(CONFIGFilePath + "PartCodeInfo.xml", "PartCodeInfo");
            string deviceTypeCode = "";
            if (configData != null)
            {
                StringDictionary keys = new StringDictionary();
                keys.Add("Part Code", partCode);
                keys.Add("Stage", stage);

                DatumList list = configData.GetData(keys, true);
                if (list != null) deviceTypeCode = list.ReadString("DeviceType").Trim();
            }
            configData = null;
            return deviceTypeCode;
        }

        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;
        /// <summary>
        /// 
        /// </summary>
        string batchID;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // MES / Results database (PCAS) Node
        int node;
        
        CoRxDCDutLoadInfo DutInfo;
        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;
                
        ProgramStatus programStatus;
        bool inSimulationMode;
        const string CONFIGFilePath = "configuration/CoRxDcTest/";

        DUTObject dut;
        string SrCalStageName = "";
        string PhaseAngleStageName = "";
        string DCTestStageName = "";
        string DCStageName = "";
        string DcTestSPCStageName = "";
        string PhaseAngleSPCStageName = "";
        string DcPreLidSPCStageName = "";
        string DcPreLidStageName = "";
        string pinCheckStageName = "";
        string lastTestStageName = "";
        #endregion

    }
}
