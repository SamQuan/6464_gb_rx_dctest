using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestSolution.CoRxDCTestCommonData;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Define all instruments available in Coherence Rx DC Test
    /// and common intrument operation functions
    /// </summary>
    public class CoRxDCTestInstruments
    {
        #region Instruments and measurement settings

        #region DC Test instruments

        /// <summary>
        /// Top TEC Controller for case Temp
        /// </summary>
        public static Inst_Nt10a TopCaseTec;
        /// <summary>
        ///  Bottom TEC Controller for case Temp
        /// </summary>
        public static Inst_Nt10a BottomCaseTec;

        /// <summary>
        /// Instruments for optical input path for siganl oscilator
        /// incuding Laser source, voa, monitor power meter and reference power meter
        /// with the calibration data between monitor and reference power meter readings
        /// </summary>
        public static CoRxSourceInstrsChain SignalInputInstrs;
        /// <summary>
        /// Instruments for optical input path for local oscilator
        /// incuding Laser source, voa, monitor power meter and reference power meter        /// 
        /// with the calibration data between monitor and reference power meter readings
        /// </summary>
        public static CoRxSourceInstrsChain LocalOscInputInstrs;

        #endregion

        #region bias sources
        /// <summary>
        /// PF_XI2 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_XI2;
        /// <summary>
        /// PF_XI1 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_XI1;
        /// <summary>
        /// PF_XQ1 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_XQ1;
        /// <summary>
        /// PF_XQ2 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_XQ2;
        /// <summary>
        /// PF_YI2 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_YI2;
        /// <summary>
        /// PF_YI1 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_YI1;
        /// <summary>
        /// PF_YQ1 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_YQ1;
        /// <summary>
        /// PF_YQ2 Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_YQ2;

        /// <summary>
        /// PF_AUX_LP Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_AUX_XL;
        /// <summary>
        /// PF_AUX_LN Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_AUX_XR;
        /// <summary>
        /// PF_AUX_RP Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_AUX_YL;
        /// <summary>
        /// PF_AUX_RN Bias source
        /// </summary>
        public static Inst_Ke24xx PdSource_AUX_YR;

        /// <summary>
        /// Bias sources for TIA YQ and YI
        /// </summary>
        public static Inst_Ke24xx TiaSource_Y;
        /// <summary>
        /// Bias sources for TIA XQ and XI
        /// </summary>
        public static Inst_Ke24xx TiaSource_X;

        /// <summary>
        /// Bias sources for Tap
        /// </summary>
        public static Inst_Ke24xx PdTapSource;

        /// <summary>
        /// Control voltage for VOA
        /// </summary>
        public static Inst_Ke24xx VOASource;

         /// <summary>
        /// Bias source for XI and XQ Output Amplitude control in AGC Mode
        /// </summary>
        public static InstType_ElectricalSource OaSource_X;
        /// <summary>
        /// Bias source for YI and YQ Output Amplitude control in AGC Mode
        /// </summary>
        public static InstType_ElectricalSource OaSource_Y;
        /// <summary>
        /// Bias source for XI and XQ Mode control in AGC Mode
        /// </summary>
        public static InstType_ElectricalSource MCSource_X;
        /// <summary>
        /// Bias source for YI and YQ Mode control in AGC Mode
        /// </summary>
        public static InstType_ElectricalSource MCSource_Y;
        /// <summary>
        /// Bias source for X Output control
        /// </summary>
        public static InstType_ElectricalSource OCSource_X;
        /// <summary>
        /// Bias source for Y Output control
        /// </summary>
        public static InstType_ElectricalSource OCSource_Y;

        /// <summary>
        /// Bias source for X TIA Gain control in manual mode
        /// </summary>
        public static InstType_ElectricalSource GCSource_X;
        /// <summary>
        /// Bias source for Y TIA Gain control in manual mode
        /// </summary>
        public static InstType_ElectricalSource GCSource_Y;

        /// <summary>
        /// Bias source for L Thermal phase Control
        /// </summary>
        public static InstType_ElectricalSource ThermPhaseSource_L;
        /// <summary>
        /// Bias source for R Thermal phase Control
        /// </summary>
        public static InstType_ElectricalSource ThermPhaseSource_R;

        public static Instr_SPI inSPI;

 

        #endregion

        /// <summary>
        /// Itla laser source for phase angle
        /// </summary>
        public static Inst_CoRxITLALaserSource PhaseAngleLaserSource = null;

        /// <summary>
        /// Oscillator for XI TIA output
        /// </summary>
        public static Inst_Tds3034 OSC_XI = null;

        /// <summary>
        /// Oscillator for XQ TIA output
        /// </summary>
        public static Inst_Tds3034 OSC_XQ = null;

        /// <summary>
        /// Oscillator for YI TIA output
        /// </summary>
        public static Inst_Tds3034 OSC_YI = null;

        /// <summary>
        /// Oscillator for YQ TIA output
        /// </summary>
        public static Inst_Tds3034 OSC_YQ = null;

        /// <summary>
        /// Instruments involves in polarization adjust in signal path
        /// </summary>
        public static CoRxPolarisationAdjustInstrs SigPathPolAdjustInstrs = null;

        #region Settings related with instrument controls and settings
        /// <summary>
        /// If Auxiliary PD source share the sources with other PD, 
        /// it will be connected to the output terminal on Front pannel
        /// </summary>
        public static bool IsAuxPDOnFront = true;

        /// <summary>
        /// Are AUX devices to be measured on this device
        /// </summary>
        public static bool IsTestAuxPD = false;

        /// <summary>
        /// is device 6464 type
        /// </summary>
        public static bool Is6464 = false;

        /// <summary>
        /// Flag to indicate ithe case temperature is control by 2 Tecs(top & bottom) or 1
        /// </summary>
        public static bool IsDualCaseTecControl = false;
        /// <summary>
        /// Flag to indicate if bw pins are duplicated to be pd tap or not 
        /// if set to be false, the PdTapSource should be set to be null
        /// </summary>
        public static bool IsBiasPDTap = false;

        #endregion

        public static int TriggerInLine;
        public static int TriggerOutLine;
        public static int TriggerNoUsedLine;

        #endregion

        /// <summary>
        ///  If AUX PD share source with other PD,  
        /// setting "isSelectAuxPd" to be true will set the soure output terminals to be front
        /// </summary>
        /// <param name="isSelectAuxPd">if connected sources to Aux pds  </param>
        public static void IsSource4AuxPd(bool isSelectAuxPd)
        {
            if (!IsAuxPDOnFront) return;

            // IF select the source connected to AUX PD, 
            // set the output terminal to front pannel, else set it to rear pannel
            PdSource_AUX_XR.SelectRearTerminals = !isSelectAuxPd;
            PdSource_AUX_XL.SelectRearTerminals = !isSelectAuxPd;
            PdSource_AUX_YR.SelectRearTerminals = !isSelectAuxPd;
            PdSource_AUX_YL.SelectRearTerminals = !isSelectAuxPd;

        }

        /// <summary>
        /// Set all auxiliary PD sources's output enable status
        /// /// if the pd sources are share with auxilary pd, switch the output terminal befroe call this function
        /// </summary>
        /// <param name="outputStatus">output enable status  </param>
        public static void SetAllAuxPdSourceOutput(bool outputStatus)
        {
            if (IsTestAuxPD)
            {
                if (IsAuxPDOnFront && !PdSource_AUX_XL.SelectRearTerminals &&
                    !PdSource_AUX_YL.SelectRearTerminals && !PdSource_AUX_YR.SelectRearTerminals)
                    throw new Exception(" Trying to changed source output while Aux PD source still connected to other Pds");
                PdSource_AUX_XR.OutputEnabled = outputStatus;
                PdSource_AUX_XL.OutputEnabled = outputStatus;
                PdSource_AUX_YR.OutputEnabled = outputStatus;
                PdSource_AUX_YL.OutputEnabled = outputStatus;
            }
        }

        /// <summary>
        /// Set all PD sources's output enable status
        /// if the pd sources are share with auxilary pd, switch the output terminal befroe call this function
        /// </summary>
        /// <param name="outputStatus">output enable status  </param>
        public static void SetAllPDSourceOutput(bool outputStatus)
        {
            if (IsAuxPDOnFront && !PdSource_AUX_XR.SelectRearTerminals && !PdSource_AUX_XL.SelectRearTerminals &&
                !PdSource_AUX_YL.SelectRearTerminals && !PdSource_AUX_YR.SelectRearTerminals)
                throw new Exception(" Trying to changed source output while IT still connected to other AUX Pds");
            PdSource_XI1.OutputEnabled = outputStatus;
            PdSource_XQ1.OutputEnabled = outputStatus;
            PdSource_YI1.OutputEnabled = outputStatus;
            PdSource_YQ1.OutputEnabled = outputStatus;
            if (!Is6464)
            {
                PdSource_XI2.OutputEnabled = outputStatus;
                PdSource_XQ2.OutputEnabled = outputStatus;
                PdSource_YI2.OutputEnabled = outputStatus;
                PdSource_YQ2.OutputEnabled = outputStatus;
            }
            if (Is6464)
            {
                PdSource_XI2.UseFrontTerminals = true;
                PdSource_XQ2.UseFrontTerminals = true;
                PdSource_YI2.UseFrontTerminals = true;
                PdSource_YQ2.UseFrontTerminals = true;
            }

        }

        public static void SetAllPDSourceAutoOutput(bool EnableAutoOutputStatus)
        {

            PdSource_XI1.EnableAutoOutput = EnableAutoOutputStatus;
            PdSource_XQ1.EnableAutoOutput = EnableAutoOutputStatus;
            PdSource_YI1.EnableAutoOutput = EnableAutoOutputStatus;
            PdSource_YQ1.EnableAutoOutput = EnableAutoOutputStatus;

            if (!Is6464)
            {
                PdSource_XI2.EnableAutoOutput = EnableAutoOutputStatus;
                PdSource_XQ2.EnableAutoOutput = EnableAutoOutputStatus;
                PdSource_YI2.EnableAutoOutput = EnableAutoOutputStatus;
                PdSource_YQ2.EnableAutoOutput = EnableAutoOutputStatus;
            }
        }

        /// <summary>
        /// Set all TIA sources's output enable status
        /// </summary>
        /// <param name="outputStatus">output enable status  </param>
        public static void setAllTiaSourceOutput(bool outputStatus)
        {
            TiaSource_X.OutputEnabled = outputStatus;
            TiaSource_Y.OutputEnabled = outputStatus;
        }

        /// <summary>
        /// Initialise all TIA sourcees to fixed voltage function
        /// </summary>
        public static void InitAllTIASources()
        {
            setAllTiaSourceOutput(false);
            TiaSource_X.SourceFixedVoltage();
            TiaSource_Y.SourceFixedVoltage();
        }


        public static void SetupTIA()
        {

            inSPI.SPI_Reset(true);
            uint Temp = inSPI.ReadDIO_SPI(2);
            inSPI.WriteDIO_SPI(05, 01);   // Reset TIA
            inSPI.WriteDIO_SPI(06, 00);   // Set all channels to active 

            for (uint offset = 0; offset < 385; offset += 128)
            {
                inSPI.WriteDIO_SPI(System.Convert.ToUInt16(07 + offset), 02);        // Mode manual - Analog = 1
                inSPI.WriteDIO_SPI(System.Convert.ToUInt16(08 + offset), 512);       // Set Monitor pin to monitor gain contro, voltage 
                inSPI.WriteDIO_SPI(System.Convert.ToUInt16(09 + offset), 0xFF00);    // DAC DIN Control sets maximium gain
                inSPI.WriteDIO_SPI(System.Convert.ToUInt16(10 + offset), 64);       // ADC Enabled
                inSPI.WriteDIO_SPI(System.Convert.ToUInt16(14 + offset), 771);       // PEAKING_CONTROL sets maximium peak
                inSPI.WriteDIO_SPI(System.Convert.ToUInt16(15 + offset), 02);       // SUM_CIRCUIT_CONTROL
            }


        }

        public static void ResetTIA()
        {

            inSPI.SPI_Reset(false);
            inSPI.SPI_Reset(true);

        }
                   /// <summary>
        /// Set TIA Source 's output voltage and current compliance
        /// </summary>
        /// <param name="Vset_V"></param>
        /// <param name="Icompliance_A"></param>
        public static void SetTIABias(double Vset_V, double Icompliance_A)
        {
            TiaSource_X.SourceFixedVoltage();
            TiaSource_X.CurrentComplianceSetPoint_Amp = Icompliance_A;
            TiaSource_X.SenseCurrent(Icompliance_A, Icompliance_A);

            TiaSource_Y.SourceFixedVoltage();
            TiaSource_Y.CurrentComplianceSetPoint_Amp = Icompliance_A;
            TiaSource_Y.SenseCurrent(Icompliance_A, Icompliance_A);

            TiaSource_X.VoltageRange_Volt = Vset_V;
            TiaSource_X.VoltageSetPoint_Volt = Vset_V;

            TiaSource_Y.VoltageRange_Volt = Vset_V;
            TiaSource_Y.VoltageSetPoint_Volt = Vset_V;
        }

        /// <summary>
        /// Initialise all Aux pd sourcees to fixed voltage function
        /// </summary>
        ///<param name="Vmax_X_V"> Max volt it might set to X chip source, it will be set as voltge range</param>
        ///<param name="Vmax_Y_V"> Max volt it might set to Y chip source, it will be set as voltge range</param>
        ///<param name="Icompl_X_A"> X chip source Current compliance </param>
        ///<param name="Icompl_Y_A"> Y chip source Current compliance </param>
        public static void InitAllAuxPDSourcesAsVoltSource(double Vmax_X_V, double Vmax_Y_V, double Icompl_X_A, double Icompl_Y_A)
        {
            SetAllAuxPdSourceOutput(false);

            int avgNum = 1;
            PdSource_AUX_XR.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_X_A, 0, false, 0, avgNum, 1, true);
            PdSource_AUX_XR.VoltageRange_Volt = Vmax_X_V;

            PdSource_AUX_XL.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_X_A, 0, false, 0, avgNum, 1, true);
            PdSource_AUX_XL.VoltageRange_Volt = Vmax_X_V;

            PdSource_AUX_YR.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_Y_A, 0, false, 0, avgNum, 1, true);
            PdSource_AUX_YR.VoltageRange_Volt = Vmax_Y_V;

            PdSource_AUX_YL.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_Y_A, 0, false, 0, avgNum, 1, true);
            PdSource_AUX_YL.VoltageRange_Volt = Vmax_Y_V;
        }

        /// <summary>
        /// Initialise all pd sourcees to fixed voltage function
        /// </summary>
        ///<param name="Vmax_V"> Max volt it might set to source, it will be set as voltge range</param>
        ///<param name="Icompl_A"> Current compliance </param>
        public static void InitAllPdSourcesAsVoltSource(double Vmax_V, double Icompl_A)
        {
            SetAllPDSourceOutput(false);
            int avgNum = 1;
            PdSource_XI1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_XI1.VoltageRange_Volt = Vmax_V;

            PdSource_XQ1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_XQ1.VoltageRange_Volt = Vmax_V;

            PdSource_YI1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_YI1.VoltageRange_Volt = Vmax_V;

            PdSource_YQ1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_YQ1.VoltageRange_Volt = Vmax_V;

            if (!Is6464)
            {
                PdSource_XI2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_XI2.VoltageRange_Volt = Vmax_V;

                PdSource_XQ2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_XQ2.VoltageRange_Volt = Vmax_V;

                PdSource_YI2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_YI2.VoltageRange_Volt = Vmax_V;

                PdSource_YQ2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_YQ2.VoltageRange_Volt = Vmax_V;
            }


        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Vmax_V"></param>
        /// <param name="Icompl_A"></param>
        public static void InitAllPdSourcesAsVoltSourceWithAvg(double Vmax_V, double Icompl_A, int avgNum)
        {
            SetAllPDSourceOutput(false);
            PdSource_XI1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_XI1.VoltageRange_Volt = Vmax_V;

            PdSource_XQ1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_XQ1.VoltageRange_Volt = Vmax_V;

            PdSource_YI1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_YI1.VoltageRange_Volt = Vmax_V;

            PdSource_YQ1.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
            PdSource_YQ1.VoltageRange_Volt = Vmax_V;

            if (!Is6464)
            {
                PdSource_XI2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_XI2.VoltageRange_Volt = Vmax_V;

                PdSource_XQ2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_XQ2.VoltageRange_Volt = Vmax_V;

                PdSource_YI2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_YI2.VoltageRange_Volt = Vmax_V;

                PdSource_YQ2.InitSourceVMeasureI_MeasurementAccuracy(
                Icompl_A, Icompl_A, false, 0, avgNum, 1, true);
                PdSource_YQ2.VoltageRange_Volt = Vmax_V;

            }

        }
        /// <summary>
        /// If Aux PD has independent sourece with PD, it will do nothing,
        /// Or it will switch all auxPD source to front pannel or rear pannel enpends on 'isConnected2AuxPd'
        /// </summary>
        /// <param name="isConnected2AuxPd"> if aux pd share source with PDs 
        /// it will switch output terminals to front pannel when it is true
        ///  and to rear pannel when it it false </param>
        public static void SetSource2AuxPD(bool isConnected2AuxPd)
        {
            if (!IsAuxPDOnFront) return;

            PdSource_AUX_XR.SelectRearTerminals = !isConnected2AuxPd;
            PdSource_AUX_XL.SelectRearTerminals = !isConnected2AuxPd;
            PdSource_AUX_YR.SelectRearTerminals = !isConnected2AuxPd;
            PdSource_AUX_YL.SelectRearTerminals = !isConnected2AuxPd;

        }

        /// <summary>
        /// Initialise pd sources
        /// </summary>        
        public static void InitialiseAllSource()
        {
            if (CoRxDCTestInstruments.PdTapSource != null) // if tap pd exist
            {
                PdTapSource.SetDefaultState();
                PdTapSource.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdTapSource.ConfigureVoltageSweepReadings();// set reading format to be "volt,curr"                

            }
            if (IsTestAuxPD) // Only run this code if AUX testing is selected
            {
                PdSource_AUX_XR.SetDefaultState();
                PdSource_AUX_XR.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_AUX_XR.ConfigureVoltageSweepReadings();

                PdSource_AUX_XL.SetDefaultState();
                PdSource_AUX_XL.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_AUX_XL.ConfigureVoltageSweepReadings();

                PdSource_AUX_YR.SetDefaultState();
                PdSource_AUX_YR.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_AUX_YR.ConfigureVoltageSweepReadings();

                PdSource_AUX_YL.SetDefaultState();
                PdSource_AUX_YL.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_AUX_YL.ConfigureVoltageSweepReadings();
            }
            TiaSource_X.SetDefaultState();
            TiaSource_X.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            TiaSource_X.ConfigureVoltageSweepReadings();

            TiaSource_Y.SetDefaultState();
            TiaSource_Y.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            TiaSource_Y.ConfigureVoltageSweepReadings();

            PdSource_XI1.SetDefaultState();
            PdSource_XI1.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            PdSource_XI1.ConfigureVoltageSweepReadings();

            PdSource_XQ1.SetDefaultState();
            PdSource_XQ1.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            PdSource_XQ1.ConfigureVoltageSweepReadings();

            PdSource_YI1.SetDefaultState();
            PdSource_YI1.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            PdSource_YI1.ConfigureVoltageSweepReadings();

            PdSource_YQ1.SetDefaultState();
            PdSource_YQ1.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            PdSource_YQ1.ConfigureVoltageSweepReadings();

            if (!Is6464)
            {
                PdSource_XI2.SetDefaultState();
                PdSource_XI2.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_XI2.ConfigureVoltageSweepReadings();

                PdSource_XQ2.SetDefaultState();
                PdSource_XQ2.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_XQ2.ConfigureVoltageSweepReadings();

                PdSource_YI2.SetDefaultState();
                PdSource_YI2.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_YI2.ConfigureVoltageSweepReadings();

                PdSource_YQ2.SetDefaultState();
                PdSource_YQ2.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
                PdSource_YQ2.ConfigureVoltageSweepReadings();
            }

            VOASource.SetDefaultState();
            VOASource.OutputOffState = Inst_Ke24xx.EnumOutputOffType.HIMPedance;
            VOASource.ConfigureVoltageSweepReadings();
            VOASource.SelectRearTerminals = true;

        }

        /// <summary>
        /// Set source's interlock
        /// </summary>
        /// <param name="bEnableInterLock"> Is enable interlaock function for source </param>
        public static void SetSourceInterLock(bool bEnableInterLock)
        {
            if (CoRxDCTestInstruments.PdTapSource != null)
            {
                PdTapSource.EnableInterLock = bEnableInterLock;

            }

            if (IsTestAuxPD)
            {
                PdSource_AUX_XR.EnableAutoOutput = bEnableInterLock;

                PdSource_AUX_XL.EnableInterLock = bEnableInterLock;

                PdSource_AUX_YR.EnableInterLock = bEnableInterLock;

                PdSource_AUX_YL.EnableInterLock = bEnableInterLock;

            }

            TiaSource_X.EnableInterLock = bEnableInterLock;

            TiaSource_Y.EnableInterLock = bEnableInterLock;

            PdSource_XI1.EnableInterLock = bEnableInterLock;

            PdSource_XQ1.EnableInterLock = bEnableInterLock;

            PdSource_YI1.EnableInterLock = bEnableInterLock;

            PdSource_YQ1.EnableInterLock = bEnableInterLock;

            if (!Is6464)
            {
                PdSource_XI2.EnableInterLock = bEnableInterLock;
                PdSource_XQ2.EnableInterLock = bEnableInterLock;
                PdSource_YI2.EnableInterLock = bEnableInterLock;
                PdSource_YQ2.EnableInterLock = bEnableInterLock;
            }
        }

        /// <summary>
        /// Set all aux pd sources to be current source 
        /// </summary>
        /// <param name="Imax_A"> Max current might be set, it will set as current range </param>
        /// <param name="V_Compl_V"> Voltage compliance  </param>
        public static void InitAllAuxPdSourceAsCurrentSource(double Imax_A, double V_Compl_V)
        {
            int avgNum = 1;
            PdSource_AUX_XR.InitSourceIMeasureV_MeasurementAccuracy(
                V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_AUX_XR.CurrentRange_Amp = Imax_A;
            PdSource_AUX_XL.InitSourceIMeasureV_MeasurementAccuracy(
                V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_AUX_XL.CurrentRange_Amp = Imax_A;
            PdSource_AUX_YR.InitSourceIMeasureV_MeasurementAccuracy(
                V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_AUX_YR.CurrentRange_Amp = Imax_A;
            PdSource_AUX_YL.InitSourceIMeasureV_MeasurementAccuracy(
                V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_AUX_YL.CurrentRange_Amp = Imax_A;

        }

        /// <summary>
        /// Set all aux pd sources to be current source 
        /// </summary>
        /// <param name="Imax_A"> Max current might be set, it will set as current range </param>
        /// <param name="V_Compl_V"> Voltage compliance  </param>
        public static void InitAllPdSourceAsCurrentSource(double Imax_A, double V_Compl_V)
        {
            int avgNum = 1;
            PdSource_XI1.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_XI1.CurrentRange_Amp = Imax_A;
            PdSource_XQ1.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_XQ1.CurrentRange_Amp = Imax_A;
            PdSource_YI1.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_YI1.CurrentRange_Amp = Imax_A;
            PdSource_YQ1.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
            PdSource_YQ1.CurrentRange_Amp = Imax_A;

            if (!Is6464)
            {
                PdSource_XI2.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
                PdSource_XI2.CurrentRange_Amp = Imax_A;
                PdSource_XQ2.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
                PdSource_XQ2.CurrentRange_Amp = Imax_A;
                PdSource_YI2.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
                PdSource_YI2.CurrentRange_Amp = Imax_A;
                PdSource_YQ2.InitSourceIMeasureV_MeasurementAccuracy(V_Compl_V, V_Compl_V, false, 0, avgNum, 1, true);
                PdSource_YQ2.CurrentRange_Amp = Imax_A;
            }
        }

        /// <summary>
        /// Set PD Source 's output voltage and current compliance
        /// </summary>
        /// <param name="Vset_V"></param>
        /// <param name="Icompliance_A"></param>
        public static void SetAllPdBias(double Vset_V, double Icompliance_A)
        {
            PdSource_XI1.CurrentComplianceSetPoint_Amp = Icompliance_A;
            PdSource_XI1.SenseCurrent(Icompliance_A, Icompliance_A);
            PdSource_XI1.VoltageSetPoint_Volt = Vset_V;

            PdSource_XQ1.CurrentComplianceSetPoint_Amp = Icompliance_A;
            PdSource_XQ1.SenseCurrent(Icompliance_A, Icompliance_A);
            PdSource_XQ1.VoltageSetPoint_Volt = Vset_V;

            PdSource_YI1.CurrentComplianceSetPoint_Amp = Icompliance_A;
            PdSource_YI1.SenseCurrent(Icompliance_A, Icompliance_A);
            PdSource_YI1.VoltageSetPoint_Volt = Vset_V;

            PdSource_YQ1.CurrentComplianceSetPoint_Amp = Icompliance_A;
            PdSource_YQ1.SenseCurrent(Icompliance_A, Icompliance_A);
            PdSource_YQ1.VoltageSetPoint_Volt = Vset_V;

            if (!Is6464)
            {
                PdSource_XI2.CurrentComplianceSetPoint_Amp = Icompliance_A;
                PdSource_XI2.SenseCurrent(Icompliance_A, Icompliance_A);
                PdSource_XI2.VoltageSetPoint_Volt = Vset_V;

                PdSource_XQ2.CurrentComplianceSetPoint_Amp = Icompliance_A;
                PdSource_XQ2.SenseCurrent(Icompliance_A, Icompliance_A);
                PdSource_XQ2.VoltageSetPoint_Volt = Vset_V;

                PdSource_YI2.CurrentComplianceSetPoint_Amp = Icompliance_A;
                PdSource_YI2.SenseCurrent(Icompliance_A, Icompliance_A);
                PdSource_YI2.VoltageSetPoint_Volt = Vset_V;

                PdSource_YQ2.CurrentComplianceSetPoint_Amp = Icompliance_A;
                PdSource_YQ2.SenseCurrent(Icompliance_A, Icompliance_A);
                PdSource_YQ2.VoltageSetPoint_Volt = Vset_V;

            }

        }

        /// <summary>
        /// Set AUX PD Source 's output voltage and current compliance
        /// </summary>
        /// <param name="Vset_X_V"> X chip bias </param>
        /// /// <param name="Vset_Y_V"> Y Chip bias </param>
        /// <param name="Icompliance_X_A"> X chip source current compliant </param>
        /// <param name="Icompliance_Y_A"> Y chip source current compliant </param>
        public static void SetAllAuxPdBias(double Vset_X_V, double Vset_Y_V, double Icompliance_X_A, double Icompliance_Y_A)
        {
            PdSource_AUX_XR.CurrentComplianceSetPoint_Amp = Icompliance_X_A;
            PdSource_AUX_XR.SenseCurrent(Icompliance_X_A, Icompliance_X_A);
            PdSource_AUX_XR.VoltageSetPoint_Volt = Vset_X_V;

            PdSource_AUX_XL.CurrentComplianceSetPoint_Amp = Icompliance_X_A;
            PdSource_AUX_XL.SenseCurrent(Icompliance_X_A, Icompliance_X_A);
            PdSource_AUX_XL.VoltageSetPoint_Volt = Vset_X_V;

            PdSource_AUX_YR.CurrentComplianceSetPoint_Amp = Icompliance_Y_A;
            PdSource_AUX_YR.SenseCurrent(Icompliance_Y_A, Icompliance_Y_A);
            PdSource_AUX_YR.VoltageSetPoint_Volt = Vset_Y_V;

            PdSource_AUX_YL.CurrentComplianceSetPoint_Amp = Icompliance_Y_A;
            PdSource_AUX_YL.SenseCurrent(Icompliance_Y_A, Icompliance_Y_A);
            PdSource_AUX_YL.VoltageSetPoint_Volt = Vset_Y_V;

        }

        #region control pins setting
        /// <summary>
        /// Set OC Bias
        /// </summary>
        /// <param name="Vset_V">  0: connected to ground
        ///                        double.NaN: floating
        ///                        others: output voltage to specified value 
        /// </param>
        /// <param name="Icompliance_A"></param>
        public static void SetOCBias(double Vset_V, double Icompliance_A)
        {
            if (OCSource_X != null)
            {
                OCSource_X.CurrentComplianceSetPoint_Amp = Icompliance_A;
                OCSource_X.VoltageSetPoint_Volt = Vset_V;
            }
            if (OCSource_Y != null)
            {
                OCSource_Y.CurrentComplianceSetPoint_Amp = Icompliance_A;
                OCSource_Y.VoltageSetPoint_Volt = Vset_V;
            }

        }

        /// <summary>
        /// Set output amplitude control bias voltage and current compliance
        /// if Vset_V if out of 0.5~2.5, the pin will be left as floating
        /// </summary>
        /// <param name="Vset_V"> 
        ///                       0.5~3: set to specified voltage, Voutput_PP = 300~900 mV
        ///                       others: floating , Voutput_PP = 500mV  
        /// </param>
        /// <param name="Icompliance_A"></param>
        public static void SetOABias(double Vset_V, double Icompliance_A)
        {
            if (Vset_V >= 0.5 && Vset_V <= 3)
            {
                if (OaSource_X != null)
                {
                    OaSource_X.CurrentComplianceSetPoint_Amp = Icompliance_A;
                    OaSource_X.VoltageSetPoint_Volt = Vset_V;
                    OaSource_X.OutputEnabled = true;
                }
                if (OaSource_Y != null)
                {
                    OaSource_Y.CurrentComplianceSetPoint_Amp = Icompliance_A;
                    OaSource_Y.VoltageSetPoint_Volt = Vset_V;
                    OaSource_Y.OutputEnabled = true;
                }

            }
            else
            {
                if (OaSource_X != null)
                    OaSource_X.OutputEnabled = false;
                if (OaSource_Y != null)
                    OaSource_Y.OutputEnabled = false;
            }
        }


        /// <summary>
        /// Set MC Bias,Only 0 or floating available for Mc Pin,
        /// </summary>
        /// <param name="Vset_V"> NaN : floating,TIA In AGC control mode,
        ///                        less than 0.8v  : TIA In manual control mode,
        ///                        greater than 2V : TIA In AGC control mode 
        ///                        </param>
        //  /// <param name="Icompliance_A"></param>
        /// <summary>        
        public static void SetMCBias(double Vset_V, double Icompliance_A)
        {

            if (MCSource_Y != null)
            {
                MCSource_Y.CurrentComplianceSetPoint_Amp = Icompliance_A;
                MCSource_Y.VoltageSetPoint_Volt = Vset_V;
                MCSource_Y.OutputEnabled = true;
            }
            if (MCSource_X != null)
            {
                MCSource_X.CurrentComplianceSetPoint_Amp = Icompliance_A;
                MCSource_X.VoltageSetPoint_Volt = Vset_V;
                MCSource_X.OutputEnabled = true;

            }
        }

        // <summary>
        /// Set GC Bias
        /// </summary>
        /// <param name="Vset_V">  double.NaN : floating,
        ///                       0~ 2.2V: Sets TIA Gain to 5000ohms which is maximun 
        ///                         >2.2V: Sets TIA Gain to 5000ohms which is maximun
        /// </param>
        /// <param name="Icompliance_A"></param>
        public static void SetGCBias(double Vset_V, double Icompliance_A)
        {

            if (GCSource_X != null)
            {
                GCSource_X.CurrentComplianceSetPoint_Amp = Icompliance_A;
                GCSource_X.VoltageSetPoint_Volt = Vset_V;
                GCSource_X.OutputEnabled = true;
            }
            if (GCSource_Y != null)
            {
                GCSource_Y.CurrentComplianceSetPoint_Amp = Icompliance_A;
                GCSource_Y.VoltageSetPoint_Volt = Vset_V;
                GCSource_Y.OutputEnabled = true;
            }

        }

        /// <summary>
        /// Set Thermal phase bias voltage and current compliance
        /// </summary>
        /// <param name="Iset_A"></param>
        /// <param name="Vcompliant_V"></param>
        public static void SetThermPhaseBias(double Iset_A, double Vcompliant_V)
        {
            if (ThermPhaseSource_L != null)
            {
                ThermPhaseSource_L.OutputEnabled = true;
                ThermPhaseSource_L.VoltageComplianceSetPoint_Volt = Vcompliant_V;
                ThermPhaseSource_L.CurrentSetPoint_amp = Iset_A;

            }
            if (ThermPhaseSource_R != null)
            {
                ThermPhaseSource_R.OutputEnabled = true;
                ThermPhaseSource_R.VoltageComplianceSetPoint_Volt = Vcompliant_V;
                ThermPhaseSource_R.CurrentSetPoint_amp = Iset_A;

            }
        }

        /// <summary>
        /// Set all control pins bias
        /// </summary>
        /// <param name="Vset_MC_V"></param>
        /// <param name="Icomp_MC_A"></param>
        /// <param name="Vset_GC_V"></param>
        /// <param name="Icomp_GC_A"></param>
        /// <param name="Vset_OC_V"></param>
        /// <param name="Icomp_OC_A"></param>
        /// <param name="Vset_OA_V"></param>
        /// <param name="Icomp_OA_A"></param>
        /// <param name="Iset_ThermPhase_A"></param>
        /// <param name="Vcomp_ThermPhase_V"></param>
        public static void SetAllControlLevel(double Vset_MC_V, double Icomp_MC_A,
            double Vset_GC_V, double Icomp_GC_A, double Vset_OC_V, double Icomp_OC_A,
            double Vset_OA_V, double Icomp_OA_A, double Iset_ThermPhase_A, double Vcomp_ThermPhase_V)
        {
            SetMCBias(Vset_MC_V, Icomp_MC_A);
            SetGCBias(Vset_GC_V, Icomp_GC_A);
            SetOCBias(Vset_OC_V, Icomp_OC_A);
            SetOABias(Vset_OA_V, Icomp_OA_A);
            SetThermPhaseBias(Iset_ThermPhase_A, Vcomp_ThermPhase_V);
        }

        #endregion

        /// <summary>
        /// Setup trigger link loop in sources involve in pd resposivity measurement
        /// </summary>
        /// <param name="masterSource"> source as master in trigger link </param>
        /// <param name="VsetPD_V"> Pd bias voltage </param>
        /// <param name="VsetTap_V"> Tap pd bias volatge </param>
        /// <param name="numPoints"></param>
        public static void SetupPdSourceTriggerLink(Inst_Ke24xx masterSource, double VsetPD_V, double VsetTap_V, int numPoints)
        {
            bool isSetTriggerOut = false;
            if (object.ReferenceEquals(PdSource_XI1, masterSource))
                ConfigSourceTrigger(PdSource_XI1,
                    VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            else
            {
                ConfigSourceTrigger(PdSource_XI1, VsetPD_V, numPoints, TriggerInLine, TriggerOutLine);
                isSetTriggerOut = true;
            }


            //if (object.ReferenceEquals(PdSource_XI2, masterSource))
            //    ConfigSourceTrigger(PdSource_XI2,
            //        VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            //else if (isSetTriggerOut) // Source_XI1 is not master and trigger out line set on it
            //    ConfigSourceTrigger(PdSource_XI2,
            //        VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            //else  // Source_XI1 is master
            //{
            //    ConfigSourceTrigger(PdSource_XI2,
            //        VsetPD_V, numPoints, TriggerInLine, TriggerOutLine);
            //    isSetTriggerOut = true;
            //}

            if (object.ReferenceEquals(PdSource_XQ1, masterSource))
                ConfigSourceTrigger(PdSource_XQ1,
                    VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            else
                ConfigSourceTrigger(PdSource_XQ1,
                    VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);

            //if (object.ReferenceEquals(PdSource_XQ2, masterSource))
            //    ConfigSourceTrigger(PdSource_XQ2,
            //        VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            //else
            //    ConfigSourceTrigger(PdSource_XQ2,
            //        VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);

            if (object.ReferenceEquals(PdSource_YI1, masterSource))
                ConfigSourceTrigger(PdSource_YI1,
                    VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            else
                ConfigSourceTrigger(PdSource_YI1,
                    VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);

            //if (object.ReferenceEquals(PdSource_YI2, masterSource))
            //    ConfigSourceTrigger(PdSource_YI2,
            //        VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            //else
            //    ConfigSourceTrigger(PdSource_YI2,
            //        VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            if (object.ReferenceEquals(PdSource_YQ1, masterSource))
                ConfigSourceTrigger(PdSource_YQ1,
                    VsetPD_V, numPoints, TriggerInLine, TriggerOutLine);
            else
                ConfigSourceTrigger(PdSource_YQ1,
                    VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);

            //if (object.ReferenceEquals(PdSource_YQ2, masterSource))
            //    ConfigSourceTrigger(PdSource_YQ2,
            //        VsetPD_V, numPoints, TriggerOutLine, TriggerInLine);
            //else
            //    ConfigSourceTrigger(PdSource_YQ2,
            //        VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);

            if (IsBiasPDTap)
            {
                if (object.ReferenceEquals(PdTapSource, masterSource))
                    ConfigSourceTrigger(PdTapSource, VsetTap_V, numPoints, TriggerOutLine, TriggerInLine);
                else
                    ConfigSourceTrigger(PdTapSource, VsetTap_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            }

            masterSource.TriggerInput = Inst_Ke24xx.TriggerLayerType.SENSE;
            masterSource.TriggerOutput = Inst_Ke24xx.TriggerLayerType.DELAY;
            masterSource.EnableSRQonOPC();
        }

        public static void SetupPdSourceTriggerLinkResponsivity(Inst_Ke24xx masterSource, double VsetPD_V, double VsetTap_V, int numPoints)
        {

            ConfigSourceTrigger(PdSource_XI1, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine); //master controls all triggering and does not require feedback, no trigger out needed
            ConfigSourceTrigger(PdSource_XQ1, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            ConfigSourceTrigger(PdSource_YI1, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            ConfigSourceTrigger(PdSource_YQ1, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);

            if (!Is6464)
            {
                ConfigSourceTrigger(PdSource_XI2, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
                ConfigSourceTrigger(PdSource_XQ2, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
                ConfigSourceTrigger(PdSource_YI2, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
                ConfigSourceTrigger(PdSource_YQ2, VsetPD_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            }

            if (IsBiasPDTap) ConfigSourceTrigger(PdTapSource, VsetTap_V, numPoints, TriggerInLine, TriggerNoUsedLine);
            //ConfigMasterSourceTrigger(masterSource, ReferenceEquals(PdTapSource, masterSource) ? VsetTap_V : VsetPD_V, numPoints, TriggerNoUsedLine, TriggerInLine);

            ConfigMasterSourceTrigger(masterSource, ReferenceEquals(PdSource_YQ1, masterSource) ? VsetPD_V : VsetTap_V, numPoints, TriggerInLine, TriggerOutLine);
            masterSource.EnableSRQonOPC();
        }

        

        /// <summary>
        /// Trigger PD sources
        /// </summary>
        public static void TriggerPdSourceChain()
        {
            PdSource_XI1.ClearSweepTraceData();
            PdSource_XQ1.ClearSweepTraceData();
            PdSource_YI1.ClearSweepTraceData();
            PdSource_YQ1.ClearSweepTraceData();

            if (!Is6464)
            {
                PdSource_XI2.ClearSweepTraceData();
                PdSource_XQ2.ClearSweepTraceData();
                PdSource_YI2.ClearSweepTraceData();
                PdSource_YQ2.ClearSweepTraceData();
            }
            if (IsBiasPDTap) PdTapSource.ClearSweepTraceData();

            SetAllPDSourceOutput(true);
            // bring all source to arm layer trigger
            PdSource_XI1.Trigger();
            PdSource_XQ1.Trigger();
            PdSource_YI1.Trigger();
            PdSource_YQ1.Trigger();

            if (!Is6464)
            {
                PdSource_XI2.Trigger();
                PdSource_XQ2.Trigger();
                PdSource_YI2.Trigger();
                PdSource_YQ2.Trigger();
            }
            if (IsBiasPDTap) PdTapSource.Trigger();
        }

        /// <summary>
        /// Read all PDs' responcivity current
        /// </summary>
        /// <param name="IsUseTriggerLink"></param>
        /// <returns></returns>
        public static StrcCoRxPdRespData GetPdRespCur(bool IsUseTriggerLink)
        {
            StrcCoRxPdRespData dataTemp = new StrcCoRxPdRespData();
            if (IsUseTriggerLink)
            {
                double[] voltArr = null;
                double[] currArr = null;

                PdSource_XI1.GetSweepDataSet(out voltArr, out currArr);
                dataTemp.Ipd_XI1_mA = 1000 * Math.Abs(currArr[0]);

                PdSource_XQ1.GetSweepDataSet(out voltArr, out currArr);
                dataTemp.Ipd_XQ1_mA = 1000 * Math.Abs(currArr[0]);

                PdSource_YI1.GetSweepDataSet(out voltArr, out currArr);
                dataTemp.Ipd_YI1_mA = 1000 * Math.Abs(currArr[0]);

                PdSource_YQ1.GetSweepDataSet(out voltArr, out currArr);
                dataTemp.Ipd_YQ1_mA = 1000 * Math.Abs(currArr[0]);

                if (!Is6464)
                {
                    PdSource_XI2.GetSweepDataSet(out voltArr, out currArr);
                    dataTemp.Ipd_XI2_mA = 1000 * Math.Abs(currArr[0]);

                    PdSource_XQ2.GetSweepDataSet(out voltArr, out currArr);
                    dataTemp.Ipd_XQ2_mA = 1000 * Math.Abs(currArr[0]);

                    PdSource_YI2.GetSweepDataSet(out voltArr, out currArr);
                    dataTemp.Ipd_YI2_mA = 1000 * Math.Abs(currArr[0]);

                    PdSource_YQ2.GetSweepDataSet(out voltArr, out currArr);
                    dataTemp.Ipd_YQ2_mA = 1000 * Math.Abs(currArr[0]);
                }


                if (IsBiasPDTap)
                {
                    PdTapSource.GetSweepDataSet(out voltArr, out currArr);
                    dataTemp.I_Tap_mA = 1000 * Math.Abs(currArr[0]);
                }
                else
                    dataTemp.I_Tap_mA = 0;
            }
            else
            {
                dataTemp.Ipd_XI1_mA = 1000 * Math.Abs(PdSource_XI1.CurrentActual_amp);
                dataTemp.Ipd_XQ1_mA = 1000 * Math.Abs(PdSource_XQ1.CurrentActual_amp);
                dataTemp.Ipd_YI1_mA = 1000 * Math.Abs(PdSource_YI1.CurrentActual_amp);
                dataTemp.Ipd_YQ1_mA = 1000 * Math.Abs(PdSource_YQ1.CurrentActual_amp);
 

                if (!Is6464)
                {
                    dataTemp.Ipd_XI2_mA = 1000 * Math.Abs(PdSource_XI2.CurrentActual_amp);
                    dataTemp.Ipd_XQ2_mA = 1000 * Math.Abs(PdSource_XQ2.CurrentActual_amp);
                    dataTemp.Ipd_YI2_mA = 1000 * Math.Abs(PdSource_YI2.CurrentActual_amp);
                    dataTemp.Ipd_YQ2_mA = 1000 * Math.Abs(PdSource_YQ2.CurrentActual_amp);
                }

                if (IsBiasPDTap)
                    dataTemp.I_Tap_mA = 1000 * Math.Abs(PdTapSource.CurrentActual_amp);
                else
                    dataTemp.I_Tap_mA = 0;
            }
            return dataTemp;
        }


        public static StrcCoRxPdRespData[] GetPdRespCurList(bool IsUseTriggerLink, int ExpectedSweepPoints)
        {
            StrcCoRxPdRespData[] dataTemp = new StrcCoRxPdRespData[ExpectedSweepPoints];

            if (IsUseTriggerLink)
            {
                double[] voltArr = null;
                double[] currArrXI1, currArrXI2, currArrXQ1, currArrXQ2, currArrYI1, currArrYI2, currArrYQ1, currArrYQ2, currArrTap = null;

                FormatException formatEx = new FormatException("Returned sweep data points does not match the expected number of points");

                PdSource_XI1.GetSweepDataSet(out voltArr, out currArrXI1);
                if (voltArr.Length != ExpectedSweepPoints || currArrXI1.Length != ExpectedSweepPoints) throw formatEx;

                PdSource_XQ1.GetSweepDataSet(out voltArr, out currArrXQ1);
                if (voltArr.Length != ExpectedSweepPoints || currArrXQ1.Length != ExpectedSweepPoints) throw formatEx;
 
                PdSource_YI1.GetSweepDataSet(out voltArr, out currArrYI1);
                if (voltArr.Length != ExpectedSweepPoints || currArrYI1.Length != ExpectedSweepPoints) throw formatEx;

                PdSource_YQ1.GetSweepDataSet(out voltArr, out currArrYQ1);
                if (voltArr.Length != ExpectedSweepPoints || currArrYQ1.Length != ExpectedSweepPoints) throw formatEx;

                
                if (!Is6464)
                {
                    PdSource_XI2.GetSweepDataSet(out voltArr, out currArrXI2);
                    if (voltArr.Length != ExpectedSweepPoints || currArrXI2.Length != ExpectedSweepPoints) throw formatEx;

                    PdSource_XQ2.GetSweepDataSet(out voltArr, out currArrXQ2);
                    if (voltArr.Length != ExpectedSweepPoints || currArrXQ2.Length != ExpectedSweepPoints) throw formatEx;

                    PdSource_YI2.GetSweepDataSet(out voltArr, out currArrYI2);
                    if (voltArr.Length != ExpectedSweepPoints || currArrYI2.Length != ExpectedSweepPoints) throw formatEx;

                    PdSource_YQ2.GetSweepDataSet(out voltArr, out currArrYQ2);
                    if (voltArr.Length != ExpectedSweepPoints || currArrYQ2.Length != ExpectedSweepPoints) throw formatEx;

                    for (int i = 0; i < ExpectedSweepPoints; ++i)
                    {
                        dataTemp[i].Ipd_XI2_mA = 1000 * Math.Abs(currArrXI2[i]);
                        dataTemp[i].Ipd_XQ2_mA = 1000 * Math.Abs(currArrXQ2[i]);
                        dataTemp[i].Ipd_YI2_mA = 1000 * Math.Abs(currArrYI2[i]);
                        dataTemp[i].Ipd_YQ2_mA = 1000 * Math.Abs(currArrYQ2[i]);
                    }
                }


                if (IsBiasPDTap)
                {
                    PdTapSource.GetSweepDataSet(out voltArr, out currArrTap);
                    if (voltArr.Length != ExpectedSweepPoints || currArrTap.Length != ExpectedSweepPoints) throw formatEx;
                }


                for (int i = 0; i < ExpectedSweepPoints; ++i)
                {

                    dataTemp[i].Ipd_XI1_mA = 1000 * Math.Abs(currArrXI1[i]);
                    dataTemp[i].Ipd_XQ1_mA = 1000 * Math.Abs(currArrXQ1[i]);
                    dataTemp[i].Ipd_YI1_mA = 1000 * Math.Abs(currArrYI1[i]);
                    dataTemp[i].Ipd_YQ1_mA = 1000 * Math.Abs(currArrYQ1[i]);

                    if (IsBiasPDTap)
                    {
                        dataTemp[i].I_Tap_mA = 1000 * Math.Abs(currArrTap[i]);
                    }
                    else
                        dataTemp[i].I_Tap_mA = 0;

                }
            }

            else
                throw new NotImplementedException("Only responsivity test on trigger link is supported");
            return dataTemp;
        }

        public static void SetupPdSourceTriggerImm()
        {

            if (!Is6464)
            {
                PdSource_XI2.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
                PdSource_XQ2.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
                PdSource_YI2.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
                PdSource_YQ2.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            }

            PdSource_XI1.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            PdSource_XQ1.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            PdSource_YI1.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            PdSource_YQ1.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            if (IsBiasPDTap) PdTapSource.TriggerSource = Inst_Ke24xx.TriggerSourceType.IMMediate;

        }
        public static CoRxVOASweepData GetVOASweepPDCur(double voaVolts, bool IsUseTriggerLink)
        {
            double PD1, PD2, PD3, PD4, MPD;
            double VOACurrent;

            // Set VOA Voltage
            VOASource.VoltageSetPoint_Volt = voaVolts;
            System.Threading.Thread.Sleep(100);
            VOACurrent = VOASource.CurrentActual_amp;
            // Now measure the PD Currents
            PD1 = PdSource_XI1.CurrentActual_amp;
            PD2 = 0;
            PD3 = PdSource_YI1.CurrentActual_amp;
            PD4 = 0;

            if (!Is6464)
            {
                PD2 = PdSource_XI2.CurrentActual_amp;
                PD4 = PdSource_YI2.CurrentActual_amp;
            }

            MPD = PdTapSource.CurrentActual_amp;
            CoRxVOASweepData TestData = new CoRxVOASweepData(voaVolts, VOACurrent, PD1, PD2, PD3, PD4, MPD);
            return TestData;
        }

        /// <summary>
        /// Reset pd sources' trigger function and set it to idle
        /// </summary>
        public static void ResetPdSourceTrigger()
        {
            PdSource_XI1.AbortSweep();
            PdSource_XQ1.AbortSweep();
            PdSource_YI1.AbortSweep();
            PdSource_YQ1.AbortSweep();

            if (!Is6464)
            {
                PdSource_XI2.AbortSweep();
                PdSource_XQ2.AbortSweep();
                PdSource_YI2.AbortSweep();
                PdSource_YQ2.AbortSweep();
            }


            if (IsBiasPDTap) PdTapSource.AbortSweep();

        }

        /// <summary>
        /// Clean up all pd soureces' trigger setting
        /// </summary>
        public static void ClearUpPdSourceTrigger()
        {
            PdSource_XI1.CleanUpSweep();
            PdSource_XQ1.CleanUpSweep();
            PdSource_YI1.CleanUpSweep();
            PdSource_YQ1.CleanUpSweep();

            if (!Is6464)
            {
                PdSource_XI2.CleanUpSweep();
                PdSource_XQ2.CleanUpSweep();
                PdSource_YI2.CleanUpSweep();
                PdSource_YQ2.CleanUpSweep();
            }

            if (IsBiasPDTap) PdTapSource.CleanUpSweep();
        }



        /// <summary>
        /// Switch serial port to control specified ITLA
        /// </summary>
        /// <param name="itlaSource"></param>
        public static void SelectItla(Inst_CoRxITLALaserSource itlaSource)
        {
            if (itlaSource == null) return;

            itlaSource.GetItla().ixoliteChassis.SelectMyPort();
        }

        /// <summary>
        /// Setup oscilator to acquire phase angle waveform
        /// </summary>
        ///<param name="xScale_S"> sencondes in X scale per division </param>
        /// <param name="arrYScale_V"> Y scale array with 4 data, Volt in Y scale per division, the sequence should be XI, XQ, YI, YQ </param>
        /// <param name="dataPoint"> Data ponits in waveform data </param>
        /// <param name="waveformAverageFactor"></param>
        /// <param name="stopCondition"> Condition for waveform acquisition to stop  </param>
        /// <param name="bandWidth"> banwidth for each channel </param>
        /// <param name="coupling"> coupling mode for each channel </param>
        /// <param name="impedance"> Output impedance for each channel </param>
        public static void InitialiseOSC(double xScale_S, double[] arrYScale_V,
            int dataPoint, int waveformAverageFactor,
            Inst_Tds3034.EnumAcquisitionStopCondition stopCondition,
            Inst_Tds3034.EnumBandWidth bandWidth, Inst_Tds3034.EnumCouplingMode coupling,
            Inst_Tds3034.EnumImpedance impedance)
        {
            OSC_XI.AcquisitionState = false;

            // turn on all channel to display
            OSC_XI.IsChannelWaveformDisplay = true;
            OSC_XQ.IsChannelWaveformDisplay = true;
            OSC_YI.IsChannelWaveformDisplay = true;
            OSC_YQ.IsChannelWaveformDisplay = true;
            OSC_XI.IsChannelSelected = true;


            OSC_XI.BandWidth = bandWidth;
            OSC_XQ.BandWidth = bandWidth;
            OSC_YI.BandWidth = bandWidth;
            OSC_YQ.BandWidth = bandWidth;

            OSC_XI.Coupling = coupling;
            OSC_XQ.Coupling = coupling;
            OSC_YI.Coupling = coupling;
            OSC_YQ.Coupling = coupling;

            OSC_XI.Impedance = impedance;
            OSC_XQ.Impedance = impedance;
            OSC_YI.Impedance = impedance;
            OSC_YQ.Impedance = impedance;

            OSC_XI.VerticalPosition_Div = 3;
            OSC_XQ.VerticalPosition_Div = 1;
            OSC_YI.VerticalPosition_Div = -1;
            OSC_YQ.VerticalPosition_Div = -3;

            OSC_XI.VeticalScale_V = arrYScale_V[0];
            OSC_XQ.VeticalScale_V = arrYScale_V[1];
            OSC_YI.VeticalScale_V = arrYScale_V[2];
            OSC_YQ.VeticalScale_V = arrYScale_V[3];

            // I'm not sure which intrument should be on chan1& 3 , so set diplay format on all with YT by try
            // to avoid exception 
            try
            {
                OSC_XI.DisplayFormat = Inst_Tds3034.EnumDisplayFormat.YT;
            }
            catch
            { }
            try
            {
                OSC_XQ.DisplayFormat = Inst_Tds3034.EnumDisplayFormat.YT;
            }
            catch
            { }
            try
            {
                OSC_YI.DisplayFormat = Inst_Tds3034.EnumDisplayFormat.YT;
            }
            catch
            { }
            try
            {
                OSC_YQ.DisplayFormat = Inst_Tds3034.EnumDisplayFormat.YT;
            }
            catch
            { }

            // This will take effect on all channels             
            OSC_XI.ClearAllWaveformStorage();
            OSC_XI.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.AVE;
            OSC_XI.AcquisitionStopCondition = stopCondition;

            OSC_XI.AcquireAvgNum = waveformAverageFactor;           // move it to config setting

            OSC_XI.HorizontalScale_S = xScale_S;
            OSC_XI.HorizontalDelayStatus = false;
            OSC_XI.TriggerPosition = 50;
            OSC_XI.DataPoints = dataPoint;
            OSC_XI.WavformDataStartPoint = 1;
            OSC_XI.WaveformDataStopPoint = dataPoint;
            // set trigger to be edge trigger
            OSC_XI.SetTriggerEdge(Inst_Tds3034.EnumTriggerNum.MAI, Inst_Tds3034.EnumTrigerEdgeSource.CH2,
                Inst_Tds3034.EnumTiggerEdgeSlopeType.RIS, Inst_Tds3034.EnumTriggerEdgeCouplingType.DC);
            OSC_XI.EnableTriggerB = false;
        }

        /// <summary>
        /// Start a waveform acquisition
        /// </summary>
        public static void StartWaveformAcquire()
        {
            OSC_XI.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.AVE;
            OSC_XI.EnableServiceRequest();
            OSC_XI.HorizontalDelayStatus = false;

            OSC_XI.AcquisitionState = true;
            OSC_XI.RestartAcquisition();
            OSC_XI.Trigger();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns> Original   offset, amplitude, power, phase, period  </returns>
        public static StrcSinusoidalCoefficientsLimit GetWavefromParameter()
        {

            StrcSinusoidalCoefficientsLimit coeff_Limit = new StrcSinusoidalCoefficientsLimit();
            // Get the waveform characterise parameter as initial sinusoidal fit parameter  
            coeff_Limit.Coeff_XI = SetupOscillatorMeasurement(CoRxDCTestInstruments.OSC_XI);
            coeff_Limit.Coeff_XQ = SetupOscillatorMeasurement(CoRxDCTestInstruments.OSC_XQ);


            coeff_Limit.Coeff_YI = SetupOscillatorMeasurement(CoRxDCTestInstruments.OSC_YI);
            coeff_Limit.Coeff_YQ = SetupOscillatorMeasurement(CoRxDCTestInstruments.OSC_YQ);


            return coeff_Limit;
        }

        public static CoRxVOASweepData[] FetchVoaSweepResults()
        {
            if (!Is6464)
            {
                double[] PdSource_XI1_I, PdSource_XI2_Or_XQ_I, PdSource_YI1_I, PdSource_YI2_Or_YQ_I, PdTapSource_I, VOASource_I, VoltSweep;


                PdSource_XI1.GetSweepDataSet(out VoltSweep, out PdSource_XI1_I);
                PdSource_XI2.GetSweepDataSet(out VoltSweep, out PdSource_XI2_Or_XQ_I);
                PdSource_YI1.GetSweepDataSet(out VoltSweep, out PdSource_YI1_I);
                PdSource_YI2.GetSweepDataSet(out VoltSweep, out PdSource_YI2_Or_YQ_I);
                PdTapSource.GetSweepDataSet(out VoltSweep, out PdTapSource_I);
                VOASource.GetSweepDataSet(out VoltSweep, out VOASource_I);

                int sweepPoints = VoltSweep.Length;
                bool SameLength = sweepPoints == PdSource_XI1_I.Length && PdSource_XI1_I.Length == PdSource_XI2_Or_XQ_I.Length && PdSource_XI2_Or_XQ_I.Length == PdSource_YI1_I.Length && PdSource_YI1_I.Length == PdSource_YI2_Or_YQ_I.Length && PdSource_YI2_Or_YQ_I.Length == PdTapSource_I.Length && PdTapSource_I.Length == VOASource_I.Length;
                if (!SameLength)
                {

                    throw new FormatException("Sweep data vector received by the different instruments have different lengths");
                }

                CoRxVOASweepData[] returnData = new CoRxVOASweepData[sweepPoints];
                for (int i = 0; i < sweepPoints; ++i)
                {
                    returnData[i] = new CoRxVOASweepData(VoltSweep[i], VOASource_I[i], PdSource_XI1_I[i], PdSource_XI2_Or_XQ_I[i], PdSource_YI1_I[i], PdSource_YI2_Or_YQ_I[i], PdTapSource_I[i]);
                }

                return returnData;
            }
            else
            {
                double[] PdSource_XI, PdSource_XQ, PdSource_YI, PdSource_YQ, PdTapSource_I, VOASource_I, VoltSweep;


                PdSource_XI1.GetSweepDataSet(out VoltSweep, out PdSource_XI);
                PdSource_YI1.GetSweepDataSet(out VoltSweep, out PdSource_YI);
                PdSource_XQ1.GetSweepDataSet(out VoltSweep, out PdSource_XQ);
                PdSource_YQ1.GetSweepDataSet(out VoltSweep, out PdSource_YQ);
                PdTapSource.GetSweepDataSet(out VoltSweep, out PdTapSource_I);
                VOASource.GetSweepDataSet(out VoltSweep, out VOASource_I);

                int sweepPoints = VoltSweep.Length;
                bool SameLength = sweepPoints == PdSource_XI.Length && PdSource_XI.Length ==  PdSource_YI.Length && PdSource_YI.Length ==  PdTapSource_I.Length && PdTapSource_I.Length == VOASource_I.Length;
                if (!SameLength)
                {

                    throw new FormatException("Sweep data vector received by the different instruments have different lengths");
                }

                CoRxVOASweepData[] returnData = new CoRxVOASweepData[sweepPoints];
                for (int i = 0; i < sweepPoints; ++i)
                {
                    returnData[i] = new CoRxVOASweepData(VoltSweep[i], VOASource_I[i], PdSource_XI[i] , PdSource_XQ[i], PdSource_YI[i] , PdSource_YQ[i] , PdTapSource_I[i]);
                }

                return returnData;
            }

        }



        public static CoRxVOASweepData[] FetchVoaSweepResultsY()
        {
            if (!Is6464)
            {
                double[] PdSource_XI1_I, PdSource_XI2_I, PdSource_YI1_I, PdSource_YI2_I, PdTapSource_I, VOASource_I, VoltSweep;
                PdSource_XQ1.GetSweepDataSet(out VoltSweep, out PdSource_XI2_I);
                PdSource_XI1.GetSweepDataSet(out VoltSweep, out PdSource_XI1_I);
                 PdSource_YI1.GetSweepDataSet(out VoltSweep, out PdSource_YI1_I);
                PdSource_YQ1.GetSweepDataSet(out VoltSweep, out PdSource_YI2_I);
                PdTapSource.GetSweepDataSet(out VoltSweep, out PdTapSource_I);
                VOASource.GetSweepDataSet(out VoltSweep, out VOASource_I);

                int sweepPoints = VoltSweep.Length;
                bool SameLength = sweepPoints == PdSource_XI1_I.Length && PdSource_XI1_I.Length == PdSource_XI2_I.Length && PdSource_XI2_I.Length == PdSource_YI1_I.Length && PdSource_YI1_I.Length == PdSource_YI2_I.Length && PdSource_YI2_I.Length == PdTapSource_I.Length && PdTapSource_I.Length == VOASource_I.Length;
                if (!SameLength)
                {

                    throw new FormatException("Sweep data vector received by the different instruments have different lengths");
                }

                CoRxVOASweepData[] returnData = new CoRxVOASweepData[sweepPoints];
                for (int i = 0; i < sweepPoints; ++i)
                {
                    returnData[i] = new CoRxVOASweepData(VoltSweep[i], VOASource_I[i], PdSource_XI1_I[i], PdSource_XI2_I[i], PdSource_YI1_I[i], PdSource_YI2_I[i], PdTapSource_I[i]);
                }

                return returnData;
            }
            else
            {
                double[] PdSource_XI1_I, PdSource_YI1_I, PdTapSource_I, VOASource_I, VoltSweep;


                PdSource_XI1.GetSweepDataSet(out VoltSweep, out PdSource_XI1_I);
                PdSource_YI1.GetSweepDataSet(out VoltSweep, out PdSource_YI1_I);
                PdTapSource.GetSweepDataSet(out VoltSweep, out PdTapSource_I);
                VOASource.GetSweepDataSet(out VoltSweep, out VOASource_I);

                int sweepPoints = VoltSweep.Length;
                bool SameLength = sweepPoints == PdSource_XI1_I.Length && PdSource_XI1_I.Length == PdSource_YI1_I.Length && PdSource_YI1_I.Length == PdTapSource_I.Length && PdTapSource_I.Length == VOASource_I.Length;
                if (!SameLength)
                {

                    throw new FormatException("Sweep data vector received by the different instruments have different lengths");
                }

                CoRxVOASweepData[] returnData = new CoRxVOASweepData[sweepPoints];
                for (int i = 0; i < sweepPoints; ++i)
                {
                    returnData[i] = new CoRxVOASweepData(VoltSweep[i], VOASource_I[i], PdSource_XI1_I[i], PdSource_XI1_I[i], PdSource_YI1_I[i], PdSource_YI1_I[i], PdTapSource_I[i]);
                }

                return returnData;
            }

        }
        
        public static void ConfigureAndRunVoaSweep(Inst_Ke24xx master, double Voa_SweepStart_V, double Voa_SweepStop_V, int Voa_sweepPoints, double VsetPD_V, double VsetTap_V)
        {

            // three types of intruments: Master, Slave or Voa

            if (ReferenceEquals(master, VOASource))
                throw new ArgumentException("This algorithm does not allow the Master to be the Voa intrument");
            else
            {
                ConfigSourceTrigger(VOASource, Voa_SweepStart_V, Voa_SweepStop_V, Voa_sweepPoints, TriggerInLine, TriggerOutLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.DELAY, 0.041, 0.1); // 100 ms for the voa MEMS to settle and 25 ms to ensure that all instrument completed their measurements before setting a new voltage
                //ConfigSourceTrigger(VOASource, Voa_SweepStart_V, Voa_SweepStop_V, Voa_sweepPoints, TriggerInLine, TriggerOutLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.DELAY, 0.025, 10000); // 100 ms for the voa MEMS to settle and 25 ms to ensure that all instrument completed their measurements before setting a new voltage
            }

            List<Inst_Ke24xx> slaves = new List<Inst_Ke24xx>();
            if (!ReferenceEquals(master, PdSource_XI1)) slaves.Add(PdSource_XI1);
            if (!ReferenceEquals(master, PdSource_YI1)) slaves.Add(PdSource_YI1);
            if (!ReferenceEquals(master, PdTapSource)) slaves.Add(PdTapSource);

            if (!Is6464)
            {
                if (!ReferenceEquals(master, PdSource_XI2)) slaves.Add(PdSource_XI2);
                if (!ReferenceEquals(master, PdSource_YI2)) slaves.Add(PdSource_YI2);
            }
            else
            {
                if (!ReferenceEquals(master, PdSource_XQ1)) slaves.Add(PdSource_XQ1);
                if (!ReferenceEquals(master, PdSource_YQ1)) slaves.Add(PdSource_YQ1);
            }

            foreach (Inst_Ke24xx slave in slaves)
            {

                ConfigSourceTrigger(slave, ReferenceEquals(slave, PdTapSource) ? VsetTap_V : VsetPD_V, Voa_sweepPoints, TriggerInLine, TriggerNoUsedLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.NONE);
            }

            ConfigSourceTrigger(master, ReferenceEquals(master, PdSource_YQ1) ? VsetPD_V : VsetTap_V, Voa_sweepPoints, TriggerInLine, TriggerOutLine, Inst_Ke24xx.TriggerLayerType.DELAY, Inst_Ke24xx.TriggerLayerType.DELAY);


            foreach (Inst_Ke24xx slave in slaves)
            {
                slave.OutputEnabled = true;
            }

            master.OutputEnabled = true;

            VOASource.OutputEnabled = true;

            System.Threading.Thread.Sleep(2000);

            // move out from idle
            foreach (Inst_Ke24xx slave in slaves)
            {
                slave.Trigger();
            }

            master.Trigger();

            VOASource.Trigger();

            //wait till sweep completion
            master.WaitForSweepToFinish(60000);
            foreach (Inst_Ke24xx slave in slaves)
            {
                //slave.WaitForSweepToFinish(50);
                slave.WaitForSweepToFinish(50);
            }
            //VOASource.WaitForSweepToFinish(50);
            VOASource.WaitForSweepToFinish(50);

        }

        public static void ConfigureAndRunVoaSweepX(Inst_Ke24xx master, double Voa_SweepStart_V, double Voa_SweepStop_V, int Voa_sweepPoints, double VsetPD_V, double VsetTap_V)
        {

            // three types of intruments: Master, Slave or Voa

            if (ReferenceEquals(master, VOASource))
                throw new ArgumentException("This algorithm does not allow the Master to be the Voa intrument");
            else
            {
                ConfigSourceTrigger(VOASource, Voa_SweepStart_V, Voa_SweepStop_V, Voa_sweepPoints, TriggerInLine, TriggerOutLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.DELAY, 0.025, 0.1); // 100 ms for the voa MEMS to settle and 25 ms to ensure that all instrument completed their measurements before setting a new voltage
            }

            List<Inst_Ke24xx> slaves = new List<Inst_Ke24xx>();
            if (!ReferenceEquals(master, PdSource_XI1)) slaves.Add(PdSource_XI1);
            if (!ReferenceEquals(master, PdTapSource)) slaves.Add(PdTapSource);

            if (!Is6464)
            {
                if (!ReferenceEquals(master, PdSource_XI2)) slaves.Add(PdSource_XI2);
            }

            foreach (Inst_Ke24xx slave in slaves)
            {

                ConfigSourceTrigger(slave, ReferenceEquals(slave, PdTapSource) ? VsetTap_V : VsetPD_V, Voa_sweepPoints, TriggerInLine, TriggerNoUsedLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.NONE);
            }

            ConfigSourceTrigger(master, ReferenceEquals(master, PdTapSource) ? VsetTap_V : VsetPD_V, Voa_sweepPoints, TriggerOutLine, TriggerInLine, Inst_Ke24xx.TriggerLayerType.DELAY, Inst_Ke24xx.TriggerLayerType.DELAY);


            // enable output
            master.OutputEnabled = true;
            foreach (Inst_Ke24xx slave in slaves)
            {
                slave.OutputEnabled = true;
            }
            VOASource.OutputEnabled = true;

            // move out from idle
            master.Trigger();
            foreach (Inst_Ke24xx slave in slaves)
            {
                slave.Trigger();
            }
            VOASource.Trigger();

            //wait till sweep completion
            master.WaitForSweepToFinish(60000);
            foreach (Inst_Ke24xx slave in slaves)
            {
                slave.WaitForSweepToFinish(50);
            }
            VOASource.WaitForSweepToFinish(50);

        }


        /* auto off enabled
public static void ConfigureAndRunVoaSweep(Inst_Ke24xx master, double Voa_SweepStart_V, double Voa_SweepStop_V, int Voa_sweepPoints, double VsetPD_V, double VsetTap_V)
{

    // three types of intruments: Master, Slave or Voa
    if (ReferenceEquals(master, VOASource) || ReferenceEquals(master, TiaSource_X) || ReferenceEquals(master, TiaSource_Y))
        throw new ArgumentException("This algorithm does not allow the Master to be the Voa or TIA intrument");
    else
    {
        ConfigSourceTrigger(VOASource, Voa_SweepStart_V, Voa_SweepStop_V, Voa_sweepPoints, TriggerInLine, TriggerOutLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.DELAY, 0.15, 0.1); // 100 ms for the voa MEMS to settle and 150 ms to ensure that all instrument completed their measurements before setting a new voltage
        //ConfigSourceTrigger(VOASource, Voa_SweepStart_V, Voa_SweepStop_V, Voa_sweepPoints, TriggerInLine, TriggerOutLine, Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType.DELAY, 2, 0.5); // 100 ms for the voa MEMS to settle and 150 ms to ensure that all instrument completed their measurements before setting a new voltage
    }

    List<Inst_Ke24xx> slaves = new List<Inst_Ke24xx>();

    if (!ReferenceEquals(master, PdSource_XI1)) slaves.Add(PdSource_XI1);
    if (!ReferenceEquals(master, PdSource_XI2)) slaves.Add(PdSource_XI2);
    if (!ReferenceEquals(master, PdSource_YI1)) slaves.Add(PdSource_YI1);
    if (!ReferenceEquals(master, PdSource_YI2)) slaves.Add(PdSource_YI2);
    if (!ReferenceEquals(master, PdTapSource)) slaves.Add(PdTapSource);

    ConfigSourceTrigger(master, ReferenceEquals(master, PdTapSource) ? VsetTap_V : VsetPD_V, Voa_sweepPoints, TriggerOutLine, TriggerInLine, Inst_Ke24xx.TriggerLayerType.SOURCE, Inst_Ke24xx.TriggerLayerType.SENSE);

    for (int idx = 0; idx < slaves.Count; idx++)
    {
        Inst_Ke24xx slave = slaves[idx];
        double TriggerDelay = 0.025 * (double)(idx + 1); // allow 25ms per measurement let them measure sequencially becuase we are configuring the instrument to turn output off after each measurement
        //double TriggerDelay = 0.25 * (double)(idx + 1);
        ConfigSourceTrigger(slave, ReferenceEquals(slave, PdTapSource) ? VsetTap_V : VsetPD_V, Voa_sweepPoints, TriggerInLine, TriggerNoUsedLine, Inst_Ke24xx.TriggerLayerType.SOURCE, Inst_Ke24xx.TriggerLayerType.NONE, TriggerDelay);
    }

    SetAllPDSourceOutput(false);
    setAllTiaSourceOutput(true);
    SetAllPDSourceAutoOutput(true); //automatically output on before measurement and output off after measurement
    if (IsBiasPDTap) PdTapSource.EnableAutoOutput = true;
    if (IsBiasPDTap) PdTapSource.OutputEnabled = false;
    VOASource.OutputEnabled = true;

    // move out from idle
    master.Trigger();
    foreach (Inst_Ke24xx slave in slaves)
    {
        slave.Trigger();
    }
    VOASource.Trigger();

    //wait till sweep completion
    master.WaitForSweepToFinish(60000);
    foreach (Inst_Ke24xx slave in slaves)
    {
        slave.WaitForSweepToFinish(50);
    }
    VOASource.WaitForSweepToFinish(50);
    SetAllPDSourceAutoOutput(false);
    if (IsBiasPDTap) PdTapSource.EnableAutoOutput = false;
}
*/

        public static void ConfigSourceTrigger(Inst_Ke24xx source, double sweepStart_V, double sweepStop_V, int sweepPoints, int trigInLine, int trigOutLine, Inst_Ke24xx.TriggerLayerType TLinkInput, Inst_Ke24xx.TriggerLayerType TLinkOutput, double TriggerDelay_s = 0, double SourceDelay_s = 0)
        {

            source.CleanUpSweep();

            //Arm information
            source.ArmSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            source.ArmDirection = Inst_Ke24xx.TriggerDirectionType.ACCEPTOR;
            source.ArmCount = 1;

            //sweep information
            source.SourceSweptVoltage();
            source.SweepStartVoltage_V = sweepStart_V;
            source.SweepStopVoltage_V = sweepStop_V;
            if (sweepStop_V == sweepStart_V) throw new ArgumentException("Sweep start cannot be the same as Sweep Stop");
            source.SweepVoltageStep_V = (sweepStop_V - sweepStart_V) / (sweepPoints - 1);

            //trigger information
            source.TriggerSource = Inst_Ke24xx.TriggerSourceType.TLINk;
            source.TriggerDirection = Inst_Ke24xx.TriggerDirectionType.ACCEPTOR;
            source.TriggerInput = TLinkInput;
            source.TriggerOutput = TLinkOutput;
            source.TriggerCount = sweepPoints;
            source.TriggerDelay_s = TriggerDelay_s;
            source.SourceDelay_s = SourceDelay_s;
            source.ConfigureTriggerlines(trigInLine, trigOutLine);

            source.ClearSweepTraceData();
            source.ConfigureVoltageSweepReadings();


        }


        private static void ConfigSourceTrigger(Inst_Ke24xx source, double Vset_V,
        int sweepPoints, int trigInLine, int trigOutLine, Inst_Ke24xx.TriggerLayerType TLinkInput = Inst_Ke24xx.TriggerLayerType.SENSE, Inst_Ke24xx.TriggerLayerType TLinkOutput = Inst_Ke24xx.TriggerLayerType.SENSE, double TriggerDelay_s = 0, double SourceDelay_s = 0)
        {
            source.CleanUpSweep();
            source.SourceFixedVoltage();
            source.VoltageSetPoint_Volt = Vset_V;
            source.ConfigureVoltageSweepReadings();
            source.ArmSource = Inst_Ke24xx.TriggerSourceType.IMMediate;
            source.ArmDirection = Inst_Ke24xx.TriggerDirectionType.ACCEPTOR;
            source.ArmCount = 1;
            source.TriggerSource = Inst_Ke24xx.TriggerSourceType.TLINk;
            source.ConfigureTriggerlines(trigInLine, trigOutLine);
            source.TriggerDirection = Inst_Ke24xx.TriggerDirectionType.ACCEPTOR;
            source.TriggerInput = TLinkInput;
            source.TriggerDelay_s = TriggerDelay_s;
            source.SourceDelay_s = SourceDelay_s;
            source.TriggerOutput = TLinkOutput;
            source.TriggerCount = sweepPoints;
            source.ClearSweepTraceData();
            source.ConfigureVoltageSweepReadings();
        }

        private static void ConfigMasterSourceTrigger(Inst_Ke24xx master, double Vset_V, int sweepPoints, int trigInLine, int trigOutLine)
        {
            //arm count is the number of sweep points. Triggering is done via GPIB command to get it out of the arm layer at each point
            master.CleanUpSweep();
            master.SourceFixedVoltage();
            master.VoltageSetPoint_Volt = Vset_V;
            master.ConfigureVoltageSweepReadings();
            master.TriggerSource = Inst_Ke24xx.TriggerSourceType.TLINk;
            master.ConfigureTriggerlines(trigInLine, trigOutLine);
            master.TriggerDirection = Inst_Ke24xx.TriggerDirectionType.ACCEPTOR;
            master.TriggerInput = Inst_Ke24xx.TriggerLayerType.NONE;
            master.TriggerDelay_s = 0;
            master.SourceDelay_s = 0;
            master.TriggerOutput = Inst_Ke24xx.TriggerLayerType.DELAY;
            master.TriggerCount = 1;
            master.ArmSource = Inst_Ke24xx.TriggerSourceType.BUS;
            master.ArmDirection = Inst_Ke24xx.TriggerDirectionType.ACCEPTOR;
            master.ArmCount = sweepPoints;
            master.ClearSweepTraceData();
            master.ConfigureVoltageSweepReadings();
        }


        /// <summary>
        /// Setup oscilator measurement and get waveform charactorise parameter for specified channel
        /// </summary>
        /// <param name="oscChannel"> Oscilator channel to get characterise parameter </param>
        /// <returns> double array contains: 0: offset, 1: amplitude, 2, x's power, 3, initial phase, 4: period </returns>
        private static double[] SetupOscillatorMeasurement(Inst_Tds3034 oscChannel)
        {
            double[] arrCoeff = new double[5];

            oscChannel.SetupChannelMeasurement(Inst_Tds3034.EnumMeasurementNum.MEAS1);
            oscChannel.SetupChannelMeasurement(Inst_Tds3034.EnumMeasurementNum.MEAS2);
            oscChannel.SetupChannelMeasurement(Inst_Tds3034.EnumMeasurementNum.MEAS3);
            oscChannel.SetupChannelMeasurement(Inst_Tds3034.EnumMeasurementNum.MEAS4);

            oscChannel.SetMeasurementType(Inst_Tds3034.EnumMeasurementNum.MEAS1, Inst_Tds3034.EnumMeasurementType.LOW);
            oscChannel.SetMeasurementStatus(Inst_Tds3034.EnumMeasurementNum.MEAS1, true);
            oscChannel.SetMeasurementType(Inst_Tds3034.EnumMeasurementNum.MEAS2, Inst_Tds3034.EnumMeasurementType.HIGH);
            oscChannel.SetMeasurementStatus(Inst_Tds3034.EnumMeasurementNum.MEAS2, true);
            oscChannel.SetMeasurementType(Inst_Tds3034.EnumMeasurementNum.MEAS3, Inst_Tds3034.EnumMeasurementType.AMP);
            oscChannel.SetMeasurementStatus(Inst_Tds3034.EnumMeasurementNum.MEAS3, true);
            oscChannel.SetMeasurementType(Inst_Tds3034.EnumMeasurementNum.MEAS4, Inst_Tds3034.EnumMeasurementType.PERI);
            oscChannel.SetMeasurementStatus(Inst_Tds3034.EnumMeasurementNum.MEAS4, true);
            oscChannel.WaitForOperationToComplete();

            double low = oscChannel.GetMeasurementValue(Inst_Tds3034.EnumMeasurementNum.MEAS1);
            double high = oscChannel.GetMeasurementValue(Inst_Tds3034.EnumMeasurementNum.MEAS2);
            arrCoeff[0] = 0;// (low + high) / 2; // offset

            double amp = oscChannel.GetMeasurementValue(Inst_Tds3034.EnumMeasurementNum.MEAS3);
            arrCoeff[1] = 0; // amplitude     
            arrCoeff[2] = 1;    // x's power, 

            double period = 0;
            try
            {
                period = oscChannel.GetMeasurementValue(Inst_Tds3034.EnumMeasurementNum.MEAS4);
            }
            catch
            {
                throw new Exception("Can't get Period on channel " + oscChannel.Slot);
            }

            if (period == 0)
            {
                arrCoeff[4] = 0;
            }
            else
                arrCoeff[4] = 0.00000002; // initial period: 1/50M, for the AOM modulation freq is 50.5M Hz           

            arrCoeff[3] = 0;  // (period1 + period2) / 4;  // T/2               

            return arrCoeff;
        }
    }
}
