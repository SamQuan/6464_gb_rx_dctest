// [Copyright]
//
// Bookham ITLA Black Box
// Bookham.TestSolution.TunableModules
//
// CalTableConfigAccessor.cs
//
// Author: paul.annetts, 2006
// Design: ITLA Module Setup and Functional Test Program DD

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to access Calibration config files
    /// </summary>
    public class CalTableConfigAccessor
    {
        /// <summary>
        /// Constructor - loads the config file.
        /// </summary>
        /// <param name="dutObject">DUT Object with Test Jig data</param>
        /// <param name="configFile">The config file to load</param>
        public CalTableConfigAccessor(DUTObject dutObject, string configFile)
        {
            configKeys = new StringDictionary();
            configKeys["DeviceType"] = dutObject.PartCode;
            configKeys["TestStage"] = dutObject.TestStage;
            configKeys["ConnectorVariant"] = "C34";

            config =
                new ConfigDataAccessor(configFile, "CalibrationTable");         
        }

        /// <summary>
        /// Get Calibration Entry. Throws an exception if the calibration is not valid!
        /// </summary>
        /// <typeparam name="T">Type of calibration entry, can be bool, int, double 
        /// or string</typeparam>
        /// <param name="calName">Calibration name</param>
        /// <returns>Calibration value of type T</returns>
        public T GetCalibrationEntry<T>(string calName)
        {
            bool isValid = false;
            T val = GetCalibrationEntry<T>(calName, out isValid);
            if (!isValid)
            {
                throw new Exception("Calibration '" + calName + "' has expired");
            }
            return val;
        }

        /// <summary>
        /// Get Calibration Entry. This overload allows you to check if the calibration
        /// is valid. Use the other overload without the boolean reference parameter 
        /// to include a check on validity within the call.
        /// </summary>
        /// <typeparam name="T">Type of calibration entry, can be bool, int, double 
        /// or string</typeparam>
        /// <param name="calName">Calibration name</param>
        /// <param name="isValid">Reference parameter that determines if cal is valid</param>
        /// <returns>Calibration value of type T</returns>
        public T GetCalibrationEntry<T>(string calName, out bool isValid) 
        {
            // adjust parameter name
            configKeys["CalName"] = calName;

            try
            {
                DatumList configRow = config.GetData(configKeys, true);
                
                string calPeriodStr = configRow.ReadString("CalPeriod");
                string calLastDoneStr = configRow.ReadString("LastCalDone");
                int calCountMax = configRow.ReadSint32("CalCountMax");
                int calCount = configRow.ReadSint32("CalCount");
                // check the validity
                CalStdValidity calValidity = new CalStdValidity(calPeriodStr,
                        calLastDoneStr, calCountMax, calCount);
                isValid = calValidity.CalStdValid;
                
                Type t = typeof(T);
                T val;
                
                string str = configRow.ReadString("CalValue");

                object obj;

                if (t == typeof(string))
                {
                    obj = str;
                }
                else if (t == typeof(int))
                {
                    obj = int.Parse(str);                    
                }
                else if (t == typeof(double))
                {
                    obj = double.Parse(str);
                }
                else if (t == typeof(bool))
                {
                    obj = bool.Parse(str);
                }
                else
                {
                    throw new Exception("Unsupported type: " + t.ToString());
                }

                val = (T)obj;
                return val;
            }
            catch (Exception e)
            {
                throw new Exception("Couldn't find Cal Name: '" + calName + "'", e);
            }
        }
        
        /// <summary>
        /// Write Calibration Entry. Throws an exception if the calibration entry is not already present!
        /// </summary>
        /// <typeparam name="T">Type of calibration entry, can be bool, int, double 
        /// or string</typeparam>
        /// <param name="calName">Calibration name</param>
        /// <param name="value">Value to write</param>
        public void WriteCalibrationEntry<T>(string calName, T value)
        {
            // adjust parameter name
            configKeys["CalName"] = calName;
            int rows = config.MatchingRowCount(configKeys, true);
            if (rows != 1) 
            {
                throw new Exception("Config for '"+ calName + "' must already exist to be updated!");
            }
            
            DatumList oldConfigData;
                        
            // single row - read it back and delete
            oldConfigData = config.GetData(configKeys, true);
            
            DatumList newConfigData = new DatumList();
            foreach (Datum dat in oldConfigData)
            {
                if (dat.Name == "CalValue")
                {
                    newConfigData.AddString("CalValue", value.ToString());
                }
                else if (dat.Name == "LastCalDone")
                {
                    newConfigData.AddString("LastCalDone", DateTime.Now.ToString());
                }
                else newConfigData.Add(dat);
            }
            config.UpdateData(oldConfigData, newConfigData);
            config.SaveChangesToFile();            
        }

        #region Private data
        StringDictionary configKeys;
        ConfigDataAccessor config;
        #endregion
    }
}
