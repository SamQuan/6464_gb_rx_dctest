// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/FactoryWorks/FactoryWorks.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks MES Plug-In DD

using System;
using System.Text;
using System.Timers;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.Program;


namespace Bookham.TestLibrary.MES
{

	/// <summary>
	/// FactoryWorks specific MES plug-in
	/// </summary>
	public sealed class FactoryWorks : IMES
	{

		#region Constructors and related methods
		/// <summary>
		/// Constructor
		/// </summary>
		public FactoryWorks()
		{
			this.config = new FactoryWorksConfig();
			this.config.LoadConfiguration();
			this.Initialise();
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="configFilePath">Path to config file</param>
		public FactoryWorks( string configFilePath )
		{
			this.config = new FactoryWorksConfig(configFilePath);
			this.config.LoadConfiguration();
			this.Initialise();
		}

		private void Initialise()
		{
			// Standard attributes as returned by LOTQUERYFULL
			this.standardAttributes = new StringCollection();
			this.standardAttributes.Add( "CompId" );
			this.standardAttributes.Add( "FacetCoatId" );
			this.standardAttributes.Add( "WaferId" );
			this.standardAttributes.Add( "ChipId" );
			this.standardAttributes.Add( "Position" );
			this.standardAttributes.Add( "PartId" );
			this.standardAttributes.Add( "OutputTray" );
			this.standardAttributes.Add( "2DBarcode" );
			this.standardAttributes.Add( "MesaWidth" );
			this.standardAttributes.Add( "SpecificId" );
			this.standardAttributes.Add( "FibreType" );

			// Regular expression for time-date e.g. 2005-12-25 00:00:00
			this.responseTimeDateRE = "^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}";

			Request.Node = config.Node;
			Request.DropDir = config.RequestDir;
			Request.CreateTempLocalFolderIfNotPresent();

			Response.Node = config.Node;
			Response.DropDir = config.ResponseDir;
		}

		#endregion

		#region IMES implementation

        /// <summary>
        /// Initialise method to set up plugin object with initial values. 
        /// Pass in the MES ILogging interface. 
        /// </summary>
        /// <param name="logger">interface handle to system Logging object</param>
        /// <param name="usingUctDateTime">If true,use UTC Date Time,otherwise,use local date time.</param>
        public void Initialise(ILogging logger,bool usingUctDateTime)
        {
            if (logger == null)
            {
                throw new Bookham.TestEngine.PluginInterfaces.MES.MESFatalErrorException("'logger' is null");
            }

            this.logger = logger;
            usingUtc = usingUctDateTime;
        }

		/// <summary>
		/// Node number (uniquely identifies testset/client)
		/// </summary>
		public int Node
		{
			set
			{
				this.config.Node = value;
			}
			get
			{
				return this.config.Node;
			}
		}

		/// <summary>
		/// Get information about a batch/lot
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="getAttributes">If true, get the default set of attributes</param>
		/// <returns>The batch</returns>
		public MESbatch LoadBatch( string batchId, bool getAttributes )
		{
			if( getAttributes )
				return LOTQUERYFULL(batchId);
			else
				return LOTQUERY(batchId);
		}

		/// <summary>
		/// Get specific information about a batch/lot
		/// User specifies which attributes to fetch
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="getTheseBatchAttributes">Get these batch attributes specifically</param>
		/// <param name="getTheseComponentAttributes">Get these component attributes specifically</param>
		/// <returns>The batch</returns>
		public MESbatch LoadBatch( string batchId, string[] getTheseBatchAttributes, string[] getTheseComponentAttributes )
		{
			// Get the batch and all 'standard' component attributes
			MESbatch batch = LOTQUERYFULL(batchId);

			// Note that current FactoryWorks comms protocol forces
			// a call to ATTRIB_GET for each and every attribute that
			// doesn't belong to the 'standard' group.

			// Now look to see if any other batch attibutes are required
			if( getTheseBatchAttributes != null )
			{
				foreach( string attribName in getTheseBatchAttributes )
				{
					if( standardAttributes.Contains(attribName) == false )
					{
						string attribValue = this.ATTRIB_GET(batchId,"",attribName);
						batch.Attributes.Add( new DatumString(attribName,attribValue) );
					}
				}
			}

			// Now look to see if any other component attibutes are required
			if( getTheseComponentAttributes != null )
			{
				foreach( string attribName in getTheseComponentAttributes )
				{
					if( standardAttributes.Contains(attribName) == false )
					{
						foreach( DUTObject dut in batch )
						{
							string componentId = dut.SerialNumber;
							string attribValue = this.ATTRIB_GET(batchId,componentId,attribName);
							dut.Attributes.Add( new DatumString(attribName,attribValue) );
						}
					}
				}
			}

			return batch;
		}

		/// <summary>
		/// Track batch into next step
		/// </summary>
		/// <param name="batchId">Batch to track in</param>
		public void TrackIn( string batchId )
		{
			this.LOTSTATE(batchId,config.TrackInResponse.WaitForResponse);
		}

		/// <summary>
		/// Set a batch level attribute
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="attributeName">Attribute name</param>
		/// <param name="attributeValue">Attribute value</param>
		public void SetBatchAttribute( string batchId, string attributeName, string attributeValue )
		{
			this.ATTRIB_SET( batchId, "", attributeName, attributeValue, config.SetAttributeResponse.WaitForResponse );
		}

		/// <summary>
		/// Set a component level attribute
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="componentId">Component</param>
		/// <param name="attributeName">Attribute name</param>
		/// <param name="attributeValue">Attribute value</param>
		public void SetComponentAttribute( string batchId, string componentId, string attributeName, string attributeValue )
		{
			this.ATTRIB_SET( batchId, componentId, attributeName, attributeValue, config.SetAttributeResponse.WaitForResponse );
		}

		/// <summary>
		/// Mark the selected components as defective
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="componentIDlist">Components to mark as defective</param>
		/// <param name="failCodeList">Corresponding failure codes (one per component)</param>
		public 	void MarkAsDefective( string batchId, string[] componentIDlist, string[] failCodeList )
		{
			this.MARKASDEFECTIVE( batchId, componentIDlist, failCodeList, config.SetAttributeResponse.WaitForResponse );
		}

		/// <summary>
		/// Split a component into a new batch and track out
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to split</param>
		/// <param name="partCode">Partcode to assign to component</param>
		/// <returns>New batch containing component</returns>
		public string TrackOutComponentPass( string batchId, string stage, string componentId, string partCode )
		{
			return this.BTRACKOUT( batchId, stage, componentId, partCode, config.TrackOutComponentPassReworkOnHoldResponse.WaitForResponse );
		}

		/// <summary>
		/// Split a component into a new batch and send for rework
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to split</param>
		/// <param name="reworkCode">Rework code</param>
		/// <returns>New batch containing component</returns>
		public string TrackOutComponentRework( string batchId, string stage, string componentId, string reworkCode )
		{
			return this.REWORK( batchId, stage, componentId, reworkCode, config.TrackOutComponentPassReworkOnHoldResponse.WaitForResponse );
		}

		/// <summary>
		/// Split a component into a new batch and put on hold
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to split</param>
		/// <param name="onHoldCode">On-hold code</param>
		/// <returns>New batch containing component</returns>
		public string TrackOutComponentOnHold( string batchId, string stage, string componentId, string onHoldCode )
		{
			return this.ONHOLD( batchId, stage, componentId, onHoldCode, config.TrackOutComponentPassReworkOnHoldResponse.WaitForResponse );
		}

		/// <summary>
		/// Split a component into a new batch and scrap
		/// </summary>
		/// <param name="batchId">Current batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to scrap</param>
		/// <param name="failCode">Failure code</param>
		public void TrackOutComponentFail( string batchId, string stage, string componentId, string failCode )
		{
			this.FAILURE( batchId, stage, componentId, failCode, config.TrackOutComponentFailResponse.WaitForResponse );
		}

		/// <summary>
		/// Track out whole batch and optionally assign new part code
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="partCode">New part code (optional)</param>
		public void TrackOutBatchPass( string batchId, string stage, string partCode )
		{
			this.TRACKOUT( batchId, stage, partCode, config.TrackOutBatchResponse.WaitForResponse );
		}

		/// <summary>
		/// Track out whole batch and put on hold
		/// </summary>
		/// <param name="batchId">Batch</param>
		/// <param name="stage">Stage</param>
        /// <param name="onHoldCode">On-hold code</param>
		public void TrackOutBatchOnHold( string batchId, string stage, string onHoldCode )
		{
			this.ONHOLD( batchId, stage, onHoldCode, config.TrackOutBatchResponse.WaitForResponse );
		}
		# endregion

		#region Private FactoryWorks TSSrv Methods
		/// <summary>
		/// LOTSTATE
		/// Used here only to track in the specified batch
		/// </summary>
		/// <param name="batchId">Batch to track in</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		private void LOTSTATE( string batchId, bool waitForResponse )
		{
			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );

			// 
			// EXAMPLE REQUEST AND RESPONSE
 			// 
			// ,[MH100138.1],,,,MODTEST,,181,,,,LOTSTATE,,fwmes,
			// 
			// SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
			// 

			// Generate request and read response
			using( Request request = new Request( batchId, "", "", "", "", "LOTSTATE", "", RequestLevel.LOT, waitForResponse ) )
			{ 
				if( waitForResponse )
				{
					// Regular expression for success response e.g. SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
					string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),$";

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackInResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
				}
			}
		}

		/// <summary>
		/// Perform a LOTQUERY
		/// For the specified batch, find the product, stage and components therein 
		/// </summary>
		/// <param name="batchId">The batch to load</param>
		/// <returns>The batch as an MESbatch object</returns>
		private MESbatch LOTQUERY( string batchId )
		{	
			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );

			//
			// EXAMPLE REQUEST AND RESPONSE
			// 
			// ,[MH100138.2],,,,,,181,,,,LOTQUERY,,fwmes,
			// 
			// 2003-11-14 13:37:18 SUCCESS,MH100138.2,MODPACK,MT25TABCC-C59,MH100138.001,
			// 

			// Generate request and read response
			using( Request request = new Request( batchId, "", "", "", "", "LOTQUERY", "", RequestLevel.LOT, true ) )
			{
				// Regular expression for success response e.g. SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
				string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),$";


                string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelRead, successREtext, config.LoadBatchResponse.Timeout_s);

				// Parse response
				string[] responseData = fileContentsLine[0].Split( new char[]{','} );;

				MESbatch batch = new MESbatch();
				batch.Stage = responseData[2];
				batch.PartCode = responseData[3];
				int numDUTs = responseData.Length - 5;
				//batch.DUT = new DUTObject[numDUTs];
				for( int i = 0; i < numDUTs; i++ )
				{
					DUTObject dut = new DUTObject();
					dut.SerialNumber = responseData[i+4];
					batch.Add( dut );
				}

				return batch;
			}
		}

		/// <summary>
		/// Perform a LOTQUERYFULL
		/// As LOTQUERY but load all 'standard' component attributes
		/// </summary>
		/// <param name="batchId">The batch to load</param>
		/// <returns>The batch as an MESbatch object</returns>
		private MESbatch LOTQUERYFULL( string batchId )
		{

			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );

			//
			// EXAMPLE REQUEST AND RESPONSE
			// 
			// ,[MH100138.2],,,,,,181,,,,LOTQUERYFULL,,fwmes,
			//
			// 2003-11-14 13:35:23 SUCCESS,MH100138.2,MODPACK,MT25TABCC-C59,1,2003-11-14 13:35:23 (CompId,FacetCoatId,WaferId,ChipId,position,PartId,OutputTray,2DBarcode,MesaWidth,SpecificId,FibreType)
			// 2003-11-14 13:35:23 (MH100138.001,,,,,,,,,,)
			//

			// Generate request and read response
			using( Request request = new Request( batchId, "", "", "", "", "LOTQUERYFULL", "", RequestLevel.LOT, true ) )
			{
				// Regular expression for success response e.g. SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
				string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),";

                string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelRead, successREtext, config.LoadBatchResponse.Timeout_s);
				// Parse response
				string[] responseData = fileContentsLine[0].Split( new char[]{','} );
				
				MESbatch batch = new MESbatch();
				batch.Stage = responseData[2];
				batch.PartCode = responseData[3];

				if( fileContentsLine[1] != "(CompId,FacetCoatId,WaferId,ChipId,position,PartId,OutputTray,2DBarcode,MesaWidth,SpecificId,FibreType)" )
					throw new MESCommsException( "Bad LOTQUERYFULL response format" );

				int numDUTs = fileContentsLine.Length - 2;

				for( int i = 0; i < numDUTs; i++ )
				{
					string lineCSVtext = fileContentsLine[i+2];
					char[] brackets = new char[]{'(',')'};
					string[] attrib = lineCSVtext.Trim(brackets).Split( new char[]{','} );
					
					DUTObject dut = new DUTObject();

					dut.SerialNumber = attrib[0];
					dut.TrayPosition = attrib[4];
					dut.PartCode = batch.PartCode;
					dut.TestStage = batch.Stage;
					dut.BatchID = batchId;
					dut.EquipmentID = attrib[0];			

					dut.Attributes.AddString( "FacetCoatId", attrib[1] );
					dut.Attributes.AddString( "WaferId", attrib[2] );
					dut.Attributes.AddString( "ChipId", attrib[3] );
					dut.Attributes.AddString( "OutputTray", attrib[6] );
					dut.Attributes.AddString( "2DBarcode", attrib[7] );
					dut.Attributes.AddString( "MesaWidth", attrib[8] );
					dut.Attributes.AddString( "SpecificId", attrib[9] );
					dut.Attributes.AddString( "FibreType", attrib[10] );

					batch.Add( dut );
				}

				return batch;
			}
		}

		/// <summary>
		/// Read the value of a batch or component level attribute
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="componentId">The component ID (pass empty string for batch level attributes)</param>
		/// <param name="attribName">Name of attribute to read</param>
		/// <returns>The attribute value</returns>
		private string ATTRIB_GET( string batchId, string componentId, string attribName )
		{
			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			// componentId can be empty
			CheckArg( "attribName", attribName );

			// To Set and Read a Component level attribute:-
			// 
			// ,[CL99453.1],,,,,,181,,,,ATTRIB,,fwmes,CarrierType
			// 
			// 2005-11-17 12:54:36 SUCCESS,,CarrierType=Vishay
			// 
			// 
			// ,[MH100138.1]MH100138.001,,,,,,181,,,,ATTRIB,,fwmes,ReworkCode
			// 
			// 2003-11-13 13:49:54 SUCCESS,MH100138.001,ReworkCode=CompValue
			// 

			using( Request request = new Request( batchId, componentId, "", "", "", "ATTRIB", attribName, RequestLevel.LOT, true ) ) // Lot level since TSSrv mod to not respond to attribute requests in FW_COC
			{
				// Regular expression for success response e.g. SUCCESS,MH100138.001,ReworkCode=CompValue
				string successREtext = "SUCCESS," + componentId + "," + attribName + "=.*";

                string[] fileLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelRead, successREtext, config.SetAttributeResponse.Timeout_s);
				
				// Only get here if response is success, otherwise Exception will have been thrown					
				string[] commaToken = fileLine[0].Split( new char[]{','} );
				string[] equalsToken = commaToken[2].Split( new char[]{'='} );
				return equalsToken[1];
			}
		}

		/// <summary>
		/// Set the value of a batch or component level attribute
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="componentId">The component ID (pass empty string for batch level attributes)</param>
		/// <param name="attribName">Name of attribute to set</param>
		/// <param name="attribVal">Value to set</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		private void ATTRIB_SET( string batchId, string componentId, string attribName, string attribVal, bool waitForResponse )
		{
			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			// componentId can be empty
			CheckArg( "attribName", attribName );
			// attribVal can be empty

			// To Set and Read a Component level attribute:-
			// 
			// ,[MH100138.1]MH100138.001,,,,,,181,,,,ATTRIB,,fwmes,ReworkCode:CompValue:
			// 
			// 2003-11-13 13:30:04 SUCCESS: ATTRIB function complete for lot MH100138.1
			// 
			// 
			// ,[MH100138.1]MH100138.001,,,,,,181,,,,ATTRIB,,fwmes,ReworkCode
			// 
			// 2003-11-13 13:49:54 SUCCESS,MH100138.001,ReworkCode=CompValue
			// 

			using( Request request = new Request( batchId, componentId, "", "", "", "ATTRIB", attribName+":"+attribVal+":", RequestLevel.COMPONENT, waitForResponse ) )
			{

				if( waitForResponse )
				{
					// Regular expression for success response
					string successREtext = "SUCCESS: ATTRIB function complete for lot " + batchId;

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.SetAttributeResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
				}
			}
		}

		/// <summary>
		/// Mark one or more components as defective
		/// (They will be scrapped on track-out)
		/// </summary>
		/// <param name="batchId">The batch ID</param>
		/// <param name="componentIDlist">List of components within the batch to be marked as defective</param>
		/// <param name="failModelist">Corresponding list of failure reasons</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		private void MARKASDEFECTIVE( string batchId, string[] componentIDlist, string[] failModelist, bool waitForResponse )
		{
			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			if( componentIDlist.Length == 0 ) throw new MESInvalidOperationException( "Must specifiy at least one component to mark as defective" );
			if( componentIDlist.Length != failModelist.Length ) throw new MESInvalidOperationException( "componentIDlist and failModelist arrays must be of the same length" );

			//,[MH100138.7]MH100138.003|Other|:MH100138.004|LowPower|,,,,,,181,,,,MARKASDEFECTIVE,,fwmes,
			//
			// 2003-11-18 17:08:17 SUCCESS: MARKASDEFECTIVE function complete for lot MH100138.7
			//
			// Note:- Different separators used between ComponentIds and DefectCodes.

			StringBuilder componentAndFailModeList = new StringBuilder("");
			for( int i = 0; i < componentIDlist.Length; i++ )
			{
				componentAndFailModeList.Append( componentIDlist[i] );
				componentAndFailModeList.Append( "|" );
				componentAndFailModeList.Append( failModelist[i] );
				if( i != componentIDlist.Length )
					componentAndFailModeList.Append( ":" );
			}

			using( Request request = new Request( batchId, componentAndFailModeList.ToString(), "", "", "", "MARKASDEFECTIVE", "", RequestLevel.LOT, true ) )
			{
				if( waitForResponse )
				{
					// Regular expression for success response e.g. SUCCESS: MARKASDEFECTIVE function complete for lot MH100138.7
					string successREtext = "SUCCESS: MARKASDEFECTIVE function complete for lot .*";

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.MarkAsDefectiveResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
				}
			}
		}

		/// <summary>
		/// Split component from batch and send for rework
		/// </summary>
		/// <param name="batchId">Batch containing component to rework</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">Component to rework</param>
		/// <param name="reworkCode">Rework code</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		/// <returns>ID of batch containing reworked component</returns>
		private string REWORK( string batchId, string stage, string componentId, string reworkCode, bool waitForResponse )
		{
			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			CheckArg( "componentId", componentId );
			CheckArg( "reworkCode", reworkCode );

			// ,[MH100138.2],,MT25TABCC-C59,,MODTEST,,181,,Delta,,REWORK,,fwmes,
			// 
			// 2003-11-17 13:38:53 SUCCESS: REWORK function complete for lot MH100138.2
			// 
			// Note:- Sent lot down REWORK path and sets ReworkCode attribute to Delta.
			// 
			// 
			// or
			// 
			// ,[MH100138.1]MH100138.001,,,,,,181,,CapDamage,,REWORK,,fwmes,
			// 
			// 2003-11-17 13:42:23 SUCCESS: REWORK function complete for lot MH100138.2
			// 
			// Note:- Component MH100138.001 split from batch into new batch (MH100138.2).    New lot MH100138.2 sent down REWORK path and ReworkCode set to CapDamage.  PartId and FwStageName are optional.
			// 

			using( Request request = new Request( batchId, componentId, "", stage, reworkCode, "REWORK", "", RequestLevel.COMPONENT, true ) )
			{
				if( waitForResponse )
				{
					// Regular expression for success response e.g. SUCCESS: REWORK function complete for lot MH100138.2
					string successREtext = "SUCCESS: REWORK function complete for lot .*";

                    string[] fileLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutComponentPassReworkOnHoldResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown

					string[] spaceToken = fileLine[0].Split( new char[]{' '} );
					return spaceToken[6];
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
					return "";
				}
			}
		}

		/// <summary>
		/// Put a batch on hold
		/// </summary>
		/// <param name="batchId">Batch to put on hold</param>
        /// <param name="stage">Test Stage</param>
		/// <param name="onHoldCode">On-hold code</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		private void ONHOLD( string batchId, string stage, string onHoldCode, bool waitForResponse )
		{

			// 
			// ,[MH100138.8],,,,MODTEST,,181,,AcFailed,,ONHOLD,,fwmes,
			// 
			// 2003-11-14 10:02:15 SUCCESS: ONHOLD function complete for lot MH100138.8
			// 
			// Note:- Puts lot MH100138.8 on Hold and sets the HoldCode attribute to AcFailed.
			// 

			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			CheckArg( "stage", stage );
			CheckArg( "onHoldCode", onHoldCode );


			using( Request request = new Request( batchId, "", "", stage, onHoldCode, "ONHOLD", "", RequestLevel.COMPONENT, true ) )
			{
                if (waitForResponse)
                {
                    // Regular expression for success response e.g. SUCCESS: ONHOLD function complete for lot MH100138.8
                    string successREtext = "SUCCESS: ONHOLD function complete for lot .*";

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutBatchResponse.Timeout_s);

                    // Only get here if response is success, otherwise Exception will have been thrown
                }
                else
                {
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
                    // Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
                }
			}
		}

		/// <summary>
		/// Split a single component into a new batch and put that batch on hold
		/// </summary>
		/// <param name="batchId">Batch containing component to put on hold</param>
		/// <param name="stage">Test stage</param>
		/// <param name="componentId">Component to put on hold</param>
		/// <param name="onHoldCode">On-hold code</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		/// <returns></returns>
		private string ONHOLD( string batchId, string stage, string componentId, string onHoldCode, bool waitForResponse )
		{
			// 
			// ,[MH100138.1]MH100138.002,,,,MODTEST,,181,,Icc,,ONHOLD,,fwmes,
			// 
			// 2003-11-14 09:43:56 SUCCESS: ONHOLD function complete for lot MH100138.6
			// 
			// Note:- Splits component MH100138.002 to new batch (MH100138.6), set HoldCode attribute for the new lot and puts it on hold.
			// 

			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			CheckArg( "stage", stage );
			CheckArg( "componentId", componentId );
			CheckArg( "onHoldCode", onHoldCode );


			using( Request request = new Request( batchId, componentId, "", stage, onHoldCode, "ONHOLD", "", RequestLevel.COMPONENT, true ) )
			{ 
				if( waitForResponse )
				{
					// Regular expression for success response e.g. SUCCESS: ONHOLD function complete for lot MH100138.8
					string successREtext = "SUCCESS: ONHOLD function complete for lot .*";

                    string[] fileLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutComponentPassReworkOnHoldResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
					string[] spaceToken = fileLine[0].Split( new char[]{' '} );
					return spaceToken[6];
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
					return "";
				}
			}
		}

		/// <summary>
		/// TRACKOUT
		/// Track a batch out from its current stage
		/// </summary>
		/// <param name="batchId">The batch to track out</param>
		/// <param name="stage">The Stage the batch is in pre-trackout.</param>
		/// <param name="partCode">The part code to set (batch level)</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		private void TRACKOUT( string batchId, string stage, string partCode, bool waitForResponse )
		{
			//
			// EXAMPLE REQUEST AND RESPONSE
			// 
			// ,[MH100138.1],,MT25TABCC-C59,,MODTEST,,181,,,,TRACKOUT,,fwmes,
			// 
			// 2003-11-13 15:50:47 SUCCESS: TRACKOUT function complete for lot MH100138.1
			// 
			// Note:- The PartId is optional and does not change the lot attribute.
			// 

			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			CheckArg( "stage", stage );
			//CheckArg( "partCode", partCode );

			using( Request request = new Request( batchId, "", partCode, stage, "", "TRACKOUT", "", RequestLevel.LOT, waitForResponse ) )
			{
				if( waitForResponse )
				{
					// Regular expression for success response
					string successREtext = "SUCCESS: TRACKOUT function complete for lot " + batchId;

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutBatchResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
				}
			}
		}

		/// <summary>
		/// BTRACKOUT
		/// To change the PartId of a component and split it to a new batch.
		/// </summary>
		/// <param name="batchId">The batch containing the component</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">The componentId</param>
		/// <param name="partCode">The partCode to apply to the component (optional)</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		/// <returns>The ID of the new batch</returns>
		private string BTRACKOUT( string batchId, string stage, string componentId, string partCode, bool waitForResponse )
		{

			//
			// ,[MH100138.1],,MH100138.001:MT25TABCC-C34,,MODTEST,,181,,,,BTRACKOUT,,fwmes,
			//
			// 2003-11-13 16:07:25 SUCCESS: BTRACKOUT function complete for lot MH100138.1
			//
			// Note:- MH100138.001 was split to a new batch (MH100138.2), the Lot level PartId changed to MT25TABCC-C34 and Tracked-out.  The original batch (MH100138.1) is not Tracked-out.
			//

			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			CheckArg( "componentId", componentId );
			// Note: partCode is optional

			using( Request request = new Request( batchId, componentId, partCode, stage, "", "BTRACKOUT", "", RequestLevel.LOT, waitForResponse ) )
			{
				if( waitForResponse )
				{
					// Regular expression for success response
					string successREtext = "SUCCESS: BTRACKOUT function complete for lot " + batchId;

                    string[] fileLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutComponentPassReworkOnHoldResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
					string[] spaceToken = fileLine[0].Split( new char[]{' '} );
					return spaceToken[6];
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
					return "";
				}
			}
		}

		/// <summary>
		///  Split component from batch and scrap which results in new batch terminating
		/// </summary>
		/// <param name="batchId">The batch</param>
		/// <param name="stage">Stage</param>
		/// <param name="componentId">The component to scrap</param>
		/// <param name="failCode">Reason for component failure/scrap</param>
		/// <param name="waitForResponse">If true, wait for confirmation that the operation has completed</param>
		private void FAILURE( string batchId, string stage, string componentId, string failCode, bool waitForResponse )
		{
			// ,[MH100138.3]MH100138.003,,,,MODTEST,,181,,Icc,,FAILURE,,fwmes,
			//
			// SUCCESS: FAILURE function complete for lot MH100138.3

			// PRECONDITIONS - check here before request is generated
			CheckArg( "batchId", batchId );
			CheckArg( "componentId", componentId );

			using( Request request = new Request( batchId, componentId, "", stage, failCode, "FAILURE", "", RequestLevel.LOT, waitForResponse ) )
			{
				if( waitForResponse )
				{
					// Regular expression for success response
					string successREtext = "SUCCESS: FAILURE function complete for lot " + batchId;

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutComponentFailResponse.Timeout_s);

					// Only get here if response is success, otherwise Exception will have been thrown
				}
				else
				{
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
					// Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
				}
			}
		}
		#endregion

        #region xiaojiang for new FW rule 20160608

        //load batch
        public MESbatch LOTQUERYNEW(string batchId, string userId)
        {
            // PRECONDITIONS - check here before request is generated
            CheckArg("batchId", batchId);
            CheckArg("userId", userId);

            //
            // EXAMPLE REQUEST AND RESPONSE
            // 
            //Drop file:
            //,[CK103520.1],,,,,,2164,,,,LOTQUERYNEW,,fwmes,

            //Return File:
            //SUCCESS,LotId,FwStageName,PartId,ComponentQty,ComponentId,currentRule,currentStepStatus,availablePath
            //2016-06-07 10:15:04 SUCCESS,CK103520.1,FINAL,PA013444,1,CK103520.001,trackOut,WaitingFortrackOut,REWORK,
            //

            // Generate request and read response
            using (Request request = new Request(batchId, "", "", "", "", "LOTQUERYNEW", "", RequestLevel.LOT, true, userId))
            {
                // Regular expression for success response e.g. SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
                string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),([^,]*),([^,]*),([^,]*),([^,]*)";

                string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelRead, successREtext, config.LoadBatchResponse.Timeout_s);
                // Parse response
                string[] responseData = fileContentsLine[0].Split(new char[] { ',' });

                MESbatch batch = new MESbatch();
                batch.Stage = responseData[2];
                batch.PartCode = responseData[3];

                //if (fileContentsLine[1] != "(CompId,FacetCoatId,WaferId,ChipId,position,PartId,OutputTray,2DBarcode,MesaWidth,SpecificId,FibreType)")
                //    throw new MESCommsException("Bad LOTQUERYFULL response format");

                int numDUTs = int.Parse(responseData[4]);

                for (int i = 0; i < numDUTs; i++)
                {
                    string lineCSVtext = fileContentsLine[i];
                    char[] brackets = new char[] { '(', ')' };
                    string[] attrib = lineCSVtext.Trim(brackets).Split(new char[] { ',' });

                    DUTObject dut = new DUTObject();

                    dut.SerialNumber = attrib[4 + numDUTs];
                    dut.PartCode = batch.PartCode;
                    dut.TestStage = batch.Stage;
                    dut.BatchID = batchId;

                    if (!batch.Attributes.IsPresent("currentRule"))
                    {
                        batch.Attributes.AddString("currentRule", attrib[5 + numDUTs]);
                        batch.Attributes.AddString("currentStepStatus", attrib[6 + numDUTs]);
                        batch.Attributes.AddString("availablePath", attrib[7 + numDUTs]);
                    }

                    batch.Add(dut);
                }

                return batch;
            }
        }

        //track in
        public void LOTSTATE(string batchId, string userId)
        {
            // PRECONDITIONS - check here before request is generated
            CheckArg("batchId", batchId);
            CheckArg("userId", userId);
            bool waitForResponse = config.TrackInResponse.WaitForResponse;

            // 
            // EXAMPLE REQUEST AND RESPONSE
            // 
            // ,[MH100138.1],,,,MODTEST,,181,,,,LOTSTATE,,fwmes,
            // 
            // SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
            // 

            // Generate request and read response
            using (Request request = new Request(batchId, "", "", "", "", "LOTSTATE", "", RequestLevel.LOT, waitForResponse, userId))
            {
                if (waitForResponse)
                {
                    // Regular expression for success response e.g. SUCCESS,MH100138.1,MODTEST,MT25TABCC-C59,MH100138.001,
                    string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),$";

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackInResponse.Timeout_s);

                    // Only get here if response is success, otherwise Exception will have been thrown
                }
                else
                {
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
                }
            }
        }

        public void ATTRIB_SET(string batchId, string componentId, string attribName, string attribVal,string userId)
        {
            CheckArg("batchId", batchId);
            CheckArg("attribName", attribName);
            CheckArg("userId", userId);
            bool waitForResponse = config.SetAttributeResponse.WaitForResponse;

            // To Set and Read a Component level attribute:-
            // 
            // ,[CK100242.1]CK100242.001,,,,,,2164,,,,ATTRIB,,fwmes,PartId:PA013444:
            // 
            // 2003-11-13 13:30:04 SUCCESS: ATTRIB function complete for lot CK100242.1
            // 
            // 

            using (Request request = new Request(batchId, componentId, "", "", "", "ATTRIB", attribName + ":" + attribVal + ":", RequestLevel.COMPONENT, waitForResponse, userId))
            {

                if (waitForResponse)
                {
                    // Regular expression for success response
                    string successREtext = "SUCCESS: ATTRIB function complete for lot " + batchId;

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.SetAttributeResponse.Timeout_s);

                    // Only get here if response is success, otherwise Exception will have been thrown
                }
                else
                {
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
                    // Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
                }
            }
        }

        //trach out pass
        public void TRACKOUT(string batchId, string stage, string partCode, string userId )
        {
            //
            // EXAMPLE REQUEST AND RESPONSE
            // 
            // ,[MH100138.1],,MT25TABCC-C59,,MODTEST,,181,,,,TRACKOUT,,fwmes,
            // 
            // 2003-11-13 15:50:47 SUCCESS: TRACKOUT function complete for lot MH100138.1
            // 
            // Note:- The PartId is optional and does not change the lot attribute.
            // 

            // PRECONDITIONS - check here before request is generated
            CheckArg("batchId", batchId);
            CheckArg("stage", stage);
            CheckArg("userId", userId);
            //CheckArg( "partCode", partCode );
            bool waitForResponse = config.TrackOutBatchResponse.WaitForResponse;

            using (Request request = new Request(batchId, "", partCode, stage, "", "TRACKOUT", "", RequestLevel.LOT, waitForResponse, userId))
            {
                if (waitForResponse)
                {
                    // Regular expression for success response
                    string successREtext = "SUCCESS: TRACKOUT function complete for lot " + batchId;

                    string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutBatchResponse.Timeout_s);

                    // Only get here if response is success, otherwise Exception will have been thrown
                }
                else
                {
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
                    // Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
                }
            }
        }
        //rework
        public string REWORK(string batchId, string componentId, string partCode, string stage, string userId, string reworkCode )
        {
            // PRECONDITIONS - check here before request is generated
            CheckArg("batchId", batchId);
            CheckArg("reworkCode", reworkCode);
            CheckArg("userId", userId);
            bool waitForResponse = config.TrackOutComponentPassReworkOnHoldResponse.WaitForResponse;

            // ,[MH100138.2],,MT25TABCC-C59,,MODTEST,,181,,Delta,,REWORK,,fwmes,
            // 
            // 2003-11-17 13:38:53 SUCCESS: REWORK function complete for lot MH100138.2
            // ,[CK104492.1],,PA013444,,CFP2_TX_GBFinalTest,,2168,,Failed,,REWORK,,fwmes,
            // Note:- Sent lot down REWORK path and sets ReworkCode attribute to Delta.
            // 
            // or
            // 
            // ,[MH100138.1]MH100138.001,,,,,,181,,CapDamage,,REWORK,,fwmes,
            // 
            // 2003-11-17 13:42:23 SUCCESS: REWORK function complete for lot MH100138.2
            // 
            // Note:- Component MH100138.001 split from batch into new batch (MH100138.2).    New lot MH100138.2 sent down REWORK path and ReworkCode set to CapDamage.  PartId and FwStageName are optional.
            // 

            Request request = null;
            if (!string.IsNullOrEmpty(componentId))
                request = new Request(batchId, componentId, "", "", reworkCode, "REWORK", "", RequestLevel.COMPONENT, true, userId);
            else if (!string.IsNullOrEmpty(stage) && !string.IsNullOrEmpty(partCode))
                request = new Request(batchId, "", partCode, stage, reworkCode, "REWORK", "", RequestLevel.COMPONENT, true, userId);

            using (request)
            {
                if (waitForResponse)
                {
                    // Regular expression for success response e.g. SUCCESS: REWORK function complete for lot MH100138.2
                    string successREtext = "SUCCESS: REWORK function complete for lot .*";

                    string[] fileLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelWrite, successREtext, config.TrackOutComponentPassReworkOnHoldResponse.Timeout_s);

                    // Only get here if response is success, otherwise Exception will have been thrown

                    string[] spaceToken = fileLine[0].Split(new char[] { ' ' });
                    return spaceToken[6];
                }
                else
                {
                    FactoryWorks.DropRequest(request, FactoryWorks.LogLabelWrite, this.logger);
                    // Request will not be removed as deleteFileOnDisposal=waitForResponse(false) was set on construction
                    return "";
                }
            }
        }

        #endregion



        #region xiaojiang 2017.03.17
        public MESbatch LotQueryFullNew(string batchId,string userId)
        {
            CheckArg("batchId", batchId);
            CheckArg("userID", userId);

            // Generate request and read response
            using (Request request = new Request(batchId, "", "", "", "", "LOTQUERYFULLNEW", "", RequestLevel.COMPONENT, true,userId))
            {
                string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),";

                string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelRead, successREtext, config.LoadBatchResponse.Timeout_s);

                string[] responseData = fileContentsLine[0].Split(new char[] { ',' });

                MESbatch batch = new MESbatch();
                batch.Stage = responseData[2];
                batch.PartCode = responseData[4];
                batch.Attributes.AddString("lot_type", responseData[3]);
                batch.Attributes.AddString("current_rule", responseData[5]);
                batch.Attributes.AddString("current_step_status", responseData[6]);
                int numDUTs = fileContentsLine.Length - 2;

                //if (fileContentsLine[1] != "(CompId,FacetCoatId,WaferId,ChipId,position,PartId,OutputTray,2DBarcode,MesaWidth,SpecificId,FibreType)")
                //    throw new MESCommsException("Bad LOTQUERYFULL response format");

                for (int i = 0; i < numDUTs; i++)
                {
                    string lineCSVtext = fileContentsLine[i + 2];
                    char[] brackets = new char[] { '(', ')' };
                    string[] attrib = lineCSVtext.Trim(brackets).Split(new char[] { ',' });

                    DUTObject dut = new DUTObject();
                    if(attrib.Length>5)
                        if (!string.IsNullOrEmpty(attrib[5]) && !string.Equals(batch.PartCode, attrib[5], StringComparison.CurrentCultureIgnoreCase))
                            throw new MESCommsException("Unmached PartCode between Lot and component!");
                    try
                    {
                        dut.Attributes.AddString("lot_type", responseData[3]);
                        dut.PartCode = batch.PartCode;
                        dut.TestStage = batch.Stage;
                        dut.BatchID = batchId;
                        dut.SerialNumber = attrib[0];
                        dut.EquipmentID = attrib[0];

                        dut.Attributes.AddString("FacetCoatId", attrib[1]);
                        dut.Attributes.AddString("WaferId", attrib[2]);
                        dut.Attributes.AddString("ChipId", attrib[3]);

                        dut.TrayPosition = attrib[4];
                        dut.Attributes.AddString("OutputTray", attrib[6]);
                        dut.Attributes.AddString("2DBarcode", attrib[7]);
                        dut.Attributes.AddString("MesaWidth", attrib[8]);
                        dut.Attributes.AddString("SpecificId", attrib[9]);
                        dut.Attributes.AddString("FibreType", attrib[10]);

                    }
                    catch { }
                    batch.Add(dut);
                }

                return batch;
            }
        }

        //LOTSTATENEW
        public MESbatch LotStateNew(string batchId, string userId)
        {
            CheckArg("batchId", batchId);
            CheckArg("userId", userId);

            bool waitForResponse = true;// config.TrackInResponse.WaitForResponse;
            MESbatch batch = new MESbatch();

            // Generate request and read response
            using (Request request = new Request(batchId, "", "", "", "", "LOTSTATENEW", "", RequestLevel.COMPONENT, waitForResponse, userId))
            {
                string successREtext = "SUCCESS,([^,]*),([^,]*),([^,]*),(.*),$";
                string[] fileContentsLine = this.DropRequestAndWaitForResponse(request, FactoryWorks.LogLabelRead, successREtext, config.LoadBatchResponse.Timeout_s);

                string[] responseData = fileContentsLine[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                batch.Stage = responseData[2];
                batch.PartCode = responseData[3];
                batch.Attributes.AddString("lot_type", responseData[4]);

                int numDUTs = responseData.Length;

                for (int i = 5; i < numDUTs; i++)
                {
                    DUTObject dut = new DUTObject();

                    dut.SerialNumber = responseData[i];
                    dut.PartCode = batch.PartCode;
                    dut.TestStage = batch.Stage;
                    dut.BatchID = batchId;
                    dut.EquipmentID = dut.SerialNumber;
                    dut.Attributes.AddString("lot_type", responseData[4]);

                    batch.Add(dut);
                }
            }

            return batch;
        }

        #endregion




        #region Private low-level methods
        /// <summary>
		/// Drop a request file
		/// Dont wait for a response
		/// </summary>
		/// <param name="request">The request</param>
        /// <param name="operationLogLabel"> Log label indicating a FactoryWorks read or write operation</param>
        /// <param name="logger"> Reference to logger object</param>
        private static void DropRequest(Request request, string operationLogLabel, ILogging logger)
		{

			Response response = new Response();

			// Delete any existing response (message) file
			response.Remove();

			// Create new request file and move to drop folder
			request.CreateLocalFile();
			request.MoveToDropFolder();

            // Write log entry for request to FactoryWorks:
            logger.DatabaseTransactionWrite(operationLogLabel, FactoryWorks.LogLabelNoWaitRequest +
                                                 request.GetRequestCommand() + "  Dropped To File: " + request.DropFilePath);

		}

		/// <summary>
		/// Drop a request file and wait for a response
		/// </summary>
		/// <param name="request">The request</param>
        /// <param name="operationLogLabel"> Log label indicating a FactoryWorks read or write operation</param>
		/// <param name="successREtext">Regular expression matching format of response</param>
		/// <param name="timeout_s">Max time to wait for a response in seconds</param>
		/// <returns>The response</returns>
		private string[] DropRequestAndWaitForResponse( Request request, string operationLogLabel, string successREtext, int timeout_s )
		{

			Response response = new Response();

			// Delete any existing response (message) file
			response.Remove();

			// Create new request file and move to drop folder
			request.CreateLocalFile();
			request.MoveToDropFolder();

            // Write log entry for request to FactoryWorks:
            this.logger.DatabaseTransactionWrite(operationLogLabel, FactoryWorks.LogLabelRequest +
                                                 request.GetRequestCommand() + "  Dropped To File: " + request.DropFilePath);

			// Wait for response
			DateTime startTime;
            if (usingUtc)
            {
                startTime = DateTime.UtcNow;
            }
            else
            {
                startTime = DateTime.Now;
            }
			while( !response.IsReady() )
			{
				System.Threading.Thread.Sleep( 1000 );
				TimeSpan elapsedTime ;
                if (usingUtc)
                {
                    elapsedTime = DateTime.UtcNow.Subtract(startTime);
                }
                else
                {
                    elapsedTime = DateTime.Now.Subtract(startTime);
                }
				if( elapsedTime.Seconds > timeout_s )
					throw new MESTimeoutException();
			}


			// Read file contents and parse
			string[] contents = response.ReadFile();
			string[] contentsMinusTimeDate = new string[contents.Length];

            // Write log entry for FactoryWorks' response:
            this.logger.DatabaseTransactionWrite(operationLogLabel, FactoryWorks.LogLabelResponse
                                                + String.Concat(contents));


			Regex successRE = new Regex( this.responseTimeDateRE + ' ' + successREtext );
			Regex failureRE = new Regex( this.responseTimeDateRE + " FAILURE: (.*)$" );
            Regex successNoTimeDateRE = new Regex(successREtext);

			if( successRE.IsMatch(contents[0]) )
			{
				// Remove time-date from begining of each line
				Regex timeDateRE = new Regex( this.responseTimeDateRE + " .*" );
				for( int i = 0; i < contents.Length; i++ )
				{
					if( timeDateRE.IsMatch(contents[i]) == false )
						throw new MESCommsException( "Found reponse line that doesnt start with date-time" );
					
					contentsMinusTimeDate[i] = contents[i].Substring( 20 );
				}
			}
            else if (successNoTimeDateRE.IsMatch(contents[0]))
            {
                //Success response with no Time-Date.
                for (int i = 0; i < contents.Length; i++)
                {
                    contentsMinusTimeDate[i] = contents[i];
                }
            }
			else if( failureRE.IsMatch(contents[0]) )
			{
				// Extract failure reason and throw
				string[] line1 = contents[0].Split( new char[]{' '} );
				string failReason = string.Join( " ", line1, 3, line1.Length-3 );
				throw new MESInvalidOperationException( failReason );
			}
			else
			{
				// Should never get a response like this
				throw new MESCommsException( "Unknown format: " + contents[0] );
			}

			return contentsMinusTimeDate;
		}

		/// <summary>
		/// Check function argument value is neither null nor empty string
		/// </summary>
		/// <param name="name">Argument name</param>
		/// <param name="val">Argument value</param>
		private static void CheckArg( string name, string val )
		{
			if( val == null || val.Length == 0 ) throw new MESFatalErrorException( name + " arg is either null or empty" );
		}
		#endregion

		#region Private Data
		/// <summary>Config information for this class</summary>
		private FactoryWorksConfig config;
		/// <summary>Standard attributes as returned by LOTQUERYFULL</summary>
		private StringCollection standardAttributes;
		/// <summary>Regular Expression matching the format of all FactoryWorks responses</summary>
		private string responseTimeDateRE;

        /// <summary>
        /// Reference to component providing logging functionality.
        /// </summary>
        private ILogging logger;

        /// <summary>
        /// Log entry application label for MES data reads.
        /// </summary>
        private const string LogLabelRead = "MESDataRead";

        /// <summary>
        /// Log entry application label for MES data writes.
        /// </summary>
        private const string LogLabelWrite = "MESDataWrite";

        /// <summary>
        /// Log entry Request message header label.
        /// </summary>
        private const string LogLabelRequest = "REQUEST drop-file: ";

        /// <summary>
        /// Log entry Request message header label.
        /// </summary>
        private const string LogLabelNoWaitRequest = "REQUEST With No Wait drop-file: ";

        /// <summary>
        /// Log entry Response message header label.
        /// </summary>
        private const string LogLabelResponse = "RESPONSE: ";

        /// <summary>
        /// If true,use UTC Date Time,otherwise,use local date time.
        /// </summary>
        private static bool usingUtc = true;
        public static bool UsingUtc
        {
            get
            {
                return usingUtc;
            }
        }
		#endregion
	}
}
