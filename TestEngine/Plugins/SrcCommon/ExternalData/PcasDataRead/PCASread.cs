// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasDataRead\PcasRead.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Collections;				// ArrayList
using System.Data.OleDb;				// OleDbConnection
using System.Text;						// StringBuilder
using System.Globalization;				// CultureInfo
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.ExternalData;	// ILogging
using System.Reflection;				// typeof
using System.Xml;						// XML Reader
using System.IO;						// System I/O



namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	///  Worker class to handle all interaction with PCAS
	/// </summary>
	internal class PCASread
	{
		/// <summary>
		/// Create a PCASread object and open connection to the PCAS database.
		/// </summary>
		/// <param name="logger">A logging component to allow use of the TestEngine's logging features.</param>
		/// <param name="DataSource">The DataSource name. ( e.g. "PCAS" )</param>
		internal PCASread(ILogging logger, string DataSource)
		{
			this.logger = logger;

			if ( DataSource == null || DataSource.Length == 0 )
			{
				throw new BookhamExternalDataException("Empty or null data source");
			}
			Connect("Provider=MSDAORA; Data Source=" + DataSource + "; User ID=pcas; Password=test");
			
			SetArchivePath(DataSource);
		}

		/// <summary>
		/// Sets the location of the raw data archive path.
		/// </summary>
		/// <param name="DataSource">The data source name</param>
		private void SetArchivePath(string DataSource)
		{
			const string configDataFileName = "Configuration/PcasReadConfig.xml";
			
			// These built-in defaults may be overridden by config data if present.
			string location = "UK";
			string paiFiler = @"\\zpgty0x0\results\";
			string sznFiler = @"\\Szn-sfl-clst-01\Results\";

			XmlTextReader configData = new XmlTextReader(configDataFileName);

			try
			{
				// Read the file
				while (configData.Read())
				{
					// Start of element.
					if (configData.NodeType == XmlNodeType.Element)
					{
						if (configData.LocalName == "Location")
						{
							location = configData.ReadString();
						}
						else if (configData.LocalName == "ArchivePathPAI")
						{
							paiFiler = configData.ReadString();
						}						
						else if (configData.LocalName == "ArchivePathSZN")
						{
							sznFiler = configData.ReadString();
						}
					}
 				} // Finished reading

				// Set a default
				resultsArchive = paiFiler;

                if (location.ToUpper() == "UK" || location.ToUpper() == "PAI")
				{
                    if (DataSource.ToUpper() == "PCASC" || DataSource.ToUpper() == "PCAS_C")	// Location UK , SZN database
						resultsArchive = sznFiler;
				}
                else if (location == "China" || location.ToUpper() == "SZN")
				{
                    if (DataSource.ToUpper() == "PCAS" || DataSource.ToUpper() == "PCASC" || DataSource.ToUpper() == "PCAS_C")							// Location SZN , SZN database
							resultsArchive = sznFiler;
                        if (DataSource.ToUpper() == "PCASP" || DataSource.ToUpper() == "PCAS_P")	// Location SZN , UK database
							resultsArchive = paiFiler;
				} else
				{
					throw new BookhamExternalDataException("Unknown location '" + location + "' found in config file '" + System.IO.Path.GetFileName(configDataFileName)+"'");
				}
			}
			catch (UnauthorizedAccessException uae)
			{
				throw new BookhamExternalDataException("Error accessing config file '" + System.IO.Path.GetFileName(configDataFileName)+"'",uae);
			}
			catch (System.IO.FileNotFoundException fnfe)
			{
				throw new BookhamExternalDataException("Unable to locate config file '" + System.IO.Path.GetFileName(configDataFileName)+"'",fnfe);
			}			
			catch (System.IO.DirectoryNotFoundException dnfe)
			{
				throw new BookhamExternalDataException("Unable to locate configuration directory",dnfe);
			}
			catch (System.IO.IOException ioe)
			{
				throw new BookhamExternalDataException("IO error accessing config file '" + System.IO.Path.GetFileName(configDataFileName)+"'",ioe);
			}
			catch (System.Xml.XmlException xe)
			{
				throw new BookhamExternalDataException("XML error with config file '" + System.IO.Path.GetFileName(configDataFileName)+"'",xe);
			}
			finally
			{
				configData.Close();
			}
		}

		/// <summary>
		/// Connects to the data source
		/// </summary>
		/// <param name="ConnectionString">The database connection string</param>
		private void Connect(string ConnectionString)
		{
			dbConnection = new OleDbConnection(ConnectionString);
			logger.DatabaseTransactionWrite(LOG_LABEL,"Opening connection to PCAS");
			dbConnection.Open();
		}

		/// <summary>
		/// Disconnect from the data source
		/// </summary>
		internal void Disconnect()
		{
			logger.DatabaseTransactionWrite(LOG_LABEL,"Closing connection to PCAS");
			dbConnection.Close();		
		}


		/// <summary>
		/// Gets/Sets the value of the keyInfo structure.
		/// </summary>
		internal deviceInfo DeviceInfo
		{
			set { keyInfo = value ; }
			get { return keyInfo; }
		}


		/// <summary>
		/// Given a spec id, return the corresponding results table group
		/// </summary>
		/// <param name="spec">The spec_id e.g. inpmz_dc_152700_15</param>
		/// <returns>The results table name group e.g. a00100</returns>
		private string resultsTableForSpec(string spec)
		{
			StringBuilder sqlStmt  = new StringBuilder();
			sqlStmt.Append("select results_table_id from "+keyInfo.schema+".test_master");
			
			string whereClause = generateWhereClauseFromKeys( "" , keyInfo.testStage , keyInfo.deviceType, spec );
			if ( whereClause.Length  > 0 )
			{
				sqlStmt.Append(" where " + whereClause);
			}
			

			OleDbCommand myCommand = new OleDbCommand(sqlStmt.ToString(),dbConnection);
			OleDbDataReader myReader;
			
			logger.DeveloperWrite(LOG_LABEL,sqlStmt.ToString());
			myReader = myCommand.ExecuteReader();

			string resultsTable;

			try
			{
				if( myReader.Read() == false)
				{
					myReader.Close();
					logger.DeveloperWrite(LOG_LABEL,"getResultsTableForSpec - no data found for spec '"+spec+"'" );
					return null;
				}
				
				// Ensure that there is only one item, by attempting to read again.
				resultsTable = myReader.GetString(0);
				if( myReader.Read() != false)
				{
					myReader.Close();
					logger.ErrorInExternalDataPlugin("getResultsTableForSpec - multiple linked tables found for spec '"+spec+"'. Use a more specific query." );
					return null;
				}
			}
			finally
			{
				myReader.Close();
			}

			return resultsTable;
		}



		/// <summary>
		/// For a given results table group (eg a00100),
		/// return all results tables (eg a00100_0, a00100_1)
		/// </summary>
		/// <param name="resultsTableGroup">The results table group e.g. a00100</param>
		/// <returns>A list of results tables e.g. a00100_0,a00100_1</returns>
		private ArrayList resultsTablesForGroup( string resultsTableGroup )
		{
			if ( resultsTableGroup == null || resultsTableGroup.Length == 0 )
			{
				// Should not happen.
				logger.ErrorInExternalDataPlugin("Warning ! - attempting to retrieve entire list of tables in " + keyInfo.schema);
				return null;
			}

			// The PCAS tmp_cols table has results table groups in uppercase - everwhere else is lowercase
			string resultsTableGroupUPPERCASE = resultsTableGroup.ToUpper(CultureInfo.InvariantCulture);


			string sqlStmt = "select unique table_name from "+keyInfo.schema+".tmp_cols where table_name like '" + resultsTableGroupUPPERCASE + "_%'";

			OleDbCommand myCommand = new OleDbCommand(sqlStmt,dbConnection);
			OleDbDataReader myReader = null;

			logger.DeveloperWrite(LOG_LABEL,sqlStmt);
			ArrayList tableNameList = new ArrayList();
			
			myReader = myCommand.ExecuteReader();
			
			try
			{
				while( myReader.Read() )
				{
					string tableNameAsString = myReader.GetString(0);
					tableNameList.Add( tableNameAsString );
				}
			}
			finally
			{
				myReader.Close();
			}

			return tableNameList;
		}

		/// <summary>
		/// Generates a sql WHERE clause using information provided in the four pass parameters, any of which may be null or zero length.
		/// </summary>
		/// <param name="serialNo">The PCAS 'serial_no'</param>
		/// <param name="testStage">The PCAS 'test_stage'</param>
		/// <param name="deviceType">The PCAS 'device_type'</param>
		/// <param name="specification">The PCAS 'spec_id'</param>
		/// <returns></returns>
		internal static string generateWhereClauseFromKeys(string serialNo , string testStage , string deviceType, string specification )
		{
			StringBuilder whereClause = new StringBuilder();

			if ( serialNo != null && serialNo.Length > 0 )
			{
				whereClause.Append(" serial_no='"+serialNo+"'");
			}

			if ( testStage != null && testStage.Length > 0 )
			{
				if ( whereClause.Length > 0 ) whereClause.Append(" and");
				whereClause.Append(" test_stage='"+testStage+"'");
			}

			if ( deviceType != null && deviceType.Length > 0 )
			{
				if ( whereClause.Length > 0 ) whereClause.Append(" and");
				whereClause.Append(" device_type='"+deviceType+"'");
			}			
			
			if ( specification != null && specification.Length > 0 )
			{
				if ( whereClause.Length > 0 ) whereClause.Append(" and");
				whereClause.Append(" spec_id='"+specification+"'");
			}

			return whereClause.ToString();
		}

		/// <summary>
		/// Finds the group of tables containing the most recent data. 
		/// </summary>
		/// <param name="serialNo">The PCAS 'serial_no'</param>
		/// <param name="testStage">The PCAS 'test_stage'</param>
		/// <param name="deviceType">The PCAS 'device_type'</param>
		/// <param name="specification">The PCAS 'spec_id'</param>
		/// <returns>The table names are without a "_0", "_1" etc. suffix ( e.g. a0012 )</returns>
		private string latestTableGroup ( string serialNo , string testStage , string deviceType, string specification )
		{			
			string latestGroup = "";
			ArrayList keyList = tableGroupList ( serialNo, testStage, deviceType, specification );
			if ( keyList.Count > 0 )
			{
				latestGroup =  keyList[0].ToString();
			}
			return latestGroup;
		}

		/// <summary>
		/// Returns a list of table groups  that relate to the key information provided.
		/// </summary>
		/// <param name="serialNo">The PCAS 'serial_no'</param>
		/// <param name="testStage">The PCAS 'test_stage'</param>
		/// <param name="deviceType">The PCAS 'device_type'</param>
		/// <param name="specification">The PCAS 'spec_id'</param>
		/// <returns>A collection of table names ( without suffixes ) e.g ( a0123, a0152 )</returns>
		private ArrayList tableGroupList ( string serialNo , string testStage , string deviceType, string specification )
		{

			StringBuilder sqlStmt = new StringBuilder();
			
			// Note. Can't use SELECT DISTINCT and ORDER BY in the same clause. Oracle errors: ( ORA-01791: not a SELECTed expression )
			sqlStmt.Append("select RESULTS_TABLE_ID from "+keyInfo.schema+".test_link");

			string whereClause = generateWhereClauseFromKeys( serialNo , testStage , deviceType, specification );
			if ( whereClause.Length > 0 )
			{
				sqlStmt.Append(" where ");
				sqlStmt.Append(whereClause);
			}
			sqlStmt.Append(" ORDER BY TIME_DATE DESC");

			OleDbCommand myCommand = new OleDbCommand(sqlStmt.ToString(),dbConnection);
			OleDbDataReader myReader;

			logger.DeveloperWrite(LOG_LABEL,sqlStmt.ToString());
			myReader = myCommand.ExecuteReader();

			ArrayList tableList = new ArrayList();
			
			try
			{
				while ( myReader.Read() )
				{
					string tableGroup = myReader.GetString(0);
					if ( ! tableList.Contains(tableGroup) )
					{
						logger.DebugOut(LOG_LABEL,"PcasRead : Adding table " + tableGroup);
						tableList.Add(tableGroup);
					}
				}
			}
			finally
			{
				myReader.Close();
			}
			
			return tableList;
		}

		/// <summary>
		/// Retrieve all results from a single table group.
		/// </summary>
		/// <param name="sqlStmt">An SQL statement</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		/// <returns>An array of datumLists, one item per record found in the table group.</returns>
		private DatumList[] getAllResultsFromSingleTableGroup( string sqlStmt, bool retrieveRawData )
			{
			OleDbCommand myCommand = new OleDbCommand(sqlStmt,dbConnection);
			OleDbDataReader myReader;
			
			logger.DeveloperWrite(LOG_LABEL,sqlStmt);
			myReader = myCommand.ExecuteReader();
			
			ArrayList allData = new ArrayList();
			DatumList resultsData;
			
			try
			{
				while ( myReader.Read() )
				{
					resultsData = new DatumList();
					createDatumListFromSingleTable( resultsData , myReader, retrieveRawData );
		
					// single read completed.
					allData.Add(resultsData);
				}
			}
			finally
			{
				myReader.Close();
			}

			DatumList[] fullDatumList = (DatumList[])allData.ToArray(typeof(DatumList));

			return fullDatumList;
		}


		/// <summary>
		/// Retrieve a single set of results from a related group of one or more tables ( e.g. a0012_0 a0012_1 etc )
		/// </summary>
		/// <param name="sqlStmt">A SQL statement</param>
		/// <param name="resultsData">The datumList to update</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		private void getResultsFromSingleTableGroup( string sqlStmt, DatumList resultsData, bool retrieveRawData )
			{
			OleDbCommand myCommand = new OleDbCommand(sqlStmt,dbConnection);
			OleDbDataReader myReader;

			logger.DeveloperWrite(LOG_LABEL,sqlStmt);
			myReader = myCommand.ExecuteReader();
			
			try
			{
				if( myReader.Read() == false )
				{
					myReader.Close();
					logger.DeveloperWrite(LOG_LABEL,"getResultsFromSingleTableGroup - no data found" );
					return;
				}

				createDatumListFromSingleTable( resultsData , myReader, retrieveRawData );
			}
			finally
			{
				myReader.Close();
			}
		}

		/// <summary>
		/// Joins data found in one or more related tables into a single datumList.
		/// </summary>
		/// <param name="resultsData">The datumList to update.</param>
		/// <param name="myReader">An existing instance of a DataReader</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
        private void createDatumListFromSingleTable(DatumList resultsData, OleDbDataReader myReader, bool retrieveRawData)
		{

			string nodeNumber = "";
			bool updateFilePaths = false;

			// Deal with each field
			for( int icol = 0; icol < myReader.FieldCount; icol++ )
			{
				string nameAsString = myReader.GetName(icol);
				Type t = myReader.GetFieldType(icol);
				
				string valAsString = "";
				double valAsDouble = 0;
				Int32 valAsInteger = 0;
				bool valAsBool = false;

				
				// using TYPE create an appropriate Datum. Retrieve data as correct type. Add to DatumList.
				switch ( t.ToString() )
				{
					case "System.String":
						if ( ! myReader.IsDBNull(icol) )	// ignore null strings
						{
							valAsString = myReader.GetString(icol);
							// Desc/val lists may be already partially filled with _0 results in which case SPEC_ID , SERIAL_NO & TIME_DATE are already present
						
							if( nameAsString == "SERIAL_NO" )	// Check whether the field name is "SERIAL_NO"
							{
								// See if it already exists
								if ( resultsData.IsPresent("SERIAL_NO") )
								{
									string serialNo = resultsData.ReadString("SERIAL_NO");
									// Check that it matches the existing value
									if( valAsString != serialNo )
									{
										myReader.Close();
										logger.ErrorInExternalDataPlugin("Attempted to append results from a different SERIAL_NO" );
									}
									// Don't store it again.
									break;
								}

							}

							if( nameAsString == "TIME_DATE" )	// Check whether the field name is "TIME_DATE"
							{
								// See if it already exixts
								if ( resultsData.IsPresent("TIME_DATE") )
								{
									string timeDate = resultsData.ReadString("TIME_DATE");
									// Check that it matches the existing value
									if( valAsString != timeDate )
									{
										myReader.Close();
										logger.ErrorInExternalDataPlugin("Attempted to append results from a different TIME_DATE" );
									}
									// Don't store it again.
									break;
								}
							}

							if( nameAsString == "SPEC_ID" )
							{
								// See if it already exixts
								if ( resultsData.IsPresent("SPEC_ID") )
								{
									string specId = resultsData.ReadString("SPEC_ID");
									// Check that it matches the existing value
									if( valAsString != specId )
									{
										myReader.Close();
										logger.ErrorInExternalDataPlugin("Attempted to append results from a different SPEC_ID" );
									}
									// Don't store it again.
									break;
								}
							}

							// deal with *plot* , *image* , raw_* etc
                            if ((nameAsString.StartsWith("FILE_") 
                                && (!nameAsString.EndsWith("_TIMESTAMP")))  ||
                                nameAsString.EndsWith("_FILE")              ||
                                (nameAsString.Contains("_FILE_")
                                && (!nameAsString.EndsWith("_TIMESTAMP")))  ||
                                nameAsString.StartsWith("PLOT_")            ||
                                nameAsString.EndsWith("_PLOT")              ||
                                nameAsString.Contains("_PLOT_")             || 
                                nameAsString.StartsWith("IMAGE_")           ||
                                nameAsString.EndsWith("_IMAGE")             ||
                                nameAsString.Contains("_IMAGE_")            ||
                                (nameAsString.StartsWith("MATRIX_") &&
                                (!nameAsString.Equals ("MATRIX_NUMBER")))   ||
                                nameAsString.EndsWith("_MATRIX")            ||
                                nameAsString.Contains("_MATRIX_")           ||
                                nameAsString.StartsWith("RAW_")             ||
                                nameAsString.EndsWith("_RAW")               ||
                                nameAsString.Contains("_RAW_"))   
							{
                                //Only capture Blob Data results if required.
                                if (retrieveRawData)
                                {
                                    // default the file directory path to "\\unknownServerLocation", just so that the
                                    // UpdateFilePaths function can work properly (abuse of DatumFileLink!)
                                    string filePath = @"\\unknownServer\Location";
                                    int lastSlash = valAsString.LastIndexOf(System.IO.Path.DirectorySeparatorChar);
                                    if (lastSlash > -1)
                                    {
                                        filePath = valAsString.Substring(0, lastSlash - 1); // The path is not expected to have a trailing slash
                                        valAsString = valAsString.Substring(lastSlash, valAsString.Length - lastSlash);
                                    }
                                    else
                                    {
                                        // The full file path was not stored. 
                                        // Using our skill and judgement we will need to create this.
                                        updateFilePaths = true;
                                    }

                                    DatumFileLink newDatum = new DatumFileLink(nameAsString, filePath, valAsString);
                                    resultsData.Add(newDatum);
                                }
							}
							else
							{
								// This is not a 'special' value. Add it to our list. We may need NODE later on in the code, so capture this value.
								DatumString stringDatum = new DatumString(nameAsString, valAsString);
								resultsData.Add(stringDatum);
								if ( nameAsString == "NODE" )
								{
									// Capture NODE
									nodeNumber = valAsString;
								}
							}
						}			// ! myReader.IsDBNull(icol)
						break;

					case "System.Boolean":
						if ( ! myReader.IsDBNull(icol) )
						{
							valAsBool = myReader.GetBoolean(icol);
							DatumBool boolDatum = new DatumBool(nameAsString, valAsBool);
							resultsData.Add(boolDatum);
						}
						break;

					case "System.Double":
						if ( ! myReader.IsDBNull(icol) )
						{
							valAsDouble = myReader.GetDouble(icol);
							DatumDouble dblDatum = new DatumDouble(nameAsString, valAsDouble);
							resultsData.Add(dblDatum);
							if ( nameAsString == "NODE" )
							{
								// Capture NODE
								nodeNumber = valAsDouble.ToString(CultureInfo.InvariantCulture);
							}
						}
						break;

					case "System.Decimal":
						if ( ! myReader.IsDBNull(icol) )
						{
							valAsInteger = checked((int)myReader.GetDecimal(icol));

                            if ((nameAsString.IndexOf("BOOL_") == 0) && ((valAsInteger == 0) || (valAsInteger == 1)))
                            {
                                //Then this is apseudo boolean parameter (supported by Test Engine based soltuions only).
                                DatumBool boolDatum = new DatumBool(nameAsString, Convert.ToBoolean(valAsInteger));
                                resultsData.Add(boolDatum);
                            }
                            else
                            {
                                DatumSint32 decDatum = new DatumSint32(nameAsString, valAsInteger);
                                resultsData.Add(decDatum);
                            }
							
							if ( nameAsString == "NODE" )
							{
								// Capture NODE
								nodeNumber = valAsInteger.ToString(CultureInfo.InvariantCulture);
							}
						}
						break;

					default:
						logger.ErrorInExternalDataPlugin("Unhandled data type from PCAS : " + t.ToString());
						break;
				}
			
			} // icol

			if ( updateFilePaths )
			{
				// The full path to the file is not always stored in PCAS.
				// We need to check each FileLink datum and create path information if it is missing.
				UpdateFilePaths(nodeNumber, resultsData);
			}
		}

		/// <summary>
		/// Ensures that DatumFileLinks are populated with the full path to the raw data files.
		/// </summary>
		/// <param name="nodeNumber">PCAS node number</param>
		/// <param name="resultsData">The results data to update</param>
		private void UpdateFilePaths(string nodeNumber, DatumList resultsData)
		{
			// If the path to a raw data file was not stored, we will need to create one.          
			if ( nodeNumber.Length == 0 )
			{
				// NODE cannot be found. This is a fatal error.				
				logger.ErrorInExternalDataPlugin("PCAS Read Plug-in Fatal Error: NODE is not stored as part of test results. Not attempting to update DatumFileLink paths");
                return;
		    }
                     
            if ( nodeNumber.IndexOf("NODE") == -1 )
			{
				// Prepend "NODE" if not already there
				nodeNumber = "NODE" + nodeNumber;
			}

            string pcasResultsDate = resultsData.ReadString("TIME_DATE").Substring(0, 8);

            //Find the path on the server to the blob data.
            bool foundBlobDataFile = false;
            
            string fullResultsPath;
            string directoryResultsPath;

            foreach (Datum resultDatum in resultsData)
            {
                // Update the path for each DatumFileLink
                if (resultDatum.Type.ToString() == "FileLinkType")
                {
                    DatumFileLink dfl = (DatumFileLink)resultDatum;
                    // Don't update entries that already have a known full path.
                    // I.e. don't have our "special" directory path default
                    if (dfl.GetDirectoryPath() == @"\\unknownServer\Location")
                    {
                        foundBlobDataFile = false;
                        //For Test Engine Release 1, only the following standard format path is supported: \nodeXXX\YYYYMMDD\
                        directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, pcasResultsDate);
                        fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());
                        
                        if (File.Exists(fullResultsPath))
                        {
                            foundBlobDataFile = true;
                        }
                        else
                        {
                            //The Blob file might have been stored the next day.
                            directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, getNextDay(pcasResultsDate));
                            fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());
                            
                            if (File.Exists(fullResultsPath))
                            {
                                foundBlobDataFile = true;
                            }
                        }

                        if (!foundBlobDataFile)
                        {
                            //The Blob Data wasn't stored the same day the PCAS results were stored, or the day after.
                            //Try searching back up to 8 days.
                            string tryDate = pcasResultsDate;
                            for (int i = 0; i < 8; i++)
                            {
                                //Try the previous day to the one just tried
                                tryDate = getPreviousDay(tryDate);

                                //Create the path
                                directoryResultsPath = Path.Combine(resultsArchive + nodeNumber, tryDate);
                                fullResultsPath = Path.Combine(directoryResultsPath, dfl.GetFileName());

                                if (File.Exists(fullResultsPath))
                                {
                                    //Exit here if we've found the Blob File.
                                    foundBlobDataFile = true;
                                    break;
                                }
                            }
                        }

                        if (!foundBlobDataFile)
                        {
                            //Can't find where the Blob data is stored!!! Write a warning log, so that this is captured.
                            //Put a dummy directory path. If the test program needs this data,  then the program will 
                            //Throw an exception when it triers to access the non-existant file.
                            string errorLogMessage = "PCAS Read Plug-in Error: Unable to find Blob Data File " + dfl.GetFileName() + " for NODE " + nodeNumber + ". Cannot update DatumFileLink " + dfl.Name + " path";
                            logger.ErrorWrite(errorLogMessage);
                        }
                        else
                        {
                            dfl.Update(directoryResultsPath, dfl.GetFileName());
                        }
                    }
                }
            }
		}

        /// <summary>
        /// Given a date in the format YYYYMMDD, return the next day in the same format
        /// </summary>
        /// <param name="date">The date in format YYYYMMDD</param>
        /// <returns>The next day in format YYYYMMDD </returns>
        private string getNextDay(string date)
        {
            //IFormatProvider culture = new CultureInfo("en-GB", true);
            //DateTime today = DateTime.Parse(date.Substring(6, 2) + "/" +
            //                                date.Substring(4, 2) + "/" +
            //                                date.Substring(0, 4), culture, DateTimeStyles.None);
            int day = int.Parse(date.Substring(6, 2));
            int month = int.Parse(date.Substring(4, 2));
            int year = int.Parse(date.Substring(0, 4));

            DateTime today = new DateTime(year, month, day);


            DateTime nextDay = today.AddDays(1);

            return (nextDay.Year.ToString() +
                    convertToStringWithAtLeastTwoDigits(nextDay.Month) +
                    convertToStringWithAtLeastTwoDigits(nextDay.Day));
        }

        /// <summary>
        /// Given a date in the format YYYYMMDD, return the previous day in the same format
        /// </summary>
        /// <param name="date">The date in format YYYYMMDD</param>
        /// <returns>The previous day in format YYYYMMDD</returns>
        private string getPreviousDay(string date)
        {
            //IFormatProvider culture = new CultureInfo("en-GB", true);
            //DateTime today = DateTime.Parse(date.Substring(6, 2) + "/" +
                                            //date.Substring(4, 2) + "/" +
                                            //date.Substring(0, 4), culture, DateTimeStyles.None);

            int day = int.Parse(date.Substring(6, 2));
            int month = int.Parse(date.Substring(4, 2));
            int year = int.Parse(date.Substring(0, 4));

            DateTime today = new DateTime(year, month, day);


            DateTime previousDay = today.AddDays(-1);

            return (previousDay.Year.ToString() + 
                    convertToStringWithAtLeastTwoDigits(previousDay.Month) +
                    convertToStringWithAtLeastTwoDigits(previousDay.Day));
        }

        /// <summary>
        /// Convert an integer to a string. Result must have at least two digits.
        /// </summary>
        /// <param name="number">The number</param>
        /// <returns>String representation</returns>
        private string convertToStringWithAtLeastTwoDigits(int number)
        {
            if (number < 10)
            {
                return ("0" + number.ToString());
            }
            else
            {
                return (number.ToString());
            }
        }
		
		/// <summary>
		/// Retrieve a number of sets of results from a set of related results tables.
		/// </summary>
		/// <param name="serialNo">The PCAS serial number.</param>
		/// <param name="resultsTableGroup">The PCAS results table group name ( e.g. a00123 )</param>
		/// <param name="whereClause">The SQL where clause.</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		/// <returns>An array of datumLists, one item per set of results.</returns>
        private DatumList[] getMultipleResultsFromTableGroup(string serialNo, string resultsTableGroup, string whereClause, bool retrieveRawData)
		{
			ArrayList tableList = resultsTablesForGroup(resultsTableGroup);
			DatumList[] datumList = null;

			StringBuilder sqlSelectStmt = new StringBuilder();
			StringBuilder sqlWhereStmt = new StringBuilder();

			sqlSelectStmt.Append("select * from ");
			sqlWhereStmt.Append( " where alias_0.SERIAL_NO='" + serialNo + "'");

			for ( int i = 0 ; i < tableList.Count ; i++ )
			{
				string fullResultsTable = keyInfo.schema + "." + tableList[i];
				string alias = " alias_" + i.ToString(CultureInfo.InvariantCulture);
				
				// Build SQL
				if ( i > 0 )
				{
					sqlSelectStmt.Append(" , ");
					sqlWhereStmt.Append(" and " + alias + ".serial_no='"+serialNo+"'");
					sqlWhereStmt.Append(" and " + alias + ".time_date=alias_0.time_date");
				}
				sqlSelectStmt.Append( fullResultsTable + " " + alias );
			}
			
			string sqlStmt = sqlSelectStmt.ToString() + sqlWhereStmt.ToString() + " and alias_1." + whereClause;
			datumList = getAllResultsFromSingleTableGroup( sqlStmt, retrieveRawData );

			return datumList;
		}

		/// <summary>
		/// Retrieves the latest results from a set of related tables.
		/// </summary>
		/// <param name="serialNo">The PCAS serial number</param>
		/// <param name="resultsTableGroup">The PCAS results table group ( e.g. a0152 )</param>
		/// <param name="WhereClause">The SQL where clause.</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
        /// <returns>A single set of results.</returns>
        private DatumList getLatestResultsFromTableGroups(string serialNo, string resultsTableGroup, string WhereClause, bool retrieveRawData)
		{
			ArrayList tableList = resultsTablesForGroup(resultsTableGroup);
			DatumList datumList = new DatumList();

			for ( int i = 0 ; i < tableList.Count ; i++ )
			{
				string fullResultsTable = keyInfo.schema + "." + tableList[i];

				string sqlStmt	=	"select * from "+fullResultsTable +
					" where SERIAL_NO='" + serialNo + "' and " + WhereClause;

				getResultsFromSingleTableGroup( sqlStmt, datumList, retrieveRawData );
			}

			return datumList;
		}

		/// <summary>
		/// Retrieves the latest results from a set of related tables.
		/// </summary>
		/// <param name="serialNo">The PCAS serial number</param>
		/// <param name="resultsTableGroup">The PCAS results table group ( e.g. a0152 )</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
        /// <returns>A single set of results.</returns>
        private DatumList getLatestResultsFromTableGroups(string serialNo, string resultsTableGroup, bool retrieveRawData)
		{
			ArrayList tableList = resultsTablesForGroup(resultsTableGroup);
			DatumList datumList = new DatumList();

			for ( int i = 0 ; i < tableList.Count ; i++ )
			{
				string fullResultsTable = keyInfo.schema + "." + tableList[i];

                //string sqlStmt	=	"select * from "+fullResultsTable+" alias" +
                //    " where SERIAL_NO='" + serialNo + "'" +
                //    " and time_date = (select max(time_date) from "+fullResultsTable+" where alias.serial_no=serial_no)";

                string sqlStmt = "select * from " + fullResultsTable +
                       " where SERIAL_NO='" + serialNo + "'" +
                    " and time_date = (select max(time_date) from " + fullResultsTable + " where serial_no='" + serialNo + "')";

				getResultsFromSingleTableGroup( sqlStmt, datumList, retrieveRawData );
			}

			return datumList;
		}

		/// <summary>
		/// This function selects results from the ONE TABLE specified by the spec argument.
		/// Since multiple results can exist for a single device, the user must specify a where clause 
		/// </summary>
		/// <param name="serialNo">The device serial number e.g. CL12345.001</param>
		/// <param name="spec">The spec_id e.g. inpmz_dc_152700_15</param>
		/// <param name="whereClause">A Where clause. Not starting with the word 'WHERE'</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
        /// <returns>A single set of results.</returns>
        internal DatumList getResultsForDevice(string serialNo, string spec, string whereClause, bool retrieveRawData)
			{
			// PRECONDITIONS
			if( whereClause == null || whereClause.Length == 0 )
			{
				logger.ErrorInExternalDataPlugin("ERROR in getResultsForDevice - you must supply a whereClause" );
			}

			// Find one and only table for spec
			string resultsTableGroup = resultsTableForSpec(spec);

			if ( resultsTableGroup == null || resultsTableGroup.Length == 0 )
			{
				logger.DeveloperWrite(LOG_LABEL,"No results tables found for spec '" + spec + "'");
				return null;
			}

			// Get the results from that table
			DatumList datumList = getLatestResultsFromTableGroups( serialNo, resultsTableGroup, whereClause, retrieveRawData);

			return datumList;
		}


		/// <summary>
		/// Retrieves a set of results for a single device.
		/// </summary>
		/// <param name="serialNo">The PCAS serial number.</param>
		/// <param name="deviceType">The PCAS device type</param>
		/// <param name="testStage">The PCAS test stage.</param>
		/// <param name="whereClause">The SQL where clause.</param>
        /// /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		/// <returns>A single set of results.</returns>
        internal DatumList getResultsForDevice(string serialNo, string deviceType, string testStage, string whereClause, bool retrieveRawData)
		{
			if( whereClause == null || whereClause.Length == 0 )
			{
				logger.ErrorInExternalDataPlugin("ERROR in getResultsForDevice - you must supply a whereClause" );
			}
			
			// get a list of tables that match the keys.
			ArrayList tableList = tableGroupList(serialNo, testStage, deviceType, "");
			// throw if no data (null)
			if ( tableList == null )
			{
				logger.ErrorInExternalDataPlugin("No results found");
			}

			DatumList datumList = null;

			// get date of last record matching the SQL
			for ( int i=0 ; i < tableList.Count ; i++ )
			{
				// Search each table in reverse chronological order for a matching record.
				// Stop when we find one.
					logger.DebugOut(LOG_LABEL, "PcasRead : Searching table " + tableList[i].ToString());
					datumList = getLatestResultsFromTableGroups(serialNo, tableList[i].ToString(), whereClause, retrieveRawData );
					if ( datumList.Count > 0  )
						break;
			}
			return datumList;
		}

		/// <summary>
		/// Retrieves a number of results entries for a device.
		/// </summary>
		/// <param name="serialNo">The PCAS serial number.</param>
		/// <param name="deviceType">The PCAS device type</param>
		/// <param name="testStage">The PCAS test stage.</param>
		/// <param name="whereClause">The SQL where clause.</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		/// <returns>An array of results datumLists for the device.</returns>
        internal DatumList[] getMultipleResultsForDevice(string serialNo, string deviceType, string testStage, string whereClause, bool retrieveRawData)
		{
			if( whereClause == null || whereClause.Length == 0 )
			{
				logger.ErrorInExternalDataPlugin("ERROR in getResultsForDevice - you must supply a whereClause" );
			}
			
			// get a list of tables that match the keys.
			ArrayList tableList = tableGroupList(serialNo, testStage, deviceType, "");
			
			// Bale out if no data
			if ( tableList == null || tableList.Count == 0 )
			{
				logger.DeveloperWrite("LOG_LABEL","No data found");
				return null;
			}

			ArrayList datumArray = new ArrayList();

			for ( int i=0 ; i < tableList.Count ; i++ )
			{
				// Add everything that we find.
				logger.DebugOut(LOG_LABEL,"PcasRead : Searching table " + tableList[i].ToString());
				DatumList[] SingleTableDatumLists = getMultipleResultsFromTableGroup(serialNo, tableList[i].ToString(), whereClause, retrieveRawData );
				
				// add each DatumList to our output collection
				for ( int j = 0 ; j < SingleTableDatumLists.Length ; j++ )
				{
					datumArray.Add(SingleTableDatumLists[j]);
				}
			}

			DatumList[] datumCollection = (DatumList[])datumArray.ToArray( typeof( DatumList ) );

			return datumCollection;
		}


		/// <summary>
		/// Retrieves a number of results entries for a device.
		/// </summary>
		/// <param name="serialNo">The PCAS serial number.</param>
		/// <param name="specification">The PCAS spec_id</param>
		/// <param name="whereClause">The SQL where clause.</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		/// <returns>An array of results datumLists for the device.</returns>
        internal DatumList[] getMultipleResultsForDevice(string serialNo, string specification, string whereClause, bool retrieveRawData)
			{
			if( whereClause == null || whereClause.Length == 0 )
			{
				logger.ErrorInExternalDataPlugin("getResultsForDevice - you must supply a whereClause" );
			}
			
			// get table groups for our spec
			string tableGroup = resultsTableForSpec(specification);

			// Quit if no data (null)
			if ( tableGroup == null )
			{
				logger.DeveloperWrite(LOG_LABEL,"No results found");
				return null;
			}
					
			DatumList[] datumList = getMultipleResultsFromTableGroup(serialNo, tableGroup, whereClause, retrieveRawData );

			return datumList;
		}



		
		/// <summary>
		///  This is the method used if no WHERE clause is specified.
		/// </summary>
		/// <param name="serialNo">The device's SERIAL_NO</param>
		/// <param name="testStage">The device's TEST_STAGE</param>
		/// <param name="deviceType">The device's DEVICE_TYPE</param>
		/// <param name="specification">The device's SPECIFICATION</param>
        /// <param name="retrieveRawData">True if Blob Data is to be retrieved.</param>
		/// <returns>Datum list containing all previous results data</returns>
        internal DatumList getLatestResultsForDevice(string serialNo, string testStage, string deviceType, string specification, bool retrieveRawData)
		{
			// Find table with latest results in it
			string tableGroup = latestTableGroup(serialNo, testStage , deviceType, specification);
			if ( tableGroup.Length == 0 )
			{
				logger.DeveloperWrite(LOG_LABEL,"getLatestResultsForDevice - no data found for device ");
				return null;
			}
			// Get all data from that table
			DatumList datumList = getLatestResultsFromTableGroups( serialNo, tableGroup, retrieveRawData );

			return datumList;
		}

		/// <summary>
		/// Reference to logging component.
		/// </summary>
		internal ILogging logger;

		//
		// Private Data
		//
		/// <summary>
		/// Database connection
		/// </summary>
		OleDbConnection dbConnection; 
		/// <summary>
		/// Location of results archive
		/// </summary>
		private string resultsArchive = "";
		/// <summary>
		/// Label used by logging component
		/// </summary>
		const string LOG_LABEL = "ExtData_PcasRead_debug";
		/// <summary>
		/// Key data structure
		/// </summary>
		private deviceInfo keyInfo;
	}


}
