﻿using System.Collections.Generic;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using System.Collections.Specialized;
using Bookham.TestEngine.PluginInterfaces.TestControl;

namespace SPC.Rule
{
    public class PcasHelper
    {
        public const string SCHEMA = "hiberdb";
        public const string COC_SCHEMA = "coc";

        private IDataRead dataReaderPcas = null;

        public PcasHelper(ITestEngine engine)
        {
            try
            {
                dataReaderPcas = engine.GetDataReader(); //Shenzhen
            }
            catch
            {
                dataReaderPcas = null;
            }
        }

       
        #region Public Tools
        public List<ElementCollection> ObtainData(List<DatumList> previousData, List<string> searchParams)
        {
            if (!(previousData != null && previousData.Count > 0)) return null;

            List<ElementCollection> returnData = new List<ElementCollection>();
            previousData.ForEach(delegate(DatumList data)
            {
                ElementCollection elements = new ElementCollection();
                bool add = true;

                searchParams.ForEach(delegate(string param)
                {
                    if (data.IsPresent(param)) elements.Add(new Element(param, data.ReadDouble(param)));
                    else add = false;
                });


                if (add)
                {
                    elements.TimeDate = data.ReadString("TIME_DATE");
                    returnData.Add(elements);
                }
            });

            returnData.Sort();
            return returnData;
        }

        public List<ElementCollection> ObtainData(Identifier keys, string[] searchParams, bool cocData = false)
        {
            DatumList[] previousData = GetFromPcas(keys, searchParams, cocData);

            if (previousData == null) return null;
            if (previousData.Length == 0) return null;

            return ObtainData(new List<DatumList>(previousData), new List<string>(searchParams));
        }

        public DatumList[] GetFromPcas(Identifier keys, string[] searchParams, bool cocData = false)
        { 
            return dataReaderPcas.GetResults(GenerateMapKeys(keys, cocData), false, GenerateWhereClause(keys));
        }

        #endregion

        #region private Methods
        private string GenerateWhereClause(Identifier keys)
        {
            string where = string.Empty;

            where = string.Format("NODE = {0} AND (UPPER(TEST_STATUS) = 'PASS' OR UPPER(TEST_STATUS) LIKE '%FAIL%') AND TIME_DATE BETWEEN {1} AND {2}", keys.NodeId, keys.TimeForm, keys.TimeTo);

            return where;
        }

        private StringDictionary GenerateMapKeys(Identifier keys, bool cocData = false)
        {
            StringDictionary mapKeys = new StringDictionary();
            mapKeys["SCHEMA"] = cocData ? COC_SCHEMA : SCHEMA;
            mapKeys["SERIAL_NO"] = keys.SerialNumber;
            mapKeys["TEST_STAGE"] = keys.TestStage;

            if(!string.IsNullOrEmpty(keys.DeviceType))
                mapKeys["DEVICE_TYPE"] = keys.DeviceType;

            return mapKeys;
            
        }

        //private string AllParamsExist(DatumList previousData, string[] searchParams)
        //{
        //    //if (!searchParams.ToList().TrueForAll(s => previousData.IsPresent(s)))
        //    return searchParams.ToList().Find(s => !previousData.IsPresent(s));
        //}
        #endregion
    }
}



 