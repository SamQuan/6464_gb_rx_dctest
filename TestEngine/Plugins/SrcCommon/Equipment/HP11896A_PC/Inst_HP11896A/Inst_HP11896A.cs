// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_HP11896A.cs
//
// Author: alice.huang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_HP11896A : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_HP11896A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrhp11896A = new InstrumentDataRecord(
                "HEWLETT-PACKARD  11896A",				// hardware name 
                "0.000",  			// minimum valid firmware version 
                "5.000");			// maximum valid firmware version 
            instrhp11896A.Add("MaxPosistion", "999");
            instrhp11896A.Add("MinPosistion", "0");
            ValidHardwareData.Add("HP11896A", instrhp11896A);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_HP11896A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("HP11896A", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_HP11896A)chassisInit;

            this.Timeout_ms = 1000;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string command = "*IDN?\n";
                string rsp = this.instrumentChassis.Query_Unchecked(command, this);
                // Return the firmware version in the 4th comma seperated field
                return rsp.Split(',')[3].Trim().TrimStart('V');
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string command = "*IDN?\n";
                string rsp = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idn = rsp.Split(',');
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            StopScan();
            instrumentChassis.Clear();
            instrumentChassis.Write_Unchecked("*RST", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }

        /// <summary>
        /// Stops the scanning. The instrument is placed in manual mode and the scan time is set to zero.
        /// </summary>
        public void StopScan()
        {
            instrumentChassis.Write_Unchecked(":ABOR", this);
        }

        /// <summary>
        /// Starts the paddles scanning
        /// </summary>
        public void StartScan()
        {
            instrumentChassis.Write_Unchecked(":INIT:IMM", this);
        }

        /// <summary>
        /// Restores the state of the instrument from the specied  save/recall register. An instrument setup must 
        /// have been stored previously in the specied register. *RCL 0 has the same eect as *RST.
        /// </summary>
        /// <param name="regNum"> Register num , should be 0 ~ 9 </param>
        public void CallRegister(int regNum)
        {
            string cmd = string.Format("*RCL {0}", regNum);
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// Stores the current state of the device in a save register.
        /// </summary>
        /// <param name="regNum"> the save register where the data will be saved. 1 ~ 9 </param>
        public void SaveToRegister(int regNum)
        {
            string cmd = string.Format("*SAV {0}", regNum);
            instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// sets the positions of one of the paddles when the instrument is in manual mode. To insure the paddle has reached its nal
        /// position, it is best to send either the *WAI or *OPC commands before issuing other commands.
        /// </summary>
        public int Position
        {
            set
            {
                if (value > MaxPosition) value = MaxPosition;
                if (value < MinPosition) value = MinPosition;
                string cmd = string.Format(":PADD{0}:POS {1}", Slot, value);
                instrumentChassis.Write_Unchecked(cmd, this);
            }
            get
            {
                string cmd = string.Format(":PADD{0}:POS?", Slot);
                int psn = int .Parse ( instrumentChassis.Query_Unchecked(cmd, this));
                return psn;
            }
        }

        /// <summary>
        /// Gets maximun position available for adjust
        /// </summary>
        public int MaxPosition
        {
            get
            {
                return int.Parse(HardwareData["MaxPosistion"]);
            }
        }

        /// <summary>
        /// Gets minimun position available for adjust
        /// </summary>
        public int MinPosition
        {
            get
            {
                return int.Parse(HardwareData["MinPosistion"]);
            }
        }

        /// <summary>
        /// Making use of the chassis OPC* command
        /// </summary>
        public void WaitForOperationToComplete()
        {
            instrumentChassis.WaitForOperationComplete();
        }
        

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_HP11896A instrumentChassis;
        #endregion
    }
}
