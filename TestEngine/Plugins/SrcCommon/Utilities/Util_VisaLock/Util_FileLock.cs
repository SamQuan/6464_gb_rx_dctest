// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Utilities
//
// Util_FileLock.cs
//
// Author: Cypress Chen, 2009.03
// Design: [Reference design documentation]
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// A help class to lock a file.
    /// And you can use it to 'lock' any resource(including VISA/instrument etc.) by locking a file.
    /// </summary>
    public class Util_FileLock : IDisposable
    {
        #region static properties
        private static string _defaultFileName = null;
        private static int _defaultTimeout_ms = 0;
        private static int _defaultFileDetectedInterval_ms = 50;
        /// <summary>
        /// Gets or sets the default file name.
        /// </summary>
        public static string DefaultFileName
        {
            get { return _defaultFileName; }
            set { _defaultFileName = value; }
        }
        /// <summary>
        /// Gets or sets the default time out in ms.
        /// </summary>
        public static int DefaultTimeout_ms
        {
            get { return _defaultTimeout_ms; }
            set { _defaultTimeout_ms = value; }
        }
        /// <summary>
        /// Gets or sets the default file detected interval.
        /// </summary>
        public static int DefaultFileDetectedInterval_ms
        {
            get { return _defaultFileDetectedInterval_ms; }
            set { _defaultFileDetectedInterval_ms = value; }
        }
        #endregion

        /// <summary>
        /// Initializes the default settings such as the file name and timeout.
        /// </summary>
        /// <param name="defaultFileName">If the file name is empty, there will be no file locked.</param>
        /// <param name="defaultTimeout_ms">Default time out for waiting for locking the file.</param>
        /// <param name="defaultFileDetectedInterval_ms">Default Interval to check whether the file can be locked if trying to lock the file.</param>
        public static void Initialize(string defaultFileName, int defaultTimeout_ms,int defaultFileDetectedInterval_ms)
        {
            Util_FileLock._defaultFileName = defaultFileName;
            Util_FileLock._defaultTimeout_ms = defaultTimeout_ms;
            Util_FileLock._defaultFileDetectedInterval_ms = defaultFileDetectedInterval_ms;
        }

        #region events
        /// <summary>
        /// FileLocked event which happens when the file is locked.
        /// </summary>
        public static event EventHandler<FileLockedEventArgs> FileLocked;
        /// <summary>
        /// FileUnlocked event which happens when the file is unlocked.
        /// </summary>
        public static event EventHandler<FileUnlockedEventArgs> FileUnlocked;

        /// <summary>
        /// Raises FileLocked event.
        /// </summary>
        /// <param name="fileName"></param>
        protected void OnFileLocked(string fileName)
        {
            EventHandler<FileLockedEventArgs> tempHandler = FileLocked;
            if (tempHandler != null)
            {
                tempHandler(this, new FileLockedEventArgs(fileName));
            }
        }
        /// <summary>
        /// Raises FileUnlocked event.
        /// </summary>
        /// <param name="fileName"></param>
        protected void OnFileUnlocked(string fileName)
        {
            EventHandler<FileUnlockedEventArgs> tempHandler = FileUnlocked;
            if (tempHandler != null)
            {
                tempHandler(this, new FileUnlockedEventArgs(fileName));
            }
        }
        #endregion

        private FileStream _file;
        /// <summary>
        /// Initialize a new instance to lock a file.
        /// </summary>
        /// <param name="fileName">File Name.It can be a file on the server.If the file name is empty, there will be no file locked.</param>
        /// <param name="timeout_ms">Time out for trying to lock the file.</param>
        /// <param name="fileDetectedInterval_ms">The file detected interval in ms.</param>
        public Util_FileLock(string fileName, int timeout_ms,int fileDetectedInterval_ms)
        {
            _file = null;
            this.OpenFile(fileName, timeout_ms, fileDetectedInterval_ms);
        }

        /// <summary>
        /// Initializes an instance of the class with default settings.
        /// </summary>
        public Util_FileLock()
        {
            _file=null;
            if (_defaultFileName == null || _defaultTimeout_ms <= 0)
            {
                throw new InvalidOperationException("Please set the default file name and the default timeout first.");
            }
            this.OpenFile(_defaultFileName, _defaultTimeout_ms, _defaultFileDetectedInterval_ms);
        }

        /// <summary>
        /// Opens a file and lock it.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="timeout_ms"></param>
        /// <param name="fileDetectedInterval_ms"></param>
        private void OpenFile(string fileName,int timeout_ms,int fileDetectedInterval_ms)
        {
            if (fileName == null)
            {
                throw new ArgumentNullException("fileName");
            }
            if (timeout_ms <= 0 || fileDetectedInterval_ms <= 0)
            {
                throw new ArgumentOutOfRangeException("timeout_ms And fileDetectedInterval_ms", "Please make sure that timeout_ms>0 and fileDetectedInterval_ms>0.");
            }

            //do nothing if file name is empty.
            if (fileName == string.Empty)
            {
                return;
            }

            DateTime startTime = DateTime.Now;
            TimeSpan timeSpan = new TimeSpan(0, 0, 0, 0, timeout_ms);

           
            while (true)
            {

                try
                {
                    //Open the file and lock it.
                    _file = new System.IO.FileStream(fileName, FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
                    OnFileLocked(fileName);
                    break;
                }
                catch (IOException ex)
                {
                    string message = ex.Message;
                    //throw the exception if the file is not locked by other process.
                    if (!message.Contains("The process cannot access the file") || !message.Contains("because it is being used by another process."))
                    {
                        throw;
                    }

                    //throw time out exception.
                    if (DateTime.Now > startTime + timeSpan)
                    {
                        throw new TimeoutException("Time out for openning file:" + fileName, ex);
                    }
                    System.Threading.Thread.Sleep(fileDetectedInterval_ms);
                }
                catch
                {
                    throw;
                }
            }
        }



        #region IDisposable Members
        private bool _disposed = false;

        /// <summary>
        /// Disposes object.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore we call GC.SupressFinalize to take this object off the finalization queue 
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly or indirectly by a user's code. 
        // Managed and unmanaged resources can be disposed.
        // If disposing equals false, the method has been called by the runtime from inside the finalizer
        // and we should not reference other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // So that we only close file once we check to see if Dispose has already been called.
            if(!this._disposed)
            {
                if (disposing == true)
                {
                    if (_file != null)
                    {
                        _file.Close();
                        OnFileUnlocked(_file.Name);
                    }
                }
            }
            this._disposed = true;         
        }

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method does not get called.
        /// <summary>
        /// Destructor.
        /// </summary>
        ~Util_FileLock()      
        {
            Dispose(false);
        }

        #endregion
    }
}
