// [Copyright]
//
// Bookham [Coherence RX DC Test]
// Bookham.TestSolution.TestControl
//
// TC_CoRxDcTest.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Xml;
using System.Collections;
using Bookham.TestEngine.Framework.Exceptions;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.PluginInterfaces.MES;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;
using System.IO;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Generic;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.MES;
//using Bookham.TestLibrary.ExternalData;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test control for Corx dc test
    /// NB: Implements a Test Control with a single device in the batch.
    /// The following parameters are hardcoded in the DUTObject and will need to be changed,
    /// either by replacement value or by reading the MES:
    /// - ProgramPluginName and Version
    /// - PartCode
    /// - TestStage
    /// </summary>
    public class TC_CoRxDcTest : ITestControl
    {
        #region Constructor
        /// <summary>
        /// Constructor. 
        /// </summary>
        public TC_CoRxDcTest()
        {
            // initialise the Batch View tab control array (private variable)
            this.batchCtlList = new Type[1];
            this.batchCtlList[0] = typeof(BatchViewCtl);
            // TODO: Add any new controls here

            // initialise the Test Control tab control array (private variable)
            this.testCtlList = new Type[2];
            this.testCtlList[0] = typeof(LoadBatchDialogueCtl);  // mes online
            this.testCtlList[1] = typeof(ManualLoadBatchDialogueCtl);  // Mes offline, input Dut info manually
        }

        #endregion

        #region ITestControl Implementation.

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] BatchContentsCtlTypeList
        {
            get
            {
                return (batchCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// Informs the Test Engine of all the allowed Batch View GUI User Controls
        /// NB: DON'T CHANGE THIS!!!
        /// </summary>
        public Type[] TestControlCtlTypeList
        {
            get
            {
                return (testCtlList);
            }
        }

        /// <summary>
        /// Get Property.
        /// This specifies whether the Test Control Plug-in operated in Manual or Automatic Mode.
        /// </summary>
        public AutoManual TestType
        {
            get
            {
                // TODO: Update. Return AutoManual.AUTO if this is an automatic mode Test Control Plug-in.
                return AutoManual.MANUAL;
            }
        }

        /// <summary>
        /// Load a batch. Called when the Core requests that a new batch be loaded from the MES, via the 
        /// Test Control Server.
        /// </summary>
        /// <param name="testEngine">Object implementing the ITestEngine Interface.</param>
        /// <param name="mes">Object implementing the IMES interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">The privilege level of the currently logged in user.</param>
        /// <returns>The BatchId of the loaded batch.</returns>
        public string LoadBatch(ITestEngine testEngine, IMES mes,
            string operatorName, TestControlPrivilegeLevel operatorType)
        {
            // Get the Node from IMES (e.g. For FactoryWorks this is set by it's config XML file)
            this.inSimulationMode = testEngine.InSimulationMode;
            this.mes = mes as FactoryWorks;

            this.UserId = operatorName;

            this.node = mes.Node;
            dutInfo = null;
            // Read test control setting from config file
            ReadTestControlConfig(testEngine);

            #region xiaojiang  auto update SW

            if (!this.UpdateSoftwareFunc(testEngine, this.mes.Node.ToString()))
            {
                testEngine.ErrorRaise("Software is out of date!!!");
            }
            #endregion

            if (!Util_SrCalCounter.CheckSrCalCounter(true))
            {
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, 
                    " The splitter cal data is expired, \nto go on any test you have to run splitter calibration first");
                isMesOnline = false;
            }
            
            testEngine.SendStatusMsg(" Loading batch...");
            List<string> stageList = null;
            try
            {
                if (!isMesOnline)
                {
                    stageList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "TestStageNoFW.csv");
                    if (operatorType != TestControlPrivilegeLevel.Technician)
                    {
                        if (stageList.Contains("Sr Cal"))
                        {
                            stageList.Remove("Sr Cal");
                        }
                    }
                }
                else
                {
                    stageList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "FwsTestStageList.csv");
                    if (operatorType != TestControlPrivilegeLevel.Technician)
                    {
                        if (stageList.Contains("Sr Cal"))
                        {
                            stageList.Remove("Sr Cal");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                testEngine.ErrorRaise("Can't get test stage infomation available for this test control for " + ex.Message);
            }

            bool isSnOK = true;
            while (true)
            {
                // SHOW THE USER CONTROL
                testEngine.PageToFront(TestControlCtl.TestControl);
                if (isMesOnline)
                {

                    testEngine.SelectTestControlCtlToDisplay(typeof(LoadBatchDialogueCtl));
                }// MES ON LINE
                else
                {
                    testEngine.SelectTestControlCtlToDisplay(typeof(ManualLoadBatchDialogueCtl));
                    List<string> codeList = null;
                    try
                    {
                        codeList = Util_GetStageInfo.GetStageInfo(CONFIGFilePath + "PartCodeList.csv");
                    }
                    catch (Exception ex)
                    {
                        testEngine.ErrorRaise("Can't get test stage infomation available for this test control for " + ex.Message);
                    }

                    testEngine .SendToCtl (TestControlCtl .TestControl , typeof (ManualLoadBatchDialogueCtl ),
                                new DatumStringArray("PartCodeInfo",codeList.ToArray() ));
                    testEngine.SendToCtl(TestControlCtl.TestControl,
                        typeof(ManualLoadBatchDialogueCtl),
                        new DatumStringArray("StageInfo", stageList.ToArray()));

                }// MES NOT ON LINE
                testEngine.GetUserAttention(TestControlCtl.TestControl);

                // wait for the message to say "OK" has been clicked...
                CtlMsg msg = testEngine.WaitForCtlMsg();

                if (!isMesOnline)   // Get informatio as stage, genericcode, Sn
                {
                    this.dutInfo = (CoRxDCDutLoadInfo)msg.Payload;
                    
                    // if the GUI Msg is not available dut information, reload batch
                    if (this.dutInfo == null)
                    {
                        testEngine.SendStatusMsg(" invalid batch information!\n reloading batch information ...");
                        continue;
                    }
                    // If DutInfo is ok, set BatchId to SN
                    batchID = dutInfo.DutSerialNumber;

                }// mes not no line
                else
                {
                    batchID = (string)msg.Payload;
                    if (batchID == null || batchID.Length < 4)
                    {
                        testEngine.SendStatusMsg(" invalid batch information!\n reloading batch information ...");
                        continue;
                    }
                    try
                    {                        
                        testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl),
                            new MesLoadDeviceMsg(" Retrieve batch information from MES, Please wait ... ", false, false));
                        testEngine.SendStatusMsg("Retrieving Batch information from MES ...");
                        //MESbatch dutBatch = mes.LoadBatch(batchID, true);
                        MESbatch dutBatch = this.mes.LotQueryFullNew(batchID, this.UserId);

                       

                        if (dutBatch[0] != null)
                        {// if batch information has been retrieve, process the information
                           // this.mes.Node = this.nodeID;

                            if (dutBatch.Attributes.ReadString("current_rule").ToLower() == "trackin")
                            {
                                this.mes.LotStateNew(batchID, this.UserId);
                            }

                            // if tha batch is not available for stage deliver, promote to reload batch
                            if (!(stageList.Contains(dutBatch[0].TestStage.Trim())))   // may be can set it as string "stage in factory work"in configration file
                            {
                                testEngine.SendStatusMsg("Batch is in incorrected stage in Mes");
                                testEngine.ShowUserQuery(TestControlTab.TestControl,
                                    " the batch stay in stage of " + dutBatch[0].TestStage +
                                    " \n so it can't process in data deliver stage, please reload batch!",
                                    new Bookham.TestEngine.PluginInterfaces.TestControl.ButtonInfo("&Continue", null));
                                testEngine.SendToCtl(TestControlCtl.TestControl,
                                    typeof(LoadBatchDialogueCtl),
                                    new MesLoadDeviceMsg("  waiting to load new batch", true, true));
                                continue;
                            }
                            this.dutInfo = new CoRxDCDutLoadInfo(dutBatch[0].SerialNumber, dutBatch[0].PartCode);
                            
                            dutInfo.DeviceType = dutBatch[0].PartCode;
                            dutInfo.TestStage = dutBatch[0].TestStage;
                            //if (dutBatch[0].Attributes.IsPresent("lot_type"))
                            //{
                            //    dutInfo.LotType = dutBatch[0].Attributes.ReadString("lot_type");
                            //}

                            CheckLotType(testEngine, dutBatch[0]);

                            dutInfo.LotType = dutBatch[0].Attributes.ReadString("lot_type");
                            
                        }// if available mes information has been achieve
                        else
                        {
                            continue;   // if no available dut in batch, go on batch load loop
                        }


                        #region xiaojiang 20160902 check SPC


                        ConfigDataAccessor configData = new ConfigDataAccessor(CONFIGFilePath + "Fws2PcasStage.xml", "Fws2PcasStage");
                        string pcasStage = "";
                        if (configData != null)
                        {
                            StringDictionary keys = new StringDictionary();
                            keys.Add("FwsStageName", dutInfo.TestStage);
                            DatumList list = null;
                            try
                            {
                                list = configData.GetData(keys, true);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                            if (list != null) pcasStage = list.ReadString("PcasStageName").Trim();
                        }
                        configData = null;

                        // Lookup node based on configuration
                        //this.mes.Node = this.nodeID;
                        this.PcasStage = pcasStage;
                        //Do spc check
                        CheckSPCStatus(testEngine);

                        #endregion

                    }// try to retrieve mes infromation                    

                    catch (MESTimeoutException)
                    {
                        // Log the fact that there was an MES Timeout exception.
                        testEngine.SendStatusMsg("MES Load Batch Operation timedout.");

                        //Just start over.
                        continue;
                    }
                    catch (MESInvalidOperationException e)
                    {
                        // Log the fact that there was an MES Invalid operation exception.
                        testEngine.SendStatusMsg("MES Invalid operation exception." + e.Message + e.StackTrace);


                        //Just start over.
                        continue;
                    }
                    catch (MESCommsException e)
                    {
                        testEngine.SendStatusMsg("MES Communications exception." + e.Message + e.StackTrace);

                        //Just start over.
                        continue;
                    }
                    catch ( Exception ex)
                    {

                        //TestEngine.PluginInterfaces.TestControl.ButtonId rsp =
                            testEngine.ShowContinueUserQuery(TestControlTab.TestControl, ex.Message );
                            testEngine.SendStatusMsg(ex.Message);
                        //if (rsp == Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId.Yes)
                        //{
                            testEngine.SendToCtl(TestControlCtl.TestControl, typeof(LoadBatchDialogueCtl),
                              new MesLoadDeviceMsg(" waiting to load new batch", true, true));
                            continue;
                        //}
                        //else
                        //{
                        //    continue;
                        //    isSnOK = false;
                        //}
                    }
                }   // mes on line
                
                break;
            }  // while ( true) to get available batch information

            // ... Clear up GUI.
            testEngine.CancelUserAttention(TestControlCtl.TestControl);
            testEngine.GuiClear(TestControlCtl.TestControl);
            // Reset one-time run flag
            programRanOnce = false;
            // Return serial number to Test Engine

            testEngine.SendStatusMsg("Batch load completed!");
            if (isSnOK)
                return dutInfo.DutSerialNumber;
            else
                return null;
        }

        /// <summary>
        /// Get the next available DUT from the Batch.
        /// </summary>
        /// <param name="testEngine">Object that implements the ITestEngine Interface.</param>
        /// <param name="testJigArg">Object tha implements ITestJig Interface.</param>
        /// <param name="operatorName">The name of the currently logged in user.</param>
        /// <param name="operatorType">Privilege level of the currently logged in user.</param>
        /// <returns>DUT object for the next selected device for test or Null if end of batch.</returns>
        public DUTObject GetNextDUT(ITestEngine testEngine, ITestJig testJigArg,
            string operatorName, TestControlPrivilegeLevel operatorType)
        {
            
            if (this.programRanOnce) return null; // end of batch after one run!

            if ((!Util_SrCalCounter.CheckSrCalCounter(false)) && (dutInfo.TestStage != srCalStageName ))
            {
                Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId optUser=
                    testEngine.ShowYesNoUserQuery(TestControlTab.TestControl, 
                    " The splitter cal data is expired,"+
                    "\nto go on any test you have to run splitter calibration first" +
                    "Do you want to run splitter calibration now?");
                if (optUser == Bookham.TestEngine.PluginInterfaces.TestControl.ButtonId.Yes)
                {
                    dutInfo.TestStage = srCalStageName;
                }
                else
                {
                    testEngine.ErrorRaise ("Splitter calibration data expired, test can't go on");
                    return null;
                }
                isMesOnline = false;
            }


            testEngine.SelectBatchCtlToDisplay(typeof(BatchViewCtl));
            testEngine.PageToFront(TestControlCtl.BatchView);

            //Comstruct DUTObject - test engine get required program, serial no etc. from here
            dut = new DUTObject();
            dut.BatchID = batchID;  //from Load Batch
            dut.SerialNumber = dutInfo.DutSerialNumber;
            // Equipment ID is based on network computer name
           // dut.EquipmentID = System.Net.Dns.GetHostName();
            // Set results node
            dut.NodeID = this.node;
            // Get Test Jig ID
            dut.TestJigID = testJigArg.GetJigID(testEngine.InDebugMode, testEngine.InSimulationMode);

            //dut.EquipmentID = "SZN-CFP2-RX-DC01";
            dut.EquipmentID = EquitpmentReader.ReadEquipment(testEngine, dut);

            //if (lastTestStageName != "pincheck" && this.programStatus == ProgramStatus.Success)
            //{
            //    lastTestStageName = "pincheck";
            //    //dut.TestStage = "PinCheck";
            //    dut.PartCode = "pa014138";
            //}
            //else
            //{
            //    if (this.programStatus != ProgramStatus.Success)
            //    {
            //        return null;
            //    }

                // TODO: CHANGE THE FOLLOWING LINES FOR REQUIRED PROGRAM AND DEVICE
                // Set generic code to be Part code
                dut.PartCode = dutInfo.PartCode;
                if (!isMesOnline)
                    dut.TestStage = dutInfo.TestStage;
                else
                {
                    ConfigDataAccessor configData = new ConfigDataAccessor(CONFIGFilePath + "Fws2PcasStage.xml", "Fws2PcasStage");
                    string pcasStage = "";
                    if (configData != null)
                    {
                        StringDictionary keys = new StringDictionary();
                        keys.Add("FwsStageName", dutInfo.TestStage);
                        DatumList list = null;
                        try
                        {
                            list = configData.GetData(keys, true);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        if (list != null) pcasStage = list.ReadString("PcasStageName").Trim();
                    }
                    configData = null;
                    dut.TestStage = pcasStage;
                    dut.Attributes.AddString("pcasStage", pcasStage);
                }


                //if (!dut.TestStage.ToLower().Contains("phase") && !dut.TestStage.ToLower().Contains("sr cal") && lastTestStageName != "pincheck" && this.programStatus == ProgramStatus.Success)
                //{
                //    lastTestStageName = "pincheck";
                //    dut.TestStage = "PinCheck";
                //    //dut.PartCode = "pa014138";
                //}
                //else
                //{
                //    if (this.programStatus != ProgramStatus.Success)
                //    {
                //        return null;
                //    }
                //}
            //}
            // Add reference so that we can add information of device type list and specification to it
            //dut.Attributes.AddReference("DutInfo", DutInfo); 
            if (dut.TestStage == srCalStageName)
                dut.ProgramPluginName = "TP_CoRxSRCalTest";
            else if (dut.TestStage == dcTestName)
                dut.ProgramPluginName = "TP_CoRxDcTest";
            else if ((dut.TestStage == phaseAngleStageName) || (dut.TestStage == phaseAngleSPCStageName))
                dut.ProgramPluginName = "TP_CoRxPATest";
            else if ((dut.TestStage == dcMeasureStageName) || (dut.TestStage == dcTestSPCStageName)
                                    || (dut.TestStage == dcPreLidTestName) || (dut.TestStage == PRELIDDINGStageName))
                dut.ProgramPluginName = "TP_CoRxDcTest_NewLimit";
            else if (dut.TestStage == pinCheckStageName)
                dut.ProgramPluginName = "TP_CoRxDcTest_PinCheck";
        
            dut.ProgramPluginVersion = "";
            dut.Attributes.Add(new DatumBool("IsUseMes", isMesOnline));
            dut.Attributes.AddString("DeviceType", dutInfo.DeviceType);

            if (isMesOnline)
            {
                dut.Attributes.AddString("lot_type", dutInfo.LotType);
            }
            else
            {
                dut.Attributes.AddString("lot_type", "Engineering");
            }

            try
            {
                //add max continous test count
                if (dut.Attributes.IsPresent("lot_type"))
                {
                    string lotType = dut.Attributes.ReadString("lot_type");
                    if (lotType.ToLower() == "production")
                    {
                        if (RetestOutofRange(testEngine, dut))
                        {
                            testEngine.ShowContinueUserQuery(TestControlTab.TestControl,
                                string.Format("Retest count out of spec, testing denied! MaxTestCount[{0}]", this.maxTestCount));

                            return null;
                        }
                    }
                }
                
            }
            catch { }

            testEngine.GetUserAttention(TestControlCtl.BatchView);
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dut);

            CtlMsg msg = testEngine.WaitForCtlMsg();
            testEngine.CancelUserAttention(TestControlCtl.BatchView);

            DutTestOption testOption = null;
            try
            {
                testOption = (DutTestOption)msg.Payload;
            }
            catch
            { }

            if (testOption == null) return null;

            // only allowed though here once!
            if (testOption.TestOption == enmTestOption.GoOnTest)
            {
                if (!dut.TestStage.ToLower().Contains("pincheck"))
                {
                    this.programRanOnce = true;
                }
                
                return dut;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Called after the test program has concluded, to decide what the outgoing 
        /// Part ID and other attributes are, having inspected the test results.
        /// </summary>
        /// <param name="testEngine">Reference to object implmenting ITestEngine</param>
        /// <param name="programStatus">The Status of the Test Program Run (succeeded/error/aborted etc).</param>
        /// <param name="testResults">Detailed test results</param>
        /// <returns>Outcome of the results analysis.</returns>
        public DUTOutcome PostDevice(ITestEngine testEngine, ProgramStatus programStatus, TestResults testResults)
        {
            //TODO: Modify DUTOutcome object, 
            //If required: add details based on analysis of the test results, specify 
            //what specifications the test results are to be stored against plus any other supporting data.

            List<String> specsToUse = new List<String>();
            foreach (SpecResults specRes in testResults.SpecResults)
            {
                specsToUse.Add(specRes.Name);
            }
            DUTOutcome dutOutcome = new DUTOutcome();
            dutOutcome.OutputSpecificationNames = specsToUse.ToArray();
            if (programStatus == ProgramStatus.Success)
            {
                for (int i = 0; i < dutOutcome.OutputSpecificationNames.Length; i++)
                {
                    if (testResults.SpecResults[dutOutcome.OutputSpecificationNames[i]].Status.Status == PassFail.Pass)
                    {
                        programStatus = ProgramStatus.Success;
                        break;
                    }
                    else
                    {
                        programStatus = ProgramStatus.Failed;
                    }
                }
            }
            this.programStatus = programStatus;
            if (testResults.OverallPassFail != Bookham.TestEngine.Framework.Limits.PassFail.Pass)
                isDutTrackOut = false;
            return dutOutcome;
        }

        /// <summary>
        /// Commit any post-test MES updates prepared in method PostDevice.
        /// </summary>
        /// <param name="testEngine">Object implementing ITestEngine</param>
        /// <param name="dataWriteError">True if there was a problem writing Test Results to the database.</param>
        public void PostDeviceCommit(ITestEngine testEngine, bool dataWriteError)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the current device under test to the MES.
            testEngine.SendStatusMsg("Post Device Commit");
            ////Mark the Device as Tested. Also record it's pass fail status.
            if ((this.programStatus != ProgramStatus .Success) && !inSimulationMode && isMesOnline)
            // We only want to condemn a device which is definitely duff, so assume all
            // other statuses leave the device as "untested".
            {                                
                isDutTrackOut = false;
               
            }
                       
        }

        /// <summary>
        /// The Test Engine has concluded operations on the currently loaded batch. Update Batch level parameters 
        /// in the MES.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void PostBatch(ITestEngine testEngine)
        {
            //TODO: For those Test Software Solutions which interact with an MES, store the test
            //status for the currently loaded Batch in the MES.
            testEngine.SendStatusMsg("Post Batch");

            //string[] files;
            //files = Directory.GetFiles(@"C:\Testset\CFP2GB_Rx_DCTest\TestEngine\pcasc\2449", "*.pcasDrop");

            //DatumList listDevice = new DatumList();
            //foreach (string file in files)
            //{
            //    using (CsvReader cr = new CsvReader())
            //    {
            //        List<string[]> list = cr.ReadFile(file);
            //        string deviceType = "";
            //        string passFailedResult = "";
            //        for (int i = 0; i < list.Count; i++)
            //        {
            //            if (list[i][0] == "~DEVICE TYPE")
            //            {
            //                deviceType = list[i + 1][0];
            //            }
            //            if (list[i][0] == "$TEST_STATUS$0")
            //            {
            //                passFailedResult = list[i + 1][0];
            //            }
            //        }

            //        listDevice.AddString(deviceType, passFailedResult);
            //    }
            //}
            

            //Trackout the Batch if we are not in Simulation Mode.
            if ((!this.inSimulationMode) && (isDutTrackOut) && isMesOnline &&
                this.programStatus == ProgramStatus.Success)
            {
                mes.TrackOutBatchPass(dut.BatchID, dutInfo.TestStage, dutInfo.PartCode);
            }
        }

        /// <summary>
        /// Mark all devices in the Batch for retest.
        /// </summary>
        /// <param name="testEngine">Reference to object that implements the ITestEngine Interface.</param>
        public void RestartBatch(ITestEngine testEngine)
        {

            //TODO: Use whatever method is appropriate to your implmentation to mark all devices in the 
            //current batch untested. It is up to the implmenter to decide whether this means restart all 
            //devices tested in this session or whether to restart the entire batch. Where this functionality
            //is not required, the solution developer may choose to take no action here.
            testEngine.SendToCtl(TestControlCtl.BatchView, typeof(BatchViewCtl), dutInfo);
            testEngine.SendStatusMsg("Restart Batch");
        }
        # endregion



        #region add by xiaojiang
        private int GetTestCycle(ITestEngine engine)
        {
            StringDictionary mapkeys = new StringDictionary();
            mapkeys.Add("SCHEMA", "hiberdb");
            mapkeys.Add("SERIAL_NO", this.dut.SerialNumber);
            mapkeys.Add("DEVICE_TYPE", this.dut.PartCode);//this.pcasDeviceType);
            mapkeys.Add("TEST_STAGE",this.dut.TestStage);//this.MainTestStage);

            
            DatumList[] ret = engine.GetDataReader().GetResults(mapkeys, true, "");
            if (ret == null) return 0;
            return ret.Length;
        }

        private PassFail GetLatestResult(ITestEngine engine)
        {
            StringDictionary mapkeys = new StringDictionary();
            mapkeys.Add("SCHEMA", "hiberdb");
            mapkeys.Add("SERIAL_NO", this.dut.SerialNumber);
            mapkeys.Add("DEVICE_TYPE", this.dut.PartCode);
            mapkeys.Add("TEST_STAGE", this.dut.TestStage);

            DatumList datum = engine.GetDataReader().GetLatestResults(mapkeys, false);
            if (datum == null) return PassFail.Fail;

            return datum.ReadString("TEST_STATUS").ToLower().Contains("pass") ? PassFail.Pass : PassFail.Fail;
        }

        #endregion

        /// <summary>
        /// Read test control setting
        /// </summary>
        /// <param name="engine"></param>
        private void ReadTestControlConfig(ITestEngine engine)
        {
            if (isTCPliginConfigRead)
                return;

            int result;
            string commonErrorMessageString = "Unable to read Test Control Plug-in configuration File: ";

            //Read Configuration file.
            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader config = new XmlTextReader(CONFIGFilePath + "Node" + node.ToString() + "/TestControlPluginConfig.xml");

            try
            {
                /* Scan file, one element a time. */
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                        if (config.LocalName == "UseMES")
                        {
                            string useMes = config.ReadString();

                            result = String.Compare(useMes.Trim(), "yes", true, System.Globalization.CultureInfo.InvariantCulture);
                            if (result == 0)
                            {
                                isMesOnline = true;
                            }
                            else
                            {
                                result = String.Compare(useMes.Trim(), "no", true, System.Globalization.CultureInfo.InvariantCulture);

                                if (result == 0)
                                {
                                    isMesOnline = false;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid UseMES field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else
                        }  // end if "UseMes"

                        if (config.LocalName == "EquipmentID")
                        {
                            equipmentID = config.ReadString();
                        }

                        if (config.LocalName == "UpdateSoftwareConfig")
                        {
                            updateSoftwareConfig = config.ReadString();
                        }

                        if (config.LocalName == "InhibitTrackout")
                        {
                            string trackoutInhibitString = config.ReadString();

                            result = String.Compare(trackoutInhibitString.Trim(), "yes", true);
                            if (result == 0)
                            {
                                isDutTrackOut = false;
                            }
                            else
                            {
                                result = String.Compare(trackoutInhibitString.Trim(), "no", true);

                                if (result == 0)
                                {
                                    isDutTrackOut = true;
                                }
                                else
                                {
                                    //Invalid configuration file contents.
                                    engine.ErrorRaise("Invalid InhibitTrackout field contents in Configuration/ITLA/TestControlPluginConfig.xml configuration file.");
                                }
                            }  // end else: "InhibitTrackout" != "yes"
                        }  // end if "InhibitTrackout"

                        if (config.LocalName == "SrCalCounterFilePath")
                        {
                            string filePath = config.ReadString().Trim();
                            
                            Util_SrCalCounter.SrCalCounterFilePath = filePath;
                        }
                        if (config.LocalName == "SrCalStageName")
                        {
                            srCalStageName = config.ReadString().Trim();
                        }
                        if ( config .LocalName == "PhaseAngleStageName")
                        {
                            phaseAngleStageName = config.ReadString().Trim();
                        }
                        if ( config .LocalName == "DcTestStageName")
                        {
                            dcTestName = config .ReadString().Trim();
                        }
                        if (config.LocalName == "DcMeasurementStageName")
                        {
                            dcMeasureStageName = config.ReadString().Trim();
                        }

                        if (config.LocalName == "DcSpcStageName")
                        {
                            dcTestSPCStageName  = config.ReadString().Trim();
                        }
                        if (config.LocalName == "PhaseAngleSpcStageName")
                        {
                            phaseAngleSPCStageName = config.ReadString().Trim();
                        }

                        if (config.LocalName == "DcPreLidStageName")
                        {
                            dcPreLidTestName = config.ReadString().Trim();
                        }

                        if (config.LocalName == "PRELIDDINGStageName")
                        {
                            PRELIDDINGStageName = config.ReadString().Trim();
                        }

                        if (config.LocalName=="PinCheckStageName")
                        {
                            pinCheckStageName = config.ReadString().Trim();
                        }

                        if (config.LocalName == "MaxTestCount")
                            this.maxTestCount = int.Parse(config.ReadString().Trim());
                        
                    } // end if config.NodeType == XmlNodeType.Element: get available text nod
                }  // end while loop

                // Set flag to indicate that configuration has been read.
                isTCPliginConfigRead = true;
            }
            catch (UnauthorizedAccessException uae)
            {

                /* User did not have read permission for file. */
                engine.ErrorRaise(commonErrorMessageString + "Access denied " + uae.Message + uae.StackTrace);
            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                /* File not found error. */
                engine.ErrorRaise(commonErrorMessageString + "Configuration File not found " + fnfe.Message);
            }
            catch (System.IO.IOException ioe)
            {
                /* General IO failure. Not file not found. */
                engine.ErrorRaise(commonErrorMessageString + "General IO Failure " + ioe.Message);
            }
            catch (System.Xml.XmlException xe)
            {
                /* XML parse error. */
                engine.ErrorRaise(commonErrorMessageString + "XMl Parser Error " + xe.Message);
            }
            finally
            {
                /* Close file. */
                if (config != null) config.Close();
            }
        }

        public void CheckLotType(ITestEngine testEngine, DUTObject loadedBatch)
        {
            if (loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "ENGINEERING" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "PRODUCTION" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "CUSTOMERRETURN" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "REWORK" || loadedBatch.Attributes.ReadString("lot_type").ToUpper() == "STANDARD")
            {
                //DO NOTHING
            }
            else
            {
                testEngine.ErrorRaise("This batch's current lot_type is " + loadedBatch.Attributes.ReadString("lot_type") + " , it must be engineering,production or customerreturn !"+
                    "\r\n当前器件lot_type 是 " + loadedBatch.Attributes.ReadString("lot_type") + " , 必须是 engineering,production 或者 customerreturn !");
            }
        } 



        private ActionResult CheckSPCStatus(ITestEngine testEngine)
        {
            #region xiaojiang 20160902
            ActionResult result = ActionResult.Empty;
            try
            {
                if (this.PcasStage == "")
                {
                    this.PcasStage = "ALL";
                }
                result = SpcHelper.MustDoSpcTest(testEngine, this.mes.Node, this.PcasStage);
            }
            catch
            {
                result = ActionResult.Empty;
            }

            if (result == ActionResult.Continue)
            {
                // do nothing
            }
            else if (result == ActionResult.Information)
            {
                //testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "Please do spc test today as soon as posible!\n(请尽快做标准件测试！)");
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "请尽快做标准件测试！");
            }
            else if (result == ActionResult.Stop)
            {
                //testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "Please do spc test first today!\n(请先做标准件测试！)");
                //throw new Exception("Must test spc first and then do production devices!\n(请先做标准件测试才允许做产品测试！)");
                testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "请先做标准件测试！");
                throw new Exception("请先做标准件测试！");
            }
            else if (result == ActionResult.ForceStop)
            {
                //testEngine.ShowContinueUserQuery(TestControlTab.TestControl, "SPC Rule Failed! Please retest！\n(标准件超出SPC limit 范围！请重新测试标准件！)");
                //throw new Exception("SPC Rule Failed! Please retest！\n(标准件超出SPC limit 范围！请重新测试标准件！)");
                throw new Exception("标准件超出SPC limit 范围！请重新测试标准件！");
            }
            else if (result == ActionResult.Empty)
            {
                testEngine.SendStatusMsg("Error to judge daily spc status: Action is Empty!!!");
            }
            #endregion


            return result;
        }

        private string UpdateFileString(string updatefile)
        {
            string updateContent = string.Empty;
            if (File.Exists(updatefile))
            {
                using (StreamReader sr = new StreamReader(updatefile))
                {
                    updateContent = sr.ReadLine().Trim();
                }
            }

            return updateContent;
        }
        private bool ResetUpdateFileEmpty(string node, string updatefile)
        {
            try
            {
                INI.WriteValue(this.updateSoftwareConfig, "Node", node, "True");

                FileInfo info = new FileInfo(updatefile);
                info.IsReadOnly = false;

                using (StreamWriter sw = new StreamWriter(updatefile))
                {
                    sw.WriteLine("Empty!");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool UpdateSoftwareFinished(string node)
        {
            if (string.IsNullOrEmpty(this.updateSoftwareConfig)) return true;

            bool result = true;
            string value = "true";
            try
            {
                value = INI.GetStringValue(this.updateSoftwareConfig, "Node", node, "True");
            }
            catch { }

            if (bool.TryParse(value, out result))
                return result;

            return true;
        }
        private bool UpdateSoftwareFunc(ITestEngine engine, string node)
        {
            string updatefile = "Configuration\\UpdateFile";
            string updateContent = string.Empty;
            updateContent = UpdateFileString(updatefile);

            if (this.UpdateSoftwareFinished(node))
            {
                if (updateContent.ToLower() == "latest")
                    ResetUpdateFileEmpty(node, updatefile);

                return true;
            }

            updateContent = UpdateFileString(updatefile);

            if (updateContent.ToLower() == "latest")
            {
                ResetUpdateFileEmpty(node, updatefile);
                return true;
            }


            engine.ShowContinueUserQuery(TestControlTab.TestControl, "Software has been already updated, please download the latest version.\r\n软件版本已更新，请关闭软件重新下载最新版本进行测试！");
            return false;
        }

        private bool RetestOutofRange(ITestEngine engine, DUTObject dut)
        {
            StringDictionary mapKeys = new StringDictionary();

            mapKeys.Add("SCHEMA", "hiberdb");
            mapKeys.Add("SERIAL_NO", dut.SerialNumber);
            mapKeys.Add("TEST_STAGE", dut.TestStage);
            mapKeys.Add("DEVICE_TYPE", dut.PartCode);//这个要加上，因为测试次数达到最大时，可以转Code再测试。

            DatumList list = engine.GetDataReader().GetLatestResults(mapKeys, false);

            if (list.IsPresent("RETEST"))
            {
                string retest = list["RETEST"].ValueToString();

                engine.SendStatusMsg("RETEST:" + retest);

                int CL = this.ReadRetest(dut.TestStage);
                int RC = 999;
                int.TryParse(retest.Trim(), out RC);

                if (RC == CL - 1)
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                else if (RC == CL)
                {
                    return true;
                }
            }
            else
            {
                DatumList[] lists = engine.GetDataReader().GetResults(mapKeys, false, "");

                int CL = this.ReadRetest(dut.TestStage);
                if (lists.Length == CL)
                {
                    engine.ShowContinueUserQuery(TestControlTab.TestControl, "Warning: This is the last time to test this device on this stage!!!");
                }
                else if (lists.Length == CL+1)
                {
                    return true;
                }
            }
            return false;
        }

        private int ReadRetest(string stage)
        {
            string file = "Configuration\\Retest2Scrap.csv";
            if (!File.Exists(file)) return 999;

            int count = 999;

            using (StreamReader sr = new StreamReader(file))
            {
                while (!sr.EndOfStream)
                {
                    string[] line = sr.ReadLine().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (line.Length != 2) continue;

                    if (stage.ToLower() == line[0].Trim().ToLower())
                    {
                        int.TryParse(line[1].Trim(), out count);

                        break;
                    }
                }
            }
            return count;
        }

        #region Private data
        // Batch View tab controls
        Type[] batchCtlList;
        // Test Control tab controls
        Type[] testCtlList;
        /// <summary>
        /// 
        /// </summary>
        string batchID;

        // Flag for the program ran (so we can abort the batch)
        bool programRanOnce = false;
        // MES / Results database (PCAS) Node
        int node;
        // Serial number variable
        //string serialNum;
        // Serial number variable
        CoRxDCDutLoadInfo dutInfo;
        /// <summary>
        /// flag to indicate if test control configureation file was read or not
        /// </summary>
        bool isTCPliginConfigRead;
        /// <summary>
        /// Flag to indicate if Fws is available or not
        /// </summary>
        bool isMesOnline;
        /// <summary>
        /// Flag to indicate if dut trackout after test
        /// </summary>
        bool isDutTrackOut;
        FactoryWorks mes;
        string UserId;
        int maxTestCount;

        ProgramStatus programStatus;
        bool inSimulationMode;
        const string CONFIGFilePath = "configuration/CoRxDcTest/";
        string updateSoftwareConfig = "";

        DUTObject dut;
        string srCalStageName = "";
        string phaseAngleStageName = "";
        string dcTestName = "";  // old DC Test
        string dcMeasureStageName = "";  // dc measurement
        string dcTestSPCStageName = "";
        string phaseAngleSPCStageName = "";
        string dcPreLidTestName = "";
        string PRELIDDINGStageName = "";
        string pinCheckStageName = "";
        int nodeID;
        string equipmentID;
        string lastTestStageName = "";
        string PcasStage = "";

        TestResults testAllResult;

        #endregion

    }
}
