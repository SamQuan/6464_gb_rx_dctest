// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_X2.cs
//
// Author: mark.fullalove, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestSolution.ChassisNS
{
    // TODO: 
    // Fill in the gaps.
    public class Chassis_X2 : ChassisType_Serial
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_X2(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "Hardware_Unknown",			// hardware name 
                "Firmware_Unknown",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "Firmware_Unknown";
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "Hardware_Unknown";
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    // Custom setup for this chassis before it goes online

                    // Setup RS232 variables
                    this.BaudRate = 19200;
                    this.DataBits = 8;
                    //this.StopBits = System.IO.Ports.StopBits.One;
                    this.Handshaking = System.IO.Ports.Handshake.None;
                    this.Parity = System.IO.Ports.Parity.None;
                    this.InputBufferSize_bytes = 1024;
                    this.OutputBufferSize_bytes = 1024;
                    this.NewLine = "\r";

                    this.Configure(BaudRate, DataBits, StopBits, Parity, Handshaking, InputBufferSize_bytes, OutputBufferSize_bytes);
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // Custom setup for this chassis after it goes online,  
                    // When the port is opened "FF" appears in the Rx buffer

                    // Flush the buffer


                }
            }
        }
        #endregion
    }
}
