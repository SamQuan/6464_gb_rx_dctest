using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Bookham.TestLibrary.Utilities;

using Bookham.TestEngine.Framework.InternalData;
using System.Collections.Specialized;
using System.Diagnostics;

namespace WindowsApplication1
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            


            //create a dummy datum list: this is to simulate the
            // object data that the test control will pass thru to us
            Datum MyDatumInstance;
            DatumList MyDatumListInstance = new DatumList();
            string myColumnName;
            
            
            MyDatumInstance = new DatumString("teststage","final test");
            MyDatumListInstance.Add(MyDatumInstance);
            MyDatumInstance = new DatumString("devicetype","ntw606de");
            MyDatumListInstance.Add(MyDatumInstance);


            // i have a config file hadler for each config file, and keep these all in a collection
            ConfigFileHandlerCollection myconffiles = new ConfigFileHandlerCollection();
            ConfigFileHandler instruments =new ConfigFileHandler(@"c:\TE_Framework\src\Tools\ConfigTool\TestConfigFile4.xml", "instruments", MyDatumListInstance, true);
            // i pass in the file path, the tablename (which is also the name i know it as in the collection, and a flag to say if i am a read / writable config file
            ConfigFileHandler stages = new ConfigFileHandler(@"c:\TE_Framework\src\Tools\ConfigTool\TestConfigFile3.xml", "stages", MyDatumListInstance, true);
            
            
            //i add the config files to the collection
            myconffiles.Add(stages);
            myconffiles.Add(instruments);

            //ok, so my config file handler has now looked at the dataset columns, and for any columns in
            //my datumlist that i passed it, it will have kept a record of the known keys for this config file.


            //from now on, if i want to get config data, i just have to pass in the particular keys i am really interested in
            //ie i don't need to keep worrying about device type, test stage anymore, the handler is doing this for me


            //ok, so now we want to do a search for our entry where address = 717

            // old method = create keylist with things like device type, test stage, address so we get the right row
            //new method  = just use the key you have a particular need for, ie address = 717
            StringDictionary mySearchValue = new StringDictionary();
            mySearchValue.Add("address","717");


            StringDictionary mySearchValue2=new StringDictionary();
            StringDictionary emptydict = new StringDictionary();


            // the next command will take your one key, and add the other known keys to the list
            //and return this new compound key list to you
            mySearchValue2 = myconffiles["stages"].GetCompoundKeyList(emptydict);

            int rowCount;
            DatumList dataValues;

            

            //If i want to, i can do a search for my particular config data, knowing that the confighandler will
            //create the compound list for me
            mySearchValue.Clear();
            mySearchValue.Add("address", "717");
            
            
            rowCount= myconffiles["stages"].GetMatchingRowCount(mySearchValue, true);
            dataValues=myconffiles["stages"].GetARowOfData(mySearchValue, true);
            Debug.Print(dataValues.ToStringSummary());

            mySearchValue.Clear();
            mySearchValue.Add("hmmer", "445");
            
            // the easy bit is i can do this type of thing on any one of my 15 config files, and i now don't need to keep
            //track of that particular files known data, the handler does it for me.
            rowCount = myconffiles["instruments"].GetMatchingRowCount(mySearchValue, false);
            dataValues = myconffiles["instruments"].GetARowOfData(mySearchValue, false);
            Debug.Print(dataValues.ToStringSummary());



            //so maybe i want to add a new key to the stored list, or overwrite a stored value in a particular config files
            //known key list
            myconffiles["instruments"].AddNewKeysToStoredKeyList(dataValues);

            // or add a key.
            myconffiles["instruments"].RemoveKeyFromStoredKeyList("devicetype");



            //anyway, as you can see, the config file handler allows you to read or write data without
            //you having to try and keep track of the particular known column names and values.
            //you only have to keep track of the things you are interested in.


        }
    }
}