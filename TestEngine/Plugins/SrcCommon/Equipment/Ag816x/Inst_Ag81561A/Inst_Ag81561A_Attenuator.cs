// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestSolution.Instruments
//
// Inst_Ag81561A_Attenuator.cs
//
// Author: Simon Ede, adapted by Bill P. Godfrey, 2006.
// Design: As specified in Ag8156 driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Agilent 81561A optical attenuator driver
    /// </summary>
    public class Inst_Ag81561A_Attenuator : InstType_OpticalAttenuator
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag81561A_Attenuator(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Configure valid hardware information

            // Add all Ag81561A details
            InstrumentDataRecord Ag8156AInstData = new InstrumentDataRecord(
                "Agilent Technologies 81561A",      // hardware name of chassis
                "0",  			                // minimum valid firmware version 
                "V4.04(6757)");	            // maximum valid firmware version 

            Ag8156AInstData.Add("MinWavelength_nm", "1200");    // minimum wavelength
            Ag8156AInstData.Add("MaxWavelength_nm", "1650");    // maximum wavelength
            Ag8156AInstData.Add("MaxAttenuation_dB", "60");     // maximum attenuation

            ValidHardwareData.Add("Ag8156A", Ag8156AInstData);

            // Configure valid chassis information
            // Add Chassis_Ag816x chassis details
            InstrumentDataRecord Ag816xChassisData = new InstrumentDataRecord(
                "Chassis_Ag816x",				// chassis driver name  
                "0",							// minimum valid chassis driver version  
                "1.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag816x", Ag816xChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ag816x)base.InstrumentChassis;

            // Generate the common command stem that most commands use
            this.commandIdn = ":SLOT" + Slot + ":IDN?";
            this.commandAttStem = "INP" + Slot + ":ATT";
            this.commandWavStem = "INP" + Slot + ":WAV";
            this.commandOutpStem = "OUTP" + Slot + ":STAT";
            this.commandOffsStem = "INP" + Slot + ":OFFS";

            // default to safe mode
            this.safeMode = true;

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the chassis.
            get
            {
                // Read the chassis ID string
                string idn = this.instrumentChassis.Query_Unchecked(commandIdn, null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware details.
            get
            {
                // Read the instrument ID string and split the comma seperated fields
                string[] idn = this.instrumentChassis.Query_Unchecked(commandIdn, null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST, and a cls
            chassisWrite("*RST");
            chassisWrite("*CLS");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    /* Nothing to do. */
                }
            }
        }
        #endregion

        #region Optical Attenuator functions

        /// <summary>
        /// Set/Return attenuation level (in dB)
        /// </summary>
        public override double Attenuation_dB
        {
            get
            {
                // Read the current attenuation level (dB)
                string cmd = commandAttStem + "?";
                string resp = chassisQuery(cmd);

                double result = getDblFromString(cmd, resp);
                return result;
            }
            set
            {
                // Set the attenuation level (dB)
                string cmd = commandAttStem + (char)32 + value;
                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Set/Return wavelength (in nm)
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                // Read the current wavelength setting (nm)
                string cmd = commandWavStem + "?";
                string resp = chassisQuery(cmd);

                double result = getDblFromString(cmd, resp);
                return result * mult_m_to_nm;
            }
            set
            {
                // Set the wavelength (nm)
                string cmd = commandWavStem + (char)32 + (value / mult_m_to_nm) + " m";
                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Set/Return output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Read the current output state
                bool result;
                string cmd = commandOutpStem + "?";
                string resp = chassisQuery(cmd);
                if (resp == "1")
                {
                    result = true;
                }
                else if (resp == "0")
                {
                    result = false;
                }
                else
                {
                    throw new InstrumentException("Invalid response to '" + cmd + "': " + resp);
                }
                return result;
            }
            set
            {
                //Set the output state
                string cmd;

                if (value)
                {
                    cmd = commandOutpStem + (char)32 + "ON";
                }
                else
                {
                    cmd = commandOutpStem + (char)32 + "OFF";
                }

                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// Set/Return calibration factor (in dB)
        /// </summary>
        public override double CalibrationFactor_dB
        {
            get
            {
                // Read the current calibration factor (dB)
                string cmd = commandOffsStem + "?";
                string resp = chassisQuery(cmd);

                double result = getDblFromString(cmd, resp);
                return result;
            }
            set
            {
                // Set the calibration factor (dB)
                string cmd = commandOffsStem + (char)32 + value;
                chassisWrite(cmd);
            }
        }

        #endregion

        #region Ag8156A Optical Attenuator Specific functions

        /// <summary>
        /// SafeMode flag. If true, instrument will always wait for completion after every command and check
        /// the error registers. If false it will do neither.
        /// </summary>
        public bool SafeMode
        {
            get
            {
                return this.safeMode;
            }
            set
            {
                safeMode = value;
            }
        }
        #endregion

        #region Helper functions

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            if (safeMode)
            {
                this.instrumentChassis.Write(command, this, true, writeMaxTries, writeDelay_ms);
            }
            else
            {
                // if not in safe mode commands are async and no error checking
                this.instrumentChassis.Write(command, this, false, writeMaxTries, writeDelay_ms);
            }
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            if (safeMode)
            {
                return this.instrumentChassis.Query(command, this);
            }
            else
            {
                // if not in safe mode, don't check for errors
                return this.instrumentChassis.Query(command, this, false);
                //return this.instrumentChassis.Query_Unchecked(command, null);
            }
        }

        /// <summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// </summary>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag816x instrumentChassis;

        private bool safeMode;

        // Common command stems for most commands
        private string commandIdn;
        private string commandAttStem;
        private string commandWavStem;
        private string commandOutpStem;
        private string commandOffsStem;

        // Common sleep constants.
        private const int writeMaxTries = 20;
        private const int writeDelay_ms = 100;

        // Unit convertion constants.
        private const double mult_m_to_nm = 1000000000; /* 1E9 */

        #endregion
    }
}
