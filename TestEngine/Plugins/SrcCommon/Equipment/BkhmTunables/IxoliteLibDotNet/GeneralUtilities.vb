Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module GeneralUtilities
	
	
	Public Const SPEED_OF_LIGHT As Double = 299792.458
	Public Const PI As Double = 3.14159265358979 ' Only used in the matrix maths module.
	
	
	Public Structure LARGE_INTEGER ' LARGE_INTEGER structure is for a 64 bit Long Int
		Dim LowPart As Integer
		Dim HighPart As Integer
	End Structure
	
	Private Const CTRL_DEFS_KEY As String = "ctrl_defs_filename"
	Private Const MAX_CTRL_DEFS As Short = 20
	
	Private timerFreq As Decimal ' High-performance timer frequency.
	Private timerPeriod As Double ' In microseconds.
	Private timerOverhead As Double ' In microseconds.
	
	' The following declarations (and subsequent calls) have been changed on the basis
	' of the Microsoft Knowledge Base Article 172338 (JC 01-09-04 v2.1.8).
	'Private Declare Function QueryPerformanceFrequency Lib "kernel32.dll" (lpFrequency As LARGE_INTEGER) As Long
	'Private Declare Function QueryPerformanceCounter Lib "kernel32.dll" (lpPerformanceCount As LARGE_INTEGER) As Long
    Private Declare Function QueryPerformanceFrequency Lib "kernel32.dll" (ByRef lpFrequency As Long) As Boolean
	Private Declare Function QueryPerformanceCounter Lib "kernel32.dll" (ByRef lpPerformanceCount As Long) As Boolean
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Private Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="FAE78A8D-8978-4FD4-8208-5B7324A8F795"'
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Byte, ByRef pSrc As Integer, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Byte, ByRef pSrc As Short, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Byte, ByRef pSrc As Single, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Byte, ByVal pSrc As String, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Integer, ByRef pSrc As Byte, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Short, ByRef pSrc As Byte, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByRef pDest As Single, ByRef pSrc As Byte, ByVal ByteLen As Integer)
    Private Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (ByVal pDest As String, ByRef pSrc As Byte, ByVal ByteLen As Integer)
	Private Declare Function apiGetComputerName Lib "kernel32"  Alias "GetComputerNameA"(ByVal lpBuffer As String, ByRef nSize As Integer) As Integer
	Private Declare Function apiGetUserName Lib "advapi32"  Alias "GetUserNameA"(ByVal lpBuffer As String, ByRef nSize As Integer) As Integer
	Private Declare Function GetPrivateProfileSection Lib "kernel32"  Alias "GetPrivateProfileSectionA"(ByVal lpApplicationName As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
	Private Declare Function GetCurrentDirectory Lib "kernel32"  Alias "GetCurrentDirectoryA"(ByVal nBufferLength As Integer, ByVal lpBuffer As String) As Integer
	Private Declare Function sndPlaySound Lib "winmm.dll"  Alias "sndPlaySoundA"(ByVal lpszSoundName As String, ByVal uFlags As Integer) As Integer
	
	
	
	
	Public Sub DisplayTimeToComplete(ByRef chan As Short, ByRef startchan As Short, ByRef endchan As Short, ByRef lblname As System.Windows.Forms.Label)
		Static firstchanchange As Double
		Static thischanchange As Double
		Static runningaverage As Double
		Dim timeleft As Double
		Dim hh As Short
		Dim mm As Short
		Dim ss As Short
		Dim strtimeleft As String
		
		On Error GoTo ErrorHandler
		
		If chan = startchan Then
			firstchanchange = VB.Timer()
			thischanchange = firstchanchange
			runningaverage = 0
			lblname.Visible = False
		Else
			lblname.Visible = True
			thischanchange = VB.Timer()
			runningaverage = (thischanchange - firstchanchange) / (chan - startchan)
			timeleft = runningaverage * (endchan - chan + 1)
			hh = timeleft \ 3600
			mm = (timeleft - hh * 3600) \ 60
			ss = CShort(timeleft - hh * 3600 - mm * 60)
			strtimeleft = VB6.Format(hh, "#00") & ":" & VB6.Format(mm, "#00") & ":" & VB6.Format(ss, "#00")
			lblname.Text = "Time To Completion = " & strtimeleft
		End If
		Exit Sub
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "DisplayTimeToComplete")
		lblname.Text = "Time To Completion - ERROR"
		firstchanchange = VB.Timer()
		thischanchange = firstchanchange
		runningaverage = 0
	End Sub
	
	Public Function ChannelIsInBand(ByRef chan As Short) As String
		' Changed in v3.0.8 to use the full ITU band range as specified in MSA v4.1
		If (chan >= MIN_ITU_CBAND_CHANNEL) And (chan <= MAX_ITU_CBAND_CHANNEL) Then
			ChannelIsInBand = CBAND
		Else
			If (chan >= MIN_ITU_LBAND_CHANNEL) And (chan <= MAX_ITU_LBAND_CHANNEL) Then
				ChannelIsInBand = LBAND
			Else
				ChannelIsInBand = BANDERROR
			End If
		End If
	End Function
	
	
	
	Public Sub PlayItSam(ByVal fname As String)
		Dim pname As String
		pname = My.Application.Info.DirectoryPath & "\" & fname
		If FileExists(pname) Then sndPlaySound(pname, &H20001)
	End Sub
	
	
	
	Public Sub LongBeep()
		Dim ii As Object
		For ii = 1 To 3
			Beep()
			msDelay((100))
		Next ii
	End Sub
	
	
	
	'UPGRADE_ISSUE: VB.Shape type was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
	Public Sub LightOff(ByRef shp As Object)
		'UPGRADE_ISSUE: Shape property shp.FillColor was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		shp.FillColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.SystemColors.Control)
		'UPGRADE_ISSUE: Shape property shp.BorderStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
		shp.BorderStyle = System.Windows.Forms.BorderStyle.None
	End Sub
	
	
	
	
    ''UPGRADE_ISSUE: VB.Shape type was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
    'Public Sub LightOn(ByRef shp As Object, ByRef colour As Integer)
    '	'UPGRADE_ISSUE: Shape property shp.FillColor was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '	shp.FillColor = colour
    '	'UPGRADE_ISSUE: Constant vbSolid was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
    '	'UPGRADE_ISSUE: Shape property shp.BorderStyle was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '	shp.BorderStyle = vbSolid
    'End Sub
	
	
	
	
	Public Function GetPCName() As String
		Dim sResult As String
		sResult = Space(255)
		Call apiGetComputerName(sResult, 255)
		GetPCName = Left(sResult, InStr(sResult, Chr(0)) - 1) ' Strip trailing nulls.
	End Function
	
	
	
	
	Public Function GetUserName() As String
		Dim sResult As String
		sResult = Space(255)
		Call apiGetUserName(sResult, 255)
		GetUserName = Left(sResult, InStr(sResult, Chr(0)) - 1) ' Strip trailing nulls.
	End Function
	
	
	
	
	Public Function CurrentDirectory() As String
		Dim Length As Integer
		CurrentDirectory = Space(128)
		Length = GetCurrentDirectory(128, CurrentDirectory)
		CurrentDirectory = Left(CurrentDirectory, Length)
	End Function
	
	
	
	Public Function FileExists(ByRef pname As String) As Boolean
		Dim reply As String
		On Error GoTo Oops
		'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
		reply = Dir(pname)
		If reply = "" Then FileExists = False Else FileExists = True
		Exit Function
Oops: 
		FileExists = False
	End Function
	
	
	
	Public Function FMod(ByRef v1 As Double, ByRef v2 As Double) As Double
		' Note: must use "Int" not "CInt" becasue "CInt" rounds.
		FMod = v1 - v2 * Int(v1 / v2)
	End Function
	
	
	
	
	Public Function GetIniSection(ByRef sSection As String, ByRef sKeyList() As String, ByRef iCount As Short) As String
		' Reads a data item from the .ini file. Note that the .ini filename must be the same as the
		' application name. Comments can be included in the .ini file using a semicolon.
		Dim sIniData As String
		Dim iReturn As Short
		Dim sIniFilename As String
		
		On Error GoTo IniError
		
		'UPGRADE_WARNING: App property App.EXEName has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		sIniFilename = LCase(My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".ini")
		sIniData = Space(1024)
		iReturn = GetPrivateProfileSection(sSection, sIniData, Len(sIniData), sIniFilename)
		If iReturn = 0 Then GoTo IniError
		
		' Strip the linefeed.
		sIniData = Left(sIniData, iReturn)
		
		' Check for and strip trailing comments.
		iReturn = InStr(1, sIniData, ";", CompareMethod.Text)
		If iReturn > 0 Then
			sIniData = Trim(Left(sIniData, iReturn - 1))
		Else
			sIniData = Trim(sIniData)
		End If
		
		' Replace <CR>, <LF> and null characters with a comma then separate into tokens.
		' These are of the form "a=b" and are loaded into the array "sKeyList".
		sIniData = Replace(sIniData, vbCr, ",")
		sIniData = Replace(sIniData, vbLf, ",")
		sIniData = Replace(sIniData, Chr(0), ",")
		Call Tokenize(sIniData, sKeyList, iCount)
		If iCount <= 0 Then
			GoTo IniError
		Else
			GetIniSection = sIniData
		End If
		Exit Function
		
IniError: 
		GetIniSection = ""
	End Function
	
	
	
	
	Public Function GetIniData(ByRef sSection As String, ByRef sKey As String) As String
		' Reads a data item from the .ini file. Note that the .ini filename must be
		' the same as the application name. Comments can be included in the .ini
		' file using a semicolon.
		Dim sIniData As String
		Dim iReturn As Short
		Dim sIniFilename As String
		
		On Error GoTo IniError
		'UPGRADE_WARNING: App property App.EXEName has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		sIniFilename = LCase(My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".ini")
		sIniData = Space(128)
		iReturn = GetPrivateProfileString(sSection, sKey, "", sIniData, Len(sIniData), sIniFilename)
		If iReturn = 0 Then GoTo IniError
		sIniData = Left(sIniData, iReturn) ' strip the linefeed
		
		' Check for trailing comments.
		iReturn = InStr(1, sIniData, ";", CompareMethod.Text)
		If iReturn > 0 Then
			GetIniData = Trim(NoTabs(Left(sIniData, iReturn - 1)))
		Else
			GetIniData = Trim(NoTabs(sIniData))
		End If
		Exit Function
		
IniError: 
		GetIniData = ""
	End Function
	
	
	
	
	Public Sub SetIniData(ByRef sSection As String, ByRef strKey As String, ByRef strvalue As String)
		Dim sIniFilename As String
		'UPGRADE_WARNING: App property App.EXEName has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		sIniFilename = LCase(My.Application.Info.DirectoryPath & "\" & My.Application.Info.AssemblyName & ".ini")
		Call WritePrivateProfileString(sSection, strKey, strvalue, sIniFilename)
	End Sub
	
	
	
	
	Public Sub SaveWindowPosition(ByRef fname As String, ByRef ftop As String, ByRef fleft As String)
		' Saves the top and left positions to the registry, ensuring thet they do not go
		' off-screen top or left.
		If CDbl(fleft) < 0 Then
			SaveSetting(My.Application.Info.Title, "Settings", fname & ".Left", "0")
		Else
			SaveSetting(My.Application.Info.Title, "Settings", fname & ".Left", fleft)
		End If
		If CDbl(ftop) < 0 Then
			SaveSetting(My.Application.Info.Title, "Settings", fname & ".Top", "0")
		Else
			SaveSetting(My.Application.Info.Title, "Settings", fname & ".Top", ftop)
		End If
	End Sub
	
	
	
	
	Public Function ConvBytesToInt(ByRef rcv() As Byte) As Short
		' Converts 2 bytes to an integer value.
		Dim intdata As Short
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		temp(1) = rcv(2)
		temp(2) = rcv(1)
		CopyMemory(intdata, temp(1), 2)
		ConvBytesToInt = intdata
	End Function
	
	
	
	Public Function Conv2BytesToLong(ByRef rcv() As Byte) As Integer
		' Converts 4 bytes to a long value.
		Dim lngdata As Integer
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		temp(1) = rcv(2)
		temp(2) = rcv(1)
		CopyMemory(lngdata, temp(1), 2)
		Conv2BytesToLong = lngdata
	End Function
	
	
	
	
	Public Function Conv3BytesToLong(ByRef rcv() As Byte) As Integer
		' Converts 3 bytes to a long value.
		' This is rarely (ever?) used. Might use a "3 byte" message as a special case
		' if required later.
		Dim lngdata As Integer
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(4) As Byte
		temp(1) = rcv(3)
		temp(2) = rcv(2)
		temp(3) = rcv(1)
		If temp(3) And &H80s Then ' check state of upper bit of 24bit MSB
			temp(4) = &HFFs ' if set, sign is negative, put 1's into 32bit MSB
		Else
			temp(4) = &H0s ' if clear sign is positive, put 0's into 32bit MSB
		End If
		CopyMemory(lngdata, temp(1), 4) ' place 4 bytes from array into long variable
		Conv3BytesToLong = lngdata
	End Function
	
	
	
	Public Function Conv4BytesToLong(ByRef rcv() As Byte) As Integer
		' Converts 4 bytes to a long value.
		Dim lngdata As Integer
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(4) As Byte
		temp(1) = rcv(4)
		temp(2) = rcv(3)
		temp(3) = rcv(2)
		temp(4) = rcv(1)
		CopyMemory(lngdata, temp(1), 4)
		Conv4BytesToLong = lngdata
	End Function
	
	
	
	Public Function Conv4BytesToSingle(ByRef rcv() As Byte) As Single
		' Converts 4 bytes to a single value. The MSB is expected to be in array byte 1.
		Dim sngdata As Single
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(4) As Byte
		temp(1) = rcv(4)
		temp(2) = rcv(3)
		temp(3) = rcv(2)
		temp(4) = rcv(1)
		CopyMemory(sngdata, temp(1), 4)
		Conv4BytesToSingle = sngdata
	End Function
	
	
	
	
	Public Sub ConvIntToBytes(ByRef bytArray() As Byte, ByRef intdata As Short)
		' Converts an integer value into 2 bytes with the MSB in element 1
		' of the output array.
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(2) As Byte
		CopyMemory(temp(1), intdata, 2)
		bytArray(1) = temp(2)
		bytArray(2) = temp(1)
	End Sub
	
	
	
	
	Public Sub ConvLongToBytes(ByRef snd() As Byte, ByRef lngdata As Integer)
		' Converts a long data value to 4 bytes with the MSB in element 1
		' of the output array.
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
        '		Dim temp(4) As Byte
        '		CopyMemory(temp(1), lngdata, 4)
        '		snd(1) = temp(4)
        '		snd(2) = temp(3)
        '		snd(3) = temp(2)
        '		snd(4) = temp(1)
        Dim temp() As Byte
        temp = System.BitConverter.GetBytes(lngdata)

        snd(1) = temp(3)
        snd(2) = temp(2)
        snd(3) = temp(1)
        snd(4) = temp(0)

    End Sub
	
	
	
	
	Public Sub ConvSingleToBytes(ByRef snd() As Byte, ByRef sngdata As Single)
		' Converts a single data value to 4 bytes with the MSB in element 1
		' of the output array.
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(4) As Byte
		CopyMemory(temp(1), sngdata, 4)
		snd(1) = temp(4)
		snd(2) = temp(3)
		snd(3) = temp(2)
		snd(4) = temp(1)
	End Sub
	
	
	
	
	Public Function ConvHexToLong(ByRef hexstring As String) As Integer
		Dim hstr As String
		Dim lenf As Short
		Dim indx As Short
		Dim result As Integer
		Dim thisint As Short
		Dim thisstr As String
		
		On Error GoTo ErrorHandler
		
		lenf = Len(hexstring)
		hstr = LCase(hexstring)
		
		If (Mid(hstr, 1, 2) = "0x") Or (Mid(hstr, 1, 2) = "&h") Then
			hstr = Mid(hstr, 3, lenf)
			lenf = lenf - 2
		End If
		
		For indx = 1 To lenf
			thisstr = Mid(hstr, indx, 1)
			Select Case thisstr
				Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" : thisint = CShort(thisstr)
				Case "a" : thisint = 10
				Case "b" : thisint = 11
				Case "c" : thisint = 12
				Case "d" : thisint = 13
				Case "e" : thisint = 14
				Case "f" : thisint = 15
			End Select
			result = result * 16 + thisint
		Next indx
		ConvHexToLong = result
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ConvHexToLong")
		ConvHexToLong = GENERAL_ERROR_CODE
	End Function
	
	
	
	
	Public Function ConvLongToHex(ByVal lngval As Integer) As String
		Dim result As String
		Dim dd As Byte
		result = ""
		dd = lngval And 255
		If dd < 16 Then result = "0" & Hex(dd) Else result = Hex(dd)
		dd = (lngval \ 256) And 255
		If dd < 16 Then result = "0" & Hex(dd) & result Else result = Hex(dd) & result
		ConvLongToHex = result
	End Function
	
	
	
	
	Public Function Log10(ByRef dblval As Double) As Double
		Log10 = System.Math.Log(dblval) / System.Math.Log(10)
	End Function
	
	
	
	
	Public Function THz2ITUChannel(ByRef freq As Double) As Short
		' Converts a frequency in THz to an ITU channel number. The formula for
		' this is defined in the MSA v4. Errors return a channel number of 0.
		' The L band has been defined (locally) as channels 105-192, 190.9-186.55THz.
		Dim fr As Double
		On Error GoTo Overflow
		If freq <= 0 Then
			THz2ITUChannel = 0
		Else
			fr = (196.1 - freq) / 0.05 + 1#
			THz2ITUChannel = CShort(fr)
		End If
		Exit Function
Overflow: 
		THz2ITUChannel = 0
	End Function
	
	
	
	Public Function ITUChannel2THz(ByRef chan As Double) As Double
		' Reverse of THz2ITUChannel.
		ITUChannel2THz = 196.1 - 0.05 * (chan - 1#)
	End Function
	
	
	
	
    Public Sub usDelay(ByRef usTime As Integer)
        ' This function provides very high resolution delays in microseconds.
        ' This is based on the High Performance Timer built into Pentium class CPU's
        ' and is accesible through the Win32 API.
        ' Mike Rigby-Jones 8/2/2001
        ' usTime is in microseconds.
        ' Changed as per Microsoft KBA 172338 (JC 01-09-04 v2.1.8)
        Dim countStart As Decimal
        Dim countCurrent As Decimal
        Dim countEnd As Decimal
        Dim retval As Boolean
        Dim secTime As Double

        ' Convert microseconds delay to seconds. Get current value of timer.
        ' Calculate what timer will be in usTime microseconds from now.
        ' Then loop until timer reaches this value.
        secTime = usTime / 1000000.0#
        retval = QueryPerformanceCounter(countStart)
        countEnd = countStart + ((timerFreq * secTime) / 10000)
        Do
            retval = QueryPerformanceCounter(countCurrent)
        Loop While countCurrent < countEnd
    End Sub
	
	
	
	
	Public Sub msDelay(ByRef msd As Short)
        ' Delay for specified number of milliseconds.  
        System.Threading.Thread.Sleep(TimeSpan.FromMilliseconds(msd))
	End Sub
	
	
	
	
	Public Sub sDelay(ByRef secs As Double)
		' Delay for the specified number of seconds. This doesn't need to be so
		' accurate so we use the "Timer" function which gives seconds since
        ' midnight to 2 places. At midnight the timer goes from 86399.99 to zero.
        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(secs))
    End Sub
	
	
	
	
	Public Sub InitTimer()
		' This function checks the frequency of the Pentium High Performance Timer.
		' The frequency is used to calculate accurate delays.
		' Mike Rigby-Jones 8/2/2001
		' Changed as per MKBA 172338 (JC 01-09-04 v2.1.8)
		'Dim Buffer As LARGE_INTEGER  ' buffer to hold timer count
		'Dim Buffer2 As LARGE_INTEGER  ' buffer to hold timer count
		Dim retval As Boolean
		Dim Buffer As Decimal
		Dim t1 As Decimal
		Dim t2 As Decimal
		
		On Error GoTo ErrorHandler
		
		' Get the frequency of the high-performance timer and, in the process,
		' determine if it actually exists or not.
		If QueryPerformanceFrequency(Buffer) Then
			timerFreq = 10000 * Buffer ' 10000 times too small because of datatype.
			timerPeriod = 1# / timerFreq ' Microseconds
			retval = QueryPerformanceCounter(t1)
			retval = QueryPerformanceCounter(t2)
			timerOverhead = 10000 * (t2 - t1) * timerPeriod ' Microseconds
		Else
			MsgBox("This computer does not have a high-performance timer.")
			' NEED TO ADD ERROR HANDLING!! (what to do if no timer?)
			timerFreq = 1 ' Not zero because we might divide by it.
			timerOverhead = 0
			timerPeriod = 1
			Exit Sub
		End If
		Exit Sub
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "InitTimer")
	End Sub
	
	
	
	
	
    'Public Function LI2Curr(ByRef li As LARGE_INTEGER) As Decimal
    '	' This function converts the data in a LARGE_INTEGER structure into
    '	' the form of VB's 64-bit Currency data type.
    '	Dim temp As Decimal
    '	'UPGRADE_WARNING: Couldn't resolve default property of object li. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	CopyMemory(temp, li, 8)
    '	LI2Curr = temp * 1000
    'End Function
	
	
	
	
	
	Public Sub GetNextNumber(ByRef junk As String, ByRef newjunk As String, ByRef Num As String)
		' This routine is required for decoding the text strings returned from the
		' Santur modules. It is a lot more robust than any other technique but may
		' be a little slower.  It just plonks along the string looking at individual
		' characters until it finds something numeric, then continues until it finds
		' something non-numeric. This does NOT handle Scientific Notation.
		Dim indx As Short
		Dim start As Short
		Dim ch As String
		Dim lenf As Short
		
		On Error GoTo ErrorHandler
		
		lenf = Len(junk)
		indx = 0
		newjunk = junk
		Num = ""
		Do 
			indx = indx + 1
			If indx > lenf Then Exit Sub
			ch = Mid(junk, indx, 1)
		Loop While (ch <> ".") And (ch <> "+") And (ch <> "-") And (Not IsNumeric(ch))
		
		start = indx
		Do 
			indx = indx + 1
			If indx > lenf Then
				' Reached the end of the string.
				Num = Mid(junk, start)
				newjunk = ""
				Exit Do
			End If
			ch = Mid(junk, indx, 1)
		Loop While (ch = ".") Or (ch = "+") Or (ch = "-") Or (IsNumeric(ch))
		
		Num = Mid(junk, start, indx - start)
		' Check that we don't just have symbols.
		If Num <> "" Then
			If Not IsNumeric(Num) Then Num = ""
		End If
		newjunk = Mid(junk, indx)
		Exit Sub
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetNextNumber")
		Num = ""
	End Sub
	
	
	
	
	
	Public Sub GetNextExpNumber(ByRef junk As String, ByRef Num As String)
		' Reads the input string character by character, expecting only characters to
		' be found in an exponential (scientific notation) number.
		Dim indx As Short
		Dim start As Short
		Dim ch As String
		Dim lenf As Short
		Dim foundE As Boolean
		
		On Error GoTo ErrorHandler
		
		lenf = Len(junk)
		indx = 0
		Num = ""
		foundE = False
		' Scan along the line until we find the first number or required symbol.
		Do 
			indx = indx + 1
			If indx > lenf Then Exit Sub
			ch = Mid(junk, indx, 1)
		Loop While (ch <> ".") And (ch <> "+") And (ch <> "-") And (ch <> "E") And (Not IsNumeric(ch))
		
		' Should now be at the start of a number. Pick out the characters until finding one
		' which is not numeric or a required symbol, or until the end of the line.
		start = indx
		Do 
			indx = indx + 1
			If indx > lenf Then
				' Reached the end of the string.
				Num = Mid(junk, start)
				Exit Do
			End If
			ch = Mid(junk, indx, 1)
			
			' The HP seems to put another E at the end of the number, so we need
			' to get rid of it. Change it to something else to force termination.
			If ch = "E" Then
				If foundE Then
					ch = "Z"
				Else
					foundE = True
				End If
			End If
		Loop While (ch = ".") Or (ch = "+") Or (ch = "-") Or (ch = "E") Or (IsNumeric(ch))
		
		Num = Mid(junk, start, indx - start)
		' Check that we don't just have symbols.
		If Num <> "" Then
			If Not IsNumeric(Num) Then Num = ""
		End If
		Exit Sub
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetNextExpNumber")
		Num = ""
	End Sub
	
	
	
	
	Public Sub GetNumbersFromLine(ByRef line As String, ByRef number() As String, ByRef Count As Short)
		' Gets all numbers from a line and dumps them into a string array. The number of values
		' found is returned in parameter "count".
		Dim lineA As String
        Dim lineB As String = ""
        Dim Num As String = ""
		Dim ind1 As Short
		Dim ind2 As Short
		
		ind1 = LBound(number)
		ind2 = UBound(number)
		Count = 0
		lineA = line
		
		Do 
			Call GetNextNumber(lineA, lineB, Num)
			If Num <> "" Then
				number(ind1 + Count) = Num
				Count = Count + 1
				lineA = lineB
			End If
		Loop While (Num <> "") And ((ind1 + Count) <= ind2)
	End Sub
	
	
	
	
	
	Public Function CFormat(ByRef inp As Object, ByRef size As Short, ByRef fmt As String) As String
		' Output "inp" according to the format "fmt" right justified in a field "size".
		'UPGRADE_WARNING: Couldn't resolve default property of object inp. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		CFormat = Right("                              " & VB6.Format(CStr(inp), fmt), size)
	End Function
	
	
	
	Public Function NoTabs(ByRef line As String) As String
		' Replaces all tabs in a line with spaces.
		Dim LineLen As Short
		Dim indx As Short
		Dim Line2 As String
		Dim nextch As String
		
		On Error GoTo ErrorHandler
		LineLen = Len(line)
		Line2 = ""
		
		For indx = 1 To LineLen
			nextch = Mid(line, indx, 1)
			If (Asc(nextch) = 9) Then
				' Replace tab with a space.
				Line2 = Line2 & " "
			Else
				Line2 = Line2 & nextch
			End If
		Next indx
		
		NoTabs = Line2
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "NoTabs")
	End Function
	
	
	
	
	
	Public Sub Tokenize(ByRef line As String, ByRef tokens() As String, ByRef TokenIndex As Short)
		' Remove extraneous spaces and tabs then break the line into "A=B" type tokens.
		' Tokens are separated by commas.  TokenIndex returns the number of tokens found.
		' Moved out of the Setup form because it found a use when reading an ini section.
		Dim strpos As Short
		Dim SepChar As New VB6.FixedLengthString(1)
		Dim StartPos As Short
		Dim LastPos As Short
		Dim LineLen As Short
		Dim indx As Short
		Dim Line2 As String
		Dim nextch As String
		
		On Error GoTo ErrorHandler
		SepChar.Value = ","
		StartPos = 1
		LastPos = 1
		TokenIndex = 0
		LineLen = Len(line)
		If LineLen = 0 Then Exit Sub
		Line2 = ""
		
		' Get rid of spaces and tabs.
		For indx = 1 To LineLen
			nextch = Mid(line, indx, 1)
			If (nextch = " ") Or (Asc(nextch) = 9) Then
				' Ignore it.
			Else
				Line2 = Line2 & nextch
			End If
		Next indx
		
		' Replace empty tokens and any terminators at the end of the line.
		line = Replace(Line2, ",,", ",")
		LineLen = Len(line)
		If LineLen = 0 Then Exit Sub
		If Mid(line, LineLen, 1) = SepChar.Value Then
			LineLen = LineLen - 1
			line = Mid(line, 1, LineLen)
		End If
		
		' Loop while a separator exists between the current position and the end
		' of the line.
		TokenIndex = LBound(tokens)
		strpos = InStr(StartPos, line, SepChar.Value, CompareMethod.Text)
		Do While (strpos > 0) And (strpos < LineLen)
			If TokenIndex > UBound(tokens) Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logMESSAGE("Too many tokens on line (" & line & ")")
				GoTo Aborted
			End If
			tokens(TokenIndex) = Mid(line, StartPos, strpos - StartPos)
			TokenIndex = TokenIndex + 1
			StartPos = strpos + 1
			strpos = InStr(StartPos, line, SepChar.Value, CompareMethod.Text)
		Loop 
		If strpos < LineLen Then
			' There is another token at the end of the line without a terminating
			' separator.
			tokens(TokenIndex) = Mid(line, StartPos)
		Else
			TokenIndex = TokenIndex - 1
		End If
		Exit Sub
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "Tokenize: " & line)
Aborted: 
		TokenIndex = 0
	End Sub
	
	
	
	
	Public Sub DecodeCmd(ByRef cmdstr As String, ByRef param() As Byte, ByRef plen As Byte, ByRef cmd As Byte, ByRef snd() As Byte, ByRef nextpos As Byte)
		' Removed from WBTTSetup in v2.2.18 because it is now needed for the Modulator form.
		' This procedure decodes the command string "cmdstr" into the output array "send",
		' then copies "plen" parameter bytes from array "param" into the output array "send".
		' The next free position in the output array is returned as "nextpos".
		' "cmd" is the first byte of the command string, expected to be the relevant MSA
		' command to the DSP, and is merely output for the convenience of the caller.
		' Also note that this byte doesn't go into the "send" array.
		Dim cnt As Byte
		
		On Error GoTo ErrorHandler
		
		If cmdstr <> "" Then
			cmd = CByte("&H" & Mid(cmdstr, 1, 2))
			nextpos = Len(cmdstr) / 2
			For cnt = 2 To nextpos
				snd(cnt - 1) = CByte("&H" & Mid(cmdstr, 2 * cnt - 1, 2))
			Next cnt
		Else
			cmd = 0
			nextpos = 1
		End If
		
		For cnt = 1 To plen
			snd(nextpos + cnt - 1) = param(cnt)
		Next cnt
		nextpos = nextpos + plen
		Exit Sub
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "DecodeCmd " & cmdstr)
	End Sub
	
	
	
	
	Public Function DateTimeString() As String
		Dim dt As String
		dt = CStr(Now)
		DateTimeString = Year(CDate(dt)) & VB6.Format(Month(CDate(dt)), "00") & VB6.Format(VB.Day(CDate(dt)), "00") & VB6.Format(Hour(CDate(dt)), "00") & VB6.Format(Minute(CDate(dt)), "00") & VB6.Format(Second(CDate(dt)), "00")
	End Function
	
	
	
	
	
	Public Function OpenDumpFile(ByRef filename As String, Optional ByRef comments As String = "") As Short
		' General purpose routine to open a text file and put out a header.
		' The file is always opened for Append mode.
		' If only a filename is supplied, not a full pathname, the file is opened in the local directory.
		Dim fh As Short
		Dim hw As String
		Dim sw As String
		Dim pc As String
		Dim un As String
		Dim sn As String
		Dim foreignmodule As Boolean
		
		On Error GoTo ErrorHandler
		
		fh = FreeFile
		If filename = "" Then
			FileOpen(fh, "data_dump.txt", OpenMode.Append)
		Else
			If (InStr(1, filename, "\", CompareMethod.Text) > 0) Or (InStr(1, filename, "/", CompareMethod.Text) > 0) Then
				' Assume fully qualified pathname is provided.
				FileOpen(fh, filename, OpenMode.Append)
			Else
				' Local filename only provided so we supply the pathname.
				FileOpen(fh, My.Application.Info.DirectoryPath & "\" & filename, OpenMode.Append)
			End If
		End If
		
		pc = GetPCName
		un = GetUserName
		
		PrintLine(fh, "GUI:           v" & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision & "   " & My.Application.Info.Title)
		PrintLine(fh, "Machine:       ", pc)
		PrintLine(fh, "User:          ", un)
		PrintLine(fh, "Date:          ", Now)
		PrintLine(fh)
		'Print #fh, "Unit Serial:   ", sn
		'Print #fh, "H/W Revision:  ", hw
		'Print #fh, "F/W Revision:  ", sw
		'Print #fh,
		If comments <> "" Then
			PrintLine(fh, comments)
			PrintLine(fh)
		End If
		
		OpenDumpFile = fh
		Exit Function
		
ErrorHandler: 
		OpenDumpFile = 0
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "OpenDumpFile")
	End Function
	
	
	
	
	
	Public Function IniFileOK() As Boolean
		Dim sIniData As String
		sIniData = GetIniData(INI_COMMS, "this_key_must_exist")
		If Len(sIniData) = 0 Then
			IniFileOK = False
		Else
			IniFileOK = True
		End If
	End Function
	
	
	
	
	Public Function GetGuiPortOptions() As String
		' Expecting to see a string such as "gui_port_options=&H378,&H278,&HCC00" in the ini file.
		GetGuiPortOptions = GetIniData(INI_COMMS, "gui_port_options")
	End Function
	
	
	
	Public Function GetGuiPortAddress() As String
		' Expecting to see a string such as "gui_port_address=&H378" in the ini file.
		GetGuiPortAddress = GetIniData(INI_COMMS, "gui_port_address")
	End Function
	
	
	
	Public Function GetModuleBaseAddress() As String
		' Expecting to see a string such as "module_base_address=&H40" in the ini file.
		GetModuleBaseAddress = GetIniData(INI_COMMS, "module_base_address")
	End Function
	
	
	
	Public Function GetModuleAddress() As String
		' Expecting to see a string such as "module_address=&H40" in the ini file.
		GetModuleAddress = GetIniData(INI_COMMS, "module_address")
	End Function
	
	
	
	Public Function GetInstrumentPortAddress(ByRef addr As String) As Object
		' "addr" is expected to be a string such as "wave_port_address".
		GetInstrumentPortAddress = GetIniData(INI_COMMS, addr)
	End Function
	
	
	Public Function GetCtrlDefsSubdir() As String
		' Gets the folder name for the setup definitions files.
		GetCtrlDefsSubdir = GetIniData(INI_SETUPFORM, "ctrl_defs_subdir")
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logMESSAGE. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logMESSAGE("CtrlDefsSubdir=" & GetCtrlDefsSubdir)
	End Function
	
	
	
	
	Public Sub GetCtrlDefsFileList(ByRef flist() As String)
		' Gets the list of setup files defined in the ini file and returns them in a string array.
		' For example "ctrl_defs_filename1=setup.txt"
		Dim indx As Short
		Dim cnt As Short
		Dim fl As String
		cnt = UBound(flist) - LBound(flist) + 1
		For indx = 1 To cnt
			fl = Trim(GetIniData(INI_SETUPFORM, CTRL_DEFS_KEY & CStr(indx)))
			If fl <> "" Then flist(LBound(flist) + indx - 1) = fl
		Next indx
	End Sub
End Module