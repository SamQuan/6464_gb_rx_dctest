﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.CoRxDCTestCommonData
{
    public class CoRxVOASweepData
    {
        public readonly double VoaVoltage_V;
        public readonly double VoaCurrent_A;
        public readonly double XIPCurrent_A;
        public readonly double XIN_Or_XQ_Current_A;
        public readonly double X_Sum_A;
        public readonly double YIPCurrent_A;
        public readonly double YIN_Or_YQ_Current_A;
        public readonly double Y_Sum_A;
        public readonly double MPDCurrent_A;
        private double mXIPAttenuation_dB;
        private double mYIPAttenuation_dB;
        private double mMPDAttenuation_dB;
        private double mXAttenuation_dB;
        private double mYAttenuation_dB;




        //private double mPDL_db;
        private double mAverageAtteuation_db;

        public CoRxVOASweepData(double voaVolts, double voaCurrent, double xipCurrent, double XinOrXQCurrent, double yipCurrent, double YinOrYqCurrent, double mpdCurrent)
        {
            VoaVoltage_V = voaVolts;
            VoaCurrent_A = voaCurrent;
            XIPCurrent_A = xipCurrent;
            XIN_Or_XQ_Current_A = XinOrXQCurrent;
            X_Sum_A = xipCurrent + XinOrXQCurrent;
            YIPCurrent_A = yipCurrent;
            YIN_Or_YQ_Current_A = YinOrYqCurrent;
            Y_Sum_A = yipCurrent + YinOrYqCurrent;
            MPDCurrent_A = mpdCurrent;

            
        }

        public double PDL_db
        {
            get { return Math.Abs( mXAttenuation_dB - mYAttenuation_dB); }

        }

        public double AverageAttenuation_db
        {
            get { return mAverageAtteuation_db; }
            set { mAverageAtteuation_db = value; }
        }



        public double MPDAttenuation_dB
        {
            get { return mMPDAttenuation_dB; }
            set
            {
                mMPDAttenuation_dB = value;
            }
        }


        public double XIPAttenuation_dB
        {
            get { return mXIPAttenuation_dB;  }
            set
            {
                mXIPAttenuation_dB = value;
            }
        }

        public double YIPAttenuation_dB
        {
            get { return mYIPAttenuation_dB;}
            set
            {
                
                mYIPAttenuation_dB = value;
            }
        }

        public double XAttenuation_dB
        {
            get { return mXAttenuation_dB; }
            set
            {

                mXAttenuation_dB = value;
            }
        }

        public double YAttenuation_dB
        {
            get { return mYAttenuation_dB; }
            set
            {

                mYAttenuation_dB = value;
            }
        }




#region measuredCurrents
        public double VOACurrent_mA
        {
            get
            {
                return 1000 * VoaCurrent_A;
            }
        }

        public double XIPCurrent_mA
        {
            get 
            {
                return 1000 * XIPCurrent_A;
            }
        }


        public double XIN_Or_XQ_Current_mA
        {
            get
            {
                return 1000 * XIN_Or_XQ_Current_A;
            }
        }

        public double YIPCurrent_mA
        {
            get
            {
                return 1000 * YIPCurrent_A;
            }
        }


        public double YIN_Or_YQ_Current_mA
        {
            get
            {
                return 1000 * YIN_Or_YQ_Current_A;
            }
        }
        public double MPDCurrent_mA
        {
            get
            {
                return 1000 * MPDCurrent_A;
            }
        }

        #endregion
    }
}
