<?xml version="1.0"?>
<doc>
    <assembly>
        <name>TestEngine.Framework.Messages</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestEngine.Framework.Messages.MsgContainer">
            <summary>
            This class is used to hold a message, including the sender, recipient
            and payload. It is used to hold a simple asynchronous message and to
            provide a base class for a signal message.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgContainer.#ctor(System.String,System.String,System.Object,System.Object,System.Int64,System.Int64)">
            <summary>
            Cosntructor. Builds a message container.
            </summary>
            <param name="sender">Node name of sender.</param>
            <param name="recipient">Node name of recipient.</param>
            <param name="deliveryTag">Tag object assigned by delivery agent.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Sequence id.</param>
            <param name="respSeq">Responding-to sequence id. (Or zero.)</param>
            <remarks>Called by message centre Send functions only.</remarks>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgContainer.ToString">
            <summary>
            Displays message summary for diagnostics.
            </summary>
            <returns></returns>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.MsgContainer.Sender">
            <summary>
            Gets the sender node name.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.MsgContainer.Recipient">
            <summary>
            Gets the recipient node name.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.MsgContainer.DeliveryTag">
            <summary>
            Gets the tag object for the delivery agent.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.MsgContainer.Payload">
            <summary>
            Gets the message payload.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.MsgContainer.MsgSeq">
            <summary>
            Gets the message sequence number assigned to this message.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.MsgContainer.RespSeq">
            <summary>
            Gets the message sequence number for the message that this message
            is in response to, or zero if this message is not a response.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.SignalMsgContainer">
            <summary>
            Container for a signal message. This inhierits from MsgContainer and
            adds extra information recoding the signal id used.
            </summary>
            <remarks>A new instance of this class will be created for each node
            that subscribes to a signal. The payload reference will be copied to
            each one.</remarks>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.SignalMsgContainer.#ctor(System.String,System.String,System.Object,System.Object,System.Int64,System.String)">
            <summary>
            Constuctor. Builds a signal message container.
            </summary>
            <param name="sender">Node name of sender.</param>
            <param name="recipient">Node name of recipient.</param>
            <param name="deliveryTag">Tag object assigned by delivery agent.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Sequence id.</param>
            <param name="signalId">Signal name.</param>
            <remarks>Called within message centre code only.</remarks>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.SignalMsgContainer.SignalId">
            <summary>
            Gets the signal id name.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.DeliveryException">
            <summary>
            This exception should be thrown by a delivery agent on potentially
            recoverable delivery failure. (For example, queue full.)
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.DeliveryException.#ctor">
            <summary>
            Construct base class with default description.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.DeliveryException.#ctor(System.String)">
            <summary>
            Construct exception with message.
            </summary>
            <param name="message">Message text.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.DeliveryException.#ctor(System.String,System.Exception)">
            <summary>
            Construct exception with message and wrapped inner exception.
            </summary>
            <param name="message">Message text.</param>
            <param name="caughtException">Inner exception.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.IDeliverMsg">
            <summary>
            This interface describes a delivery agent. Any class that implements this
            interface may act as a delivery agent.
            </summary>
            <remarks>Delivery agent call should not block.</remarks>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IDeliverMsg.DeliverMsg(Bookham.TestEngine.Framework.Messages.MsgContainer)">
            <summary>
            Deliver a message to the named recipient in manner specified by
            the specific delivery agent.
            </summary>
            <param name="msgCont">Message container to deliver.</param>
            <remarks>Called by message centre code within the sender's thread.</remarks>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.IGuiProxy">
            <summary>
            Front end interface describing the GuiProxy class in the back end.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IReceiveMsg)">
            <summary>
            Register control as node with message centre.
            </summary>
            <param name="name">New node name.</param>
            <param name="ctl">Control implementing IReceiveMsg.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.RemoveNode(System.String)">
            <summary>
            Remove node from message centre.
            </summary>
            <param name="name">Name of node to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.RegisterRestrictedSignal(System.String,System.String)">
            <summary>
            Register restricted signal.
            </summary>
            <param name="sigId">New signal name.</param>
            <param name="nodeName">Restrict to this node.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.RegisterUnrestrictedSignal(System.String)">
            <summary>
            Register unrestricted signal.
            </summary>
            <param name="sigId">New signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.SubscribeSignal(System.String,System.String)">
            <summary>
            Subscribe named node to signal.
            </summary>
            <param name="nodeName">Node name to subscribe.</param>
            <param name="sigId">Signal name.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.UnsubscribeSignal(System.String,System.String)">
            <summary>
            Unsubscribe named node from signal.
            </summary>
            <param name="nodeName">Unsubscribing node name.</param>
            <param name="sigId">Signal name to unsubscribe from.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.SendAsync(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send a new asynchronous message. (Not a response.)
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Payload object reference.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.SendAsync(System.String,System.String,System.Object,System.Int64,System.Int64)">
            <summary>
            Send an asynchronous message as a response.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Payload object reference.</param>
            <param name="msgSeq">Message sequence number.</param>
            <param name="respSeq">Sequence number fo message being responded to.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IGuiProxy.SendSignal(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Broadcast a signal to all subscribed nodes.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="signalId">Signal name.</param>
            <param name="payload">Payload object reference.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.IMsgCentre">
            <summary>
            Summary description for IMsgCentre.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object)">
            <summary>
            Register a node.
            </summary>
            <param name="nodeName">Name of node to register. '*' characters will be replaced with digits.</param>
            <param name="delivery">Link to delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
            <returns>New node name, including '*' replacement.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object,System.String)">
            <summary>
            Register a node.
            </summary>
            <param name="nodeName">Name of node to register. '*' characters will be replaced with digits.</param>
            <param name="delivery">Link to delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
            <param name="parentNodeName">Node which will contain nodeName's node as stand-in.</param>
            <returns>New node name, including '*' replacement.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.SecureNodeToThread(System.String)">
            <summary>
            Set the named node's thread to the current thread.
            </summary>
            <param name="nodeName">Name of node to secure.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.RemoveNode(System.String)">
            <summary>
            Remove the named node.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.RemoveNode(System.String,System.String)">
            <summary>
            Remove the named node.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
            <param name="parentNodeName">Node which will contain nodeName's node as stand-in.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.RegisterRestrictedSignal(System.String,System.String)">
            <summary>
            Register a signal which may only be sent by the named node.
            </summary>
            <param name="sigId">Name of new signal.</param>
            <param name="nodeName">Name of node who may send signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.RegisterUnrestrictedSignal(System.String)">
            <summary>
            Register a signal which may be sent by any node.
            </summary>
            <param name="sigId">Name of new signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.SubscribeSignal(System.String,System.String)">
            <summary>
            Subscribe node to the named signal.
            </summary>
            <param name="nodeName">Node subscribing...</param>
            <param name="sigId">... to this signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.UnsubscribeSignal(System.String,System.String)">
            <summary>
            Unscribe node from the named signal.
            </summary>
            <param name="nodeName">Node to unsubscribe.</param>
            <param name="sigId">From this signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.SendAsync(System.String,System.String,System.Object,System.Int64,System.Int64)">
            <summary>
            Send response message via delivery agent.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">This message sequence number.</param>
            <param name="respSeq">Sequence number of message being responded to.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.SendAsync(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send new message via delivery agent.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IMsgCentre.SendSignal(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send signal to many nodes via delivery agent.
            </summary>
            <param name="sender">Sender node.</param>
            <param name="signalId">Signal id.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.IReceiveMsg">
            <summary>
            Objects using GuiProxy implement this interface.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.IReceiveMsg.ReceiveMsg(Bookham.TestEngine.Framework.Messages.MsgContainer)">
            <summary>
            Called by delivery agent delegate when a message has arrived for this node.
            </summary>
            <param name="msgCont">The message container.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.ManagedCtlBase">
            <summary>
            Base class for controls managed using the control manager. Developers may use
            "Add Inhierited Control" to make subclasses of this class.
            
            A number of events are raised allowing to the developer to pick up messages from
            the message centre.
            <list type="bullet">
            	<item>MsgReceived, for messages from the worker partner.</item>
            	<item>SignalReceived, for signals.</item>
            	<item>ExtMsgReceived, for messages from everyone else.</item>
            </list>
            
            Other events are raised for dynamic area interactions.
            <list type="bullet">
            	<item>FillDynPage, raised when this control fills a dynamic area page.</item>
            	<item>IntoView, raised when the control is visible to the user.</item>
            	<item>OutOfView, raised when the control is no longer visible.</item>
            	<item>LeaveDynPage, raised when the control is removed.</item>
            </list>
            </summary>
            <remarks>See DD coument for a guise to use of this class.</remarks>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.components">
            <summary> 
            Required designer variable.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.#ctor">
            <summary>
            Base class constructor.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.Dispose(System.Boolean)">
            <summary> 
            Clean up any resources being used.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.InitializeComponent">
            <summary> 
            Required method for Designer support - do not modify 
            the contents of this method with the code editor.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.Initialise(System.String,System.String,Bookham.TestEngine.Framework.Messages.IGuiProxy)">
            <summary>
            Set up node names.
            </summary>
            <param name="ctlNodeName">This control's node name.</param>
            <param name="wrkNodeName">Worker partner's node name.</param>
            <param name="msgs">Message centre proxy for UI thread.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.checkInitialisedAndThrow">
            <summary>
            Check initialised flag, throw if not initialised.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.sendToWorker(System.Object)">
            <summary>
            Send new message to worker thread.
            </summary>
            <param name="payload">Message payload.</param>
            <returns>Sequence number used.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.sendToWorker(System.Object,System.Int64)">
            <summary>
            Send response message to worker thread.
            </summary>
            <param name="payload">Message payload.</param>
            <param name="respSeq">Sequence number of message being responded to.</param>
            <returns>Sequence number used for this message.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.sendToNode(System.Object,System.String)">
            <summary>
            Send new message to any node.
            </summary>
            <param name="payload">Message payload.</param>
            <param name="destNode">Destination node.</param>
            <returns>Sequence number used.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.sendToNode(System.Object,System.String,System.Int64)">
            <summary>
            Send response to any node.
            </summary>
            <param name="payload">Message payload.</param>
            <param name="destNode">Destination node.</param>
            <param name="respSeq">Sequence number fo message being responded to.</param>
            <returns>Sequence number used to send.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.registerRestrictedSignal(System.String)">
            <summary>
            Register a signal, send restricted to this node.
            </summary>
            <param name="signalId">New signal name.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.subscribeToSignal(System.String)">
            <summary>
            Subscribe to a signal.
            </summary>
            <param name="signalId">Signal name.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.unsubscribeToSignal(System.String)">
            <summary>
            Unsubscribe to a signal.
            </summary>
            <param name="signalId">Signal name.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.sendSignal(System.Object,System.String)">
            <summary>
            Send a signal.
            </summary>
            <param name="payload">Signal message payload.</param>
            <param name="signalId">Signal id.</param>
            <returns>Message sequence number used.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ReceiveMsg(Bookham.TestEngine.Framework.Messages.MsgContainer)">
            <summary>
            Receive message and route to specific event or log. To be called by 
            Framework.Messages.GuiProxy only. Implements IReceiveMsg.
            </summary>
            <param name="msgCont">Conatined message.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.RaiseOutOfViewEvent">
            <summary>
            Raise the OutOfView event.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.RaiseIntoViewEvent">
            <summary>
            Raise the IntoView event.
            </summary>
            <returns>Button reference for the accept button.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.RaiseFillDynPageEvent">
            <summary>
            Raise the FillDynPage event.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.RaiseLeaveDynPageEvent">
            <summary>
            Raise the LeaveDynPage event.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.msgs">
            <summary>
            Message centre proxy for UI.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ctlNodeName">
            <summary>
            This control's node name.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.wrkNodeName">
            <summary>
            Worker partner's node name.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.initialised">
            <summary>
            Tracks initialised state. Initialise cannot be merged into ctor.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.nextSeq">
            <summary>
            Next message sequence number.
            </summary>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.expectSignals">
            <summary>
            Are signals expected?
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.CtlNodeName">
            <summary>
            Gets the name of the control's node.
            </summary>
        </member>
        <member name="P:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.WrkNodeName">
            <summary>
            Gets the worker partner node name.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceived">
            <summary>
            Event raised when a message from the worker has been received.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.SignalReceived">
            <summary>
            Event raised when a signal from any node has been received.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ExtMsgReceived">
            <summary>
            Event raised when a message from any node except the worker partner.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ActOnOutOfView">
            <summary>
            Event raised when a control moves out of view.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ActOnIntoView">
            <summary>
            Event raised when a control moves into view, returning the button reference for the
            accept button.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ActOnFillDynPage">
            <summary>
            Event raised when a control fills a dynamic area page.
            </summary>
        </member>
        <member name="E:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ActOnLeaveDynPage">
            <summary>
            Event raised when a control leaves a dynamic area page.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg">
            <summary>
            Delegate for the MsgReceived event.
            </summary>
            <remarks>
            payload is message received.
            inMsgSeq is message's sequence number.
            respSeq is either 0 or the sequence number of the message this one is in
            response to.
            </remarks>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.SignalReceivedDlg">
            <summary>
            Delegate for the SignalReceived event.
            </summary>
            <remarks>
            payload is the signal payload.
            sender is the sender of the signal.
            sigMsgSeq is the sequence number of the signal message.
            </remarks>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.ExtMsgReceivedDlg">
            <summary>
            Delegate for the ExtMsgReceived event.
            </summary>
            <remarks>
            payload is message received.
            sender is the message sender.
            msgSeq is message's sequence number.
            respSeq is either 0 or the sequence number of the message this one is in
            response to.
            </remarks>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.SimpleDlg">
            <summary>
            Simple delegate event type for dynamic area. No params, no return.
            </summary>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.ManagedCtlBase.IntoViewDlg">
            <summary>
            Delegate type for IntoView event.
            </summary>
            <remarks>Function returns button to use as the accept button. </remarks>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.MsgCentre">
            <summary>
            Proxy for the core of the message centre, including the registry and the means
            to send messages and signals.
            </summary>
            <remarks>
            Contains static members only.
            </remarks>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.#ctor">
            <summary>
            Private constructor to prevent construction.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RegisterSingletonInstance(Bookham.TestEngine.Framework.Messages.IMsgCentre)">
            <summary>
            Register the wrapped singleton object implementing IMsgCentre.
            </summary>
            <param name="singleton">New singleton instance.</param>
        </member>
        <member name="F:Bookham.TestEngine.Framework.Messages.MsgCentre.singleton">
            <summary>
            Stored singleton instance.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.checkInitialised">
            <summary>
            Check if singleton is initialised. Throw if it is not.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object)">
            <summary>
            Register a node.
            </summary>
            <param name="nodeName">Name of node to register. '*' characters will be replaced with digits.</param>
            <param name="delivery">Link to delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
            <returns>New node name, including '*' replacement.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RegisterNode(System.String,Bookham.TestEngine.Framework.Messages.IDeliverMsg,System.Object,System.String)">
            <summary>
            Register a node.
            </summary>
            <param name="nodeName">Name of node to register. '*' characters will be replaced with digits.</param>
            <param name="delivery">Link to delivery agent.</param>
            <param name="deliveryTag">Delivery agent's tag.</param>
            <param name="parentNodeName">Node which will contain nodeName's node as stand-in.</param>
            <returns>New node name, including '*' replacement.</returns>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.SecureNodeToThread(System.String)">
            <summary>
            Set the named node's thread to the current thread.
            </summary>
            <param name="nodeName">Name of node to secure.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RemoveNode(System.String,System.String)">
            <summary>
            Remove the named node.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
            <param name="parentNodeName">Node which will contain nodeName's node as stand-in.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RemoveNode(System.String)">
            <summary>
            Remove the named node.
            </summary>
            <param name="nodeName">Name of node to remove.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RegisterRestrictedSignal(System.String,System.String)">
            <summary>
            Register a signal which may only be sent by the named node.
            </summary>
            <param name="sigId">Name of new signal.</param>
            <param name="nodeName">Name of node who may send signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.RegisterUnrestrictedSignal(System.String)">
            <summary>
            Register a signal which may be sent by any node.
            </summary>
            <param name="sigId">Name of new signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.SubscribeSignal(System.String,System.String)">
            <summary>
            Subscribe node to the named signal.
            </summary>
            <param name="nodeName">Node subscribing...</param>
            <param name="sigId">... to this signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.UnsubscribeSignal(System.String,System.String)">
            <summary>
            Unscribe node from the named signal.
            </summary>
            <param name="nodeName">Node to unsubscribe.</param>
            <param name="sigId">From this signal.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.SendAsync(System.String,System.String,System.Object,System.Int64,System.Int64)">
            <summary>
            Send response message via delivery agent.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">This message sequence number.</param>
            <param name="respSeq">Sequence number of message being responded to.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.SendAsync(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send new message via delivery agent.
            </summary>
            <param name="sender">Sender node name.</param>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.SendAnonymously(System.String,System.Object,System.Int64)">
            <summary>
            Anonymously send a new message via delivery agent.
            </summary>
            <param name="recipient">Recipient node name.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentre.SendSignal(System.String,System.String,System.Object,System.Int64)">
            <summary>
            Send signal to many nodes via delivery agent.
            </summary>
            <param name="sender">Sender node.</param>
            <param name="signalId">Signal id.</param>
            <param name="payload">Message payload.</param>
            <param name="msgSeq">Message sequence number.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.MsgCentreFrontEndException">
            <summary>
            Exception thrown by message centre front end.
            </summary>
        </member>
        <member name="M:Bookham.TestEngine.Framework.Messages.MsgCentreFrontEndException.#ctor(System.String)">
            <summary>
            Construct with message.
            </summary>
            <param name="msg">Message for exception base class.</param>
        </member>
        <member name="T:Bookham.TestEngine.Framework.Messages.NamespaceDoc">
            <summary>
            Test Engine Messaging Framework definitions. 
            These classes are needed by Test Software developers producing components 
            which use definitions within the Test Engine's internal messaging framework.
            </summary>
        </member>
    </members>
</doc>
