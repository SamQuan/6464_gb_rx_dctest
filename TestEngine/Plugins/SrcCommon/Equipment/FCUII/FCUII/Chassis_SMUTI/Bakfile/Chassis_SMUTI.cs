// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_SMUTI.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using System.IO.Ports;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using System.Threading;

namespace Bookham.TestLibrary.ChassisTypes
{
    /// <summary>
    /// Chassis_SMUTI
    /// </summary>
    public class Chassis_SMUTI : Chassis
    {

       public SerialPort serialPort_FCUII;

        #region Constructor
        /// <summary>
        /// Chassis Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for Serial chassis, this is the COM port name,e.g. COM1, COM2, ...)</param>
        public Chassis_SMUTI(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
             //Setup expected valid hardware variants 
            ChassisDataRecord chassisData = new ChassisDataRecord(
                "Hardware_Unknown",			// hardware name 
                "Firmware_Unknown",			// minimum valid firmware version 
                "Firmware_Unknown");		// maximum valid firmware version 
            ValidHardwareData.Add("ChassisData", chassisData);

            serialPort_FCUII = new SerialPort(resourceString);

            configSerialPort();
            
        }
        /// <summary>
        /// configure Serial Port
        /// </summary>
        private void configSerialPort()
        {
            this.serialPort_FCUII.BaudRate = 115200;
            this.serialPort_FCUII.DataBits = 8;
            this.serialPort_FCUII.StopBits = StopBits.One;
            this.serialPort_FCUII.Parity = Parity.Even;
            this.serialPort_FCUII.Handshake = Handshake.None;
            this.serialPort_FCUII.RtsEnable = true;
            this.serialPort_FCUII.DtrEnable = true;
            this.serialPort_FCUII.ReadBufferSize = 819200;
            this.serialPort_FCUII.WriteBufferSize = 409600;
            this.serialPort_FCUII.NewLine = "\r\n";
            this.serialPort_FCUII.ReadTimeout = SerialPort.InfiniteTimeout;
            this.serialPort_FCUII.WriteTimeout = SerialPort.InfiniteTimeout;
            this.Timeout_ms = 500000;
           

            if (!serialPort_FCUII.IsOpen)
                serialPort_FCUII.Open();
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return "Firmware_Unknown";
                
            }
        }
        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {

                return "Hardware_Unknown";
                
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public override bool IsOnline
        { 
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
            }
        }



        ///// <summary>
        ///// set dufault statues
        ///// </summary>
        //public override void SetDefaultState()
        //{
        //    string command;
        //    command = string.Format("*rst");
        //    instChassis.Write(command, this.instChassis.serialPort_FCUII);
        //    Thread.Sleep(2000);
        //}

        #endregion

       /// <summary>
       /// Clear
        /// </summary>
        public void Clear()
        {
            serialPort_FCUII.DiscardInBuffer();
            serialPort_FCUII.DiscardOutBuffer();
        }
        #region Common command
        
        /// <summary>
        /// Get slot contents
        /// part number of the plugin as a string (e.g. "XXXXX")
        /// </summary>
        /// <param name="slotNumber"></param>
        /// <returns></returns>
        public string GetSlotID(int slotNumber)
        {
            //TODO: what's command?
            string command="*OPT?";
            return this.Query(command,serialPort_FCUII);
        }

        /// <summary>
        /// Returns the Vendor, Part number, serial number & Firmware version, & Hardware version of the unit, 
        /// i.e. Bookham Tech PLC, XXX-XXXX, XXX-XXXX, FW:0001, HW:0001
        /// </summary>
        /// <returns></returns>
        public string GetIDN()
        {
            string command = "*IDN?";
            //return this.Query(command,serialPort_FCUII);
            string str = this.Query(command, serialPort_FCUII);
            return str;
        }


        #region command
        ///// <summary>
        ///// Returns the latest error string
        ///// </summary>
        ///// <returns></returns>
        //public string GetErrorMessage()
        //{
        //    return this.Query("*ERR", null);
        //}
        ///// <summary>
        ///// Returns "OK" upon completion.
        ///// </summary>
        ///// <returns></returns>
        //public void Reset()
        //{
        //    int t = Timeout_ms;
        //    this.Timeout_ms = 4000;
        //    string r = this.Query("*RST", null);
        //    this.Timeout_ms = t;
        //    if (r!="OK")
        //        throw new ChassisException("'*RST' fail");


        //}
        ///// <summary>
        ///// Returns a list of card types fitted in the rack. Comma separated. 
        ///// Processor card, serial number, slot card, serial number��. 
        ///// "EMPTY" if not present
        ///// </summary>
        ///// <returns></returns>
        //public string GetCardList()
        //{
        //    string r = this.Query("*OPT?", null);
        //    if (r == "EMPTY")
        //        throw new ChassisException("Cards not present!");
        //    return r;

        //}
        #endregion

        #endregion
        /// <summary>
        /// Wrter command to serial port
        /// </summary>
        /// <param name="SP1">serial Port</param>
        /// <param name="command">command</param>
        public void Write(string command,SerialPort SP1)
        {
            SP1.DiscardOutBuffer();
            byte[] bufferW = new byte[10000];
            bufferW = Encoding.Unicode.GetBytes(command + "\r");
            long leng = bufferW.Length;
            for (int i = 0; i < leng; i++, i++)
            {
                SP1.Write(new byte[] { bufferW[i] }, 0, 1);
                //Thread.Sleep(1);
                while (SP1.ReadByte() != bufferW[i])
                    SP1.Write(new byte[] { bufferW[i] }, 0, 1);
            }
            string str;
            if (command.Contains("SOUR"))
            {
                do
                {
                    str = SP1.ReadLine();
                //str = SP1.ReadLine();
                }while(!str.Contains("OK"));
                if (!str.Contains("OK"))
                    throw new Exception("invalid return data!");
            }
            //SP1.WriteLine(command);
            //string str = "";
            //do
            //{
            //    str = SP1.ReadLine();

            //} while (str != "OK" && !str.Contains("STAR"));
  
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SP1">serial Port</param>
        /// <param name="command">serial Port</param>
        /// <returns></returns>
        public string Query(string command,SerialPort SP1)
        {
            Write(command,SP1);
            Thread.Sleep(10);
            string str = SP1.ReadLine();
            str = SP1.ReadLine();
            return str;
        }
    }
}