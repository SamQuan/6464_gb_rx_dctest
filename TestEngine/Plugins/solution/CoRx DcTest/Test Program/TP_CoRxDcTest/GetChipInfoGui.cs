// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TP_CoRxDcTest/TestEngineManagedControl1.cs
// 
// Author: alice.huang
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// GUI To get chip information manualy
    /// </summary>
    public partial class GetChipInfoGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        TP_CoRxDcTestGui parent;
        public GetChipInfoGui(TP_CoRxDcTestGui parentForm)
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
            this.parent = parentForm;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DatumList infoList = new DatumList();
            if (txtXChipID.Text.Trim().Length < 3)
            {
                MessageBox.Show("Plese enter X chip ID");
                return;
            }
            else
            {
                infoList.AddString("X_CHIP_ID", txtXChipID.Text.Trim());
            }
            if (txtXChipBin.Text.Trim().Length < 1)
            {
                MessageBox.Show("Plese enter X Chip sn");
                return;
            }
            else
                infoList.AddString("X_CHIP_BIN", txtXChipBin.Text.Trim());
            if (txtXCocSN.Text.Trim().Length < 3)
            {
                MessageBox.Show("Plest enter the Xchip COC SN ");
                return;
            }
            else
                infoList.AddString("X_COC_SN", txtXCocSN.Text.Trim());
            if (txtXwaferID.Text.Trim().Length < 3)
            {
                MessageBox.Show("Please enter the X chip wafer id");
                return;
            }
            else
                infoList.AddString("X_WAFER_ID", txtXwaferID.Text.Trim());

            if (txtYChipBin.Text.Trim().Length < 1)
            {
                MessageBox.Show("Please enter the Y chip Bin ");
                return;
            }
            else
                infoList.AddString("Y_CHIP_BIN", txtYChipBin.Text.Trim());

            if (txtYChipID.Text.Trim().Length < 3)
            {
                MessageBox.Show("Please enter the Y chip ID");
                return;
            }
            else
                infoList.AddString("Y_CHIP_ID", txtYChipID.Text.Trim());
            if (txtYCocSN.Text.Trim().Length < 3)
            {
                MessageBox.Show("Please enter Y chip COC SN");
                return;
            }
            else
                infoList.AddString("Y_COC_SN", txtYCocSN.Text.Trim());
            if (txtYWaferID.Text.Trim().Length < 3)
            {
                MessageBox.Show("Please enter Y chip Wafer ID");
                return;
            }
            else
                infoList.AddString("Y_WAFER_ID", txtYWaferID.Text.Trim());

            parent.SendToWorker(infoList);
            parent.CtrlFinished();
        }
    }
}
