// [Copyright]
//
// Bookham Test Engine Library
// Tunable Modules Driver
//
// IxoliteMsgLogger.cs
// 
// Author: Bill Godfrey
// Design: Tunable Module Driver DD

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.ChassisNS
{
    /// <summary>
    /// Class implementing IMsgLogger for the IxoliteLibDotNet library, replacing the MsgLogger window.
    /// </summary>
    class IxoliteMsgLogger : IxoliteLibDotNet.IMsgLogger
    {
        #region Private data
        /// <summary>
        /// Waiting queue of messages to be read into teh transaction/error log file.
        /// </summary>
        Queue<string> msgLogQueue;

        /// <summary>
        /// Line of hex bytes sent/recvd building up.
        /// </summary>
        StringBuilder hexTrans;
        #endregion

        #region IMsgLogger Members
        public string LastMsgRcvdStr()
        {
            string msg = hexTrans.ToString();
            hexTrans.Remove(0, hexTrans.Length);
            return msg;
        }

        public string LastMsgSentStr()
        {
            return this.LastMsgRcvdStr();
        }

        public void LogINTERNAL(ref short errnum, ref string errsrc, ref string ErrMsg, ref string where)
        {
            msgLogQueue.Enqueue("MInternal error: (" + errnum + ", " + errsrc + ", " + ErrMsg + ", " + where + ")"); 
        }

        public void clearLastMsgRcvd()
        {
            hexTrans.Remove(0, hexTrans.Length);
        }

        public void clearLastMsgSent()
        {
            this.clearLastMsgRcvd();
        }

        public void logFATAL(ref string strErrMsg)
        {
            msgLogQueue.Enqueue("E" + strErrMsg);
        }

        public void logLastMsgRcvd(ref byte DataByte)
        {
            hexTrans.Append(DataByte.ToString("X02") + " ");
        }

        public void logLastMsgSent(ref byte DataByte)
        {
            hexTrans.Append(DataByte.ToString("X02") + " ");
        }

        public void logMESSAGE(ref string strErrMsg)
        {
            msgLogQueue.Enqueue("M" + strErrMsg);
        }

        public bool logMsgQueueEmpty()
        {
            return this.msgLogQueue.Count == 0;
        }

        public void logMsgQueueInit()
        {
            this.msgLogQueue = new Queue<string>();
            this.hexTrans = new StringBuilder();
        }

        public string logMsgQueueRead()
        {
            return msgLogQueue.Dequeue();
        }

        #endregion
    }
}
