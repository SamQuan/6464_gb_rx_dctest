using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace ag86060TestHarness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_Ag86060 testChassis;

        // PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_Ag86060_SimpleSwitch testInstr;        
        #endregion

        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource = "GPIB0::8::INSTR";
        const string chassisName = "Ag86060 Chassis";
        const string instr1Name = "SW1";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            testChassis = new Chassis_Ag86060(chassisName, "Chassis_Ag86060", visaResource);
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            testInstr = new Inst_Ag86060_SimpleSwitch(instr1Name, "Inst_Ag86060_SimpleSwitch", "", "", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;            
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest: Min output is: "  + testInstr.MinimumSwitchState + "\n");
            TestOutput("\n\n*** T01_FirstTest: Max output is: " + testInstr.MaximumSwitchState + "\n");


            TestOutput("\n\n*** T01_FirstTest: Select Output 0. ***");
            testInstr.SetDefaultState();

            testInstr.SwitchState = 0;
            Assert.AreEqual(testInstr.SwitchState, 0);
            TestOutput("\n\n*** Output 0 Selected ***");
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest: Select Output 1***");
            testInstr.SetDefaultState();

            testInstr.SwitchState = 1;
            Assert.AreEqual(testInstr.SwitchState, 1);

            TestOutput("\n\n*** Output 1 Selected ***");
        }

        [Test]
        public void T03_ThirdTest()
        {
            TestOutput("\n\n*** T03_ThirdTest: Select Last output***");
            testInstr.SetDefaultState();

            testInstr.SwitchState = testInstr.MaximumSwitchState;
            Assert.AreEqual(testInstr.SwitchState, testInstr.MaximumSwitchState);
            TestOutput("\n\n*** Last output port Selected ***");
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}
