// [Copyright]
//
// Bookham [Coherent RX DC Test]
// Bookham.TestSolution.TestModules
//
// DualChipTestJig.cs
//
// Author: alice.huang, 2010
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using Bookham.TestEngine.PluginInterfaces.TestJig;
using Bookham.TestEngine.Equipment;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class CoRxDcTestJig : ITestJig 
    {
        private InstrumentCollection instCollection;
        private ChassisCollection chassisCollection;

        #region ITestJig Members
        /// <summary>
        /// Instrument chassis collection
        /// </summary>
        public ChassisCollection ChassisCollection
        {
            set { this.chassisCollection = value; }
        }
        /// <summary>
        /// Jig ID
        /// </summary>
        /// <param name="InDebugMode"></param>
        /// <param name="InSimulationMode"></param>
        /// <returns></returns>
        public string GetJigID(bool InDebugMode, bool InSimulationMode)
        {
            return "CoRxDcTestJig";
        }
        /// <summary>
        /// Instrument collection
        /// </summary>
        public InstrumentCollection InstCollection
        {
            set { this.instCollection = value; }
        }

        #endregion
    }
}
