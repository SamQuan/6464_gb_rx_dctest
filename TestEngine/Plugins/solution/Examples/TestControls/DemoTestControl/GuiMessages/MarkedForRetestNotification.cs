// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// MarkedForRetestNotification.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Test Control Batch View GUI Notifies Test Control Plug-in 
	/// that a device has been marked for retest.
	/// </summary>
	public class MarkedForRetestNotification
	{
		/// <summary>
		/// Constructor. Initialises the serialNumber field.
		/// </summary>
		/// <param name="serialNumber">The serial number of the device in the batch marked for retest.</param>
		public MarkedForRetestNotification(string serialNumber)
		{
			this.serialNumber = serialNumber;
		}

		/// <summary>
		/// Serial number of the device marked for retest
		/// </summary>
		public readonly string serialNumber;
	}
}
