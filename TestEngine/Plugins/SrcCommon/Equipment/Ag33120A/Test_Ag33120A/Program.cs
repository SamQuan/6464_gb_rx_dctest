using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Ag33120A
{
    class Program
    {
        static void Main(string[] args)
        {
            TestObject test = new TestObject();
            test.Init();
            test.Run();
            test.T01_Waveform();
            test.T02_TestSignal();
            test.T03_TestModulation();
            test.T04_TestModulationFunctions();
            test.shutDown();
        }
    }
}
