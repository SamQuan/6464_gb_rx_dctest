﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace Bookham.TestLibrary.Utilities
{
    public class Mail
    {
        public static void SendEmail(string smtpHost, string fromAddress, string toAddress,string subject, string body, string password)
        {
            string userName = fromAddress.ToLower().EndsWith("@oclaro.com") ? fromAddress.Replace("@oclaro.com", "") : fromAddress;
            SmtpClient smtpClient = new SmtpClient(smtpHost);
            smtpClient.Host = smtpHost;
            smtpClient.Credentials = new System.Net.NetworkCredential(userName, password);

            fromAddress = fromAddress.ToLower().EndsWith("@oclaro.com") ? fromAddress : fromAddress + "@oclaro.com";

            using (MailMessage message = new MailMessage(fromAddress, fromAddress, subject, body))
            {
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;

                string[] tos = toAddress.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string to in tos)
                {
                    message.To.Add(to.ToLower().EndsWith("@oclaro.com") ? to : to + "@oclaro.com");
                }
                smtpClient.Send(message);
            }
        }

        public static string HeaderString()
        {
            string headerString = string.Empty;
            string tableformat, headformat;
            //'border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid
            // ';border-right: windowtext 0.5pt solid; border-top: windowtext 0.5pt solid;border-left: windowtext 0.5pt solid;border-bottom: windowtext 0.5pt solid;' valign='top'
            tableformat = "<table border='2' style='border: black thin solid'> ";
            headformat = "<td align='left' height='24' style='background-color: #DCDCDC; font-size: 14pt; font-family: Calibri' valign='top'>";
            headerString = tableformat + "<tr>";
            headerString = headerString + headformat + "Parameter" + "</td>";
            headerString = headerString + headformat + "Value" + "</td>";
            headerString = headerString + headformat + "LBOUND" + "</td>";
            headerString = headerString + headformat + "MEAN" + "</td>";
            headerString = headerString + headformat + "UBOUND" + "</td>";
            headerString = headerString + headformat + "Remark" + "</td>";
            headerString = headerString + "</tr>";

            return headerString;
        }
        public static string ValueString(string para, double value, double LCL, double Mean, double UCL, string Remark)
        {

            string valueString = string.Empty;
            string bodyformat;//';border-right: windowtext 0.5pt solid; border-top: windowtext 0.5pt solid;border-left: windowtext 0.5pt solid;border-bottom: windowtext 0.5pt solid;' valign='top'

            bodyformat = "<td align='left' height='22' style='background-color: #FFF5EE; font-size: 12pt; font-family: Calibri'>";
            valueString = valueString + bodyformat + para + "</td>";
            valueString = valueString + bodyformat + value + "</td>";
            valueString = valueString + bodyformat + LCL.ToString("00.###") + "</td>";
            valueString = valueString + bodyformat + Mean.ToString("00.###") + "</td>";
            valueString = valueString + bodyformat + UCL.ToString("00.###") + "</td>";

            if(!string.IsNullOrEmpty(Remark))
                bodyformat = "<td align='left' height='22' style='background-color: #FFFF00; font-size: 12pt; font-family: Calibri'>";

            valueString = valueString + bodyformat + Remark + "</td>";
            valueString = valueString + "</tr>";


            return valueString;
        }
    }
}
