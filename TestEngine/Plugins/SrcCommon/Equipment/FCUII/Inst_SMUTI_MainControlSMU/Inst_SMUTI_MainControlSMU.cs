// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Inst_SMUTI_TriggeredSMU.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using System.ComponentModel;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_TriggeredSMU
    /// </summary>
    public class Inst_SMUTI_MainControlSMU : Instrument
    {
        private Chassis_SMUTI instChassis;

        #region some enum variable define


        /// <summary>
        /// sweep direction ,forward or reverse
        /// </summary>
        protected enum SweepDirection
        {
           
            /// <summary>
            /// forward sweep
            /// </summary>
            forward,
            /// <summary>
            /// reverse sweep
            /// </summary>
            reverse,
        }

        /// <summary>
        /// sweep ,overall map or super mode map
        /// </summary>
        public enum Sweep
        {
            OverallMap,
            SuperMap
        }

        #endregion


        public event ProgressChangedEventHandler OverallmapProgressChanged;
        public event ProgressChangedEventHandler SupermapProgressChanged;

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_MainControlSMU(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 

            // 8 Channel High Current Low Voltage card (DSDR High Current )
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord MainControlCard = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("HighCurrentLowVoltageCard", MainControlCard);

            //TODO: add other type card

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Hardware_Unknown",								// chassis driver name  
                "Hardware_Unknown",									// minimum valid chassis driver version  
                "Hardware_Unknown");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);
            instChassis = (Chassis_SMUTI)chassisInit;
        }
        
        #region Instrument override
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            //TODO:Parse string according return format string
            get
            {
                return instChassis.FirmwareVersion;
            }
        }
        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            //TODO:Parse string according return format string
            get 
            {
                return instChassis.HardwareIdentity;
            }
        }
        /// <summary>
        /// set dufault statues
        /// </summary>
        public override void SetDefaultState()
        {
            string command;
            command = string.Format("*rst");
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
            Thread.Sleep(2000);
        }

        /// <summary>
        ///  Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                //base.IsOnline = value;
                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Basic Operation Function

         /// <summary>
        /// do overall maping,return overall mapping data
         /// </summary>
         /// <param name="delayTime_mS">overall mapping sweep delay time</param>
         /// <returns>file of overall map result</returns>
        public string OverAllMpaing(double delayTime_mS)
        {
            double indexLines = 0;
            string command;
            double delayTime_S = delayTime_mS / 1000;
            command = string.Format(":OMAP DEL" + delayTime_S.ToString());
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

            List<string> OverallMapingData = new List<string>();
            List<string> referPdMappingData = new List<string>();
            List<string> filterPdMappingData = new List<string>();
            string readData;
            string[] result;
            try
            {
                do
                {
                    readData = instChassis.serialPort_FCUII.ReadLine();

                    if (readData.Contains("STAR")) continue;
                    if (readData.Contains("END")) break;
                    if (readData == " ") break;
                    
                    //updata overMapping Percent
                    indexLines++;
                    //double temp = Math.Round(indexLine / overMappingMaxLines, 2) * 100;
                    double temp = indexLines;
                    this.overMappingPercent = int.Parse(temp.ToString());
                    double powerRatio = 999;

                    //List<string> listResult = new List<string>();
                    if (OverallmapProgressChanged != null)
                    {
                        ProgressChangedEventArgs arg = new ProgressChangedEventArgs(overMappingPercent, null);
                        OverallmapProgressChanged(this.instChassis.serialPort_FCUII, arg);
                    }


                    string[] tempData = readData.Split(',');
                    StringBuilder strPowerRatio = new StringBuilder();
                    StringBuilder strReferPD = new StringBuilder();
                    StringBuilder strFilterPD = new StringBuilder();
                    for (int i = 0; i < tempData.Length; i++)
                    {
                        if (tempData[i] != "" && tempData[i] != ",")
                        {
                            bool isDigit = true;
                            string[] array = tempData[i].Split('&');
                            for (int j = 0; j < array.Length; j++)
                            {
                                foreach (char var in array[j].Trim())
                                {
                                    if (!char.IsNumber(var) && var != 46 && var != 32) //can not contain "not_digit", " ","&"
                                    {
                                        isDigit = false;
                                        break;
                                    }
                                }
                            }

                            string[] array2 = tempData[i].Split('&');
                            if (isDigit)
                            {
                                if (Math.Abs(double.Parse(array2[1].Trim())) != 0)
                                    powerRatio = Math.Abs(double.Parse(array2[0].Trim())) / Math.Abs(double.Parse(array2[1].Trim()));
                                else
                                    powerRatio = 999;
                            }
                            else
                                powerRatio = 999;

                            strPowerRatio.Append(powerRatio.ToString("#0.0000"));
                            strPowerRatio.Append(",");
                            strFilterPD.Append(array2[0].Trim());
                            strFilterPD.Append(",");
                            strReferPD.Append(array2[1].Trim());
                            strReferPD.Append(",");
                        }
                        
                    }
                    //remove the last ","
                    strPowerRatio.Remove(strPowerRatio.Length - 1, 1);
                    strFilterPD.Remove(strFilterPD.Length - 1, 1);
                    strReferPD.Remove(strReferPD.Length - 1, 1);

                    OverallMapingData.Add(strPowerRatio.ToString());
                    referPdMappingData.Add(strReferPD.ToString());
                    filterPdMappingData.Add(strFilterPD.ToString());

                } while (readData != " ");
            }
            catch (Exception)
            {
                throw;
            }
            //make result file
            string overallMapDataFileName = DataDirectory("OverMap_PowerRatio", SweepDirection.forward, Sweep.OverallMap);
            string referPdMapDataFileName = DataDirectory("ReferPD_OverMap_", SweepDirection.forward, Sweep.OverallMap);
            string filterPdMapDataFileName = DataDirectory("FilterPD_OverMap_", SweepDirection.forward, Sweep.OverallMap);

            List<string> overallMapResultFileName = new List<string>();
            overallMapResultFileName.Add(overallMapDataFileName);
            overallMapResultFileName.Add(referPdMapDataFileName);
            overallMapResultFileName.Add(filterPdMapDataFileName);

            Dictionary<string, List<string>> overallMapResult = new Dictionary<string, List<string>>();
            overallMapResult.Add(overallMapDataFileName, OverallMapingData);
            overallMapResult.Add(referPdMapDataFileName, referPdMappingData);
            overallMapResult.Add(filterPdMapDataFileName, filterPdMappingData);

            foreach (string fileName in overallMapResultFileName)
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (string var in overallMapResult[fileName])
                    {
                        sw.WriteLine(var);
                    }
                    sw.Close();
                }
            }


            return overallMapDataFileName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="smFileName"></param>
        /// <returns></returns>
        private List<string> GetMiddleLineData(string smFileName)
        {
            List<string> returnData = new List<string>();
            using (StreamReader sr = new StreamReader(smFileName))
            {
                StringBuilder fileData = new StringBuilder();
                string title = sr.ReadLine();
                int lineNumber = 0;
                while (sr.Peek() > -1)
                {
                    string str = sr.ReadLine();
                    lineNumber++;
                    StringBuilder sb = new StringBuilder();
                    string[] strTemp = str.Split(',');
                    for (int i = 0; i < strTemp.Length; i++)
                    {
                        if (i != 1)
                        {
                            double temp = double.Parse(strTemp[i]) / 1000;
                            strTemp[i] = temp.ToString("##0.##########");
                        }
                        sb.Append(strTemp[i]);
                        sb.Append(",");
                    }
                    sb.Remove(sb.Length - 1, 1);

                    fileData.Append(sb);
                    fileData.Append(",");
                    if (lineNumber > 50)
                    {
                        fileData.Remove(fileData.Length - 1, 1);
                        returnData.Add(fileData.ToString());
                        fileData.Remove(0, fileData.Length);
                        lineNumber = 0;
                    }
                }

                //remove the last ","
                if (lineNumber > 0)
                {
                    fileData.Remove(fileData.Length - 1, 1);
                    returnData.Add(fileData.ToString());
                }
                sr.Close();
            }

            return returnData;

        }

        /// <summary>
        /// send middileLine data to Fcu
        /// </summary>
        /// <param name="imData">super mode middle file</param>
        public void SetMiddleLineData(string smFileName)
        {
            List<string> fileData = new List<string>();
            fileData = GetMiddleLineData(smFileName);
            
            instChassis.serialPort_FCUII.DiscardInBuffer();
            for (int i = 0; i < fileData.Count; i++)
            {
                int startPont = i * 51;
                instChassis.Write(":MIDDle " + startPont.ToString() + "," + fileData[i], this.instChassis.serialPort_FCUII);
                instChassis.serialPort_FCUII.ReadLine();
                instChassis.serialPort_FCUII.ReadLine();
            }
            
        }

        /// <summary>
        /// do super mode maping,return super mode maping data
        /// </summary>
        /// <param name="delayTime_mS">sweep delay time</param>
        /// <returns>List contain 2 file of forward map and reverse map result </returns>
        public List<string> SuperModeMaping(double delayTime_mS, bool powerRatioUsed)
        {


            string command;
            double delayTime_S = delayTime_mS / 1000;
            command = string.Format(":SMAP DEL " + delayTime_S.ToString());
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

            List<string> forwardMappingData = new List<string>();
            List<string> reverseMappingData = new List<string>();
            List<string> returnData = new List<string>();
            List<string> referPdForwardMappingData = new List<string>();
            List<string> referPdReverseMappingData = new List<string>();
            List<string> filterPdForwardMappingData = new List<string>();
            List<string> filterPdReverseMappingData = new List<string>();
            
            string readData;
            string[] result;
            double indexLines = 0;
            try
            {
                do
                {
                    readData = instChassis.serialPort_FCUII.ReadLine();

                    if (readData.Contains("STAR")) continue;
                    if (readData.Contains("END")) break;

                    //updata overMapping Percent
                    indexLines++;
                    //double temp = Math.Round(indexLines / superModeMappingMaxLines, 2) * 100;
                    double temp = indexLines;
                    this.superModeMappingPercent = int.Parse(temp.ToString());

                    if (SupermapProgressChanged != null)
                    {
                        ProgressChangedEventArgs arg = new ProgressChangedEventArgs(this.superModeMappingPercent, null);
                        SupermapProgressChanged(this.instChassis.serialPort_FCUII, arg);
                    }
                    //split forward and reverse mapping data
                    string[] tempStr = readData.Split(',');
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sbReferPD = new StringBuilder();
                    StringBuilder sbFilterPD = new StringBuilder();
                    double powerRatio;
                    if (!powerRatioUsed)
                    {
                        //forward mapping data
                        for (int i = 0; i <= tempStr.Length / 2; i++)
                        {
                            sb.Append(tempStr[i]);
                            sb.Append(",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        forwardMappingData.Add(sb.ToString());

                        //reverse mapping data
                        sb.Remove(0, sb.Length);
                        for (int i = tempStr.Length - 1; i >= tempStr.Length / 2; i--)
                        {
                            sb.Append(tempStr[i]);
                            sb.Append(",");
                        }
                        sb.Remove(sb.Length - 1, 1);
                        reverseMappingData.Add(sb.ToString());
                    }
                    else
                    {
                        //forward mapping data(powerRation)
                        for (int i = 0; i <= tempStr.Length / 2; i++)
                        {
                            if (tempStr[i].Contains("&"))
                            {
                                bool isDigit = true;
                                string[] array = tempStr[i].Split('&');
                                for (int j = 0; j < array.Length; j++)
                                {
                                    foreach (char var in array[j].Trim())
                                    {
                                        if (!char.IsNumber(var) && var != 46 && var != 32) //can not contain "not_digit", " ","&"
                                        {
                                            isDigit = false;
                                            break;
                                        }
                                    }
                                }

                                string[] array2 = tempStr[i].Split('&');
                                if (isDigit)
                                {
                                    if (Math.Abs(double.Parse(array2[1].Trim())) != 0)
                                        powerRatio = Math.Abs(double.Parse(array2[0].Trim())) / Math.Abs(double.Parse(array2[1].Trim()));
                                    else
                                        powerRatio = 999;
                                }
                                else
                                    powerRatio = 999;
                                sb.Append(powerRatio.ToString("#0.0000"));
                                sb.Append(",");
                                sbFilterPD.Append(array2[0].Trim());
                                sbFilterPD.Append(",");
                                sbReferPD.Append(array2[1].Trim());
                                sbReferPD.Append(",");

                            }
                        }
                        //remove the last ","
                        sb.Remove(sb.Length - 1, 1);
                        sbFilterPD.Remove(sbFilterPD.Length - 1, 1);
                        sbReferPD.Remove(sbReferPD.Length - 1, 1);

                        forwardMappingData.Add(sb.ToString());
                        referPdForwardMappingData.Add(sbReferPD.ToString());
                        filterPdForwardMappingData.Add(sbFilterPD.ToString());

                        //reverse mapping data(powerRatio)
                        sb.Remove(0, sb.Length);
                        sbFilterPD.Remove(0, sbFilterPD.Length);
                        sbReferPD.Remove(0, sbReferPD.Length);
                        for (int i = tempStr.Length - 1; i >= tempStr.Length / 2; i--)
                        {
                            if (tempStr[i].Contains("&"))
                            {
                                bool isDigit = true;
                                string[] array = tempStr[i].Split('&');
                                for (int j = 0; j < array.Length; j++)
                                {
                                    foreach (char var in array[j].Trim())
                                    {
                                        if (!char.IsNumber(var) && var != 46 && var != 32) //can not contain "not_digit", " ","&"
                                        {
                                            isDigit = false;
                                            break;
                                        }
                                    }
                                }

                                string[] array2 = tempStr[i].Split('&');
                                if (isDigit)
                                {
                                    if (Math.Abs(double.Parse(array2[1].Trim())) != 0)
                                        powerRatio = Math.Abs(double.Parse(array2[0].Trim())) / Math.Abs(double.Parse(array2[1].Trim()));
                                    else
                                        powerRatio = 999;
                                }
                                else
                                    powerRatio = 999;
                                sb.Append(powerRatio.ToString("#0.0000"));
                                sb.Append(",");
                                sbFilterPD.Append(array2[0].Trim());
                                sbFilterPD.Append(",");
                                sbReferPD.Append(array2[1].Trim());
                                sbReferPD.Append(",");
                            }
                        }

                        sb.Remove(sb.Length - 1, 1);
                        sbFilterPD.Remove(sbFilterPD.Length - 1, 1);
                        sbReferPD.Remove(sbReferPD.Length - 1, 1);

                        reverseMappingData.Add(sb.ToString());
                        referPdReverseMappingData.Add(sbReferPD.ToString());
                        filterPdReverseMappingData.Add(sbFilterPD.ToString());

                    }
                    Thread.Sleep(10);
                } while (readData != "");
            }
            catch (Exception)
            {
                throw;
            }
            string forwardFileName;
            string reverseFileName;
            string refPdForwardFileName;
            string filterPdForwardFileName;
            string refPdReverseFileName;
            string filterPdReverseFileName;
            if (powerRatioUsed)
            {
                forwardFileName = DataDirectory("PowerRation_", SweepDirection.forward, Sweep.SuperMap);
                reverseFileName = DataDirectory("PowerRation_", SweepDirection.reverse, Sweep.SuperMap);
            }
            else
            {
                forwardFileName = DataDirectory("LockerTx_", SweepDirection.forward, Sweep.SuperMap);
                reverseFileName = DataDirectory("LockerTx_", SweepDirection.reverse, Sweep.SuperMap);
            }
            refPdForwardFileName = DataDirectory("RefPD_", SweepDirection.forward, Sweep.SuperMap);
            filterPdForwardFileName = DataDirectory("filterPD_", SweepDirection.forward, Sweep.SuperMap);
            refPdReverseFileName = DataDirectory("RefPD_", SweepDirection.reverse, Sweep.SuperMap);
            filterPdReverseFileName = DataDirectory("filterPD_", SweepDirection.reverse, Sweep.SuperMap);
                           
            List<string> SmMapResultFileName = new List<string>();
            SmMapResultFileName.Add(forwardFileName);
            SmMapResultFileName.Add(reverseFileName);
            SmMapResultFileName.Add(refPdForwardFileName);
            SmMapResultFileName.Add(filterPdForwardFileName);
            SmMapResultFileName.Add(refPdReverseFileName);
            SmMapResultFileName.Add(filterPdReverseFileName);

            Dictionary<string, List<string>> SmMapResult = new Dictionary<string, List<string>>();
            SmMapResult.Add(forwardFileName, forwardMappingData);
            SmMapResult.Add(reverseFileName, reverseMappingData);
            SmMapResult.Add(refPdForwardFileName, referPdForwardMappingData);
            SmMapResult.Add(refPdReverseFileName, referPdReverseMappingData);
            SmMapResult.Add(filterPdForwardFileName, filterPdForwardMappingData);
            SmMapResult.Add(filterPdReverseFileName, filterPdReverseMappingData);

            foreach (string fileName in SmMapResultFileName)
            {
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    foreach (string var in SmMapResult[fileName])
                    {
                        sw.WriteLine(var);
                    }
                    sw.Close();
                }
            }

            returnData.Add(forwardFileName);
            returnData.Add(reverseFileName);
            return returnData;
        }

        /// <summary>
        /// Get temperature of the source card
        /// </summary>
        /// <returns></returns>
        public double GetSouCardTemperature()
        {
            string command;
            command = string.Format(":TEMP{0}", this.Slot);
            double temperature = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
            return temperature;
        }

        /// <summary>
        /// get middle line data from flash,this function can use to check weather the middle data sent to FCUII
        /// </summary>
        /// <returns></returns>
        public double[] GetMidLineDataFromFlash()
        {
            string measureData = instChassis.Query(":MIDD?", this.instChassis.serialPort_FCUII);
            string[] data = measureData.Split(',');
            List<double> middleLineData = new List<double>();
            for (int i = 0; i < data.Length; i++)
            {
                middleLineData.Add(double.Parse(data[i]));
            }
            return middleLineData.ToArray();
        }

        #endregion

        #region private function
                                   
        protected string DataDirectory(string preStr, SweepDirection sweepDirction,Sweep sweepType)
        {
            string FileName = "";
            switch (sweepType)
            {
                case Sweep.OverallMap:
                    FileName = string.Format("{0}{1:yyyyMMddhhmmss}.csv", preStr, DateTime.Now);
                    break;
                case Sweep.SuperMap:
                    FileName = string.Format("{0}{1}{2:yyyyMMddhhmmss}.csv", preStr, Enum.GetName(typeof(SweepDirection), sweepDirction), DateTime.Now);
                    break;
                default:
                    break;
            }
            string dir = Directory.GetDirectoryRoot(System.Environment.CurrentDirectory) + "FcuMarkIIResult";
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            return Path.Combine(dir, FileName);
        }
        #endregion

        #region private val
        private int overMappingPercent = 0;
        private const int overMappingMaxLines = 147;
        private int superModeMappingPercent = 0;
        private const int superModeMappingMaxLines = 201;

        #endregion

    }
}
