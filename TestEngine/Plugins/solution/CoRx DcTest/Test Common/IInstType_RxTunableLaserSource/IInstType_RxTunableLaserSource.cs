using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.InstrTypes
{
    public interface IInstType_RxTunableLaserSource
    {
        int CurrentChan
        {
            set;
            get;
        }
        /// <summary>
        /// set/Get  WaveLen in nM
        /// </summary>
        double WaveLen_nM
        {
            set;
            get;
        }
        /// <summary>
        /// Set/Get Frequency in GHz
        /// </summary>
        double Frequency_GHz
        {
            set;
            get;
        }
        /// <summary>
        /// Set/Get Power output in dBm
        /// </summary>
        double Power_dBm
        {
            set;
            get;
        }
        /// <summary>
        /// Set/Get Output status
        /// </summary>
        bool OutputEnable
        {
            get;
            set;
        }

        /// <summary>
        /// Get minimun available frequency in Ghz
        /// </summary>
        double MinFreq_GHz
        {
            get;
            //set;
        }

        /// <summary>
        /// Get maximun available frequency in Ghz
        /// </summary>
        double MaxFreq_GHz
        {
            get;
            //set;
        }

        /// <summary>
        /// Get minimun wavelength available in nm
        /// </summary>
        double MinWaveLen_nM
        {
            get;
            //set;
        }

        /// <summary>
        /// Get maximun wavelength available in nm
        /// </summary>
        double MaxWaveLen_nM
        {
            get;
            //set;
        }

        /// <summary>
        /// Get Minimun output power in dBm
        /// </summary>
        double MinPower_dBm
        {
            get;
            //set;
        }

        /// <summary>
        ///  Get maximun output power in dBm
        /// </summary>
        double MaxPower_dBm
        {
            get;
            //set;
        }
    }
}
