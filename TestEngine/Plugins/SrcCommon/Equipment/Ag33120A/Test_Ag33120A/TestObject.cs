using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using Bookham.TestSolution.ChassisNS;
//using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.Instruments;
using NUnit.Framework;

namespace Test_Ag33120A
{
    internal class TestObject
    {
        [TestFixtureSetUp ]
        public void Init()
        {
            address = "//szn-dt-jackxue/GPIB0::10::INSTR";

            chassis = new Chassis_Ag33120A("Ag33120A", "Chassis_Ag33120A", address);
            instr_Ag33120 = new Instr_Ag33120A("Inst_33120A", "Instr_Ag33120A", "", "", chassis);

            this.chassis.IsOnline = true;
            instr_Ag33120.IsOnline = true;
            
            instr_Ag33120.OutputLoad = Instr_Ag33120A.EnumLoadType.Ohm50;
            instr_Ag33120.OutputUnit = Instr_Ag33120A.EnumVoltUnit.VRMS;
            instr_Ag33120.OutputOff();
        }

        public void Run()
        {
            instr_Ag33120.Frequency_Khz = 550;
            double freq = instr_Ag33120.Frequency_Khz;

            instr_Ag33120.Amplitude_V = 0.100;
            double amplitude = instr_Ag33120.Amplitude_V;
        }
        [TestFixtureTearDown ]
        public void shutDown()
        {
            chassis.IsOnline = false;
            instr_Ag33120.IsOnline = false;
        }

        [Test]
        public void T01_Waveform()
        {
            Debug.WriteLine("Setting DC Waveform");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.DC, 0, 0, 1);
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.DC, instr_Ag33120.WaveForm);

            Debug .WriteLine("Setting noise waveform");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.NOIS, 5000, 0.5, 0);
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.NOIS,instr_Ag33120.WaveForm);

            Debug .WriteLine("Setting RAMP waveform");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.RAMP, 3000, 1, 0);
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.RAMP,instr_Ag33120.WaveForm);
            
            Debug .WriteLine("Setting Sinusoid waveform");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.SIN, 3000, 2, -1);
            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.SIN,instr_Ag33120.WaveForm);
            Assert.AreEqual(-1, instr_Ag33120 .Voffset_V,0.1);
            Assert.AreEqual(2, instr_Ag33120.Amplitude_V, 0.2);
            Assert.AreEqual(3.000, instr_Ag33120.Frequency_Khz, 0.01);

            Debug .WriteLine("Setting Square waveform");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.SQU, 4000, 2, 1);
            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.SQU, instr_Ag33120.WaveForm);
            Assert.AreEqual(4, instr_Ag33120.Frequency_Khz, 0.02);
            Assert.AreEqual(2, instr_Ag33120.Amplitude_V, 0.1);
            Assert.AreEqual(1, instr_Ag33120.Voffset_V);

            Debug .WriteLine ("Setting triangle waveform");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.TRI, 2000,2, 0.5);
            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.TRI, instr_Ag33120.WaveForm);
            Assert.AreEqual(2, instr_Ag33120.Amplitude_V, 0.1);
            Assert.AreEqual(2, instr_Ag33120.Frequency_Khz, 0.1);
            Assert.AreEqual(0.5, instr_Ag33120.Voffset_V, 0.1);            
                      
        }
        [Test]
        public void T02_TestSignal()
        {
            Debug.WriteLine("Setting waveform sinusois");
            instr_Ag33120.OutputUnit = Instr_Ag33120A.EnumVoltUnit.VPP;
            instr_Ag33120.WaveForm = Instr_Ag33120A.EnumWaveFormType.SIN;
            Debug.WriteLine("Setting amplitude to 5v");
            instr_Ag33120.Amplitude_V = 2.5;
            Debug.WriteLine("Setting frequency to 20K");
            instr_Ag33120.Frequency_Khz = 20;
            Debug.WriteLine("Setting offset to 0.5");
            instr_Ag33120.Voffset_V = 0.5;

            Assert.AreEqual(Instr_Ag33120A.EnumWaveFormType.SIN, instr_Ag33120.WaveForm);
            Assert.AreEqual(2.5, instr_Ag33120.Amplitude_V, 0.1);
            Assert.AreEqual(20, instr_Ag33120.Frequency_Khz, 0.2);
            Assert.AreEqual(0.5, instr_Ag33120.Voffset_V, 0.1);
        }

        [Test]
        public void T03_TestModulation()
        {
            Debug.WriteLine("Setting modulation type to be AM");
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.AM;
            instr_Ag33120.ModulationState = true;
            instr_Ag33120 .ModulationWaveForm = Instr_Ag33120A.EnumWaveFormType .SQU ;
            //Debug.WriteLine("Setting modulation source to be internal");
            //instr_Ag33120.ModulationSource = Instr_Ag33120A.EnumModulationSource.INT;
            System.Threading.Thread.Sleep(Tstep_mS);
            Debug.WriteLine("Setting modulation source to be external");
            instr_Ag33120.ModulationSource = Instr_Ag33120A.EnumModulationSource.EXT;
            System.Threading.Thread.Sleep(Tstep_mS);
            instr_Ag33120.ModulationSource = Instr_Ag33120A.EnumModulationSource.BOTH;
            System.Threading.Thread.Sleep(Tstep_mS);
            instr_Ag33120.ModulationState = false;

            Debug.WriteLine("Setting modulation type to BM");
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.BM;
            
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            instr_Ag33120.ModulationState = false;

            Debug.WriteLine("Setting modulation type to be FM");
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.FM;
            instr_Ag33120.WaveForm = Instr_Ag33120A.EnumWaveFormType.TRI;
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationType.FM, instr_Ag33120.ModulationType);
            instr_Ag33120.ModulationState = false;

            Debug.WriteLine("Setting modulation type to be frequency sweep");
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.None;
            Assert.AreEqual(Instr_Ag33120A .EnumModulationType .None , instr_Ag33120.ModulationType);
            System.Threading.Thread.Sleep(Tstep_mS);
            instr_Ag33120.ModulationState = false;



        }

        [Test]
        public void T04_TestModulationFunctions()
        {
            instr_Ag33120.OutputOff();
            Debug.WriteLine("Setting carrier wave to be square, 5V, 20K");
            instr_Ag33120.SetWaveform(Instr_Ag33120A.EnumWaveFormType.SQU, 20000, 3, 0);
            System.Threading.Thread.Sleep(Tstep_mS);

            // Setting AM Modulation
            Debug.WriteLine("Setting modulation to be AM");
            //instr_Ag33120.SetAmModulation(Instr_Ag33120A.EnumWaveFormType.SIN, 
            //    Instr_Ag33120A.EnumModulationSource.INT, 5000, 50);
            //instr_Ag33120.ModulationState = true;
            //System.Threading.Thread.Sleep(Tstep_mS);
            instr_Ag33120.ModulationState = false;
            // Setting Modulation source to be external
            Debug.WriteLine("Setting modulation source to be external");
            instr_Ag33120.SetAmModulation(Instr_Ag33120A.EnumWaveFormType.SQU,
                Instr_Ag33120A.EnumModulationSource.EXT, 8000, 30);
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationSource.EXT, instr_Ag33120.ModulationSource);

            Debug.WriteLine("Setting modulation source to be both");
            instr_Ag33120.SetAmModulation(Instr_Ag33120A.EnumWaveFormType.SIN,
                Instr_Ag33120A.EnumModulationSource.BOTH, 5000, 50);
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationSource.BOTH, instr_Ag33120.ModulationSource);
            instr_Ag33120.ModulationState = false;

            // Setting BM Modulation
            Debug.WriteLine("Setting BM Modulation");
            instr_Ag33120.SetBmModulation(Instr_Ag33120A.EnumModulationSource.EXT, 5, 5000, 90);
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationType.BM, instr_Ag33120.ModulationType);
            instr_Ag33120.ModulationState = false;

            Debug.WriteLine("Setting FM Modulation");
            instr_Ag33120.setFmModulation(Instr_Ag33120A.EnumWaveFormType.TRI, 10000, 5000);
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationType.FM, instr_Ag33120.ModulationType);
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.None;
            instr_Ag33120.ModulationState = false;

            Debug.WriteLine("Setting FSK Modulation");
            instr_Ag33120.setFSKModulation(Instr_Ag33120A.EnumModulationSource.INT, 5000, 10000);
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationType.FSK, instr_Ag33120.ModulationType);
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.None;
            instr_Ag33120.ModulationState = false;

            Debug.WriteLine("Setting Frequency sweep modulation");
            instr_Ag33120.setFrequencySweep(1000, 10000, 3, Instr_Ag33120A.EnumSweepMode.LIN);
            instr_Ag33120.ModulationState = true;
            System.Threading.Thread.Sleep(Tstep_mS);
            Assert.AreEqual(Instr_Ag33120A.EnumModulationType.SWE, instr_Ag33120.ModulationType);
            instr_Ag33120.ModulationType = Instr_Ag33120A.EnumModulationType.None;
            instr_Ag33120.ModulationState = false;

        }
        #region private parameter

        private Chassis_Ag33120A chassis;
        private Instr_Ag33120A instr_Ag33120;

        private string address;
        private int Tstep_mS = 3000;
        #endregion
    }
}
