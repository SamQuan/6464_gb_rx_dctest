using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.CoRxDCTestCommonData
{
    /// <summary>
    /// CoRX PD responsivity test data
    /// </summary>
    public struct StrcCoRxPdRespData
    {
        /// <summary>
        /// Optical input power
        /// </summary>
        public double OpcPower_DBm;
        /// <summary>
        /// 
        /// </summary>
        public double Wavelen_nm;

        public int Chan;

        public double Ipd_XI1_mA;
        public double Ipd_XI2_mA;
        public double Ipd_XQ1_mA;
        public double Ipd_XQ2_mA;
        public double Ipd_YI1_mA;
        public double Ipd_YI2_mA;
        public double Ipd_YQ1_mA;
        public double Ipd_YQ2_mA;

        /// <summary>
        /// 
        /// </summary>
        public double I_Tap_mA;

    }
    /// <summary>
    /// CoRx PD responsivity data  class
    /// </summary>
    public class CoRxPdRespData
    {
        public readonly StrcCoRxPdRespData TestData;
        public readonly double Res_XI1;
        public readonly double Res_XI2;
        public readonly double Res_XQ2;
        public readonly double Res_XQ1;
        public readonly double Res_YI1;
        public readonly double Res_YI2;
        public readonly double Res_YQ1;
        public readonly double Res_YQ2;
        public readonly double Avg_Res;

        public readonly double Res_Tap;

        public readonly double DeltaRes_XI1;
        public readonly double DeltaRes_XI2;
        public readonly double DeltaRes_XQ2;
        public readonly double DeltaRes_XQ1;
        public readonly double DeltaRes_YI1;
        public readonly double DeltaRes_YI2;
        public readonly double DeltaRes_YQ1;
        public readonly double DeltaRes_YQ2;

        public readonly double XDeltaRes_XI1;
        public readonly double XDeltaRes_XI2;
        public readonly double XDeltaRes_XQ2;
        public readonly double XDeltaRes_XQ1;
        public readonly double YDeltaRes_YI1;
        public readonly double YDeltaRes_YI2;
        public readonly double YDeltaRes_YQ1;
        public readonly double YDeltaRes_YQ2;

        public readonly double  CMRR_XI;
        public readonly double  CMRR_XQ;
        public readonly double  CMRR_YI;
        public readonly double  CMRR_YQ;
        public readonly double CMRR_EFF;

        public readonly double RespImbalanceRatio;
        public readonly double RespImbalance_X;
        public readonly double RespImbalance_Y;

        public readonly double MinIpd_mA;
        public readonly double MaxIpd_mA;

        public readonly double MinIpd_X_mA;
        public readonly double MaxIpd_X_mA;
        public readonly double MinIpd_Y_mA;
        public readonly double MaxIpd_Y_mA;

        //add by chaoqun
        public  double Ripple_XI1;
        public  double Ripple_XI2;
        public  double Ripple_XQ1;
        public  double Ripple_XQ2;
        public  double Ripple_YI1;
        public  double Ripple_YI2;
        public  double Ripple_YQ1;
        public  double Ripple_YQ2;
        public double OpticalPower;         // Add to allow isolation calculation

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="measData"> Co Rx Pd responsivity measurement data </param>
        public CoRxPdRespData(StrcCoRxPdRespData measData, bool is6464)
        {
            TestData = measData;

            if (measData.OpcPower_DBm != double.NegativeInfinity)
            {
                double power_mW = Alg_PowConvert_dB.Convert_dBmtomW(measData.OpcPower_DBm);

                OpticalPower = power_mW;
                Res_XI1 = TestData.Ipd_XI1_mA / power_mW;
                Res_XI2 = TestData.Ipd_XI2_mA / power_mW;
                Res_XQ1 = TestData.Ipd_XQ1_mA / power_mW;
                Res_XQ2 = TestData.Ipd_XQ2_mA / power_mW;
                Res_YI1 = TestData.Ipd_YI1_mA / power_mW;
                Res_YI2 = TestData.Ipd_YI2_mA / power_mW;
                Res_YQ1 = TestData.Ipd_YQ1_mA / power_mW;
                Res_YQ2 = TestData.Ipd_YQ2_mA / power_mW;
                Res_Tap = TestData.I_Tap_mA / power_mW;


                double avg_Res_X;
                double avg_Res_Y;

                if (is6464)
                {
                    Avg_Res = (Res_XI1 + Res_XQ1 + Res_YI1 + Res_YQ1) / 4;
                    avg_Res_X = (Res_XI1 + Res_XQ1) / 2;
                    avg_Res_Y = (Res_YI1 + Res_YQ1) / 2;

                }
                else {

                    Avg_Res = (Res_XI1 + Res_XI2 + Res_XQ1 + Res_XQ2 + Res_YI1 + Res_YI2 + Res_YQ1 + Res_YQ2) / 8;
                    avg_Res_X = (Res_XI1 + Res_XI2 + Res_XQ1 + Res_XQ2) / 4;
                    avg_Res_Y = (Res_YI1 + Res_YI2 + Res_YQ1 + Res_YQ2) / 4;
                
                }
                
                
                
                DeltaRes_XI1 =  10 * Math.Log(Res_XI1 / Avg_Res) / Math.Log(10);
                DeltaRes_XI2 =  10 * Math.Log(Res_XI2 / Avg_Res) / Math.Log(10);
                DeltaRes_XQ1 =  10 * Math.Log(Res_XQ1 / Avg_Res) / Math.Log(10);
                DeltaRes_XQ2 =  10 * Math.Log(Res_XQ2 / Avg_Res) / Math.Log(10);
                DeltaRes_YI1 =  10 * Math.Log(Res_YI1 / Avg_Res) / Math.Log(10);
                DeltaRes_YI2 =  10 * Math.Log(Res_YI2 / Avg_Res) / Math.Log(10);
                DeltaRes_YQ1 =  10 * Math.Log(Res_YQ1 / Avg_Res) / Math.Log(10);
                DeltaRes_YQ2 =  10 * Math.Log(Res_YQ2 / Avg_Res) / Math.Log(10);

                XDeltaRes_XI1 = 10 * Math.Log(Res_XI1 / avg_Res_X) / Math.Log(10);
                XDeltaRes_XI2 = 10 * Math.Log(Res_XI2 / avg_Res_X) / Math.Log(10);
                XDeltaRes_XQ1 = 10 * Math.Log(Res_XQ1 / avg_Res_X) / Math.Log(10);
                XDeltaRes_XQ2 = 10 * Math.Log(Res_XQ2 / avg_Res_X) / Math.Log(10);
                YDeltaRes_YI1 = 10 * Math.Log(Res_YI1 / avg_Res_Y) / Math.Log(10);
                YDeltaRes_YI2 = 10 * Math.Log(Res_YI2 / avg_Res_Y) / Math.Log(10);
                YDeltaRes_YQ1 = 10 * Math.Log(Res_YQ1 / avg_Res_Y) / Math.Log(10);
                YDeltaRes_YQ2 = 10 * Math.Log(Res_YQ2 / avg_Res_Y) / Math.Log(10);
                
                CMRR_XI =  20 * Math.Log(Math.Abs((Res_XI1 - Res_XI2) / (Res_XI1 + Res_XI2))) / Math.Log(10);
                CMRR_XQ =  20 * Math.Log(Math.Abs((Res_XQ1 - Res_XQ2) / (Res_XQ1 + Res_XQ2))) / Math.Log(10);
                CMRR_YI =  20 * Math.Log(Math.Abs((Res_YI1 - Res_YI2) / (Res_YI1 + Res_YI2))) / Math.Log(10);
                CMRR_YQ =  20 * Math.Log(Math.Abs((Res_YQ1 - Res_YQ2) / (Res_YQ1 + Res_YQ2))) / Math.Log(10);
                
                double temp = Math.Pow( 1.0 / Math.Pow(10.0 , CMRR_XI/20.0), 2.0);
                temp += Math.Pow( 1.0 / Math.Pow(10.0 , CMRR_XQ/20.0), 2.0);
                temp += Math.Pow( 1.0 / Math.Pow(10.0 , CMRR_YI/20.0), 2.0);
                temp += Math.Pow( 1.0 / Math.Pow(10.0 , CMRR_YQ/20.0), 2.0);
                temp = 0.25 * temp;
                CMRR_EFF = 20 * Math.Log10(1.0 / Math.Sqrt(temp));

                // This code replaced for new GA requirement which average P&N
                //////// Get maximun pd current in X chip PDs
                //////MaxIpd_X_mA = Math.Max(Math.Abs(TestData.Ipd_XI1_mA), Math.Abs(TestData.Ipd_XI2_mA));
                //////MaxIpd_X_mA = Math.Max(Math.Abs(TestData.Ipd_XQ1_mA), MaxIpd_X_mA);
                //////MaxIpd_X_mA = Math.Max(Math.Abs(TestData.Ipd_XQ2_mA), MaxIpd_X_mA);

                //////// Get maximun pd current in Y chip PDs
                //////MaxIpd_Y_mA = Math.Max(Math.Abs(TestData.Ipd_YI2_mA), Math.Abs(TestData.Ipd_YI1_mA));
                //////MaxIpd_Y_mA = Math.Max(Math.Abs(TestData.Ipd_YQ1_mA), MaxIpd_Y_mA);
                //////MaxIpd_Y_mA = Math.Max(Math.Abs(TestData.Ipd_YQ2_mA), MaxIpd_Y_mA);
                //////// The maximum value goes from the greater one in X and Y chip
                //////MaxIpd_mA = Math.Max(MaxIpd_X_mA, MaxIpd_Y_mA);

                //////// Get minimun pd current in X chip PDs
                //////MinIpd_X_mA = Math.Min(Math.Abs(TestData.Ipd_XI1_mA), Math.Abs(TestData.Ipd_XI2_mA));
                //////MinIpd_X_mA = Math.Min(Math.Abs(TestData.Ipd_XQ1_mA), MinIpd_X_mA);
                //////MinIpd_X_mA = Math.Min(Math.Abs(TestData.Ipd_XQ2_mA), MinIpd_X_mA);
                //////// Get minimun pd current in Y chip PDs
                //////MinIpd_Y_mA = Math.Min(Math.Abs(TestData.Ipd_YI2_mA), Math.Abs(TestData.Ipd_YI1_mA));
                //////MinIpd_Y_mA = Math.Min(Math.Abs(TestData.Ipd_YQ1_mA), MinIpd_Y_mA);
                //////MinIpd_Y_mA = Math.Min(Math.Abs(TestData.Ipd_YQ2_mA), MinIpd_Y_mA);
                //////// The minimum value goes from the less one in X and Y chip
                //////MinIpd_mA = Math.Min(MinIpd_X_mA, MinIpd_Y_mA);

                MaxIpd_X_mA = Math.Max(Math.Abs(TestData.Ipd_XI1_mA + TestData.Ipd_XI2_mA) / 2, Math.Abs(TestData.Ipd_XQ1_mA + TestData.Ipd_XQ2_mA) / 2);
                MinIpd_X_mA = Math.Min(Math.Abs(TestData.Ipd_XI1_mA + TestData.Ipd_XI2_mA) / 2, Math.Abs(TestData.Ipd_XQ1_mA + TestData.Ipd_XQ2_mA) / 2);

                MaxIpd_Y_mA = Math.Max(Math.Abs(TestData.Ipd_YI1_mA + TestData.Ipd_YI2_mA) / 2, Math.Abs(TestData.Ipd_YQ1_mA + TestData.Ipd_YQ2_mA) / 2);
                MinIpd_Y_mA = Math.Min(Math.Abs(TestData.Ipd_YI1_mA + TestData.Ipd_YI2_mA) / 2, Math.Abs(TestData.Ipd_YQ1_mA + TestData.Ipd_YQ2_mA) / 2);

                MaxIpd_mA = Math.Max(MaxIpd_X_mA, MaxIpd_Y_mA);
                MinIpd_mA = Math.Min(MinIpd_X_mA, MinIpd_Y_mA);

                if (MinIpd_X_mA > 0.00000001)  // if current reading goed below nA, it not a valid value
                {
                    RespImbalance_X = 10 * Math.Log10(MaxIpd_X_mA / MinIpd_X_mA);
                }
                else
                    RespImbalance_X = double.PositiveInfinity;

                if (MinIpd_Y_mA > 0.00000001)
                {
                    RespImbalance_Y = 10 * Math.Log10(MaxIpd_Y_mA / MinIpd_Y_mA);
                }
                else
                    RespImbalance_Y = double.PositiveInfinity;

                if (MinIpd_mA > 0.00000001)
                {
                    RespImbalanceRatio = 10 * Math.Log10(MaxIpd_mA / MinIpd_mA);
                }
                else
                    RespImbalanceRatio = double.PositiveInfinity;                
            }
            else  // if optical power is too low, set all cal parameters to be 0
            {
                Res_XI1 = 0;
                Res_XI2 = 0;
                Res_XQ1 = 0;
                Res_XQ2 = 0;
                Res_YI1 = 0;
                Res_YI2 = 0;
                Res_YQ1 = 0;
                Res_YQ2 = 0;
                Avg_Res = 0;
                Res_Tap = 0;
                DeltaRes_XI1 = 0;
                DeltaRes_XI2 = 0;
                DeltaRes_XQ1 = 0;
                DeltaRes_XQ2 = 0;
                DeltaRes_YI1 = 0;
                DeltaRes_YI2 = 0;
                DeltaRes_YQ1 = 0;
                DeltaRes_YQ2 = 0;
                XDeltaRes_XI1 = 0;
                XDeltaRes_XI2 = 0;
                XDeltaRes_XQ1 = 0;
                XDeltaRes_XQ2 = 0;
                YDeltaRes_YI1 = 0;
                YDeltaRes_YI2 = 0;
                YDeltaRes_YQ1 = 0;
                YDeltaRes_YQ2 = 0;
                CMRR_XI = 0;
                CMRR_XQ = 0;
                CMRR_YI = 0;
                CMRR_YQ = 0;

                //add by chaoqun
                Ripple_XI1 = 0;
                Ripple_XI2 = 0;
                Ripple_XQ1 = 0;
                Ripple_XQ2 = 0;
                Ripple_YI1 = 0;
                Ripple_YI2 = 0;
                Ripple_YQ1 = 0;
                Ripple_YQ2 = 0;

            }
        }

        /// <summary>
        ///  Put all data member parameter (includes tap pd ) name in a string
        /// </summary>
        /// <returns></returns>
        public static string GetDataStringHeader()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("Power (dBm),");
            strHeader.Append("PD XI1 (mA),");
            strHeader.Append("PD XI2 (mA),");
            strHeader.Append("PD XQ1 (mA),");
            strHeader.Append("PD XQ2 (mA),");
            strHeader.Append("PD YI1 (mA),");
            strHeader.Append("PD YI2 (mA),");
            strHeader.Append("PD YQ1 (mA),");
            strHeader.Append("PD YQ2 (mA),");
            strHeader.Append("PD Tap (mA),");
            
            strHeader.Append("Responsivity PD XI1 (A/W),");
            strHeader.Append("Responsivity PD XI2 (A/W),");
            strHeader.Append("Responsivity PD XQ1 (A/W),");
            strHeader.Append("Responsivity PD XQ2 (A/W),");
            strHeader.Append("Responsivity PD YI1 (A/W),");
            strHeader.Append("Responsivity PD YI2 (A/W),");
            strHeader.Append("Responsivity PD YQ1 (A/W),");
            strHeader.Append("Responsivity PD YQ2 (A/W),");            
            strHeader.Append("Responsivity PD Tap1 (A/W,");

            strHeader.Append("Dev. Avg. PD XI1 (dB),");
            strHeader.Append("Dev. Avg. PD XI2 (dB),");
            strHeader.Append("Dev. Avg. PD XQ1 (dB),");
            strHeader.Append("Dev. Avg. PD XQ2 (dB),");
            strHeader.Append("Dev. Avg. PD YI1 (dB),");
            strHeader.Append("Dev. Avg. PD YI2 (dB),");
            strHeader.Append("Dev. Avg. PD YQ1 (dB),");
            strHeader.Append("Dev. Avg. PD YQ2 (dB),");
            strHeader.Append("CMRR PD XI (dB),");
            strHeader.Append("CMRR PD XQ (dB),");
            strHeader.Append("CMRR PD YI (dB),");
            strHeader.Append("CMRR PD YQ (dB),");
            strHeader.Append("CMRR PD EFF (dB),");
            strHeader.Append("Resposivity Ratio,");
            strHeader.Append("X Resposivity Ratio,");
            strHeader.Append("Y Resposivity Ratio,");

            return strHeader.ToString();
        }

        public static string GetDataStringHeader6464()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("Power (dBm),");
            strHeader.Append("PD XI (mA),");
            strHeader.Append("PD XQ (mA),");
            strHeader.Append("PD YI (mA),");
            strHeader.Append("PD YQ (mA),");
            strHeader.Append("PD Tap (mA),");

            strHeader.Append("Responsivity PD XI (A/W),");
            strHeader.Append("Responsivity PD XQ (A/W),");
            strHeader.Append("Responsivity PD YI (A/W),");
            strHeader.Append("Responsivity PD YQ (A/W),");
            strHeader.Append("Responsivity PD Tap1 (A/W,");

            strHeader.Append("Dev. Avg. PD XI (dB),");
            strHeader.Append("Dev. Avg. PD XQ (dB),");
            strHeader.Append("Dev. Avg. PD YI (dB),");
            strHeader.Append("Dev. Avg. PD YQ (dB),");
            strHeader.Append("Resposivity Ratio,");
            strHeader.Append("X Resposivity Ratio,");
            strHeader.Append("Y Resposivity Ratio,");

            return strHeader.ToString();
        }
        /// <summary>
        ///  Put all data member value ( includes Tap )  into a string
        /// </summary>
        /// <returns></returns>
        public string DataToString()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(TestData.OpcPower_DBm);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XI2_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XQ1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XQ2_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YI2_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YQ1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YQ2_mA);
            dataStr.Append(",");            
            dataStr.Append(TestData.I_Tap_mA);
            dataStr.Append(",");
            
            dataStr.Append(Res_XI1);
            dataStr.Append(",");
            dataStr.Append(Res_XI2);
            dataStr.Append(",");
            dataStr.Append(Res_XQ1);
            dataStr.Append(",");
            dataStr.Append(Res_XQ2);
            dataStr.Append(",");
            dataStr.Append(Res_YI1);
            dataStr.Append(",");
            dataStr.Append(Res_YI2);
            dataStr.Append(",");
            dataStr.Append(Res_YQ1);
            dataStr.Append(",");
            dataStr.Append(Res_YQ2);
            dataStr.Append(",");            
            dataStr.Append(Res_Tap);
            dataStr.Append(",");

            dataStr.Append(DeltaRes_XI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ2);
            dataStr.Append(",");
            dataStr.Append(CMRR_XI);
            dataStr.Append(",");
            dataStr.Append(CMRR_XQ);
            dataStr.Append(",");
            dataStr.Append(CMRR_YI);
            dataStr.Append(",");
            dataStr.Append(CMRR_YQ);
            dataStr.Append(",");
            dataStr.Append(CMRR_EFF);
            dataStr.Append(",");
            dataStr.Append(RespImbalanceRatio);
            dataStr.Append(",");
            dataStr.Append(RespImbalance_X);
            dataStr.Append(",");
            dataStr.Append(RespImbalance_Y);
            dataStr.Append(",");
            return dataStr.ToString();
        }

        public string DataToString6464()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(TestData.OpcPower_DBm);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XQ1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YQ1_mA);
            dataStr.Append(",");
             dataStr.Append(TestData.I_Tap_mA);
            dataStr.Append(",");

            dataStr.Append(Res_XI1);
            dataStr.Append(",");
            dataStr.Append(Res_XQ1);
            dataStr.Append(",");
            dataStr.Append(Res_YI1);
            dataStr.Append(",");
            dataStr.Append(Res_YQ1);
            dataStr.Append(",");
            dataStr.Append(Res_Tap);
            dataStr.Append(",");

            dataStr.Append(DeltaRes_XI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ1);
            dataStr.Append(",");
            dataStr.Append(RespImbalanceRatio);
            dataStr.Append(",");
            dataStr.Append(RespImbalance_X);
            dataStr.Append(",");
            dataStr.Append(RespImbalance_Y);
            dataStr.Append(",");
            return dataStr.ToString();
        }
        /// <summary>
        ///  Put all PD parameter name in a string
        /// </summary>
        /// <returns></returns>
        public static string GetPdDataStringHeader()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("Power (dBm),");
            strHeader.Append("PD XI1 (mA),");
            strHeader.Append("PD XI2 (mA),");
            strHeader.Append("PD XQ1 (mA),");
            strHeader.Append("PD XQ2 (mA),");
            strHeader.Append("PD YI1 (mA),");
            strHeader.Append("PD YI2 (mA),");
            strHeader.Append("PD YQ1 (mA),");
            strHeader.Append("PD YQ2 (mA),");
            strHeader.Append("Responsivity PD XI1 (A/W),");
            strHeader.Append("Responsivity PD XI2 (A/W),");
            strHeader.Append("Responsivity PD XQ1 (A/W),");
            strHeader.Append("Responsivity PD XQ2 (A/W),");
            strHeader.Append("Responsivity PD YI1 (A/W),");
            strHeader.Append("Responsivity PD YI2 (A/W),");
            strHeader.Append("Responsivity PD YQ1 (A/W),");
            strHeader.Append("Responsivity PD YQ2 (A/W),");
            strHeader.Append("Dev. Avg. PD XI1 (dB),");
            strHeader.Append("Dev. Avg. PD XI2 (dB),");
            strHeader.Append("Dev. Avg. PD XQ1 (dB),");
            strHeader.Append("Dev. Avg. PD XQ2 (dB),");
            strHeader.Append("Dev. Avg. PD YI1 (dB),");
            strHeader.Append("Dev. Avg. PD YI2 (dB),");
            strHeader.Append("Dev. Avg. PD YQ1 (dB),");
            strHeader.Append("Dev. Avg. PD YQ2 (dB),");
            strHeader.Append("CMRR PD XI (dB),");
            strHeader.Append("CMRR PD XQ (dB),");
            strHeader.Append("CMRR PD YI (dB),");
            strHeader.Append("CMRR PD YQ (dB),");
            strHeader.Append("CMRR PD EFF (dB),");
            strHeader.Append("Resposivity Ratio,");
            strHeader.Append("X Resposivity Ratio,");
            strHeader.Append("Y Resposivity Ratio,");
            return strHeader.ToString();
        }
        public static string GetPdDataStringHeader6464()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("Power (dBm),");
            strHeader.Append("PD XI (mA),");
            strHeader.Append("PD XQ (mA),");
            strHeader.Append("PD YI (mA),");
            strHeader.Append("PD YQ (mA),");
            strHeader.Append("Responsivity PD XI (A/W),");
            strHeader.Append("Responsivity PD XQ (A/W),");
            strHeader.Append("Responsivity PD YI (A/W),");
            strHeader.Append("Responsivity PD YQ (A/W),");
            strHeader.Append("Dev. Avg. PD XI (dB),");
            strHeader.Append("Dev. Avg. PD XQ (dB),");
            strHeader.Append("Dev. Avg. PD YI (dB),");
            strHeader.Append("Dev. Avg. PD YQ (dB),");

            strHeader.Append("Resposivity Ratio,");
            strHeader.Append("X Resposivity Ratio,");
            strHeader.Append("Y Resposivity Ratio,");
            return strHeader.ToString();
        }
        /// <summary>
        ///  Put all pd parameter data into a string
        /// </summary>
        /// <returns></returns>
        public string GetPdDataToString()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(TestData.OpcPower_DBm);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XI2_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XQ1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XQ2_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YI2_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YQ1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YQ2_mA);
            dataStr.Append(",");
            dataStr.Append(Res_XI1);
            dataStr.Append(",");
            dataStr.Append(Res_XI2);
            dataStr.Append(",");
            dataStr.Append(Res_XQ1);
            dataStr.Append(",");
            dataStr.Append(Res_XQ2);
            dataStr.Append(",");
            dataStr.Append(Res_YI1);
            dataStr.Append(",");
            dataStr.Append(Res_YI2);
            dataStr.Append(",");
            dataStr.Append(Res_YQ1);
            dataStr.Append(",");
            dataStr.Append(Res_YQ2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ2);
            dataStr.Append(",");
            dataStr.Append(CMRR_XI);
            dataStr.Append(",");
            dataStr.Append(CMRR_XQ);
            dataStr.Append(",");
            dataStr.Append(CMRR_YI);
            dataStr.Append(",");
            dataStr.Append(CMRR_YQ);
            dataStr.Append(",");
            dataStr.Append(CMRR_EFF);
            dataStr.Append(",");
            dataStr.Append(RespImbalanceRatio);
            dataStr .Append (",");
            dataStr .Append (RespImbalance_X );
            dataStr .Append (",");
            dataStr .Append (RespImbalance_Y );
            dataStr .Append (",");
            return dataStr.ToString();
        }

        public string GetPdDataToString6464()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(TestData.OpcPower_DBm);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_XQ1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YI1_mA);
            dataStr.Append(",");
            dataStr.Append(TestData.Ipd_YQ1_mA);
            dataStr.Append(",");
            dataStr.Append(Res_XI1);
            dataStr.Append(",");
            dataStr.Append(Res_XQ1);
            dataStr.Append(",");
            dataStr.Append(Res_YI1);
            dataStr.Append(",");
            dataStr.Append(Res_YQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ1);
            dataStr.Append(",");
            dataStr.Append(RespImbalanceRatio);
            dataStr.Append(",");
            dataStr.Append(RespImbalance_X);
            dataStr.Append(",");
            dataStr.Append(RespImbalance_Y);
            dataStr.Append(",");
            return dataStr.ToString();
        }


        //add it by chaoqun
        /// <summary>
        /// put all pd ripple data parameter name in a string
        /// </summary>
        /// <returns></returns>
        public static string GetPdRippleStringHeader()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("Ripple_XI1 (%),");
            strHeader.Append("Ripple_XI2 (%),");
            strHeader.Append("Ripple_XQ1 (%),");
            strHeader.Append("Ripple_XQ2 (%),");
            strHeader.Append("Ripple_YI1 (%),");
            strHeader.Append("Ripple_YI2 (%),");
            strHeader.Append("Ripple_YQ1 (%),");
            strHeader.Append("Ripple_YQ2 (%),");

            return strHeader.ToString();
        }

        /// <summary>
        /// Put all PD's ripple data into string
        /// </summary>
        /// <returns></returns>
        public string GetPdRippleString()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(Ripple_XI1);
            dataStr.Append(",");
            dataStr.Append(Ripple_XI2);
            dataStr.Append(",");
            dataStr.Append(Ripple_XQ1);
            dataStr.Append(",");
            dataStr.Append(Ripple_XQ2);
            dataStr.Append(",");
            dataStr.Append(Ripple_YI1);
            dataStr.Append(",");
            dataStr.Append(Ripple_YI2);
            dataStr.Append(",");
            dataStr.Append(Ripple_YQ1);
            dataStr.Append(",");
            dataStr.Append(Ripple_YQ2);
            dataStr.Append(",");

            return dataStr.ToString();

        }

        /// <summary>
        ///  Put all responsivity parameter name in a string
        /// </summary>
        /// <returns></returns>
        public static string GetPdRespStringHeader()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("RESP_XIP (A/W),");
            strHeader.Append("RESP_XIN (A/W),");
            strHeader.Append("RESP_XQP (A/W),");
            strHeader.Append("RESP_XQN (A/W),");
            strHeader.Append("RESP_YIP (A/W),");
            strHeader.Append("RESP_YIN (A/W),");
            strHeader.Append("RESP_YQP (A/W),");
            strHeader.Append("RESP_YQN (A/W),");      

            return strHeader.ToString();
        }

        public static string GetPdRespStringHeader6464()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("RESP_XI (A/W),");
            strHeader.Append("RESP_XQ (A/W),");
            strHeader.Append("RESP_YI (A/W),");
            strHeader.Append("RESP_YQ (A/W),");
            return strHeader.ToString();
        }

        /// <summary>
        ///  Put pd reposivity data into a string
        /// </summary>
        /// <returns></returns>
        public string GetPdRespString()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(Res_XI1);
            dataStr.Append(",");
            dataStr.Append(Res_XI2);
            dataStr.Append(",");
            dataStr.Append(Res_XQ1);
            dataStr.Append(",");
            dataStr.Append(Res_XQ2);
            dataStr.Append(",");
            dataStr.Append(Res_YI1);
            dataStr.Append(",");
            dataStr.Append(Res_YI2);
            dataStr.Append(",");
            dataStr.Append(Res_YQ1);
            dataStr.Append(",");
            dataStr.Append(Res_YQ2);
            dataStr.Append(",");            

            return dataStr.ToString();
        }
        
        public string GetPdRespString6464()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(Res_XI1);
            dataStr.Append(",");
            dataStr.Append(Res_XQ1);
            dataStr.Append(",");
            dataStr.Append(Res_YI1);
            dataStr.Append(",");
            dataStr.Append(Res_YQ1);
            dataStr.Append(",");
            return dataStr.ToString();
        }

        /// <summary>
        ///  Put all deviation responsivity parameter name in a string
        /// </summary>
        /// <returns></returns>
        public static string GetDevRespStringHeader()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("DEV_XIP (dB),");
            strHeader.Append("DEV_XIN (dB),");
            strHeader.Append("DEV_XQP (dB),");
            strHeader.Append("DEV_XQN (dB),");
            strHeader.Append("DEV_YIP (dB),");
            strHeader.Append("DEV_YIN (dB),");
            strHeader.Append("DEV_YQP (dB),");
            strHeader.Append("DEV_YQN (dB),");
            
            return strHeader.ToString();
        }

        public static string GetDevRespStringHeader6464()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("DEV_XI (dB),");
            strHeader.Append("DEV_XQ (dB),");
            strHeader.Append("DEV_YI (dB),");
            strHeader.Append("DEV_YQ (dB),");
             return strHeader.ToString();
        }
        /// <summary>
        ///  Put all pd reposivity data into a string
        /// </summary>
        /// <returns></returns>
        public string GetDevRespString()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);            
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI2);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ2);
            dataStr.Append(",");            

            return dataStr.ToString();
        }

        public string GetDevRespString6464()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_XQ1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YI1);
            dataStr.Append(",");
            dataStr.Append(DeltaRes_YQ1);
            dataStr.Append(",");
            return dataStr.ToString();
        }
        /// <summary>
        ///  Put all CMRR parameter name in a tring
        /// </summary>
        /// <returns></returns>
        public static string GetCMRRStringHeader()
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("CMRR_XI (dB),");
            strHeader.Append("CMRR_XQ (dB),");
            strHeader.Append("CMRR_YI (dB),");
            strHeader.Append("CMRR_YQ (dB),");
            strHeader.Append("CMRR_EFF (dB)");
            return strHeader.ToString();
        }
        /// <summary>
        ///  Put all CMRR data into a string
        /// </summary>
        /// <returns></returns>
        public string GetCMRRString()
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            dataStr.Append(CMRR_XI);
            dataStr.Append(",");
            dataStr.Append(CMRR_XQ);
            dataStr.Append(",");
            dataStr.Append(CMRR_YI);
            dataStr.Append(",");
            dataStr.Append(CMRR_YQ);
            dataStr.Append(",");
            dataStr.Append(CMRR_EFF);


            return dataStr.ToString();
        }

        /// <summary>
        /// Return tap pd data member header in string ,  each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public static string GetTapDataStringHeader()
        {
            StringBuilder header = new StringBuilder();
            header.Append("Wavelength (nm),");
            header.Append("Power (dBm),");
            header.Append("PD Tap (mA),");
            header.Append("Responsivity PD Tap1 (A/W,");

            return header.ToString();
        }

        /// <summary>
        /// Return Tap pd  data value in string , each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public string GetTapDataString()
        {
            StringBuilder datastr = new StringBuilder();
            datastr.Append(TestData.Wavelen_nm);
            datastr.Append(",");

            datastr.Append(TestData.OpcPower_DBm);
            datastr.Append(",");
            datastr.Append(TestData.I_Tap_mA);
            datastr.Append(",");
            datastr.Append(Res_Tap);
            datastr.Append(",");

            return datastr.ToString();

        }

        /// <summary>
        /// Return Tap pd Responsivity member header in string , each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public static string GetTapRespStringHeader()
        {
            StringBuilder header = new StringBuilder();
            header.Append("Wavelength (nm),");
            header.Append("Responsivity PD Tap1 (A/W,");

            return header.ToString();
        }

        /// <summary>
        /// Return Tap pd Responsivity data in string , each parametes sperated by comma
        /// </summary>
        /// <returns></returns>
        public string GetTapRespString()
        {
            StringBuilder datastr = new StringBuilder();
            datastr.Append(TestData.Wavelen_nm);
            datastr.Append(",");

            datastr.Append(Res_Tap);
            datastr.Append(",");

            return datastr.ToString();
        }

        /// <summary>
        ///  Put single chip's deviation responsivity parameter name in a String
        /// </summary>
        /// <param name="strChipId"> Chip Id , "X" or "Y" </param>
        /// <returns></returns>
        public static string GetChipDevRespStringHeader( string strChipId)
        {
            StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength (nm),");
            strHeader.Append("DEV_" + strChipId +"I (dB),");
            strHeader.Append("DEV_" + strChipId + "Q (dB),");        
            return strHeader.ToString();
        }
        /// <summary>
        ///  Put single chip's  pds reposivity deviation ratio data into a string
        /// </summary>
        /// <param name="strChipId"> Chip Id , "X" or "Y" </param>
        /// <returns> String combined as " wavelen_nm, DelRes_pd_I1,DelRes_pd_I2,DelRes_pd_Q1,DelRes_pd_Q2, </returns>
        public string GetChipDevRespString(string strChipId)
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            // Get DeltaRes according to chip id
            if ( string .Compare ("X",strChipId ,true )== 0)
            {
                dataStr.Append(XDeltaRes_XI1);
                dataStr.Append(",");
                dataStr.Append(XDeltaRes_XI2);
                dataStr.Append(",");
                dataStr.Append(XDeltaRes_XQ1);
                dataStr.Append(",");
                dataStr.Append(XDeltaRes_XQ2);
                dataStr.Append(",");
            }
            else if (string .Compare ( strChipId , "Y",true )== 0)
            {
                dataStr.Append(YDeltaRes_YI1);
                dataStr.Append(",");
                dataStr.Append(YDeltaRes_YI2);
                dataStr.Append(",");
                dataStr.Append(YDeltaRes_YQ1);
                dataStr.Append(",");
                dataStr.Append(YDeltaRes_YQ2);
                dataStr.Append(",");
            }
            else 
                throw new ArgumentException (  strChipId + " is not a correct chip id ");
            return dataStr.ToString();
        }

        public string GetChipDevRespString6464(string strChipId)
        {
            StringBuilder dataStr = new StringBuilder();
            dataStr.Append(TestData.Wavelen_nm);
            dataStr.Append(",");
            // Get DeltaRes according to chip id
            if (string.Compare("X", strChipId, true) == 0)
            {
                dataStr.Append(XDeltaRes_XI1);
                dataStr.Append(",");
                dataStr.Append(XDeltaRes_XQ1);
                dataStr.Append(",");
            }
            else if (string.Compare(strChipId, "Y", true) == 0)
            {
                dataStr.Append(YDeltaRes_YI1);
                dataStr.Append(",");
                dataStr.Append(YDeltaRes_YQ1);
                dataStr.Append(",");
            }
            else
                throw new ArgumentException(strChipId + " is not a correct chip id ");
            return dataStr.ToString();
        }
    }

}
