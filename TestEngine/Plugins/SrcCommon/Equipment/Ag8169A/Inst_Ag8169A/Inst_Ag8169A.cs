// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Ag8169A.cs
//
// Author: jerryxw.hu, 2008
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_Ag8169A : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag8169A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            //HEWLETT-PACKARD,HP8169A,3425G00518,1.02
            InstrumentDataRecord Ag8169AData = new InstrumentDataRecord(
                "HEWLETT-PACKARD HP8169A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "1.02");	   // maximum valid firmware version 
            ValidHardwareData.Add("Ag8169A", Ag8169AData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisAg8169AData = new InstrumentDataRecord(
                "Chassis_Ag8169A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ag8169A", chassisAg8169AData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            instrumentChassis = (Chassis_Ag8169A)base.InstrumentChassis;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // Read the chassis ID string
                string idn = instrumentChassis.Query_Unchecked("*IDN?", null);

                // Return the firmware version in the 4th comma seperated field
                return idn.Split(',')[3].Trim();
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // Read the chassis ID string and split the comma seperated fields
                string[] idn = instrumentChassis.Query_Unchecked("*IDN?", null).Split(',');

                // Return field1, the manufacturer name and field 2, the model number
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Clear();
            instrumentChassis.Write("*RST", this);
            instrumentChassis.Write("*CLS", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    /* Nothing to do. */
                }
            }
        }
        #endregion
        #region   Polarization Controller Function
    #region Switching On and Off the Instrument Display
        /// <summary>
        /// 
        /// </summary>
        public bool Display
                    {
                        get
                        {   bool displayEnable;
                            string displayState=instrumentChassis.Query_Unchecked("DISP:ENAB?", this);
                            if(displayState=="+1")
                            {
                                displayEnable=true;
                            }
                            else
                            {
                                displayEnable=false;
                            }
                            return displayEnable;
                         }
                        set
                        {
                            if (value == true)
                            {
                                instrumentChassis.Write("DISP:ENAB 1" , this);
                            }
                            else if (value == false)
                            {
                                instrumentChassis.Write("DISP:ENAB 0", this);
                            }

                        }
                    }
      #endregion
    #region Position Polarizing Filter
        /// <summary>
        /// 
        /// </summary>
        public double PolarizingFilterPosition
                    {
                        get
                        {
                            double position =Convert.ToDouble(instrumentChassis.Query_Unchecked("POS:POL?", this));
                            return position;

                        }
                        set
                        {  if(value >=-360 && value <= 360)
                          {
                              instrumentChassis.Write("POS:POL " + value, this);
                          }
                        else
                         {
                             string errStr = string.Format("Position set value :{0} is out-of-range!", value);
                             throw new InstrumentException(errStr);
                         }
                      
                        }

                    }
    #endregion
    #region SettingPolarizationState
        /// <summary>
        /// 
        /// </summary>
        public double EPSilonb
        {
            get
            {
                double epsilonbState = Convert.ToDouble(instrumentChassis.Query_Unchecked(":CIRC:EPS?", this));
                return epsilonbState;
            }
            set
            {
                if (value >= -720 && value <= 720)
                {
                    instrumentChassis.Write(":CIRC:EPS " + value, this);
                }
                else
                {
                    string errStr = string.Format("EPSilonb set value :{0} is out-of-range!", value);
                    throw new InstrumentException(errStr);
                }

            }

        }
        /// <summary>
        /// 
        /// </summary>
        public double THETap
        {
            get
            {
                double thetapState = Convert.ToDouble(instrumentChassis.Query_Unchecked(":CIRC:THET?", this));
                return thetapState;
            }
            set
            {
                if (value >= -2160 && value <= 2160)
                {
                    instrumentChassis.Write(":CIRC:THET " + value, this);
                }
                else
                {
                    string errStr = string.Format("THETap set value :{0} is out-of-range!", value);
                    throw new InstrumentException(errStr);
                }
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public double HALF
        {
            get
            {
                double half = Convert.ToDouble(instrumentChassis.Query_Unchecked(":POS:HALF?", this));
                return half;
            }
            set
            {
                if (value >= -360 && value <= 360)
                {
                    instrumentChassis.Write(":POS:HALF " + value, this);
                }
                else
                {
                    string errStr = string.Format("HALF set value :{0} is out-of-range!", value);
                    throw new InstrumentException(errStr);
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double QUARter
        {
            get
            {
                double quarter = Convert.ToDouble(instrumentChassis.Query_Unchecked(":POS:QUAR?", this));
                return quarter;
            }
            set
            {
                if (value >= -360 && value <= 360)
                {
                    instrumentChassis.Write(":POS:QUAR " + value, this);
                }
                else
                {
                    string errStr = string.Format("QUARter set value :{0} is out-of-range!", value);
                    throw new InstrumentException(errStr);
                }

            }
        }
                    #endregion
    #region Scanning the Sphere
        /// <summary>
        /// 
        /// </summary>
        public int PSPHereRATE
        {
            get
            {
                int psphereRate;
                string rate = instrumentChassis.Query_Unchecked(":PSPH:RATE?", this);
                psphereRate = Convert.ToInt16(rate);
                return psphereRate;
            }
            set
            {
                instrumentChassis.Write(":PSPH:RATE "+value, this);
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public void IMMediate()
        {
            instrumentChassis.Write(":INIT", this);
        }
        /// <summary>
        /// 
        /// </summary>
        public void ABORt()
        {
            instrumentChassis.Write(":ABOR", this);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SYSTemERRor()
        {
          string systemError=instrumentChassis.Query_Unchecked(":SYST:ERR?", this);
          return systemError;
        }
            #endregion 
#endregion

        #region Private data
                    // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag8169A instrumentChassis;
        #endregion
    }
}
