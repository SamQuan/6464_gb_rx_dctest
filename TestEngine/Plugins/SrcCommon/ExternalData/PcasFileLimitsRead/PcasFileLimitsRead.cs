// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasFileLimitsRead\PcasFileLimitsRead.cs
// 
// Author: Paul Annetts
// Design: 

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO; // Path
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Framework.Limits;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestLibrary.ExternalData
{
    /// <summary>
    /// File limits object that reads Test Engine limits directly from a PCAS export file
    /// </summary>
    public class PcasFileLimitsRead : ILimitRead
    {
        /// <summary>
		/// Required constructor from Test Engine
		/// </summary>
		/// <param name="dataSource">Ignored</param>
		/// <param name="logger">The component providing logging functionality.</param>
        public PcasFileLimitsRead(ILogging logger, string dataSource)
		{			
		}

        #region ILimitRead Members
        /// <summary>
        /// Required ILimitRead member. Gets a single Specification from a single file.      
        /// </summary>
        /// <param name="keyData">Expects a "Filename" key in keyData object with the value used 
        /// to provide the path to the CSV file.</param>
        /// <returns>Specification list</returns>
        public SpecList GetLimit(StringDictionary keyData)
        {
            SpecList specList = new SpecList();
            
            string fileName = keyData["Filename"];

            Specification spec;
            using (CsvReader reader = new CsvReader())
            {
                reader.OpenFile(fileName);
                // elements on a single line
                string[] lineElems;
                // read the 1st line
                lineElems = reader.GetLine();
                int lineElemLen = lineElems.Length;                
                // Get the specification name from first line or the file name
                string specName;
                int lineNbr = 0;
                
                if (lineElemLen < 2)
                {
                    specName = lineElems[0];                    
                    // now need to get 1st limit in 2nd line
                    lineElems = reader.GetLine();
                    lineNbr++;
                }
                else
                {
                    specName = Path.GetFileNameWithoutExtension(fileName);                    
                }

                // create the specification object (Priority is always 1).
                spec = new Specification(specName, keyData, 1);
                // read the elements
                do
                {
                    lineNbr++;
                    
                    // convert the file line to a limit object
                    RawParamLimit limit;
                    try
                    {
                        // convert the line to a RawParamLimit class
                        limit = this.convertCsvLineToRawParamLimit(lineElems);
                    }
                    catch (Exception e)
                    {
                        // rethrow exception with line and file info
                        string errMsg = String.Format("Invalid line in PCAS file: '{0}', line {1}",
                            fileName, lineNbr);
                        throw new Exception(errMsg, e);
                    }
                    // add to spec
                    spec.Add(limit);
                }
                while ((lineElems = reader.GetLine()) != null);
            }
            // Add the sole spec to the spec list
            specList.Add(spec);
            // return the spec list
            return specList;
        }

        #endregion

        #region Private helper fns        
        private RawParamLimit convertCsvLineToRawParamLimit(string[] lineElems)
        {            
            // read off the limit info that we're interested in
            string shortDesc = lineElems[0].Trim();
            string limitMinStr = lineElems[2].Trim();
            string limitMaxStr = lineElems[3].Trim();
            string units = lineElems[4].Trim();
            string format = lineElems[5].Trim();
            string typeStr = lineElems[7].Trim();
            string priorityStr = lineElems[8].Trim();
            
            // what type is it
            object obj = Enum.Parse(typeof(PCASdataType), typeStr, true);
            PCASdataType dataType = (PCASdataType)obj;
            // priority
            int priority = Int32.Parse(priorityStr);
            // accuracy
            int accuracy = PcasLimitConvertUtils.GetAccuracy(dataType, format);
            
            // Calculate limits details in Test Engine terms
            LimitOperand operand;
            DatumType paramType;
            Datum lowLimit;
            Datum highLimit;
            // Default to "no limit" with null high and low limit datums unless the data type is numeric
            switch ( dataType )
            {
                case PCASdataType.PCAS_FLOAT_126:
                {
                    operand = LimitOperand.InRange;
                    paramType = DatumType.Double;
                    double limitMin = Double.Parse(limitMinStr);
                    double limitMax = Double.Parse(limitMaxStr);
                    lowLimit = new DatumDouble(shortDesc, limitMin);
                    highLimit = new DatumDouble(shortDesc, limitMax);
                    break;
                }
                case PCASdataType.PCAS_NUMBER_38:
                {
                    operand = LimitOperand.InRange;
                    // Look to see if we have a Pseudo PCAS Boolean, which is a PCAS Type of PCAS_NUMBER_38
                    // with units = "bool", and a Short Descriptor that starts with "BOOL_".
                    // Note that this is a Test Engine specific format.
                    Int32 limitMin = Int32.Parse(limitMinStr);
                    Int32 limitMax = Int32.Parse(limitMaxStr);

                    if ((string.Compare(units, "bool", true) == 0) && (shortDesc.IndexOf("BOOL_") == 0))
                    {
                        if (((limitMin != 0) && (limitMin != 1)) ||
                            ((limitMax != 0) && (limitMax != 1)))
                        {
                            //Error - this is supposed to be a pseudo boolean, where  0  = false, and 1 = true.                                    
                            throw new System.Exception("Invalid Boolean Limit For Parameter: " + shortDesc);
                        }
                        else
                        {
                            paramType = DatumType.BoolType;
                            lowLimit = new DatumBool(shortDesc, Convert.ToBoolean(limitMin));
                            highLimit = new DatumBool(shortDesc, Convert.ToBoolean(limitMax));
                        }
                    }
                    else
                    {
                        paramType = DatumType.Sint32;
                        lowLimit = new DatumSint32(shortDesc, checked(limitMin));	// Use a checked block to ensure that an overflow is raised if values are outside the acceptable range
                        highLimit = new DatumSint32(shortDesc, checked(limitMax));
                    }
                    break;
                }

                default:
                {
                    operand = LimitOperand.NoLimits;
                    paramType = PcasLimitConvertUtils.GetDatumTypeFromStringShortDesc(shortDesc);
                    lowLimit = null;
                    highLimit = null;
                    break;
                }
            }

            // construct and return the limit
            RawParamLimit rawLimit = new RawParamLimit(shortDesc, 
                paramType, lowLimit, highLimit, operand, priority, 
                accuracy, units);
            return rawLimit;
        }
        #endregion

    }
}
