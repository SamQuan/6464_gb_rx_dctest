// [Copyright]
//
// Bookham Modular Test Engine
// Utilities
//
// Utilities\Util_DelimitedFileAccess\DelimitedFileReader.cs
// 
// Author: Paul Annetts
// Design: 


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Delimiter for the delimited file
    /// </summary>
    public enum DelimitedFileDelimiter
    {
        /// <summary>Comma delimiter</summary>
        Comma,
        /// <summary>Tab delimiter</summary>        
        Tab,
        /// <summary>Space delimiter</summary>        
        Space,
    }

    /// <summary>
    /// Simple DelimitedFileReader class. No clever stuff with quote values in CSV elements!
    /// Can specify what separates the elements and whether to concatenate adjacent components.
    /// </summary>
    /// <remarks>This class is disposable, if disposed will close the file</remarks>
    public class DelimitedFileReader : IDisposable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public DelimitedFileReader(DelimitedFileDelimiter delimiter, bool noEmptyElements)
        {
            this.streamReader = null;
            this.delimiter = delimiter;
            this.noEmptyElements = noEmptyElements;
            this.delimiterChar = generateDelimiterChar(this.delimiter);
        }

        private char generateDelimiterChar(DelimitedFileDelimiter delimiter)
        {
            switch (delimiter)
            {
                case DelimitedFileDelimiter.Comma:
                    return ',';
                case DelimitedFileDelimiter.Tab:
                    return '\t';
                case DelimitedFileDelimiter.Space:
                    return ' ';
                default:
                    throw new ArgumentException("Invalid delimiter: " + delimiter);
            }
        }

        /// <summary>
        /// Open file
        /// </summary>
        /// <param name="ssvFile">File name to open</param>
        public void OpenFile(string ssvFile)
        {
            streamReader = new StreamReader(ssvFile);
        }


        /// <summary>
        /// Close file
        /// </summary>
        public void CloseFile()
        {
            if (streamReader == null)
            {
                throw new Exception("Can't close the CsvReader file, none open!");
            }
            this.Dispose();            
        }


        /// <summary>
        /// Get next line from file. Returns null if empty
        /// </summary>
        /// <returns>Array of strings for this line</returns>
        public string[] GetLine()
        {
            StringSplitOptions splitOptions;
            if (noEmptyElements)
            {
                splitOptions = StringSplitOptions.RemoveEmptyEntries; 
            }
            else
            {
                splitOptions = StringSplitOptions.None;
            }

            // read the next line
            string line = streamReader.ReadLine();
            // check for end of file (null).
            if (line == null) return null;
            string[] elemsInLine = line.Split(new char[] { this.delimiterChar }, splitOptions);
            return elemsInLine;
        }

        /// <summary>
        /// Reads the entire file
        /// </summary>
        /// <param name="ssvFile">File name to open</param>
        /// <returns>List of Array of strings representing the whole file. Array can/will be jagged...</returns>
        public List<string[]> ReadFile(string ssvFile)
        {
            this.OpenFile(ssvFile);
            List<string[]> fileLines = new List<string[]>();

            string[] lineElems;
            while (true)
            {
                // read the next line
                lineElems = this.GetLine();
                // check for end of file
                if (lineElems == null) break;
                fileLines.Add(lineElems);
            }
            this.CloseFile();
            return fileLines;            
        }


        #region IDisposable Members

        /// <summary>
        /// IDisposable method
        /// </summary>
        public void Dispose()
        {
            if (streamReader != null)
            {
                streamReader.Dispose();
                streamReader = null;
            }
        }

        #endregion
        
        #region Private Data
        private StreamReader streamReader;
        private DelimitedFileDelimiter delimiter;
        private char delimiterChar;
        private bool noEmptyElements;
        #endregion

       
    }
}
