// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestLibrary.Utilities
//
// SimpleInstrumentSharingHelper.cs
//
// Author: Cypress Chen, 2009.03
// Design: [Reference design documentation]
using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Equipment;
using System.Data;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// A class for instrument sharing in a simple lock way.
    /// </summary>
    public static class SimpleInstrumentSharingHelper
    {
        static SimpleInstrumentSharingHelper()
        {
            Util_FileLock.FileLocked += new EventHandler<FileLockedEventArgs>(Util_FileLock_FileLocked);
            Util_FileLock.FileUnlocked += new EventHandler<FileUnlockedEventArgs>(Util_FileLock_FileUnlocked);
        }

        #region private fields
        static ISwitch _switch;
        #endregion

        /// <summary>
        /// Initializes the class.
        /// </summary>
        /// <param name="instruments">Instrument collection including Shared 'Switch'. This parameter can be got from the InitCode method of test program.</param>
        /// <param name="configFileName">The configuration file name for this SimpleInstrumentSharingHelper.</param>
        public static void Initialize(InstrumentCollection instruments, string configFileName)
        {
            Initialize(instruments, configFileName, null);           
        }

        /// <summary>
        /// Initializes the class.
        /// </summary>
        /// <param name="instruments">Instrument collection including Shared 'Switch'. This parameter can be got from the InitCode method of test program.</param>
        /// <param name="configFileName">The configuration file name for this SimpleInstrumentSharingHelper.</param>
        /// <param name="switchInstrument">The switch instrument</param>
        public static void Initialize(InstrumentCollection instruments, string configFileName,ISwitch switchInstrument)
        {
            //*******************************
            #region 1.open config & Get value
            DataSet configDataSet = new DataSet();
            configDataSet.ReadXml(configFileName);
            DataTable configTable = configDataSet.Tables["InstrumentSharingConfig"];

            string instrumentSharingFunctionEnable = null;
            string switchValue = null;
            string sharedSwithName = null;
            string lockedFileName = null;
            int fileDetectedInterval_ms = 0;
            int timeoutForLock_ms = 0;
            int switchSettlingTime_ms = 0;
            string sharedInstrumentNames = null;
            foreach (DataRow row in configTable.Rows)
            {
                string parameterName = (string)row["ParameterName"];
                string value = (string)row["Value"];

                if (parameterName == "LockedFileName")
                {
                    lockedFileName = value;
                    continue;
                }

                if (parameterName == "SharedSwithName")
                {
                    sharedSwithName = value;
                    continue;
                }

                if (parameterName == "SwitchValue")
                {
                    switchValue = value;
                    continue;
                }

                if (parameterName == "InstrumentSharingFunctionEnable")
                {
                    instrumentSharingFunctionEnable = value.ToUpper();
                    continue;
                }

                if (parameterName == "FileDetectedInterval_ms")
                {
                    fileDetectedInterval_ms = int.Parse(value);
                    continue;
                }

                if (parameterName == "TimeoutForLock_ms")
                {
                    timeoutForLock_ms = int.Parse(value);
                    continue;
                }

                if (parameterName == "SwitchSettlingTime_ms")
                {
                    switchSettlingTime_ms = int.Parse(value);
                    continue;
                }

                if (parameterName == "SharedInstrumentNames")
                {
                    sharedInstrumentNames = value;
                    continue;
                }
            }

            #endregion

            //check whether the function is enabled.
            if (instrumentSharingFunctionEnable != "YES" && instrumentSharingFunctionEnable != "ENABLE")
            {
                Util_FileLock.Initialize("", 999, 999);
                return;
            }

            Util_FileLock.Initialize(lockedFileName, timeoutForLock_ms, fileDetectedInterval_ms);
            Instrument switchInst = null;
            if (switchInstrument == null)
            {
                try
                {
                    switchInst = instruments[sharedSwithName];
                }
                catch
                {
                    throw new Exception(string.Format("Please make sure shared switch name {0} exists in instrument config.", sharedSwithName));
                }
            }

            InitializeSwitch(switchInst, switchValue, switchSettlingTime_ms, switchInstrument);

            InitializeSharedInstruments(instruments, sharedInstrumentNames);
        }

        /// <summary>
        /// Disables the instrument sharing function.
        /// </summary>
        public static void Disable()
        {
            if (_switch == null)
            {
                return;
            }

            _switch = null;//set switch to null to disable the instrument sharing.
        }

        static void InitializeSwitch(Instrument instrument, string value, int switchSettlingTime_ms,ISwitch switchInstrument)
        {
            _switch = null;
            if (switchInstrument == null)
            {
                //1. if it's Electrical Source
                if (instrument is InstType_ElectricalSource)
                {
                    _switch = new ElectrialSourceSwitch();
                }

                //2. if it's simple switch.
                if (instrument is InstType_SimpleSwitch)
                {
                    _switch = new SimpleSwitch();
                }
            }
            else
            {
                _switch = switchInstrument;
            }

            if (_switch == null)
            {
                throw new InvalidOperationException("The instrument is not supported as a 'Switch' for sharing: " + instrument.GetType().ToString());
            }

            try
            {
                _switch.Initialize(instrument, value, switchSettlingTime_ms);
            }
            catch
            {
                _switch = null;
                throw;
            }
        }

        /// <summary>
        /// Initialized shared instruments--set them onlime.
        /// </summary>
        /// <param name="instruments"></param>
        /// <param name="sharedInstrumentNames"></param>
        static void InitializeSharedInstruments(InstrumentCollection instruments, string sharedInstrumentNames)
        {
            string[] sharedInstrumentNameArray = sharedInstrumentNames.Split(',');
            using (Util_FileLock fileLock = new Util_FileLock())
            {
                foreach (string instrumentName in sharedInstrumentNameArray)
                {
                    Instrument sharedInstrument = instruments[instrumentName];
                    if (sharedInstrument.InstrumentChassis.IsOnline == false)
                    {
                        sharedInstrument.InstrumentChassis.IsOnline = true;
                    }
                    if (sharedInstrument.IsOnline == false)
                    {
                        sharedInstrument.IsOnline = true;
                    }
                }
            }
        }

        #region file lock event response method
        static void Util_FileLock_FileUnlocked(object sender, FileUnlockedEventArgs e)
        {
        }

        static void Util_FileLock_FileLocked(object sender, FileLockedEventArgs e)
        {
            if (_switch == null)
            {
                return;
            }
            if (_switch.IsOnline == false)
            {
                return;
            }
            _switch.Switch();
        }
        #endregion



        #region Switch helper classes and interface

        class ElectrialSourceSwitch : ISwitch
        {
            private InstType_ElectricalSource _instrument;
            private bool _outputEnabled;
            private int _switchSettlingTime_ms;
            #region ISwitch Members

            public void Initialize(Instrument instrument, string value, int settlingTime_ms)
            {
                _instrument = (InstType_ElectricalSource)instrument;
                _outputEnabled = bool.Parse(value);
                _switchSettlingTime_ms = settlingTime_ms;
            }

            public void Switch()
            {
                if (_instrument.OutputEnabled != _outputEnabled)
                {
                    _instrument.OutputEnabled = _outputEnabled;
                    System.Threading.Thread.Sleep(_switchSettlingTime_ms);
                }
            }

            public bool IsOnline
            {
                get { return _instrument.IsOnline; }
            }

            #endregion
        }

        class SimpleSwitch : ISwitch
        {
            private InstType_SimpleSwitch _instrument;
            private int _channelNumber;
            private int _switchSettlingTime_ms;
            #region ISwitch Members

            public void Initialize(Instrument instrument, string value, int settlingTime_ms)
            {
                _instrument = (InstType_SimpleSwitch)instrument;
                _channelNumber = int.Parse(value);
                _switchSettlingTime_ms = settlingTime_ms;
            }

            public void Switch()
            {
                if (_instrument.SwitchState != _channelNumber)
                {
                    _instrument.SwitchState = _channelNumber;
                    System.Threading.Thread.Sleep(_switchSettlingTime_ms);
                }
            }

            public bool IsOnline
            {
                get { return _instrument.IsOnline; }
            }

            #endregion
        }

        /// <summary>
        /// Interface for Shared Switch
        /// </summary>
        public interface ISwitch
        {
            /// <summary>
            /// Initializes Switch.
            /// </summary>
            /// <param name="instrument"></param>
            /// <param name="value"></param>
            /// <param name="settlingTime_ms"></param>
            void Initialize(Instrument instrument, string value, int settlingTime_ms);
            /// <summary>
            /// Switches.
            /// </summary>
            void Switch();
            /// <summary>
            /// Gets whether the switch is online.
            /// </summary>
            bool IsOnline { get;}
        }


        #endregion

    }
}
