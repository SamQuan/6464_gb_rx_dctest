using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestControl
{
    internal enum enmTestOption
    {
        GoOnTest,
        CancelTest
    }
    /// <summary>
    /// this gui msg is use by batchViewCtl gui to send message to worker deciding that
    /// wether the load dut will go on test or not
    /// </summary>
    internal class DutTestOption
    {
        private enmTestOption dutTestOption;
        public DutTestOption(enmTestOption testOption)
        {
            dutTestOption = testOption;
        }

        internal enmTestOption TestOption
        {           
            get { return dutTestOption; }
            set { dutTestOption = value; }
        }
    }
}
