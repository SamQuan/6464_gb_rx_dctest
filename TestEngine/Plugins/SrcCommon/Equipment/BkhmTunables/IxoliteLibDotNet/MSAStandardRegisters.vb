Option Strict Off
Option Explicit On
Module MSAStandardRegisters
	
	
	' MSA command set. See the "I2C Reference Document for 300 Pin MSA 10G and 40G Transponders"
	
	Public Const SET_TX_COMMAND As Byte = &H40s
	Public Const READ_TX_COMMAND As Byte = &H41s
	Public Const SAVE_TX_COMMAND As Byte = &H42s
	Public Const RESTORE_TX_COMMAND As Byte = &H43s
	Public Const SET_RX_COMMAND As Byte = &H44s
	Public Const READ_RX_COMMAND As Byte = &H45s
	Public Const SAVE_RX_COMMAND As Byte = &H46s
	Public Const RESTORE_RX_COMMAND As Byte = &H47s
	Public Const SET_ITU_CHANNEL As Byte = &H49s
	Public Const READ_ITU_CHANNEL As Byte = &H4As
	Public Const SET_RX_THRESHOLD As Byte = &H4Bs
	Public Const READ_RX_THRESHOLD As Byte = &H4Cs
	Public Const SET_DEMUX_PHASE_OFFSET As Byte = &H4Ds ' Addition in MSA v4.1
	Public Const READ_DEMUX_PHASE_OFFSET As Byte = &H4Es ' Addition in MSA v4.1
	
	Public Const SET_TRANSMIT_CHIRP_VALUE As Byte = &H51s ' Addition in MSA v4.1
	Public Const READ_TRANSMIT_CHIRP_VALUE As Byte = &H52s ' Addition in MSA v4.1
	
	Public Const READ_LASER_BIAS_CURRENT As Byte = &H60s
	Public Const READ_LASER_OUTPUT_POWER As Byte = &H61s
	Public Const READ_SUBMOUNT_TEMP As Byte = &H62s
	Public Const READ_RX_AC_POWER As Byte = &H63s
	Public Const READ_RX_AVG_POWER As Byte = &H64s
	Public Const READ_LASER_WAVELENGTH As Byte = &H65s
	Public Const READ_MODULE_TEMP As Byte = &H66s
	Public Const READ_APD_TEMP As Byte = &H67s
	Public Const READ_MOD_BIAS As Byte = &H68s
	Public Const READ_LASER_OUTPUT_POWER_DBM As Byte = &H6As
	Public Const READ_RX_AVG_POWER_DBM As Byte = &H6Bs
	Public Const READ_LASER_ABSOLUTE_TEMP As Byte = &H6Cs ' Addition in MSA v4.1
	
	Public Const READ_TX_ALARM_STATUS As Byte = &H80s
	Public Const READ_RX_ALARM_STATUS As Byte = &H81s
	Public Const READ_PSU_ALARM_STATUS As Byte = &H82s
	Public Const READ_TX_ALARM_MASK As Byte = &H86s
	Public Const READ_RX_ALARM_MASK As Byte = &H84s
	Public Const READ_PSU_ALARM_MASK As Byte = &H88s
	Public Const SET_TX_ALARM_MASK As Byte = &H85s
	Public Const SET_RX_ALARM_MASK As Byte = &H83s
	Public Const SET_PSU_ALARM_MASK As Byte = &H87s
	Public Const READ_ALARM_SUMMARY As Byte = &H89s
	Public Const INTERRUPT_CONTROL As Byte = &H8As
	
	Public Const READ_SUPPLIER_CODE As Byte = &HA0s
	Public Const READ_MODULE_TYPE As Byte = &HA1s
	' Public Const READ_CUSTOMER_PARAMETERS As Byte = &HA2     ' See frmCustomerData
	' Public Const WRITE_CUSTOMER_PARAMETERS As Byte = &HA3    ' See frmCustomerData
	Public Const READ_FIRST_ITU_CHAN As Byte = &HA4s
	Public Const READ_LAST_ITU_CHAN As Byte = &HA5s
	Public Const READ_ITU_CHAN_SPACING As Byte = &HA6s
	Public Const READ_REVISION_CODES As Byte = &HA7s
	Public Const READ_UNIT_SERIAL_NUMBER As Byte = &HA8s
	Public Const READ_MANUFACTURE_DATE As Byte = &HA9s
	Public Const READ_UNIT_PART_NUMBER As Byte = &HAAs
	
	Public Const READ_LINK_STATUS As Byte = &HC0s
	Public Const PIN_CONTROL_MODE As Byte = &HC1s
	Public Const READ_MAX_I2C_RATE As Byte = &HC2s
	Public Const ENTER_PROTECT_MODE As Byte = &HC3s
	Public Const ENTER_VENDOR_PROTECT_MODE As Byte = &HC3s ' Same but needs a password.
	Public Const EXIT_PROTECT_MODE As Byte = &HC4s
	Public Const RESET_CPN As Byte = &HC5s
	Public Const SOFT_MODE As Byte = &HC6s
	Public Const READ_EDITION_AND_MODE As Byte = &HC7s
	Public Const DATA_LOOPBACK As Byte = &HC8s
	
	Public Const SETCAL As Byte = &HF4s
	Public Const GETCAL As Byte = &HF5s
	Public Const SETINDEXEDCAL As Byte = &HF6s
	Public Const GETINDEXEDCAL As Byte = &HF7s
	
	Public Const OIFDEVICEREG As Byte = &H5s
End Module