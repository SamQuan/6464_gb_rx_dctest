﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestModules.GuiMsg
{
    internal class VOASweepProgress
    {
        private double mProgressPct = 0;
        private string mSweepMsg = "";
        public VOASweepProgress()
        {

        }

        public string SweepMsg
        {
            get { return mSweepMsg; }
            set { mSweepMsg = value; }
        }

        public double Progress
        {
            get { return mProgressPct; }
            set { mProgressPct = value; }
        }

    }
}
