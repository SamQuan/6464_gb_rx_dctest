// [Copyright]
//
// Bookham [Coherence Rx DC Test ]
// Bookham.TestSolution.TestPrograms
//
// TP_CoRxPATest.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Config;
using System.IO;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// Program for Co Rx phase angle test
    /// </summary>
    public  class TP_CoRxPATest : ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private string specName;
        Specification MainSpec;

        TestParamConfigAccessor testParmConfig;
        TestParamConfigAccessor instrParamConfig;

        TempTableConfigAccessor tempParamConfig;

        ConfigDataAccessor instrumentsCalData;
        double[] testTemperatures = null;
        double[] freqArr2TestPhaseAngle = null;
        EnumFrequencySetMode freqSetMode = EnumFrequencySetMode.ByChan;

        Inst_CoRxITLALaserSource itla_sig = null;
        Inst_CoRxITLALaserSource itla_loc = null;

        DatumList chipsInfoList;
        DateTime time_Start;

        double VauxPd_X_V;
        double VauxPd_Y_V;
        #endregion

        #region ITestProgram Members
        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return typeof(TP_CoRxPATestGui); }
        }

        /// <summary>
        /// Program initialise
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SetVersionControl(false, false);
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui("Initialising ...");
            time_Start = DateTime.Now;

            foreach (Chassis chassisTemp in chassis.Values )
            {
                chassisTemp.EnableLogging = false;
            }

            foreach (Instrument instrTemp in instrs.Values )
            {
                instrTemp.EnableLogging = false;
            }

            LoadConfigFiles(engine, dutObject);
            // load specification
            SpecList specs = this.loadSpecs(engine,dutObject);

            // TODO: Get chips information
            if (testParmConfig.GetBoolParam("IsGetChipInfo"))
            {
                bool isPrevDataOK = ValidatePrevStageData(engine, dutObject);
                if (!isPrevDataOK)
                    engine.ErrorInProgram("the previous test data from PCAS or manual entry is not OK!");
            }
            else
            {
                chipsInfoList = null;
            }

            #region Get auxilary pd bais according to wafer id
            if (chipsInfoList != null)
            {
                string wfID = "";
                if (chipsInfoList.IsPresent("X_WAFER_ID")) wfID = chipsInfoList.ReadString("X_WAFER_ID");

                VauxPd_X_V = GetAuxPdBias_V(wfID);

                wfID = "";
                if (chipsInfoList.IsPresent("Y_WAFER_ID")) wfID = chipsInfoList.ReadString("Y_WAFER_ID");

                VauxPd_Y_V = GetAuxPdBias_V(wfID);
            }
            #endregion

            InitialiseInstrs(engine, instrs);        
            
            //Initialising test modules
            engine.SendStatusMsg("Initialising Test modules ");
            InitCaseTempModule(engine);
            if (testParmConfig.GetBoolParam("IsTestAux")) InitDarkCurrentModule(engine);   // Only run if Aux PDs exist

            InitPhaseAngleModules(engine, dutObject , (Inst_Ke24xx)instrs[instrParamConfig.GetStringParam("MasterSourceNameInTriggerLink")]);

            // Clean up the blob file folder
            CleanUpBlobFiles();
        }
        /// <summary>
        /// Run test program
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void Run(ITestEngineRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            
            bool isModuleRunOk = false;
            ButtonId userOpt= ButtonId .Cancel;
            
            engine.GuiToFront();
            engine.ShowContinueUserQuery("Please fix the DUT in the jig then continue until the the cover is close!");
            
            string modname; 
            DatumList tmData ; 
            engine.GuiToFront();
            
            if (engine.IsSimulation) return;
            engine.SendToGui(" DUT Test started, don't Move the DUT or fibre Connector with message promoted");
                        
            modname = string.Format("Setting case temp to {0}", testTemperatures[0]);
            
            tmData = engine.RunModule(modname).ModuleRunData.ModuleData;
            try
            {
                isModuleRunOk = tmData.ReadSint32("IsTemperatureSetOK") == 1;
            }
            catch  // (Exception ex)
            {
                engine.SendToGui("Case temperature can't set to " + testTemperatures[0].ToString() + "C");
                isModuleRunOk = false;
            }
            if (!isModuleRunOk)
            {
                userOpt = engine.ShowYesNoUserQuery("Case temperature can't set to " + 
                        testTemperatures[0].ToString() + "C\n Would you like to continue the sequencial temperature test or not?");
                if (userOpt == ButtonId.No)
                    engine.RaiseNonParamFail(0, "Fail to control case temperature to " + testTemperatures[0].ToString());
            }
            //test the local and signal Power
            //DatumList Power = ReadLocalAndSignalPower(engine, instrs);            
            //this.MainSpec.SetTraceData(Power);

         //   bool tempFlagContinue =testParmConfig.GetBoolParam("IsAbortTest@DarkCurrentFail");               
         //   engine.RunModule("Dark Current Test",!tempFlagContinue);

            

            engine .GuiToFront();
            engine .SendToGui(new GuiMsg .CautionOfLaserRqst());
            engine.SendToGui(" DUT Test started, don't Move the DUT or fibre Connector with message promoted");

            int idx_Temp = 0;  // Start from normal temperature 
            double temp;
            double temp_From = 0;
            double temp_To = 0;
            do  // Loop around all temperatures
            {
                engine.GuiShow();
                engine.GuiToFront();
                temp = testTemperatures[idx_Temp];                 
                modname = string.Format("Phase Angle test @{0}C", temp.ToString().Replace('-', 'M'));
                engine.RunModule(modname);

                // If the last temperature, recover to normal temperature and end  temperature loop 
                if (idx_Temp >= testTemperatures.Length - 1)  
                {
                    double temp_Recover = testParmConfig.GetDoubleParam("RecoverTemperature");
                    modname = string.Format("Recover from {0} to {1}", temp, temp_Recover);
                    engine.RunModule(modname);
                    break;                   
                }

                // set case temperature to the next
                do
                {
                    temp_From = testTemperatures[idx_Temp];
                    temp_To = testTemperatures[++idx_Temp];
                    modname = string.Format("Setting case temp to {0}", temp_To);
                    
                    try
                    {
                        tmData = engine.RunModule(modname).ModuleRunData.ModuleData;
                        isModuleRunOk = tmData.ReadSint32("IsTemperatureSetOK") == 1 ;
                    }
                    catch // (Exception ex)
                    {
                        engine.SendToGui("Case temperature can't set to " + temp_To.ToString() + "C");
                        isModuleRunOk = false;
                    }
                    if (!isModuleRunOk)
                    {
                        userOpt = engine.ShowYesNoUserQuery("Case temperature can't set to " +
                            temp_To.ToString() + "C\n Would you like to continue the sequencial temperature test or not?");
                        if (userOpt != ButtonId.Yes)   // recover to normal temperature and abort test
                        {
                            double temp_Recover = testParmConfig.GetDoubleParam("RecoverTemperature");
                            modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
                            engine.RunModule(modname);
                            break;
                        }
                    }
                } while (!isModuleRunOk && (idx_Temp< testTemperatures.Length-1) );

                if (!isModuleRunOk)  // if the case temperature can't set, abort the test
                    break;
            }
            while (true);

            
        }

        /// <summary>
        /// Read local and signal power
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        /// <returns>datumList power value</returns>
        private DatumList ReadLocalAndSignalPower(ITestEngineRun engine, InstrumentCollection instrs)
        {
            //read signal power
            engine.ShowContinueUserQuery("Please Connect signal fiber to the powerMeter  head , and test power !");
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_SigMon"];
            CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = true;  //open itla to test power;
            System.Threading.Thread.Sleep(200);
            
            double signalPower = CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ReadPower();
            CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = false;

            engine.ShowContinueUserQuery("Please Connect signal fiber to DUT , Then Continue !");
            //read local power
           
            engine.ShowContinueUserQuery("Please Connect the local fiber to the powerMeter,and test power !");
            CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = true;  //open itla to test power;
            System.Threading.Thread.Sleep(200);
            double localPower = CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ReadPower();
            CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = false;
            engine.ShowContinueUserQuery("Please Connect Local fiber to DUT , Then Continue !");

            //return data
            DatumList power = new DatumList();
            power.AddDouble("INPUT_POWER_SIGNAL", signalPower);
            power.AddDouble("INPUT_POWER_LOCAL", localPower);
            return power;
        }

        /// <summary>
        /// post run phase: shut down instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;
            // TODO: shutdown actions
            CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = false;
            
            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);
            bool bEnableInterLock = instrParamConfig.GetBoolParam("IsSourceWithInterLock");
            if (bEnableInterLock) CoRxDCTestInstruments.SetSourceInterLock(false);

            //KD10
            CoRxDCTestInstruments.TopCaseTec.OutputEnabled = false;//modified by chaoqun

        }
        /// <summary>
        /// generate test drop file
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, 
                    DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            // Write keys required for external data (example below for PCAS) 
            engine.BringTestResultsTabToFront();

            StringDictionary keys = new StringDictionary();
            keys.Add("SCHEMA", testParmConfig.GetStringParam("PCASSchema"));
            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", this.specName);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...
            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)
            // !(beware, all these parameters MUST exist in the specification)!...
            DatumList traceData = new DatumList();

            DateTime testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(time_Start);
            traceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            traceData.AddString("SPEC_ID", MainSpec.Name);
            traceData.AddString("TIME_DATE", time_Start.ToString("yyyyMMddHHmmss"));

            traceData.AddSint32("NODE", dutObject.NodeID);            
            traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            traceData.AddString("PART_CODE", dutObject.PartCode);

            if (engine.GetProgramRunStatus() == ProgramStatus.Success)
            {
                traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
            }
            else
            {
                string msg = engine.GetProgramRunStatus().ToString();
                string runInfo = msg.Substring(0, (msg.Length > 14 ? 14 : msg.Length));
                traceData.AddString("TEST_STATUS", runInfo);
            }
            traceData.AddString("SOFTWARE_IDENT",                
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            traceData.AddString("OPERATOR_ID", userList.UserListString);
            traceData.AddString("COMMENTS", engine.GetProgramRunComments());
            if (chipsInfoList != null) traceData.AddListItems(chipsInfoList);

            //XIAOJIANG 2017.03.20
            if (this.MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    traceData.AddOrUpdateString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    traceData.AddOrUpdateString("LOT_TYPE", "Unknown");

            if (this.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    traceData.AddOrUpdateString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    traceData.AddOrUpdateString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (this.MainSpec.ParamLimitExists("LOT_ID"))
                traceData.AddOrUpdateString("LOT_ID", dutObject.BatchID);
            if (this.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
                traceData.AddOrUpdateString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            //END ADD XIAOJIANG 2017.03.20

            traceData.AddDouble("TEMPERATURE_1", testTemperatures[0]);
            traceData.AddDouble("TEMPERATURE_2", testTemperatures[1]);
            traceData.AddDouble("TEMPERATURE_3", testTemperatures[2]);

            if (MainSpec.ParamLimitExists("TEST_WAVELENGTH_1"))
            traceData.AddDouble("TEST_WAVELENGTH_1", Math.Round(Freq_GHz_TO_Wave_nm(freqArr2TestPhaseAngle[0]), 3));
            if (MainSpec.ParamLimitExists("TEST_WAVELENGTH_2"))
            traceData.AddDouble("TEST_WAVELENGTH_2", Math.Round(Freq_GHz_TO_Wave_nm(freqArr2TestPhaseAngle[1]), 3));
            if (MainSpec.ParamLimitExists("TEST_WAVELENGTH_3"))
            traceData.AddDouble("TEST_WAVELENGTH_3", Math.Round(Freq_GHz_TO_Wave_nm(freqArr2TestPhaseAngle[2]), 3));


            traceData.AddString("X_WAFER_ID", "UNKNOWN");
            traceData.AddString("Y_WAFER_ID", "UNKNOWN");
            traceData.AddString("X_CHIP_BIN", "UNKNOWN");
            traceData.AddString("X_CHIP_ID", "UNKNOWN");
            traceData.AddString("X_COC_SN", "UNKNOWN");
            traceData.AddString("Y_CHIP_BIN", "UNKNOWN");
            traceData.AddString("Y_CHIP_ID", "UNKNOWN");
            traceData.AddString("Y_COC_SN", "UNKNOWN");



            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("TC1_") || paramLimit.ExternalName.StartsWith("TC2_"))
                {
                    if (!traceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            traceData.Add(dummyValue);
                    }
                }
            }
            #endregion
            // pick the specification to add this data to...

            if (dutObject.TestStage.ToLower().Contains("spc"))
            {
                DatumList spcList = new DatumList();

                foreach (ParamLimit item in this.MainSpec)
                {
                    if (item.MeasuredData == null)
                    {
                        continue;
                    }

                    switch (item.MeasuredData.Type)
                    {
                        case DatumType.BoolType:
                            spcList.AddBool(item.ExternalName, Convert.ToBoolean(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.DatumListType:
                            break;
                        case DatumType.Double:
                            spcList.AddDouble(item.ExternalName, Convert.ToDouble(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.DoubleArray:
                            break;
                        case DatumType.EnumType:
                            break;
                        case DatumType.FileLinkType:
                            break;
                        case DatumType.ObjectReference:
                            break;
                        case DatumType.ObjectValue:
                            break;
                        case DatumType.PlotType:
                            break;
                        case DatumType.Sint32:
                            spcList.AddSint32(item.ExternalName, Convert.ToInt32(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.StringArray:
                            break;
                        case DatumType.StringType:
                            break;
                        case DatumType.Uint32:
                            spcList.AddUint32(item.ExternalName, Convert.ToUInt32(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.Unknown:
                            break;
                        default:
                            break;
                    }
                }

                dutObject.Attributes.AddOrUpdateReference("SPC_RESULTS", spcList);


                SpcConfigReader.Initialise(dutObject.NodeID.ToString(), dutObject.TestStage);
                DatumList listSPCParams = SpcHelper.ReadSpcParams(dutObject.NodeID.ToString());

                if (listSPCParams != null)
                {
                    foreach (Datum item in listSPCParams)
                    {
                        if (spcList.GetDatumNameList().Contains(item.Name))
                        {
                            double measureVule = spcList.ReadDouble(item.Name);
                            string paramNameTemp;
                            if (item.Name.Length > 26)
                            {
                                if (item.Name.Contains("_XIP_XQP"))
                                    paramNameTemp = item.Name.Replace("_XIP_XQP", "_XIQ");
                                else if (item.Name.Contains("_YIP_YQP"))
                                    paramNameTemp = item.Name.Replace("_YIP_YQP", "_YIQ");
                                else
                                    paramNameTemp = item.Name;
                                traceData.AddOrUpdateDouble("DEL_" + paramNameTemp, measureVule - double.Parse(item.ValueToString()));
                            }
                            else
                            {
                                traceData.AddOrUpdateDouble("DEL_" + item.Name, measureVule - double.Parse(item.ValueToString()));
                            }

                           
                        }
                    }
                }
            }



            engine.SetTraceData(specName, traceData);
            
            ArchiveBlobfiles(dutObject);
        }

        #endregion


        #region Initialise function

        /// <summary>
        /// Load config file for test parameters, temperature setting and instrument calibration
        /// </summary>
        /// <param name="dutObj"></param>
        void LoadConfigFiles(ITestEngineInit engine, DUTObject dutObj)
        {
            engine.SendStatusMsg("Load configuration files");
            string configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\TempTable.xml", dutObj.NodeID);
            tempParamConfig = new TempTableConfigAccessor(dutObj, 1, configFilePath);

            testParmConfig = new TestParamConfigAccessor(dutObj,
               @"Configuration\CoRxDcTest\DCTestParams.xml", "ALL", "CoRxDCTestParams");

            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrumentParams.xml", dutObj.NodeID);
            instrParamConfig = new TestParamConfigAccessor(dutObj, configFilePath,"ALL", "InstrumentParams");

            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrsCalibration.xml", dutObj.NodeID);
            instrumentsCalData = new ConfigDataAccessor(configFilePath, "Calibration");

            // Get Test temperatures
            List<double> tempList = new List<double>();
            tempList.Add(testParmConfig.GetDoubleParam("NormalTemperature"));
            string[] tempStrArr = testParmConfig.GetStringParam("TemperatureArrayToTest").Split(',');
            if (tempStrArr != null && tempStrArr.Length > 0)
            {
                foreach (string temp in tempStrArr)
                {
                    if (temp .Trim() != "")  tempList.Add(double.Parse(temp));
                }
            }
            testTemperatures = tempList.ToArray();

            #region Get Freq/Wavelen/Chan array to test phase angle

            if (!testParmConfig.GetBoolParam("IsGetFreqsFromLimitFile"))
            {
                string freqArrStr = "";
                try
                {
                    freqArrStr = testParmConfig.GetStringParam("Freq2TestPhaseAngle");
                    freqSetMode = EnumFrequencySetMode.ByFrequency;
                }
                catch
                {
                    try
                    {
                        freqArrStr = testParmConfig.GetStringParam("Chan2TestPhaseAngle");
                        freqSetMode = EnumFrequencySetMode.ByChan;
                    }
                    catch
                    {
                        try
                        {
                            freqArrStr = testParmConfig.GetStringParam("Wavelen2TestPhaseAngle");
                            freqSetMode = EnumFrequencySetMode.ByWavelen;
                        }
                        catch
                        {
                            throw new ArgumentException("There is no any test frequency infromation in test parameter config file such as " +
                                    @"'Freq2TestPhaseAngle' 'Chan2TestPhaseAngle' ' Wavelen2TestPhaseAngle'");
                        }
                    }
                }
                if (freqArrStr.Trim() != "")
                {
                    string[] freqArrTemp = freqArrStr.Split(',');
                    List<double> freqList = new List<double>();
                    foreach (string freqTemp in freqArrTemp)
                    {
                        freqList.Add(double.Parse(freqTemp));
                    }
                    freqArr2TestPhaseAngle = freqList.ToArray();
                }
                else
                {
                    freqArr2TestPhaseAngle = null;
                }
            }
            
            #endregion

            CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsSmoothWaveform = 
                testParmConfig.GetBoolParam("IsSmoothWaveform");
            CoRxDCTestCommonData.CoRxChipPhaseAngleData.WaveformSmoothFactor =
                testParmConfig.GetIntParam("WavefromSmoothingFatator");
            CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate = testParmConfig.GetBoolParam("IsSinusoidalFitWithTemplate");
            CoRxDCTestInstruments.Is6464 = testParmConfig.GetBoolParam("Is6464");
            try
            {
                CoRxDCTestCommonData.CoRxChipPhaseAngleData.SinusoidalFitTemplatePath =
                    testParmConfig.GetStringParam("SinusoudalFitTemplatePath");
            }
            catch ( Exception ex)
            {
                CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate = false;
                engine.ErrorInProgram(ex.Message);
            }
        }

        /// <summary>
        /// Load specification
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        private SpecList loadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (testParmConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification

                #region Get loccal Limit file path
                // initialise limit file reader
                string limitFileName = "";
                string localLimitFileDir = testParmConfig.GetStringParam("PcasLimitFileDirectory");

                StringDictionary keys = new StringDictionary();
                keys.Add("TestStage", dutObject.TestStage);
                keys.Add("DeviceType", dutObject.PartCode);
                ConfigDataAccessor localLimitConfig = new ConfigDataAccessor(
                    Path.Combine(localLimitFileDir, testParmConfig.GetStringParam("LocalLimitStragety")),
                    testParmConfig.GetStringParam("LocalLimitTableName"));

                // Get file name from data base
                try
                {
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }
                catch
                {
                    keys.Remove("DeviceType");
                    System.Threading.Thread.Sleep(100);
                    keys.Add("DeviceType", "ALL");
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }

                if (limitFileName != "")
                {
                    limitFileName = Path.Combine(localLimitFileDir, limitFileName);
                    if (File.Exists(limitFileName))
                        mainSpecKeys.Add("Filename", limitFileName);
                    else
                        engine.ErrorInProgram("Can't find limit file at " + limitFileName);

                }  //LimitFileName!= ""
                else
                {
                    engine.ErrorInProgram("No local limit file available for device type " + dutObject.PartCode);
                }                
                #endregion

            }
            else  // get pcas limit file
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            // Get our specification object (so we can initialise modules with appropriate limits)
            MainSpec = tempSpecList[0];            
             
            if (testParmConfig.GetBoolParam("IsGetFreqsFromLimitFile") || (freqArr2TestPhaseAngle == null) )
            {
                List<double> lstWavelen = new List<double>();

                freqSetMode = EnumFrequencySetMode.ByWavelen;

                // use Try to avoid exception rise up if not all wavelen is provided by the limit file 
                try
                {
                    lstWavelen.Add (double.Parse(MainSpec.GetParamLimit("TC_WAVELENGTH_1").LowLimit.ValueToString()));
                }
                catch
                { }

                try
                {
                    lstWavelen.Add(double.Parse(MainSpec.GetParamLimit("TC_WAVELENGTH_2").LowLimit.ValueToString()));
                }
                catch
                { }
                try
                {
                    lstWavelen.Add(double.Parse(MainSpec.GetParamLimit("TC_WAVELENGTH_3").LowLimit.ValueToString()));
                }
                catch
                { }

                freqArr2TestPhaseAngle = lstWavelen .ToArray();
            }

            SpecList specList = new SpecList();
            specName = MainSpec.Name;
            specList.Add(MainSpec);
            engine.SetSpecificationList(specList);
            return specList;
        }

        /// <summary>
        /// Retrieve & verify test parameter from previous stage
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObj"></param>
        /// <returns></returns>
        bool ValidatePrevStageData(ITestEngineInit engine, DUTObject dutObj)
        {
            bool isDataRetrieved = false;

            bool isPcasData = testParmConfig.GetBoolParam("IsRetrieveChipInfoFromPcas");
            if (isPcasData)
            {
                engine.SendStatusMsg("Retrieving previous data from PCAS...");
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                //2008-3-24: by Ken: Determine whether tcmz_map stage tested.
                StringDictionary preStageKeys = new StringDictionary();
                preStageKeys.Add("SCHEMA", testParmConfig.GetStringParam("ChipInfoPcasSchema"));
                preStageKeys.Add("SERIAL_NO", dutObj.SerialNumber);
                preStageKeys.Add("TEST_STAGE", testParmConfig.GetStringParam("StageName2RetrieveChipInfo"));

                DatumList prevData = dataRead.GetLatestResults(preStageKeys, true);

                if (prevData == null)
                {
                    string errorDescription = string.Format("No data be retrieve from {0} stage {1}," +
                        " could you please check the previous data to ensure enough data for this stage?",
                        testParmConfig.GetStringParam("ChipInfoPcasSchema"),
                        testParmConfig.GetStringParam("StageName2RetrieveChipInfo"));
                    engine.ErrorInProgram(errorDescription);
                }
                try
                {
                    chipsInfoList = new DatumList();
                    chipsInfoList.AddString("X_CHIP_ID", prevData.ReadString("X_CHIP_ID"));
                    chipsInfoList.AddString("X_CHIP_BIN", prevData.ReadString("X_CHIP_BIN"));
                    chipsInfoList.AddString("X_COC_SN", prevData.ReadString("X_COC_SN"));
                    chipsInfoList.AddString("X_WAFER_ID", prevData.ReadString("X_WAFER_ID"));
                    chipsInfoList.AddString("Y_CHIP_ID", prevData.ReadString("Y_CHIP_ID"));
                    chipsInfoList.AddString("Y_CHIP_BIN", prevData.ReadString("Y_CHIP_BIN"));
                    chipsInfoList.AddString("Y_COC_SN", prevData.ReadString("Y_COC_SN"));
                    chipsInfoList.AddString("Y_WAFER_ID", prevData.ReadString("Y_WAFER_ID"));
                    isDataRetrieved = true;
                }
                catch
                {
                    isDataRetrieved = false;
                    engine.ErrorInProgram("Not all neccesary data required by this stage is retrieve from previous stage," +
                        "\nTest on this stage can't go on, please check the previous data");
                }
            }
            else
            {
                engine.GuiToFront();
                engine.SendToGui(new GuiMsg.GetChipInfoRqst());
                engine.GuiUserAttention();
                engine.SendStatusMsg("Constructing engineering previous test data");

                DatumList previousData = (DatumList)engine.ReceiveFromGui().Payload;
                engine.GuiCancelUserAttention();
                if (previousData == null || previousData.Count < 3)
                    engine.ErrorInProgram("Not enough previous data entry to carry test on this stage");
                chipsInfoList = new DatumList();
                chipsInfoList.AddListItems(previousData);
                isDataRetrieved = true;
            }
            return isDataRetrieved;
        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        private void InitialiseInstrs(ITestEngineInit engine, InstrumentCollection instrs)
        {
            engine.SendToGui("Initialising instruments ...");
            engine.SendStatusMsg("Initialising instruments ...");
            #region Assign instruments
             
            CoRxDCTestInstruments.SignalInputInstrs = new CoRxSourceInstrsChain();

            itla_sig = new Inst_CoRxITLALaserSource("iTLA_Sig",(Instr_ITLA)instrs["iTLA_Sig"]);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource =  itla_sig ;
            CoRxDCTestInstruments.SignalInputInstrs.Voa = null; // (InstType_OpticalAttenuator)instrs["VOA_Sig"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_SigMon"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_SigRef"];

            CoRxDCTestInstruments.LocalOscInputInstrs = new CoRxSourceInstrsChain();
            itla_loc = new Inst_CoRxITLALaserSource("iTLA_Loc",(Instr_ITLA)instrs["iTLA_Loc"]);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource =   itla_loc;
            CoRxDCTestInstruments.LocalOscInputInstrs.Voa = null; // (InstType_OpticalAttenuator)instrs["VOA_Loc"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_LocMon"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_LocRef"];
                        
            CoRxDCTestInstruments.TopCaseTec = (Inst_Nt10a)instrs["CaseTec"];
            CoRxDCTestInstruments.IsDualCaseTecControl = instrParamConfig.GetBoolParam("IsUseBottomTEC");
            if (CoRxDCTestInstruments.IsDualCaseTecControl)
            {
                CoRxDCTestInstruments.BottomCaseTec = (Inst_Nt10a)instrs["BottomCaseTec"];
            }
            else
                CoRxDCTestInstruments.BottomCaseTec = null;
            CoRxDCTestInstruments.inSPI = (Instr_SPI)instrs["inSPI"];

            CoRxDCTestInstruments.PdSource_AUX_XR = (Inst_Ke24xx)instrs["AuxPdSource_XR"];
            CoRxDCTestInstruments.PdSource_AUX_XL = (Inst_Ke24xx)instrs["AuxPdSource_XL"];
            CoRxDCTestInstruments.PdSource_AUX_YL = (Inst_Ke24xx)instrs["AuxPdSource_YL"];
            CoRxDCTestInstruments.PdSource_AUX_YR = (Inst_Ke24xx)instrs["AuxPdSource_YP"];
            CoRxDCTestInstruments.IsAuxPDOnFront = instrParamConfig.GetBoolParam("IsAuxPdOnFrontTernimal");

            CoRxDCTestInstruments.PdSource_XI1 = (Inst_Ke24xx)instrs["PdSource_XI1"];
            CoRxDCTestInstruments.PdSource_XI2 = (Inst_Ke24xx)instrs["PdSource_XI2"];
            CoRxDCTestInstruments.PdSource_XQ1 = (Inst_Ke24xx)instrs["PdSource_XQ1"];
            CoRxDCTestInstruments.PdSource_XQ2 = (Inst_Ke24xx)instrs["PdSource_XQ2"];

            CoRxDCTestInstruments.PdSource_YI1 = (Inst_Ke24xx)instrs["PdSource_YI1"];
            CoRxDCTestInstruments.PdSource_YI2 = (Inst_Ke24xx)instrs["PdSource_YI2"];
            CoRxDCTestInstruments.PdSource_YQ1 = (Inst_Ke24xx)instrs["PdSource_YQ1"];
            CoRxDCTestInstruments.PdSource_YQ2 = (Inst_Ke24xx)instrs["PdSource_YQ2"];
            CoRxDCTestInstruments.TriggerInLine = instrParamConfig.GetIntParam("SlaveSourceTriggerInLine");
            CoRxDCTestInstruments.TriggerOutLine = instrParamConfig.GetIntParam("SlaveSourceTriggerOutputLine");
            CoRxDCTestInstruments.TriggerNoUsedLine = instrParamConfig.GetIntParam("SlaveSourceTriggerNoUseLine");

            CoRxDCTestInstruments.TiaSource_X = (Inst_Ke24xx)instrs["TIASource_X"];
            CoRxDCTestInstruments.TiaSource_Y = (Inst_Ke24xx)instrs["TIASource_Y"];

            CoRxDCTestInstruments.IsBiasPDTap = testParmConfig. GetBoolParam("IsPdTapBias");
            CoRxDCTestInstruments.PdTapSource = null;

            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                try
                {
                    CoRxDCTestInstruments.PdTapSource = (Inst_Ke24xx)instrs["TapSource"];
                }
                catch
                {
                    engine.ErrorInProgram("Can't get source available to bais Tap PD");
                }
            }

            CoRxDCTestInstruments.VOASource = null;
            try
            {
                CoRxDCTestInstruments.VOASource = (Inst_Ke24xx)instrs["VoaSource"];
            }
            catch
            { engine.ErrorInProgram("No instrument available to bias VOA"); }


            try
            {
                CoRxDCTestInstruments.GCSource_X = (InstType_ElectricalSource)instrs["GCSource_X"];
                CoRxDCTestInstruments.GCSource_Y = (InstType_ElectricalSource)instrs["GCSource_Y"];
                CoRxDCTestInstruments.MCSource_X = (InstType_ElectricalSource)instrs["MCSource_X"];
                CoRxDCTestInstruments.MCSource_Y = (InstType_ElectricalSource)instrs["MCSource_Y"];
                CoRxDCTestInstruments.OaSource_X = (InstType_ElectricalSource)instrs["OaSource_X"];
                CoRxDCTestInstruments.OaSource_Y = (InstType_ElectricalSource)instrs["OaSource_Y"];
                CoRxDCTestInstruments.OCSource_X = (InstType_ElectricalSource)instrs["OcSource_X"];
                CoRxDCTestInstruments.OCSource_Y = (InstType_ElectricalSource)instrs["OcSource_Y"];
                CoRxDCTestInstruments.ThermPhaseSource_L = (InstType_ElectricalSource)instrs["ThermPhaseSource_L"];
                CoRxDCTestInstruments.ThermPhaseSource_R = (InstType_ElectricalSource)instrs["ThermPhaseSource_R"];
            }
            catch
            { }

            CoRxDCTestInstruments.PhaseAngleLaserSource = new Inst_CoRxITLALaserSource ("PhaseAngle Laser Source", (Instr_ITLA)instrs["iTLA_PA"]);
            CoRxDCTestInstruments.OSC_XI = (Inst_Tds3034)instrs["OSC_XI"];
            CoRxDCTestInstruments.OSC_XQ = (Inst_Tds3034)instrs["OSC_XQ"];
            CoRxDCTestInstruments.OSC_YI = (Inst_Tds3034)instrs["OSC_YI"];
            CoRxDCTestInstruments.OSC_YQ = (Inst_Tds3034)instrs["OSC_YQ"];

            CoRxDCTestInstruments.SigPathPolAdjustInstrs = new CoRxPolarisationAdjustInstrs();

            List<Inst_HP11896A> polList= new List<Inst_HP11896A>();
            polList.Add((Inst_HP11896A)instrs["sigOpc_1"]);
            polList.Add((Inst_HP11896A)instrs["sigOpc_2"]);
            polList.Add((Inst_HP11896A)instrs["sigOpc_3"]);
            polList.Add((Inst_HP11896A)instrs["sigOpc_4"]);
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolController = polList.ToArray();

            List<Inst_Ke24xx> curSourceList = new List<Inst_Ke24xx>();
            curSourceList.Add(CoRxDCTestInstruments.PdSource_AUX_XL);
            curSourceList.Add(CoRxDCTestInstruments.PdSource_AUX_YL);
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.LstCurrSources = curSourceList;

            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolInitPosition = new int[polList.Count];
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolInitPosition[0] = instrParamConfig.GetIntParam("SigPolarizationInitPosition_Ch1");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolInitPosition[1] = instrParamConfig.GetIntParam("SigPolarizationInitPosition_Ch2");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolInitPosition[2] = instrParamConfig.GetIntParam("SigPolarizationInitPosition_Ch3");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolInitPosition[3] = instrParamConfig.GetIntParam("SigPolarizationInitPosition_Ch4");

            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolAdjustStep = new int[polList.Count];
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolAdjustStep[0] = instrParamConfig.GetIntParam("SigPolarizationPositionStep_Ch1");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolAdjustStep[1] = instrParamConfig.GetIntParam("SigPolarizationPositionStep_Ch2");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolAdjustStep[2] = instrParamConfig.GetIntParam("SigPolarizationPositionStep_Ch3");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.ArrPolAdjustStep[3] = instrParamConfig.GetIntParam("SigPolarizationPositionStep_Ch4");

            CoRxDCTestInstruments.SigPathPolAdjustInstrs.MinCurrentTolerance_A = instrParamConfig.GetDoubleParam("SigPolarizationAdjustCurrTolerance_A");
            CoRxDCTestInstruments.SigPathPolAdjustInstrs.Tsettle_mS = instrParamConfig.GetIntParam("SigPolarizationSettleTime_mS");

            #endregion

            #region setting measurement  parameters            

            CoRxDCTestInstruments.LocalOscInputInstrs.sngVoaDefAtt_dB = testParmConfig.GetDoubleParam("InitAtt_LocVoa_dB");
            CoRxDCTestInstruments.SignalInputInstrs.sngVoaDefAtt_dB = testParmConfig.GetDoubleParam("InitAtt_SigVoa_dB");

            CoRxDCTestInstruments.SignalInputInstrs.PowerSetTolerance_dB = testParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.PowerSetTolerance_dB = testParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.MaxPowerTuningCount = testParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");
            CoRxDCTestInstruments.SignalInputInstrs.MaxPowerTuningCount = testParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");

            string configFilePath = instrParamConfig.GetStringParam("SplitterCalDataBaseConfigPath");
            string configTableName = instrParamConfig.GetStringParam("SplitterCalDataBaseConfigTableName");
            string sigSplitterName = instrParamConfig.GetStringParam("SignalPathSplitterName");
            string locSplitterName = instrParamConfig.GetStringParam("LocPathSplitterName");

            try
            {
                CoRxDCTestInstruments.SignalInputInstrs.SetupSplitterRatioData(
                    sigSplitterName, configFilePath, configTableName);
                CoRxDCTestInstruments.SignalInputInstrs.SplitterRatiosCalData.FrequencyTolerance_Ghz =
                    instrParamConfig.GetDoubleParam("FreqTolerance_Ghz");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.SignalInputInstrs.IsSplitterRatioCalOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Signal path, Test have to Abort!");

            try
            {
                CoRxDCTestInstruments.LocalOscInputInstrs.SetupSplitterRatioData(
                    locSplitterName, configFilePath, configTableName);
                CoRxDCTestInstruments .LocalOscInputInstrs .SplitterRatiosCalData.FrequencyTolerance_Ghz =
                    instrParamConfig.GetDoubleParam("FreqTolerance_Ghz");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.LocalOscInputInstrs.IsSplitterRatioCalOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Loc oscilatior path, Test have to Abort!");
           
            #endregion

            #region Initialise instruments

            if (!engine.IsSimulation)  // if testset on line, initialise instruments
            {
                // Seting powermeters calibration value
                double calFactor, calOffset;
                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.ReferencePower = calOffset;

                CoRxDCTestInstruments.SignalInputInstrs.MaxShutDownPower_dBm = instrParamConfig.GetDoubleParam("LocPathMaxShutDownPower_dBm");
                CoRxDCTestInstruments.LocalOscInputInstrs.MaxShutDownPower_dBm = instrParamConfig.GetDoubleParam("SigPathMaxShutDownPower_dBm");

                // Shut down laser source for safe                
                CoRxDCTestInstruments.SelectItla(itla_sig);
                if (itla_sig != null ) itla_sig.CurrentChan = 1;
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.OutputEnable = false;
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.Power_dBm = instrParamConfig.GetDoubleParam("SignalSourcePower@Test_dBm");

                System.Threading.Thread.Sleep(100);
                CoRxDCTestInstruments.SelectItla(itla_loc);
                if (itla_loc != null ) itla_loc.CurrentChan = 1;
                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.OutputEnable = false;
                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.Power_dBm = instrParamConfig.GetDoubleParam("LocSourcePower@Test_dBm");

                System.Threading.Thread.Sleep(100);
                CoRxDCTestInstruments.SelectItla(CoRxDCTestInstruments.PhaseAngleLaserSource);
                CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = false;
                CoRxDCTestInstruments.PhaseAngleLaserSource.CurrentChan = 1;
                CoRxDCTestInstruments.PhaseAngleLaserSource.Power_dBm =(instrParamConfig.GetDoubleParam("PAItlaPower@Test_dBm"));

                CoRxDCTestInstruments.OSC_XI.SetDefaultState();
                CoRxDCTestInstruments.OSC_XQ.SetDefaultState();
                CoRxDCTestInstruments.OSC_YI.SetDefaultState();
                CoRxDCTestInstruments.OSC_YQ.SetDefaultState();

                // Zero all power meter modules' dark current
                try
                {
                    Inst_Ag816x_OpticalPowerMeter inst_8163 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon as Inst_Ag816x_OpticalPowerMeter;
                    inst_8163.SafeMode = false;
                    inst_8163.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;
                    //inst_8163.Range = instrParamConfig.GetDoubleParam("OpmRange_LocPath Mon_dBM");
                    //inst_8163.ZeroAllModuleDarkCurrent();

                    Inst_Ag816x_OpticalPowerMeter inst_8163_3 = CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                    inst_8163_3.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;
                    //inst_8163_3.Range = instrParamConfig.GetDoubleParam("OpmRange_SigPathRef_dBM");
                    inst_8163.SafeMode = false;

                    Inst_Ag816x_OpticalPowerMeter inst_8163_2 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                    inst_8163_2.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;
                    //inst_8163_2.Range = instrParamConfig.GetDoubleParam("OpmRange_LocPath Ref_dBM");
                    inst_8163_2.SafeMode = false;
                    //if (!object.ReferenceEquals(inst_8163.InstrumentChassis, inst_8163_2.InstrumentChassis))
                    //    inst_8163_2.ZeroAllModuleDarkCurrent();

                }
                catch
                { }

               
                // Initialise case tec controler
                CoRxDCTestInstruments.TopCaseTec.OutputEnabled = false;
                CoRxDCTestInstruments.TopCaseTec.OperatingMode = InstType_TecController.ControlMode.Temperature;
                CoRxDCTestInstruments.TopCaseTec.ProportionalGain = instrParamConfig.GetDoubleParam("TecCase_PropotionGain");
                CoRxDCTestInstruments.TopCaseTec.IntegralGain = instrParamConfig.GetDoubleParam("TecCase_IntegrationGain");
                CoRxDCTestInstruments.TopCaseTec.TecCurrentCompliance_amp = instrParamConfig.GetDoubleParam("TecCase_CurrentCompliance_amp");
                CoRxDCTestInstruments.TopCaseTec.SensorTemperatureSetPoint_C = testTemperatures[0];
                CoRxDCTestInstruments.TopCaseTec.OutputEnabled = true;

                // initialise bias sources
                CoRxDCTestInstruments.InitialiseAllSource();
                bool bEnableInterLock = instrParamConfig.GetBoolParam("IsSourceWithInterLock");
                CoRxDCTestInstruments.SetSourceInterLock(bEnableInterLock);
            }
            #endregion
        }

        /// <summary>
        /// Initialise modules for all case temperatue ivolves in the whole test 
        /// </summary>
        /// <param name="engine"></param>
        void InitCaseTempModule(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising case temperature controlling modules");
            
            // Setup 25 ~ 25c module
            double temp_From = testParmConfig.GetDoubleParam("RoomTemperature");
            double temp_To = testTemperatures[0];
            string modname = string.Format("Setting case temp to {0}", temp_To);
            SetCaseTempControl(engine, modname, temp_From, temp_To,false );

            double temp_Recover = testParmConfig.GetDoubleParam("RecoverTemperature");
            modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
            SetCaseTempControl(engine, modname, temp_To, temp_Recover, false);
            
            if (testTemperatures.Length > 1)
            {                
                for (int indx = 1; indx < testTemperatures.Length; indx++)
                {
                    temp_From = testTemperatures[indx-1];
                    temp_To = testTemperatures[indx];
                    modname = string.Format("Setting case temp to {0}", temp_To);
                    SetCaseTempControl(engine, modname, temp_From, temp_To,false);

                    // add a module to recover from this temperature to the recovered temperature
                    modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
                    SetCaseTempControl(engine, modname, temp_To, temp_Recover,false);
                }
            }
            
        }

        /// <summary>
        /// Initialise case temperature control module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="modName"> module name </param>
        /// <param name="temp_From"> case temperature to start runing this module </param>
        /// <param name="temp_To">  case temperature to be achieved </param>
        /// <param name="isSaveReslt"> If link temperature setting result to limit file </param>
        void SetCaseTempControl(ITestEngineInit engine, string modName,
                            double temp_From, double temp_To, bool isSaveReslt)
        {
            // Get TEC Controller parameter from config file and set to it
            DatumDouble dt_timeout_s = new DatumDouble("MaxExpectedTimeForOperation_s",
                instrParamConfig.GetDoubleParam("TecCase_Timeout_S"));
            DatumSint32 dt_intval; 
            double temp_setting = 0;
            TempTablePoint[] t_points;
            t_points = tempParamConfig.GetTemperatureCal(temp_From, temp_To);
            if (t_points.Length < 1)
                engine.ErrorInProgram(string.Format("Can't find temperature setting in temperature configure file" +
                    " with temperature from {0} to {1}", temp_From, temp_To));
            temp_setting = temp_To + t_points[0].OffsetC;

            ModuleRun module;
            if (CoRxDCTestInstruments .IsDualCaseTecControl)  // Stack tec control
            {
                // the following param varies with setting temp
                dt_intval = new DatumSint32("TimeBetweenReadings_ms",
                            instrParamConfig.GetIntParam("TecCase_UpdateTime_mS"));
                DatumDouble TopTec_RqdTimeInToleranceWindow_s = new DatumDouble("TopTec_RqdTimeInToleranceWindow_s",
                    instrParamConfig.GetDoubleParam("TecCase_InTolerance_S"));
                DatumDouble dt_TopTecStable_s = new DatumDouble("TopTec_RqdStabilisationTime_s", t_points[0].DelayTime_s);
                DatumDouble dt_TopTecSet = new DatumDouble("TopTec_SetPointTemperature_C", temp_setting);
                DatumDouble dt_TopTecTol = new DatumDouble("TopTec_TemperatureTolerance_C", t_points[0].Tolerance_degC);

                DatumDouble dt_BottomTecTimeout_s = new DatumDouble("BtmTec_RqdTimeInToleranceWindow_s",
                    instrParamConfig.GetDoubleParam("BottomTecCase_InTolerance_S"));
                DatumDouble dt_BottomTecTstable_s = new DatumDouble("BtmTec_RqdStabilisationTime_s",
                    instrParamConfig.GetDoubleParam("BottomTecCase_StabiliseTime_S"));
                DatumDouble dt_BottomTecDegSet = new DatumDouble("BtmTec_SetPointTemperature_C",
                    instrParamConfig.GetDoubleParam(
                    "BottomTecCaseDegSet_" + temp_To.ToString("###").Replace('-', 'M') + "C"));
                DatumDouble BtmTec_TemperatureTolerance_C = new DatumDouble("BtmTec_TemperatureTolerance_C",
                    instrParamConfig.GetDoubleParam("BottomTecCase_Tolerance_C"));

                module = engine.AddModuleRun(modName, "StackedTecControl", "");
                module.Instrs.Add("BtmTec_Controller", CoRxDCTestInstruments.BottomCaseTec);
                module.Instrs.Add("TopTec_Controller", CoRxDCTestInstruments.TopCaseTec);

                module.ConfigData.Add(dt_TopTecSet);
                module.ConfigData.Add(dt_TopTecStable_s);
                module.ConfigData.Add(dt_TopTecTol);
                module.ConfigData.Add(TopTec_RqdTimeInToleranceWindow_s);

                module.ConfigData.Add(dt_BottomTecDegSet);
                module.ConfigData.Add(dt_BottomTecTimeout_s);
                module.ConfigData.Add(dt_BottomTecTstable_s);
                module.ConfigData.Add(BtmTec_TemperatureTolerance_C);

            }
            else  // Single Tec Control
            {
                DatumDouble dt_TargetTemp = new DatumDouble("TargetTemperature_C", temp_To);
                DatumDouble dt_Tstable_s = new DatumDouble("RqdStabilisationTime_s", t_points[0].DelayTime_s);
                DatumDouble dt_DegSet_C = new DatumDouble("SetPointTemperature_C", temp_setting);
                DatumDouble dt_DegTol = new DatumDouble("TemperatureTolerance_C", t_points[0].Tolerance_degC);
                dt_intval = new DatumSint32("TempTimeBtwReadings_ms",
                    instrParamConfig.GetIntParam("TecCase_UpdateTime_mS"));

                module = engine.AddModuleRun(modName, "SimpleTempControl", "");
                module.Instrs.Add("Controller", CoRxDCTestInstruments.TopCaseTec );

                module.ConfigData.Add(dt_TargetTemp);
                module.ConfigData.Add(dt_Tstable_s);
                module.ConfigData.Add(dt_DegSet_C);
                module.ConfigData.Add(dt_DegTol);
            }
            module.ConfigData.Add(dt_timeout_s);
            module.ConfigData.Add(dt_intval);

            if (isSaveReslt)
            {
                string tempStr = temp_To.ToString("#").Replace('-', 'M');
                string paramName = string.Format("TEMPERATURE_ROOM_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "ActualTempReading");
                tempStr = temp_To.ToString("#").Replace('-', 'M');
                paramName = string.Format("TEMPERATURE_DUT_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "TargetTemp");
                paramName = string.Format("TEMPERATURE_SET_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "IsTemperatureSetOK");
            }
        }
        

        /// <summary>
        /// Initialise module for darkcurrent test
        /// </summary>
        /// <param name="engine"></param>
        void InitDarkCurrentModule(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising Dark current test modules ...");
            ModuleRun module = engine.AddModuleRun("Dark Current Test", "TM_InitialiseTest", "");
            module.ConfigData.AddDouble("PDBias_V", testParmConfig.GetDoubleParam("Vpd@DarkCurrent_V"));
            module.ConfigData.AddDouble("PDCompCurrent_A", testParmConfig.GetDoubleParam("IpdCompliant@DarkCurrent_A"));
            module.ConfigData.AddDouble("TiaBias_V", testParmConfig.GetDoubleParam("Vcc_V"));
            module.ConfigData.AddDouble("TIACompCurrent_A", testParmConfig.GetDoubleParam("IccComliant@DarkCurrent_A"));

            module.ConfigData.AddDouble("AuxPDBias_X_V", VauxPd_X_V);
            module.ConfigData.AddDouble("AuxPDBias_Y_V", VauxPd_Y_V);

            module.ConfigData.AddDouble("AuxPDCompCurrent_X_A", testParmConfig.GetDoubleParam("IauxPdCompliant_X@DarkCurrent_A"));
            module.ConfigData.AddDouble("AuxPDCompCurrent_Y_A", testParmConfig.GetDoubleParam("IauxPdCompliant_Y@DarkCurrent_A"));


            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                module.ConfigData.AddDouble("PDTapBias_V", testParmConfig.GetDoubleParam("VPdTap_V"));
                module.ConfigData.AddDouble("PDTapCompCurrent_A", testParmConfig.GetDoubleParam("IPdTapCompliance_A"));

            }
            
            module.Limits.AddParameter(MainSpec, "AUX_BIAS_SIG_X", "AuxPDBias_X_V");
            module.Limits.AddParameter(MainSpec, "AUX_BIAS_SIG_Y", "AuxPDBias_Y_V");
        }
        

        /// <summary>
        /// Initialise responsivity modules under all temperature
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        ///<param name="masterSource"> Source to use as master trigger source </param>
        void InitPhaseAngleModules(ITestEngineInit engine, DUTObject dutObject, Inst_Ke24xx masterSource)
        {
            string extParamName;

            engine.SendToGui("Initialising Phase angle test modules ...");
            
            for (int idx = 0; idx < testTemperatures.Length; idx++)
            {
                
                string temp = testTemperatures[idx].ToString().Replace('-','M');
                string moduleName = string.Format("Phase Angle test @{0}C", temp);

                ModuleRun module = engine.AddModuleRun(moduleName, "TM_CoRxPhaseAngle", "");
                if (idx == 0)
                {
                    module.ConfigData.AddBool("SavePdCurrents", true);
                    if (MainSpec.ParamLimitExists("XIP_CURRENT_WL1"))
                        module.Limits.AddParameter(MainSpec, "XIP_CURRENT_WL1", "XIP_CURRENT_WL0");
                    if (MainSpec.ParamLimitExists("XIP_CURRENT_WL2"))
                        module.Limits.AddParameter(MainSpec, "XIP_CURRENT_WL2", "XIP_CURRENT_WL1");
                    if (MainSpec.ParamLimitExists("XIP_CURRENT_WL3"))
                        module.Limits.AddParameter(MainSpec, "XIP_CURRENT_WL3", "XIP_CURRENT_WL2");

                    if (MainSpec.ParamLimitExists("YIP_CURRENT_WL1"))
                        module.Limits.AddParameter(MainSpec, "YIP_CURRENT_WL1", "YIP_CURRENT_WL0");
                    if (MainSpec.ParamLimitExists("YIP_CURRENT_WL2"))
                        module.Limits.AddParameter(MainSpec, "YIP_CURRENT_WL2", "YIP_CURRENT_WL1");
                    if (MainSpec.ParamLimitExists("YIP_CURRENT_WL3"))
                        module.Limits.AddParameter(MainSpec, "YIP_CURRENT_WL3", "YIP_CURRENT_WL2");

                }
                else module.ConfigData.AddBool("SavePdCurrents", false);

                module.ConfigData.AddDouble("TiaBias_V", testParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", testParmConfig.GetDoubleParam("IccComliant@Pa_A"));
                module.ConfigData.AddDouble("PDBias_V", testParmConfig.GetDoubleParam("Vpd_V"));
                module.ConfigData.AddDouble("PDCompCurrent_A", testParmConfig.GetDoubleParam("IpdCompliant@Pa_A"));
                
                module.ConfigData.AddDouble("AuxPDBias_X_V", VauxPd_X_V);
                module.ConfigData.AddDouble("AuxPDBias_Y_V", VauxPd_Y_V);
                module.ConfigData.AddDouble("AuxPDCompCurrent_X_A", testParmConfig.GetDoubleParam("IauxPdCompliant_X@Pa_A"));
                module.ConfigData.AddDouble("AuxPDCompCurrent_Y_A", testParmConfig.GetDoubleParam("IauxPdCompliant_Y@Pa_A"));

                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    module.ConfigData.AddDouble("PDTapBias_V", testParmConfig.GetDoubleParam("VPdTap_V"));
                    module.ConfigData.AddDouble("PDTapCompCurrent_A", testParmConfig.GetDoubleParam("IPdTapCompliance_A"));
                }

                module.ConfigData.AddBool("IsPdSourceUseTrigger", instrParamConfig.GetBoolParam("IsPdSourceUseTriggerLink"));
                module.Instrs.Add("Master Source", masterSource);
                module .ConfigData .AddSint32 ("WaveformDataPoints", instrParamConfig.GetIntParam("OscDataPoints"));
                module .ConfigData .AddDouble ("OscXscale_S",instrParamConfig .GetDoubleParam("OscXscale_S"));
                module.ConfigData.AddSint32("WaveformAverageFactor", testParmConfig.GetIntParam("WaveformAverageFactor"));
                
                double[] arrYScale_V = new double[4];
                arrYScale_V[0] = instrParamConfig .GetDoubleParam("OscXiYscale_V");
                arrYScale_V[1] = instrParamConfig .GetDoubleParam("OscXqYscale_V");
                arrYScale_V[2] = instrParamConfig .GetDoubleParam ("OscYiYscale_V");
                arrYScale_V[3] = instrParamConfig .GetDoubleParam ("OscYqYscale_V");

                module .ConfigData .AddDoubleArray ("OscYscaleArray_V",arrYScale_V );

                module.ConfigData.AddString("BlobFileDirectory", testParmConfig.GetStringParam("PlotDataTempDirectory"));
                module.ConfigData.AddString("FilePrefix_WaveformData", dutObject.SerialNumber + "_" +  testParmConfig.GetStringParam("FilePrefix_WaveformData"));
                module.ConfigData.AddString("FilePrefix_PdCurrent", dutObject.SerialNumber + "_" +  testParmConfig.GetStringParam("FilePrefix_PdCurrent"));
                module.ConfigData.AddString("FilePrefix_Coff", dutObject.SerialNumber + "_" +  testParmConfig.GetStringParam("FilePrefix_Coff"));

                if (freqSetMode == EnumFrequencySetMode.ByWavelen)
                {
                    module.ConfigData.AddDoubleArray("WavelenArray", freqArr2TestPhaseAngle);
                }
                else if (freqSetMode == EnumFrequencySetMode.ByChan)
                {
                    module.ConfigData.AddDoubleArray("ChanArray", freqArr2TestPhaseAngle);
                }
                else
                {
                    module.ConfigData.AddDoubleArray("FrequencyArray", freqArr2TestPhaseAngle);
                }
                
                module.ConfigData.AddBool("IsAdjustSigOpcPower", testParmConfig.GetBoolParam("IsAdjustSigPower@Resp"));
                module.ConfigData.AddDouble("RqsSigOpcPower_DBm", testParmConfig.GetDoubleParam("Power_Sig@Resp"));
                module.ConfigData.AddBool("IsAdjustLocOpcPower", testParmConfig.GetBoolParam("IsAdjustLocPower@Resp"));
                module.ConfigData.AddDouble("RqsLocOpcPower_DBm", testParmConfig.GetDoubleParam("Power_Loc@Resp"));
                
                module.ConfigData.AddBool("IsDisablePolarizationAdjust", testParmConfig.GetBoolParam("IsDisablePolarizationAdjust"));                
                string innParamName = "";

                for (int ind_Wl = 0; ind_Wl < freqArr2TestPhaseAngle.Length; ind_Wl++)
                {
                    extParamName = string.Format("PHASE_XIP_WL{0}_TEMP{1}", ind_Wl + 1, idx+1);
                    innParamName = string .Format("Phase_XI_{0}", ind_Wl );
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("PHASE_XQP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("Phase_XQ_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("PHASE_YIP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("Phase_YI_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("PHASE_YQP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("Phase_YQ_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("FIT_CORRE_XIP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("FIT_CORRE_XI_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("FIT_CORRE_XQP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("FIT_CORRE_XQ_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("FIT_CORRE_YIP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("FIT_CORRE_YI_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("FIT_CORRE_YQP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("FIT_CORRE_YQ_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("PHASE_ERR_XIP_XQP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("PhaseError_X_Deg_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("PHASE_ERR_YIP_YQP_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("PhaseError_Y_Deg_{0}", ind_Wl);
                    module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                   
                    extParamName = string.Format("POL_SIG_X_CURRENT_WL{0}_TEMP{1}", ind_Wl+1 , idx + 1);
                    innParamName = string.Format("IauxPD_XL_A_{0}", ind_Wl);
                    if (MainSpec.ParamLimitExists(extParamName))
                        module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                    extParamName = string.Format("POL_SIG_Y_CURRENT_WL{0}_TEMP{1}", ind_Wl + 1, idx + 1);
                    innParamName = string.Format("IauxPD_YL_A_{0}", ind_Wl);
                    if (MainSpec.ParamLimitExists(extParamName))
                        module.Limits.AddParameter(MainSpec, extParamName, innParamName);
                }  // for each wavelen 

                extParamName = string.Format("PHASE_TEMP{0}_FILE",  idx + 1);
                innParamName = "PhaseDataFile";
                module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                extParamName = string.Format("FIT_COEFFICIENT_TEMP{0}_FILE", idx + 1);
                innParamName = "PhaseAnalysisCoeffFile";
                module.Limits.AddParameter(MainSpec, extParamName, innParamName);

                extParamName = string.Format("POL_CURRENT_TEMP{0}_FILE", idx + 1);
                innParamName = "PdDataFile";
                module.Limits.AddParameter(MainSpec, extParamName, innParamName);


                extParamName = string.Format("ICC_TIA_TEMP{0}", idx+1);
                innParamName = "IccTia_X_A_0";
                module.Limits.AddParameter(MainSpec, extParamName, innParamName);

//                extParamName = string.Format("ICC_TIA_Y_TEMP{0}", idx+1);
//                innParamName = "IccTia_Y_A_0";
//                module.Limits.AddParameter(MainSpec, extParamName, innParamName);

            } // End for loop
        }
        #endregion
        #region Helper functions
        /// <summary>
        /// Calculate wavelength
        /// </summary>
        /// <param name="freq_GHz"></param>
        /// <returns></returns>
        private double Freq_GHz_TO_Wave_nm(double freq_GHz)
        {
            // Would need multiply by 1e9 to convert from GHz to Hz.
            // And then to multiply by 1e-9 to convert from nm to m.
            // These cancel out, so do no multiplication.
            const double SpeedLight_m_p_s = 299792458.0;

            double wave_nm = SpeedLight_m_p_s / freq_GHz;
            return wave_nm;
        }



        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void ReadCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null)
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = instrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        /// <summary>
        /// Run at the end of test to archive results for this device
        /// </summary>
        private string ArchiveBlobfiles(DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                testParmConfig.GetStringParam("ResultsArchiveDirectory"),
                "BlobFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(testParmConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(testParmConfig.GetStringParam("PlotDataTempDirectory")))
            {
                string[] csvFilesToAdd = Directory.GetFiles(
                    testParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);
                string[] zipFilesToAdd = Directory.GetFiles(
                    testParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.zip", SearchOption.TopDirectoryOnly);
                string[] txtFilesToAdd = Directory.GetFiles(
                    testParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.txt", SearchOption.TopDirectoryOnly);

                if (csvFilesToAdd.Length > 0 || zipFilesToAdd.Length > 0 || txtFilesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    
                    using (zipFile)
                    {
                        if (csvFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in csvFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (zipFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in zipFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (txtFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in txtFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }
                    }
                    return zipFileName;
                }
            }
            return null;
        }

        /// <summary>
        /// Run at the start of test to delete any old CSV data
        /// </summary>
        protected void CleanUpBlobFiles()
        {
            if (!Directory.Exists(testParmConfig.GetStringParam("PlotDataTempDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    testParmConfig.GetStringParam("PlotDataTempDirectory"), "*.csv");
            string[] zipFilesToRemove = Directory.GetFiles(
                    testParmConfig.GetStringParam("PlotDataTempDirectory"), "*.zip");
            string[] txtFilesToRemove = Directory.GetFiles(
                    testParmConfig.GetStringParam("PlotDataTempDirectory"), "*.txt");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in zipFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in txtFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        /// <summary>
        /// To decide the bias applying to auxilary pd
        ///   the chip wafer no.>=CQRT0072A, then the bias is 14v
        ///   the chip wafer no.less than CQRT0072A, then the bias is 10V
        /// </summary>
        /// <param name="waferID"></param>
        /// <returns>Auxilary pd bias level in volt </returns>
        public double GetAuxPdBias_V(string waferID)
        {
            string[] ArrayWaferID = waferID.Split('.');
            string[] ArrayWaferStander = testParmConfig.GetStringParam("LeastWfId2useHighAuxPdBias").Split('.');

            string TempWaferID = "";
            string WaferStander = "";

            if (ArrayWaferID[0].Length > 8)
            {
                TempWaferID = ArrayWaferID[0].Substring(4, 4);
            }
            else
            {
                TempWaferID = ArrayWaferID[0].Substring(3, 4);
            }

            if (ArrayWaferStander[0].Length > 8)
            {
                WaferStander = ArrayWaferStander[0].Substring(4, 4);
            }
            else
            {
                WaferStander = ArrayWaferStander[0].Substring(3, 4);
            }

            if (string.Compare(TempWaferID, WaferStander, true) >= 0)
            {
                double Vbias = double.Parse(MainSpec.GetParamLimit("TC2_AUX_PD_REVERSE_BIAS").HighLimit.ValueToString()); //TestParmConfig.GetDoubleParam("VauxPD_High_Full_V");
                if (Vbias > 0) Vbias = -Vbias;
                return Vbias;
            }
            else
            {
                double Vbias = double.Parse(MainSpec.GetParamLimit("TC1_AUX_PD_REVERSE_BIAS").HighLimit.ValueToString());//TestParmConfig.GetDoubleParam("VauxPD_Low_Full_V");
                if (Vbias > 0) Vbias = -Vbias;
                return Vbias;
            }
        }

        #endregion
    }
}
