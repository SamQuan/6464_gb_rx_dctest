// [Copyright]
//
// Bookham [Coherence Rx DC Test ]
// Bookham.TestSolution.TestPrograms
//
// TP_CoRxDcTest.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Config;
using System.IO;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// Program for Co Rx dc test
    /// </summary>
    public class TP_CoRxDcTest : ITestProgram
    {
        #region Private data
        /// <summary>
        /// specification name we are using in the program
        /// </summary>
        private string specName;
        Specification MainSpec;

        TestParamConfigAccessor TestParmConfig;
        TestParamConfigAccessor InstrParamConfig;

        TempTableConfigAccessor TempParamConfig;

        ConfigDataAccessor InstrumentsCalData;
        double[] TestTemperatures = null;
        double[] FreqArr2VerifySR = null;
        EnumFrequencySetMode FreqSetMode = EnumFrequencySetMode.ByChan;

        Inst_CoRxITLALaserSource itla_sig = null;
        Inst_CoRxITLALaserSource itla_loc = null;

        DatumList ChipsInfoList;
        double VauxPd_X_V;
        double VauxPd_Y_V;

        DateTime time_Start;
        #endregion

        #region ITestProgram Members
        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return typeof(TP_CoRxDcTestGui); }
        }

        /// <summary>
        /// Initialise phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            engine.SetVersionControl(false, false);
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui("Initialising ...");
            time_Start = DateTime.Now;

            foreach ( Chassis  chassisTemp in chassis.Values )
            {
                chassisTemp.EnableLogging = false;
            }

            foreach (Instrument instrTemp in instrs.Values )
            {
                instrTemp.EnableLogging = false;
            }

            LoadConfigFiles(engine, dutObject);
            // load specification
            SpecList specs = this.LoadSpecs(engine,dutObject);

            // TODO: Get chips information
            if (TestParmConfig.GetBoolParam("IsGetChipInfo"))
            {
                bool isPrevDataOK = ValidatePrevStageData(engine, dutObject);
                if (!isPrevDataOK)
                    engine.ErrorInProgram("the previous test data from PCAS or manual entry is not OK!");
            }
            else
            {
                ChipsInfoList = null;
            }

            #region Get auxilary pd bais according to wafer id
            string wfID = "";
            if (ChipsInfoList.IsPresent("X_WAFER_ID")) wfID = ChipsInfoList.ReadString("X_WAFER_ID");
            
            VauxPd_X_V = GetAuxPdBias_V(wfID);
            
            wfID = "";
            if (ChipsInfoList.IsPresent("Y_WAFER_ID")) wfID = ChipsInfoList.ReadString("Y_WAFER_ID");
                     
            VauxPd_Y_V = GetAuxPdBias_V(wfID);
            
            #endregion

            InitialiseInstrs(engine, instrs);        
            
            //Initialising test modules
            engine.SendStatusMsg("Initialising Test modules ");
            InitCaseTempModule(engine);
            
            InitSRVerifyModules(engine);
            InitDarkCurrentModule(engine);
            InitCouplieEffModules(engine);
            InitResponsivityModules(engine, (Inst_Ke24xx)instrs[InstrParamConfig.GetStringParam("MasterSourceNameInTriggerLink")]);

            // Clean up the blob file folder
            CleanUpBlobFiles();
        }
        /// <summary>
        /// Test run phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void Run(ITestEngineRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;
            bool isModuleRunOk = false;
            ButtonId userOpt= ButtonId .Cancel;
            // Increase Sr Cal counter in config file
            Util_SrCalCounter.UpdateSrCalCounter();

            engine.GuiToFront();
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.ZeroDarkCurrent_End();
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ZeroDarkCurrent_End();
            CoRxDCTestInstruments.SelectItla(itla_loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.SelectItla(itla_sig);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            engine.ShowContinueUserQuery("Please fix the DUT  in the jig then continue until the the cover is close!");
            
            string modname; 
            DatumList tmData ; 
            #region Verifying splitter ratio
            engine.GuiToFront();

            //  Alice: comment the sr verify for debug covenience 
            if (TestParmConfig.GetBoolParam("IsVerifySr"))
            {
                modname = "Verify Signal Path Splitter Ratio";
                engine.SendToGui(modname);
                engine.GuiUserAttention();
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the Signal laser to its monitor power head then continue"));
                GuiMsg.CoRxSwitchConnectorCompleted rspFiberSwitch = null;
                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);

                engine.GuiCancelUserAttention();
                engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
                CoRxDCTestInstruments.SelectItla(itla_sig);
                engine.RunModule(modname);

                CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
                System.Threading.Thread.Sleep(100);
                CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();


                CoRxDCTestInstruments.SelectItla(itla_loc);
                CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
                engine.GuiToFront();
                engine.GuiUserAttention();
                engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the Signal laser output Connector to DUT then continue"));

                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);


                CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
                modname = "Verify Local Osc Path Splitter Ratio";
                engine.SendToGui(modname);
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the local OSC laser to its monitor power head then continue"));

                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);

                engine.GuiCancelUserAttention();
                engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
                engine.RunModule(modname);
                CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
                System.Threading.Thread.Sleep(100);
                CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

                engine.GuiToFront();
                engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the local OSC laser output Connector to DUT then continue"));

                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);
            }

            #endregion

            engine.GuiToFront();
            engine.SendToGui(" DUT Test started, don't Move the DUT or fibre Connector with message promoted");
            
            modname = string.Format("Setting case temp to {0}", TestTemperatures[0]);            
            tmData = engine.RunModule(modname).ModuleRunData.ModuleData;
            try
            {
                isModuleRunOk = tmData.ReadSint32("IsTemperatureSetOK") == 1 ;
            }
            catch //(Exception ex)
            {
                engine.SendToGui("Case temperature can't set to " + TestTemperatures[0].ToString() + "C");
                isModuleRunOk = false;
            }
            if (!isModuleRunOk)
            {
                userOpt = engine.ShowYesNoUserQuery("Case temperature can't set to " + 
                        TestTemperatures[0].ToString() + "C\n Would you like to continue the sequencial temperature test or not?");
                if (userOpt == ButtonId.No)
                    engine.RaiseNonParamFail(0, "Fail to control case temperature to " + TestTemperatures[0].ToString());
            }
            // set ITLA to no any light output status, set gainDac =0 
            //    itla_sig.SetGainDAC(0);
            //    itla_loc.SetGainDAC(0);
            

            bool tempFlagContinue =TestParmConfig.GetBoolParam("IsAbortTest@DarkCurrentFail");               
            engine.RunModule("Dark Current Test",!tempFlagContinue);

            engine .GuiToFront();
           
            engine .SendToGui(new GuiMsg .CautionOfLaserRqst());
            engine.SendToGui( "Test Started , don't move and DUT and  the fibres!");

            int idx_Temp = 0;  // Start from normal temperature 
            double temp;
            double temp_From = 0;
            double temp_To = 0;
            do  // Loop around all temperatures
            {
                engine.GuiShow();
                engine.GuiToFront();
                temp = TestTemperatures[idx_Temp]; 
                modname = string.Format("Coupling Efficiency test @{0}C", temp.ToString().Replace('-','M'));
                engine.RunModule(modname);
                modname = string.Format("Responsivity test @{0}C", temp.ToString().Replace('-', 'M'));
                engine.RunModule(modname);

                // If the last temperature, recover to normal temperature and end  temperature loop 
                if (idx_Temp >= TestTemperatures.Length - 1)  
                {
                    double temp_Recover = TestParmConfig.GetDoubleParam("RecoverTemperature");
                    modname = string.Format("Recover from {0} to {1}", temp, temp_Recover);
                    engine.RunModule(modname);
                    break;                   
                }

                // set case temperature to the next
                do
                {
                    temp_From = TestTemperatures[idx_Temp];
                    temp_To = TestTemperatures[++idx_Temp];
                    modname = string.Format("Setting case temp to {0}", temp_To);
                    
                    try
                    {
                        tmData = engine.RunModule(modname).ModuleRunData.ModuleData;
                        isModuleRunOk = tmData.ReadSint32("IsTemperatureSetOK") == 1;
                    }
                    catch // (Exception ex)
                    {
                        engine.SendToGui("Case temperature can't set to " + temp_To.ToString() + "C");
                        isModuleRunOk = false;
                    }
                    if (!isModuleRunOk)
                    {
                        userOpt = engine.ShowYesNoUserQuery("Case temperature can't set to " +
                            temp_To.ToString() + "C\n Would you like to continue the sequencial temperature test or not?");
                        if (userOpt != ButtonId.Yes)   // recover to normal temperature and abort test
                        {
                            double temp_Recover = TestParmConfig.GetDoubleParam("RecoverTemperature");
                            modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
                            engine.RunModule(modname);
                            break;
                        }
                    }
                } while (!isModuleRunOk && (idx_Temp< TestTemperatures.Length-1) );

                if (!isModuleRunOk)  // if the case temperature can't set, abort the test
                    break;
            }
            while (true);
        }
        /// <summary>
        /// Post run phase, shut down instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;
            // TODO: shutdown actions
            CoRxDCTestInstruments.SelectItla(itla_sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.SelectItla(itla_loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();
            
            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            if (CoRxDCTestInstruments.IsBiasPDTap) CoRxDCTestInstruments.PdTapSource.OutputEnabled = false;
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);

            bool bEnableInterLock = InstrParamConfig.GetBoolParam("IsSourceWithInterLock");
            if (bEnableInterLock) CoRxDCTestInstruments.SetSourceInterLock(false);
        }
        /// <summary>
        /// Data write phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, 
                    DUTOutcome dutOutcome, TestData results, UserList userList)
        {            
            engine.BringTestResultsTabToFront();

            StringDictionary keys = new StringDictionary();
            // TODO: MUST Add real values below!
            keys.Add("SCHEMA", TestParmConfig.GetStringParam("PCASSchema"));
            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", this.specName);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...
            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)
            // !(beware, all these parameters MUST exist in the specification)!...
            DatumList traceData = new DatumList();

            DateTime  testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(time_Start);
            traceData.AddDouble("TEST_TIME", testTime.TotalMinutes);            
            traceData.AddString("TIME_DATE", time_Start.ToString("yyyyMMddHHmmss"));
            traceData.AddString("TIME", time_Start .ToString ("HH:mm:ss"));

                traceData.AddSint32 ("NODE", dutObject.NodeID);
            traceData.AddString("LOT_ID", dutObject.BatchID);
            traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            if (engine.GetProgramRunStatus() == ProgramStatus.Success)
            {
                traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
            }
            else
            {
                string msg = engine.GetProgramRunStatus().ToString();
                string runInfo = msg.Substring(0, (msg.Length > 14 ? 14 : msg.Length));
                traceData.AddString("TEST_STATUS", runInfo);
            }
            
            traceData.AddString("SOFTWARE_ID",
               
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            traceData.AddString("OPERATOR", userList.UserListString);
            traceData.AddString("COMMENTS", engine.GetProgramRunComments());
            
            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") )
                {
                    if (!traceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            traceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            // pick the specification to add this data to...
            engine.SetTraceData(specName, traceData);
        }

        #endregion


        #region Initialise function

        /// <summary>
        /// Load config file for test parameters, temperature setting and instrument calibration
        /// </summary>
        /// <param name="dutObj"></param>
        void LoadConfigFiles(ITestEngineInit engine, DUTObject dutObj)
        {
            engine.SendStatusMsg("Load configuration files");
            
            // temperature setting parameters
            string configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\TempTable.xml", dutObj.NodeID);
            TempParamConfig = new TempTableConfigAccessor(dutObj, 1, configFilePath);

            // test parameters
            TestParmConfig = new TestParamConfigAccessor(dutObj,
               @"Configuration\CoRxDcTest\DCTestParams.xml", "ALL", "CoRxDCTestParams");

            // settings depends on instruments
            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrumentParams.xml", dutObj.NodeID);
            InstrParamConfig = new TestParamConfigAccessor(dutObj, configFilePath,"ALL", "InstrumentParams");

            // instrument calibration data
            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrsCalibration.xml", dutObj.NodeID);
            InstrumentsCalData = new ConfigDataAccessor(configFilePath, "Calibration");

            // Get Test temperatures
            List<double> tempList = new List<double>();
            tempList.Add(TestParmConfig.GetDoubleParam("NormalTemperature"));
            string[] tempStrArr = TestParmConfig.GetStringParam("TemperatureArrayToTest").Split(',');
            if (tempStrArr != null && tempStrArr.Length > 0)
            {
                foreach (string temp in tempStrArr)
                {
                    if (temp .Trim() != "") tempList.Add(double.Parse(temp));
                }
            }
            TestTemperatures = tempList.ToArray();

            #region Get Freq/Wavelen/Chan array to verify SR
            // the frequency information might in frequency, wavelen or channel
            string freqArrStr = "";
            try
            {
                freqArrStr = TestParmConfig.GetStringParam("Freq2VerifySR");
                FreqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            catch 
            {
                try
                {
                    freqArrStr = TestParmConfig.GetStringParam("Chan2VerifySR");
                    FreqSetMode = EnumFrequencySetMode.ByChan;
                }
                catch
                {
                    try
                    {
                        freqArrStr = TestParmConfig.GetStringParam("Wavelen2VerifySR");
                        FreqSetMode = EnumFrequencySetMode.ByWavelen;
                    }
                    catch
                    {
                    }
                }
            }
            if (freqArrStr.Trim() != "")
            {
                string[] freqArrTemp = freqArrStr.Split(',');
                List<double> freqList = new List<double>();
                foreach (string freqTemp in freqArrTemp)
                {
                    freqList.Add(double.Parse(freqTemp));
                }
                FreqArr2VerifySR = freqList.ToArray();
            }
            else
            {
                FreqArr2VerifySR = null;
            }
            #endregion

        }

        /// <summary>
        /// load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        private SpecList LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();

            if (TestParmConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification

                #region Get loccal Limit file path
                // initialise limit file reader
                string limitFileName = "";
                string localLimitFileDir = TestParmConfig.GetStringParam("PcasLimitFileDirectory");

                StringDictionary keys = new StringDictionary();
                keys.Add("TestStage", dutObject.TestStage);
                keys.Add("DeviceType", dutObject.PartCode);
                ConfigDataAccessor localLimitConfig = new ConfigDataAccessor(
                    Path.Combine(localLimitFileDir, TestParmConfig.GetStringParam("LocalLimitStragety")),
                    TestParmConfig.GetStringParam("LocalLimitTableName"));

                // Get file name from data base
                try
                {
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }
                catch
                {
                    limitFileName = "";
                }

                if (limitFileName != "")
                {
                    limitFileName = Path.Combine(localLimitFileDir, limitFileName);
                    if (File.Exists(limitFileName))
                        mainSpecKeys.Add("Filename", limitFileName);
                    else
                        engine.ErrorInProgram("Can't find limit file at " + limitFileName);

                }  //LimitFileName!= ""
                else
                {
                    engine.ShowContinueUserQuery("No local limit file available for device type " + dutObject.PartCode);
                }
                #endregion

            }
            else  // get pcas limit file
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            // Get our specification object (so we can initialise modules with appropriate limits)
            if (tempSpecList != null)
                MainSpec = tempSpecList[0];
            else
                MainSpec = null;

            SpecList specList = new SpecList();
            specName = MainSpec.Name;
            specList.Add(MainSpec);
            engine.SetSpecificationList(specList);
            return specList;
        }

        /// <summary>
        /// Retrieve & verify test parameter from previous stage
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObj"></param>
        /// <returns></returns>
        bool ValidatePrevStageData(ITestEngineInit engine, DUTObject dutObj)
        {
            bool isDataRetrieved = false;

            bool isPcasData = TestParmConfig.GetBoolParam("IsRetrieveChipInfoFromPcas");
            if (isPcasData)
            {
                engine.SendStatusMsg("Retrieving previous data from PCAS...");
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");
                                
                StringDictionary preStageKeys = new StringDictionary();
                preStageKeys.Add("SCHEMA", TestParmConfig.GetStringParam("ChipInfoPcasSchema"));
                preStageKeys.Add("SERIAL_NO", dutObj.SerialNumber);
                preStageKeys.Add("TEST_STAGE", TestParmConfig.GetStringParam("StageName2RetrieveChipInfo"));
                
                DatumList prevData = dataRead.GetLatestResults(preStageKeys, true);

                if (prevData == null)
                {
                    string errorDescription = string.Format("No data be retrieve from {0} stage {1}," +
                        " could you please check the previous data to ensure enough data for this stage?",
                        TestParmConfig.GetStringParam("ChipInfoPcasSchema"),
                        TestParmConfig.GetStringParam("StageName2RetrieveChipInfo"));
                    engine.ErrorInProgram(errorDescription);
                }
                try
                {
                    ChipsInfoList = new DatumList();
                    ChipsInfoList.AddString("X_CHIP_ID", prevData.ReadString("X_CHIP_ID"));
                    ChipsInfoList.AddString("X_CHIP_BIN", prevData.ReadString("X_CHIP_BIN"));
                    ChipsInfoList.AddString("X_COC_SN", prevData.ReadString("X_COC_SN"));
                    ChipsInfoList.AddString("X_WAFER_ID", prevData.ReadString("X_WAFER_ID"));
                    ChipsInfoList.AddString("Y_CHIP_ID", prevData.ReadString("Y_CHIP_ID"));
                    ChipsInfoList.AddString("Y_CHIP_BIN", prevData.ReadString("Y_CHIP_BIN"));
                    ChipsInfoList.AddString("Y_COC_SN", prevData.ReadString("Y_COC_SN"));
                    ChipsInfoList.AddString("Y_WAFER_ID", prevData.ReadString("Y_WAFER_ID"));
                    isDataRetrieved = true;
                }
                catch
                {
                    isDataRetrieved = false;
                    engine.ErrorInProgram("Not all neccesary data required by this stage is retrieve from previous stage," +
                        "\nTest on this stage can't go on, please check the previous data");
                }
            }
            else
            {
                engine.GuiToFront();
                engine.SendToGui(new GuiMsg.GetChipInfoRqst());
                engine.GuiUserAttention();
                engine.SendStatusMsg("Constructing engineering previous test data");

                DatumList previousData = (DatumList)engine.ReceiveFromGui().Payload;
                engine.GuiCancelUserAttention();
                if (previousData == null || previousData.Count < 3)
                    engine.ErrorInProgram("Not enough previous data entry to carry test on this stage");
                ChipsInfoList = new DatumList();
                ChipsInfoList.AddListItems(previousData);
                isDataRetrieved = true;
            }
            return isDataRetrieved;
        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        private void InitialiseInstrs(ITestEngineInit engine, InstrumentCollection instrs)
        {
            engine.SendToGui("Initialising instruments ...");
            engine.SendStatusMsg("Initialising instruments ...");
            #region Assign instruments
             
            CoRxDCTestInstruments.SignalInputInstrs = new CoRxSourceInstrsChain();

            itla_sig = new Inst_CoRxITLALaserSource("iTLA_Sig",(Instr_ITLA)instrs["iTLA_Sig"]);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource =  itla_sig ;
            CoRxDCTestInstruments.SignalInputInstrs.Voa = null; // (InstType_OpticalAttenuator)instrs["VOA_Sig"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_SigMon"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_SigRef"];

            CoRxDCTestInstruments.LocalOscInputInstrs = new CoRxSourceInstrsChain();
            itla_loc = new Inst_CoRxITLALaserSource("iTLA_Loc",(Instr_ITLA)instrs["iTLA_Loc"]);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource =   itla_loc;
            CoRxDCTestInstruments.LocalOscInputInstrs.Voa = null; // (InstType_OpticalAttenuator)instrs["VOA_Loc"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_LocMon"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_LocRef"];

            CoRxDCTestInstruments.TopCaseTec = (Inst_Nt10a)instrs["CaseTec"];
            CoRxDCTestInstruments.IsDualCaseTecControl = InstrParamConfig.GetBoolParam("IsUseBottomTEC");
            if (CoRxDCTestInstruments.IsDualCaseTecControl)
            {
                CoRxDCTestInstruments.BottomCaseTec = (Inst_Nt10a)instrs["BottomCaseTec"];
            }
            else
                CoRxDCTestInstruments.BottomCaseTec = null;

            
            CoRxDCTestInstruments.PdSource_AUX_XR = (Inst_Ke24xx)instrs["AuxPdSource_XR"];
            CoRxDCTestInstruments.PdSource_AUX_XL = (Inst_Ke24xx)instrs["AuxPdSource_XL"];
            CoRxDCTestInstruments.PdSource_AUX_YL = (Inst_Ke24xx)instrs["AuxPdSource_YL"];
            CoRxDCTestInstruments.PdSource_AUX_YR = (Inst_Ke24xx)instrs["AuxPdSource_YP"];
            CoRxDCTestInstruments.IsAuxPDOnFront = InstrParamConfig.GetBoolParam("IsAuxPdOnFrontTernimal");

            CoRxDCTestInstruments.PdSource_XI1 = (Inst_Ke24xx)instrs["PdSource_XI1"];
            CoRxDCTestInstruments.PdSource_XI2 = (Inst_Ke24xx)instrs["PdSource_XI2"];
            CoRxDCTestInstruments.PdSource_XQ1 = (Inst_Ke24xx)instrs["PdSource_XQ1"];
            CoRxDCTestInstruments.PdSource_XQ2 = (Inst_Ke24xx)instrs["PdSource_XQ2"];

            CoRxDCTestInstruments.PdSource_YI1 = (Inst_Ke24xx)instrs["PdSource_YI1"];
            CoRxDCTestInstruments.PdSource_YI2 = (Inst_Ke24xx)instrs["PdSource_YI2"];
            CoRxDCTestInstruments.PdSource_YQ1 = (Inst_Ke24xx)instrs["PdSource_YQ1"];
            CoRxDCTestInstruments.PdSource_YQ2 = (Inst_Ke24xx)instrs["PdSource_YQ2"];
            CoRxDCTestInstruments.TriggerInLine = InstrParamConfig.GetIntParam("SlaveSourceTriggerInLine");
            CoRxDCTestInstruments.TriggerOutLine = InstrParamConfig.GetIntParam("SlaveSourceTriggerOutputLine");
            CoRxDCTestInstruments.TriggerNoUsedLine = InstrParamConfig.GetIntParam("SlaveSourceTriggerNoUseLine");

            CoRxDCTestInstruments.TiaSource_X = (Inst_Ke24xx)instrs["TIASource_X"];
            CoRxDCTestInstruments.TiaSource_Y = (Inst_Ke24xx)instrs["TIASource_Y"];
 

            CoRxDCTestInstruments.IsBiasPDTap = TestParmConfig.GetBoolParam("IsPdTapBias");
            CoRxDCTestInstruments.PdTapSource = null;

            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                try
                {
                    CoRxDCTestInstruments.PdTapSource = (Inst_Ke24xx)instrs["TapSource"];
                }
                catch
                {
                    engine.ErrorInProgram("Can't get source available to bais Tap PD");
                }
            }

            try
            {
                CoRxDCTestInstruments.GCSource_X = (InstType_ElectricalSource)instrs["GCSource_X"];
                CoRxDCTestInstruments.GCSource_Y = (InstType_ElectricalSource)instrs["GCSource_Y"];
                CoRxDCTestInstruments.MCSource_X = (InstType_ElectricalSource)instrs["MCSource_X"];
                CoRxDCTestInstruments.MCSource_Y = (InstType_ElectricalSource)instrs["MCSource_Y"];
                CoRxDCTestInstruments.OaSource_X = (InstType_ElectricalSource)instrs["OaSource_X"];
                CoRxDCTestInstruments.OaSource_Y = (InstType_ElectricalSource)instrs["OaSource_Y"];
                CoRxDCTestInstruments.OCSource_X = (InstType_ElectricalSource)instrs["OcSource_X"];
                CoRxDCTestInstruments.OCSource_Y = (InstType_ElectricalSource)instrs["OcSource_Y"];
                CoRxDCTestInstruments.ThermPhaseSource_L = (InstType_ElectricalSource)instrs["ThermPhaseSource_L"];
                CoRxDCTestInstruments.ThermPhaseSource_R = (InstType_ElectricalSource)instrs["ThermPhaseSource_R"];
            }
            catch
            { }
            #endregion

            #region setting measurement  parameters            

            CoRxDCTestInstruments.LocalOscInputInstrs.sngVoaDefAtt_dB = TestParmConfig.GetDoubleParam("InitAtt_LocVoa_dB");
            CoRxDCTestInstruments.SignalInputInstrs.sngVoaDefAtt_dB = TestParmConfig.GetDoubleParam("InitAtt_SigVoa_dB");

            CoRxDCTestInstruments.SignalInputInstrs.PowerSetTolerance_dB = TestParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.PowerSetTolerance_dB = TestParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.MaxPowerTuningCount = TestParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");
            CoRxDCTestInstruments.SignalInputInstrs.MaxPowerTuningCount = TestParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");

            string configFilePath = InstrParamConfig.GetStringParam("SplitterCalDataBaseConfigPath");
            string configTableName = InstrParamConfig.GetStringParam("SplitterCalDataBaseConfigTableName");
            string sigSplitterName = InstrParamConfig.GetStringParam("SignalPathSplitterName");
            string locSplitterName = InstrParamConfig.GetStringParam("LocPathSplitterName");

            try
            {
                CoRxDCTestInstruments.SignalInputInstrs.SetupSplitterRatioData(
                    sigSplitterName, configFilePath, configTableName);
                CoRxDCTestInstruments.SignalInputInstrs.SplitterRatiosCalData.FrequencyTolerance_Ghz =
                    InstrParamConfig.GetDoubleParam("FreqTolerance_Ghz");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.SignalInputInstrs.IsSplitterRatioCalOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Signal path, Test have to Abort!");

            try
            {
                CoRxDCTestInstruments.LocalOscInputInstrs.SetupSplitterRatioData(
                    locSplitterName, configFilePath, configTableName);
                CoRxDCTestInstruments .LocalOscInputInstrs .SplitterRatiosCalData.FrequencyTolerance_Ghz =
                    InstrParamConfig.GetDoubleParam("FreqTolerance_Ghz");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.LocalOscInputInstrs.IsSplitterRatioCalOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Loc oscilatior path, Test have to Abort!");
            
            // Check if the splitter ratio cal data is outdate or exceed maximun allowed count

            #endregion

            #region Initialise instruments

            if (!engine.IsSimulation)  // if testset on line, initialise instruments
            {
                // Seting powermeters calibration value
                double calFactor, calOffset;
                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.ReferencePower = calOffset;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.ReferencePower = calOffset;

                CoRxDCTestInstruments.SignalInputInstrs.MaxShutDownPower_dBm = InstrParamConfig.GetDoubleParam("LocPathMaxShutDownPower_dBm");
                CoRxDCTestInstruments.LocalOscInputInstrs.MaxShutDownPower_dBm = InstrParamConfig.GetDoubleParam("SigPathMaxShutDownPower_dBm");

                // Shut down laser source for safe  
                if (itla_sig != null)
                {
                    CoRxDCTestInstruments.SelectItla(itla_sig);
                    itla_sig.CurrentChan = 1;
                }
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.OutputEnable = false;
                itla_sig.Power_dBm = InstrParamConfig .GetDoubleParam("SignalSourcePower@Test_dBm");
                
                if (itla_loc != null)
                {
                    System.Threading.Thread.Sleep(100);
                    CoRxDCTestInstruments.SelectItla(itla_loc);
                    itla_loc.CurrentChan = 1;
                }
                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.OutputEnable = false;
                itla_loc.Power_dBm = InstrParamConfig.GetDoubleParam("LocSourcePower@Test_dBm");
                // Zero all power meter modules' dark current
                try
                {
                    Inst_Ag816x_OpticalPowerMeter inst_8163 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon as Inst_Ag816x_OpticalPowerMeter;
                    inst_8163.SafeMode = false;
                    inst_8163.Range = InstrParamConfig.GetDoubleParam("OpmRange_LocPath Mon_dBM");
                    inst_8163.ZeroAllModuleDarkCurrent();

                    Inst_Ag816x_OpticalPowerMeter inst_8163_3 = CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                    inst_8163_3.Range = InstrParamConfig.GetDoubleParam("OpmRange_SigPathRef_dBM");
                    inst_8163_3.SafeMode = false;

                    Inst_Ag816x_OpticalPowerMeter inst_8163_2 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                    inst_8163_2.Range = InstrParamConfig.GetDoubleParam("OpmRange_LocPath Ref_dBM");
                    inst_8163_2.SafeMode = false;
                    if (!object.ReferenceEquals(inst_8163.InstrumentChassis, inst_8163_2.InstrumentChassis))
                        inst_8163_2.ZeroAllModuleDarkCurrent();

                }
                catch
                { }

                // Initialise case tec controler
                CoRxDCTestInstruments.TopCaseTec.OutputEnabled = false;
                CoRxDCTestInstruments.TopCaseTec.OperatingMode = InstType_TecController.ControlMode.Temperature;
                CoRxDCTestInstruments.TopCaseTec.ProportionalGain = InstrParamConfig.GetDoubleParam("TecCase_PropotionGain");
                CoRxDCTestInstruments.TopCaseTec.IntegralGain = InstrParamConfig.GetDoubleParam("TecCase_IntegrationGain");
                CoRxDCTestInstruments.TopCaseTec.TecCurrentCompliance_amp = InstrParamConfig.GetDoubleParam("TecCase_CurrentCompliance_amp");
                CoRxDCTestInstruments.TopCaseTec.SensorTemperatureSetPoint_C = TestTemperatures[0];
                CoRxDCTestInstruments.TopCaseTec.OutputEnabled = true;

                // initialise bias sources
                CoRxDCTestInstruments.InitialiseAllSource();

                bool bEnableInterLock = InstrParamConfig.GetBoolParam("IsSourceWithInterLock");
                CoRxDCTestInstruments.SetSourceInterLock(bEnableInterLock);
            }
            #endregion
        }

        /// <summary>
        /// Initialise modules for all case temperatue ivolves in the whole test 
        /// </summary>
        /// <param name="engine"></param>
        void InitCaseTempModule(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising case temperature controlling modules");
            
            // Setup 25 ~ 25c module
            double temp_From = TestParmConfig.GetDoubleParam("RoomTemperature");
            double temp_To = TestTemperatures[0];
            string modname = string.Format("Setting case temp to {0}", temp_To);
            SetCaseTempControl(engine, modname, temp_From, temp_To,true );

            double temp_Recover = TestParmConfig.GetDoubleParam("RecoverTemperature");
            modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
            SetCaseTempControl(engine, modname, temp_To, temp_Recover, false);

            // setup 25 ~ -5c module
           
            if (TestTemperatures.Length > 1)
            {
                
                for (int indx = 1; indx < TestTemperatures.Length; indx++)
                {
                    temp_From = TestTemperatures[indx-1];
                    temp_To = TestTemperatures[indx];
                    modname = string.Format("Setting case temp to {0}", temp_To);
                    SetCaseTempControl(engine, modname, temp_From, temp_To,true);

                    // add a module to recover from this temperature to the recovered temperature
                    modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
                    SetCaseTempControl(engine, modname, temp_To, temp_Recover,false);
                }
            }
            
        }

        /// <summary>
        /// Initialise case temperature control module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="modName"> module name </param>
        /// <param name="temp_From"> case temperature to start runing this module </param>
        /// <param name="temp_To">  case temperature to be achieved </param>
        /// <param name="isSaveReslt"> If link temperature setting result to limit file </param>
        void SetCaseTempControl(ITestEngineInit engine, string modName,
                            double temp_From, double temp_To, bool isSaveReslt)
        {
            // Get TEC Controller parameter from config file and set to it
            DatumDouble dt_timeout_s = new DatumDouble("MaxExpectedTimeForOperation_s",
                InstrParamConfig.GetDoubleParam("TecCase_Timeout_S"));
            DatumSint32 dt_intval; 
            double temp_setting = 0;
            TempTablePoint[] t_points;
            t_points = TempParamConfig.GetTemperatureCal(temp_From, temp_To);
            if (t_points.Length < 1)
                engine.ErrorInProgram(string.Format("Can't find temperature setting in temperature configure file" +
                    " with temperature from {0} to {1}", temp_From, temp_To));
            temp_setting = temp_To + t_points[0].OffsetC;

            ModuleRun module;
            if (CoRxDCTestInstruments .IsDualCaseTecControl)  // Stack tec control
            {
                // the following param varies with setting temp
                dt_intval = new DatumSint32("TimeBetweenReadings_ms",
                            InstrParamConfig.GetIntParam("TecCase_UpdateTime_mS"));
                DatumDouble TopTec_RqdTimeInToleranceWindow_s = new DatumDouble("TopTec_RqdTimeInToleranceWindow_s",
                    InstrParamConfig.GetDoubleParam("TecCase_InTolerance_S"));
                DatumDouble dt_TopTecStable_s = new DatumDouble("TopTec_RqdStabilisationTime_s", t_points[0].DelayTime_s);
                DatumDouble dt_TopTecSet = new DatumDouble("TopTec_SetPointTemperature_C", temp_setting);
                DatumDouble dt_TopTecTol = new DatumDouble("TopTec_TemperatureTolerance_C", t_points[0].Tolerance_degC);

                DatumDouble dt_BottomTecTimeout_s = new DatumDouble("BtmTec_RqdTimeInToleranceWindow_s",
                    InstrParamConfig.GetDoubleParam("BottomTecCase_InTolerance_S"));
                DatumDouble dt_BottomTecTstable_s = new DatumDouble("BtmTec_RqdStabilisationTime_s",
                    InstrParamConfig.GetDoubleParam("BottomTecCase_StabiliseTime_S"));
                DatumDouble dt_BottomTecDegSet = new DatumDouble("BtmTec_SetPointTemperature_C",
                    InstrParamConfig.GetDoubleParam(
                    "BottomTecCaseDegSet_" + temp_To.ToString("###").Replace('-', 'M') + "C"));
                DatumDouble BtmTec_TemperatureTolerance_C = new DatumDouble("BtmTec_TemperatureTolerance_C",
                    InstrParamConfig.GetDoubleParam("BottomTecCase_Tolerance_C"));

                module = engine.AddModuleRun(modName, "StackedTecControl", "");
                module.Instrs.Add("BtmTec_Controller", CoRxDCTestInstruments.BottomCaseTec);
                module.Instrs.Add("TopTec_Controller", CoRxDCTestInstruments.TopCaseTec);

                module.ConfigData.Add(dt_TopTecSet);
                module.ConfigData.Add(dt_TopTecStable_s);
                module.ConfigData.Add(dt_TopTecTol);
                module.ConfigData.Add(TopTec_RqdTimeInToleranceWindow_s);

                module.ConfigData.Add(dt_BottomTecDegSet);
                module.ConfigData.Add(dt_BottomTecTimeout_s);
                module.ConfigData.Add(dt_BottomTecTstable_s);
                module.ConfigData.Add(BtmTec_TemperatureTolerance_C);

            }
            else  // Single Tec Control
            {
                DatumDouble dt_Tstable_s = new DatumDouble("RqdStabilisationTime_s", t_points[0].DelayTime_s);
                DatumDouble dt_DegSet_C = new DatumDouble("SetPointTemperature_C", temp_setting);
                DatumDouble dt_DegTol = new DatumDouble("TemperatureTolerance_C", t_points[0].Tolerance_degC);
                dt_intval = new DatumSint32("TempTimeBtwReadings_ms",
                    InstrParamConfig.GetIntParam("TecCase_UpdateTime_mS"));

                module = engine.AddModuleRun(modName, "SimpleTempControl", "");
                module.Instrs.Add("Controller", CoRxDCTestInstruments.TopCaseTec );

                module.ConfigData.Add(dt_Tstable_s);
                module.ConfigData.Add(dt_DegSet_C);
                module.ConfigData.Add(dt_DegTol);
            }
            module.ConfigData.Add(dt_timeout_s);
            module.ConfigData.Add(dt_intval);

            if (isSaveReslt)
            {
                string tempStr = temp_To.ToString("#").Replace('-', 'M');
                string paramName = string.Format("TEMPERATURE_ROOM_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "ActualTempReading");
                tempStr = temp_To.ToString("#").Replace('-', 'M');
                paramName = string.Format("TEMPERATURE_DUT_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "TargetTemp");
                paramName = string.Format("TEMPERATURE_SET_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "IsTemperatureSetOK");
            }
        }
    
        /// <summary>
        /// Initialise modules to verify Splitters' ratio
        /// </summary>
        /// <param name="engine"></param>
        void InitSRVerifyModules(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising splitter ratio verify module ...");
            if (FreqArr2VerifySR== null  || FreqArr2VerifySR .Length <1) return;

            #region Sig path verify module
            ModuleRun module = engine.AddModuleRun("Verify Signal Path Splitter Ratio", "TM_CoRxSRVerify", "");
            module.ConfigData.AddString("GuiTitle", "Verify Signal Path Splitter Ratio");
            module.ConfigData.AddDouble("LaserSourcePower_dBm", InstrParamConfig.GetDoubleParam("SignalSourcePower@SRVerify_dBm"));
            module.ConfigData.AddDouble("VoaAtten_dB", InstrParamConfig.GetDoubleParam("Att_Sig@SRVerify_dB"));
            module.ConfigData.AddDouble("MaxDeltaSR_dB", InstrParamConfig.GetDoubleParam("MaxDeltaSR_Sig@SRVerify_dB"));
            module.ConfigData.AddSint32("MaxRetryTimes", TestParmConfig.GetIntParam("MaxRetryTimes@SRVerify"));
            
            // Add this param to config data will save data to plot file
            module.ConfigData.AddString("FileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));

            module.ConfigData.AddReference("OpcChainInstrs", CoRxDCTestInstruments.SignalInputInstrs);

            // Add frequency message  to verify according to its setting mode
            string freqParamName;
            if (FreqSetMode == EnumFrequencySetMode.ByChan)
                freqParamName = "ChanArr2Verify";
            else if (FreqSetMode == EnumFrequencySetMode .ByWavelen )
                freqParamName = "WavelenArr2Verify" ;
            else 
                freqParamName = "FreqArr2Verify" ;

            module.ConfigData.AddDoubleArray(freqParamName, FreqArr2VerifySR);
            #endregion

            #region Loc path verify module
            module = engine.AddModuleRun("Verify Local Osc Path Splitter Ratio", "TM_CoRxSRVerify", "");
            module.ConfigData.AddString("GuiTitle", "Verify Local Osc Path Splitter Ratio");
            module.ConfigData.AddDouble("LaserSourcePower_dBm", InstrParamConfig.GetDoubleParam("LocSourcePower@SRVerify_dBm"));
            module.ConfigData.AddDouble("VoaAtten_dB", InstrParamConfig.GetDoubleParam("Att_Loc@SRVerify_dB"));
            module.ConfigData.AddDouble("MaxDeltaSR_dB", InstrParamConfig.GetDoubleParam("MaxDeltaSR_Loc@SRVerify_dB"));
            module.ConfigData.AddSint32("MaxRetryTimes", TestParmConfig.GetIntParam("MaxRetryTimes@SRVerify"));
            module.ConfigData.AddReference("OpcChainInstrs", CoRxDCTestInstruments.LocalOscInputInstrs);

            module.ConfigData.AddString("FileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));
            module.ConfigData.AddDoubleArray(freqParamName, FreqArr2VerifySR);
            #endregion
        }

        /// <summary>
        /// Initialise module for darkcurrent test
        /// </summary>
        /// <param name="engine"></param>
        void InitDarkCurrentModule(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising Dark current test modules ...");
            ModuleRun module = engine.AddModuleRun("Dark Current Test", "TM_InitialiseTest", "");
            module .ConfigData .AddDouble("PDBias_V",TestParmConfig.GetDoubleParam("Vpd@DarkCurrent_V"));
            module .ConfigData .AddDouble("PDCompCurrent_A",TestParmConfig .GetDoubleParam("IpdCompliant@DarkCurrent_A"));
            module .ConfigData .AddDouble ("TiaBias_V", TestParmConfig .GetDoubleParam("Vcc_V"));
            module .ConfigData .AddDouble ("TIACompCurrent_A",TestParmConfig .GetDoubleParam("IccComliant@DarkCurrent_A"));

            module .ConfigData .AddDouble ("AuxPDBias_X_V", VauxPd_X_V);
            module .ConfigData .AddDouble ("AuxPDBias_Y_V", VauxPd_Y_V);

            module.ConfigData.AddDouble("AuxPDCompCurrent_X_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_X@DarkCurrent_A"));
            module.ConfigData.AddDouble("AuxPDCompCurrent_Y_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_Y@DarkCurrent_A"));
            module.ConfigData.AddBool("TC_TestAuxPD", Convert.ToBoolean(MainSpec.GetParamLimit("TC_INCLUDE_AUX_TEST").MeasuredData.ValueToString()));

            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                module.ConfigData.AddDouble("PDTapBias_V", TestParmConfig.GetDoubleParam("VPdTap_V"));
                module.ConfigData.AddDouble("PDTapCompCurrent_A", TestParmConfig.GetDoubleParam("IPdTapCompliance_A"));

                module.Limits.AddParameter(MainSpec, "DARKCURRENT_TAP1_35C", "IPdTap_nA");
            }
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_XL_35C", "IauxPD_XL_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_XR_35C", "IauxPD_XR_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_YL_35C", "IauxPD_YL_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_YR_35C", "IauxPD_YR_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_XI1_35C", "Ipd_XI1_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_XI2_35C", "Ipd_XI2_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_XQ1_35C", "Ipd_XQ1_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_XQ2_35C", "Ipd_XQ2_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_YI1_35C", "Ipd_YI1_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_YI2_35C", "Ipd_YI2_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_YQ1_35C", "Ipd_YQ1_nA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_YQ2_35C", "Ipd_YQ2_nA");

            module.Limits.AddParameter(MainSpec, "DARKCURRENT_TIA1_35C", "Icc_X_mA");
            module.Limits.AddParameter(MainSpec, "DARKCURRENT_TIA2_35C", "Icc_Y_mA");
        }

        /// <summary>
        ///  Initialise Coupling eff modules test under all temperatures
        /// </summary>
        /// <param name="engine"></param>
        void InitCouplieEffModules(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising Coupling Eff test modules ...");
            double wavelen_Ref = TestParmConfig.GetDoubleParam("Wavelen4TempTest_nm");
            double freq_Ref = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelen_Ref);
            for (int idx=0; idx< TestTemperatures.Length ;idx++)                
            {
                string temp = TestTemperatures[idx].ToString().Replace('-', 'M');
                string moduleName = string.Format("Coupling Efficiency test @{0:##}C", temp);
                ModuleRun module = engine.AddModuleRun(moduleName, "TM_CoRxMultiCoupleEff", "");

                if (MainSpec.ParamLimitExists("AUX_BIAS_LOC_X"))
                    module.Limits.AddParameter(MainSpec, "AUX_BIAS_LOC_X", "VauxPD_XR_V");
                if (MainSpec.ParamLimitExists("AUX_BIAS_LOC_Y"))
                    module.Limits.AddParameter(MainSpec , "AUX_BIAS_LOC_X", "VauxPD_YR_V");

                if (MainSpec.ParamLimitExists("AUX_BIAS_SIG_X"))
                    module.Limits.AddParameter(MainSpec , "AUX_BIAS_SIG_X", "VauxPD_XL_V");
                if (MainSpec.ParamLimitExists("AUX_BIAS_SIG_Y"))
                    module.Limits.AddParameter(MainSpec , "AUX_BIAS_SIG_X", "VauxPD_YL_V");
                                
                
                module.ConfigData.AddDouble("TiaBias_V", TestParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", TestParmConfig.GetDoubleParam("IccComliant_A"));

                module.ConfigData.AddDouble("AuxPDBias_X_V", VauxPd_X_V);
                module.ConfigData.AddDouble("AuxPDBias_Y_V", VauxPd_Y_V);

                module.ConfigData.AddDouble("AuxPDCompCurrent_X_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_X@CoupleEff_A"));
                module.ConfigData.AddDouble("AuxPDCompCurrent_Y_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_Y@CoupleEff_A"));

                module.ConfigData.AddDouble("ReferenceWavelen_nM", wavelen_Ref);
                
                string paramName="";
                if (idx == 0) // If  normal temp test, test series frequencies and statistical
                {
                    module.ConfigData.AddBool("SaveDataToFile", true);
                    module .ConfigData .AddString("BlobFileDirectory", TestParmConfig .GetStringParam("PlotDataTempDirectory"));
                    module .ConfigData .AddString("FileSufix4SigPath", TestParmConfig .GetStringParam ("FilePrefix_CoupleEff4SigPath"));
                    module .ConfigData.AddString("FileSufix4LocPath", TestParmConfig .GetStringParam("FilePrefix_CoupleEff4LocPath"));
                    module.ConfigData.AddDouble("Freq_Start_GHz", TestParmConfig.GetDoubleParam("StartFreq@CoupleEff_GHz"));
                    module.ConfigData.AddDouble("Freq_Stop_GHz", TestParmConfig.GetDoubleParam("StopFreq@CoupleEff_GHz"));

                    module.Limits.AddParameter(MainSpec, "PLOT_AUX_SIG_35C", "SigPathDataFile");
                    module.Limits.AddParameter(MainSpec, "PLOT_AUX_LO_35C", "LocPathDataFile");
                    
                    paramName = string .Format ("RESP_XL_LO_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL_Avg");
                    paramName = string .Format ("RESP_XL_LO_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL_Max");
                    paramName = string .Format ("RESP_XL_LO_{0}C_MIN", temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL_Min");

                    paramName = string .Format ("RESP_XL_SIG_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL_Avg");
                    paramName = string .Format ("RESP_XL_SIG_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL_Max");
                    paramName = string .Format ("RESP_XL_SIG_{0}C_MIN",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL_Min");

                    paramName = string .Format ("RESP_XR_LO_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR_Avg");
                    paramName =string .Format ("RESP_XR_LO_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR_Max");
                    paramName = string .Format ("RESP_XR_LO_{0}C_MIN",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR_Min");

                    paramName = string .Format ("RESP_XR_SIG_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR_Avg");
                    paramName = string .Format ("RESP_XR_SIG_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR_Max");
                    paramName = string.Format("RESP_XR_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR_Min");
                    
                    paramName = string .Format ("RESP_YL_LO_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL_Avg");
                    paramName = string .Format ("RESP_YL_LO_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL_Max");
                    paramName = string .Format ("RESP_YL_LO_{0}C_MIN",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL_Min");

                    paramName = string .Format ("RESP_YL_SIG_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL_Avg");
                    paramName = string.Format("RESP_YL_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL_Max");
                    paramName = string .Format ("RESP_YL_SIG_{0}C_MIN",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL_Min");

                    paramName = string .Format ("RESP_YR_LO_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR_Avg");
                    paramName = string .Format ("RESP_YR_LO_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR_Max");
                    paramName = string .Format ("RESP_YR_LO_{0}C_MIN",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR_Min");

                    paramName = string .Format ("RESP_YR_SIG_{0}C_AVG",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR_Avg");
                    paramName = string .Format ("RESP_YR_SIG_{0}C_MAX",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR_Max");
                    paramName = string .Format ("RESP_YR_SIG_{0}C_MIN",temp );
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR_Min");

                    
                }
                else // If not normal temp test, only the ref wavelen is tested
                {
                    module.ConfigData.AddBool("SaveDataToFile", false);
                    module.ConfigData.AddDouble("Freq_Start_GHz", freq_Ref);
                    module.ConfigData.AddDouble("Freq_Stop_GHz", freq_Ref);
                }
                module.ConfigData.AddDouble("Freq_Step_GHz", TestParmConfig.GetDoubleParam("StepFreq@CoupleEff_GHz"));
                module.ConfigData.AddBool("IsAdjustSigOpcPower", TestParmConfig.GetBoolParam("IsAdjustSigPower@CoupleEff"));
                module.ConfigData.AddDouble("RqsSigOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Sig@CoupleEff"));
                module.ConfigData.AddBool("IsAdjustLocOpcPower", TestParmConfig.GetBoolParam("IsAdjustLocPower@CoupleEff"));
                module.ConfigData.AddDouble("RqsLocOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Loc@CoupleEff"));

                paramName = string.Format("RESP_XL_LO_{0}C_{1:####}NM", temp, wavelen_Ref);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL");
                
                paramName = string.Format("RESP_XL_SIG_{0}C_{1:####}NM", temp, wavelen_Ref);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL");

                paramName = string.Format("RESP_XR_LO_{0}C_{1:####}NM", temp, wavelen_Ref);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR");

                paramName = string .Format ("RESP_XR_SIG_{0}C_{1:####}NM",temp,wavelen_Ref );
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR");
                
                paramName = string .Format ("RESP_YL_LO_{0}C_{1:####}NM",temp,wavelen_Ref );
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL");
                
                paramName = string .Format ("RESP_YL_SIG_{0}C_{1:####}NM",temp,wavelen_Ref );
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL");
 
                paramName = string .Format("RESP_YR_LO_{0}C_{1:####}NM",temp,wavelen_Ref );
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR");

                paramName = string .Format ("RESP_YR_SIG_{0}C_{1:####}NM",temp ,wavelen_Ref );
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR");

            }
        }

        /// <summary>
        /// Initialise responsivity modules under all temperature
        /// </summary>
        /// <param name="engine"></param>
        ///<param name="masterSource"> Source to use as master trigger source </param>
        void InitResponsivityModules(ITestEngineInit engine, Inst_Ke24xx masterSource)
        {
            string paramName;

            engine.SendToGui("Initialising Responsivity test modules ...");
            double wavelen_Ref = TestParmConfig.GetDoubleParam("Wavelen4TempTest_nm");
            double freq_Ref = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelen_Ref);

            for (int idx = 0; idx < TestTemperatures.Length; idx++)
            {

                string temp = TestTemperatures[idx].ToString().Replace('-','M');
                string moduleName = string.Format("Responsivity test @{0}C", temp);

                ModuleRun module = engine.AddModuleRun(moduleName, "TM_CoRxResponsivity", "");
                module.ConfigData.AddDouble("TiaBias_V", TestParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", TestParmConfig.GetDoubleParam("IccComliant@Resp_A"));
                module.ConfigData.AddDouble("PDBias_V", TestParmConfig.GetDoubleParam("Vpd_V"));
                module.ConfigData.AddDouble("PDCompCurrent_A", TestParmConfig.GetDoubleParam("IpdCompliant@Resp_A"));
                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    module.ConfigData.AddDouble("PDTapBias_V", TestParmConfig.GetDoubleParam("VPdTap_V"));
                    module.ConfigData.AddDouble("PDTapCompCurrent_A", TestParmConfig.GetDoubleParam("IPdTapCompliance_A"));
                }

                module.ConfigData.AddDouble("ReferenceWavelen_nM", wavelen_Ref);

                module.ConfigData.AddBool("IsPdSourceUseTrigger", InstrParamConfig.GetBoolParam("IsPdSourceUseTriggerLink"));
                module.Instrs.Add("Master Source", masterSource);

                module.ConfigData.AddString("BlobFileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));
                module.ConfigData.AddString("FileSufix4SigPath", TestParmConfig.GetStringParam("FilePrefix_SigPathData"));
                module.ConfigData.AddString("FileSufix4LocPath", TestParmConfig.GetStringParam("FilePrefix_LocPathData"));
                
                module.ConfigData.AddString("FileSufix4SigPathResp", TestParmConfig.GetStringParam("FilePrefix_SigPathResp"));
                module.ConfigData.AddString("FileSufix4LocPathResp", TestParmConfig.GetStringParam("FilePrefix_LocPathResp"));
                module.ConfigData.AddString("FileSufix4RespStat", TestParmConfig.GetStringParam("FilePrefix_RespStat"));

                module.ConfigData.AddString("FileSufix4SigPathDevResp", TestParmConfig.GetStringParam("FilePrefix_SigPathDevResp"));
                module.ConfigData.AddString("FileSufix4LocPathDevResp", TestParmConfig.GetStringParam("FilePrefix_LocPathDevResp"));
                module.ConfigData.AddString("FileSufix4DevRespStat", TestParmConfig.GetStringParam("FilePrefix_DevRespStat"));

                module.ConfigData.AddString("FileSufix4SigPathCMRR", TestParmConfig.GetStringParam("FilePrefix_SigPathCMRR"));
                module.ConfigData.AddString("FileSufix4LocPathCMRR", TestParmConfig.GetStringParam("FilePrefix_LocPathCMRR"));
                module.ConfigData.AddString("FileSufix4CMRRStat", TestParmConfig.GetStringParam("FilePrefix_CmrrStat"));

                string Freq_Range_Str = TestParmConfig.GetStringParam("RangeFreq@Resp_GHz");
                module.ConfigData.AddOrUpdateReference("Freq_Range", GetListOfFrequencies(Freq_Range_Str));

                module.ConfigData.AddBool("IsAdjustSigOpcPower", TestParmConfig.GetBoolParam("IsAdjustSigPower@Resp"));
                module.ConfigData.AddDouble("RqsSigOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Sig@Resp"));
                module.ConfigData.AddBool("IsAdjustLocOpcPower", TestParmConfig.GetBoolParam("IsAdjustLocPower@Resp"));
                module.ConfigData.AddDouble("RqsLocOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Loc@Resp"));

                #region Add parameters to limit
                paramName = string .Format ("CMRR_XI_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XI_Avg");
                paramName = string .Format ("CMRR_XI_LO_{0:}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XI_Max");
                paramName = string .Format ("CMRR_XI_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XI_Min");
                
                paramName = string .Format ("CMRR_XI_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XI_Avg");
                paramName = string .Format ("CMRR_XI_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XI_Max");
                paramName = string .Format ("CMRR_XI_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XI_Min");

                paramName = string .Format ("CMRR_XQ_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XQ_Avg");
                paramName = string .Format ("CMRR_XQ_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XQ_Max");
                paramName = string .Format ("CMRR_XQ_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XQ_Min");
                
                paramName = string .Format ("CMRR_XQ_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XQ_Avg");
                paramName = string .Format ("CMRR_XQ_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XQ_Max");
                paramName = string .Format ("CMRR_XQ_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XQ_Min");

                paramName = string .Format ("CMRR_YI_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YI_Avg");
                paramName = string .Format ("CMRR_YI_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YI_Max");
                paramName = string .Format ("CMRR_YI_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YI_Min");

                paramName = string .Format ("CMRR_YI_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YI_Avg");
                paramName = string .Format ("CMRR_YI_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YI_Max");
                paramName = string .Format ("CMRR_YI_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YI_Min");

                paramName = string .Format ("CMRR_YQ_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YQ_Avg");
                paramName = string .Format ("CMRR_YQ_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YQ_Max");
                paramName = string .Format ("CMRR_YQ_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YQ_Min");
                
                paramName = string .Format ("CMRR_YQ_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YQ_Avg");
                paramName = string .Format ("CMRR_YQ_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YQ_Max");
                paramName = string .Format ("CMRR_YQ_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YQ_Min");

                paramName = string .Format ("RESP_XI1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Avg");
                paramName = string .Format ("RESP_XI1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Max");
                paramName = string .Format ("RESP_XI1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Min");

                paramName = string .Format ("RESP_XI1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Avg");
                paramName = string .Format ("RESP_XI1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Max");
                paramName = string .Format ("RESP_XI1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Min");

                paramName = string .Format ("RESP_XI2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI2_Avg");
                paramName = string .Format ("RESP_XI2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI2_Max");
                paramName = string .Format ("RESP_XI2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI2_Min");

                paramName = string .Format ("RESP_XI2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI2_Avg");
                paramName = string .Format ("RESP_XI2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI2_Max");
                paramName = string .Format ("RESP_XI2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI2_Min");

                paramName = string .Format ("RESP_XQ1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Avg");
                paramName = string .Format ("RESP_XQ1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Max");
                paramName = string.Format("RESP_XQ1_LO_{0}C_MIN", temp);
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Min");

                paramName = string .Format ("RESP_XQ1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Avg");
                paramName = string.Format ("RESP_XQ1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Max");
                paramName = string .Format ("RESP_XQ1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Min");

                paramName = string .Format ("RESP_XQ2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ2_Avg");
                paramName = string .Format ("RESP_XQ2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ2_Max");
                paramName = string .Format ("RESP_XQ2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ2_Min");

                paramName = string .Format ("RESP_XQ2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ2_Avg");
                paramName = string .Format ("RESP_XQ2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ2_Max");
                paramName = string .Format ("RESP_XQ2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ2_Min");

                paramName = string .Format ("RESP_YI1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Avg");
                paramName = string .Format ("RESP_YI1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Max");
                paramName = string .Format ("RESP_YI1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Min");
                
                paramName = string .Format ("RESP_YI1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Avg");
                paramName = string .Format ("RESP_YI1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Max");
                paramName = string .Format ("RESP_YI1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Min");

                paramName = string .Format ("RESP_YI2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI2_Avg");
                paramName = string .Format ("RESP_YI2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI2_Max");
                paramName = string .Format ("RESP_YI2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI2_Min");

                paramName = string .Format ("RESP_YI2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI2_Avg");
                paramName = string .Format ("RESP_YI2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI2_Max");
                paramName = string .Format ("RESP_YI2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI2_Min");

                paramName = string .Format ("RESP_YQ1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Avg");
                paramName = string .Format ("RESP_YQ1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Max");
                paramName = string .Format ("RESP_YQ1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Min");

                paramName =string .Format ("RESP_YQ1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Avg");
                paramName = string .Format ("RESP_YQ1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Max");
                paramName = string .Format ("RESP_YQ1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Min");

                paramName = string .Format ("RESP_YQ2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ2_Avg");
                paramName = string .Format ("RESP_YQ2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ2_Max");
                paramName = string .Format ("RESP_YQ2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ2_Min");

                paramName = string .Format ("RESP_YQ2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ2_Avg");
                paramName = string .Format ("RESP_YQ2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ2_Max");
                paramName = string .Format ("RESP_YQ2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ2_Min");

                paramName = string .Format ("DEVAVG_XI1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Avg");
                paramName = string .Format ("DEVAVG_XI1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Max");
                paramName = string .Format ("DEVAVG_XI1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Min");

                paramName = string .Format ("DEVAVG_XI1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Avg");
                paramName = string .Format ("DEVAVG_XI1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Max");
                paramName = string .Format ("DEVAVG_XI1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Min");

                paramName = string .Format ("DEVAVG_XI2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI2_Avg");
                paramName = string .Format ("DEVAVG_XI2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI2_Max");
                paramName = string .Format ("DEVAVG_XI2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI2_Min");

                paramName = string .Format ("DEVAVG_XI2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI2_Avg");
                paramName = string .Format ("DEVAVG_XI2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI2_Max");
                paramName = string .Format ("DEVAVG_XI2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI2_Min");

                paramName = string .Format ("DEVAVG_XQ1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Avg");
                paramName = string .Format ("DEVAVG_XQ1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Max");
                paramName = string .Format ("DEVAVG_XQ1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Min");

                paramName = string .Format ("DEVAVG_XQ1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Avg");
                paramName = string .Format ("DEVAVG_XQ1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Max");
                paramName = string .Format ("DEVAVG_XQ1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Min");

                paramName = string .Format ("DEVAVG_XQ2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ2_Avg");
                paramName = string .Format ("DEVAVG_XQ2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ2_Max");
                paramName = string .Format ("DEVAVG_XQ2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ2_Min");

                paramName = string .Format ("DEVAVG_XQ2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ2_Avg");
                paramName = string .Format ("DEVAVG_XQ2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ2_Max");
                paramName = string .Format ("DEVAVG_XQ2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ2_Min");

                paramName = string .Format ("DEVAVG_YI1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Avg");
                paramName = string .Format ("DEVAVG_YI1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Max");
                paramName = string .Format ("DEVAVG_YI1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Min");

                paramName = string .Format ("DEVAVG_YI1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Avg");
                paramName = string .Format ("DEVAVG_YI1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Max");
                paramName = string .Format ("DEVAVG_YI1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Min");

                paramName = string .Format ("DEVAVG_YI2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI2_Avg");
                paramName = string .Format ("DEVAVG_YI2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI2_Max");
                paramName = string .Format ("DEVAVG_YI2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI2_Min");

                paramName = string.Format ("DEVAVG_YI2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI2_Avg");
                paramName = string .Format ("DEVAVG_YI2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI2_Max");
                paramName = string .Format ("DEVAVG_YI2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI2_Min");

                paramName = string .Format("DEVAVG_YQ1_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Avg");
                paramName = string .Format ("DEVAVG_YQ1_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Max");
                paramName = string .Format ("DEVAVG_YQ1_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Min");

                paramName = string .Format ("DEVAVG_YQ1_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Avg");
                paramName = string .Format ("DEVAVG_YQ1_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Max");
                paramName = string .Format ("DEVAVG_YQ1_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Min");

                paramName = string .Format ("DEVAVG_YQ2_LO_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ2_Avg");
                paramName = string .Format ("DEVAVG_YQ2_LO_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ2_Max");
                paramName = string .Format ("DEVAVG_YQ2_LO_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ2_Min");

                paramName = string .Format ("DEVAVG_YQ2_SIG_{0}C_AVG",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ2_Avg");
                paramName = string .Format ("DEVAVG_YQ2_SIG_{0}C_MAX",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ2_Max");
                paramName = string .Format ("DEVAVG_YQ2_SIG_{0}C_MIN",temp );
                module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ2_Min");

                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    paramName = string.Format("RESP_TAP1_LO_{0}C_AVG", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Tap_Avg");
                    paramName = string.Format("RESP_TAP1_LO_{0}C_MAX", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Tap_Max");
                    paramName = string.Format("RESP_TAP1_LO_{0}C_MIN", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Tap_Min");

                    paramName = string.Format("RESP_TAP1_SIG_{0}C_AVG", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap_Avg");
                    paramName = string.Format("RESP_TAP1_SIG_{0}C_MAX", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap_Max");
                    paramName = string.Format("RESP_TAP1_SIG_{0}C_MIN", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap_Min");

                    paramName = string.Format("RESP_TAP1_LO_{0}C_{1:####}NM", temp, wavelen_Ref);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Tap");

                    paramName = string.Format("RESP_TAP1_SIG_{0}C_{1:####}NM", temp, wavelen_Ref);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap");
                }


                paramName = string .Format ("PLOT_PD_LO_{0}C",temp );
                module.Limits.AddParameter(MainSpec, paramName, "LocPathDataFile");
                paramName = string.Format("PLOT_PD_SIG_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathDataFile");
                
                paramName = string.Format("PLOT_CMRR_LO_{0}C", temp);
                if ( MainSpec.ParamLimitExists( paramName ))
                    module.Limits.AddParameter(MainSpec, paramName, "LocCMRRDataFile");
                paramName = string.Format("PLOT_CMRR_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "SigCMRRDataFile");
                
                paramName = string.Format("PLOT_DEV_LO_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "LocDevRespDataFile");
                paramName = string.Format("PLOT_DEV_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "SigDevRespDataFile");
                
                paramName = string.Format("PLOT_RESP_LO_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "LocRespDataFile");
                paramName = string.Format("PLOT_RESP_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "SigRespDataFile");
                paramName = string.Format("PLOT_RESP_STAT_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "RespStatFile");


                #endregion
            } // End for loop
        }
        #endregion
        #region Helper functions
        /// <summary>
        /// Convert a string to an array of integers representing the frequencies
        /// </summary>
        /// <exception cref="ArgumentException">when the input string is not in a valid format</exception>
        /// <param name="rangeStr">Example: "191100:50:191550,193400,196100:-50:195650,193550"</param>
        /// <returns>Array of integers with different frequencies</returns>
        private int[] GetListOfFrequencies(string rangeStr)
        {
            Exception ArgEx = new ArgumentException("Could not convert string below to a valid list of numbers:\n" + rangeStr);
            List<int> freqList = new List<int>();

            string[] temp = rangeStr.Split(',');
            foreach (string FreqLine in temp)
            {

                int freq = 0;
                if (Int32.TryParse(FreqLine, out freq))
                {
                    freqList.Add(freq);
                }
                else if (FreqLine.IndexOf(":") != -1)
                {
                    string[] range = FreqLine.Split(':');
                    if (range.Length != 3)
                        throw ArgEx; //when the are not 3 numbers

                    int StartFreq = 0, StepFreq = 0, LastFreq = 0;
                    if (int.TryParse(range[0], out StartFreq) && int.TryParse(range[1], out StepFreq) && int.TryParse(range[2], out LastFreq))
                    {   //reach here means found 3 integers seperated by two colons (:)
                        //check for valid numbers
                        if (StepFreq < 0)
                        {
                            if (LastFreq >= StartFreq)
                                throw ArgEx;
                            for (int i = StartFreq; i >= LastFreq; i = i + StepFreq)
                            {
                                freqList.Add(i);
                            }
                        }
                        else if (StepFreq > 0)
                        {

                            if (LastFreq <= StartFreq)
                                throw ArgEx;
                            for (int i = StartFreq; i <= LastFreq; i = i + StepFreq)
                            {
                                freqList.Add(i);
                            }
                        }
                        else throw ArgEx; // when step is 0

                    }

                    else throw ArgEx; // when not valid 3 integers in the x:y:z range format

                }
                else throw ArgEx; // when one of the comma seperated items is neither a valid range format (x:y:x) nor a number

            }

            int[] freqArray = new int[freqList.Count];
            freqList.CopyTo(freqArray);
            return freqArray;


        }
        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void ReadCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null) // if instrument sn is provided, get cal data by sn
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        /// <summary>
        /// Run at the end of test to archive results for this device
        /// </summary>
        private string ArchiveBlobfiles(DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                TestParmConfig.GetStringParam("ResultsArchiveDirectory"),
                "BlobFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(TestParmConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(TestParmConfig.GetStringParam("PlotDataTempDirectory")))
            {
                string[] csvFilesToAdd = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);
                string[] zipFilesToAdd = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.zip", SearchOption.TopDirectoryOnly);
                string[] txtFilesToAdd = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.txt", SearchOption.TopDirectoryOnly);

                if (csvFilesToAdd.Length > 0 || zipFilesToAdd.Length > 0 || txtFilesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    
                    using (zipFile)
                    {
                        if (csvFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in csvFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (zipFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in zipFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (txtFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in txtFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }
                    }
                    return zipFileName;
                }
            }
            return null;
        }

        /// <summary>
        /// Run at the start of test to delete any old CSV data
        /// </summary>
        protected void CleanUpBlobFiles()
        {
            if (!Directory.Exists(TestParmConfig.GetStringParam("PlotDataTempDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"), "*.csv");
            string[] zipFilesToRemove = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"), "*.zip");
            string[] txtFilesToRemove = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"), "*.txt");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in zipFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in txtFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        /// <summary>
        /// To decide the bias applying to auxilary pd
        ///   the chip wafer no.>=CQRT0072A, then the bias is 14v
        ///   the chip wafer no.less than CQRT0072A, then the bias is 10V
        /// </summary>
        /// <param name="waferID"></param>
        /// <returns>Auxilary pd bias level in volt </returns>
        public double GetAuxPdBias_V(string waferID)
        {
            string[] ArrayWaferID = waferID.Split('.');
            string[] ArrayWaferStander = TestParmConfig.GetStringParam("LeastWfId2useHighAuxPdBias").Split('.');

            string TempWaferID = "";
            string WaferStander = "";

            if (ArrayWaferID[0].Length > 8)
            {
                TempWaferID = ArrayWaferID[0].Substring(4, 4);
            }
            else
            {
                TempWaferID = ArrayWaferID[0].Substring(3, 4);
            }

            if (ArrayWaferStander[0].Length > 8)
            {
                WaferStander = ArrayWaferStander[0].Substring(4, 4);
            }
            else
            {
                WaferStander = ArrayWaferStander[0].Substring(3, 4);
            }

            if (string.Compare(TempWaferID, WaferStander, true) >= 0)
            {
                return double.Parse(MainSpec.GetParamLimit("TC2_AUX_PD_REVERSE_BIAS").HighLimit.ValueToString()); //TestParmConfig.GetDoubleParam("VauxPD_High_Full_V");
            }
            else
            {
                return double.Parse(MainSpec.GetParamLimit("TC1_AUX_PD_REVERSE_BIAS").HighLimit.ValueToString());//TestParmConfig.GetDoubleParam("VauxPD_Low_Full_V");
            }
        }
        #endregion
    }
}
