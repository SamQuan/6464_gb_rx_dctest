using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Class to get stage information for the test control
    /// </summary>
    public class Util_GetStageInfo
    {
        /// <summary>
        /// Read Stage name from a CSV file, the first coloum should be the stage name
        /// </summary>
        /// <param name="fileName">Csv file that contains stage information </param>
        /// <returns></returns>
        public static List<string> GetStageInfo(string fileName)
        {
            StreamReader reader;
            List<string> stageList = new List<string>();

            try
            {
                reader = new StreamReader(fileName);
            }
            catch( Exception ex)
            {
                throw ex;
            }
            if (reader != null)
            {
                // skip the header line.
                reader.ReadLine();

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    string[] fields = line.Split(new char[] { ',' });

                    if (fields.Length >= 1)
                    {
                        stageList.Add(fields[0].Trim());
                    }
                }
                reader.Close();
            }
            return stageList;
        }
    }
}
