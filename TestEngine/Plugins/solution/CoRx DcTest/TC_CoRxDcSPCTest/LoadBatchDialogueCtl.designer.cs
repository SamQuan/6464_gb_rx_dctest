// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.Designer.cs
//
// Author: alice.huang, 2009
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    partial class LoadBatchDialogueCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtSN = new System.Windows.Forms.TextBox();
            this.btnLoadBatch = new System.Windows.Forms.Button();
            this.lblMesMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(32, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lot ID";
            // 
            // txtSN
            // 
            this.txtSN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSN.Location = new System.Drawing.Point(35, 59);
            this.txtSN.Name = "txtSN";
            this.txtSN.Size = new System.Drawing.Size(168, 31);
            this.txtSN.TabIndex = 1;
            this.txtSN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSN_KeyPress);
            // 
            // btnLoadBatch
            // 
            this.btnLoadBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadBatch.ForeColor = System.Drawing.Color.Blue;
            this.btnLoadBatch.Location = new System.Drawing.Point(565, 161);
            this.btnLoadBatch.Name = "btnLoadBatch";
            this.btnLoadBatch.Size = new System.Drawing.Size(172, 31);
            this.btnLoadBatch.TabIndex = 2;
            this.btnLoadBatch.Text = "&Load Batch";
            this.btnLoadBatch.UseVisualStyleBackColor = true;
            this.btnLoadBatch.Click += new System.EventHandler(this.btnLoadBatch_Click);
            // 
            // lblMesMessage
            // 
            this.lblMesMessage.AutoSize = true;
            this.lblMesMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMesMessage.ForeColor = System.Drawing.Color.Blue;
            this.lblMesMessage.Location = new System.Drawing.Point(30, 137);
            this.lblMesMessage.Name = "lblMesMessage";
            this.lblMesMessage.Size = new System.Drawing.Size(70, 25);
            this.lblMesMessage.TabIndex = 3;
            this.lblMesMessage.Text = "label1";
            // 
            // LoadBatchDialogueCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblMesMessage);
            this.Controls.Add(this.btnLoadBatch);
            this.Controls.Add(this.txtSN);
            this.Controls.Add(this.label2);
            this.Name = "LoadBatchDialogueCtl";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.LoadBatchDialogueCtl_MsgReceived);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSN;
        private System.Windows.Forms.Button btnLoadBatch;
        private System.Windows.Forms.Label lblMesMessage;

    }
}
