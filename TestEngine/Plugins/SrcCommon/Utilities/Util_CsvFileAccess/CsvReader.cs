// [Copyright]
//
// Bookham Modular Test Engine
// Utilities
//
// Utilities\Util_CsvFileAccess\CsvReader.cs
// 
// Author: Paul Annetts
// Design: 


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Bookham.TestLibrary.Utilities
{
    /// <summary>
    /// Simple CsvReader class. No clever stuff with quote values in CSV elements!
    /// </summary>
    /// <remarks>This class is disposable, if disposed will close the file</remarks>
    public class CsvReader : IDisposable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CsvReader()
        {
            streamReader = null;
        }

        /// <summary>
        /// Open file
        /// </summary>
        /// <param name="csvFile">File name to open</param>
        public void OpenFile(string csvFile)
        {
            streamReader = new StreamReader(csvFile);
        }


        /// <summary>
        /// Close file
        /// </summary>
        public void CloseFile()
        {
            if (streamReader == null)
            {
                throw new Exception("Can't close the CsvReader file, none open!");
            }
            this.Dispose();            
        }


        /// <summary>
        /// Get next line from file. Returns null if empty
        /// </summary>
        /// <returns>Array of strings for this line</returns>
        public string[] GetLine()
        {
            // read the next line
            string line = streamReader.ReadLine();
            // check for end of file (null).
            if (line == null) return null;
            string[] elemsInLine = line.Split(',');
            return elemsInLine;
        }

        /// <summary>
        /// Reads the entire file
        /// </summary>
        /// <param name="csvFile">File name to open</param>
        /// <returns>List of Array of strings representing the whole file. Array can/will be jagged...</returns>
        public List<string[]> ReadFile(string csvFile)
        {
            this.OpenFile(csvFile);
            List<string[]> fileLines = new List<string[]>();

            string[] lineElems;
            while (true)
            {
                // read the next line
                lineElems = this.GetLine();
                // check for end of file
                if (lineElems == null) break;
                fileLines.Add(lineElems);
            }
            this.CloseFile();
            return fileLines;            
        }


        #region IDisposable Members

        /// <summary>
        /// IDisposable method
        /// </summary>
        public void Dispose()
        {
            if (streamReader != null)
            {
                streamReader.Dispose();
                streamReader = null;
            }
        }
        
        #endregion
        
        #region Private Data
        private StreamReader streamReader;
        #endregion

       
    }
}
