using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Class for polarisation tuning Instrument colection and functions
    /// </summary>
    public class CoRxPolarisationAdjustInstrs
    {
        /// <summary>
        /// Array of polarisation controllers available to be adjust 
        /// </summary>
        public Inst_HP11896A[] ArrPolController;

        /// <summary>
        /// Array of initialise Position setting for each polarisation controller
        /// </summary>
        public int[] ArrPolInitPosition;

        /// <summary>
        /// Position adjust step for each polarisation controller
        /// </summary>
        public int[] ArrPolAdjustStep;

        /// <summary>
        /// List of current sources to read current when adjust polarisation, it should have 2 sources, one for X chip, the other for Y chip
        /// </summary>
        public List<Inst_Ke24xx> LstCurrSources;

        /// <summary>
        ///  Current Tolerance to decide if miniment current is reached or not 
        /// </summary>
        public double MinCurrentTolerance_A;
        
        /// <summary>
        /// mS waiting for polarisation settle to read currents
        /// </summary>
        public int Tsettle_mS;

        /// <summary>
        ///  Sweep polarization to refind the optimise position that balance the lacal and signal path's current
        /// </summary>
        /// <param name="polController"></param>        
        /// <param name="stepPosition"></param>
        /// <param name="deltaCurr_Min"></param>        
        /// <returns> If get polarization to set the local and signal photo current into balance tolerance </returns>
        internal  bool OptimisePolarisation(Inst_HP11896A polController,int stepPosition, double deltaCurr_Min_A, out double deltaCurr_A)
        {
            bool isMinCurrDelta = false;

            int startPosition = polController .MinPosition ;
            int stopPosition = polController.MaxPosition;
            
            // To ensure each tunning begin at a same position
            polController.Position = startPosition;
            polController.WaitForOperationToComplete();
                        
            // set inititial value to extremly high as to get the min delta current from measurement by comparision
            deltaCurr_A = double.PositiveInfinity;
            for (int poi = startPosition; poi < stopPosition; poi += stepPosition)
            {
                polController.Position = poi;
                polController.WaitForOperationToComplete();

                System.Threading.Thread.Sleep(Tsettle_mS);
                deltaCurr_A =  Math.Abs(LstCurrSources[0].CurrentActual_amp - LstCurrSources[1].CurrentActual_amp);
                
                if (Math.Abs(deltaCurr_A - deltaCurr_Min_A) < MinCurrentTolerance_A)
                {
                    isMinCurrDelta = true;
                    break;
                }
            }

            return isMinCurrDelta;
        }

        /// <summary>
        /// Adjust a single channel in polarization controller to get best polarization status
        /// </summary>
        /// <param name="polController"> a single polarization controllor channel to be adjust </param>
        /// <param name="stepPosition"> step to vray the position during the adjustment </param>
        /// <returns></returns>
        internal ClsPolarisationAdjustSetting AdjustToOptimisePosition(Inst_HP11896A polController,int stepPosition)
        
        {
            int startPosition = polController .MinPosition ;
            int stopPosition = polController.MaxPosition;
            // To ensure each tunning begin at a same position
            polController.Position = startPosition;
            polController.WaitForOperationToComplete();

            List<StrcPolarisationPointReading> lstPolReading = new List<StrcPolarisationPointReading>();
            double deltaCurr_A;
            // set inititial value to extremly high as to get the min delta current from measurement by comparision
            double deltaCurrent_Min=double .PositiveInfinity ;
            int ind_Min = 0;
            int ind = 0;
            for (int poi = startPosition; poi < stopPosition; poi += stepPosition, ind++)
            {
                polController.Position = poi;
                polController.WaitForOperationToComplete();

                System.Threading.Thread.Sleep(Tsettle_mS);
                deltaCurr_A = Math.Abs(LstCurrSources[0].CurrentActual_amp  - LstCurrSources[1].CurrentActual_amp );  

                lstPolReading.Add(new StrcPolarisationPointReading(poi, deltaCurr_A));
                
               if ( deltaCurr_A < deltaCurrent_Min) 
               {
                   deltaCurrent_Min = deltaCurr_A;
                   ind_Min = ind;
               }
               if (deltaCurr_A < MinCurrentTolerance_A)    // if delta current is low enough to fall in tolerance required, stop tuning to reduce tuning time
                   break;
                
            }
            
            deltaCurrent_Min = lstPolReading[ind_Min].DeltaCurrent;

            // Refind the optimise polarization
            polController.Position = lstPolReading[ind_Min].PolPosition;
            polController.WaitForOperationToComplete();
            System.Threading.Thread.Sleep(Tsettle_mS*5);

            deltaCurr_A = Math.Abs(LstCurrSources[0].CurrentActual_amp - LstCurrSources[1].CurrentActual_amp);
            
            ClsPolarisationAdjustSetting polSetting = new ClsPolarisationAdjustSetting();
            polSetting.StartPosition = startPosition;
            polSetting.Step = stepPosition;
            polSetting.StopPosition = polController.Position;
            polSetting.DeltaCurrent_A = deltaCurr_A;

            return polSetting;
        }


        /// <summary>
        /// adjust all channels in polarization controller to get best polarization status
        /// </summary>
        /// <returns></returns>
        public bool GetOptimisePolarization()
        {
            if (ArrPolController  == null ) return true ;

            // Set all polarization to initialise position
            ArrPolController[0].SetDefaultState();
            ArrPolController[0].WaitForOperationToComplete();
            // set each polarisation controll axis to it mini position one by one
            for ( int indx = 0; indx< ArrPolController .Length ; indx++)
            {
                
                if (ArrPolInitPosition != null)
                    ArrPolController[indx].Position = ArrPolInitPosition[indx];
                else
                    ArrPolController[indx].Position = ArrPolController[indx].MinPosition;

            }
            // set the minmun point to be start and set the minnum delta current to be max double to get min in comparasion            
            int Indx_DeltaCur_Min = 0;
            double deltaCur_Min = double .PositiveInfinity;

            List<ClsPolarisationAdjustSetting> lstPolSetting = new List<ClsPolarisationAdjustSetting>();
            ClsPolarisationAdjustSetting polSetting= null ;
            
            // Adjust polarization status by adjusting channels one by one
            for (int indx = 0; indx < ArrPolController.Length; indx++)
            {
                if (ArrPolAdjustStep != null)
                {
                    polSetting = AdjustToOptimisePosition(ArrPolController[indx], ArrPolAdjustStep[indx]);
                }
                else
                    polSetting = AdjustToOptimisePosition(ArrPolController[indx], 1);

                // Record all polarization's seeting  forfurther recover
                polSetting.ArrPolInitPosition = new int[ ArrPolController .Length ];
                for (int tmp = 0; tmp < ArrPolController.Length; tmp++)
                {
                    polSetting.ArrPolInitPosition[tmp] = ArrPolController[tmp].Position;
                }

                if (deltaCur_Min > polSetting.DeltaCurrent_A)
                {
                    deltaCur_Min = polSetting.DeltaCurrent_A;
                    Indx_DeltaCur_Min = indx;
                }
                

                lstPolSetting.Add(polSetting);
                if (deltaCur_Min < MinCurrentTolerance_A)
                    break;
            }

            // If the optimise polarization status is not achieved by the last adjust channel, try to recover the settings 
            // to channel that the opotimise pol achieved during the whole adjust process
            if (Indx_DeltaCur_Min != lstPolSetting.Count - 1)  // if last tuning is not max , recover to max
            {
                for (int indx = 0; indx < ArrPolController.Length; indx++)
                {
                    ArrPolController[indx].Position = lstPolSetting[Indx_DeltaCur_Min].ArrPolInitPosition[indx];
                    ArrPolController[indx].WaitForOperationToComplete();
                }

                 if (ArrPolAdjustStep != null)
                {
                    polSetting = AdjustToOptimisePosition(ArrPolController[Indx_DeltaCur_Min], ArrPolAdjustStep[Indx_DeltaCur_Min]);
                }
                else
                    polSetting = AdjustToOptimisePosition(ArrPolController[Indx_DeltaCur_Min], 1);

                
            }

            return true;
        }
    }

    /// <summary>
    /// Structure to define the readings involves in each polarization channel position setting point
    /// </summary>
    public struct StrcPolarisationPointReading
    {
        /// <summary>
        /// Polarization position on a channel
        /// </summary>
        public int PolPosition;
        
        /// <summary>
        /// Delta current between 2 sources
        /// </summary>
        public double DeltaCurrent;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="position"></param>
        /// <param name="current">deltaCurrent</param>
        public StrcPolarisationPointReading(int position, double deltaCurrent)
        {
            PolPosition = position;
            DeltaCurrent = deltaCurrent;
        }
        /// <summary>
        /// Put position and delta current into a string, sperated with comma
        /// </summary>
        /// <returns></returns>
        public string ToSring()
        {
            return string.Format("{0},{1}", PolPosition, DeltaCurrent);
        }
    }

    /// <summary>
    /// Class for polarization controller single channel adjust setting 
    /// </summary>
    public class ClsPolarisationAdjustSetting:IComparable
    {
        /// <summary>
        /// Initial positions for all the channels available in the polarization controller frames
        /// </summary>
        public int[] ArrPolInitPosition;
        /// <summary>
        /// Single Channel in Polarization controller, this channel will be asjust with this setting
        /// </summary>
        public Inst_HP11896A PolController;
        /// <summary>
        /// Position to start adjust
        /// </summary>
        public int StartPosition;
        /// <summary>
        /// Last position in adjust
        /// </summary>
        public int StopPosition;
        /// <summary>
        /// Position adjust step
        /// </summary>
        public int Step;
        /// <summary>
        /// Delta current btw the specified sources at the optimise position achieves by this adjustment
        /// </summary>
        public double DeltaCurrent_A;
        
        #region IComparable Members

        /// <summary>
        /// Compare the object's DealtaCurrent_A then return the result
        /// </summary>
        /// <param name="obj"></param>
        /// <returns> return the the compare result of the current sum reading </returns>
        public int CompareTo(object obj)
        {
            ClsPolarisationAdjustSetting tmp = (ClsPolarisationAdjustSetting)obj;
            if (tmp != null)
            {
                double deltaCur = this.DeltaCurrent_A - tmp.DeltaCurrent_A;

                if (deltaCur == 0)
                    return 0;
                else if (deltaCur > 0)
                    return 1;
                else
                    return -1;
            }
            else
                return -1;
        }

        #endregion
    }
}
