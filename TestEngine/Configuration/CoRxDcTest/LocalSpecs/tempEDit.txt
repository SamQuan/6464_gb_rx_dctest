ATTEN_X_SIG_ZAZC_1529NM_GRAD,Gradient of attenuation on X chip at ZZZ degC,0.1,25,dB/V,#0.00,0,2,0
ATTEN_X_SIG_ZAZC_1529NM_MAX,Maximum attenuation on X chip at ZZZ degC,15,9999,dB,#0.00,0,2,0
ATTEN_X_SIG_ZAZC_1569NM_GRAD,Gradient of attenuation on X chip at ZZZ degC,0.1,25,dB/V,#0.00,0,2,0
ATTEN_X_SIG_ZAZC_1569NM_MAX,Maximum attenuation on X chip at ZZZ degC,15,9999,dB,#0.00,0,2,0
ATTEN_Y_SIG_ZAZC_1529NM_GRAD,Gradient of attenuation on Y chip at ZZZ degC,0.1,25,dB/V,#0.00,0,2,0
ATTEN_Y_SIG_ZAZC_1529NM_MAX,Maximum attenuation on Y chip at ZZZ degC,15,9999,dB,#0.00,0,2,0
ATTEN_Y_SIG_ZAZC_1569NM_GRAD,Gradient of attenuation on Y chip at ZZZ degC,0.1,25,dB/V,#0.00,0,2,0
ATTEN_Y_SIG_ZAZC_1569NM_MAX,Maximum attenuation on Y chip at ZZZ degC,15,9999,dB,#0.00,0,2,0
CMRR_XI_LO_ZAZC_AVG,Average common mode rejection ratio of XI PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_XI_LO_ZAZC_MAX,Maximum common mode rejection ratio of XI PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_XI_LO_ZAZC_MIN,Minimum common mode rejection ratio of XI PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_XI_SIG_ZAZC_AVG,Average common mode rejection ratio of XI PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_XI_SIG_ZAZC_MAX,Maximum common mode rejection ratio of XI PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_XI_SIG_ZAZC_MIN,Minimum common mode rejection ratio of XI PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_XQ_LO_ZAZC_AVG,Average common mode rejection ratio of XQ PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_XQ_LO_ZAZC_MAX,Maximum common mode rejection ratio of XQ PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_XQ_LO_ZAZC_MIN,Minimum common mode rejection ratio of XQ PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_XQ_SIG_ZAZC_AVG,Average common mode rejection ratio of XQ PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_XQ_SIG_ZAZC_MAX,Maximum common mode rejection ratio of XQ PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_XQ_SIG_ZAZC_MIN,Minimum common mode rejection ratio of XQ PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_YI_LO_ZAZC_AVG,Average common mode rejection ratio of YI PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_YI_LO_ZAZC_MAX,Maximum common mode rejection ratio of YI PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_YI_LO_ZAZC_MIN,Minimum common mode rejection ratio of YI PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_YI_SIG_ZAZC_AVG,Average common mode rejection ratio of YI PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_YI_SIG_ZAZC_MAX,Maximum common mode rejection ratio of YI PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_YI_SIG_ZAZC_MIN,Minimum common mode rejection ratio of YI PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_YQ_LO_ZAZC_AVG,Average common mode rejection ratio of YQ PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_YQ_LO_ZAZC_MAX,Maximum common mode rejection ratio of YQ PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_YQ_LO_ZAZC_MIN,Minimum common mode rejection ratio of YQ PDs with local oscillator at ZZZC,-9999,-20,dBe,#0.00,0,2,0
CMRR_YQ_SIG_ZAZC_AVG,Average common mode rejection ratio of YQ PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_YQ_SIG_ZAZC_MAX,Maximum common mode rejection ratio of YQ PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
CMRR_YQ_SIG_ZAZC_MIN,Minimum common mode rejection ratio of YQ PDs with signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
DARKCURRENT_TAP_ZAZC,Dark current of MPD at ZZZ degC,-9999,1,nA,#0.0,0,2,0
DARKCURRENT_XIN_ZAZC,Dark current of XI2 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_XIP_ZAZC,Dark current of XI1 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_XQN_ZAZC,Dark current of XQ2 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_XQP_ZAZC,Dark current of XQ1 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_YIN_ZAZC,Dark current of YI2 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_YIP_ZAZC,Dark current of YI1 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_YQN_ZAZC,Dark current of YQ2 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DARKCURRENT_YQP_ZAZC,Dark current of YQ1 PD at ZZZ degC,5,300,nA,#0.0,0,2,0
DEV_X_LO_ZAZC_FILE,x deviation for local input at ZZZC,-9999,9999,none,&,0,3,0
DEV_X_SIG_ZAZC_FILE,x deviation for signal input at ZZZC,-9999,9999,none,&,0,3,0
DEV_XIN_LO_ZAZC_AVG,Average deviation from average of XI2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIN_LO_ZAZC_MAX,Maximum deviation from average of XI2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIN_LO_ZAZC_MIN,Minimum deviation from average of XI2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIN_SIG_ZAZC_AVG,Average deviation from average of XI2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIN_SIG_ZAZC_MAX,Maximum deviation from average of XI2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIN_SIG_ZAZC_MIN,Minimum deviation from average of XI2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIP_LO_ZAZC_AVG,Average deviation from average of XI1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIP_LO_ZAZC_MAX,Maximum deviation from average of XI1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIP_LO_ZAZC_MIN,Minimum deviation from average of XI1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIP_SIG_ZAZC_AVG,Average deviation from average of XI1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIP_SIG_ZAZC_MAX,Maximum deviation from average of XI1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XIP_SIG_ZAZC_MIN,Minimum deviation from average of XI1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQN_LO_ZAZC_AVG,Average deviation from average of XQ2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQN_LO_ZAZC_MAX,Maximum deviation from average of XQ2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQN_LO_ZAZC_MIN,Minimum deviation from average of XQ2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQN_SIG_ZAZC_AVG,Average deviation from average of XQ2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQN_SIG_ZAZC_MAX,Maximum deviation from average of XQ2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQN_SIG_ZAZC_MIN,Minimum deviation from average of XQ2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQP_LO_ZAZC_AVG,Average deviation from average of XQ1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQP_LO_ZAZC_MAX,Maximum deviation from average of XQ1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQP_LO_ZAZC_MIN,Minimum deviation from average of XQ1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQP_SIG_ZAZC_AVG,Average deviation from average of XQ1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQP_SIG_ZAZC_MAX,Maximum deviation from average of XQ1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_XQP_SIG_ZAZC_MIN,Minimum deviation from average of XQ1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_Y_LO_ZAZC_FILE,y deviation for local input at ZZZC,-9999,9999,none,&,0,3,0
DEV_Y_SIG_ZAZC_FILE,y deviation for signal input at ZZZC,-9999,9999,none,&,0,3,0
DEV_YIN_LO_ZAZC_AVG,Average deviation from average of YI2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIN_LO_ZAZC_MAX,Maximum deviation from average of YI2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIN_LO_ZAZC_MIN,Minimum deviation from average of YI2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIN_SIG_ZAZC_AVG,Average deviation from average of YI2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIN_SIG_ZAZC_MAX,Maximum deviation from average of YI2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIN_SIG_ZAZC_MIN,Minimum deviation from average of YI2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIP_LO_ZAZC_AVG,Average deviation from average of YI1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIP_LO_ZAZC_MAX,Maximum deviation from average of YI1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIP_LO_ZAZC_MIN,Minimum deviation from average of YI1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIP_SIG_ZAZC_AVG,Average deviation from average of YI1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIP_SIG_ZAZC_MAX,Maximum deviation from average of YI1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YIP_SIG_ZAZC_MIN,Minimum deviation from average of YI1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQN_LO_ZAZC_AVG,Average deviation from average of YQ2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQN_LO_ZAZC_MAX,Maximum deviation from average of YQ2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQN_LO_ZAZC_MIN,Minimum deviation from average of YQ2 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQN_SIG_ZAZC_AVG,Average deviation from average of YQ2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQN_SIG_ZAZC_MAX,Maximum deviation from average of YQ2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQN_SIG_ZAZC_MIN,Minimum deviation from average of YQ2 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQP_LO_ZAZC_AVG,Average deviation from average of YQ1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQP_LO_ZAZC_MAX,Maximum deviation from average of YQ1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQP_LO_ZAZC_MIN,Minimum deviation from average of YQ1 PD with local oscillator at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQP_SIG_ZAZC_AVG,Average deviation from average of YQ1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQP_SIG_ZAZC_MAX,Maximum deviation from average of YQ1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
DEV_YQP_SIG_ZAZC_MIN,Minimum deviation from average of YQ1 PD with signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
ICC_TIA_X_ZAZC,TIA_X current draw at ZZZ degC,140,170,mA,#0.00,0,2,0
ICC_TIA_Y_ZAZC,TIA_Y current draw at ZZZ degC,140,170,mA,#0.00,0,2,0
MAX_LANE_CMRR_LO_ZAZC,Maximum common mode rejection ratio LO at ZZZC,-9999,-20,dBe,#0.00,0,2,0
MAX_LANE_CMRR_SIG_ZAZC,Maximum common mode rejection ratio Signal at ZZZC,-9999,-22,dBe,#0.00,0,2,0
MAX_LANE_DARKCURRENT_ZAZC,Maximum dark current at ZZZC,5,300,nA,#0.0,0,2,0
MAX_LANE_DEV_LO_ZAZC,Maximum deviation for LO at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
MAX_LANE_DEV_SIG_ZAZC,Maximum deviation for Signal at ZZZC,-1.24,1.24,dB,#0.00,0,2,0
MAX_LANE_RIPPLE_LOC_ZAZC,max responsivity ripple with local input over lanes and wavelength at ZZZC,0,10,%,#0.0,0,2,0
MAX_X_DEV_LO_ZAZC,max x deviation for local input at ZZZC,-9999,9999,dB,#0.00,0,2,0
MAX_X_DEV_SIG_ZAZC,max x deviation for signal input at ZZZC,-9999,9999,dB,#0.00,0,2,0
MAX_Y_DEV_LO_ZAZC,max y deviation for local input at ZZZC,-9999,9999,dB,#0.00,0,2,0
MAX_Y_DEV_SIG_ZAZC,max y deviation for signal input at ZZZC,-9999,9999,dB,#0.00,0,2,0
MIN_LANE_RESP_LO_ZAZC,Min responsivity LO at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
MIN_LANE_RESP_SIG_ZAZC,Min Responsivity Signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
PD_LO_ZAZC_FILE,data of PDs with local oscillator at ZZZ degC,-9999,9999,none,&,0,3,0
PD_SIG_ZAZC_FILE,data of PDs with signal at ZZZ degC,-9999,9999,none,&,0,3,0
PLOT_CMRR_LO_ZAZC,plot for  cmrr for local Osc @normal temperature,-9999,9999,none,&,0,3,0
PLOT_CMRR_SIG_ZAZC,plot for  cmrr for signal Osc @ normal temperature,-9999,9999,none,&,0,3,0
PLOT_PD_LO_ZAZC,Plot of PDs with local oscillator at ZZZ degC,-9999,9999,file,&,0,4,0
PLOT_PD_SIG_ZAZC,Plot of PDs with signal at ZZZ degC,-9999,9999,file,&,0,4,0
PLOT_RESP_LO_ZAZC,plot for  Potodiode responsivity for local Osc @normal temperature,-9999,9999,none,&,0,3,0
PLOT_RESP_SIG_ZAZC,plot for  Potodiode responsivity for signal Osc @ normal temperature,-9999,9999,none,&,0,3,0
PLOT_STAT_ZAZC,plot for statistics  @normal temperature,-9999,9999,none,&,0,3,0
PLOT_VOA_SIG_ZAZC_1529NM,Plot of VOA with signal at ZZZC,-9999,9999,file,&,0,4,0
PLOT_VOA_SIG_ZAZC_1569NM,Plot of VOA with signal at ZZZC,-9999,9999,file,&,0,4,0
RESP_TAP_SIG_ZAZC_1550NM,Responsivity of MPD with signal at ZZZ at 1550nm,0.013,0.03,A/W,#0.000,0,2,0
RESP_TAP_SIG_ZAZC_AVG,Average Responsivity of MPD with signal at ZZZC,0.01,0.03,A/W,#0.000,0,2,0
RESP_TAP_SIG_ZAZC_MAX,Maximum Responsivity of MPD with signal at ZZZC,0.01,0.035,A/W,#0.000,0,2,0
RESP_TAP_SIG_ZAZC_MIN,Minimum Responsivity of MPD with signal at ZZZC,0.01,0.035,A/W,#0.000,0,2,0
RESP_XIN_LO_ZAZC_1550NM,Responsivity of XI2 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIN_LO_ZAZC_AVG,Average responsivity of XI2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIN_LO_ZAZC_MAX,Maximum responsivity of XI2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIN_LO_ZAZC_MIN,Minimum responsivity of XI2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIN_SIG_ZAZC_1550NM,Responsivity of XI2 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIN_SIG_ZAZC_AVG,Average responsivity of XI2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIN_SIG_ZAZC_MAX,Maximum responsivity of XI2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIN_SIG_ZAZC_MIN,Minimum responsivity of XI2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIP_LO_ZAZC_1550NM,Responsivity of XI1 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIP_LO_ZAZC_AVG,Average responsivity of XI1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIP_LO_ZAZC_MAX,Maximum responsivity of XI1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIP_LO_ZAZC_MIN,Minimum responsivity of XI1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XIP_SIG_ZAZC_1550NM,Responsivity of XI2 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIP_SIG_ZAZC_AVG,Average responsivity of XI1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIP_SIG_ZAZC_MAX,Maximum responsivity of XI1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XIP_SIG_ZAZC_MIN,Minimum responsivity of XI1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQN_LO_ZAZC_1550NM,Responsivity of XQ2 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQN_LO_ZAZC_AVG,Average responsivity of XQ2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQN_LO_ZAZC_MAX,Maximum responsivity of XQ2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQN_LO_ZAZC_MIN,Minimum responsivity of XQ2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQN_SIG_ZAZC_1550NM,Responsivity of XQ2 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQN_SIG_ZAZC_AVG,Average responsivity of XQ2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQN_SIG_ZAZC_MAX,Maximum responsivity of XQ2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQN_SIG_ZAZC_MIN,Minimum responsivity of XQ2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQP_LO_ZAZC_1550NM,Responsivity of XQ1 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQP_LO_ZAZC_AVG,Average responsivity of XQ1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQP_LO_ZAZC_MAX,Maximum responsivity of XQ1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQP_LO_ZAZC_MIN,Minimum responsivity of XQ1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_XQP_SIG_ZAZC_1550NM,Responsivity of XQ1 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQP_SIG_ZAZC_AVG,Average responsivity of XQ1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQP_SIG_ZAZC_MAX,Maximum responsivity of XQ1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_XQP_SIG_ZAZC_MIN,Minimum responsivity of XQ1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIN_LO_ZAZC_1550NM,Responsivity of YI2 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIN_LO_ZAZC_AVG,Average responsivity of YI2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIN_LO_ZAZC_MAX,Maximum responsivity of YI2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIN_LO_ZAZC_MIN,Minimum responsivity of YI2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIN_SIG_ZAZC_1550NM,Responsivity of YI2 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIN_SIG_ZAZC_AVG,Average responsivity of YI2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIN_SIG_ZAZC_MAX,Maximum responsivity of YI2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIN_SIG_ZAZC_MIN,Minimum responsivity of YI2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIP_LO_ZAZC_1550NM,Responsivity of YI1 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIP_LO_ZAZC_AVG,Average responsivity of YI1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIP_LO_ZAZC_MAX,Maximum responsivity of YI1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIP_LO_ZAZC_MIN,Minimum responsivity of YI1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YIP_SIG_ZAZC_1550NM,Responsivity of YI1 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIP_SIG_ZAZC_AVG,Average responsivity of YI1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIP_SIG_ZAZC_MAX,Maximum responsivity of YI1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YIP_SIG_ZAZC_MIN,Minimum responsivity of YI1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQN_LO_ZAZC_1550NM,Responsivity of YQ2 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQN_LO_ZAZC_AVG,Average responsivity of YQ2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQN_LO_ZAZC_MAX,Maximum responsivity of YQ2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQN_LO_ZAZC_MIN,Minimum responsivity of YQ2 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQN_SIG_ZAZC_1550NM,Responsivity of YQ2 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQN_SIG_ZAZC_AVG,Average responsivity of YQ2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQN_SIG_ZAZC_MAX,Maximum responsivity of YQ2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQN_SIG_ZAZC_MIN,Minimum responsivity of YQ2 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQP_LO_ZAZC_1550NM,Responsivity of YQ1 PD with local oscillator at ref wavelength at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQP_LO_ZAZC_AVG,Average responsivity of YQ1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQP_LO_ZAZC_MAX,Maximum responsivity of YQ1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQP_LO_ZAZC_MIN,Minimum responsivity of YQ1 PD with local oscillator at ZZZC,0.035,0.125,A/W,#0.0000,0,2,0
RESP_YQP_SIG_ZAZC_1550NM,Responsivity of YQ1 PD with signal at ref wavelength at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQP_SIG_ZAZC_AVG,Average responsivity of YQ1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQP_SIG_ZAZC_MAX,Maximum responsivity of YQ1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RESP_YQP_SIG_ZAZC_MIN,Minimum responsivity of YQ1 PD with signal at ZZZC,0.04,0.125,A/W,#0.0000,0,2,0
RIPPLE_LO_ZAZC_FILE,ripple file for local input at ZZZC,-9999,9999,none,&,0,3,0
TEMPERATURE_DUT_ZAZC,DUT temperature at ZZZ degC,-9999,9999,degC,#0.00,0,2,0
TEMPERATURE_ROOM_ZAZC,Room temperature at ZZZ degC,-9999,9999,degC,#0.00,0,2,0
TEMPERATURE_SET_ZAZC,Temperature set at ZZZC,1,1,boolean,0,0,1,0
VOLTAGE_VOA10DB_X_ZAZC_1529NM,Voltage on VOA for 10dB of attenuation on X chip at ZZZC,5,8,V,#0.000,0,2,0
VOLTAGE_VOA10DB_X_ZAZC_1569NM,Voltage on VOA for 10dB of attenuation on X chip at ZZZC,5,8,V,#0.000,0,2,0
VOLTAGE_VOA10DB_Y_ZAZC_1529NM,Voltage on VOA for 10dB of attenuation on Y chip at ZZZC,5,8,V,#0.000,0,2,0
VOLTAGE_VOA10DB_Y_ZAZC_1569NM,Voltage on VOA for 10dB of attenuation on Y chip at ZZZC,5,8,V,#0.000,0,2,0
MAX_LANE_IMBALANCE_LO_ZAZC,Maximum imbalance LO at ZZZC,0,1.6,dB,#0.00,0,2,0
MAX_LANE_IMBALANCE_SIG_ZAZC,Maximum imbalance Signal at ZZZC,0,1.6,dB,#0.00,0,2,0
MAX_LANE_IMBALANCE_X_LO_ZAZC,Maximum X imbalance LO at ZZZC,0,1.25,dB,#0.00,0,2,0
MAX_LANE_IMBALANCE_X_SIG_ZAZC,Maximum X imbalance Signal at ZZZC,0,1.25,dB,#0.00,0,2,0
MAX_LANE_IMBALANCE_Y_LO_ZAZC,Maximum Y imbalance LO at ZZZC,0,1.25,dB,#0.00,0,2,0
MAX_LANE_IMBALANCE_Y_SIG_ZAZC,Maximum Y imbalance Signal at ZZZC,0,1.25,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_0V,Average X and Y attenuation for 0V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_1V,Average X and Y attenuation for 1V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_2V,Average X and Y attenuation for 2V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_3V,Average X and Y attenuation for 3V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_4V,Average X and Y attenuation for 4V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_5V,Average X and Y attenuation for 5V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_6V,Average X and Y attenuation for 6V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_7V,Average X and Y attenuation for 7V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_8V,Average X and Y attenuation for 8V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_AVE_9V,Average X and Y attenuation for 9V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_0V,Average X and Y attenuation for 0V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_1V,Average X and Y attenuation for 1V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_2V,Average X and Y attenuation for 2V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_3V,Average X and Y attenuation for 3V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_4V,Average X and Y attenuation for 4V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_5V,Average X and Y attenuation for 5V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_6V,Average X and Y attenuation for 6V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_7V,Average X and Y attenuation for 7V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_8V,Average X and Y attenuation for 8V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_AVE_9V,Average X and Y attenuation for 9V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_0DB,Polarisation dependent loss for 0dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_1DB,Polarisation dependent loss for 1dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_2DB,Polarisation dependent loss for 2dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_3DB,Polarisation dependent loss for 3dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_4DB,Polarisation dependent loss for 4dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_5DB,Polarisation dependent loss for 5dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_6DB,Polarisation dependent loss for 6dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_7DB,Polarisation dependent loss for 7dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_8DB,Polarisation dependent loss for 8dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_9DB,Polarisation dependent loss for 9dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1529NM_10DB,Polarisation dependent loss for 10dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_0DB,Polarisation dependent loss for 0dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_1DB,Polarisation dependent loss for 1dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_2DB,Polarisation dependent loss for 2dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_3DB,Polarisation dependent loss for 3dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_4DB,Polarisation dependent loss for 4dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_5DB,Polarisation dependent loss for 5dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_6DB,Polarisation dependent loss for 6dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_7DB,Polarisation dependent loss for 7dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_8DB,Polarisation dependent loss for 8dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_9DB,Polarisation dependent loss for 9dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_PDL_SIG_ZAZC_1569NM_10DB,Polarisation dependent loss for 10dB attenuation at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_0V,Wavelength dependent loss on XIP for 0V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_1V,Wavelength dependent loss on XIP for 1V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_2V,Wavelength dependent loss on XIP for 2V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_3V,Wavelength dependent loss on XIP for 3V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_4V,Wavelength dependent loss on XIP for 4V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_5V,Wavelength dependent loss on XIP for 5V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_6V,Wavelength dependent loss on XIP for 6V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_7V,Wavelength dependent loss on XIP for 7V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_8V,Wavelength dependent loss on XIP for 8V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_X_SIG_ZAZC_9V,Wavelength dependent loss on XIP for 9V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_0V,Wavelength dependent loss on YIP for 0V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_1V,Wavelength dependent loss on YIP for 1V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_2V,Wavelength dependent loss on YIP for 2V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_3V,Wavelength dependent loss on YIP for 3V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_4V,Wavelength dependent loss on YIP for 4V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_5V,Wavelength dependent loss on YIP for 5V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_6V,Wavelength dependent loss on YIP for 6V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_7V,Wavelength dependent loss on YIP for 7V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_8V,Wavelength dependent loss on YIP for 8V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_WDL_Y_SIG_ZAZC_9V,Wavelength dependent loss on YIP for 9V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_0V,Attenuation measured on MPD for 0V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_1V,Attenuation measured on MPD for 1V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_2V,Attenuation measured on MPD for 2V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_3V,Attenuation measured on MPD for 3V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_4V,Attenuation measured on MPD for 4V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_5V,Attenuation measured on MPD for 5V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_6V,Attenuation measured on MPD for 6V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_7V,Attenuation measured on MPD for 7V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_8V,Attenuation measured on MPD for 8V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1529NM_MPD_9V,Attenuation measured on MPD for 9V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_0V,Attenuation measured on MPD for 0V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_1V,Attenuation measured on MPD for 1V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_2V,Attenuation measured on MPD for 2V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_3V,Attenuation measured on MPD for 3V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_4V,Attenuation measured on MPD for 4V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_5V,Attenuation measured on MPD for 5V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_6V,Attenuation measured on MPD for 6V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_7V,Attenuation measured on MPD for 7V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_8V,Attenuation measured on MPD for 8V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
ATTEN_SIG_ZAZC_1569NM_MPD_9V,Attenuation measured on MPD for 9V VOA bias at ZZZC,-9999,9999,dB,#0.00,0,2,0
VOA_CURRENT_MAX_ZAZC,Maximum VOA current at ZZZC,-9999,9999,mA,#0.00,0,2,0
MPD_LO_ISOLATION_MIN_ZAZC,Minimum LO isolation on MPD at ZZZC,33,9999,dB,#0.00,0,2,0
