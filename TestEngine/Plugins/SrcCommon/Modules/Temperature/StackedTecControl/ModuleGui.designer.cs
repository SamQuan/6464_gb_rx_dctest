// [Copyright]
//
// Bookham Test Engine
// TecControl
//
// Bookham.TestLibrary.TestModules/ModuleGui
// 
// Author: paul.annetts
// Design: TODO

namespace Bookham.TestLibrary.TestModules
{
    partial class ModuleGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSetPointTemperature11 = new System.Windows.Forms.Label();
            this.labelActTemperature1 = new System.Windows.Forms.Label();
            this.Sp1Value = new System.Windows.Forms.Label();
            this.SP2Value = new System.Windows.Forms.Label();
            this.ActualValue1 = new System.Windows.Forms.Label();
            this.ActualValue2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelSetPointTemperature11
            // 
            this.labelSetPointTemperature11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelSetPointTemperature11.AutoSize = true;
            this.labelSetPointTemperature11.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSetPointTemperature11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelSetPointTemperature11.Location = new System.Drawing.Point(281, 50);
            this.labelSetPointTemperature11.Name = "labelSetPointTemperature11";
            this.labelSetPointTemperature11.Size = new System.Drawing.Size(135, 22);
            this.labelSetPointTemperature11.TabIndex = 0;
            this.labelSetPointTemperature11.Text = "SetPoint (Deg)";
            // 
            // labelActTemperature1
            // 
            this.labelActTemperature1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelActTemperature1.AutoSize = true;
            this.labelActTemperature1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActTemperature1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelActTemperature1.Location = new System.Drawing.Point(456, 50);
            this.labelActTemperature1.Name = "labelActTemperature1";
            this.labelActTemperature1.Size = new System.Drawing.Size(157, 22);
            this.labelActTemperature1.TabIndex = 3;
            this.labelActTemperature1.Text = "Read Value(Deg)";
            // 
            // Sp1Value
            // 
            this.Sp1Value.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Sp1Value.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Sp1Value.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Sp1Value.Location = new System.Drawing.Point(255, 84);
            this.Sp1Value.Name = "Sp1Value";
            this.Sp1Value.Size = new System.Drawing.Size(157, 33);
            this.Sp1Value.TabIndex = 4;
            // 
            // SP2Value
            // 
            this.SP2Value.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SP2Value.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.SP2Value.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SP2Value.Location = new System.Drawing.Point(255, 155);
            this.SP2Value.Name = "SP2Value";
            this.SP2Value.Size = new System.Drawing.Size(157, 33);
            this.SP2Value.TabIndex = 5;
            // 
            // ActualValue1
            // 
            this.ActualValue1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ActualValue1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ActualValue1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActualValue1.Location = new System.Drawing.Point(456, 84);
            this.ActualValue1.Name = "ActualValue1";
            this.ActualValue1.Size = new System.Drawing.Size(157, 33);
            this.ActualValue1.TabIndex = 6;
            // 
            // ActualValue2
            // 
            this.ActualValue2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ActualValue2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ActualValue2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActualValue2.Location = new System.Drawing.Point(457, 155);
            this.ActualValue2.Name = "ActualValue2";
            this.ActualValue2.Size = new System.Drawing.Size(156, 33);
            this.ActualValue2.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(11, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 28);
            this.label1.TabIndex = 8;
            this.label1.Text = "Bottom Tec Controller";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(11, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(221, 28);
            this.label2.TabIndex = 9;
            this.label2.Text = "Top Tec Controller";
            // 
            // ModuleGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ActualValue2);
            this.Controls.Add(this.ActualValue1);
            this.Controls.Add(this.SP2Value);
            this.Controls.Add(this.Sp1Value);
            this.Controls.Add(this.labelActTemperature1);
            this.Controls.Add(this.labelSetPointTemperature11);
            this.Name = "ModuleGui";
            this.Size = new System.Drawing.Size(657, 280);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSetPointTemperature11;
        private System.Windows.Forms.Label labelActTemperature1;
        private System.Windows.Forms.Label Sp1Value;
        private System.Windows.Forms.Label SP2Value;
        private System.Windows.Forms.Label ActualValue1;
        private System.Windows.Forms.Label ActualValue2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;

    }
}
