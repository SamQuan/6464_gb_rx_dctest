// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_HP11896A.cs
//
// Author: Alexander.Wu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestSolution.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    public class Inst_HP11896A : InstType_PolarisationController
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_HP11896A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord hp11896AData = new InstrumentDataRecord(
                "HP11896A",				// hardware name 
                "0.0.0",  			// minimum valid firmware version 
                "3.0.0");			// maximum valid firmware version 
            ValidHardwareData.Add("HP11896A", hp11896AData);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord hp11896AChassisData = new InstrumentDataRecord(
                "Chassis_HP11896A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_HP11896A", hp11896AChassisData);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (ChassisType_Visa)base.InstrumentChassis;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                return "1.52.3";
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                return "HP11896A";
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write_Unchecked("*RST", this);
            instrumentChassis.Write_Unchecked(":ABORT", this);
            instrumentChassis.Write_Unchecked("*CLS", this);
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Instrument Function
        
        /// <summary>
        /// Set the Voltage Value of the Channel specified by the "channelNum"
        /// Channel Number: 1~4
        /// C1,C2,C3,C4 Voltage Range: 2~12V
        /// F.B. Range: 0~10V
        /// Max Step: 1.0V     Min Step: 0.001V
        /// Frequency Range: 0.1~2KHz
        /// </summary>
        /// <param name="channelNum">Channel Number: 1~4</param>
        /// <param name="voltageVal_V">Voltage Value: 2~12V</param>
        public override void SetVoltage(int channelNum, double voltageVal_V)
        {
            if (channelNum < 1 || channelNum > 4)
                throw new Exception("Channel number should be between 1, 2, 3, 4");
            string command = ":PADD" + channelNum.ToString("0") + ":POS " + voltageVal_V.ToString("0");            
            instrumentChassis.Write_Unchecked(command, this);
            System.Threading.Thread.Sleep(30);
        }

        /// <summary>
        /// Not Implement!!!
        /// </summary>
        /// <param name="channelNum"></param>
        public override void ReadVoltage(int channelNum)
        {
            if (channelNum < 1 || channelNum > 4)
                throw new Exception("Channel number should be between 1, 2, 3, 4");
            string command = ":PADD" + channelNum.ToString("0") + ":POS?";
            instrumentChassis.Query_Unchecked(command, this);
        }

        #endregion

        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private ChassisType_Visa instrumentChassis;
        #endregion
    }
}
