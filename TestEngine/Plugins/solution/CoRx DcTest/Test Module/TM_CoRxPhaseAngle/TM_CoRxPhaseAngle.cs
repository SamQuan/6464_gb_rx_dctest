// [Copyright]
//
// Bookham [Coherence Rx DC Test]
// Bookham.TestSolution.TestModules
//
// TM_CoRxPhaseAngle.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module for phase anglen error test
    /// </summary>
    public class TM_CoRxPhaseAngle : ITestModule
    {
        #region ITestModule Members
        /// <summary>
        /// CoRx phase angle test module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiShow();
            engine.GuiToFront();

            bool SavePDCurrents = configData.ReadBool("SavePdCurrents");

            double Vpd_V = configData.ReadDouble("PDBias_V");
            double Ipd_Compliance_A = configData.ReadDouble("PDCompCurrent_A");
            double Vcc_V = configData.ReadDouble("TiaBias_V");
            double Icc_Compliance_A = configData.ReadDouble("TIACompCurrent_A");
            
            double VauxPD_X_V = configData.ReadDouble("AuxPDBias_X_V");
            double IauxPD_Compliance_X_A = configData.ReadDouble("AuxPDCompCurrent_X_A");
            double VauxPD_Y_V = configData.ReadDouble("AuxPDBias_Y_V");
            double IauxPD_Compliance_Y_A = configData.ReadDouble("AuxPDCompCurrent_Y_A");
            
            Inst_Ke24xx masterSource = (Inst_Ke24xx)instruments["Master Source"];
            bool isPdSourceUseTrigger = configData.ReadBool("IsPdSourceUseTrigger");
            bool is6464 = CoRxDCTestInstruments.Is6464;

                        #region Get Control inputs settings
            double V_MC_V;
            double Icomp_MC_A;
            double V_GC_V;
            double Icomp_GC_A;
            double V_OC_V;
            double Icomp_OC_A;
            double V_OA_V;
            double Icomp_OA_A;
            double I_ThermPhase_A;
            double Vcomp_ThermPhase_V;
            if (configData.IsPresent("MCBias_V"))
            {
                V_MC_V = configData.ReadDouble("MCBias_V");
            }
            else 
            {
                V_MC_V = double.NaN;
            }
            if (configData.IsPresent("MCCurComp_A"))
            {
                Icomp_MC_A = configData.ReadDouble("MCCurComp_A");
            }
            else 
            {
                Icomp_MC_A = 0.01;
            }

            if (configData.IsPresent("GCBias_V"))
            {
                V_GC_V = configData.ReadDouble("GCBias_V");
            }
            else 
            {
                V_GC_V = 2.3;
            }
            if (configData.IsPresent("GCCurComp_A"))
            {
                Icomp_GC_A = configData.ReadDouble("GCCurComp_A");
            }
            else 
            {
                Icomp_GC_A = 0.01;
            }

            if (configData.IsPresent("OCBias_V"))
            {
                V_OC_V = configData.ReadDouble("OCBias_V");
            }
            else 
            {
                V_OC_V = double.NaN;
            }
            if (configData.IsPresent("OCCurComp_A"))
            {
                Icomp_OC_A = configData.ReadDouble("OCCurComp_A");
            }
            else 
            {
                Icomp_OC_A = 0.01;
            }

            if (configData.IsPresent("OABias_V"))
            {
                V_OA_V = configData.ReadDouble("OABias_V");
            }
            else
            {
                V_OA_V = double.NaN;
            }
            if (configData.IsPresent("OACurComp_A"))
            {
                Icomp_OA_A = configData.ReadDouble("OACurComp_A");
            }
            else
            {
                Icomp_OA_A = 0.01;
            }

            if (configData.IsPresent("ThermPhaseBias_A"))
            {
                I_ThermPhase_A = configData.ReadDouble("ThermPhaseBias_A");
            }
            else
            {
                I_ThermPhase_A = double.NaN;
            }
            if (configData.IsPresent("ThermPhaseVoltComp_A"))
            {
                Vcomp_ThermPhase_V = configData.ReadDouble("ThermPhaseVoltComp_A");
            }
            else
            {
                Vcomp_ThermPhase_V = 1;
            }

            #endregion

            #region Get frequency information
            EnumFrequencySetMode freqSetMode = EnumFrequencySetMode.ByChan;
            double[] Arrfreq = null;
            if (configData.IsPresent("FrequencyArray"))
            {
                Arrfreq = configData.ReadDoubleArray("FrequencyArray");                
                freqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            else if (configData.IsPresent("WavelenArray"))
            {
                Arrfreq = configData.ReadDoubleArray("WavelenArray");                
                freqSetMode = EnumFrequencySetMode.ByWavelen;
            }            
            else if (configData.IsPresent("ChanArray"))
            {
                Arrfreq = configData.ReadDoubleArray("ChanArray");                
                freqSetMode = EnumFrequencySetMode.ByChan;
            }
            
            if (Arrfreq == null || Arrfreq.Length < 1)
                engine.ErrorInModule("No frequency information available , test can't go on ");
            #endregion



            int waveformAverageFactor = configData.ReadSint32("WaveformAverageFactor");
            int dataPoint = configData.ReadSint32("WaveformDataPoints");
            double oscXscale_S = configData.ReadDouble("OscXscale_S");
            double[] arrOscYscale_V = configData.ReadDoubleArray("OscYscaleArray_V");

            string fileDirectory = configData.ReadString("BlobFileDirectory");

            string filePrefix_WaveformData = configData.ReadString("FilePrefix_WaveformData");            
            string filePrefix_PdCurrents = configData.ReadString("FilePrefix_PdCurrent");            
            string filePrefix_Coeff = configData.ReadString("FilePrefix_Coff");

            CoRxDCTestInstruments.InitialiseOSC(oscXscale_S, arrOscYscale_V, dataPoint,waveformAverageFactor,
                Inst_Tds3034.EnumAcquisitionStopCondition.SEQ,Inst_Tds3034.EnumBandWidth.ONEF, 
                Inst_Tds3034.EnumCouplingMode.DC, Inst_Tds3034.EnumImpedance.MEG);

            double VPdTap_V = 0;
            if (CoRxDCTestInstruments.IsBiasPDTap)   // Not all code get tap PD
            {
                double IPdTap_Comp_A = configData.ReadDouble("PDTapCompCurrent_A");
                VPdTap_V = configData.ReadDouble("PDTapBias_V");
                CoRxDCTestInstruments.PdTapSource.InitSourceVMeasureI_MeasurementAccuracy(
                    IPdTap_Comp_A, IPdTap_Comp_A, false, 0, 1, 1, true);
                CoRxDCTestInstruments.PdTapSource.VoltageRange_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.VoltageSetPoint_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(100);
                

            }
            CoRxDCTestInstruments.SetupPdSourceTriggerLink(masterSource, Vpd_V, VPdTap_V, 1);
            double ItapPd_A = 0;
            double IauxPD_XR_A = 0;
            double IauxPD_XL_A = 0;
            //double IauxPd_YR_A = 0;
            //double IauxPd_YL_A = 0;

            double Icc_Tia_X_mA = 0;
            double Icc_Tia_Y_mA = 0;

            StrcCoRxPdRespData[] ArrPdsCurr_mA = new StrcCoRxPdRespData[Arrfreq.Length];
            Inst_Tds3034.ClsWaveformData[] arrWfData_XI = new Inst_Tds3034.ClsWaveformData[Arrfreq.Length];
            Inst_Tds3034.ClsWaveformData[] arrWfData_XQ = new Inst_Tds3034.ClsWaveformData[Arrfreq.Length];
            Inst_Tds3034.ClsWaveformData[] arrWfData_YI = new Inst_Tds3034.ClsWaveformData[Arrfreq.Length];
            Inst_Tds3034.ClsWaveformData[] arrWfData_YQ = new Inst_Tds3034.ClsWaveformData[Arrfreq.Length];
            CoRxChipPhaseAngleData[] arrPhaseAnalasis = new CoRxChipPhaseAngleData[ Arrfreq .Length];
            StrcSinusoidalCoefficientsLimit[] coeff_Limit = new StrcSinusoidalCoefficientsLimit[Arrfreq.Length];
            #region Test PA

            CoRxDCTestInstruments.SelectItla(CoRxDCTestInstruments.PhaseAngleLaserSource);

            DatumList rtnData = new DatumList();

            for (int ind_Freq = 0; ind_Freq < Arrfreq.Length; ind_Freq++ )
            {
                
                CoRxDCTestInstruments.OSC_XI.AcquisitionState = false;
                try
                {
                    if (freqSetMode == EnumFrequencySetMode.ByChan)
                        CoRxDCTestInstruments.PhaseAngleLaserSource.CurrentChan = (int)Arrfreq[ind_Freq];
                    else if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                        CoRxDCTestInstruments.PhaseAngleLaserSource.Frequency_GHz = Arrfreq[ind_Freq];
                    else
                        CoRxDCTestInstruments.PhaseAngleLaserSource.WaveLen_nM = Arrfreq[ind_Freq];
                }
                catch
                {
                    System.Threading.Thread.Sleep(100);

                }
                // for 100G device, the RF power is too high, limit the TIA amplitude out, so change the optical power input to device
                // add by bruce he 7-13-2012
                CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = false;
              //  double power = CoRxDCTestInstruments.PhaseAngleLaserSource.Power;
                CoRxDCTestInstruments.PhaseAngleLaserSource.Power_dBm = 7.5;
                CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = true;
                double minPower = CoRxDCTestInstruments.PhaseAngleLaserSource.MinPower_dBm;

                double power_dB = CoRxDCTestInstruments.PhaseAngleLaserSource.Power_dBm;
                engine.SendStatusMsg("itla power : " + power_dB);


                System.Threading.Thread.Sleep(100);

                #region Adjust polarization on signal path and read aux pd current 

                CoRxDCTestInstruments .setAllTiaSourceOutput (false );
                CoRxDCTestInstruments .SetAllPDSourceOutput( false );

                //CoRxDCTestInstruments.SetSource2AuxPD(true);
                //CoRxDCTestInstruments.InitAllAuxPDSourcesAsVoltSource(VauxPD_X_V, VauxPD_Y_V, IauxPD_Compliance_X_A, IauxPD_Compliance_Y_A);
                //CoRxDCTestInstruments.SetAllAuxPdBias(VauxPD_X_V, VauxPD_Y_V, IauxPD_Compliance_X_A, IauxPD_Compliance_Y_A);
                //CoRxDCTestInstruments .SetAllAuxPdSourceOutput (true );
                System.Threading.Thread.Sleep(100);

                bool disableSOPC = configData.ReadBool("IsDisablePolarizationAdjust");
                if (!disableSOPC)
                {
                    engine.SendToGui(new DatumString("", string.Format("Testing  {0} , adjusting Polarization controller ", Arrfreq[ind_Freq])));
                    CoRxDCTestInstruments.SigPathPolAdjustInstrs.GetOptimisePolarization();
                    disableSOPC = true;
                }

                // Record aux pd current after adjust polarisation 
                engine.SendToGui(new DatumString("", string.Format("Testing  {0} , Reading Pd Currents ", Arrfreq[ind_Freq])));
                System .Threading .Thread .Sleep (10000);
                //IauxPD_XR_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XR.CurrentActual_amp);
                //IauxPD_XL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XL.CurrentActual_amp);
                //IauxPd_YR_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_YR.CurrentActual_amp);
                //IauxPd_YL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_YL.CurrentActual_amp);

                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    ItapPd_A = Math.Abs(CoRxDCTestInstruments.PdTapSource.CurrentActual_amp);

                }

                CoRxDCTestInstruments.SetAllAuxPdSourceOutput(false);
                CoRxDCTestInstruments.SetSource2AuxPD(false);
                #endregion

                #region power up pds and turn on itla to test pa
                
                CoRxDCTestInstruments.InitAllPdSourcesAsVoltSource(Vpd_V, Ipd_Compliance_A);
                CoRxDCTestInstruments.SetTIABias(Vcc_V, Icc_Compliance_A);
                CoRxDCTestInstruments.setAllTiaSourceOutput(true);
                CoRxDCTestInstruments.SetAllPdBias(Vpd_V, Ipd_Compliance_A);
                CoRxDCTestInstruments.SetAllPDSourceOutput(true);
                if (is6464)
                {
                    CoRxDCTestInstruments.ResetTIA();
                    System.Threading.Thread.Sleep(300);
                    double Icc_X_A = CoRxDCTestInstruments.TiaSource_X.CurrentActual_amp;
                    CoRxDCTestInstruments.SetupTIA();
                    System.Threading.Thread.Sleep(500);

                }
                CoRxDCTestInstruments.SetAllControlLevel(V_MC_V, Icomp_MC_A, V_GC_V, Icomp_GC_A,
                    V_OC_V, Icomp_OC_A, V_OA_V, Icomp_OA_A, I_ThermPhase_A, Vcomp_ThermPhase_V);
                System.Threading.Thread.Sleep(1000);


                CoRxDCTestInstruments.TriggerPdSourceChain();
                if (isPdSourceUseTrigger)
                {
                    bool status = masterSource.WaitForSweepToFinish();
                    if (!status)
                    {
                        throw new InstrumentException("Error waiting for sweep");
                    }
                }
                ArrPdsCurr_mA[ind_Freq] = CoRxDCTestInstruments.GetPdRespCur(isPdSourceUseTrigger);
                if (SavePDCurrents)
                {
                    rtnData.AddDouble(string.Format("XIP_CURRENT_WL{0}", ind_Freq), ArrPdsCurr_mA[ind_Freq].Ipd_XI1_mA);
                    rtnData.AddDouble(string.Format("YIP_CURRENT_WL{0}", ind_Freq), ArrPdsCurr_mA[ind_Freq].Ipd_YI1_mA);
                }
                CoRxDCTestInstruments.ResetPdSourceTrigger();

                Icc_Tia_X_mA = CoRxDCTestInstruments.TiaSource_X.CurrentActual_amp * 1000;  // TO mA
                Icc_Tia_Y_mA = CoRxDCTestInstruments.TiaSource_Y.CurrentActual_amp * 1000;  // TO mA

                #endregion

                engine.SendToGui(new DatumString("", string.Format("Testing  {0} , Getting waveform data ", Arrfreq[ind_Freq])));
                CoRxDCTestInstruments.StartWaveformAcquire();
                try
                {
                    CoRxDCTestInstruments.OSC_XI.WaitForAcquisitionToFinish();
                }
                catch
                {
                    //engine.ErrorInModule("Can't get waveform data from oscilascope, could you please check if the device work properly?");
                }
                CoRxDCTestInstruments.OSC_XI.AcquisitionState = false;

                // if equals, X & Y Will use deffirent oas frame, so they will be wait for Y chip completes
                if (CoRxDCTestInstruments.OSC_XI.Slot == CoRxDCTestInstruments.OSC_YI.Slot ||
                    CoRxDCTestInstruments.OSC_XI.Slot == CoRxDCTestInstruments.OSC_YQ.Slot)
                {
                    CoRxDCTestInstruments.OSC_YI.WaitForAcquisitionToFinish();
                    CoRxDCTestInstruments.OSC_YI.AcquisitionState = false;
                }

                arrWfData_XI[ind_Freq] = CoRxDCTestInstruments.OSC_XI.GetWaveform();
                arrWfData_XQ[ind_Freq] = CoRxDCTestInstruments.OSC_XQ.GetWaveform();
                arrWfData_YI[ind_Freq] = CoRxDCTestInstruments.OSC_YI.GetWaveform();
                arrWfData_YQ[ind_Freq] = CoRxDCTestInstruments.OSC_YQ.GetWaveform();

                if (!CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate)
                {
                    coeff_Limit[ind_Freq] = CoRxDCTestInstruments.GetWavefromParameter();

                    arrPhaseAnalasis[ind_Freq] = new CoRxChipPhaseAngleData(arrWfData_XI[ind_Freq],
                        arrWfData_XQ[ind_Freq], arrWfData_YI[ind_Freq], arrWfData_YQ[ind_Freq], coeff_Limit[ind_Freq]);
                }
                else
                {
                    arrPhaseAnalasis[ind_Freq] = new CoRxChipPhaseAngleData(arrWfData_XI[ind_Freq],
                        arrWfData_XQ[ind_Freq], arrWfData_YI[ind_Freq], arrWfData_YQ[ind_Freq]);
                }
                
                rtnData.AddDouble("Phase_XI_" + ind_Freq.ToString() , arrPhaseAnalasis[ind_Freq].Phase_XI_Deg);
                rtnData.AddDouble("Phase_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].Phase_XQ_Deg);
                rtnData.AddDouble("Phase_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].Phase_YI_Deg);
                rtnData.AddDouble("Phase_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].Phase_YQ_Deg);

                double deltaPhase_X = CoRxChipPhaseAngleData.MaxPhase_Deg;
                double deltaPhase_Y = CoRxChipPhaseAngleData.MaxPhase_Deg;

                if (arrPhaseAnalasis[ind_Freq].Phase_XI_Deg != CoRxChipPhaseAngleData.MaxPhase_Deg ||
                    arrPhaseAnalasis[ind_Freq].Phase_XQ_Deg != CoRxChipPhaseAngleData.MaxPhase_Deg)
                {
                    deltaPhase_X = Math.Abs(arrPhaseAnalasis[ind_Freq].Phase_XI_Deg - arrPhaseAnalasis[ind_Freq].Phase_XQ_Deg);
                    if (Math.Abs(deltaPhase_X) > 180)   // clamp phase offset within +/- 180 deg
                        deltaPhase_X += (Math.Sign(deltaPhase_X) * (-1) * 360);
                }

                rtnData.AddDouble("DeltaPhase_X_" + ind_Freq.ToString(), deltaPhase_X);

                if (arrPhaseAnalasis[ind_Freq].Phase_YI_Deg != CoRxChipPhaseAngleData.MaxPhase_Deg ||
                    arrPhaseAnalasis[ind_Freq].Phase_YQ_Deg != CoRxChipPhaseAngleData.MaxPhase_Deg)
                {
                    deltaPhase_Y = Math.Abs(arrPhaseAnalasis[ind_Freq].Phase_YI_Deg - arrPhaseAnalasis[ind_Freq].Phase_YQ_Deg);
                    if (Math.Abs(deltaPhase_Y) > 180)   // clamp phase offset within +/- 180 deg
                        deltaPhase_Y += (Math.Sign(deltaPhase_Y) * (-1) * 360);
                }

                rtnData.AddDouble("DelataPhase_Y_" + ind_Freq.ToString(), deltaPhase_Y);


                rtnData.AddDouble("Offset_XI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XI.Coeffs[0]);
                rtnData.AddDouble("Amplitude_XI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XI.Coeffs[1]);
                rtnData.AddDouble("RadiusPhase_XI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XI.Coeffs[3]);
                rtnData.AddDouble("Period_XI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XI.Coeffs[4]);
                rtnData.AddDouble("FitError_XI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XI.MeanSquaredError);
                rtnData.AddDouble("FIT_CORRE_XI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].FitedCorelation_XI);

                rtnData.AddDouble("Offset_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XQ.Coeffs[0]);
                rtnData.AddDouble("Amplitude_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XQ.Coeffs[1]);
                rtnData.AddDouble("RadiusPhase_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XQ.Coeffs[3]);
                rtnData.AddDouble("Period_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XQ.Coeffs[4]);
                rtnData.AddDouble("FitError_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_XQ.MeanSquaredError);
                rtnData.AddDouble("FIT_CORRE_XQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].FitedCorelation_XQ);

                // error = delta(I-Q) - 90
                double delta_IQ = arrPhaseAnalasis[ind_Freq].Phase_XI_Deg - arrPhaseAnalasis[ind_Freq].Phase_XQ_Deg;
                if (Math.Abs(delta_IQ) > 180)   // clamp phase offset within +/- 180 deg
                {
                    delta_IQ += (Math.Sign(delta_IQ) * (-1) * 360);
                }
                // if the delta is negative, plus 90, or ,minus 90
                double delta_IQ_Err = delta_IQ + (Math.Sign(delta_IQ) * (-1) * 90);
                
                rtnData.AddDouble("PhaseError_X_Deg_" + ind_Freq.ToString(), delta_IQ_Err);

                rtnData.AddDouble("Offset_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YI.Coeffs[0]);
                rtnData.AddDouble("Amplitude_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YI.Coeffs[1]);
                rtnData.AddDouble("RadiusPhase_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YI.Coeffs[3]);
                rtnData.AddDouble("Period_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YI.Coeffs[4]);
                rtnData.AddDouble("FitError_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YI.MeanSquaredError);
                rtnData.AddDouble("FIT_CORRE_YI_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].FitedCorelation_YI);


                rtnData.AddDouble("Offset_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YQ.Coeffs[0]);
                rtnData.AddDouble("Amplitude_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YQ.Coeffs[1]);
                rtnData.AddDouble("RadiusPhase_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YQ.Coeffs[3]);
                rtnData.AddDouble("Period_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YQ.Coeffs[4]);
                rtnData.AddDouble("FitError_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].SinFit_YQ.MeanSquaredError);
                rtnData.AddDouble("FIT_CORRE_YQ_" + ind_Freq.ToString(), arrPhaseAnalasis[ind_Freq].FitedCorelation_YQ);

                delta_IQ = arrPhaseAnalasis[ind_Freq].Phase_YI_Deg - arrPhaseAnalasis[ind_Freq].Phase_YQ_Deg;
                if (Math.Abs(delta_IQ) > 180)  // clamp phase offset within +/- 180 deg
                {
                    delta_IQ += (Math.Sign(delta_IQ) * (-1) * 360);
                }
                // if the delta is negative, plus 90, or ,minus 90
                delta_IQ_Err = delta_IQ + (Math.Sign(delta_IQ) * (-1) * 90);
                
                rtnData.AddDouble("PhaseError_Y_Deg_" + ind_Freq.ToString(),delta_IQ_Err);

                rtnData.AddDouble("IauxPD_XR_A_" + ind_Freq.ToString(), IauxPD_XR_A);
                rtnData.AddDouble("IauxPD_XL_A_" + ind_Freq.ToString(), IauxPD_XL_A);
                rtnData.AddDouble("IauxPD_YR_A_" + ind_Freq.ToString(), IauxPD_XR_A);
                rtnData.AddDouble("IauxPD_YL_A_" + ind_Freq.ToString(), IauxPD_XL_A);
                rtnData.AddDouble("ItapPd_A_" + ind_Freq.ToString(), ItapPd_A);

                rtnData.AddDouble("IccTia_X_A_" + ind_Freq.ToString(), Icc_Tia_X_mA);
                rtnData.AddDouble("IccTia_Y_A_" + ind_Freq.ToString(), Icc_Tia_Y_mA);

            }
            #endregion

            CoRxDCTestInstruments.PhaseAngleLaserSource.OutputEnable = false;
            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);
            if (CoRxDCTestInstruments.IsBiasPDTap)
                CoRxDCTestInstruments.PdTapSource.OutputEnabled = false;

            string fileName= "";
            try
            {
                fileName = WriteWaveformData(arrPhaseAnalasis, filePrefix_WaveformData, fileDirectory); ;
                rtnData.AddFileLink("PhaseDataFile", fileName);
            }
            catch  // ignore all exception by writing file 
            { }

            try
            {
                fileName = WritePAAnalysisData(coeff_Limit , arrPhaseAnalasis, filePrefix_Coeff, fileDirectory);
                rtnData.AddFileLink("PhaseAnalysisCoeffFile", fileName);
            }
            catch  // ignore all exception by writing file 
            { }

            try
            {
                fileName = WritePdCurrent(ArrPdsCurr_mA, filePrefix_PdCurrents, fileDirectory);
                rtnData.AddFileLink("PdDataFile", fileName);
            }
            catch  // ignore all exception by writing file 
            {}            

            return rtnData;
        }

        /// <summary>
        /// Write phase angle fit coefficiency into file
        /// </summary>
        /// <param name="arrPAData"> phase angle fit analysis data array</param>
        /// <param name="filePrefix"></param>
        /// <param name="fileDirection"></param>
        /// <returns> Full path for plot file </returns>
        string  WritePAAnalysisData(StrcSinusoidalCoefficientsLimit[] arrWaveformParam,CoRxChipPhaseAngleData[] arrPAData, string filePrefix, string fileDirection)
        {
            if (arrPAData == null || arrPAData.Length < 1)
                return "";

            List<string> strList = new List<string>();

            if (CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate)
            { 
                strList.Add("Waveform ID, Amp_V, Freq_Hz,Phase_Deg,Offset_V,MeanSquareError,FitedColrelationm,");
            }
            else 
                strList.Add("Waveform ID, Amp_V, Freq_Hz,Phase_Deg,Offset_V,MeanSquareError,FitedColrelationm," +
                    " , org offset , org amp , org rad phase, org period,"  +", cal rad phase, cal period,");

            StringBuilder strBuilder = new StringBuilder();

            for (int idx = 0; idx < arrPAData.Length; idx++)
            {
                if (arrPAData[idx] != null)
                {
                    // Coeffs array : offset, amplitude, power, phase, period, 

                    // XI DATA 
                    strBuilder = new StringBuilder();
                    strBuilder.Append("XI_WL");
                    strBuilder.Append(idx + 1);
                    strBuilder.Append(",");

                    if (arrPAData[idx].SinFit_XI.Coeffs != null && arrPAData[idx].SinFit_XI.Coeffs != null)
                    {
                        strBuilder.Append(arrPAData[idx].SinFit_XI.Coeffs[1]);
                        strBuilder.Append(",");
                        strBuilder.Append(1 / arrPAData[idx].SinFit_XI.Coeffs[4]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].Phase_XI_Deg);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_XI.Coeffs[0]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_XI.MeanSquaredError);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].FitedCorelation_XI);
                        strBuilder.Append(",");

                        if (!CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate)
                        {
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XI[0]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XI[1]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XI[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XI[4]);
                            strBuilder.Append(",");

                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_XI.Coeffs[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_XI.Coeffs[4]);
                            strBuilder.Append(",");
                        }

                    }
                    strList.Add(strBuilder.ToString());

                    // XQ DATA 
                    strBuilder = new StringBuilder();
                    strBuilder.Append("XQ_WL");
                    strBuilder.Append(idx + 1);
                    strBuilder.Append(",");

                    if (arrPAData[idx].SinFit_XQ.Coeffs != null && arrPAData[idx].SinFit_XQ.Coeffs != null)
                    {
                        strBuilder.Append(arrPAData[idx].SinFit_XQ.Coeffs[1]);
                        strBuilder.Append(",");
                        strBuilder.Append(1 / arrPAData[idx].SinFit_XQ.Coeffs[4]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].Phase_XQ_Deg);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_XQ.Coeffs[0]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_XQ.MeanSquaredError);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].FitedCorelation_XQ);
                        strBuilder.Append(",");

                        if (!CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate)
                        {
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XQ[0]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XQ[1]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XQ[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_XQ[4]);
                            strBuilder.Append(",");

                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_XQ.Coeffs[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_XQ.Coeffs[4]);
                            strBuilder.Append(",");
                        }
                    }
                    strList.Add(strBuilder.ToString());

                    // YI DATA 
                    strBuilder = new StringBuilder();
                    strBuilder.Append("YI_WL");
                    strBuilder.Append(idx + 1);
                    strBuilder.Append(",");

                    if (arrPAData[idx].SinFit_YI.Coeffs != null && arrPAData[idx].SinFit_YI.Coeffs != null)
                    {
                        strBuilder.Append(arrPAData[idx].SinFit_YI.Coeffs[1]);
                        strBuilder.Append(",");
                        strBuilder.Append(1 / arrPAData[idx].SinFit_YI.Coeffs[4]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].Phase_YI_Deg);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_YI.Coeffs[0]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_YI.MeanSquaredError);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].FitedCorelation_YI);
                        strBuilder.Append(",");

                        if (!CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate)
                        {
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YI[0]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YI[1]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YI[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YI[4]);
                            strBuilder.Append(",");

                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_YI.Coeffs[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_YI.Coeffs[4]);
                            strBuilder.Append(",");
                        }
                    }
                    strList.Add(strBuilder.ToString());

                    // YQ DATA 
                    strBuilder = new StringBuilder();
                    strBuilder.Append("YQ_WL");
                    strBuilder.Append(idx + 1);
                    strBuilder.Append(",");

                    if (arrPAData[idx].SinFit_YQ.Coeffs != null && arrPAData[idx].SinFit_YQ.Coeffs != null)
                    {
                        strBuilder.Append(arrPAData[idx].SinFit_YQ.Coeffs[1]);
                        strBuilder.Append(",");
                        strBuilder.Append(1 / arrPAData[idx].SinFit_YQ.Coeffs[4]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].Phase_YQ_Deg);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_YQ.Coeffs[0]);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].SinFit_YQ.MeanSquaredError);
                        strBuilder.Append(",");
                        strBuilder.Append(arrPAData[idx].FitedCorelation_YQ);
                        strBuilder.Append(",");

                        if (!CoRxDCTestCommonData.CoRxChipPhaseAngleData.IsFitWithTemplate)
                        {
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YQ[0]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YQ[1]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YQ[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrWaveformParam[idx].Coeff_YQ[4]);
                            strBuilder.Append(",");

                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_YQ.Coeffs[3]);
                            strBuilder.Append(",");
                            strBuilder.Append(arrPAData[idx].SinFit_YQ.Coeffs[4]);
                            strBuilder.Append(",");
                        }
                    }
                    strList.Add(strBuilder.ToString());
                }                
            }

            string fileName = Util_GenerateFileName.GenWithTimestamp(fileDirection, filePrefix, "", "csv");

            using (CsvWriter wrier = new CsvWriter())
            {
                wrier.WriteFile(fileName, strList);
            }

            return fileName;


        }

        /// <summary>
        /// write PD current to blob file
        /// </summary>
        /// <param name="arrPdCurrent"></param>
        /// <param name="filePrefix"></param>
        /// <param name="fileDirection"> </param>
        /// <returns></returns>
        string WritePdCurrent(StrcCoRxPdRespData[] arrPdCurrent, string filePrefix, string fileDirection)
        {

            if (arrPdCurrent == null || arrPdCurrent.Length < 1)
                return "";

            List<string> lstPdCurr = new List<string>();

             StringBuilder strHeader = new StringBuilder();
            strHeader.Append("Wavelength ID,");
            
            strHeader.Append("PD XI1 (mA),");
            strHeader.Append("PD XI2 (mA),");
            strHeader.Append("PD XQ1 (mA),");
            strHeader.Append("PD XQ2 (mA),");
            strHeader.Append("PD YI1 (mA),");
            strHeader.Append("PD YI2 (mA),");
            strHeader.Append("PD YQ1 (mA),");
            strHeader.Append("PD YQ2 (mA),");
            lstPdCurr.Add(strHeader.ToString());


            for (int idx = 0; idx < arrPdCurrent.Length; idx++)
            {
                StringBuilder dataSb = new StringBuilder();
                dataSb.Append(idx);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_XI1_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_XI2_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_XQ1_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_XQ2_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_YI1_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_YI2_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_YQ1_mA);
                dataSb.Append(",");
                dataSb.Append(arrPdCurrent[idx].Ipd_YQ2_mA);
                dataSb.Append(",");

                lstPdCurr.Add(dataSb.ToString());
            }

            string fileName = Util_GenerateFileName.GenWithTimestamp(fileDirection, filePrefix, "", "csv");
            using (CsvWriter writer = new CsvWriter())
            {
                writer .WriteFile (fileName , lstPdCurr);
            }
            return fileName;
        }

        /// <summary>
        /// Write waveform data to plot file
        /// </summary>
        /// <param name="arrPAData"> phase angle fit analysis data array</param>
        /// <param name="filePrefix"></param>
        /// <param name="fileDirection"></param>
        /// <returns> Full path of plot file </returns>
        string WriteWaveformData(CoRxChipPhaseAngleData[] arrPAData, string filePrefix, string fileDirection)
        {
            if (  arrPAData == null || arrPAData .Length <1)
                return "";

            List<string> lstDataStr = new List<string>();
            
            // file header
            StringBuilder headerSb = new StringBuilder();
            for (int idx = 0; idx < arrPAData.Length; idx++)
            {
                headerSb.Append("X data (S) WL ");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb .Append ("XI Raw (V) WL" );
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append("XQ Raw (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append("YI Raw (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append("YQ Raw (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");

                headerSb.Append("XI Fited (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append("XQ Fited (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append("YI Fited (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append("YQ Fited (V) WL");
                headerSb.Append(idx + 1);
                headerSb.Append(",");
                headerSb.Append(",");

            }
            lstDataStr.Add(headerSb.ToString());

            // File contents
            for (int ind_Point = 0; ind_Point < arrPAData[0].WfData_XI.Xdata.Length; ind_Point++)
            {
                StringBuilder dataSb = new StringBuilder();

                for (int idx_Wl = 0; idx_Wl < arrPAData.Length; idx_Wl++)
                {
                    dataSb.Append(arrPAData[idx_Wl].WfData_XI.Xdata[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].WfData_XI.Ydata[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].WfData_XQ.Ydata[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].WfData_YI.Ydata[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].WfData_YQ.Ydata[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].SinFit_XI.FittedYArray[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].SinFit_XQ.FittedYArray[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].SinFit_YI.FittedYArray[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(arrPAData[idx_Wl].SinFit_YQ.FittedYArray[ind_Point]);
                    dataSb.Append(",");
                    dataSb.Append(",");

                }
                lstDataStr.Add(dataSb.ToString());
            }

            string fileName = Util_GenerateFileName .GenWithTimestamp ( fileDirection , filePrefix ,"","csv");
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(fileName, lstDataStr);
            }
            return fileName;
        }

        
        /// <summary>
        /// Module GUI
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(TM_CoRxPhaseAngleGui)); }
        }

        #endregion
    }
}
