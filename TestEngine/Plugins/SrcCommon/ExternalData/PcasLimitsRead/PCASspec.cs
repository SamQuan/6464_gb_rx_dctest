// [Copyright]
//
// Bookham Modular Test Engine
// External Data
//
// Library\PcasLimitsRead\PcasSpec.cs
// 
// Author: Mark Fullalove
// Design: External Data Library Design Document.

using System;
using System.Collections;



namespace Bookham.TestLibrary.ExternalData
{
	/// <summary>
	/// A collection of limits
	/// </summary>
	internal class PCASspec : System.Collections.CollectionBase
	{

		/// <summary>
		/// Create an empty, named PCASspec object.
		/// </summary>
		/// <param name="name">Spec name</param>
		public PCASspec( string name )
		{
			// Can't remove this. It is used in GetLimit()
			this.name = name;
		}

		// Collection Implementation
		/// <summary>
		/// Add a PCASlimit to the collection.
		/// </summary>
		/// <param name="limit">Limit to add</param>
		public void Add( PCASlimit limit )
		{
			List.Add( limit );
		}

		internal string name;
	}
}
