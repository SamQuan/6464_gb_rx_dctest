// [Copyright]
//
// Bookham TTA
// Bookham.TestSolution.Instruments
//
// Instr_TTA.cs
// 
// Author: joseph.olajubu, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.BlackBoxes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;


namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for the TTA. Assumes T-CMZ Goldbox 
    /// Contains an MZ Chip of Technology G4 T1 or later. 
    /// This affects the pin assignments on the GoldBox. 
    /// It is assumed that MZ Left Bias is on Pin 35 and 
    /// MZ Right Bias is on Pin 34.
    /// </summary>
    public class Instr_TTA : Instr_ITLA
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_TTA(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit, true)
        {
            // Setup expected valid hardware variants 
            ValidHardwareData.Add(
                            "TTA",
                            new InstrumentDataRecord("Unknown", "Unknown", "Unknown"));
        }
        #endregion

        #region IBlackBoxIdentity  Overrides
        /// <summary>
        /// Set the Laser Technology String. For TTA, this is the MZ Technology string.
        /// </summary>
        /// <param name="mzTechnology">MZ Technology string : Usually one of 
        /// G3 T1, P3 T2, G4 T1 or G4 T2
        /// </param>
        public override void SetLaserTechnology(string mzTechnology)
        {
            this.ixoliteChassis.SetOifRegisterAsString(0xE3, mzTechnology);
        }

        /// <summary>
        /// Get the Laser Technology String. For TTA, this is the MZ Technology string.
        /// </summary>
        /// <returns>The MZ Technology String</returns>
        public override string GetLaserTechnology()
        {
            return this.ixoliteChassis.GetOifRegisterAsString(0xE3);
        }
        #endregion

        #region TTA Specific Commands
        /// <summary>
        /// Get MZ Right Imbalance Electrode Control DAC Value
        /// </summary>
        /// <returns>DAC value</returns>
        public int GetMzRightImbalDac()
        {
            return this.ixoliteChassis.GetOifRegisterAsUint(0xD8);
        }

        /// <summary>
        /// Set MZ Right Imbalance Electrode Control DAC Value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetMzRightImbalDac(int dac)
        {
            this.ixoliteChassis.SetOifRegisterAsUint(0xD8, (short)dac);
        }


        /// <summary>
        /// Get MZ Left Imbalance Electrode Control DAC Value
        /// </summary>
        /// <returns>DAC value</returns>
        public int GetMzLeftImbalDac()
        {
            return this.ixoliteChassis.GetOifRegisterAsUint(0xD7);
        }

        /// <summary>
        /// Set MZ Left Imbalance Electrode Control DAC Value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetMzLeftImbalDac(int dac)
        {
            this.ixoliteChassis.SetOifRegisterAsUint(0xD7, (short)dac);
        }


        /// <summary>
        /// Get MZ Left Bias Voltage Value for use at Modulator Peak point in mV (Pin35)
        /// </summary>
        /// <returns>DAC value</returns>
        public short GetMzLeftBiasPeak_mV()
        {
            return this.ixoliteChassis.GetOifRegisterAsInt(0xB8);
        }

        /// <summary>
        /// Set MZ Left Bias Voltage Value for use at Modulator Peak point in mV (Pin35)
        /// </summary>
        /// <param name="bias">DAC Value</param>
        public void SetMzLeftBiasPeak_mV(short bias)
        {
            this.ixoliteChassis.SetOifRegisterAsInt(0xB8, bias);
        }

        /// <summary>
        /// Get MZ Right Bias Voltage Value for use at Modulator Peak point in mV (Pin34)
        /// </summary>
        /// <returns>DAC value</returns>
        public short GetMzRightBiasPeak_mV()
        {
            return this.ixoliteChassis.GetOifRegisterAsInt(0xB7);
        }

        /// <summary>
        /// Set MZ Right Bias Voltage Value for use at Modulator Peak point in mV (Pin34)
        /// </summary>
        /// <param name="bias">DAC Value</param>
        public void SetMzRightBiasPeak_mV(short bias)
        {
            this.ixoliteChassis.SetOifRegisterAsInt(0xB7, bias);
        }


        /// <summary>
        /// Get MZ Left Bias Voltage Value for use at Modulator Quadrature point in mV (Pin35)
        /// </summary>
        /// <returns>DAC value</returns>
        public short GetMzLeftBiasQuad_mV()
        {
            return this.ixoliteChassis.GetOifRegisterAsInt(0xE5);
        }

        /// <summary>
        /// Set MZ Left Bias Voltage Value for use at Modulator Quadrature point in mV (Pin35)
        /// </summary>
        /// <param name="bias">DAC Value</param>
        public void SetMzLeftBiasQuad_mV(short bias)
        {
            this.ixoliteChassis.SetOifRegisterAsInt(0xE5, bias);
        }

        /// <summary>
        /// Get MZ Right Bias Voltage Value for use at Modulator Quadrature point in mV (Pin34)
        /// </summary>
        /// <returns>DAC value</returns>
        public short GetMzRightBiasQuad_mV()
        {
            return this.ixoliteChassis.GetOifRegisterAsInt(0xE4);
        }

        /// <summary>
        /// Set MZ Right Bias Voltage Value for use at Modulator Quadrature point in mV (Pin34)
        /// </summary>
        /// <param name="bias">DAC Value</param>
        public void SetMzRightBiasQuad_mV(short bias)
        {
            this.ixoliteChassis.SetOifRegisterAsInt(0xE4, bias);
        }

        /// <summary>
        /// Get MZ Modulation Voltage DAC Value
        /// </summary>
        /// <returns>DAC value</returns>
        public short GetMzModulationDAC()
        {
            return this.ixoliteChassis.GetOifRegisterAsInt(0xE6);
        }

        /// <summary>
        /// Set MZ Modulation Voltage DAC Value
        /// </summary>
        /// <param name="dac">DAC Value</param>
        public void SetMzModulationDAC(short dac)
        {
            this.ixoliteChassis.SetOifRegisterAsInt(0xE6, dac);
        }

        /// <summary>
        /// Write Extinction Ratio Data
        /// </summary>
        /// <param name="dac">Extinction Ratio Data Array (4 numbers)</param>
        public void SetExtinctionRatioData(short[] extinctionRatioData)
        {
            if (extinctionRatioData.Length != 4)
            {
                throw new InstrumentException("Invalid number of Extinction Ratio Values " + extinctionRatioData.Length);
            }

            //Set the number of bytes to be wrtten
            this.ixoliteChassis.SetOifRegisterAsUInt(0x87, 8);

            //Now write the values.
            foreach (short val in extinctionRatioData)
            {
                this.ixoliteChassis.SetOifRegisterAsUInt(0x0B, (ushort)val);
            }
        }

        /// <summary>
        /// Read Extinction Ratio Data
        /// </summary>
        /// <param name="dac">Extinction Ratio Data Array (4 numbers)</param>
        public short[] GetExtinctionRatioData()
        {
            short[] vals = new short[4];
            int numOfVals = this.ixoliteChassis.GetOifRegisterAsUint(0x87);

            if (numOfVals != 8)
            {
                throw new InstrumentException("Invalid number of DC ER Values from device: " + numOfVals);
            }

            for (int i = 0; i < 4; i++)
            {
                vals[i] = (short)this.ixoliteChassis.GetOifRegisterAsUint(0x0B);
            }

            return vals;
        }

        #endregion
    }
}
