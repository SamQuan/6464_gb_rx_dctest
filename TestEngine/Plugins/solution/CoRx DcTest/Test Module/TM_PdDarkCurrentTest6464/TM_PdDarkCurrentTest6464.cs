// [Copyright]
//
// Bookham [Coherence Rx DC Test ]
// Bookham.TestSolution.TestModules
//
// TM_PdDarkCurrentTest6464
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module to read pd lekeage currents
    /// </summary>
    public class TM_PdDarkCurrentTest6464 : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// Read pd lekeage currents
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            double Vpd_V = configData.ReadDouble("PDBias_V");
            double Ipd_Compliance_A = configData.ReadDouble("PDCompCurrent_A");
            double Vcc_V = configData.ReadDouble("TiaBias_V");
            double Icc_Compliance_A = configData.ReadDouble("TIACompCurrent_A");

            #region Get Control inputs settings
            double V_MC_V;
            double Icomp_MC_A;
            double V_GC_V;
            double Icomp_GC_A;
            double V_OC_V;
            double Icomp_OC_A;
            double V_OA_V;
            double Icomp_OA_A;
            double I_ThermPhase_A;
            double Vcomp_ThermPhase_V;

            double SignalSourcePower = configData.ReadDouble("SignalSourcePower");
            double LocSourcePower = configData.ReadDouble("LocSourcePower");
            
           
            if (configData.IsPresent("MCBias_V"))
            {
                V_MC_V = configData.ReadDouble("MCBias_V");
            }
            else
            {
                V_MC_V = double.NaN;
            }
            if (configData.IsPresent("MCCurComp_A"))
            {
                Icomp_MC_A = configData.ReadDouble("MCCurComp_A");
            }
            else
            {
                Icomp_MC_A = 0.01;
            }

            if (configData.IsPresent("GCBias_V"))
            {
                V_GC_V = configData.ReadDouble("GCBias_V");
            }
            else
            {
                V_GC_V = 2.3;
            }
            if (configData.IsPresent("GCCurComp_A"))
            {
                Icomp_GC_A = configData.ReadDouble("GCCurComp_A");
            }
            else
            {
                Icomp_GC_A = 0.01;
            }

            if (configData.IsPresent("OCBias_V"))
            {
                V_OC_V = configData.ReadDouble("OCBias_V");
            }
            else
            {
                V_OC_V = double.NaN;
            }
            if (configData.IsPresent("OCCurComp_A"))
            {
                Icomp_OC_A = configData.ReadDouble("OCCurComp_A");
            }
            else
            {
                Icomp_OC_A = 0.01;
            }

            if (configData.IsPresent("OABias_V"))
            {
                V_OA_V = configData.ReadDouble("OABias_V");
            }
            else
            {
                V_OA_V = double.NaN;
            }
            if (configData.IsPresent("OACurComp_A"))
            {
                Icomp_OA_A = configData.ReadDouble("OACurComp_A");
            }
            else
            {
                Icomp_OA_A = 0.01;
            }

            if (configData.IsPresent("ThermPhaseBias_A"))
            {
                I_ThermPhase_A = configData.ReadDouble("ThermPhaseBias_A");
            }
            else
            {
                I_ThermPhase_A = double.NaN;
            }
            if (configData.IsPresent("ThermPhaseVoltComp_A"))
            {
                Vcomp_ThermPhase_V = configData.ReadDouble("ThermPhaseVoltComp_A");
            }
            else
            {
                Vcomp_ThermPhase_V = 1;
            }

            #endregion

            //set ITLA souce  gaindac = 0  to ensure there is no light 
            //signal
            CoRxDCTestInstruments.SelectItla(CoRxDCTestInstruments.SignalInputInstrs.LaserSource);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.unlock();
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.SetGainDAC(0);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.GetItla().SetFrontEvenDAC(0);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.GetItla().SetFrontOddDAC(0);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.GetItla().SetRearDAC(0);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.GetItla().SetPhaseDAC(0);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.GetItla().SetSOADAC(0);
            
            //local       
            CoRxDCTestInstruments.SelectItla(CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.unlock();
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.SetGainDAC(0);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.GetItla().SetFrontEvenDAC(0);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.GetItla().SetFrontOddDAC(0);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.GetItla().SetRearDAC(0);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.GetItla().SetPhaseDAC(0);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.GetItla().SetSOADAC(0);
          
                     
                    
            // Power on by Seq in Vpd-> Vcc-> Vmc, Tia in AGC Mode
            CoRxDCTestInstruments.SetSource2AuxPD(false);
            CoRxDCTestInstruments.InitAllPdSourcesAsVoltSource(Vpd_V, Ipd_Compliance_A);

            CoRxDCTestInstruments.SetAllPdBias(Vpd_V, Ipd_Compliance_A);
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);
            CoRxDCTestInstruments.SetTIABias(Vcc_V, Icc_Compliance_A);
            CoRxDCTestInstruments.setAllTiaSourceOutput(true);
            System.Threading.Thread.Sleep(300);
            CoRxDCTestInstruments.SetupTIA();
            CoRxDCTestInstruments.ResetTIA();
            System.Threading.Thread.Sleep(300);
            double Icc_X_A = CoRxDCTestInstruments.TiaSource_X.CurrentActual_amp;
            CoRxDCTestInstruments.SetupTIA();
            System.Threading.Thread.Sleep(500);
 
            CoRxDCTestInstruments.SetAllControlLevel(V_MC_V, Icomp_MC_A, V_GC_V, Icomp_GC_A,
                V_OC_V, Icomp_OC_A, V_OA_V, Icomp_OA_A, I_ThermPhase_A, Vcomp_ThermPhase_V);

           
            
            double Icc_Y_A = CoRxDCTestInstruments.TiaSource_X.CurrentActual_amp;

            // CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            CoRxDCTestInstruments.ResetTIA();
            double maxIpd_A =0;
            int delayTime = 1000;
            // Get each pd's leakage current
            CoRxDCTestInstruments.PdSource_XI1.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double Ipd_XI1_A = Math.Abs ( CoRxDCTestInstruments.PdSource_XI1.CurrentActual_amp);
            maxIpd_A = Math .Max ( maxIpd_A ,Ipd_XI1_A );
 
            CoRxDCTestInstruments.PdSource_XI1.OutputEnabled = false;
            CoRxDCTestInstruments.PdSource_XQ1.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double Ipd_XQ1_A = Math .Abs ( CoRxDCTestInstruments.PdSource_XQ1.CurrentActual_amp);
            maxIpd_A = Math .Max ( maxIpd_A , Ipd_XQ1_A );

            CoRxDCTestInstruments.PdSource_XQ1.OutputEnabled = false;
            CoRxDCTestInstruments.PdSource_YI1.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double Ipd_YI1_A = Math .Abs ( CoRxDCTestInstruments.PdSource_YI1.CurrentActual_amp);
            maxIpd_A = Math .Max ( maxIpd_A ,Ipd_YI1_A );

            CoRxDCTestInstruments.PdSource_YI1.OutputEnabled = false;
            CoRxDCTestInstruments.PdSource_YQ1.OutputEnabled = true;
            System.Threading.Thread.Sleep(delayTime);
            double Ipd_YQ1_A = Math .Abs ( CoRxDCTestInstruments.PdSource_YQ1.CurrentActual_amp);
            maxIpd_A = Math .Max ( maxIpd_A , Ipd_YQ1_A );
            
            // Get tap pd leakage current if it exist
            DatumList returnData = new DatumList();
            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                double IPdTap_Comp_A = configData.ReadDouble("PDTapCompCurrent_A");
                double VPdTap_V = configData.ReadDouble("PDTapBias_V");
                CoRxDCTestInstruments.PdTapSource.InitSourceVMeasureI_MeasurementAccuracy(
                    IPdTap_Comp_A, 0, false, 0, 1, 1, true);
                CoRxDCTestInstruments.PdTapSource.VoltageRange_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.CurrentComplianceSetPoint_Amp = IPdTap_Comp_A;
                CoRxDCTestInstruments.PdTapSource.SenseCurrent(IPdTap_Comp_A, 0);
                                
                CoRxDCTestInstruments.PdTapSource.VoltageSetPoint_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(delayTime);
                
                double IPdtap_A = CoRxDCTestInstruments.PdTapSource.CurrentActual_amp;
                returnData.AddDouble("IPdTap_nA", Math.Abs(IPdtap_A) * 1e+9);
                
            }
            //recover ITLA to original status
            CoRxDCTestInstruments.SelectItla(CoRxDCTestInstruments.SignalInputInstrs.LaserSource);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.GetItla().ModuleReset();
            //System.Threading.Thread.Sleep(15000);
            //local       
            CoRxDCTestInstruments.SelectItla(CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.GetItla().ModuleReset();
            System.Threading.Thread.Sleep(15000);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.CurrentChan = 1;
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.OutputEnable = false;
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.Power_dBm = LocSourcePower;

            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.CurrentChan = 1;
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.OutputEnable = false;
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource.Power_dBm = SignalSourcePower;
      
            // Mutiplex 1000000000 to translate current in nA as required
            returnData.AddDouble("Ipd_XI1_nA", Ipd_XI1_A * 1e+9);
            returnData.AddDouble("Ipd_XQ1_nA", Ipd_XQ1_A * 1e+9);
            returnData.AddDouble("Ipd_YI1_nA", Ipd_YI1_A * 1e+9);
            returnData.AddDouble("Ipd_YQ1_nA", Ipd_YQ1_A * 1e+9);
 
            returnData.AddDouble("Ipd_Max_nA", maxIpd_A * 1e+9);

            returnData.AddDouble("Icc_X_mA", Math .Abs (Icc_X_A) * 1000);
            returnData.AddDouble("Icc_Y_mA", Math .Abs (Icc_Y_A) * 1000);
            return returnData;
        }
        
        // User GUI
        public Type UserControl
        {
            get { return (typeof(TM_PdDarkCurrentTesttGui)); }
        }

        #endregion
    }
}
