using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// XFP Standard Wrapper functions.
    /// </summary>
    public interface IMSA_XFP
    {
        /// <summary>
        /// Get Module Identifier Byte (@ Address 0)
        /// </summary>
        /// <returns>The Identifier Byte</returns>
        byte GetModuleIdentifier();

        /// <summary>
        /// Get Signal Conditioner Control Bit (LSB @ Address 1)
        /// false = 0; true = 1; 
        /// </summary>
        /// <returns>Signal Conditioner Control Byte</returns>
        bool GetSignalConditionerControlBit();

        /// <summary>
        /// Set Signal Conditioner Control Bit (LSB @ Address 1)
        /// false = 0, true = 1
        /// </summary>
        bool SetSignalConditionerControlBit(bool bit);

        /// <summary>
        /// Get Temp High Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetTempHighAlarmThreshold();

        /// <summary>
        /// Set Temp High Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTempHighAlarmThreshold(int thresh);

        /// <summary>
        /// Get Temp High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        int GetTempHighWarningThreshold();

        /// <summary>
        /// Set Temp High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTempHighWarningThreshold(int thresh);


        /// <summary>
        /// Get Temp Low Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetTempLowAlarmThreshold();

        /// <summary>
        /// Set Temp Low Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTempLowAlarmThreshold(int thresh);

        /// <summary>
        /// Get Temp Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        int GetTempLowWarningThreshold();

        /// <summary>
        /// Set Temp Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTempLowWarningThreshold(int thresh);


        /// <summary>
        /// Get Bias High Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetBiasHighAlarmThreshold();

        /// <summary>
        /// Set Bias High Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetBiasHighAlarmThreshold(int thresh);

        /// <summary>
        /// Get Bias High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        int GetBiasHighWarningThreshold();

        /// <summary>
        /// Set Bias High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetBiasHighWarningThreshold(int thresh);


        /// <summary>
        /// Get Bias Low Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetBiasLowAlarmThreshold();

        /// <summary>
        /// Set Bias Low Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetBiasLowAlarmThreshold(int thresh);

        /// <summary>
        /// Get Bias Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        int GetBiasLowWarningThreshold();

        /// <summary>
        /// Set Bias Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetBiasLowWarningThreshold(int thresh);



        /// <summary>
        /// Get TxPower High Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetTxPowerHighAlarmThreshold();

        /// <summary>
        /// Set TxPower High Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTxPowerHighAlarmThreshold(int thresh);

        /// <summary>
        /// Get TxPower High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        int GetTxPowerHighWarningThreshold();

        /// <summary>
        /// Set TxPower High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTxPowerHighWarningThreshold(int thresh);


        /// <summary>
        /// Get TxPower Low Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetTxPowerLowAlarmThreshold();

        /// <summary>
        /// Set TxPower Low Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTxPowerLowAlarmThreshold(int thresh);

        /// <summary>
        /// Get TxPower Low Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetTxPowerLowWarningThreshold();

        /// <summary>
        /// Set TxPower Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetTxPowerLowWarningThreshold(int thresh);

        /// <summary>
        /// Get RxPower High Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetRxPowerHighAlarmThreshold();

        /// <summary>
        /// Set RxPower High Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetRxPowerHighAlarmThreshold(int thresh);

        /// <summary>
        /// Get RxPower High Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetRxPowerHighWarningThreshold();

        /// <summary>
        /// Set RxPower High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetRxPowerHighWarningThreshold(int thresh);


        /// <summary>
        /// Get RxPower Low Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetRxPowerLowAlarmThreshold();

        /// <summary>
        /// Set RxPower Low Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetRxPowerLowAlarmThreshold(int thresh);

        /// <summary>
        /// Get RxPower Low Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetRxPowerLowWarningThreshold();

        /// <summary>
        /// Set RxPower Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetRxPowerLowWarningThreshold(int thresh);

        /// <summary>
        /// Get Aux1 High Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux1HighAlarmThreshold();

        /// <summary>
        /// Set Aux1 High Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux1HighAlarmThreshold(int thresh);

        /// <summary>
        /// Get Aux1 High Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux1HighWarningThreshold();

        /// <summary>
        /// Set Aux1 High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux1HighWarningThreshold(int thresh);


        /// <summary>
        /// Get Aux1 Low Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux1LowAlarmThreshold();

        /// <summary>
        /// Set Aux1 Low Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux1LowAlarmThreshold(int thresh);

        /// <summary>
        /// Get Aux1 Low Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux1LowWarningThreshold();

        /// <summary>
        /// Set Aux1 Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux1LowWarningThreshold(int thresh);

        /// <summary>
        /// Get Aux2 High Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux2HighAlarmThreshold();

        /// <summary>
        /// Set Aux2 High Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux2HighAlarmThreshold(int thresh);

        /// <summary>
        /// Get Aux2 High Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux2HighWarningThreshold();

        /// <summary>
        /// Set Aux2 High Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux2HighWarningThreshold(int thresh);


        /// <summary>
        /// Get Aux2 Low Alarm Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux2LowAlarmThreshold();

        /// <summary>
        /// Set Aux2 Low Alarm Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux2LowAlarmThreshold(int thresh);

        /// <summary>
        /// Get Aux2 Low Warning Threshold
        /// </summary>
        /// <returns>The Threshold Value</returns>
        int GetAux2LowWarningThreshold();

        /// <summary>
        /// Set Aux2 Low Warning Threshold
        /// </summary>
        /// <param name="thresh">The Threshold</param>
        void SetAux2LowWarningThreshold(int thresh);

        /// <summary>
        /// Get Transmitter Wavelength
        /// </summary>
        /// <returns>The Set Wavelength in nm</returns>
        double GetTransmitterWavelength();

        /// <summary>
        /// Set Transmitter Wavelength
        /// </summary>
        /// <param name="wavelength_nm">The Desired Wavelength in nm</param>
        void SetTransmitterWavelength(double wavelength_nm);

        /// <summary>
        /// Get Alarm or Warning Register Byte.
        /// </summary>
        /// <param name="addr">The Address of the register to interrogate
        /// Valid Range of address values is 80h - 87h
        /// </param>
        /// <returns>The latched alarm register data</returns>
        byte GetAlarmOrWarningRegister(byte addr);


        /// <summary>
        /// Get Alarm or Warning  Mask Register Byte.
        /// </summary>
        /// <param name="addr">The Address of the alarm or warning mask register to interrogate
        /// Valid Range of address values is 88h - 95h
        /// </param>
        /// <returns>The latched alarm register data</returns>
        byte GetAlarmOrWarningMaskRegister(byte addr);

        /// <summary>
        /// Set Alarm or Warning  Mask Register Byte.
        /// </summary>
        /// <param name="addr">The Address of the alarm or warning mask register to be set
        /// Valid Range of address values is 88h - 95h
        /// </param>
        /// <param name="mask">The Bit Mask to be applied</param>
        void SetAlarmOrWarningMaskRegister(byte addr, byte mask);

        /// <summary>
        /// Get Module Temperature
        /// </summary>
        /// <returns>The Module Temperature in Degrees C</returns>
        double GetModuleTemperature_DegC();

        /// <summary>
        /// Get Tx Bias Current
        /// </summary>
        /// <returns>The Tx Bias Current in mA</returns>
        double GetTxBiasCurrent_mA();

        /// <summary>
        /// Get Tx Output Power 
        /// </summary>
        /// <returns>The Tx Output Power in dBm </returns>
        double GetTxOutputPower_dBm();

        /// <summary>
        /// Get Tx Output Power 
        /// </summary>
        /// <returns>The Tx Output Power in mW </returns>
        double GetTxOutputPower_mW();

        /// <summary>
        /// Get Rx Input Power 
        /// </summary>
        /// <returns>The Rx Input Power in dBm </returns>
        double GetRxInputPower_dBm();

        /// <summary>
        /// Get Rx Input Power 
        /// </summary>
        /// <returns>The Rx Input Power in mW </returns>
        double GetRxInputPower_mW();

        /// <summary>
        /// Get Auxiliary Measurement 1
        /// </summary>
        /// <returns>The 16 bit measurement quantity</returns>
        int  GetAuxMeas1();

        /// <summary>
        /// Get Auxiliary Measurement 2
        /// </summary>
        /// <returns>The 16 bit measurement quantity</returns>
        int GetAuxMeas2();


        /// <summary>
        /// Get Tx Disable Input Pin state (Reg 110, bit 7)
        /// </summary>
        /// <returns>Returns the state of the Tx Disable Pin</returns>
        bool GetTxDisablePinState();



        /// <summary>
        /// Get Software Tx Disable Bit state (Reg 110, bit 6)
        /// </summary>
        /// <returns>Returns the state of the Soft Tx Disable bit</returns>
        bool GetSoftTxDisableBitState();


        /// <summary>
        /// Set Software Tx Disable Bit state (Reg 110, bit 6)
        /// </summary>
        /// <param name="state">True = 1</param>
        /// <returns>Returns the state of the Soft Tx Disable bit</returns>
        void SetSoftTxDisableBitState(bool state);


        /// <summary>
        /// Get MOD_NR Pin State (Reg 110, bit 5)
        /// </summary>
        /// <returns>Returns the state of the MOD_NR Pin </returns>
        bool GetModNrPinState();

        /// <summary>
        /// Get P_Down State State (Reg 110, bit 4)
        /// </summary>
        /// <returns>Returns the state of the P_Down Pin </returns>
        bool GetPDownPinState();

        /// <summary>
        /// Get Soft P_Down Bit State (Reg 110, bit 3)
        /// </summary>
        /// <returns>Returns the state of the Soft P_Down Bit state </returns>
        bool GetSoftPDownBitState();

        /// <summary>
        /// Set Software Soft P_Down Bit state (Reg 110, bit 3)
        /// </summary>
        /// <param name="state">True = 1</param>
        void SetSoftPDownBitState(bool state);

        /// <summary>
        /// Get Soft !Interrupt Pin State (Reg 110, bit 2)
        /// </summary>
        /// <returns>Returns the state of the !Interrupt Pin state </returns>
        bool GetNIntPinState();

        /// <summary>
        /// Get RX LOS Bit State (Reg 110, bit 1)
        /// </summary>
        /// <returns>Returns the state of the RX LOS bit state </returns>
        bool GetRxLOSState();

        /// <summary>
        /// Get Data_Not_Ready Bit State (Reg 110, bit 0)
        /// </summary>
        /// <returns>Returns the state of the Data_Not_Ready bit state </returns>
        bool GetDataNotReadyBitState();

        /// <summary>
        /// Get Tx_NR Bit State (Reg 111, bit 7)
        /// </summary>
        /// <returns>Returns the state of the Tx_NR bit state </returns>
        bool GetTxNRBitState();

        /// <summary>
        /// Get Tx_Fault Bit State (Reg 111, bit 6)
        /// </summary>
        /// <returns>Returns the state of the Tx_Fault bit state </returns>
        bool GetTxFaultBitState();

        /// <summary>
        /// Get Tx_CDR not locked  Bit State (Reg 111, bit 5)
        /// </summary>
        /// <returns>Returns the state of the Tx_CDR not locked bit state </returns>
        bool GetTxCDRBitState();

        /// <summary>
        /// Get Rx_NR Bit State (Reg 111, bit 4)
        /// </summary>
        /// <returns>Returns the state of the Rx_NR bit state </returns>
        bool GetRxNRBitState();

        /// <summary>
        /// Get Rx_CDR not locked  Bit State (Reg 111, bit 3)
        /// </summary>
        /// <returns>Returns the state of the Rx_CDR not locked bit state </returns>
        bool GetRxCDRBitState();

        /// <summary>
        /// Enable I2c protocol Packet error checking (bit 0 of Register 118)
        /// </summary>
        void EnableI2CPacketErrorChecking();

        /// <summary>
        /// Disable I2c protocol Packet error checking (bit 0 of Register 118)
        /// </summary>
        void DisableI2CPacketErrorChecking();

        /// <summary>
        /// Enter 4-byte optional password. (Put into Registers 119 - 122)
        /// </summary>
        /// <param name="password"></param>
        void EnterOptionalPassword (byte [] password);

        /// <summary>
        /// Get the Password entered by the host (read from Registers 119 - 122) 
        /// </summary>
        /// <returns>Array of 4 bytes containing the passsword.</returns>
        byte[] GetEnteredOptionalPassword();

        /// <summary>
        /// Get the Actual Module Password entered by the host. (read from registers 123 - 126)
        /// </summary>
        /// <returns>Array of 4 bytes containing the passsword.</returns>
        byte[] GetModulePassword();


        /// <summary>
        /// Get the table select byte (Register 127)
        /// </summary>
        /// <returns>Array of 4 bytes containing the passsword.</returns>
        byte[] GetTableSelect();

        /// <summary>
        /// Set the table select byte (Register 127)
        /// </summary>
        /// <returns>Array of 4 bytes containing the passsword.</returns>
        byte[] SetTableSelect();

        /// <summary>
        /// Generic method to allow the readinging of any 1 byte register in the 
        /// address range 0 - 127.
        /// </summary>
        /// <param name="addr">The address must be in the range 0 - 127 </param>
        /// <returns>The register value</returns>
        byte ReadDigitalDiagnosticRegisterByte (byte addr);

        /// <summary>
        /// Generic method to allow the reading of any 2 byte register in the 
        /// address range 0 - 127.
        /// </summary>
        /// <param name="addr">The address must be in the range 0 - 127 </param>
        /// <returns>The register value</returns>
        int ReadDigitalDiagnosticRegisterWord(byte addr);

        /// <summary>
        /// Generic Method to allow the reading of any 1 byte register in the 
        /// paged register address space above 127
        /// </summary>
        /// <param name="tableSelect"> Valid values are 00h - 7Fh</param>
        /// <param name="regAddr"> The address of the register. Must be in the range 128 - 255</param>
        /// <returns>The register value</returns>
        byte ReadPagedRegisterByte(byte tableSelect, byte regAddr);

        /// <summary>
        /// Generic Method to allow the reading of any 2 byte register in the 
        /// paged register address space above 127
        /// </summary>
        /// <param name="tableSelect"> Valid values are 00h - 7Fh</param>
        /// <param name="regAddr"> The address of the register. Must be in the range 128 - 255</param>
        /// <returns>The register value</returns>
        int ReadPagedRegisterWord(byte tableSelect, byte regAddr);

        /// <summary>
        /// Generic Method to allow the reading of any ASCII string from registerser in the 
        /// paged register address space above 127
        /// </summary>
        /// <param name="tableSelect"> Valid values are 00h - 7Fh</param>
        /// <param name="regStartAddr"> The address where the string starts (low address). Must be in the range 128 - 255</param>
        /// <param name="length">The length of the string to be read</param>
        /// <returns>The register value</returns>
        string ReadPagedRegisterString(byte tableSelect, byte regStartAddr, byte length);

        /// <summary>
        /// Get the XFP Vendor Name (ASCII) (Table Select = 1, Registers  163-148)
        /// </summary>
        /// <returns>The Vendor Name</returns>
        string GetXFPVendorName();

        /// <summary>
        /// Get the XFP vendor IEEE company ID (Table Select = 1, Registers  167-165)
        /// </summary>
        /// <returns>The Vendor OUI</returns>
        byte []  GetXFPVendorOUI();

        /// <summary>
        /// Get Part number provided by XFP vendor (ASCII) (Table Select = 1, Registers  183-168)
        /// </summary>
        /// <returns>The Vendor Partnumber</returns>
        string GetXFPVendorPartNumber();

        /// <summary>
        /// Get vendor revision (ASCII) (Table Select = 1, Registers  185-184)
        /// </summary>
        /// <returns>The Vendor Rev</returns>
        string GetXFPVendorRev();

        /// <summary>
        /// Get vendor serial number (ASCII) (Table Select = 1, Registers  211-196)
        /// </summary>
        /// <returns>The Vendor Rev</returns>
        string GetXFPVendorSerialNumber();


        /// <summary>
        /// Get vendor date code (ASCII) (Table Select = 1, Registers  219-212)
        /// 
        /// 212-213 ASCII code, two low order digits of year. (00 = 2000).
        /// 214-215 ASCII code, digits of month (01 = Jan. through 12 = Dec.)
        /// 216-217 ASCII code, day of month (01 - 31)
        /// 218-219 ASCII code, vendor specific lot code, may be blank
        /// </summary>
        /// <returns>The Vendor Date Code as an ASCII string</returns>
        string GetXFPVendorDateCode();
    }
}
