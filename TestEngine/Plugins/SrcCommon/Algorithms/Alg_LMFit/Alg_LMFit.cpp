// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_LMFit.cpp
// A managed wrapper for a Levenberq-Marquardt fit routine
//
// Author: Mark Fullalove

#include "Alg_LMFit.h"

using namespace Bookham::TestLibrary::Algorithms;

// An simple example of a model function
double __stdcall example_fit_function(double t, double *p)
{
	// Y = aX^2 + bX + C
    return (p[0] * t * t) + (p[1] * t) + p[2];
}

// An implementaion of the print routine that doesn't print anything.
void printProgress(int n_par, double *par, int m_dat, double *fvec,
		      void *data, int iflag, int iter, int nfev)
{
	// Print nothing. 
	// If debug output is required use lm_print_default instead.
}

// Empty private constructor
Alg_LMFit::Alg_LMFit(void)
{
}

// Public
double Alg_LMFit::PerformFit(array<double> ^ xData, array<double> ^ yData, array<double> ^% coeffArray, ModelFunc^ modelFunction)
{
	lm_control_type control;
    lm_data_type data;

    lm_initialize_control(&control);

	// Get function pointer
	IntPtr fnPtr = Marshal::GetFunctionPointerForDelegate(modelFunction);
	user_func uf = static_cast<user_func>(fnPtr.ToPointer());
    data.user_func = uf;

	// Allocate memory and copy data from arrays
	int xyArraySize = xData->Length;
	data.user_t = (double *)malloc(xyArraySize * sizeof(double));
	data.user_y = (double *)malloc(xyArraySize * sizeof(double));
	for ( int i = 0 ; i < xyArraySize ; i++)
	{
		data.user_t[i] = xData[i];
		data.user_y[i] = yData[i];
	}

	int coeffArraySize = coeffArray->Length;
	double* coeffs = (double *)malloc(coeffArraySize * sizeof(double));
	for ( int i = 0 ; i < coeffArraySize ; i++)
	{
		coeffs[i] = coeffArray[i];
	}

    // perform the fit:
    lm_minimize(xyArraySize, coeffArraySize, coeffs, lm_evaluate_default, printProgress, &data, &control);

	// Coefficients are now updated so copy back out
	for ( int i = 0 ; i < coeffArraySize ; i++)
	{
		coeffArray[i] = coeffs[i];
	}

	// Clean up
	free(data.user_t);
	free(data.user_y);
	free(coeffs);

	return control.fnorm;
}

// Private function. 
// Code retained to provide an example for debugging.
double Alg_LMFit::PerformFit_InternalFunction(array<double> ^ xData, array<double> ^ yData, array<double> ^% coeffArray)
{
	lm_control_type control;
    lm_initialize_control(&control);
    
	lm_data_type data;
    data.user_func = example_fit_function;

	// Allocate memory and copy data from arrays
	int xyArraySize = xData->Length;
	data.user_t = (double *)malloc(xyArraySize * sizeof(double));
	data.user_y = (double *)malloc(xyArraySize * sizeof(double));
	for ( int i = 0 ; i < xyArraySize ; i++)
	{
		data.user_t[i] = xData[i];
		data.user_y[i] = yData[i];
	}

	int coeffArraySize = coeffArray->Length;
	double* coeffs = (double *)malloc(coeffArraySize * sizeof(double));
	for ( int i = 0 ; i < coeffArraySize ; i++)
	{
		coeffs[i] = coeffArray[i];
	}

    // perform the fit:
    lm_minimize(xyArraySize, coeffArraySize, coeffs, lm_evaluate_default, lm_print_default, &data, &control);

	// Debug information
	printf("status: %s after %d evaluations\n", lm_shortmsg[control.info], control.nfev);

	// Coefficients are now updated so copy back out
	for ( int i = 0 ; i < coeffArraySize ; i++)
	{
		coeffArray[i] = coeffs[i];
	}

	// Clean up
	free(data.user_t);
	free(data.user_y);
	free(coeffs);

	return control.fnorm;
}