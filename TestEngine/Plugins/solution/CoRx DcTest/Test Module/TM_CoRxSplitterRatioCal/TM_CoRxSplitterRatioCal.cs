// [Copyright]
//
// Bookham [Coherent RX DC Test]
// Bookham.TestSolution.TestModules
//
// TM_CoRxSplitterRatioCal.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestSolution.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Coherent RX DC Test, optical path splitter ratio calibration
    /// </summary>
    public class TM_CoRxSplitterRatioCal : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// Test module to Measre splitter ratio 
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {            
            double Sr_Max = configData.ReadDouble("SR_Max");
            double Sr_Min = configData.ReadDouble("SR_Min");
            double Voa_initAtt = configData.ReadDouble("VoaInitAtt");
            double sourceOpcPower_dBm = configData.ReadDouble("LaserSourcePower_dBm");
            DatumString title = configData.GetDatumString("GuiTitle");
            engine.GuiShow();
            engine.SendToGui(title);
            engine.GuiToFront();

            CoRxSourceInstrsChain OpcInstrs = (CoRxSourceInstrsChain)configData.ReadReference("OpcChainInstrs");
            #region Get frequency information
            EnumFrequencySetMode freqSetMode= EnumFrequencySetMode .ByChan;
            double freq_Start=0;
            double freq_Stop=0;
            double freq_Step=0;
            if (configData.IsPresent("Freq_Start_GHz") && 
                configData.IsPresent("Freq_Stop_GHz") && configData.IsPresent("Freq_Step_GHz"))
            {
                freq_Start = configData.ReadDouble("Freq_Start_GHz");
                freq_Stop = configData.ReadDouble("Freq_Stop_GHz");
                freq_Step = configData.ReadDouble("Freq_Step_GHz");
                freqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            else if (configData.IsPresent("Wavelen_Start_nm") && 
                configData.IsPresent("Wavelen_Stop_nm") && configData.IsPresent("wavelen_Step_nm"))
            {
                freq_Start = configData.ReadDouble("Wavelen_Start_nm");
                freq_Stop = configData.ReadDouble("Wavelen_Stop_nm");
                freq_Step = configData.ReadDouble("wavelen_Step_nm");
                freqSetMode = EnumFrequencySetMode.ByWavelen;
            }
            else if (configData.IsPresent("Chan_Start") && 
                configData.IsPresent("Chan_Stop") && configData.IsPresent("Chan_Step"))
            {
                freq_Start = configData.ReadDouble("Chan_Start");
                freq_Stop = configData.ReadDouble("Chan_Stop");
                freq_Step = configData.ReadDouble("Chan_Step");
                freqSetMode = EnumFrequencySetMode.ByChan;
            }
            
            #endregion

            // switch optical off and indicate operator to insert VOA ouput to OPM head
            OpcInstrs.OpticalChainOutputEnable = false;
            bool isCalOk = false;
            engine .SendToGui( new DatumString("TestMsg", 
                "Measuring splitter ratio, don't move the laser fibre..."));

            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
            OpcInstrs.LaserSource.Power_dBm = sourceOpcPower_dBm;
            OpcInstrs.OpticalChainOutputEnable = true;

            try
            {
                if (freqSetMode == EnumFrequencySetMode.ByChan)
                    isCalOk = OpcInstrs.MeasSplitterRatioByChan((int)freq_Start, (int)freq_Stop, (int)freq_Step,
                        Voa_initAtt, Sr_Max, Sr_Min);
                else if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                    isCalOk = OpcInstrs.MeasSplitterRatioByFrequency(freq_Start, freq_Stop, freq_Step,
                        Voa_initAtt, Sr_Max, Sr_Min);
                else
                    isCalOk = OpcInstrs.MeasSplitterRatioByWavelen(freq_Start, freq_Stop, freq_Step,
                        Voa_initAtt, Sr_Min, Sr_Max);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally   // to ensure the optical source output off
            {
                OpcInstrs.OpticalChainOutputEnable = false;
            }
            OpcInstrs.SafeShutDown();  // To ensure the itla is shut down
            

            bool isCalDataSaveOK = false;
            if (isCalOk) isCalDataSaveOK = OpcInstrs.SplitterRatiosCalData.SaveSplitterRatioCalData();
            engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
            engine.SendToGui(new DatumString("TestMsg",
                "Calibration completed, Please recover the optical ouput fibre connector"));
            // return data
            DatumList returnData = new DatumList();
            returnData.AddSint32("IsSrCalOk", (isCalOk? 1:0));
            returnData.AddBool("IsSrCalSaveOk", isCalDataSaveOK);
            return returnData;
        }

        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(TM_CoRxSplitterRatioCalGui)); }
        }

        #endregion
    }
}
