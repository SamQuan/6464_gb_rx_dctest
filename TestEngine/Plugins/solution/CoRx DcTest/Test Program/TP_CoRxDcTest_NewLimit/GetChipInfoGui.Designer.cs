// [Copyright]
//
// Bookham Test Engine
// $projectname$
//
// TP_CoRxDcTest/TestEngineManagedControl1
// 
// Author: alice.huang
// Design: TODO

namespace Bookham.TestSolution.TestPrograms
{
    partial class GetChipInfoGui
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtYChipID = new System.Windows.Forms.TextBox();
            this.txtYChipBin = new System.Windows.Forms.TextBox();
            this.txtYCocSN = new System.Windows.Forms.TextBox();
            this.txtYWaferID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtXChipID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtXChipBin = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtXCocSN = new System.Windows.Forms.TextBox();
            this.txtXwaferID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(699, 316);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(598, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 38);
            this.button1.TabIndex = 4;
            this.button1.Text = "&Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtYChipID);
            this.groupBox3.Controls.Add(this.txtYChipBin);
            this.groupBox3.Controls.Add(this.txtYCocSN);
            this.groupBox3.Controls.Add(this.txtYWaferID);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(301, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 291);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "YChip Information";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 240);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 24);
            this.label9.TabIndex = 9;
            this.label9.Text = "Chip BIN";
            // 
            // txtYChipID
            // 
            this.txtYChipID.Location = new System.Drawing.Point(116, 172);
            this.txtYChipID.Name = "txtYChipID";
            this.txtYChipID.Size = new System.Drawing.Size(150, 29);
            this.txtYChipID.TabIndex = 7;
            this.txtYChipID.Text = "3502";
            // 
            // txtYChipBin
            // 
            this.txtYChipBin.Location = new System.Drawing.Point(116, 235);
            this.txtYChipBin.Name = "txtYChipBin";
            this.txtYChipBin.Size = new System.Drawing.Size(150, 29);
            this.txtYChipBin.TabIndex = 6;
            this.txtYChipBin.Text = "1";
            // 
            // txtYCocSN
            // 
            this.txtYCocSN.Location = new System.Drawing.Point(116, 109);
            this.txtYCocSN.Name = "txtYCocSN";
            this.txtYCocSN.Size = new System.Drawing.Size(150, 29);
            this.txtYCocSN.TabIndex = 5;
            this.txtYCocSN.Text = "CH100040.003";
            // 
            // txtYWaferID
            // 
            this.txtYWaferID.Location = new System.Drawing.Point(116, 46);
            this.txtYWaferID.Name = "txtYWaferID";
            this.txtYWaferID.Size = new System.Drawing.Size(150, 29);
            this.txtYWaferID.TabIndex = 4;
            this.txtYWaferID.Text = "CQRT0074C.3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 24);
            this.label5.TabIndex = 3;
            this.label5.Text = "Chip ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 24);
            this.label6.TabIndex = 2;
            this.label6.Text = "Chip SN";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 24);
            this.label7.TabIndex = 1;
            this.label7.Text = "COC SN";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 24);
            this.label8.TabIndex = 0;
            this.label8.Text = "Wafer ID";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtXChipID);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtXChipBin);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtXCocSN);
            this.groupBox2.Controls.Add(this.txtXwaferID);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(15, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(268, 291);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "XChip Information";
            // 
            // txtXChipID
            // 
            this.txtXChipID.Location = new System.Drawing.Point(113, 172);
            this.txtXChipID.Name = "txtXChipID";
            this.txtXChipID.Size = new System.Drawing.Size(149, 29);
            this.txtXChipID.TabIndex = 11;
            this.txtXChipID.Text = "1101";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Chip ID";
            // 
            // txtXChipBin
            // 
            this.txtXChipBin.Location = new System.Drawing.Point(113, 235);
            this.txtXChipBin.Name = "txtXChipBin";
            this.txtXChipBin.Size = new System.Drawing.Size(149, 29);
            this.txtXChipBin.TabIndex = 9;
            this.txtXChipBin.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 235);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Chip BIN";
            // 
            // txtXCocSN
            // 
            this.txtXCocSN.Location = new System.Drawing.Point(113, 109);
            this.txtXCocSN.Name = "txtXCocSN";
            this.txtXCocSN.Size = new System.Drawing.Size(149, 29);
            this.txtXCocSN.TabIndex = 5;
            this.txtXCocSN.Text = "CH100036.005";
            // 
            // txtXwaferID
            // 
            this.txtXwaferID.Location = new System.Drawing.Point(113, 46);
            this.txtXwaferID.Name = "txtXwaferID";
            this.txtXwaferID.Size = new System.Drawing.Size(149, 29);
            this.txtXwaferID.TabIndex = 4;
            this.txtXwaferID.Text = "CQRT0074A.4";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 109);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "COC SN";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wafer ID";
            // 
            // GetChipInfoGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.Controls.Add(this.groupBox1);
            this.Name = "GetChipInfoGui";
            this.Size = new System.Drawing.Size(715, 341);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtYChipID;
        private System.Windows.Forms.TextBox txtYChipBin;
        private System.Windows.Forms.TextBox txtYCocSN;
        private System.Windows.Forms.TextBox txtYWaferID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtXCocSN;
        private System.Windows.Forms.TextBox txtXwaferID;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtXChipID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtXChipBin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
    }
}
