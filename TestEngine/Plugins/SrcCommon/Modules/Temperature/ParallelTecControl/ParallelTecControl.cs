using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;


namespace Bookham.TestLibrary.TestModules
{
    public class ParallelTecControl : ITestModule
    {
        /// <summary>
        /// This does the test for us, this class is assuming two instruments, each with a tec,
        /// This is designed for large thermal loads
        /// </summary>
        /// <param name="engine">The itest engine engine</param>
        /// <param name="userType">operator engineer or technician</param>
        /// <param name="configData">a datum list of all required config data consisting of</param>
        /// <param name="instruments">Should only contain a single tec controller unit</param>
        /// <param name="chassis">the chassis</param>
        /// <param name="calData">any cal data (none reqd)</param>
        /// <param name="previousTestData">any previous data (none reqd)</param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            TecController tecClass = new TecController(engine);            
            // Interrogate the config data and get a list of all the values we need
            // set up all the values in the tecClass class ready for our use
            DatumDouble datumMaxExpectedTimeForOperation_s = new DatumDouble("MaxExpectedTimeForOperation_s");
            configData.Read(datumMaxExpectedTimeForOperation_s);
            tecClass.TimeoutTimerInterval_s = datumMaxExpectedTimeForOperation_s.Value;

            DatumDouble datumTimeInToleranceWindow_s = new DatumDouble("RqdTimeInToleranceWindow_s");
            configData.Read(datumTimeInToleranceWindow_s);
            tecClass.WithinToleranceWindowTimer_s = datumTimeInToleranceWindow_s.Value;

            DatumDouble datumStabilisationTime_s = new DatumDouble("RqdStabilisationTime_s");
            configData.Read(datumStabilisationTime_s);
            tecClass.StabilisationTimer_s = datumStabilisationTime_s.Value;

            DatumSint32 datumTempTimeBtwReadings_ms = new DatumSint32("TempTimeBtwReadings_ms");
            configData.Read(datumTempTimeBtwReadings_ms);
            tecClass.delayTimeBetweenReadings_ms = datumTempTimeBtwReadings_ms.Value;

            DatumDouble datumTemperature1ToSet_C = new DatumDouble("Tec1_SetPointTemperature_C");
            configData.Read(datumTemperature1ToSet_C);
            tecClass.targetReading1_C = datumTemperature1ToSet_C.Value;

            DatumDouble datumTemperature2ToSet_C = new DatumDouble("Tec2_SetPointTemperature_C");
            configData.Read(datumTemperature1ToSet_C);
            tecClass.targetReading2_C = datumTemperature2ToSet_C.Value;

            DatumDouble datumTemperatureTolerance_C = new DatumDouble("TemperatureTolerance_C");
            configData.Read(datumTemperatureTolerance_C);
            tecClass.targetTolerance_C = datumTemperatureTolerance_C.Value;

            //in this case we need to control both tec controllers at the same time
            tecClass.tecController1 = (InstType_TecController)instruments["TecController1"];
            tecClass.tecController2 = (InstType_TecController)instruments["TecController2"];

            tecClass.twoTecsInParallel = true;

            tecClass.TimeoutTimerStarted = true;
            tecClass.tecController1.SensorTemperatureSetPoint_C = tecClass.targetReading1_C;
            tecClass.tecController2.SensorTemperatureSetPoint_C = tecClass.targetReading2_C;

            //switch them on 
            tecClass.tecController1.OutputEnabled = true;
            tecClass.tecController2.OutputEnabled = true;

            //use generic routine
            tecClass.TecControlRoutine();

            // stop the TimeoutException timer
            tecClass.TimeoutTimerStarted = false;

            // we are completed successfully.
            return (tecClass.moduleResults);
        }


        /// <summary>
        /// The user control we are using for display
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(ModuleGui)); }
        }

    }
}
