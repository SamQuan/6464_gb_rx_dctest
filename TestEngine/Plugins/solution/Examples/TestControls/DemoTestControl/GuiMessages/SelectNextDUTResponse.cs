// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// SelectNextDUTResponse.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;

namespace Bookham.DemoTestControlPlugin.GuiMessages
{
	/// <summary>
	/// Response to a selectNextDUTRequest. Contains the serial number of the next device to 
	/// be tested from the currently loaded batch.
	/// </summary>
	internal sealed class SelectNextDUTResponse
	{
		/// <summary>
		/// Constructor. Stores the serial number of the device to be tested.
		/// </summary>
		/// <param name="serialNumber">The serial number.</param>
		internal SelectNextDUTResponse(string serialNumber)
		{
			this.serialNumber = serialNumber;
		}

		/// <summary>
		/// The serial number of the device to be tested.
		/// </summary>
		internal readonly string serialNumber;
	}
}
