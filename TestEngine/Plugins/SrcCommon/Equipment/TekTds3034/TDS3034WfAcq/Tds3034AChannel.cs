using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Bookham.TestLibrary.Utilities;
using System.IO;

namespace Bookham.TestLibrary.Instruments

{
    //public delegate void EventHandleWaveformAcquisitionStatus( object sender, oscAcqusitionCompletedEventArgs e);    
    //public delegate void  EventHandleGetWaveformData( object sender, OscWaveformDataEventArgs e);
    //public delegate void  EventHandleSetChannelDisplayStatus( object sender, OscChannelDisplayStatusEventArgs e);

    public class Tds3034Channel

    {
        private Inst_Tds3034 osc;       

        public event EventHandler< OscWaveformDataEventArgs> GetWaveformData = null;
        //public event EventHandleGetWaveFormScaleInformation GetScaleInformation;
        public event EventHandler<OscChannelDisplayStatusEventArgs> GetChannelDisplayStatus;
        public event EventHandler<oscAcqusitionCompletedEventArgs> GetWaveformAcquisitionStatus;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="OscChannel"></param>
        public Tds3034Channel(Inst_Tds3034 OscChannel)
        {
            osc = OscChannel;
        }
        
        #region Declare all events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="waveformData"></param>
        /// <param name="scaleInfo"></param>
        public void OnGetWaveformData(Inst_Tds3034.ClsWaveformData waveformData, string fileName)
        {
            EventHandler<OscWaveformDataEventArgs> getWaveformData = GetWaveformData;
            if (getWaveformData != null)

            {
                OscWaveformDataEventArgs args = new OscWaveformDataEventArgs(waveformData,fileName);
                getWaveformData(this, args);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayStatus"></param>
        public void OnGetChannelDisplayStatus(bool displayStatus)
        {
            EventHandler<OscChannelDisplayStatusEventArgs> getChannelDisplayStatus = GetChannelDisplayStatus;
            if (getChannelDisplayStatus != null)
            {
                OscChannelDisplayStatusEventArgs args = new OscChannelDisplayStatusEventArgs(displayStatus);
                getChannelDisplayStatus(this, args);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="acquisitionStatus"></param>
        public void OnGetWaveformAcquisitionStatus(bool acquisitionStatus)
        {
            EventHandler<oscAcqusitionCompletedEventArgs> getAcquisitionStatus = GetWaveformAcquisitionStatus;
            if (getAcquisitionStatus != null)
            {
                oscAcqusitionCompletedEventArgs args = new oscAcqusitionCompletedEventArgs(acquisitionStatus);
                getAcquisitionStatus(this, args);
            }

        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        public int Channel
        {
            get
            {
                if (osc != null)
                    return 0;
                else
                    return  int .Parse (osc.Slot);
            }
        }

        public void Setup()
        { }

        public void SetChannelDisplayStatus( bool  displayStatus)
        {
            osc.IsChannelWaveformDisplay = displayStatus;
            Thread.Sleep(10);
            OnGetChannelDisplayStatus(osc.IsChannelWaveformDisplay);
        }

        public void StartWavefromAcquisition()
        {
            if ( osc .IsChannelWaveformDisplay)
            {
                osc.AcquisitionStopCondition = Inst_Tds3034.EnumAcquisitionStopCondition.SEQ;
                osc.EnableServiceRequest();
                osc .AcquisitionState = true ;
                OnGetWaveformAcquisitionStatus( false );
                osc .Trigger();                

                osc .WaitForAcquisitionToFinish();
                OnGetWaveformAcquisitionStatus (true );
            }
        }

        public void ReadWaveformData( bool isSaveData2File)
        {
            
            Inst_Tds3034 .ClsWaveformData  waveformData ;

            waveformData = osc .GetWaveform(  );

            string fileName = "";
            if ( isSaveData2File )
                fileName = saveWaveform(waveformData);
            OnGetWaveformData ( waveformData ,fileName );
        }

        private string  saveWaveform(Inst_Tds3034.ClsWaveformData wfData)
        {
            

            string fileName = "";
            if (wfData.YRawData.Length  > 0)
            {
                fileName = string.Format("wfdata_CH{0}_{1}.csv", Channel, DateTime.Now.ToString("yyyyMMddHHmmssddd"));

                fileName = Path.Combine(@"C:\Alice\wfData", fileName);
                using (CsvWriter writer = new CsvWriter())
                {
                    List<string> lstWfData = wfData .AllDataToStringList();

                    List<string > lstScale = wfData .ScaleInfomation .ScaleInfoToStringList();

                    foreach( string strScale in lstScale )
                    {
                        lstWfData .Add (strScale );
                    }

                    writer.WriteFile(fileName, lstWfData);
                }
            }

            return fileName;

        }
    }

    
}
