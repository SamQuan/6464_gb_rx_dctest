// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_Nt5A.cs
//
// Author: alice.huang, 2010
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
// TODO: Uncomment this once you refer to an instrument type
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.

    /// <summary>
    /// Instrument for KD NT5A
    /// </summary>
    public class Inst_Nt5A : InstType_TecController
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Nt5A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instrNt5a = new InstrumentDataRecord(
                "KD_OPTICS Peltier",			// hardware name 
                "V0.0",			// minimum valid firmware version 
                "V3.0");		// maximum valid firmware version 
            ValidHardwareData.Add("KD Nt5a", instrNt5a);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisNt5a = new InstrumentDataRecord(
                "Chassis_Nt5A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("ChassisNt5a", chassisNt5a);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Nt5A)chassisInit;

            if (slotInit == "")
                channel = 0;
            else 
                channel = int.Parse(slotInit);
        }
        #endregion

        #region Public Instrument Methods and Properties

        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string hid = instrumentChassis.HardwareIdentity;
                return hid;
            }
        }

        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string fv = instrumentChassis.FirmwareVersion;

                // Return firmware version
                return fv;
            }
        }


        /// <summary>
        /// Configures the instrument into a default state.
        /// </summary>
        public override void SetDefaultState()
        {
            this.OutputEnabled = false;
            SensorCurrent_amp = 0.00001;
            this.OperatingMode = ControlMode.Resistance;
        }

        #endregion

        #region Public TecController InstrumentType Methods and Properties

        /// <summary>
        /// Gets / sets whether we are in safe mode of operation, 
        /// we are in this mode by default
        /// </summary>
        public bool SafeModeOperation
        {
            get
            {
                TecsDataValues tecValue = ReadTecsDataValues();
                return tecValue.OutputCurrentLimitOn;
            }
            set
            {
                string cmd;
                if (value)
                    cmd = channel.ToString() + "/CPM";
                else
                    cmd = channel.ToString() + "/CUM";
                try
                {
                    instrumentChassis.Write_Unchecked(cmd, this);                    
                }
                catch (Exception ex)
                {
                    throw new InstrumentException(
                        "Exception happened when trying to set protect mode with command " + cmd + ex.Message);
                }
            }
        }

        /// <summary>
        /// Allows us to enable the output
        /// </summary>

        public override bool OutputEnabled
        {
            get
            {
                // Need to do a read all tecs data values, 
                // then look at the particular one we want.
                myTecDataValues = this.ReadTecsDataValues();
                return (myTecDataValues.OutputOn);
            }

            set
            {
                // This instrument will NOT allow an output enable to work unless
                // either an RAV or an RAS has been sent. So i do it here explicitly.
                string response;
                response = this.Query(channel .ToString() + "/RAS");
                string cmd;

                if (value == true)
                {
                    cmd =channel.ToString() +  "/COE";
                    this.Write(cmd);

                    //Weird instrument behaviour, 
                    //fails to do enable but doesn't show an error string
                    //Put this extra check in to catch the error

                    myTecDataValues = this.ReadTecsDataValues();

                    if (myTecDataValues.OutputOn == false)
                    {
                        throw new InstrumentException("The output has not enabled correctly");
                    }
                }
                else
                {
                    cmd =channel.ToString() + "/COD";
                    this.Write(cmd);
                }
            }
        }

        /// <summary>
        /// Gets or Sets the operating mode of the Nt10a, only expects Resistance or Temperature
        /// This cmd will result in output being OFF if mode is switched from existing mode to a new one.
        /// </summary>
        public override InstType_TecController.ControlMode OperatingMode
        {
            get
            {
                // Need to do a read all tecs data values, 
                // then look at the particular one we want.       
                myTecDataValues = this.ReadTecsDataValues();
                return (myTecDataValues.ControlMode);
            }
            set
            {
                if (OperatingMode != value)
                {
                    // Initialise the mode string
                    string modecmd;

                    switch (value)
                    {
                        case ControlMode.Temperature: modecmd =channel .ToString() + "/CCP"; break;
                        case ControlMode.Resistance: modecmd = channel .ToString() + "/CCT"; break;

                        default: throw new InstrumentException("Invalid control mode " + OperatingMode.ToString());
                    }

                    this.Write(modecmd);
                }
            }
        }

        /// <summary>
        /// Sets/returns the Proportional Gain Constant.
        /// </summary>
        public override double ProportionalGain
        {
            get
            {
                // Need to do a read all tecs data set points, 
                // then look at the particular one we want.
                myTecSetPoints = this.ReadTecDataSetPoints();
                return (Convert.ToDouble(myTecSetPoints.ProportionalGainSetting));
            }
            set
            {
                this.Write(string.Format("{0}/SPG {1:D4}", channel , Convert.ToInt32(value)));
            }
        }

        /// <summary>
        /// Sets/returns the Derivative Gain Constant. Not Supported by this instrument.
        /// </summary>
        public override double DerivativeGain
        {
            get
            {
                throw new InstrumentException("Cannot Get Derivative Gain. Instrument NT10A does not support this parameter.");
                //return (controlConstants);
            }
            set
            {
                throw new InstrumentException("Cannot Set Derivative Gain. Instrument NT10A does not support this parameter.");
            }
        }


        /// <summary>
        /// Sets/returns the Integral Gain Constant.
        /// </summary>
        public override double IntegralGain
        {
            get
            {
                // Need to do a read all tecs data set points, 
                // then look at the particular one we want.
                myTecSetPoints = this.ReadTecDataSetPoints();
                return (Convert.ToDouble(myTecSetPoints.IntegralGainSetting));
            }
            set
            {
                this.Write(string.Format("{0}/SIG {1:D4}", channel , Convert.ToInt32(value)));
            }
        }

        /// <summary>
        /// Gets the sensor type thermistor or rtd, doesn't support set operations
        /// </summary>
        public override InstType_TecController.SensorType Sensor_Type
        {
            get
            {
                //sensor type is implicitly defined from the control mode type
                InstType_TecController.SensorType mysensor = new SensorType();
                myTecSetPoints = this.ReadTecDataSetPoints();

                //control mode is already defined as only two possible values for Nt10a
                if (myTecSetPoints.ControlMode == InstType_TecController.ControlMode.Temperature)
                {
                    mysensor = InstType_TecController.SensorType.PRT_PT100_2wire;
                }
                else if (myTecSetPoints.ControlMode == InstType_TecController.ControlMode.Resistance)
                {
                    mysensor = InstType_TecController.SensorType.Thermistor_2wire;
                }
                return (mysensor);

            }
            set
            {
                throw new InstrumentException("The method or operation is not supported.");
            }
        }


        /// <summary>
        /// Sets/returns the Thermistor Sensor Steinhart-Hart Equation coefficients. 
        /// </summary>
        public override SteinhartHartCoefficients SteinhartHartConstants
        {
            get
            {
                return this.steinhartHartConstants;
            }

            set
            {
                this.steinhartHartConstants = value;
            }
        }
        

        /// <summary>
        /// Set/returns the sensor resistance  set point
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public override double SensorResistanceSetPoint_ohm
        {
            get
            {
                myTecDataValues = this.ReadTecsDataValues();
                if (myTecDataValues.ControlMode == ControlMode.Resistance)
                {
                    //Return the set resistance point in ohms. Note that the instrument 
                    // returns K ohms, so must convert units.
                    myTecSetPoints = this.ReadTecDataSetPoints();
                    return (myTecSetPoints.SetPointResistance_Kohm * 1000);
                }
                else
                {
                    throw new InstrumentException("Cannot read resistance if not in Resistance control mode");
                }
            }

            set
            {
                myTecDataValues = this.ReadTecsDataValues();
                if (myTecDataValues.ControlMode == ControlMode.Resistance)
                {
                    //Set the resistance point.
                    this.Write(string.Format("{0}/SST {1:N2}", channel , (value * 0.001)));
                }
                else
                {
                    throw new InstrumentException("Cannot set resistance if not in Resistance control mode");
                }
            }
        }

        /// <summary>
        /// Returns the actual sensor resistance
        /// Note that get should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public override double SensorResistanceActual_ohm
        {
            get
            {
                myTecDataValues = this.ReadTecsDataValues();
                if (myTecDataValues.ControlMode == ControlMode.Resistance)
                {
                    return (myTecDataValues.ThermistorResistance_Kohms * 1000);
                }
                else
                {
                    throw new InstrumentException("Cannot read resistance if not in Resistance control mode");
                }
            }
        }


        /// <summary>
        /// Get/Set thermistor current. 
        /// only 0.00001A or 0.0001A available 
        /// </summary>
        public override double SensorCurrent_amp
        {
            get
            {
                return thermistorCurrent;
            }
            set
            {
                
                if (value > 0.00001)
                {
                    try
                    {
                        this .Write ( channel.ToString() + "/CHU");
                        thermistorCurrent = 0.0001;
                    }
                    catch (Exception ex)
                    {
                        throw new InstrumentException(
                            " Can't set thermistor current to 100uA with exception " + ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        this .Write ( channel.ToString() + "/CTE");
                        thermistorCurrent = 0.00001;
                    }
                    catch (Exception ex)
                    {
                        throw new InstrumentException(
                            " Can't set thermistor current to 10uA with exception " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Set/returns the peltier current set point
        /// This operation is not supported by this instrument. 
        /// </summary>
        public override double TecCurrentSetPoint_amp
        {
            get
            {
                throw new InstrumentException("The method or operation is not supported.");
            }

            set
            {
                throw new InstrumentException("The method or operation is not supported.");
            }
        }

        /// <summary>
        /// Returns the actual TEC (peltier) current.
        /// </summary>
        public override double TecCurrentActual_amp
        {
            get
            {
                myTecDataValues = this.ReadTecsDataValues();
                return (myTecDataValues.PeltierCurrent_A);
            }
        }


        /// <summary>        
        /// Not suppored by this instrument.
        /// </summary>
        public override double TecVoltageSetPoint_volt
        {
            get
            {
                throw new InstrumentException("The method or operation is not supported.");
            }

            set
            {
                throw new InstrumentException("The method or operation is not supported.");
            }
        }

        /// <summary>
        /// Returns the actual TEC voltage
        /// </summary>
        public override double TecVoltageActual_volt
        {
            get
            {
                myTecDataValues = this.ReadTecsDataValues();
                return (myTecDataValues.PeltierVoltage_V);
            }
        }


        /// <summary>
        /// This functionality is not supported by this Tec unit
        /// </summary>
        public override double TecResistanceDC_ohm
        {
            get { throw new InstrumentException("The method or operation is not supported."); }
        }

        /// <summary>
        /// This functionality is not supported by this Tec unit
        /// </summary>
        public override double TecResistanceAC_ohm
        {
            get { throw new InstrumentException("The method or operation is not supported."); }
        }


        /// <summary>
        /// Get or set the maximum current the Tec unit can supply
        /// </summary>
        public override double TecCurrentCompliance_amp
        {
            get
            {
                myTecSetPoints = this.ReadTecDataSetPoints();
                return (myTecSetPoints.MaxPeltierCurrent_A);
            }
            set
            {
                this.Write(string.Format("{0}/SMI {1:N2}", channel , value));
            }
        }

        /// <summary>
        /// Get/Set Maximun Peltier Voltage the TEC unit can supply
        /// Note: only 2 or 20 available
        /// </summary>
        public override double TecVoltageCompliance_volt
        {
            get
            {
                TecsDataValues tecValues = ReadTecsDataValues();
                double peltierComplianceVolt_V = tecValues.IsLowVoltRange ? 2.0 : 20.0;

                return peltierComplianceVolt_V;
            }
            set
            {
                if (Math.Abs(value) > 2)
                    this.Write(channel.ToString() + "/CLV");
                else
                    this.Write(channel.ToString() + "/CHV");
            }
        }
        /// <summary>
        /// Sets/returns the RTD Sensor Callendar-Van Dusen Equation coefficients. 
        /// Not supported for this instrument, as it only supports IEC 755 PT100 compliant 
        /// RTDs. The instrument carries out the calculations converting from resistance to 
        /// Temperature and vice versa internally.
        /// </summary>
        public override CallendarVanDusenCoefficients CallendarVanDusenConstants
        {
            get
            {
                throw new InstrumentException("Instrument NT10A Driver does not support getting Callendar-Van Dusen Constants");
            }

            set
            {
                throw new InstrumentException("Instrument NT10A Driver does not support setting Callendar-Van Dusen Constants");
            }
        }
        /// <summary>
        /// Temperature function is not available on this insturment
        /// </summary>
        public override double SensorTemperatureActual_C
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }
        /// <summary>
        /// Temperature function is not available on this instrument
        /// </summary>
        public override double SensorTemperatureSetPoint_C
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public void Reset()
        {
            this.Write("0/CIN");
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsAverageOn
        {
            get
            {
                TecsDataValues tecValues = ReadTecsDataValues();

                return tecValues.AveragingOn;
            }
            set
            {
                if (value)
                    this.Write(channel.ToString() + "/CAR");
                else
                    this.Write(channel.ToString() + "/CIR");
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public void EnableThermistor()
        {
            this.Write(channel.ToString() + "/CON");
        }
        #region Private Methods

        /// <summary>
        /// Convert a thermistor resistnce Value (KOhms) into a Temperature in 
        /// Degrees Celsius, using the Steinhart-Hart equation.
        /// </summary>
        /// <param name="resistance">Resistance (KOhms) to be converted.</param>
        /// <returns>The Temperature in Celsius.</returns>
        private double convertThermistorResistanceKohmsToTemperatureDegC(double resistance)
        {
            //Call Test Engine Algorithms method for carrying out the conversion. Note 
            // that resistance in K ohms must first be converted to ohms. 
            return (SteinhartHart.ResistanceOhmsToTemperatureCelsius((resistance * 1000),
                                                                        steinhartHartConstants.A,
                                                                        steinhartHartConstants.B,
                                                                        steinhartHartConstants.C));
        }

        /// <summary>
        /// Convert a temperature in Degrees Celsius to a Thermistor Resistance (K ohms)
        /// using the Steinhart-Hart equation.
        /// </summary>
        /// <param name="temperature">The Temperature in Celsius.</param>
        /// <returns>Calculated Resistance (K ohms)</returns>
        private double convertTemperatureDegCToThermistorResistanceKohms(double temperature)
        {
            // Call Test Engine Algorithms method for carrying out the conversion.
            double resistance = SteinhartHart.TemperatureCelsiusToResistanceOhms(temperature,
                                                                                   steinhartHartConstants.A,
                                                                                   steinhartHartConstants.B,
                                                                                   steinhartHartConstants.C);
            // Return the result of the above algorithm, converted to K ohms.
            return (resistance * 0.001);
        }

        /// <summary>
        /// This function will read the tec controllers set point values,
        /// and can decipher if we are in temperature or resistance mode of operation
        /// Note: response[0] will be a string indicating set point temperature or resistance, it
        /// will have a T or a P on the end to indicate if we are in thermistor (Resistance, T) or PRT (Temperature, P) mode.
        /// </summary>
        /// <returns>TecsDataSetPoints structure</returns>
        private TecsDataSetPoints ReadTecDataSetPoints()
        {
            TecsDataSetPoints mySetPoints = new TecsDataSetPoints();
            string[] response;
            string cmd = channel .ToString() + "/RAS";

            response = this.Query(cmd).Split(',');
            //response should be comma seperated list with 4 elements

            if (response.Length != 4)
            {
                throw new InstrumentException("RAS Response doesn't have expected number of elements");
            }
            string localValue = response[0];
            string suffix = localValue.Substring(localValue.Length - 1, 1);
            localValue = localValue.Remove(localValue.Length - 1, 1);

            if (suffix == "P")
            {
                mySetPoints.ControlMode = InstType_TecController.ControlMode.Temperature;
                mySetPoints.SetPointTemperature_DegC = getDblFromString(cmd, localValue);
            }
            else if (suffix == "T")
            {
                mySetPoints.ControlMode = InstType_TecController.ControlMode.Resistance;
                mySetPoints.SetPointResistance_Kohm = getDblFromString(cmd, localValue);
            }
            else
            {
                throw new InstrumentException("Invalid response for DatasetPointsQuery " + response[0]);
            }

            mySetPoints.MaxPeltierCurrent_A = getDblFromString(cmd, response[1]);
            mySetPoints.ProportionalGainSetting = getIntFromString(cmd, response[2]);
            mySetPoints.IntegralGainSetting = getIntFromString(cmd, response[3]);
            return (mySetPoints);
        }

        /// <summary>
        /// This function will read the tec controllers set point values,
        /// and can decipher if we are in temperature or resistance mode of operation
        /// Note: response[0] will be a string indicating actual temperature or resistance, it
        /// will have a T or a P on the end to indicate if we are in thermistor (Resistance, T) or PRT (Temperature, P) mode.
        /// </summary>
        /// <returns>TecsDataValues structure</returns>
        private TecsDataValues ReadTecsDataValues()
        {
            TecsDataValues myTecsData = new TecsDataValues();
            string[] response;
            string cmd = channel .ToString() + "/RAV";
            response = this.Query(cmd).Split(',');

            //response should be comma seperated list
            if (response.Length != 6)
            {
                throw new InstrumentException(channel.ToString() + "/RAV Response doesn't have expected number of elements");
            }

            myTecsData.PeltierCurrent_A = getDblFromString(cmd, response[1]);
            myTecsData.PeltierVoltage_V = getDblFromString(cmd, response[2]);
            myTecsData.OutputOn = getboolFromTecString("myTecsData.OutputOn", response[3]);
            myTecsData.OutputCurrentLimitOn = getboolFromTecString("myTecsData.OutputCurrentLimitOn", response[4]);
            myTecsData.AveragingOn = getboolFromTecString("myTecsData.AveragingOn", response[5]);
            
            //if low range, the format should be "SX.XXXX", or it will be "SXX.XXX"
            myTecsData.IsLowVoltRange = (response[2].IndexOf('.') > 3) ? false : true;

            string localValue = response[0];

            string suffix = localValue.Substring(localValue.Length - 1, 1);
            localValue = localValue.Remove(localValue.Length - 1, 1);

            if (suffix == "P")
            {
                myTecsData.ControlMode = InstType_TecController.ControlMode.Temperature;
                myTecsData.PRTTemperature_DegC = getDblFromString(cmd, localValue);
                if ((myTecsData.OutputOn) && (localValue == "999.9")) // 999.9P = Out of limits
                {
                    throw new InstrumentException("PRT reading is out of limits range ie O/C");
                }
            }
            else if (suffix == "T")
            {

                myTecsData.ControlMode = InstType_TecController.ControlMode.Resistance;
                myTecsData.ThermistorResistance_Kohms = getDblFromString(cmd, localValue);
                if ((myTecsData.OutputOn) && (localValue == "99.99"))   // 99.99T = Out of limits
                {
                    throw new InstrumentException("Thermistor reading is out of limits range ie O/C");
                }
            }
            else
            {
                throw new InstrumentException("Invalid response for TecsDataValues Query " + response[0]);
            }

            return (myTecsData);
        }

        /// <summary>
        /// Helper function to convert a string to a double, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        /// <summary>
        /// Helper function dealing specifically with this tec controllers outputs,
        /// will decode the F or O that the tec controller provides into a true or false
        /// </summary>
        /// <param name="command">the command sent to instrument</param>
        /// <param name="response">the response of the instrument</param>
        /// <returns>bool indication of what the tec controller replies</returns>
        private bool getboolFromTecString(string command, string response)
        {
            if (response == "F") //off
            {
                return (false);
            }
            if (response == "O")
            {
                return (true); //on
            }
            else
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a bool flag F or 0:  " + response);
            }
        }

        /// <summary>
        /// Helper function to convert a string to a int32, catching any errors that occur.
        /// </summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// <returns>The int32 value</returns>
        private Int32 getIntFromString(string command, string response)
        {
            try
            {
                return Int32.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a Int32: " + response,
                    e);
            }
        }


        /// <summary>
        /// Write and checks for error or just writes, depending on safeModeOperation
        /// </summary>
        /// <param name="command">the command we wish to write to chassis</param>
        private void Write(string command)
        {
            //Add a line feed
            instrumentChassis.Write_Unchecked(command + "\n", this);
           
            //By now doing a query command we are looking for any errors which have happened
            //as a result of our previous write command 
            string response = instrumentChassis.Query_Unchecked( channel .ToString() + "/RAV\n", this);
            if (response == "Error")
            {
                throw new InstrumentException("The command '" + command + "' has caused an ERROR");
            }
           

        }

        /// <summary>
        /// Query, depending on safemodeoperation will check for error as well
        /// </summary>
        /// <param name="command"></param>
        /// <returns>the output from the instrument</returns>
        private string Query(string command)
        {
            //Add a line feed
            command += "\n";

            string response = instrumentChassis.Query_Unchecked(command, this);

            // the received message will be in format "channel/" + resonse string
            response = response.TrimStart(channel.ToString().ToCharArray());
            response = response.TrimStart('/');
            
            return (response);
        }


        #endregion
        #region Private data
        // Chassis reference
        private Chassis_Nt5A instrumentChassis;

        private TecsDataSetPoints myTecSetPoints;
        private TecsDataValues myTecDataValues;
        private SteinhartHartCoefficients steinhartHartConstants;        
        private int channel;
        private double thermistorCurrent=0;        
        #endregion

    }
}
