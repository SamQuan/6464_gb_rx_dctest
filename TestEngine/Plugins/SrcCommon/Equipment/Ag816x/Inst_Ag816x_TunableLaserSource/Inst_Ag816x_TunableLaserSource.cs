//==========================================================================================================
// Copyright:			Copyright ?2006 Bookham Inc. All Rights Reserved. This document is company 
//						confidential and should not be copied, reproduced or otherwise utilised outside of 
//						Bookham Inc without the prior written consent of the company.
//
// Project:				Bookham.TestEngine
// Module:				Inst_Ag816x_TunableLaserSource
// 
// FileName:			Inst_Ag816x_TunableLaserSource.cs
//
// Author:				ian.webb
// Design Document:		N/A - look at Bookham Test-Engine documentation for information about the format of
//                      instrument channel driver libraries.
// Revision History:	02Jan2007 ian.webb: Initial version.
//                      10Jan2007 ian.webb: Removed lock-codes storage, being replaced by an enforcement of
//                                          the default password of 1234 being required on the instrument.
//                                          NOTE: This was done to comply with the partitioning of activities
//                                          in the Test-Engine (Registry access and GUI activities are run on 
//                                          different threads.)
//                      16Jan2007 ian.webb: Changes due to feedback from Test-Engine team.
//
// Description:			Instrument channel driver for a TLS module in an Agilent 816xx main-frame.
//==========================================================================================================

using System;
using System.Collections.Generic;
using System.Text;

using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// A tunable laser source channel driver for Agilent 816x series mainframes.
    /// </summary>
    public class Inst_Ag816x_TunableLaserSource : InstType_TunableLaserSource
    {
        #region Private Variables

        /// <summary>
        /// object to use for communications.
        /// </summary>
        private Chassis_Ag816x instChassis = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of the Inst_Ag816x_TunableLaserSource class.
        /// </summary>
        /// <param name="instrumentName">descriptive instrument-channel name.</param>
        /// <param name="driverName">name of the instrument-channel driver.</param>
        /// <param name="slotId">instrument-chassis slot identifier (for multi-channel or mainframe based instruments.)</param>
        /// <param name="subSlotId">instrument-chassis module, sub-channel identifier (for mainframes with multi-channel modules.)</param>
        /// <param name="chassis">chassis object to use for communications.</param>
        public Inst_Ag816x_TunableLaserSource(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            //update this version when new firmware loads are available that are still compatible with the driver!
            const string maxFirmwareVersion = "V4.85(1126)";

            /*
             * Add details for each TLS module variant 
             */

            InstrumentDataRecord ag81680aInfo = new InstrumentDataRecord(
                "81680A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("81680A", ag81680aInfo);

            InstrumentDataRecord hp81680aInfo = new InstrumentDataRecord(
                "HP81680A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("HP81680A", hp81680aInfo);

            InstrumentDataRecord ag81940aInfo = new InstrumentDataRecord(
                "81940A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("81940A", ag81940aInfo);

            InstrumentDataRecord hp81940aInfo = new InstrumentDataRecord(
                "HP81940A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("HP81940A", hp81940aInfo);

            InstrumentDataRecord ag81949aInfo = new InstrumentDataRecord(
                "81949A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("81949A", ag81949aInfo);

            InstrumentDataRecord hp81949aInfo = new InstrumentDataRecord(
                "HP81949A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("HP81949A", hp81949aInfo);

            InstrumentDataRecord ag81989aInfo = new InstrumentDataRecord(
                "81989A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("81989A", ag81989aInfo);

            InstrumentDataRecord hp81989aInfo = new InstrumentDataRecord(
                "HP81989A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("HP81989A", hp81989aInfo);

            InstrumentDataRecord ag81640aInfo = new InstrumentDataRecord(
                "HP81640A",				                            // hardware name 
                "0",  			                                    // minimum valid firmware version 
                maxFirmwareVersion);			                    // maximum valid firmware version 
            ValidHardwareData.Add("Ag81640A", ag81640aInfo);

            InstrumentDataRecord Ag81682aInfo = new InstrumentDataRecord(
            "HP81682A",				// hardware name 
            "0",  			// minimum valid firmware version 
            maxFirmwareVersion);			// maximum valid firmware version 
            ValidHardwareData.Add("Ag81682A", Ag81682aInfo);

            /*
             * Add 816x chassis details
             */

            InstrumentDataRecord ag816xData = new InstrumentDataRecord(
                "Chassis_Ag816x",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "2.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag816x", ag816xData);

            /*
             * save the chassis driver object
             */
            this.instChassis = (Chassis_Ag816x)chassis;
        }

        #endregion


        #region Private Members

        /// <summary>
        /// Check that the slot isn't empty. This may happen before errors setup, so DON'T call any 'checked' Chassis methods.
        /// </summary>
        private void CheckSlotNotEmpty()
        {
            bool slotEmpty;

            // All the firmware is in the plugin slot, not the head
            string command = String.Format(":SLOT{0}:EMPTY?", this.Slot);
            string resp = this.instChassis.Query(command, this);
            if (resp == "1") slotEmpty = true;
            if (resp == "0") slotEmpty = false;
            else
            {
                string errStr = String.Format("Command '{0}' returned an unexpected value: '{1}'", command, resp);
                throw new InstrumentException(errStr);
            }

            if (slotEmpty)
            {
                throw new InstrumentException("Slot " + this.Slot + " is empty!");
            }
        }

        /// <summary>
        /// Reads back the laser-on rise time (from the instrument) and waits this period to ensure the laser power 
        /// has been reached before continuing.
        /// </summary>
        private void WaitForLaserRiseTime()
        {
            double riseTime_s =
                    double.Parse(this.instChassis.Query(string.Format("SOUR{0}:CHAN{1}:POW:LEV:RIS?", this.Slot, this.SubSlot), this));
            if (riseTime_s > 0)
            {
                int riseTime_ms = (int)(riseTime_s * 1000.0);
                System.Threading.Thread.Sleep(riseTime_ms);
            }
        }

        #endregion

        #region InstType_TunableLaserSource Overrides

        /// <summary>
        /// 
        /// </summary>
        public override bool BeamEnable
        {
            get
            {
                bool beamState = false;
                string myQuery = string.Format("OUTP{0}:CHAN{1}:STAT?", this.Slot, this.SubSlot);

                beamState = (Convert.ToInt32(this.instChassis.Query(myQuery, this)) > 0);

                return beamState;
            }
            set
            {
                /*
				 * CHECK SIGNAL-LOCK STATE OF THE TLS. ATTEMPT UNLOCK USING DEFAULT PASSWORD IF IT IS LOCKED
				 * -----------------------------------------------------------------------------------------
				 */

                if (Convert.ToInt32(this.instChassis.Query(":LOCK?", this)) == 1)
                {
                    /* signal lock engaged, attempt to unlock with the default password (1234) */
                    this.instChassis.Write(":LOCK 0, 1234", this, true, 10, 100);

                    /* if the instrument is still locked, throw a signal-lock exception */
                    if (Convert.ToInt32(this.instChassis.Query(":LOCK?", this)) != 0)
                    {
                        throw new InstrumentException("The signal-lock could not be disabled. The unlock process expects the instrument's " +
                            "signal-lock password to be set to the default (1234)");
                    }
                }

                /*
                 * CHANGE THE BEAM ENABLE STATE FOR THE SOURCE
                 * -------------------------------------------
                 */
                string myCommand = string.Format("OUTP{0}:CHAN{1}:STAT {2}", this.Slot, this.SubSlot, ((value == true) ? "1" : "0"));
                this.instChassis.Write(myCommand, this, true, 10, 100);

                /*
                 * WAIT UNTIL THE LASER-ON RISE TIME CONDITION HAS BEEN SATISFIED
                 * --------------------------------------------------------------
                 */
                this.WaitForLaserRiseTime();

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MaximumOuputPower
        {
            get
            {
                string myQuery = string.Format("SOUR{0}:CHAN{1}:POW? MAX", this.Slot, this.SubSlot);
                double power = Convert.ToDouble(this.instChassis.Query(myQuery, this));
                return power;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MaximumWavelengthINnm
        {
            get
            {
                //read back the maximum settable wavelength (value returned in metres so need to multiply by 1e+9)...
                string myQuery = string.Format("SOUR{0}:CHAN{1}:WAV? MAX", this.Slot, this.SubSlot);
                double wavelength = Convert.ToDouble(this.instChassis.Query(myQuery, this)) * 1000000000;
                return wavelength;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MinimumOutputPower
        {
            get
            {
                string myQuery = string.Format("SOUR{0}:CHAN{1}:POW? MIN", this.Slot, this.SubSlot);
                double power = Convert.ToDouble(this.instChassis.Query(myQuery, this));
                return power;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double MinimumWavelengthINnm
        {
            get
            {
                //read back the minimum settable wavelength (value returned in metres so need to multiply by 1e+9)...
                string myQuery = string.Format("SOUR{0}:CHAN{1}:WAV? MIN", this.Slot, this.SubSlot);
                double wavelength = Convert.ToDouble(this.instChassis.Query(myQuery, this)) * 1000000000;
                return wavelength;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double Power
        {
            get
            {
                string myQuery = string.Format("SOUR{0}:CHAN{1}:POW?", this.Slot, this.SubSlot);
                double power = Convert.ToDouble(this.instChassis.Query(myQuery, this));
                return power;
            }
            set
            {
                /*
				 * SET THE POWER LEVEL IN dBm
				 */
                this.instChassis.Write(string.Format("SOUR{0}:CHAN{1}:POW {2}", this.Slot, this.SubSlot, value.ToString()), this, true, 100, 100);

                /*
                 * WAIT UNTIL THE LASER-ON RISE TIME CONDITION HAS BEEN SATISFIED
                 * --------------------------------------------------------------
                 */
                this.WaitForLaserRiseTime();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                EnPowerUnits units = EnPowerUnits.Unspecified;

                string myQuery = string.Format("SOUR{0}:CHAN{1}:POW:UNIT?", this.Slot, this.SubSlot);
                string response = this.instChassis.Query(myQuery, this);

                int unitsId = int.MinValue;
                if (int.TryParse(response, out unitsId))
                {
                    if (unitsId == 0) units = EnPowerUnits.dBm;
                    else if (unitsId == 1) units = EnPowerUnits.mW;
                }

                return units;
            }
            set
            {
                if (value == EnPowerUnits.Unspecified) throw new Exception("unspecified is not a valid type of unit to be set!");
                else
                {
                    string myCommand = string.Format("SOUR{0}:CHAN{1}:POW:UNIT {2}", this.Slot, this.SubSlot,
                        ((value == EnPowerUnits.dBm) ? "DBM" : "W"));
                    this.instChassis.Write(myCommand, this, true, 10, 100);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override double WavelengthINnm
        {
            get
            {
                //read back the output wavelength (returned in metres so need to multiply by 1e+9)...
                string myQuery = string.Format("SOUR{0}:CHAN{1}:WAV?", this.Slot, this.SubSlot);
                double wavelength = Convert.ToDouble(this.instChassis.Query(myQuery, this)) * 1000000000;
                return wavelength;
            }
            set
            {
                this.instChassis.Write(
                    string.Format("SOUR{0}:CHAN{1}:WAV {2}NM", this.Slot, this.SubSlot, value.ToString()), this, true, 10, 100);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string response = this.instChassis.Query(string.Format(":SLOT{0}:IDN?", this.Slot), this);
                string[] idParts = response.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                return idParts[3].Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // check if the instrument channel specified is actually there!
                this.CheckSlotNotEmpty();

                // Read the SLOT IDN? string which has 4 comma seperated substrings                
                string command = String.Format(":SLOT{0}:IDN?", this.Slot);
                string resp = this.instChassis.Query(command, this);
                if (resp.Length == 0)
                {
                    string errStr = String.Format("No 816x TLS available in '{0}' at {1}:{2}", this.ChassisName, this.Slot, this.SubSlot);
                    throw new InstrumentException(errStr);
                }
                // Model Nbr after 1st comma
                string hwID = resp.Split(',')[1];

                return hwID;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void SetDefaultState()
        {
            this.Timeout_ms = 5000;
            this.WavelengthINnm = 1540.0;
            this.PowerUnits = EnPowerUnits.dBm;
            this.Power = 0;
            this.BeamEnable = false;
        }

        #endregion
    }
}
