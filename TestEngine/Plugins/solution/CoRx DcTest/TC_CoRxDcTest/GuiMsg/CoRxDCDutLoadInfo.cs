using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Dut information includes : Part code, sn, test stage and DeviceType information
    /// </summary>
    internal class CoRxDCDutLoadInfo
    {
        readonly  string partCode;
        readonly  string dutSerialNumber;        
        private string testStage;
        string  deviceType;
        string lotType;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SerialNum"> Dut SN</param>
        /// <param name="partCode"> Part Code In FWs </param>
        public CoRxDCDutLoadInfo(string SerialNum, string partCode)
        {
            this.partCode = partCode;
            dutSerialNumber = SerialNum;
            deviceType = "";           
        }

        /// <summary>
        /// Get Part Code in FWs
        /// </summary>
        public string PartCode
        {
            get { return partCode; }
        }
        /// <summary>
        /// Get dut SN
        /// </summary>
        public string DutSerialNumber
        {
            get { return dutSerialNumber; }            
        }
        /// <summary>
        /// Get/Set stage information
        /// </summary>
        public string TestStage
        {
            get { return testStage; }
            set { testStage = value; }
        }
        /// <summary>
        /// Get all devicetype available to the generic code
        /// </summary>
        public string DeviceType
        {
            get { return deviceType; }
            set { deviceType = value; }
        }
        /// <summary>
        /// get/set lot type information
        /// </summary>
        public string LotType
        {
            get
            { return lotType; }
            set
            { lotType = value; }
        }
    }
}
