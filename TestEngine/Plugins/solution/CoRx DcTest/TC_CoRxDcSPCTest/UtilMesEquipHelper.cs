﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.TestControl;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.PluginInterfaces.TestControl;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Config;


namespace Bookham.TestSolution.TestControl
{
    public static class EquitpmentReader
    {
        private static string ReadEquipID(ITestEngine engine, string filePath, string nodeID, string jigID)
        {
            ConfigDataAccessor config = new ConfigDataAccessor(filePath, "Table_EquipID");

            try
            {
                StringDictionary configKeys = new StringDictionary();
                configKeys["NodeID"] = nodeID;
                configKeys["JigID"] = jigID;

                return config.GetData(configKeys, true).ReadString("EquipID");
            }
            catch
            {
                engine.ErrorRaise(string.Format("Couldn't find EquipID by [NodeID: {0}, JigID: {1}] from file {2}", nodeID, jigID, filePath));
            }

            return null;
        }

        private static string ReadEquipID(ITestEngine engine, string filePath, string nodeID)
        {
            return ReadEquipID(engine, filePath, nodeID, "N/A");
        }

        /// <summary>
        /// Read EquipmentID method
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        public static string ReadEquipment(ITestEngine engine, DUTObject dutObject)
        {
            string equipID = ReadEquipID(engine, @"Configuration\EQUIP_ID_Config.xml", dutObject.NodeID.ToString());
            return equipID;
        }
    }
}
