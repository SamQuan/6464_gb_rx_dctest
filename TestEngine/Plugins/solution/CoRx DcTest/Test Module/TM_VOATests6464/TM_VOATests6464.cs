// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TM_VOATests.cs
//
// Author: dave.l.smith, 2014
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestLibrary.Utilities;
using System.Threading;
using System.IO;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Test Module Template.
    /// </summary>
    public class TM_VOATests6464 : ITestModule
    {
        #region ITestModule Members
        private const double speedoflight = 299792458.0;
        private const int sweepTime_ms = 20000;
        private const int progressBarLevels = 20;
        private const double PdlPolyFitResolution = 0.1d;
        private const int PdlPolyFitOrder = 5;

        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {

            // return data
            CoRxVOASweep StartFreqData = new CoRxVOASweep(), StopFreqData = new CoRxVOASweep(), CurrentFreqData = null; ;
            DatumList returnData = new DatumList();
            int idx = 0;
            engine.GuiShow();
            engine.GuiToFront();
            Inst_Ke24xx masterSource = (Inst_Ke24xx)instruments["Master Source"];

            double Vpd_V = configData.ReadDouble("PDBias_V");
            double Ipd_Compliance_A = configData.ReadDouble("PDCompCurrent_A");
            double Vcc_V = configData.ReadDouble("TiaBias_V");
            double Icc_Compliance_A = configData.ReadDouble("TIACompCurrent_A");
            //bool isPdSourceUseTrigger = configData.ReadBool("IsPdSourceUseTrigger");

            double VOA_Compliance_A = configData.ReadDouble("VOACompCurrent_A");
            double VOA_Volts_Start = configData.ReadDouble("VOASweepStartVoltage_V");
            double VOA_Volts_Stop = configData.ReadDouble("VOASweepStopVoltage_V");
            double VOA_Volts_Step = configData.ReadDouble("VOASweepStepVoltage_V");

            // Set-up attenuation values for VOA Gradient Calculation.
            // High value is either 80% of MAx attenuation or some fixed attenuation value defined in Config file

            CoRxVOASweep.UseFixedMaxAttenForGradientCalculation = configData.ReadBool("UseFixedAttenForGradientMax");
            if (CoRxVOASweep.UseFixedMaxAttenForGradientCalculation) CoRxVOASweep.FixedAttenuationForGradientCalc = configData.ReadDouble("AttenForGradientCalc");


            string fileDirectory = configData.ReadString("BlobFileDirectory");
            string filePrefix_VOAData = configData.ReadString("FilePrefix4VOA");
            string filePrefix_PDLData = configData.ReadString("FilePrefix4PDL");
            string filePrefix_VoaTempComp = configData.ReadString("FilePrefix4VOATempComp");

            //EnumFrequencySetMode freqSetMode = EnumFrequencySetMode.ByFrequency;
            double freq_Min = 0;
            double freq_Max = 0;
            double freq_Step = 0;

            freq_Min = configData.ReadDouble("Freq_Start_GHz");
            freq_Max = configData.ReadDouble("Freq_Stop_GHz");
            freq_Step = configData.ReadDouble("Freq_Step_GHz");


            // Need to check what to do here
            #region Get opc power information
            bool isAdjustSigOpcPower = configData.ReadBool("IsAdjustSigOpcPower");
            double rqsSigOpcPower_DBm;
            if (isAdjustSigOpcPower)
                rqsSigOpcPower_DBm = configData.ReadDouble("RqsSigOpcPower_DBm");
            else
                rqsSigOpcPower_DBm = double.PositiveInfinity;

            bool isAdjustLocOpcPower = configData.ReadBool("IsAdjustLocOpcPower");
            double rqsLocOpcPower_DBm;
            if (isAdjustLocOpcPower)
                rqsLocOpcPower_DBm = configData.ReadDouble("RqsLocOpcPower_DBm");
            else
                rqsLocOpcPower_DBm = double.PositiveInfinity;

            #endregion


            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());

            double VOA_Max_Current = -double.MaxValue;
            // Power up the device

            CoRxDCTestInstruments.SetupPdSourceTriggerImm();            // Set all PD sources to Immediate trigger

            double VPdTap_V = 0;
            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                double IPdTap_Comp_A = configData.ReadDouble("PDTapCompCurrent_A");
                VPdTap_V = configData.ReadDouble("PDTapBias_V");
                CoRxDCTestInstruments.PdTapSource.InitSourceVMeasureI_MeasurementAccuracy(
                    IPdTap_Comp_A, IPdTap_Comp_A, false, 0, 1, 1, true);
                CoRxDCTestInstruments.PdTapSource.VoltageRange_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.VoltageSetPoint_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(100);

            }

            Inst_CoRxITLALaserSource itlaSource_Sig = (Inst_CoRxITLALaserSource)CoRxDCTestInstruments.SignalInputInstrs.LaserSource;
            if (itlaSource_Sig != null) CoRxDCTestInstruments.SelectItla(itlaSource_Sig);

            //CoRxDCTestInstruments.InitAllPdSourcesAsVoltSourceWithAvg(Vpd_V, Ipd_Compliance_A,4);
            CoRxDCTestInstruments.InitAllPdSourcesAsVoltSource(Vpd_V, Ipd_Compliance_A);
            CoRxDCTestInstruments.SetAllPdBias(Vpd_V, Ipd_Compliance_A);
            CoRxDCTestInstruments.SetAllPDSourceOutput(true);
            CoRxDCTestInstruments.SetTIABias(Vcc_V, Icc_Compliance_A);
            CoRxDCTestInstruments.setAllTiaSourceOutput(true);
            CoRxDCTestInstruments.SetupTIA();
            CoRxDCTestInstruments.VOASource.CurrentComplianceSetPoint_Amp = VOA_Compliance_A;
            CoRxDCTestInstruments.VOASource.SourceVoltageRange = VOA_Volts_Stop;
            CoRxDCTestInstruments.VOASource.SenseCurrent(VOA_Compliance_A, VOA_Compliance_A);
            CoRxDCTestInstruments.VOASource.CurrentRange_Amp = VOA_Compliance_A;
            CoRxDCTestInstruments.VOASource.VoltageSetPoint_Volt = 0;
            CoRxDCTestInstruments.VOASource.FourWireSense = true;
            //            CoRxDCTestInstruments.PdSource_YI1.UseFrontTerminals = true;
            //            CoRxDCTestInstruments.PdSource_YQ1.UseFrontTerminals = true;
            CoRxDCTestInstruments.VOASource.OutputEnabled = true;

            //CoRxDCTestInstruments.SetAllControlLevel(V_MC_V, Icomp_MC_A, V_GC_V, Icomp_GC_A,
            //    V_OC_V, Icomp_OC_A, V_OA_V, Icomp_OA_A, I_ThermPhase_A, Vcomp_ThermPhase_V);
            GuiMsg.VOASweepProgress SweepProgressInfo = new GuiMsg.VOASweepProgress();



            CurrentFreqData = StartFreqData;
            for (double freq = freq_Min; freq <= freq_Max; freq += freq_Step)
            {
                SweepProgressInfo.SweepMsg = string.Format("Sweeping VOA voltage at {0:####}nm", speedoflight / freq);
                SweepProgressInfo.Progress = 0;
                engine.SendToGui(SweepProgressInfo);

                //List<CoRxVOASweepData> VoaSweepdataList = new List<CoRxVOASweepData>(); 
                CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz = freq;
                CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = true;

                //Set Optical power to 10dBm during voa sweep
                double targetPower_dBm = 10;
                double acceptableThreashold_dBm = 0.05;
                int maxIterations = 5;
                double currentPower_dBm = CoRxDCTestInstruments.SignalInputInstrs.GetOpticalPower_dBm();

                for (int i = 1; i <= maxIterations; i++)
                {
                    double delta = targetPower_dBm - currentPower_dBm;
                    double newSetPt_dBm = itlaSource_Sig.Power_dBm + delta;
                    itlaSource_Sig.Power_dBm = newSetPt_dBm;
                    Thread.Sleep(500);
                    currentPower_dBm = CoRxDCTestInstruments.SignalInputInstrs.GetOpticalPower_dBm();
                    if (Math.Abs(currentPower_dBm - targetPower_dBm) < acceptableThreashold_dBm)
                    {

                        break;

                    }
                    else if (i == maxIterations) {

                        engine.ErrorInModule("Could not set appropriate optical power during VOA sweep. Last power achieved: " + currentPower_dBm + "dBm");
                    
                    }

                }




                System.Threading.Thread.Sleep(500);                 // Wait for things to stabilise

                engine.SendToGui(new DatumString("", string.Format("Sweeping VOA voltage at {0:####}nm", speedoflight / freq)));

                CoRxVOASweepData[] TestDataList = null;

                //double MaxXAttenuation = -double.MaxValue;
                //double MaxYAttenuation = -double.MaxValue;
                // Loop round the voltages on the VOA measuring the appropriate PDs

                double LastAttenuation = 2;
                int sweepPoints = (int)Math.Abs((VOA_Volts_Stop - VOA_Volts_Start) / VOA_Volts_Step) + 1;

                Thread VoaTestThread = new Thread(new ParameterizedThreadStart(ConfigureAndRunVoaSweep));
                VoaTestThread.Start(new object[] { masterSource, VOA_Volts_Start, VOA_Volts_Stop, sweepPoints, Vpd_V, VPdTap_V });

                //Update progress bar while waiting for thread to return
                do
                {
                    int sleepTime_ms = sweepTime_ms / progressBarLevels;
                    Thread.Sleep(sleepTime_ms);
                    double progressIncrement = 100d / progressBarLevels;
                    SweepProgressInfo.Progress = Math.Min(100d - progressIncrement, SweepProgressInfo.Progress + progressIncrement);
                    engine.SendToGui(SweepProgressInfo);

                } while (VoaTestThread.IsAlive);
                SweepProgressInfo.Progress = 100;
                engine.SendToGui(SweepProgressInfo);
                TestDataList = CoRxDCTestInstruments.FetchVoaSweepResults();


                //debug

                /*
                List<double> x = new List<double>();
                List<double> y = new List<double>();
                List<double> v = new List<double>();
                List<double> xatt = new List<double>();
                List<double> yatt = new List<double>();

                foreach (CoRxVOASweepData TestData in TestDataList)
                {

                    x.Add(TestData.XIPCurrent_mA);
                    y.Add(TestData.YIPCurrent_mA);
                    v.Add(TestData.VoaVoltage_V);

                    xatt.Add(TestData.XIPAttenuation_dB);
                    yatt.Add(TestData.YIPAttenuation_dB);
                }


                CoRxDCTestInstruments.PdSource_XI1.CleanUpSweep();
                CoRxDCTestInstruments.PdSource_YI1.CleanUpSweep();

                //CoRxDCTestInstruments.PdSource_XQ1.CleanUpSweep();
                //CoRxDCTestInstruments.PdSource_YQ1.CleanUpSweep();
               double p = CoRxDCTestInstruments.PdSource_XQ1.CurrentActual_amp;
               p= CoRxDCTestInstruments.PdSource_YQ1.CurrentActual_amp;

                x = new List<double>();
                y = new List<double>();
                v = new List<double>();

                double aa = itlaSource_Sig.Power;
                itlaSource_Sig.Power = 12.315;
                double test2 = CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm;
                CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm = 1552.12;


                System.Threading.Thread.Sleep(4000);

                for (double vv = 0; vv < 9.001; vv += 0.1)
                {

                    vv = Math.Round(vv * 10) / 10;

                    CoRxDCTestInstruments.VOASource.VoltageSetPoint_Volt = vv;
                    System.Threading.Thread.Sleep(75);
                    x.Add(CoRxDCTestInstruments.PdSource_XI1.CurrentActual_amp * 1000);
                    y.Add(CoRxDCTestInstruments.PdSource_YI1.CurrentActual_amp * 1000);
                    v.Add(vv);
                    

                }
              
                */

                foreach (CoRxVOASweepData TestData in TestDataList)
                {

                    if (TestData.XIPAttenuation_dB > 2.5)
                    {
                        if (LastAttenuation > TestData.XIPAttenuation_dB)
                        {
                            // int iStop = 100;
                        }
                        else LastAttenuation = TestData.XIPAttenuation_dB;
                    }
                    if (TestData.VOACurrent_mA > VOA_Max_Current) VOA_Max_Current = TestData.VOACurrent_mA;
                    CurrentFreqData.Add(TestData);
                }

                double forinfo = CurrentFreqData.MaxXAttenuation;
                double forinfoY = CurrentFreqData.MaxYAttenuation;


                // We have completed sweep at this Frequency so extract data and save results
                returnData.AddDouble((idx == 0 ? "MaxAtten_X_StartFreq" : "MaxAtten_X_StopFreq"), CurrentFreqData.MaxXAttenuation);
                returnData.AddDouble((idx == 0 ? "MaxAtten_Y_StartFreq" : "MaxAtten_Y_StopFreq"), CurrentFreqData.MaxYAttenuation);


                returnData.AddDouble((idx == 0 ? "VOAVoltage10DB_X_StartFreq" : "VOAVoltage10DB_X_StopFreq"), CurrentFreqData.VoltageAtAttenuation_X(10.0));
                returnData.AddDouble((idx == 0 ? "VOAVoltage10DB_Y_StartFreq" : "VOAVoltage10DB_Y_StopFreq"), CurrentFreqData.VoltageAtAttenuation_Y(10.0));

                
                returnData.AddDouble(idx == 0 ? "VOAMaxPDL_StartFreq10" : "VOAMaxPDL_StopFreq10" , CurrentFreqData.GetMaxPdl(10));
                returnData.AddDouble(idx == 0 ? "VOAMaxPDL_StartFreq15" : "VOAMaxPDL_StopFreq15" , CurrentFreqData.GetMaxPdl(15));

                /*
            
                // pdl up to 10dB attenuation
                CurrentFreqData.CreatePdlPolyFit(10, PdlPolyFitOrder);
                returnData.AddDouble((idx == 0 ? "VOAMaxPDL_StartFreq_10dB" : "VOAMaxPDL_StopFreq_10dB"), CurrentFreqData.GetMaxFromFit(10, PdlPolyFitResolution));
                returnData.AddDouble((idx == 0 ? "PDLMaxPolyFitDeviation_StartFreq_10dB" : "PDLMaxPolyFitDeviation_StopFreq_10dB"), CurrentFreqData.MaximumDeviationFromPdlFit(10));
                fileName = CurrentFreqData.WritePdlBlobData(filePrefix_PDLData + string.Format("{0:####}NM_10dB", speedoflight / freq), fileDirectory, 10, PdlPolyFitResolution);
                returnData.AddFileLink((idx == 0 ? "PDLSweepDataFileStart_10dB" : "PDLSweepDataFileStop_10dB"), fileName);
                */
                //write voa sweep data to file
                
                string fileName = CurrentFreqData.WriteData6464(filePrefix_VOAData + string.Format("{0:####}NM", speedoflight / freq), fileDirectory);
                returnData.AddFileLink((idx == 0 ? "VOASweepDataFileStart" : "VOASweepDataFileStop"), fileName);

                ++idx;
                CurrentFreqData = StopFreqData;
            }

            double debug2 = itlaSource_Sig.Power;

            if (previousTestData.Count > 0)
            {

                //means there is information about normal temperature Voa Sweep
                double NormalTemp = previousTestData.ReadDouble("Temp");
                double CurrentTemp = configData.ReadDouble("Temp");

                string VOASweepDataFileStartNormalTemp = previousTestData.ReadFileLinkFullPath(string.Format("VOASweepDataFileStart{0}C", NormalTemp.ToString().Replace('-', 'M')));
                string VOASweepDataFileStopNormalTemp = previousTestData.ReadFileLinkFullPath(string.Format("VOASweepDataFileStop{0}C", NormalTemp.ToString().Replace('-', 'M')));

                string VOASweepDataFileStartCurrTemp = returnData.ReadFileLinkFullPath("VOASweepDataFileStart");
                string VOASweepDataFileStopCurrTemp = returnData.ReadFileLinkFullPath("VOASweepDataFileStop");

                double VOA_HSPIN_TempComp = -9999.0, VOA_MPD_TempComp = -9999.0;
                double[,] dataTable = CalculateTempCompSlope(VOASweepDataFileStartNormalTemp, VOASweepDataFileStopNormalTemp, NormalTemp, VOASweepDataFileStartCurrTemp, VOASweepDataFileStopCurrTemp, CurrentTemp, out VOA_HSPIN_TempComp, out VOA_MPD_TempComp);
                //string filename = WriteTempCompBlobData(filePrefix_VoaTempComp, fileDirectory, dataTable, NormalTemp, CurrentTemp, string.Format("{0:####}", speedoflight / freq_Min), string.Format("{0:####}", speedoflight / freq_Max));

                returnData.AddDouble("VOA_HSPIN_TempComp", VOA_HSPIN_TempComp);
                returnData.AddDouble("VOA_MPD_TempComp", VOA_MPD_TempComp);
                //returnData.AddFileLink("VOA_TEMP_COMP_FILE", filename);

            }

            returnData.AddDouble("MaxVoaCurrent", VOA_Max_Current);

            returnData.AddDouble("AttenGradient_X_StartFreq", StartFreqData.AttenuationGradient_X);
            returnData.AddDouble("AttenGradient_Y_StartFreq", StartFreqData.AttenuationGradient_Y);

            returnData.AddDouble("AttenGradient_X_StopFreq", StopFreqData.AttenuationGradient_X);
            returnData.AddDouble("AttenGradient_Y_StopFreq", StopFreqData.AttenuationGradient_Y);

            // Set dummy values for extracted data
            //     VOA_Volts_Stop - VOA_Volts_Start) / VOA_Volts_Step ,string.Format("VOA_WDL_X_{0:0.0}V", 1.5).Replace(".","_")
            Double actVolt = VOA_Volts_Start ;
            for (double Voltage = VOA_Volts_Start; Voltage < 10; Voltage = Voltage + 1)
            {
     //           returnData.AddDouble(string.Format("AttenAverage{0:0}VStartFreq", Voltage),
      //              CoRxVOASweep.AverageAttenuation(StartFreqData.AttenuationAtVoltage_X(Voltage), StartFreqData.AttenuationAtVoltage_Y(Voltage)));
                returnData.AddDouble(string.Format("AttenMPD{0:0}VStartFreq", Voltage), StartFreqData.AttenuationAtVoltage_MPD(Voltage));
      //          returnData.AddDouble(string.Format("AttenAverage{0:0}VStopFreq", Voltage),
      //              CoRxVOASweep.AverageAttenuation(StopFreqData.AttenuationAtVoltage_X(Voltage), StopFreqData.AttenuationAtVoltage_Y(Voltage)));
                returnData.AddDouble(string.Format("AttenMPD{0:0}VStopFreq", Voltage), StopFreqData.AttenuationAtVoltage_MPD(Voltage));
                
            }

            if (VOA_Volts_Step>=1)
            {
                VOA_Volts_Step /= 10.0;
            }
            for (double Voltage = VOA_Volts_Start; Voltage < VOA_Volts_Stop + .1; Voltage = Voltage + (VOA_Volts_Step * 10))
            {  
                returnData.AddDouble(string.Format("VOA_WDL_X_{0:G}V", Voltage).Replace(".", "_"), StopFreqData.AttenuationAtVoltage_X(Voltage) - StartFreqData.AttenuationAtVoltage_X(Voltage));
                returnData.AddDouble(string.Format("VOA_WDL_Y_{0:G}V", Voltage).Replace(".", "_"), StopFreqData.AttenuationAtVoltage_Y(Voltage) - StartFreqData.AttenuationAtVoltage_Y(Voltage));
                returnData.AddDouble(string.Format("AttenAverage{0:G}VStartFreq", Voltage).Replace(".", "_"), CoRxVOASweep.AverageAttenuation(StartFreqData.AttenuationAtVoltage_X(Voltage), StartFreqData.AttenuationAtVoltage_Y(Voltage)));
                returnData.AddDouble(string.Format("AttenAverage{0:G}VStopFreq", Voltage).Replace(".", "_"),CoRxVOASweep.AverageAttenuation(StopFreqData.AttenuationAtVoltage_X(Voltage), StopFreqData.AttenuationAtVoltage_Y(Voltage)));
            }

            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.VOASource.OutputEnabled = false;
            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);
            if (CoRxDCTestInstruments.IsBiasPDTap) CoRxDCTestInstruments.PdTapSource.OutputEnabled = false;
            if (CoRxDCTestInstruments.VOASource != null) CoRxDCTestInstruments.VOASource.OutputEnabled = false;

            CoRxDCTestInstruments.ClearUpPdSourceTrigger();

            //returnData.AddString("CHANGE_ME", "Hello Module Data!");
            return returnData;
        }

        public string WriteTempCompBlobData(string filePrefix, string fileDirectory, double[,] dataTable, double temp1, double temp2, string WlStart, string WlStop)
        {

            List<string> data = new List<string>();
            string header = "";
            header += string.Format("Voa Bias {0}C (V),", temp1);
            header += string.Format("HSPIN Avg Att {0}C {1}nm (dB),", temp1, WlStart);
            header += string.Format("HSPIN Avg Att {0}C {1}nm (dB),", temp1, WlStop);
            header += string.Format("HSPIN Avg Att {0}C (dB),", temp1);

            header += string.Format("MPD Att {0}C {1}nm (dB),", temp1, WlStart);
            header += string.Format("MPD Att {0}C {1}nm (dB),", temp1, WlStop);
            header += string.Format("MPD Avg Att {0}C (dB),", temp1);

            header += string.Format("Voa Bias {0}C (V),", temp2);
            header += string.Format("HSPIN Avg Att {0}C {1}nm (dB),", temp2, WlStart);
            header += string.Format("HSPIN Avg Att {0}C {1}nm (dB),", temp2, WlStop);
            header += string.Format("HSPIN Avg Att {0}C (dB),", temp2);

            header += string.Format("MPD Att {0}C {1}nm (dB),", temp2, WlStart);
            header += string.Format("MPD Att {0}C {1}nm (dB),", temp2, WlStop);
            header += string.Format("MPD Avg Att {0}C (dB),", temp2);

            header += string.Format("HSPIN Att Levels (dB),");
            header += string.Format("HSPIN Voltage@Att {0}C (V),", temp1);
            header += string.Format("HSPIN Voltage@Att {0}C (V),", temp2);
            header += string.Format("HSPIN Temp Comp (mV/C),");

            header += string.Format("MPD Att Levels (dB),");
            header += string.Format("MPD Voltage@Att {0}C (V),", temp1);
            header += string.Format("MPD Voltage@Att {0}C (V),", temp2);
            header += string.Format("MPD Temp Comp (mV/C)");

            data.Add(header);

            int coloumns = dataTable.GetLength(1);
            int rows = dataTable.GetLength(0);

            for (int row = 0; row < rows; row++)
            {
                string csvLine = "";
                for (int i = 0; i < coloumns; i++)
                {

                    csvLine += string.Format("{0}{1}", dataTable[row, i], i == (coloumns - 1) ? "" : ",");

                }
                data.Add(csvLine);
            }

            string fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix, "", "CSV");
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(fileName, data);
            }

            return fileName;

        }

        void ConfigureAndRunVoaSweep(object par)
        {
            object[] pars = (object[])par;
            Inst_Ke24xx masterSource = (Inst_Ke24xx)pars[0];
            double VOA_Volts_Start = (double)pars[1];
            double VOA_Volts_Stop = (double)pars[2];
            int sweepPoints = (int)pars[3];
            double Vpd_V = (double)pars[4];
            double VPdTap_V = (double)pars[5];

            CoRxDCTestInstruments.ConfigureAndRunVoaSweep(masterSource, VOA_Volts_Start, VOA_Volts_Stop, sweepPoints, Vpd_V, VPdTap_V);
        }

        public Type UserControl
        {
            get { return (typeof(TM_VOATestsGui)); }
        }



        #endregion

        private double[] GetAverageAttenuationArray(double[] Avg_Att_x_dB, double[] Avg_Att_y_dB)
        {

            if (Avg_Att_x_dB.Length != Avg_Att_y_dB.Length)
            {

                throw new Exception("X and Y array have different lengths");

            }

            double[] result = new double[Avg_Att_x_dB.Length];

            for (int i = 0; i < Avg_Att_x_dB.Length; i++)
            {

                result[i] = AverageAttenuation(Avg_Att_x_dB[i], Avg_Att_y_dB[i]);

            }
            return result;

        }

        //used to average any two "db" values
        private double AverageAttenuation(double attenuationX_db, double attenuationY_db)
        {
            double LinearAttenuationX = Math.Pow(10.0, attenuationX_db / 10);
            double LinearAttenuationY = Math.Pow(10.0, attenuationY_db / 10);

            return (10 * Math.Log10(Math.Abs(LinearAttenuationX + LinearAttenuationY) / 2));
        }

        private double[,] CalculateTempCompSlope(string FilePathTemp1Start, string FilePathTemp1Stop, double Temp1, string FilePathTemp2Start, string FilePathTemp2Stop, double Temp2, out double HSPIN_Comp, out double MPD_Comp)
        {

            double[] VoaBiasTemp1 = GetColoumn(FilePathTemp1Start, "VOA Bias (V)");
            double[] AvgAttTemp1Start = GetColoumn(FilePathTemp1Start, "Average Attenuation (XI and YI) (dB)");
            double[] AvgAttTemp1Stop = GetColoumn(FilePathTemp1Stop, "Average Attenuation (XI and YI) (dB)");
            double[] MPDAttTemp1Start = GetColoumn(FilePathTemp1Start, "MPD Attenuation (dB)");
            double[] MPDAttTemp1Stop = GetColoumn(FilePathTemp1Stop, "MPD Attenuation (dB)");

            double[] VoaBiasTemp2 = GetColoumn(FilePathTemp2Start, "VOA Bias (V)");
            double[] AvgAttTemp2Start = GetColoumn(FilePathTemp2Start, "Average Attenuation (XI and YI) (dB)");
            double[] AvgAttTemp2Stop = GetColoumn(FilePathTemp2Stop, "Average Attenuation (XI and YI) (dB)");
            double[] MPDAttTemp2Start = GetColoumn(FilePathTemp2Start, "MPD Attenuation (dB)");
            double[] MPDAttTemp2Stop = GetColoumn(FilePathTemp2Stop, "MPD Attenuation (dB)");


            double[] AvgAttTemp1 = GetAverageAttenuationArray(AvgAttTemp1Start, AvgAttTemp1Stop);
            double[] AvgAttTemp2 = GetAverageAttenuationArray(AvgAttTemp2Start, AvgAttTemp2Stop);
            double[] MPDAttTemp1 = GetAverageAttenuationArray(MPDAttTemp1Start, MPDAttTemp1Stop);
            double[] MPDAttTemp2 = GetAverageAttenuationArray(MPDAttTemp2Start, MPDAttTemp2Stop);


            double[] AttLevels = new double[] { 2, 4, 6, 8, 10, 12, 14, 16, 18 };
            double[] MPD_AttLevels = new double[] { 2, 4, 6, 8 };

            double[] VoltageAtAttenuationArrayTemp1 = VoltageAtAttenuationArray(AttLevels, VoaBiasTemp1, AvgAttTemp1);
            double[] VoltageAtAttenuationArrayTemp2 = VoltageAtAttenuationArray(AttLevels, VoaBiasTemp2, AvgAttTemp2);

            double[] MPD_VoltageAtAttenuationArrayTemp1 = VoltageAtAttenuationArray(MPD_AttLevels, VoaBiasTemp1, MPDAttTemp1);
            double[] MPD_VoltageAtAttenuationArrayTemp2 = VoltageAtAttenuationArray(MPD_AttLevels, VoaBiasTemp2, MPDAttTemp2);

            HSPIN_Comp = (1000.0d * AverageArray(SubtractArray(VoltageAtAttenuationArrayTemp2, VoltageAtAttenuationArrayTemp1))) / (Temp2 - Temp1);
            MPD_Comp = (1000.0d * AverageArray(SubtractArray(MPD_VoltageAtAttenuationArrayTemp2, MPD_VoltageAtAttenuationArrayTemp1))) / (Temp2 - Temp1);

            //create table with all these results
            double[,] dataTable = CoverOneToTwoDimensionArray(VoaBiasTemp1);
            dataTable = MergeArray(dataTable, AvgAttTemp1Start);
            dataTable = MergeArray(dataTable, AvgAttTemp1Stop);
            dataTable = MergeArray(dataTable, AvgAttTemp1);
            dataTable = MergeArray(dataTable, MPDAttTemp1Start);
            dataTable = MergeArray(dataTable, MPDAttTemp1Stop);
            dataTable = MergeArray(dataTable, MPDAttTemp1);

            dataTable = MergeArray(dataTable, VoaBiasTemp2);
            dataTable = MergeArray(dataTable, AvgAttTemp2Start);
            dataTable = MergeArray(dataTable, AvgAttTemp2Stop);
            dataTable = MergeArray(dataTable, AvgAttTemp2);
            dataTable = MergeArray(dataTable, MPDAttTemp2Start);
            dataTable = MergeArray(dataTable, MPDAttTemp2Stop);
            dataTable = MergeArray(dataTable, MPDAttTemp2);

            dataTable = MergeArray(dataTable, AttLevels);
            dataTable = MergeArray(dataTable, VoltageAtAttenuationArrayTemp1);
            dataTable = MergeArray(dataTable, VoltageAtAttenuationArrayTemp2);
            dataTable = MergeArray(dataTable, new double[] { HSPIN_Comp });

            dataTable = MergeArray(dataTable, MPD_AttLevels);
            dataTable = MergeArray(dataTable, MPD_VoltageAtAttenuationArrayTemp1);
            dataTable = MergeArray(dataTable, MPD_VoltageAtAttenuationArrayTemp2);
            dataTable = MergeArray(dataTable, new double[] { MPD_Comp });

            return dataTable;


        }

        double[,] CoverOneToTwoDimensionArray(double[] A)
        {

            double[,] result = new double[A.Length, 1];
            for (int i = 0; i < A.Length; i++)
            {

                result[i, 0] = A[i];
            }
            return result;

        }

        double[,] MergeArray(double[,] A, double[] B)
        {

            int rowsA = A.GetLength(0);
            int coloumnsA = A.GetLength(1);
            int rowsB = B.GetLength(0);

            double[,] result = new double[Math.Max(rowsA, rowsB), coloumnsA + 1];

            for (int i = 0; i < rowsA; i++)
            {

                for (int j = 0; j < coloumnsA; j++)
                {

                    result[i, j] = A[i, j];

                }


            }
            for (int i = 0; i < rowsB; i++)
            {


                result[i, coloumnsA] = B[i];
            }


            return result;

        }

        double AverageArray(double[] a)
        {

            double sum = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sum += a[i];
            }

            return (sum / (double)a.Length);

        }

        double[] SubtractArray(double[] a, double[] b)
        {

            CheckArrayLength(a, b);
            double[] result = new double[a.Length];

            for (int i = 0; i < a.Length; ++i)
            {

                result[i] = a[i] - b[i];

            }
            return result;
        }

        private void CheckArrayLength(double[] a, double[] b)
        {

            if (a.Length != b.Length) throw new Exception("array lengths are not equal");

        }

        private double VoltageAtAttenuation(double attenuationValue, double[] VoaBias, double[] AvgAttenuation)
        {
            double Voltage = -999.0;


            if (VoaBias.Length != AvgAttenuation.Length)
            {

                throw new Exception("voltage at attenuation exception");
            }


            for (int idx = 0; idx < VoaBias.Length; ++idx)
            {
                if (AvgAttenuation[idx] > attenuationValue)
                {
                    Voltage = VoaBias[idx - 1] + (attenuationValue - AvgAttenuation[idx - 1]) *
                            (VoaBias[idx] - VoaBias[idx - 1]) / (AvgAttenuation[idx] - AvgAttenuation[idx - 1]);
                    break;
                }
            }

           // if (Voltage < -998d) throw new Exception("Cannot find average attenuation higher than " + attenuationValue + " dB");

            return Voltage;
        }

        private double[] VoltageAtAttenuationArray(double[] AttLevels, double[] VoaBias, double[] AvgAttenuation)
        {
            double[] result = new double[AttLevels.Length];

            for (int j = 0; j < AttLevels.Length; j++)
            {

                result[j] = VoltageAtAttenuation(AttLevels[j], VoaBias, AvgAttenuation);

            }
            return result;
        }


        private double[] GetColoumn(string csvPath, string header)
        {

            var reader = new StreamReader(File.OpenRead(csvPath));
            List<double> data = new List<double>();

            var headerRow = reader.ReadLine();
            var headers = headerRow.Split(',');

            int i;
            for (i = 0; i < headers.Length; i++)
            {

                if (headers[i] == header) break;

            }

            if (i == headers.Length) throw new Exception(string.Format("Could not find the header {0} in {1}", header, csvPath));

            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split(',');
                data.Add(double.Parse(values[i]));
            }
            return data.ToArray();

        }


    }
}
