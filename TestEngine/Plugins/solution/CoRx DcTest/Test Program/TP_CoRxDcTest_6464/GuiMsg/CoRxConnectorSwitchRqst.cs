using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestPrograms.GuiMsg
{
    internal class CoRxConnectorSwitchRqst
    {
        /// <summary>
        /// Promote to switch fibre connector
        /// </summary>
        /// <param name="message"> Text message promote to the GUI </param>
        public CoRxConnectorSwitchRqst(string message)
        { this.message = message; }

        private readonly string message;

        public string Message
        {
            get { return message; }
            //set { message = value; }
        }

    }
}
