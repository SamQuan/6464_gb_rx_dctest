﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bookham.TestSolution.CoRxDCTestCommonData;
using System.Collections.Generic;

namespace CommonUnitTest
{
    [TestClass]
    public class CoRxVOASweepUnitTest
    {
        [TestMethod]
        public void GetValueFromFit()
        {
            CoRxVOASweep VoaSweep = new CoRxVOASweep();
            PopulateSweepWRandomData(VoaSweep);
            double [] xaxis = new double[10];
            VoaSweep.CreatePdlPolyFit(0.2, 5);



            List<double> AvgAttList = new List<double>();

        }

        private void PopulateSweepWRandomData(CoRxVOASweep Sweep) {

            Random rand = new Random();

            for (double i = 0; i<9; i = i + 0.1) {

                double currentdcOff = 100 - (10 * i);

                CoRxVOASweepData data = new CoRxVOASweepData(i, (rand.NextDouble() - 0.5) + (10 * i), currentdcOff - rand.NextDouble(), currentdcOff - rand.NextDouble(), currentdcOff - rand.NextDouble(), currentdcOff - rand.NextDouble(), currentdcOff - rand.NextDouble());
                Sweep.Add(data);

                    }
        }

    }
}
