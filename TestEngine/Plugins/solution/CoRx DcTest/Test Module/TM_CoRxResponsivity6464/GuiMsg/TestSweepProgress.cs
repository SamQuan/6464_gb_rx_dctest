﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestSolution.TestModules.GuiMsg
{
    internal class TestSweepProgress
    {
        private double mProgressPct = 0;

        public TestSweepProgress()
        {

        }


        public double Progress
        {
            get { return mProgressPct; }
            set { mProgressPct = value; }
        }

    }
}
