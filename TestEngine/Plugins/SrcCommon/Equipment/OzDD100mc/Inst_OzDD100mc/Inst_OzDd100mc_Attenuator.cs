// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_OzDd100mc_Attenuator.cs
//
// Author: 
// Design: As specified in OzDD100mc driver design document.

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using NationalInstruments.VisaNS;
using System.IO;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// OzDD100mc optical attenuator driver
    /// </summary>
    public class Inst_OzDd100mc_Attenuator : InstType_OpticalAttenuator
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_OzDd100mc_Attenuator(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Configure valid hardware information

            // Add all OzDd100mc details
            InstrumentDataRecord OzDd100mcInstData = new InstrumentDataRecord(
                "Hardware_Unknown",      // hardware name 
                "Firmware_Unknown",  			                // minimum valid firmware version 
                "Firmware_Unknown");			            // maximum valid firmware version 
            
            OzDd100mcInstData.Add("MinWavelength_nm", "1200");    // minimum wavelength
            OzDd100mcInstData.Add("MaxWavelength_nm", "1650");    // maximum wavelength
            OzDd100mcInstData.Add("MaxAttenuation_dB", "60");     // maximum attenuation

            ValidHardwareData.Add("OzDd100mc", OzDd100mcInstData);

            // Configure valid chassis information
            // Add Chassis_OzDd100mc chassis details
            InstrumentDataRecord OzDd100mcChassisData = new InstrumentDataRecord(
                "Chassis_OzDd100mc",				// chassis driver name  
                "0",							// minimum valid chassis driver version  
                "1.0.0.0");						// maximum valid chassis driver version
            ValidChassisDrivers.Add("OzDd100mc", OzDd100mcChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_OzDd100mc) base.InstrumentChassis;

            

        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            // get the firmware version of the chassis.
            get
            {
               return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            // return the chassis hardware details.
            get
            {
                return instrumentChassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            // Send a reset, *RST, and a cls
            chassisWrite("*RST");       
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region InstType_OpticalAttenuator

        /// <summary>
        /// Set/Return attenuation level (in dB),range from IL to 60dB
        /// need check module whether support this command
        /// </summary>
        public override double Attenuation_dB
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                if (value > attenua_dBm[attenua_dBm.Length - 1] || value < attenua_dBm[0])
                {
                    throw new AlgorithmException("out of range" + attenua_dBm[0].ToString() + " to " + attenua_dBm[attenua_dBm.Length - 1].ToString());
                }
                int index = Alg_ArrayFunctions.FindIndexOfNearestElement(attenua_dBm, value);
                int step = stepForAttenua[index];

                chassisWrite("S" + step.ToString());
            }
        }

        /// <summary>
        /// Set/Return wavelength (in nm)
        /// </summary>
        public override double Wavelength_nm
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                // Set the wavelength (nm)
                string subCmd = "";
                switch((int)value)
                {
                    case 1330:
                        subCmd = "0";
                        break;
                    case 1550:
                        subCmd = "1";
                        break;
                    default :
                        throw new InstrumentException("Invalid wavelength");
                }
                string cmd = "W" + subCmd;
                chassisWrite(cmd);
            }
        }

        /// <summary>
        /// The method or operation is not implemented
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }

        /// <summary>
        /// The method or operation is not implemented
        /// </summary>
        public override double CalibrationFactor_dB
        {
            get
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
        }

        #endregion

        #region OzDD100mc Optical Attenuator Specific functions

        /// <summary>
        /// put the mechanical elements of the attenuator into a fixed, known position
        /// at this position,the attenuation level is generally at a minimum value
        /// </summary>
        public void HomePosition()
        {
            string cmd = "H";
            chassisWrite(cmd);
        }
        /// <summary>
        /// go back one step from current position
        /// </summary>
        public void BackwardOneStep()
        {
            string cmd = "B";
            chassisWrite(cmd);
        }
        /// <summary>
        /// forward on step from current position
        /// </summary>
        public void ForwardOneStep()
        {
            string cmd = "F";
            chassisWrite(cmd);
        }
        /// <summary>
        /// re-initialize the unit.
        /// </summary>
        public void Reset()
        {
            chassisWrite("RST");
        }
        /// <summary>
        /// set the echo status
        /// When echo is on, the command (input) is retransmitted to the host. When echo is off, the
        /// response to the command is transmitted but the input is not.
        /// </summary>
        public bool Echo
        {
            get 
            {
                throw new InstrumentException("The method or operation is not implemented.");
            }
            set 
            {
                if (value)
                    chassisWrite("E1");
                else
                    chassisWrite("E0");
            }
        }
        /// <summary>
        /// get the current position in steps
        /// </summary>
        /// <returns></returns>
        public int GetCurrentPosition()
        {
            string strStep = chassisQuery("S?");
            return int.Parse(strStep.Split(':')[1]);
        }
        /// <summary>
        /// set the current position to the specified steps.
        /// </summary>
        /// <param name="steps"></param>
        public void SetPosition(int steps)
        {
            if (steps > 24000 || steps < 0)
                throw new InstrumentException("Invalid steps,should be in 0--24000");

            chassisWrite("S" + steps.ToString());
        }
        /// <summary>
        /// Forward step from current position,the step number is steps
        /// </summary>
        /// <param name="steps">step number</param>
        public void ForwardSteps(int steps)
        {
            string strStep = chassisQuery("S?");
            string str = strStep.Split(':')[1];
            int currentPosition = int.Parse(str);
            int stepsRange = 24000 - currentPosition;
            if (steps > stepsRange)
                throw new InstrumentException("steps out of range,should be less than" + stepsRange.ToString());
            chassisWrite("S+" + steps.ToString());
        }
        /// <summary>
        /// Backward step from current position,the step number is steps
        /// </summary>
        /// <param name="steps"></param>
        public void BackwardSteps(int steps)
        {
            string strStep = chassisQuery("S?");
            string str = strStep.Split(':')[1];
            int currentPosition = int.Parse(str);
            int stepsRange = currentPosition;
            if (steps > stepsRange)
                throw new InstrumentException("steps out of range,should be less than" + stepsRange.ToString());
            chassisWrite("S-" + steps.ToString());
        }

        #endregion

        #region Helper functions

        /// <summary>
        /// Chassis "Write" command - write to the chassis to change its state
        /// </summary>
        /// <param name="command">Command to send</param>
        private void chassisWrite(string command)
        {
            this.instrumentChassis.WriteLine(command, this);
        }

        /// <summary>
        /// Chassis "Query" command - read some information from the chassis
        /// </summary>
        /// <param name="command">Command to send</param>
        private string chassisQuery(string command)
        {
            return this.instrumentChassis.Query(command, this);
        }

        /// <summary>
        /// <param name="command">Command that was sent</param>
        /// <param name="response">Response to convert</param>
        /// </summary>
        /// <returns>The double value</returns>
        private double getDblFromString(string command, string response)
        {
            try
            {
                return Double.Parse(response);
            }
            catch (SystemException e)
            {
                throw new InstrumentException("Invalid response to '" + command +
                    "' , was expecting a double: " + response,
                    e);
            }
        }

        /// <summary>
        /// Get next line from file. Returns null if empty
        /// </summary>
        /// <returns>Array of strings for this line</returns>
        private string[] GetLine(StreamReader streamReader)
        {
            // read the next line
            string line = streamReader.ReadLine();
            // check for end of file (null).
            if (line == null) return null;
            string[] elemsInLine = line.Split(',');
            return elemsInLine;
        }

        public void InitPowerCalibraData(string filePowerCalibra)
        {
            try
            {
                using (StreamReader sr = new StreamReader(filePowerCalibra))
                {
                    List<string[]> fileLines = new List<string[]>();

                    this.GetLine(sr);

                    string[] lineElems;
                    while (true)
                    {
                        // read the next line
                        lineElems = this.GetLine(sr);
                        // check for end of file
                        if (lineElems == null) break;

                        fileLines.Add(lineElems);
                    }

                    List<int> listSteps = new List<int>();
                    List<double> listAttenua_dBm = new List<double>();

                    for (int i = 0; i < fileLines.Count; i++)
                    {
                        listSteps.Add(int.Parse(fileLines[i][0]));
                        listAttenua_dBm.Add(double.Parse(fileLines[i][1]));
                    }

                    stepForAttenua = listSteps.ToArray();
                    attenua_dBm = listAttenua_dBm.ToArray();
                }
            }
            catch (Exception e)
            {

                throw new AlgorithmException(e.Message);
            }
            
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_OzDd100mc instrumentChassis;

        int[] stepForAttenua;
        double[] attenua_dBm;

        #endregion
    }
}
