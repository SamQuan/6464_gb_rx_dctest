// [Copyright]
//
// Bookham Test Engine Library
// Bookham.TestLibrary.Instruments
//
// Inst_Ke2510.cs
//
// Author: T Foster & Joseph Olajubu, P Annetts added Digital IO capability
// Design: 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.ChassisNS;
using System.Collections.Generic;
using System.Threading;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for Keithley 2510 TEC controller - including its Digital Output capability
    /// </summary>
    public class Inst_Ke2510 : Instrument, IInstType_TecController, IInstType_DigiIOCollection, IInstType_SimpleTempControl
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ke2510(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information

            // Add Ke2510 details
            InstrumentDataRecord ke2510Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2510",			// hardware name 
                "A00",											// minimum valid firmware version 
                "A20");											// maximum valid firmware version 
            ke2510Data.Add("MinTecCurrentLimitAmp", "1.0");		// minimum TEC current limit
            ke2510Data.Add("MaxTecCurrentLimitAmp", "5.25");	// maximum TEC current limit
            ke2510Data.Add("MinTecVoltageLimit", "0.5");		// minimum TEC voltage limit
            ke2510Data.Add("MaxTecVoltageLimit", "10.5");		// maximum TEC voltage limit
            ke2510Data.Add("MinTemperatureLimit_C", "-2.0");		// minimum TEC voltage limit
            ke2510Data.Add("MaxTemperatureLimit_C", "50.0");	// maximum TEC voltage limit
            ke2510Data.Add("GroundConnect", "false");			// connect negative terminals to analogue 0V
            ke2510Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ValidHardwareData.Add("Ke2510", ke2510Data);
            // Add Ke2510 details
            InstrumentDataRecord ke2510Data_1 = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2510   ",			// hardware name 
                "A00",											// minimum valid firmware version 
                "A11");											// maximum valid firmware version 
            ke2510Data_1.Add("MinTecCurrentLimitAmp", "1.0");		// minimum TEC current limit
            ke2510Data_1.Add("MaxTecCurrentLimitAmp", "5.25");	// maximum TEC current limit
            ke2510Data_1.Add("MinTecVoltageLimit", "0.5");		// minimum TEC voltage limit
            ke2510Data_1.Add("MaxTecVoltageLimit", "10.5");		// maximum TEC voltage limit
            ke2510Data_1.Add("MinTemperatureLimit_C", "0.0");		// minimum TEC voltage limit
            ke2510Data_1.Add("MaxTemperatureLimit_C", "50.0");	// maximum TEC voltage limit
            ke2510Data_1.Add("GroundConnect", "false");			// connect negative terminals to analogue 0V
            ke2510Data_1.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ValidHardwareData.Add("Ke2510_1", ke2510Data_1);

            // Configure valid chassis information
            // Add 2510 chassis details
            InstrumentDataRecord ke2510ChassisData = new InstrumentDataRecord(
                "Chassis_Ke2510",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "1.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ke2510", ke2510ChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ke2510)base.InstrumentChassis;
            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(4);
            for (int ii = 1; ii <= 4; ii++)
            {
                digiOutLines.Add(new Inst_Ke2510_DigiOutLine(instrumentName + "_D" + ii, instrumentChassis, ii));
            }

            // Add Ke2510 details
            InstrumentDataRecord ke2510ATData = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2510-AT",			// hardware name 
                "A00",											// minimum valid firmware version 
                "A20");											// maximum valid firmware version 
            ke2510ATData.Add("MinTecCurrentLimitAmp", "1.0");		// minimum TEC current limit
            ke2510ATData.Add("MaxTecCurrentLimitAmp", "5.25");	// maximum TEC current limit
            ke2510ATData.Add("MinTecVoltageLimit", "0.5");		// minimum TEC voltage limit
            ke2510ATData.Add("MaxTecVoltageLimit", "10.5");		// maximum TEC voltage limit
            ke2510ATData.Add("MinTemperatureLimit_C", "0.0");		// minimum TEC voltage limit
            ke2510ATData.Add("MaxTemperatureLimit_C", "50.0");	// maximum TEC voltage limit
            ke2510ATData.Add("GroundConnect", "false");			// connect negative terminals to analogue 0V
            ke2510ATData.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ValidHardwareData.Add("Ke2510AT", ke2510ATData);

            // Configure valid chassis information
            // Add 2510 chassis details
            InstrumentDataRecord ke2510ATChassisData = new InstrumentDataRecord(
                "Chassis_Ke2510AT",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "1.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ke2510AT", ke2510ATChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ke2510)base.InstrumentChassis;
            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(4);
            for (int ii = 1; ii <= 4; ii++)
            {
                digiOutLines.Add(new Inst_Ke2510_DigiOutLine(instrumentName + "_D" + ii, instrumentChassis, ii));
            }
        }


        #region Public Instrument Methods and Properties


        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return instrumentChassis.HardwareIdentity;



            }
        }

        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return instrumentChassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Get/Set instrument online status
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                foreach (IInstType_DigitalIO dio in digiOutLines)
                {
                    dio.IsOnline = value;
                }
            }
        }

        /// <summary>
        /// Configures the instrument into a default state.
        /// 
        /// </summary>
        public override void SetDefaultState()
        {
            instrumentChassis.Write_Unchecked("*CLS", this);
            // Set factory default
            instrumentChassis.Write("*RST", this);
            // Set limits
            instrumentChassis.Write(":SOUR:TEMP:PROT:LOW " + HardwareData["MinTemperatureLimit_C"], this);
            instrumentChassis.Write(":SOUR:TEMP:PROT:HIGH " + HardwareData["MaxTemperatureLimit_C"], this);

            // Setline frequency
            instrumentChassis.Write(":SYST:LFR " + HardwareData["LineFrequency_Hz"], this);

            // Set ground connect
            if (HardwareData["GroundConnect"].ToLower() == "true")
            {
                instrumentChassis.Write(":SYST:GCON 1", this);
            }
            else
            {
                instrumentChassis.Write(":SYST:GCON 0", this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public enum LimitType
        {
            LOWER, UPPER
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstate"></param>
        /// <param name="dLimitValue"></param>
        public void Set_Protection_Level_Temperature(LimitType lstate, double dLimitValue)
        {
            if ((dLimitValue < -50) || (dLimitValue > 250))
                throw new Exception("K2510::Set_Protection_Level_Temperature - Supplied Temperature Value Out of Range");

            switch (lstate)
            {
                case LimitType.LOWER:
                    instrumentChassis.Write("SOUR:TEMP:PROT:LOW:LEV " + dLimitValue.ToString(), this);
                    break;
                case LimitType.UPPER:
                    instrumentChassis.Write("SOUR:TEMP:PROT:HIGH:LEV " + dLimitValue.ToString(), this);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public enum ProtectionType
        {
            ptRESISTANCE,
            ptTEMPERATURE
        }
        /// <summary>
        /// 
        /// </summary>
        public enum OnOrOff
        {
            OFF, ON
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pState"></param>
        /// <param name="state"></param>
        public void Set_Protection(ProtectionType pState, OnOrOff state)
        {
            string strState = state.ToString();
            switch (pState)
            {
                case ProtectionType.ptRESISTANCE:
                    instrumentChassis.Write("SOUR:RES:PROT:STAT " + strState, this);
                    break;
                case ProtectionType.ptTEMPERATURE:
                    instrumentChassis.Write("SOUR:TEMP:PROT:STAT " + strState, this);
                    break;
                default:
                    break;
            }
        }

        
        #endregion

        #region Public TecController InstrumentType Methods and Properties

        /// <summary>
        /// Sets/returns the output state
        /// </summary>
        public bool OutputEnabled
        {
            get
            {
                // Query the output state
                string rtn = instrumentChassis.Query(":OUTP:STAT?", this);

                // Return bool value
                return (rtn == trueStr);
            }
            set
            {
                // Convert bool value to string
                string boolVal = value ? trueStr : falseStr;

                // Set output
                instrumentChassis.Write(":OUTP:STAT " + boolVal, this);
            }
        }

        /// <summary>
        /// Sets/returns the control mode. Instrument driver should throw 
        /// if a particular mode is not supported
        /// </summary>
        public InstType_TecController.ControlMode OperatingMode
        {
            get
            {
                // Query the control mode
                string rtn = instrumentChassis.Query(":SOUR:FUNC:MODE?", this).Trim();

                // Convert return to ControlMode enum for return
                InstType_TecController.ControlMode mode;
                switch (rtn)
                {
                    case "TEMP": mode = InstType_TecController.ControlMode.Temperature; break;
                    case "RES": mode = InstType_TecController.ControlMode.Resistance; break;
                    case "CURR": mode = InstType_TecController.ControlMode.Current; break;
                    case "VOLT": mode = InstType_TecController.ControlMode.Voltage; break;
                    default: throw new InstrumentException("Unrecognised control mode '" + rtn + "'");
                }

                // Return mode
                return mode;
            }

            set
            {
                // Initialise the mode string
                string mode;
                switch (value)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode " + value.ToString());
                }

                // Set to instrument
                instrumentChassis.Write(":SOUR:FUNC:MODE " + mode, this);
            }
        }

        /// <summary>
        /// Sets/returns the Proportional Gain Constant.
        /// </summary>
        public double ProportionalGain
        {
            get
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode " + OperatingMode.ToString());
                }

                //Query and return Proportional Gain Value 
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:" + mode + ":LCON:GAIN?", this));
            }
            set
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode");
                }

                // Write out the new proportinal gain.
                instrumentChassis.Write(":SOUR:" + mode + ":LCON:GAIN " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Sets/returns the Derivative Gain Constant.
        /// </summary>
        public double DerivativeGain
        {
            get
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode " + OperatingMode.ToString());
                }

                //Query and return derivative Gain Value 
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:" + mode + ":LCON:DER?", this));
            }
            set
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode");
                }

                // Write out the new derivative gain.
                instrumentChassis.Write(":SOUR:" + mode + ":LCON:DER " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Sets/returns the Integral Gain Constant.
        /// </summary>
        public double IntegralGain
        {
            get
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode " + OperatingMode.ToString());
                }

                //Query and return integral Gain Value 
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:" + mode + ":LCON:INT?", this));
            }
            set
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    case InstType_TecController.ControlMode.Current: mode = "CURR"; break;
                    case InstType_TecController.ControlMode.Voltage: mode = "VOLT"; break;
                    default: throw new InstrumentException("Invalid control mode");
                }

                // Write out the new integral gain.
                instrumentChassis.Write(":SOUR:" + mode + ":LCON:INT " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Sets/returns the sensor type
        /// </summary>
        public InstType_TecController.SensorType Sensor_Type
        {
            get
            {
                // Query the sensor type
                string sensor = instrumentChassis.Query(":SENS:TEMP:TRAN?", this);

                // Query 4 wire sense mode. '0' = 2 wire, '1' = 4 wire
                string fourWire = instrumentChassis.Query(":SYST:RSEN?", this);

                // Select return value from SensorType enum 
                InstType_TecController.SensorType sensorAndMode;
                switch (sensor)
                {
                    case "RTD":
                        // Find out what kind of RTD (only USER and PT100 supported by this driver at present).
                        string rtdSensorType = instrumentChassis.Query(":SENS:TEMP:RTD:TYPE?", this);
                        if (rtdSensorType == "USER")
                        {
                            sensorAndMode = (fourWire == trueStr) ? InstType_TecController.SensorType.RTD_4wire
                                : InstType_TecController.SensorType.RTD_2wire;
                        }
                        else if (rtdSensorType == "PT100")
                        {
                            sensorAndMode = (fourWire == trueStr) ?
                                InstType_TecController.SensorType.PRT_PT100_4wire :
                                InstType_TecController.SensorType.PRT_PT100_2wire;
                        }
                        else
                        {
                            sensorAndMode = InstType_TecController.SensorType.Unsupported;
                        }
                        break;
                    case "THER":
                        sensorAndMode = (fourWire == trueStr) ?
                            InstType_TecController.SensorType.Thermistor_4wire :
                            InstType_TecController.SensorType.Thermistor_2wire;
                        break;
                    case "VSS": throw new InstrumentException("Voltage sensors are not supported by this driver");
                    case "ISS": throw new InstrumentException("Current sensors are not supported by this driver");
                    default: throw new InstrumentException("Unknown sensor type " + sensor.ToString());
                }

                // Return sensor type
                return sensorAndMode;
            }

            set
            {
                // Select operating mode
                switch (value)
                {
                    // Set RTD sensor, type User, and 2 wire 
                    case InstType_TecController.SensorType.RTD_2wire:
                        instrumentChassis.Write(":SYST:RSEN 0", this);
                        instrumentChassis.Write(":SENS:TEMP:TRAN RTD", this);
                        instrumentChassis.Write(":SENS:TEMP:RTD:TYPE USER", this);
                        break;

                    // Set RTD sensor, type User, and 4 wire  
                    case InstType_TecController.SensorType.RTD_4wire:
                        instrumentChassis.Write(":SYST:RSEN 1", this);
                        instrumentChassis.Write(":SENS:TEMP:TRAN RTD", this);
                        instrumentChassis.Write(":SENS:TEMP:RTD:TYPE USER", this);
                        break;

                    // Set PT100 compliant RTD sensor, type User, and 2 wire 
                    case InstType_TecController.SensorType.PRT_PT100_2wire:
                        instrumentChassis.Write(":SYST:RSEN 0", this);
                        instrumentChassis.Write(":SENS:TEMP:TRAN RTD", this);
                        instrumentChassis.Write(":SENS:TEMP:RTD:TYPE PT100", this);
                        break;

                    // Set PT100 compliant RTD sensor, type User, and 4 wire  
                    case InstType_TecController.SensorType.PRT_PT100_4wire:
                        instrumentChassis.Write(":SYST:RSEN 1", this);
                        instrumentChassis.Write(":SENS:TEMP:TRAN RTD", this);
                        instrumentChassis.Write(":SENS:TEMP:RTD:TYPE PT100", this);
                        break;

                    // Set thermistor sensor and 2 wire 
                    case InstType_TecController.SensorType.Thermistor_2wire:
                        instrumentChassis.Write(":SYST:RSEN 0", this);
                        instrumentChassis.Write(":SENS:TEMP:TRAN THER", this);
                        break;

                    // Set thermistor sensor and 4 wire 
                    case InstType_TecController.SensorType.Thermistor_4wire:
                        instrumentChassis.Write(":SYST:RSEN 1", this);
                        instrumentChassis.Write(":SENS:TEMP:TRAN THER", this);
                        break;

                    // Exception for all other cases
                    default: throw new InstrumentException("Unknown/Unsupported sensor type " + value.ToString());
                }
            }
        }

        /// <summary>
        /// Sets/returns the Thermistor Sensor Steinhart-Hart Equation coefficients. 
        /// </summary>
        public SteinhartHartCoefficients SteinhartHartConstants
        {
            get
            {
                // Query the Steinhart-Hart constants from the instrument and return the results
                SteinhartHartCoefficients constants
                    = new SteinhartHartCoefficients(Convert.ToDouble(instrumentChassis.Query(":SENS:TEMP:THER:A?", this)),
                                                    Convert.ToDouble(instrumentChassis.Query(":SENS:TEMP:THER:B?", this)),
                                                    Convert.ToDouble(instrumentChassis.Query(":SENS:TEMP:THER:C?", this)));

                return constants;
            }

            set
            {
                //Write the Steinhart-Hart Constants to the instrument.
                string setThermistorConstCommand = String.Format(":SENS:TEMP:THER:A {0}", value.A.ToString());
                instrumentChassis.Write(setThermistorConstCommand, this);

                setThermistorConstCommand = String.Format(":SENS:TEMP:THER:B {0}", value.B.ToString());
                instrumentChassis.Write(setThermistorConstCommand, this);

                setThermistorConstCommand = String.Format(":SENS:TEMP:THER:C {0}", value.C.ToString());
                instrumentChassis.Write(setThermistorConstCommand, this);
            }
        }

        /// <summary>
        /// Sets/returns the RTD Sensor Callendar-Van Dusen Equation coefficients. 
        /// </summary>
        public CallendarVanDusenCoefficients CallendarVanDusenConstants
        {
            get
            {
                //Get and return the current set of constants from the instrument
                CallendarVanDusenCoefficients constants
                    = new CallendarVanDusenCoefficients(Convert.ToDouble(instrumentChassis.Query("SENS:TEMP:RTD:RANG?", this)),
                                                         Convert.ToDouble(instrumentChassis.Query("SENS:TEMP:RTD:ALPH?", this)),
                                                         Convert.ToDouble(instrumentChassis.Query("SENS:TEMP:RTD:BETA?", this)),
                                                         Convert.ToDouble(instrumentChassis.Query("SENS:TEMP:RTD:DELT?", this)));

                return constants;
            }

            set
            {
                string setRtdConstCommand;

                //Write the Callendar-Van Dusen Constants to the instrument.
                if ((value.R0 != 100) && (value.R0 != 1000))
                {
                    //Unsupported R0 value for this instrument
                    throw new InstrumentException("Unsupported User RTD R0 value" + value.R0.ToString());
                }
                else
                {
                    setRtdConstCommand = String.Format(":SENS:TEMP:RTD:RANG {0}", value.R0.ToString());
                    instrumentChassis.Write(setRtdConstCommand, this);
                }

                setRtdConstCommand = String.Format(":SENS:TEMP:RTD:ALPH {0}", value.Alpha.ToString());
                instrumentChassis.Write(setRtdConstCommand, this);

                setRtdConstCommand = String.Format(":SENS:TEMP:RTD:BETA {0}", value.Beta.ToString());
                instrumentChassis.Write(setRtdConstCommand, this);

                setRtdConstCommand = String.Format(":SENS:TEMP:RTD:DELT {0}", value.Delta.ToString());
                instrumentChassis.Write(setRtdConstCommand, this);
            }
        }

        /// <summary>
        /// Sets/returns the temperature set point.
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public double SensorTemperatureSetPoint_C
        {
            get
            {
                InstType_TecController.ControlMode mode = OperatingMode;

                if (mode == InstType_TecController.ControlMode.Temperature)
                {
                    //Instrument is in Temperature mode. Read back the target temperature from the Instrument.
                    return Convert.ToDouble(instrumentChassis.Query(":SOUR:TEMP:SPO?", this));
                }
                else if (mode == InstType_TecController.ControlMode.Resistance)
                {
                    //Instrument is in resistance mode. Work out what Temperature target has been set by 
                    //reading the Resistsance set Point, and calculating the equivalent temperature, 
                    //using the appropriate equation.
                    double resistance = Convert.ToDouble(instrumentChassis.Query(":SOUR:RES:LEV?", this));

                    if ((Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire) ||
                        (Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire))
                    {
                        // Use the Steinhart-Hart equation if a Thermistor sensor is being used.
                        return (SteinhartHart.ResistanceOhmsToTemperatureCelsius(resistance,
                                                                                 SteinhartHartConstants.A,
                                                                                 SteinhartHartConstants.B,
                                                                                 SteinhartHartConstants.C));

                    }
                    else if ((Sensor_Type == InstType_TecController.SensorType.RTD_2wire) || (Sensor_Type == InstType_TecController.SensorType.RTD_4wire))
                    {
                        // Use the Callendar-Van Dusen equation as a user defined RTD sensor is being used.
                        return (CallendarVanDusen.ResistanceOhmsToTemperatureCelsius(resistance,
                                                                                      CallendarVanDusenConstants.R0,
                                                                                      CallendarVanDusenConstants.Alpha,
                                                                                      CallendarVanDusenConstants.Beta,
                                                                                      CallendarVanDusenConstants.Delta));
                    }
                    else if ((Sensor_Type == InstType_TecController.SensorType.PRT_PT100_2wire) || (Sensor_Type == InstType_TecController.SensorType.PRT_PT100_4wire))
                    {
                        // Use the Callendar-Van Dusen equation as a PT100 compliant RTD sensor is being used.
                        return (CallendarVanDusen.ResistanceOhmsToTemperatureCelsius(resistance, PT100_R0, PT100_Alpha, PT100_Beta, PT100_Delta));
                    }
                    else
                    {
                        // Invalid Sensor Type
                        throw new InstrumentException("Cannot support reading temperature with sensor '" + Sensor_Type.ToString());
                    }
                }
                else
                {
                    // The Instrument is in an invalid mode.
                    string errorString = String.Format("Cannot Get temperature with instrument in Control mode {0}", mode.ToString());
                    throw new InstrumentException(errorString);
                }
            }

            set
            {
                InstType_TecController.ControlMode mode = OperatingMode;
                if (mode == InstType_TecController.ControlMode.Temperature)
                {
                    instrumentChassis.Write(":SOUR:TEMP:SPO " + value.ToString(), this);
                }
                else if (mode == InstType_TecController.ControlMode.Resistance)
                {
                    //Instrument is in resistance mode. Work out what Resistance needs to be 
                    // set to give the desired temperature.
                    if ((Sensor_Type == InstType_TecController.SensorType.Thermistor_2wire) || (Sensor_Type == InstType_TecController.SensorType.Thermistor_4wire))
                    {
                        // Use the Steinhart-Hart equation if a Thermistor sensor is being used.
                        //Write the required resistance in K Ohms to the instrument.
                        double resistance = SteinhartHart.TemperatureCelsiusToResistanceOhms(value,
                                                                                              SteinhartHartConstants.A,
                                                                                              SteinhartHartConstants.B,
                                                                                              SteinhartHartConstants.C);

                        // Set resistance.
                        instrumentChassis.Write(":SOUR:RES:LEV " + resistance.ToString(), this);
                    }
                    else if ((Sensor_Type == InstType_TecController.SensorType.RTD_2wire) || (Sensor_Type == InstType_TecController.SensorType.RTD_4wire))
                    {
                        double resistance = CallendarVanDusen.TemperatureCelsiusToResistanceOhms(value,
                                                                                                  CallendarVanDusenConstants.R0,
                                                                                                  CallendarVanDusenConstants.Alpha,
                                                                                                  CallendarVanDusenConstants.Beta,
                                                                                                  CallendarVanDusenConstants.Delta);
                        // Set resistance.
                        instrumentChassis.Write(":SOUR:RES:LEV " + resistance.ToString(), this);
                    }
                    else if ((Sensor_Type == InstType_TecController.SensorType.PRT_PT100_2wire) || (Sensor_Type == InstType_TecController.SensorType.PRT_PT100_4wire))
                    {
                        double resistance
                             = CallendarVanDusen.TemperatureCelsiusToResistanceOhms(value, PT100_R0, PT100_Alpha, PT100_Beta, PT100_Delta);

                        // Set resistance.
                        instrumentChassis.Write(":SOUR:RES:LEV " + resistance.ToString(), this);
                    }
                }
                else
                {
                    throw new InstrumentException("Cannot set temperature in '" + mode.ToString() + "' mode");
                }
            }
        }

        /// <summary>
        /// Returns the actual temperature.
        /// </summary>
        public double SensorTemperatureActual_C
        {
            get
            {
                InstType_TecController.ControlMode mode = OperatingMode;
                if ((mode == InstType_TecController.ControlMode.Temperature) ||
                    (mode == InstType_TecController.ControlMode.Resistance))
                {
                    // Measure temperature. Instrument should be in 'C' mode from initialisation
                    return Convert.ToDouble(instrumentChassis.Query(":MEAS:TEMP?", this));
                }
                else
                {
                    // The Instrument is in an invalid mode.
                    string errorString = String.Format("Cannot Get temperature with instrument in Control mode {0}", mode.ToString());
                    throw new InstrumentException(errorString);
                }
            }
        }

        ///// <summary>
        ///// Returns the actual thermistor resistance
        ///// </summary>
        public double ThermistorResistanceActual_ohm
        {
            get
            {
                // Measure actual thermistor
                return Convert.ToDouble(instrumentChassis.Query_Unchecked(":MEAS:TSEN?", this));
            }
        }

        /// <summary>
        /// Returns the Low Limits of Thermistor
        /// </summary>
        public double ThermistorLowLimits_ohms
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query_Unchecked(":SOUR:RES:PROT:LOW:LEV?", this));
            }
            set
            {
                instrumentChassis.Write_Unchecked(":SOUR:RES:PROT:LOW:LEV " + value.ToString(), this);
            }
        }


        public double ThermistorHighLimits_ohms
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query_Unchecked(":SOUR:RES:PROT:HIGH:LEV?", this));
            }
            set
            {
                instrumentChassis.Write_Unchecked(":SOUR:RES:PROT:HIGH:LEV " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Set/returns the sensor resistance  set point
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public double SensorResistanceSetPoint_ohm
        {
            get
            {
                // Get the  sensor resistance set point. 
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:RES:LEV?", this));
            }

            set
            {
                // Make sure instrument is in resistance mode
                InstType_TecController.ControlMode mode = OperatingMode;
                if (mode != InstType_TecController.ControlMode.Resistance)
                {
                    throw new InstrumentException("Cannot set resistance in '" + mode.ToString() + "' mode");
                }

                // Set resistance. (Ignore underlining)
                instrumentChassis.Write(":SOUR:RES:LEV " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Returns the actual sensor resistance
        /// Note that set should throw an exception if the controller is not 
        /// operating in the appropriate mode 
        /// </summary>
        public double SensorResistanceActual_ohm
        {
            get
            {
                // Measure sensor resistance. 
                return Convert.ToDouble(instrumentChassis.Query(":MEAS:TSEN?", this));
            }
        }


        /// <summary>
        /// Set/returns the peltier current set point
        /// </summary>
        public double TecCurrentSetPoint_amp
        {
            get
            {
                // Get the  TEC current set point 
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:CURR:LEV?", this));
            }

            set
            {
                // Make sure instrument is in current mode
                InstType_TecController.ControlMode mode = OperatingMode;
                if (mode != InstType_TecController.ControlMode.Current)
                {
                    throw new InstrumentException("Cannot set TEC current in '" + mode.ToString() + "' mode");
                }

                // Set current
                instrumentChassis.Write(":SOUR:CURR:LEV " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Returns the actual TEC (peltier) current.
        /// </summary>
        public double TecCurrentActual_amp
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query(":MEAS:CURR?", this));
            }
        }

        /// <summary>
        /// Set/returns the TEC voltage set point
        /// </summary>
        public double TecVoltageSetPoint_volt
        {
            get
            {
                // Get the  TEC voltage set point 
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:VOLT:LEV?", this));
            }

            set
            {
                // Make sure instrument is in voltage mode
                InstType_TecController.ControlMode mode = OperatingMode;
                if (mode != InstType_TecController.ControlMode.Voltage)
                {
                    throw new InstrumentException("Cannot set voltage in '" + mode.ToString() + "' mode");
                }

                // Set voltage
                instrumentChassis.Write(":SOUR:VOLT:LEV " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Returns the actual TEC voltage
        /// </summary>
        public double TecVoltageActual_volt
        {
            get
            {
                return Convert.ToDouble(instrumentChassis.Query(":MEAS:VOLT?", this));
            }
        }

        /// <summary>
        /// Set/returns the sensor current.  
        /// </summary>
        public double SensorCurrent_amp
        {
            get
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    default: throw new InstrumentException("Invalid control mode");
                }

                // Measure sensor current 
                return Convert.ToDouble(instrumentChassis.Query(":SENS:" + mode + ":CURR?", this));
            }

            set
            {
                // Initialise the mode string
                string mode;
                switch (OperatingMode)
                {
                    case InstType_TecController.ControlMode.Temperature: mode = "TEMP"; break;
                    case InstType_TecController.ControlMode.Resistance: mode = "RES"; break;
                    default: throw new InstrumentException("Invalid control mode");
                }

                // Check if autorange
                if (value.Equals(InstType_TecController.AutoRange))
                {
                    // Set autorange on
                    instrumentChassis.Write(":SENS:" + mode + ":CURR:AUTO 1", this);
                }
                else
                {
                    // Set autorange off and set sensor current
                    instrumentChassis.Write(":SENS:" + mode + ":CURR:AUTO 0", this);
                    instrumentChassis.Write(":SENS:" + mode + ":CURR " + value.ToString(), this);
                }
            }
        }

        /// <summary>
        /// Returns the TEC DC resistance
        /// </summary>
        public double TecResistanceDC_ohm
        {
            get
            {
                // Measure and return DC resistance
                return Convert.ToDouble(instrumentChassis.Query(":MEAS:RES:DC?", this));
            }
        }

        /// <summary>
        /// Returns the TEC AC resistance
        /// </summary>
        public double TecResistanceAC_ohm
        {
            get
            {
                // Measure and return AC resistance
                return Convert.ToDouble(instrumentChassis.Query(":MEAS:RES:AC?", this));
            }
        }

        /// <summary>
        /// Set/returns the TEC compliance current
        /// </summary>
        public double TecCurrentCompliance_amp
        {
            get
            {
                // Read the protection current
                return Convert.ToDouble(instrumentChassis.Query(":SENS:CURR:PROT?", this));
            }

            set
            {
                // Set the protection voltage
                instrumentChassis.Write(":SENS:CURR:PROT " + value.ToString(), this);
            }
        }

        /// <summary>
        /// Set/returns the TEC compliance voltage
        /// </summary>
        public double TecVoltageCompliance_volt
        {
            get
            {
                // Read the protection voltage
                return Convert.ToDouble(instrumentChassis.Query(":SOUR:VOLT:PROT?", this));
            }
            set
            {
                // Set the protection voltage
                instrumentChassis.Write(":SOUR:VOLT:PROT " + value.ToString(), this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public enum DataResponseFormat
        {
            TYPE_ASCII,
            TYPE_HEX,
            TYPE_OCT,
            TYPE_BIN
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstate"></param>
        /// <param name="dLimitValue"></param>
        public void Set_Protection_Level_Resistance(LimitType lstate, double dLimitValue)
        {

            switch (lstate)
            {
                case LimitType.LOWER:
                    instrumentChassis.Write("SOUR:RES:PROT:LOW:LEV " + dLimitValue.ToString(), this);
                    break;
                case LimitType.UPPER:
                    instrumentChassis.Write("SOUR:RES:PROT:HIGH:LEV " + dLimitValue.ToString(), this);
                    break;
                default:
                    break;
            }
        }

       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        public void Set_Status_Register_Data_Format(DataResponseFormat state)
        {

            switch (state)
            {
                case DataResponseFormat.TYPE_ASCII:
                    instrumentChassis.Write("FORM:SREG ASC", this);

                    break;
                case DataResponseFormat.TYPE_HEX:
                    instrumentChassis.Write("FORM:SREG HEX", this);

                    break;
                case DataResponseFormat.TYPE_OCT:
                    instrumentChassis.Write("FORM:SREG OCT", this);

                    break;
                case DataResponseFormat.TYPE_BIN:
                    instrumentChassis.Write("FORM:SREG BIN", this);

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Set ground connect
        /// </summary>
        public bool GroundConnect
        {
            set
            {
                instrumentChassis.Write(":SYST:GCON " + (value ? "1" : "0"), this);
            }
        }
        
        
        public enum SettingControl
        {
            SET_TEMPERATURE_AND_VERIFY,
            DO_NOT_SET_TEMPERATURE_VERIFY_ONLY
        }
        static double FindMaxValueInArray(double[] oneDArray)
        {
            if (oneDArray == null)
            {
                throw new ArgumentNullException("Null array passed in");
            }
            if (oneDArray.Length == 0)
            {
                throw new Exception("Non valid array passed in");
            }
            double maxValue = oneDArray[0];
            foreach (double x in oneDArray)
            {
                if (x > maxValue)
                {
                    maxValue = x;
                }
            }
            return (maxValue);
        }
        protected const int m_nSamplingInterval = 5000;
        public bool SetTecTemperatureAndWaitToSettle(double dTargetTemperature,
                                        double dTargetTolerance, int Timeout_S,
                                        double dMaxDeltaChange, SettingControl scState,
                                        out int iElapsedTimeToTarget_S)
        {
            // dMaxDeltaChange -	To Disable this functionality set this parameter to zero or a negative number
            //						With any other value entered, once the temperature, is within the tolerance
            //						this routine will kick in and monitor the temperature delta over time 
            //						intervals of 5 seconds. Only when three consecutive samples are within the 
            //						specified delta, will the routine register that the temperature has been set.
            //						If this cannot be achieved within the nMaxTimeToWaitInSeconds, the function will
            //						fail.

            DateTime tmStartTime, tmFinishTime;
            double dElapsedTime;
            double dCurrentTemperature = 0;
            double dDeltaFromTarget = 999;
            bool bTemperatureAcheived = false;
            bool bStable = false;
            bool bTargetAcheived;
            System.TimeSpan ts;

            // Check that the temperature is sensible
            if ((dTargetTemperature < -50) || (dTargetTemperature > 225))
            {
                throw new Exception("K2510 SetTecTemperatureAndWaitToSettle - Target Temperature Supplied is Out of Range");
            }
            // Kick off the process timer
            tmStartTime = DateTime.Now;

            if (scState == SettingControl.SET_TEMPERATURE_AND_VERIFY)
            {
                // Set the Temperature
                if (OperatingMode != InstType_TecController.ControlMode.Temperature)
                    OperatingMode = InstType_TecController.ControlMode.Temperature;
                SensorTemperatureSetPoint_C = dTargetTemperature;
                OutputEnabled = true;
            }

            do
            {
                // Set Sampling Interval 
                Thread.Sleep(m_nSamplingInterval);

                dCurrentTemperature = SensorTemperatureActual_C;

                // Calculate Delta to Target
                dDeltaFromTarget = Math.Abs(dCurrentTemperature - dTargetTemperature);

                // Update the time taken to Stabilise
                tmFinishTime = DateTime.Now;
                ts = tmFinishTime - tmStartTime;
                dElapsedTime = Math.Floor(ts.TotalSeconds);

                bTemperatureAcheived = dDeltaFromTarget < dTargetTolerance;

                if (!bTemperatureAcheived && dElapsedTime > Timeout_S) break;

            } while (!bTemperatureAcheived);

            // This is the main routine completed, now need to see if we
            // need to check for oscillations in the temperature
            // However we will only do this if the temperature has acheived target within 
            // tolerance in the first place, and we haven't exceeded the maximum time allowed for settling
            if (dMaxDeltaChange > 0 && bTemperatureAcheived)
            {
                List<double> dDeltaVec = new List<double>();
                double dDelta = 9999;

                do
                {
                    int nCount = 0;
                    bStable = true;
                    do
                    {
                        dCurrentTemperature = SensorTemperatureActual_C;
                        dDelta = Math.Abs(dCurrentTemperature - dTargetTemperature);
                        // Calcuate Delta
                        dDeltaVec.Add(dDelta);
                        // Sampling Interval
                        Thread.Sleep(m_nSamplingInterval);

                    } while (++nCount < 2);

                    double dMaxima = FindMaxValueInArray(dDeltaVec.ToArray());

                    if (dMaxima > dMaxDeltaChange)
                    {
                        bStable = false;
                    }

                    // Update the time taken to Stabilise
                    tmFinishTime = DateTime.Now;
                    ts = tmFinishTime - tmStartTime;
                    dElapsedTime = Math.Floor(ts.TotalSeconds);

                    if (dElapsedTime > Timeout_S)
                    {
                        bStable = false;
                        break;
                    }

                } while (bStable == false);
            }


            // Last update of the time
            tmFinishTime = DateTime.Now;
            ts = tmFinishTime - tmStartTime;
            dElapsedTime = Math.Floor(ts.TotalSeconds);
            iElapsedTimeToTarget_S = (int)dElapsedTime;

            if (dMaxDeltaChange <= 0)
            {
                // We only have to worry about evaluating first stage performance
                bTargetAcheived = bTemperatureAcheived;
            }
            else
            {
                // User has enabled Oscillatory Check as well
                bTargetAcheived = bTemperatureAcheived && bStable;
            }

            return bTargetAcheived;
        }

        public bool SetTecResistanceAndWaitToSettle(double dTargetResistance_Ohms,
                   double dTargetTolerance_Ohms, int Timeout_S, double dMaxDeltaChange,
            SettingControl scState, out int iElapsedTimeToTarget_S)
        {
            // dMaxDeltaChange -	To Disable this functionality set this parameter to zero or a negative number
            //						With any other value entered, once the resistance, is within the tolerance
            //						this routine will kick in and monitor the resistance delta over time 
            //						intervals of 5 seconds. Only when three consecutive samples are within the 
            //						specified delta, will the routine register that the resistance has been set.
            //						If this cannot be achieved within the Timeout_S, the function will
            //						fail.
            DateTime tmStartTime, tmFinishTime;
            double dElapsedTime;
            double dCurrentResistance = 0;
            double dDeltaFromTarget = 999;
            bool bResistanceAcheived = false;
            bool bStable = false;
            System.TimeSpan ts;
            bool bTargetAcheived;

            // Kick off the process timer
            tmStartTime = DateTime.Now;

            if (scState == SettingControl.SET_TEMPERATURE_AND_VERIFY)
            {
                if (OperatingMode != InstType_TecController.ControlMode.Resistance)
                    OperatingMode = InstType_TecController.ControlMode.Resistance;
                SensorResistanceSetPoint_ohm = dTargetResistance_Ohms;
                OutputEnabled = true;
            }

            do
            {
                // Sampling Interval 
                Thread.Sleep(m_nSamplingInterval);
                dCurrentResistance = SensorResistanceActual_ohm;
                // Calculate Delta to Target
                dDeltaFromTarget = Math.Abs(dCurrentResistance - dTargetResistance_Ohms);

                // Update the time taken to Stabilise
                tmFinishTime = DateTime.Now;
                ts = tmFinishTime - tmStartTime;
                dElapsedTime = Math.Floor(ts.TotalSeconds);

                bResistanceAcheived = dDeltaFromTarget < dTargetTolerance_Ohms;

                if (!bResistanceAcheived && dElapsedTime > Timeout_S) break;

            } while (!bResistanceAcheived);

            // This is the main routine completed, now need to see if we
            // need to check for oscillations in the Resistance
            // However we will only do this if the resistance has acheived target within 
            // tolerance in the first place, and we haven't exceeded the maximum time allowed for settling
            if (dMaxDeltaChange > 0 && bResistanceAcheived)
            {
                List<double> dDeltaLst = new List<double>();
                double dDelta = 9999;

                do
                {
                    int nCount = 0;
                    bStable = true;
                    do
                    {
                        dCurrentResistance = SensorResistanceActual_ohm;

                        dDelta = Math.Abs(dCurrentResistance - dTargetResistance_Ohms);

                        // Calcuate Delta
                        dDeltaLst.Add(dDelta);

                        // Sampling Interval Set
                        Thread.Sleep(m_nSamplingInterval);

                    } while (++nCount < 2);

                    double dMaxima = FindMaxValueInArray(dDeltaLst.ToArray());

                    if (dMaxima > dMaxDeltaChange)
                    {
                        bStable = false;
                    }

                    // Update the time taken to Stabilise
                    tmFinishTime = DateTime.Now;
                    ts = tmFinishTime - tmStartTime;
                    dElapsedTime = Math.Floor(ts.TotalSeconds);

                    if (dElapsedTime > Timeout_S)
                    {
                        bStable = false;
                        break;
                    }

                } while (bStable == false);
            }

            // Last update of the time
            tmFinishTime = DateTime.Now;
            ts = tmFinishTime - tmStartTime;
            dElapsedTime = Math.Floor(ts.TotalSeconds);
            iElapsedTimeToTarget_S = (int)dElapsedTime;

            if (dMaxDeltaChange <= 0)
            {
                // We only have to worry about evaluating first stage performance
                bTargetAcheived = bResistanceAcheived;
            }
            else
            {
                // User has enabled Oscillatory Check as well
                bTargetAcheived = bResistanceAcheived && bStable;
            }
            return bTargetAcheived;
        }

        #endregion

        #region IInstType_DigiIOCollection Members
        /// <summary>
        /// Get a Digital IO Line by line number
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        public IInstType_DigitalIO GetDigiIoLine(int lineNumber)
        {
            return digiOutLines[lineNumber - 1];
        }
        /// <summary>
        /// Get enumerator for Digital IO Line
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IInstType_DigitalIO> DigiIoLines
        {
            get
            {
                return digiOutLines;
            }
        }
        #endregion

        #region Private Data

        // Chassis reference
        internal Chassis_Ke2510 instrumentChassis;

        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";

        // Callendar-Van Dusen Constants for PT00 Platinum RTDs
        const double PT100_R0 = 100;
        const double PT100_Alpha = 0.00385;
        const double PT100_Beta = 0.10863; // Only valid for Temperatures less than 0 degrees Celsius.
        const double PT100_Delta = 1.4999;
        internal List<IInstType_DigitalIO> digiOutLines;
        #endregion
    }
}
