using System;
using System.Diagnostics;
using NUnit.Framework;



namespace Bookham.TestLibrary.Algorithms
{
	/// <summary>
	/// Test Harness for BoundingIndices
	/// </summary>
	internal class LinearLeastSquaresTEST
	{

		[Test]
		public static void TEST()
		{

			double[] emptyArray = {};

			double[] x1Array = { 0, 1, 2, 3, 4, 5,  6,  7,   8,   9 };
			double[] x2Array = { 5, 5, 5, 5, 5, 5,  5,  5,   5,   5 }; // All the same value

			double[] y1Array = { 0, 1, 2, 3, 4, 5,  6,  7,   8,   9 };
			double[] y2Array = { 0, 2, 4, 6, 8, 10, 12, 14,  16,  18 };
			double[] y3Array = { 2, 3, 3, 3, 3, 7,  28, 104, 110, 80 };
			double[] y4Array = { 4, 4, 4, 4, 4, 4,  4,  4,   4,   4 }; // All the same value

			double[] Index =   { 0, 1, 2, 3, 4, 5,  6,  7,   8,   9 };

			//
			// ERROR Conditions
			//

			// Empty X array
			try
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( emptyArray, y1Array, 0, 9 );
				Debug.Assert( false );
			}
			catch( Exception e )
			{
				Debug.Assert( e.Message == "empty array" );
			}

			// Empty Y array
			try
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, emptyArray, 0, 9 );
				Debug.Assert( false );
			}
			catch( Exception e )
			{
			Debug.Assert( e.Message == "empty array" );
			}

			// To Index value > arraysize
			try
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, y1Array, 0, 10 );
				Debug.Assert( false );
			}
			catch( Exception e )
			{
				Debug.Assert( e.Message == "toIndex is greater than number of array points" );
			}

			// All x values the same
			try
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x2Array, y1Array, 0, 9 );
				Debug.Assert( false );
			}
			catch( Exception e )
			{
				Debug.Assert( e.Message == "SSxx==0 => all x values are the same" );
			}



			//
			// Available Operations
			//
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, x1Array, 0, 9 );
				Debug.Assert( linLeastSqFit.Slope == 1);
				Debug.Assert( linLeastSqFit.YIntercept == 0);
				Debug.Assert( linLeastSqFit.CorrelationCoefficient == 1);
			}

			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, y1Array, 0, 9 );
				Debug.Assert( linLeastSqFit.Slope == 1);
				Debug.Assert( linLeastSqFit.YIntercept == 0);
			}

			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, y3Array, 0, 9 );
				Debug.Assert( Math.Abs(linLeastSqFit.Slope-12.33) < 0.01 );
				Debug.Assert( Math.Abs(linLeastSqFit.YIntercept+21.2) < 0.1 );
			}

			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, y3Array, 5, 8 );
				Debug.Assert( Math.Abs(linLeastSqFit.Slope-38.5) < 0.1 );
				Debug.Assert( Math.Abs(linLeastSqFit.YIntercept+188.0) < 0.1 );
			}
				// Cope with fromIndex & toIndex interchanged
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, y3Array, 8, 5 );
				Debug.Assert( Math.Abs(linLeastSqFit.Slope-38.5) < 0.1 );
				Debug.Assert( Math.Abs(linLeastSqFit.YIntercept+188.0) < 0.1 );
			}

				// All y values = 4
			{
				LinearLeastSquaresFit linLeastSqFit = LinearLeastSquaresFitAlgorithm.Calculate( x1Array, y4Array, 0, 9 );
				Debug.Assert( Math.Abs(linLeastSqFit.Slope-0) < 0.001 );
				Debug.Assert( Math.Abs(linLeastSqFit.YIntercept-4) < 0.001 );
			}
		}


	}
}
