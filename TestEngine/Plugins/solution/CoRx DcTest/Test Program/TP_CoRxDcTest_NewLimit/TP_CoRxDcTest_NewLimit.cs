// [Copyright]
//
// Bookham [Coherence Rx DC Test ]
// Bookham.TestSolution.TestPrograms
//
// TP_CoRxDcTest.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.ExternalData;
using Bookham.TestEngine.Framework.Limits;
using System.Collections.Specialized;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestLibrary.Utilities;
using Bookham.TestEngine.Config;
using System.IO;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// Program for Co Rx dc measurement test
    /// </summary>
    public class TP_CoRxDcTest_NewLimit : ITestProgram
    {
        #region Private data
        /// <summary>
        /// Remember the specification name we are using in the program
        /// </summary>
        private string specName;
        Specification MainSpec;
        private double NormalTemperature;
        TestParamConfigAccessor TestParmConfig;
        TestParamConfigAccessor InstrParamConfig;

        TempTableConfigAccessor TempParamConfig;

        ConfigDataAccessor InstrumentsCalData;
        double[] TestTemperatures = null;
        double[] FreqArr2VerifySR = null;
        EnumFrequencySetMode FreqSetMode = EnumFrequencySetMode.ByChan;

        Inst_CoRxITLALaserSource itla_sig = null;
        Inst_CoRxITLALaserSource itla_loc = null;

        DatumList ChipsInfoList;
        double VauxPd_X_V;
        double VauxPd_Y_V;
        DateTime time_Start;
        #endregion

        #region ITestProgram Members
        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return typeof(TP_CoRxDcTest_NewLimitGui); }
        }

        /// <summary>
        /// Initialise phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void InitCode(ITestEngineInit engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {

            engine.SetVersionControl(false, false);
            engine.GuiShow();
            engine.GuiToFront();
            engine.SendToGui("Initialising ...");
            time_Start = DateTime.Now;
            foreach (Chassis chassisTemp in chassis.Values)  // disable chassis logging
            {
                chassisTemp.EnableLogging = false;
            }

            foreach (Instrument instrTemp in instrs.Values )  // disable instrument logging
            {
                instrTemp.EnableLogging = false;
            }

            LoadConfigFiles(engine, dutObject);
            CoRxDCTestInstruments.Is6464 = TestParmConfig.GetBoolParam("Is6464");
            // load specification
            SpecList specs = this.LoadSpecs(engine,dutObject);

            // TODO: Get chips information
            if (TestParmConfig.GetBoolParam("IsGetChipInfo"))
            {
                bool isPrevDataOK = ValidatePrevStageData(engine, dutObject);
                if (!isPrevDataOK)
                    engine.ErrorInProgram("the previous test data from PCAS or manual entry is not OK!");
            }
            else
            {
                ChipsInfoList = null;
            }

            #region Get auxilary pd bais according to wafer id
            if (ChipsInfoList != null)
            {
                string wfID = "";
                if (ChipsInfoList.IsPresent("X_WAFER_ID")) wfID = ChipsInfoList.ReadString("X_WAFER_ID");

                VauxPd_X_V = GetAuxPdBias_V(wfID);

                wfID = "";
                if (ChipsInfoList.IsPresent("Y_WAFER_ID")) wfID = ChipsInfoList.ReadString("Y_WAFER_ID");

                VauxPd_Y_V = GetAuxPdBias_V(wfID);
            }
            #endregion
           
            InitialiseInstrs(engine, instrs);
           
            engine.SendStatusMsg("Initialising Test modules ");
            InitCaseTempModule(engine);
           
            InitSRVerifyModules(engine);
            InitDarkCurrentModule(engine);
            if (TestParmConfig.GetBoolParam("IsTestAux")) InitCouplieEffModules(engine, dutObject);
            InitResponsivityModules(engine, dutObject, (Inst_Ke24xx)instrs[InstrParamConfig.GetStringParam("MasterSourceNameInTriggerLink")]);
            if (TestParmConfig.GetBoolParam("IsTestVOA")) InitVOATestModules(engine, dutObject, (Inst_Ke24xx)instrs[InstrParamConfig.GetStringParam("MasterSourceNameInTriggerLink")]);

            // Clean up the blob file folder
            CleanUpBlobFiles();
            engine.SendToGui("");

        }
        /// <summary>
        /// test run phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void Run(ITestEngineRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            
            if (engine.IsSimulation) return;
            bool isModuleRunOk = false;
            ButtonId userOpt= ButtonId .Cancel;
            // Increase Sr Cal counter in config file
            Util_SrCalCounter.UpdateSrCalCounter();
            
            engine.GuiToFront();
            
            bool isAbortTestOnFail = TestParmConfig.GetBoolParam("IsProgramFailOnModuleFail");
            bool isPromoteContinueOnFail = TestParmConfig.GetBoolParam("IsPromoteContinueOnModuleFail");
            //CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.ZeroDarkCurrent_End();
            //CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ZeroDarkCurrent_End();
            CoRxDCTestInstruments.SelectItla(itla_loc);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.SelectItla(itla_sig);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            engine.ShowContinueUserQuery("Please load the DUT in the jig and close the cover. Press continue when done.");
            CoRxDCTestInstruments.IsTestAuxPD = TestParmConfig.GetBoolParam("IsTestAux");
            

            string modname; 
            DatumList tmData ;
            
            #region Verifying splitter ratio
            engine.GuiToFront();
                        
            if (TestParmConfig.GetBoolParam("IsVerifySr"))
            {
                modname = "Verify Signal Path Splitter Ratio";
                engine.SendToGui(modname);
                engine.GuiUserAttention();
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the Signal laser to its monitor power head then continue"));
                GuiMsg.CoRxSwitchConnectorCompleted rspFiberSwitch = null;
                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);

                engine.GuiCancelUserAttention();
                engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
                CoRxDCTestInstruments.SelectItla(itla_sig);
                engine.RunModule(modname);
                                
                CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
                System.Threading.Thread.Sleep(10);
                CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

                CoRxDCTestInstruments.SelectItla(itla_loc);
                CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
                
                engine.GuiToFront();
                engine.GuiUserAttention();
                engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the Signal laser output Connector to DUT then continue"));

                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);
                               
                modname = "Verify Local Osc Path Splitter Ratio";
                engine.SendToGui(modname);
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the local OSC laser to its monitor power head then continue"));

                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);

                engine.GuiCancelUserAttention();
                engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
                engine.RunModule(modname);
                CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
                System.Threading.Thread.Sleep(100);
                CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

                engine.GuiToFront();
                engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
                engine.SendToGui(new GuiMsg.CoRxConnectorSwitchRqst(" Please insert the local OSC laser output Connector to DUT then continue"));

                do
                {
                    rspFiberSwitch = engine.ReceiveFromGui().Payload as GuiMsg.CoRxSwitchConnectorCompleted;
                }
                while (rspFiberSwitch == null);
            }

            #endregion

            engine.GuiToFront();
            engine.SendToGui(" DUT Test started, don't Move the DUT or fibre Connector without  message promoted");
            
            modname = string.Format("Setting case temp to {0}", TestTemperatures[0]);            
            tmData = engine.RunModule(modname).ModuleRunData.ModuleData;
            try
            {
                isModuleRunOk = tmData.ReadSint32("IsTemperatureSetOK") == 1 ;
            }
            catch //(Exception ex)
            {
                engine.SendToGui("Case temperature can't set to " + TestTemperatures[0].ToString() + "C");
                isModuleRunOk = false;
            }
            if (!isModuleRunOk)
            {
                userOpt = engine.ShowYesNoUserQuery("Case temperature can't set to " + 
                        TestTemperatures[0].ToString() + "C\n Would you like to continue the sequencial temperature test or not?");
                if (userOpt == ButtonId.No)
                    engine.RaiseNonParamFail(0, "Fail to control case temperature to " + TestTemperatures[0].ToString());
            }

            bool tempFlagContinue =TestParmConfig.GetBoolParam("IsAbortTest@DarkCurrentFail");
            engine .GuiToFront();
            
            engine .SendToGui(new GuiMsg .CautionOfLaserRqst());
            engine.SendToGui(" DUT Test started, don't Move the DUT or fibre Connector without  message promoted");

            if (TestParmConfig.GetBoolParam("IsTestAux"))
            {
                engine.RunModule("Auxilary pd Dark Current Test", !tempFlagContinue);
            }

            int idx_Temp = 0;  // Start from normal temperature 
            double temp;
            double temp_From = 0;
            double temp_To = 0;
            DatumList VoaSweepPlotsNormalTemp = new DatumList();
            do  // Loop around all temperatures
            {
                engine.GuiShow();
                engine.GuiToFront();
                temp = TestTemperatures[idx_Temp];
                bool IgnoreCode = false;                // Debug code to allow testing of VOA only to reduce run time
                if (!IgnoreCode) 
                {
                    // PD dark current 
                    modname = string.Format("PD Dark Current Test {0}C", temp.ToString().Replace('-', 'M'));
                    engine.RunModule(modname);

                    if (TestParmConfig.GetBoolParam("IsTestAux"))
                    {
                        // Test Coupling Efficiency - this is done on the Aux PD so only do this test if we are testing the Aux devices
                        modname = string.Format("Coupling Efficiency test @{0}C", temp.ToString().Replace('-', 'M'));
                        engine.RunModule(modname, !isPromoteContinueOnFail, isAbortTestOnFail);
                    }
                    // Test pd responsivity
                    modname = string.Format("Responsivity test @{0}C", temp.ToString().Replace('-', 'M'));
                    engine.RunModule(modname, !isPromoteContinueOnFail, isAbortTestOnFail);
                }
                //Test VOA
                if (TestParmConfig.GetBoolParam("IsTestVOA"))
                {
                    //if (dutObject.TestStage.ToLower().Contains("spc") && (NormalTemperature != temp))
                    //{

                    //}
                    //else
                    //{
                        modname = string.Format("VOA test @{0}C", temp.ToString().Replace('-', 'M'));

                        if (NormalTemperature != temp)
                        {

                            engine.GetModuleRun(modname).PreviousTestData.AddListItems(VoaSweepPlotsNormalTemp);

                        }

                        ModuleRunReturn moduleRunReturn = engine.RunModule(modname, !isPromoteContinueOnFail, isAbortTestOnFail);


                        if (NormalTemperature == temp)
                        {
                            string VOASweepStartPath = moduleRunReturn.ModuleRunData.ModuleData.ReadFileLinkFullPath("VOASweepDataFileStart");
                            string VOASweepStopPath = moduleRunReturn.ModuleRunData.ModuleData.ReadFileLinkFullPath("VOASweepDataFileStop");
                            VoaSweepPlotsNormalTemp.AddOrUpdateFileLink(string.Format("VOASweepDataFileStart{0}C", temp.ToString().Replace('-', 'M')), VOASweepStartPath);
                            VoaSweepPlotsNormalTemp.AddOrUpdateFileLink(string.Format("VOASweepDataFileStop{0}C", temp.ToString().Replace('-', 'M')), VOASweepStopPath);
                            VoaSweepPlotsNormalTemp.AddOrUpdateDouble("Temp", temp);//also store normal temperature information for the data
                        }
                    //}


                }
                // If the last temperature, recover to normal temperature and end  temperature loop 
                if (idx_Temp >= TestTemperatures.Length - 1)  
                {
                    double temp_Recover = TestParmConfig.GetDoubleParam("RecoverTemperature");
                    modname = string.Format("Recover from {0} to {1}", temp, temp_Recover);
                    engine.RunModule(modname);
                    break;                   
                }

                // set case temperature to the next
                do
                {
                    temp_From = TestTemperatures[idx_Temp];
                    temp_To = TestTemperatures[++idx_Temp];
                    modname = string.Format("Setting case temp to {0}", temp_To);
                   
                    try
                    {
                        tmData = engine.RunModule(modname).ModuleRunData.ModuleData;
                        isModuleRunOk = tmData.ReadSint32("IsTemperatureSetOK") == 1;
                    }
                    catch // (Exception ex)
                    {
                        engine.SendToGui("Case temperature can't set to " + temp_To.ToString() + "C");
                        isModuleRunOk = false;
                    }
                    if (!isModuleRunOk)
                    {
                        userOpt = engine.ShowYesNoUserQuery("Case temperature can't set to " +
                            temp_To.ToString() + "C\n Would you like to continue the sequencial temperature test or not?");
                        if (userOpt != ButtonId.Yes)   // recover to normal temperature and abort test
                        {
                            double temp_Recover = TestParmConfig.GetDoubleParam("RecoverTemperature");
                            modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
                            engine.RunModule(modname);
                            break;
                        }
                    }
                } while (!isModuleRunOk && (idx_Temp< TestTemperatures.Length-1) );

                if (!isModuleRunOk)  // if the case temperature can't set, abort the test
                    break;
            }
            while (true);
        }
        /// <summary>
        /// post run phase , shut down instruemnts
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="instrs"></param>
        /// <param name="chassis"></param>
        public void PostRun(ITestEnginePostRun engine, DUTObject dutObject, 
                    InstrumentCollection instrs, ChassisCollection chassis)
        {
            if (engine.IsSimulation) return;
            // TODO: shutdown actions
            CoRxDCTestInstruments.SelectItla(itla_sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.SelectItla(itla_loc);            
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            //CoRxDCTestInstruments .
            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            if (CoRxDCTestInstruments.IsBiasPDTap) CoRxDCTestInstruments.PdTapSource.OutputEnabled = false;
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);

            bool bEnableInterLock = InstrParamConfig.GetBoolParam("IsSourceWithInterLock");
            if (bEnableInterLock) CoRxDCTestInstruments.SetSourceInterLock(false);

            //KD10
            CoRxDCTestInstruments.TopCaseTec.OutputEnabled = false;//modified by chaoqun
        }
        /// <summary>
        /// data write phase
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <param name="dutOutcome"></param>
        /// <param name="results"></param>
        /// <param name="userList"></param>
        public void DataWrite(ITestEngineDataWrite engine, DUTObject dutObject, 
                    DUTOutcome dutOutcome, TestData results, UserList userList)
        {
            // Write keys required for external data (example below for PCAS)  
            engine.BringTestResultsTabToFront();

            
            StringDictionary keys = new StringDictionary();
            // TODO: MUST Add real values below!
            keys.Add("SCHEMA", TestParmConfig.GetStringParam("PCASSchema"));
            keys.Add("DEVICE_TYPE", dutObject.PartCode);
            keys.Add("TEST_STAGE", dutObject.TestStage);
            keys.Add("SPECIFICATION", this.specName);
            keys.Add("SERIAL_NO", dutObject.SerialNumber);
            // Tell Test Engine about it...
            engine.SetDataKeys(keys);

            // Add any other data required for external data (example below for PCAS)
            // !(beware, all these parameters MUST exist in the specification)!...
            DatumList traceData = new DatumList();

            DateTime  testTime_End = DateTime.Now;
            TimeSpan testTime = testTime_End.Subtract(time_Start);
            traceData.AddDouble("TEST_TIME", testTime.TotalMinutes);
            ////traceData.AddString("SPEC_ID", MainSpec.Name);
            //traceData .AddString ("TIME_DATE", time_Start.ToString ("yyyyMMddHHmmss"));

            traceData.AddSint32 ("NODE", dutObject.NodeID);            
            traceData.AddString("EQUIP_ID", dutObject.EquipmentID);
            ////traceData.AddString("PART_CODE", dutObject.PartCode);

            if (engine.GetProgramRunStatus() == ProgramStatus.Success)
            {
                traceData.AddString("TEST_STATUS", engine.GetOverallPassFail().ToString());
            }
            else
            {
                string msg = engine.GetProgramRunStatus().ToString();
                string runInfo = msg.Substring(0, (msg.Length > 14 ? 14 : msg.Length));
                traceData.AddString("TEST_STATUS", runInfo);
            }

            traceData.AddSint32("DEVICE_COMPLETE", 1);
            traceData.AddString("MATRIX_NUMBER", "UNKNOWN");
            traceData.AddString("SOFTWARE_ID",                
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            traceData.AddString("OPERATOR", userList.UserListString);
            //traceData.AddString("COMMENTS", engine.GetProgramRunComments());
            if (ChipsInfoList != null) traceData.AddListItems(ChipsInfoList);


            // Dummy data for temporary fix missing data issue
            traceData.AddString("COMMENTS", "NONE");
            traceData.AddString("COMPUTER_ID", "UNKNOWN");
            traceData.AddDouble("DATE_1900", -1);
            traceData.AddString("DAY", "NA");
            traceData.AddDouble("IDLE_TIME", -1);

            //XIAOJIANG 2017.03.20
            if (this.MainSpec.ParamLimitExists("LOT_TYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    traceData.AddOrUpdateString("LOT_TYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    traceData.AddOrUpdateString("LOT_TYPE", "Unknown");

            if (this.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTTYPE"))
                if (dutObject.Attributes.IsPresent("lot_type"))
                    traceData.AddOrUpdateString("FACTORY_WORKS_LOTTYPE", dutObject.Attributes.ReadString("lot_type"));
                else
                    traceData.AddOrUpdateString("FACTORY_WORKS_LOTTYPE", "Unknown");

            if (this.MainSpec.ParamLimitExists("LOT_ID"))
                traceData.AddOrUpdateString("LOT_ID", dutObject.BatchID);
            if (this.MainSpec.ParamLimitExists("FACTORY_WORKS_LOTID"))
                traceData.AddOrUpdateString("FACTORY_WORKS_LOTID", dutObject.BatchID);
            //END ADD XIAOJIANG 2017.03.20




            traceData.AddDouble("PROCESS_TIME", engine.GetTestTime());
            traceData.AddSint32("RELEASE_NO", -1);
            traceData.AddString("SIGNATURE", "UNKNOWN");
            traceData.AddString("TIME", "12:00:00");
            //traceData.AddString("TIME_DATE", "20000101120000");
            traceData.AddString("TRACK_ACTION", "NONE");
            traceData.AddString("TRACK_CODE", "NONE");
            traceData.AddSint32("TRACK_IN", 1);
            traceData.AddString("VARIANT_TYPE", "NONE");
            traceData.AddString("WAFER_ID_X", "UNKNOWN");
            traceData.AddString("WAFER_ID_Y", "UNKNOWN");
            traceData.AddSint32("WEEK_NUMBER", -1);
            traceData.AddString("X_CHIP_BIN", "UNKNOWN");
            traceData.AddString("X_CHIP_ID", "UNKNOWN");
            traceData.AddString("X_COC_SN", "UNKNOWN");
            traceData.AddString("Y_CHIP_BIN", "UNKNOWN");
            traceData.AddString("Y_CHIP_ID", "UNKNOWN");
            traceData.AddString("Y_COC_SN", "UNKNOWN");


            traceData.AddDouble("VOLTAGE_PD", TestParmConfig.GetDoubleParam("Vpd_V"));
            traceData.AddDouble("VOLTAGE_TIA", TestParmConfig.GetDoubleParam("Vcc_V"));

            #region Fill in blank TC and CH results
            // Add 'missing data' for TC_* and CH_* parameters
            foreach (ParamLimit paramLimit in MainSpec)
            {
                if (paramLimit.ExternalName.StartsWith("TC_") || paramLimit.ExternalName.StartsWith("TC1_") || paramLimit.ExternalName.StartsWith("TC2_"))
                {
                    if (!traceData.IsPresent(paramLimit.ExternalName))
                    {
                        Datum dummyValue = null;
                        switch (paramLimit.ParamType)
                        {
                            case DatumType.Double:
                                dummyValue = new DatumDouble(paramLimit.ExternalName, Convert.ToDouble(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.Sint32:
                                dummyValue = new DatumSint32(paramLimit.ExternalName, Convert.ToInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                            case DatumType.StringType:
                                dummyValue = new DatumString(paramLimit.ExternalName, "MISSING");
                                break;
                            case DatumType.Uint32:
                                dummyValue = new DatumUint32(paramLimit.ExternalName, Convert.ToUInt32(paramLimit.HighLimit.ValueToString()));
                                break;
                        }
                        if (dummyValue != null)
                            traceData.Add(dummyValue);
                    }
                }
            }
            #endregion

            if (dutObject.TestStage.ToLower().Contains("spc"))
            {
                DatumList spcList = new DatumList();

                foreach (ParamLimit item in this.MainSpec)
                {
                    if (item.MeasuredData == null)
                    {
                        continue;
                    }

                    switch (item.MeasuredData.Type)
                    {
                        case DatumType.BoolType:
                            spcList.AddBool(item.ExternalName, Convert.ToBoolean(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.DatumListType:
                            break;
                        case DatumType.Double:
                            spcList.AddDouble(item.ExternalName, Convert.ToDouble(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.DoubleArray:
                            break;
                        case DatumType.EnumType:
                            break;
                        case DatumType.FileLinkType:
                            break;
                        case DatumType.ObjectReference:
                            break;
                        case DatumType.ObjectValue:
                            break;
                        case DatumType.PlotType:
                            break;
                        case DatumType.Sint32:
                            spcList.AddSint32(item.ExternalName, Convert.ToInt32(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.StringArray:
                            break;
                        case DatumType.StringType:
                            break;
                        case DatumType.Uint32:
                            spcList.AddUint32(item.ExternalName, Convert.ToUInt32(item.MeasuredData.ValueToString()));
                            break;
                        case DatumType.Unknown:
                            break;
                        default:
                            break;
                    }
                }

                dutObject.Attributes.AddOrUpdateReference("SPC_RESULTS", spcList);


                SpcConfigReader.Initialise(dutObject.NodeID.ToString(), dutObject.TestStage);
                DatumList listSPCParams = SpcHelper.ReadSpcParams(dutObject.NodeID.ToString());

                if (listSPCParams != null)
                {
                    foreach (Datum item in listSPCParams)
                    {
                        if (spcList.GetDatumNameList().Contains(item.Name))
                        {
                            double measureVule = spcList.ReadDouble(item.Name);
                            string paramNameTemp;
                            if (item.Name.Length > 26)
                            {
                                paramNameTemp = item.Name.Replace("_SIG", "");
                                traceData.AddOrUpdateDouble("DEL_" + paramNameTemp, measureVule - double.Parse(item.ValueToString()));
                            }
                            else
                            {
                                traceData.AddOrUpdateDouble("DEL_" + item.Name, measureVule - double.Parse(item.ValueToString()));
                            }
                        }
                    }
                }
            }
            // pick the specification to add this data to...
            engine.SetTraceData(specName, traceData);

        }

        #endregion


        #region Initialise function

        /// <summary>
        /// Load config file for test parameters, temperature setting and instrument calibration
        /// </summary>
        /// <param name="dutObj"></param>
        void LoadConfigFiles(ITestEngineInit engine, DUTObject dutObj)
        {
            engine.SendStatusMsg("Load configuration files");
            
            // temperature setting parameters
            string configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\TempTable.xml", dutObj.NodeID);
            TempParamConfig = new TempTableConfigAccessor(dutObj, 1, configFilePath);

            // test parameters
            TestParmConfig = new TestParamConfigAccessor(dutObj,
               @"Configuration\CoRxDcTest\DCTestParams.xml", "ALL", "CoRxDCTestParams");

            // settings depends on instruments
            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrumentParams.xml", dutObj.NodeID);
            InstrParamConfig = new TestParamConfigAccessor(dutObj, configFilePath,"ALL", "InstrumentParams");
            
            // instrument calibration data
            configFilePath = string.Format(@"Configuration\CoRxDcTest\node{0}\InstrsCalibration.xml", dutObj.NodeID);
            InstrumentsCalData = new ConfigDataAccessor(configFilePath, "Calibration");

            // Get Test temperatures
            List<double> tempList = new List<double>();
            NormalTemperature = TestParmConfig.GetDoubleParam("NormalTemperature");
            tempList.Add(NormalTemperature);
            
            string TemperatureList = TestParmConfig.GetStringParam("TemperatureArrayToTest");

            if (TemperatureList.Length != 0)
            {
                string[] tempStrArr = TestParmConfig.GetStringParam("TemperatureArrayToTest").Split(',');
                if (tempStrArr != null && tempStrArr.Length > 0)
                {
                    foreach (string temp in tempStrArr)
                    {

                        if (temp.Trim() != "")
                        {
                            tempList.Add(double.Parse(temp));
                        }
                    }
                }
            }
            TestTemperatures = tempList.ToArray();

            #region Get Freq/Wavelen/Chan array to verify SR
            // the frequency information might in frequency, wavelen or channel
            string freqArrStr = "";
            try
            {
                freqArrStr = TestParmConfig.GetStringParam("Freq2VerifySR");
                FreqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            catch 
            {
                try
                {
                    freqArrStr = TestParmConfig.GetStringParam("Chan2VerifySR");
                    FreqSetMode = EnumFrequencySetMode.ByChan;
                }
                catch
                {
                    try
                    {
                        freqArrStr = TestParmConfig.GetStringParam("Wavelen2VerifySR");
                        FreqSetMode = EnumFrequencySetMode.ByWavelen;
                    }
                    catch
                    {
                    }
                }
            }
            if (freqArrStr.Trim() != "")
            {
                string[] freqArrTemp = freqArrStr.Split(',');
                List<double> freqList = new List<double>();
                foreach (string freqTemp in freqArrTemp)
                {
                    freqList.Add(double.Parse(freqTemp));
                }
                FreqArr2VerifySR = freqList.ToArray();
            }
            else
            {
                FreqArr2VerifySR = null;
            }
            #endregion

        }

        /// <summary>
        /// load specifications
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        /// <returns></returns>
        private SpecList LoadSpecs(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendStatusMsg("Loading Specification");
            ILimitRead limitReader = null;
            StringDictionary mainSpecKeys = new StringDictionary();
            if (TestParmConfig.GetBoolParam("UseLocalLimitFile"))
            {
                // initialise limit file reader
                limitReader = engine.GetLimitReader("PCAS_FILE");
                // main specification

                #region Get local Limit file path
                // initialise limit file reader
                string limitFileName = "";
                string localLimitFileDir = TestParmConfig.GetStringParam("PcasLimitFileDirectory");

                StringDictionary keys = new StringDictionary();
                keys.Add("TestStage", dutObject.TestStage);
                keys.Add("DeviceType", dutObject.PartCode);
                ConfigDataAccessor localLimitConfig = new ConfigDataAccessor(
                    Path.Combine(localLimitFileDir, TestParmConfig.GetStringParam("LocalLimitStragety")),
                    TestParmConfig.GetStringParam("LocalLimitTableName"));

                // Get file name from data base
                try
                {
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }
                catch
                {
                    keys.Remove("DeviceType");
                    System.Threading.Thread.Sleep(100);
                    keys.Add("DeviceType", "ALL");
                    limitFileName = localLimitConfig.GetData(keys, false).ReadString("LocalLimitFileName").Trim();
                }

                if (limitFileName != "")
                {
                    limitFileName = Path.Combine(localLimitFileDir, limitFileName);
                    if (File.Exists(limitFileName))
                        mainSpecKeys.Add("Filename", limitFileName);
                    else
                        engine.ErrorInProgram("Can't find limit file at " + limitFileName);

                }  //LimitFileName!= ""
                else
                {
                    engine.ErrorInProgram("No local limit file available for device type " + dutObject.PartCode);
                }                
                #endregion

            }
            else  // get pcas limit file
            {
                // Use default limit source
                limitReader = engine.GetLimitReader();

                mainSpecKeys.Add("SCHEMA", "HIBERDB");
                mainSpecKeys.Add("TEST_STAGE", dutObject.TestStage);
                mainSpecKeys.Add("DEVICE_TYPE", dutObject.PartCode);
            }

            // load main specification

            SpecList tempSpecList = limitReader.GetLimit(mainSpecKeys);
            // Get our specification object (so we can initialise modules with appropriate limits)
            MainSpec = tempSpecList[0];

            SpecList specList = new SpecList();
            specName = MainSpec.Name;
            specList.Add(MainSpec);
            engine.SetSpecificationList(specList);
            return specList;
        }

        /// <summary>
        /// Retrieve & verify test parameter from previous stage
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObj"></param>
        /// <returns></returns>
        bool ValidatePrevStageData(ITestEngineInit engine, DUTObject dutObj)
        {
            bool isDataRetrieved = false;

            bool isPcasData = TestParmConfig.GetBoolParam("IsRetrieveChipInfoFromPcas");
            if (isPcasData)
            {
                engine.SendStatusMsg("Retrieving previous data from PCAS...");
                IDataRead dataRead = engine.GetDataReader("PCAS_SHENZHEN");

                //2008-3-24: by Ken: Determine whether tcmz_map stage tested.
                StringDictionary preStageKeys = new StringDictionary();
                preStageKeys.Add("SCHEMA", TestParmConfig.GetStringParam("ChipInfoPcasSchema"));
                preStageKeys.Add("SERIAL_NO", dutObj.SerialNumber);
                preStageKeys.Add("TEST_STAGE", TestParmConfig.GetStringParam("StageName2RetrieveChipInfo"));
                
                DatumList prevData = dataRead.GetLatestResults(preStageKeys, true);

                if (prevData == null)
                {
                    string errorDescription = string.Format("No data be retrieve from {0} stage {1}," +
                        " could you please check the previous data to ensure enough data for this stage?",
                        TestParmConfig.GetStringParam("ChipInfoPcasSchema"),
                        TestParmConfig.GetStringParam("StageName2RetrieveChipInfo"));
                    engine.ErrorInProgram(errorDescription);
                }
                try
                {
                    ChipsInfoList = new DatumList();
                    ChipsInfoList.AddString("X_CHIP_ID", prevData.ReadString("X_CHIP_ID"));
                    ChipsInfoList.AddString("X_CHIP_BIN", prevData.ReadString("X_CHIP_BIN"));
                    ChipsInfoList.AddString("X_COC_SN", prevData.ReadString("X_COC_SN"));
                    ChipsInfoList.AddString("X_WAFER_ID", prevData.ReadString("X_WAFER_ID"));
                    ChipsInfoList.AddString("Y_CHIP_ID", prevData.ReadString("Y_CHIP_ID"));
                    ChipsInfoList.AddString("Y_CHIP_BIN", prevData.ReadString("Y_CHIP_BIN"));
                    ChipsInfoList.AddString("Y_COC_SN", prevData.ReadString("Y_COC_SN"));
                    ChipsInfoList.AddString("Y_WAFER_ID", prevData.ReadString("Y_WAFER_ID"));
                    isDataRetrieved = true;
                }
                catch
                {
                    isDataRetrieved = false;
                    engine.ErrorInProgram("Not all neccesary data required by this stage is retrieve from previous stage," +
                        "\nTest on this stage can't go on, please check the previous data");
                }
            }
            else
            {
                engine.GuiToFront();
                engine.SendToGui(new GuiMsg.GetChipInfoRqst());
                engine.GuiUserAttention();
                engine.SendStatusMsg("Constructing engineering previous test data");

                DatumList previousData = (DatumList)engine.ReceiveFromGui().Payload;
                engine.GuiCancelUserAttention();
                if (previousData == null || previousData.Count < 3)
                    engine.ErrorInProgram("Not enough previous data entry to carry test on this stage");
                ChipsInfoList = new DatumList();
                ChipsInfoList.AddListItems(previousData);
                isDataRetrieved = true;
            }
            return isDataRetrieved;
        }

        /// <summary>
        /// Initialise instruments
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="instrs"></param>
        private void InitialiseInstrs(ITestEngineInit engine, InstrumentCollection instrs)
        {
            engine.SendToGui("Initialising instruments ...");
            engine.SendStatusMsg("Initialising instruments ...");
            #region Assign instruments
             
            CoRxDCTestInstruments.SignalInputInstrs = new CoRxSourceInstrsChain();

            itla_sig = new Inst_CoRxITLALaserSource("iTLA_Sig",(Instr_ITLA)instrs["iTLA_Sig"]);
            CoRxDCTestInstruments.SignalInputInstrs.LaserSource = itla_sig ;
            CoRxDCTestInstruments.SignalInputInstrs.Voa = null; 
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_SigMon"];
            CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_SigRef"];

            CoRxDCTestInstruments.LocalOscInputInstrs = new CoRxSourceInstrsChain();
            itla_loc = new Inst_CoRxITLALaserSource("iTLA_Loc",(Instr_ITLA)instrs["iTLA_Loc"]);
            CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource =   itla_loc;
            CoRxDCTestInstruments.LocalOscInputInstrs.Voa = null; 
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon = (InstType_OpticalPowerMeter)instrs["Opm_LocMon"];
            CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref = (InstType_OpticalPowerMeter)instrs["Opm_LocRef"];

            CoRxDCTestInstruments.TopCaseTec = (Inst_Nt10a)instrs["CaseTec"];
            CoRxDCTestInstruments.IsDualCaseTecControl = InstrParamConfig.GetBoolParam("IsUseBottomTEC");
            if (CoRxDCTestInstruments.IsDualCaseTecControl)
            {
                CoRxDCTestInstruments.BottomCaseTec = (Inst_Nt10a)instrs["BottomCaseTec"];
            }
            else
                CoRxDCTestInstruments.BottomCaseTec = null;

            CoRxDCTestInstruments.PdSource_AUX_XR = (Inst_Ke24xx)instrs["AuxPdSource_XR"];
            CoRxDCTestInstruments.PdSource_AUX_XL = (Inst_Ke24xx)instrs["AuxPdSource_XL"];
            CoRxDCTestInstruments.PdSource_AUX_YL = (Inst_Ke24xx)instrs["AuxPdSource_YL"];
            CoRxDCTestInstruments.PdSource_AUX_YR = (Inst_Ke24xx)instrs["AuxPdSource_YP"];
            CoRxDCTestInstruments.IsAuxPDOnFront = InstrParamConfig.GetBoolParam("IsAuxPdOnFrontTernimal");

            CoRxDCTestInstruments.PdSource_XI1 = (Inst_Ke24xx)instrs["PdSource_XI1"];
            CoRxDCTestInstruments.PdSource_XI2 = (Inst_Ke24xx)instrs["PdSource_XI2"];
            CoRxDCTestInstruments.PdSource_XQ1 = (Inst_Ke24xx)instrs["PdSource_XQ1"];
            CoRxDCTestInstruments.PdSource_XQ2 = (Inst_Ke24xx)instrs["PdSource_XQ2"];

            CoRxDCTestInstruments.PdSource_YI1 = (Inst_Ke24xx)instrs["PdSource_YI1"];
            CoRxDCTestInstruments.PdSource_YI2 = (Inst_Ke24xx)instrs["PdSource_YI2"];
            CoRxDCTestInstruments.PdSource_YQ1 = (Inst_Ke24xx)instrs["PdSource_YQ1"];
            CoRxDCTestInstruments.PdSource_YQ2 = (Inst_Ke24xx)instrs["PdSource_YQ2"];
            CoRxDCTestInstruments.TriggerInLine = InstrParamConfig.GetIntParam("SlaveSourceTriggerInLine");
            CoRxDCTestInstruments.TriggerOutLine = InstrParamConfig.GetIntParam("SlaveSourceTriggerOutputLine");
            CoRxDCTestInstruments.TriggerNoUsedLine = InstrParamConfig.GetIntParam("SlaveSourceTriggerNoUseLine");

            CoRxDCTestInstruments.TiaSource_X = (Inst_Ke24xx)instrs["TIASource_X"];
            CoRxDCTestInstruments.TiaSource_Y = (Inst_Ke24xx)instrs["TIASource_Y"];

            CoRxDCTestInstruments.inSPI = (Instr_SPI)instrs["inSPI"];

            CoRxDCTestInstruments.IsBiasPDTap = TestParmConfig. GetBoolParam("IsPdTapBias");
            CoRxDCTestInstruments.PdTapSource = null;

            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                try
                {
                    CoRxDCTestInstruments.PdTapSource = (Inst_Ke24xx)instrs["TapSource"];
                }
                catch
                {
                    engine.ErrorInProgram("No intrument avialable to bias Tap PD");
                }
            }


            CoRxDCTestInstruments.VOASource = null;
            try
            {
                CoRxDCTestInstruments.VOASource = (Inst_Ke24xx)instrs["VoaSource"];
            }
            catch
            { engine.ErrorInProgram("No instrument available to bias VOA"); }
            
            try
            {
                CoRxDCTestInstruments.GCSource_X = (InstType_ElectricalSource)instrs["GCSource_X"];
                CoRxDCTestInstruments.GCSource_Y = (InstType_ElectricalSource)instrs["GCSource_Y"];
                CoRxDCTestInstruments.MCSource_X = (InstType_ElectricalSource)instrs["MCSource_X"];
                CoRxDCTestInstruments.MCSource_Y = (InstType_ElectricalSource)instrs["MCSource_Y"];
                CoRxDCTestInstruments.OaSource_X = (InstType_ElectricalSource)instrs["OaSource_X"];
                CoRxDCTestInstruments.OaSource_Y = (InstType_ElectricalSource)instrs["OaSource_Y"];
                CoRxDCTestInstruments.OCSource_X = (InstType_ElectricalSource)instrs["OcSource_X"];
                CoRxDCTestInstruments.OCSource_Y = (InstType_ElectricalSource)instrs["OcSource_Y"];
                CoRxDCTestInstruments.ThermPhaseSource_L = (InstType_ElectricalSource)instrs["ThermPhaseSource_L"];
                CoRxDCTestInstruments.ThermPhaseSource_R = (InstType_ElectricalSource)instrs["ThermPhaseSource_R"];
            }
            catch
            {
                // ignore the control pins' intrument exception
            }
            #endregion

            #region setting measurement  parameters            

            CoRxDCTestInstruments.LocalOscInputInstrs.sngVoaDefAtt_dB = TestParmConfig.GetDoubleParam("InitAtt_LocVoa_dB");
            CoRxDCTestInstruments.SignalInputInstrs.sngVoaDefAtt_dB = TestParmConfig.GetDoubleParam("InitAtt_SigVoa_dB");

            CoRxDCTestInstruments.SignalInputInstrs.PowerSetTolerance_dB = TestParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.PowerSetTolerance_dB = TestParmConfig.GetDoubleParam("MaxOptPowerTolerance_dB");
            CoRxDCTestInstruments.LocalOscInputInstrs.MaxPowerTuningCount = TestParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");
            CoRxDCTestInstruments.SignalInputInstrs.MaxPowerTuningCount = TestParmConfig.GetDoubleParam("MaxOptPowerAdjustCycles");

            string configFilePath = InstrParamConfig.GetStringParam("SplitterCalDataBaseConfigPath");
            string configTableName = InstrParamConfig.GetStringParam("SplitterCalDataBaseConfigTableName");
            string sigSplitterName = InstrParamConfig.GetStringParam("SignalPathSplitterName");
            string locSplitterName = InstrParamConfig.GetStringParam("LocPathSplitterName");

            try
            {
                CoRxDCTestInstruments.SignalInputInstrs.SetupSplitterRatioData(
                    sigSplitterName, configFilePath, configTableName);
                CoRxDCTestInstruments.SignalInputInstrs.SplitterRatiosCalData.FrequencyTolerance_Ghz =
                    InstrParamConfig.GetDoubleParam("FreqTolerance_Ghz");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.SignalInputInstrs.IsSplitterRatioCalOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Signal path, Test have to Abort!");

            try
            {
                CoRxDCTestInstruments.LocalOscInputInstrs.SetupSplitterRatioData(
                    locSplitterName, configFilePath, configTableName);
                CoRxDCTestInstruments .LocalOscInputInstrs .SplitterRatiosCalData.FrequencyTolerance_Ghz =
                    InstrParamConfig.GetDoubleParam("FreqTolerance_Ghz");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            if (!CoRxDCTestInstruments.LocalOscInputInstrs.IsSplitterRatioCalOK)
                engine.ErrorInProgram(" The Splitter ratio Cal data is not available for Loc oscilatior path, Test have to Abort!");
                        
            #endregion

            #region Initialise instruments

            if (!engine.IsSimulation)  // if testset on line, initialise instruments
            {
                // Seting powermeters calibration value
                double calFactor, calOffset;
                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.ReferencePower = calOffset;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Mon.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;

                ReadCalData(CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.ReferencePower = calOffset;
                CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.ReferencePower = calOffset;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;

                ReadCalData(CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Name, "all", "Power_dBm", out calFactor, out calOffset);
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.CalOffset_dB = calFactor;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.ReferencePower = calOffset;
                CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref.Range = Bookham.TestLibrary.InstrTypes.InstType_OpticalPowerMeter.AutoRange;

                CoRxDCTestInstruments.SignalInputInstrs.MaxShutDownPower_dBm = InstrParamConfig.GetDoubleParam("LocPathMaxShutDownPower_dBm");
                CoRxDCTestInstruments.LocalOscInputInstrs.MaxShutDownPower_dBm = InstrParamConfig.GetDoubleParam("SigPathMaxShutDownPower_dBm");
                

                // Shut down laser source for safe                
                CoRxDCTestInstruments.SelectItla(itla_sig);
                if (itla_sig != null) itla_sig.CurrentChan = 1;
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.OutputEnable = false;
                CoRxDCTestInstruments.SignalInputInstrs.LaserSource.Power_dBm = InstrParamConfig.GetDoubleParam("SignalSourcePower@Test_dBm");

                System.Threading.Thread.Sleep(100);
                CoRxDCTestInstruments.SelectItla(itla_loc);
                if (itla_loc != null ) itla_loc.CurrentChan = 1;
                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.OutputEnable = false;
                CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

                CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource.Power_dBm = InstrParamConfig.GetDoubleParam("LocSourcePower@Test_dBm");
                // Zero all power meter modules' dark current
                //try
                //{
                //    Inst_Ag816x_OpticalPowerMeter inst_8163 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Mon as Inst_Ag816x_OpticalPowerMeter;
                //    if ( inst_8163 != null )
                //    {
                //    inst_8163.SafeMode = false;
                //    inst_8163.Range = InstrParamConfig.GetDoubleParam("OpmRange_LocPath Mon_dBM");
                //    inst_8163.ZeroAllModuleDarkCurrent();
                //    }

                //    Inst_Ag816x_OpticalPowerMeter inst_8163_3 = CoRxDCTestInstruments.SignalInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                //    if (inst_8163_3 != null )
                //    {
                //    inst_8163_3.Range = InstrParamConfig.GetDoubleParam("OpmRange_SigPathRef_dBM");
                //    inst_8163_3.SafeMode = false;
                //    }

                //    Inst_Ag816x_OpticalPowerMeter inst_8163_2 = CoRxDCTestInstruments.LocalOscInputInstrs.OPM_Ref as Inst_Ag816x_OpticalPowerMeter;
                //    if (inst_8163_2 != null )
                //    {
                //    inst_8163_2.Range = InstrParamConfig.GetDoubleParam("OpmRange_LocPath Ref_dBM");
                //    inst_8163_2.SafeMode = false;
                //    if (!object.ReferenceEquals(inst_8163.InstrumentChassis, inst_8163_2.InstrumentChassis))
                //        inst_8163_2.ZeroAllModuleDarkCurrent();
                //    }
                    
                //}
                //catch
                //{ }
                               
                // Initialise case tec controler
                CoRxDCTestInstruments.TopCaseTec.OutputEnabled = false;
                CoRxDCTestInstruments.TopCaseTec.OperatingMode = InstType_TecController.ControlMode.Temperature;
                CoRxDCTestInstruments.TopCaseTec.ProportionalGain = InstrParamConfig.GetDoubleParam("TecCase_PropotionGain");
                CoRxDCTestInstruments.TopCaseTec.IntegralGain = InstrParamConfig.GetDoubleParam("TecCase_IntegrationGain");
                CoRxDCTestInstruments.TopCaseTec.TecCurrentCompliance_amp = InstrParamConfig.GetDoubleParam("TecCase_CurrentCompliance_amp");
                CoRxDCTestInstruments.TopCaseTec.SensorTemperatureSetPoint_C = TestTemperatures[0];
                CoRxDCTestInstruments.TopCaseTec.OutputEnabled = true;

                // initialise bias sources
                CoRxDCTestInstruments.InitialiseAllSource();
                bool bEnableInterLock = InstrParamConfig.GetBoolParam("IsSourceWithInterLock");
                CoRxDCTestInstruments.SetSourceInterLock(bEnableInterLock);
            }
            #endregion
        }

        /// <summary>
        /// Initialise modules for all case temperatue ivolves in the whole test 
        /// </summary>
        /// <param name="engine"></param>
        void InitCaseTempModule(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising case temperature controlling modules");
            
            // Setup 25 ~ 25c module
            double temp_From = TestParmConfig.GetDoubleParam("RoomTemperature");
            double temp_To = TestTemperatures[0];
            string modname = string.Format("Setting case temp to {0}", temp_To);
            SetCaseTempControl(engine, modname, temp_From, temp_To,true );

            double temp_Recover = TestParmConfig.GetDoubleParam("RecoverTemperature");
            modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
            SetCaseTempControl(engine, modname, temp_To, temp_Recover, false);
           
            if (TestTemperatures.Length > 1)
            {
                
                for (int indx = 1; indx < TestTemperatures.Length; indx++)
                {
                    temp_From = TestTemperatures[indx-1];
                    temp_To = TestTemperatures[indx];
                    modname = string.Format("Setting case temp to {0}", temp_To);
                    SetCaseTempControl(engine, modname, temp_From, temp_To,true);

                    // add a module to recover from this temperature to the recovered temperature
                    modname = string.Format("Recover from {0} to {1}", temp_To, temp_Recover);
                    SetCaseTempControl(engine, modname, temp_To, temp_Recover,false);
                }
            }
            
        }

        /// <summary>
        /// Initialise case temperature control module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="modName"> module name </param>
        /// <param name="temp_From"> case temperature to start runing this module </param>
        /// <param name="temp_To">  case temperature to be achieved </param>
        /// <param name="isSaveReslt"> If link temperature setting result to limit file </param>
        void SetCaseTempControl(ITestEngineInit engine, string modName,
                            double temp_From, double temp_To, bool isSaveReslt)
        {
            // Get TEC Controller parameter from config file and set to it
            DatumDouble dt_timeout_s = new DatumDouble("MaxExpectedTimeForOperation_s",
                InstrParamConfig.GetDoubleParam("TecCase_Timeout_S"));
            DatumSint32 dt_intval; 
            double temp_setting = 0;
            TempTablePoint[] t_points;
            t_points = TempParamConfig.GetTemperatureCal(temp_From, temp_To);
            if (t_points.Length < 1)
                engine.ErrorInProgram(string.Format("Can't find temperature setting in temperature configure file" +
                    " with temperature from {0} to {1}", temp_From, temp_To));
            temp_setting = temp_To + t_points[0].OffsetC;

            ModuleRun module;
            if (CoRxDCTestInstruments .IsDualCaseTecControl)  // Stack tec control
            {
                // the following param varies with setting temp
                dt_intval = new DatumSint32("TimeBetweenReadings_ms",
                            InstrParamConfig.GetIntParam("TecCase_UpdateTime_mS"));
                DatumDouble TopTec_RqdTimeInToleranceWindow_s = new DatumDouble("TopTec_RqdTimeInToleranceWindow_s",
                    InstrParamConfig.GetDoubleParam("TecCase_InTolerance_S"));
                DatumDouble dt_TopTecStable_s = new DatumDouble("TopTec_RqdStabilisationTime_s", t_points[0].DelayTime_s);
                DatumDouble dt_TopTecSet = new DatumDouble("TopTec_SetPointTemperature_C", temp_setting);
                DatumDouble dt_TopTecTol = new DatumDouble("TopTec_TemperatureTolerance_C", t_points[0].Tolerance_degC);

                DatumDouble dt_BottomTecTimeout_s = new DatumDouble("BtmTec_RqdTimeInToleranceWindow_s",
                    InstrParamConfig.GetDoubleParam("BottomTecCase_InTolerance_S"));
                DatumDouble dt_BottomTecTstable_s = new DatumDouble("BtmTec_RqdStabilisationTime_s",
                    InstrParamConfig.GetDoubleParam("BottomTecCase_StabiliseTime_S"));
                DatumDouble dt_BottomTecDegSet = new DatumDouble("BtmTec_SetPointTemperature_C",
                    InstrParamConfig.GetDoubleParam(
                    "BottomTecCaseDegSet_" + temp_To.ToString("###").Replace('-', 'M') + "C"));
                DatumDouble BtmTec_TemperatureTolerance_C = new DatumDouble("BtmTec_TemperatureTolerance_C",
                    InstrParamConfig.GetDoubleParam("BottomTecCase_Tolerance_C"));

                module = engine.AddModuleRun(modName, "StackedTecControl", "");
                module.Instrs.Add("BtmTec_Controller", CoRxDCTestInstruments.BottomCaseTec);
                module.Instrs.Add("TopTec_Controller", CoRxDCTestInstruments.TopCaseTec);

                
                module.ConfigData.Add(dt_TopTecSet);
                module.ConfigData.Add(dt_TopTecStable_s);
                module.ConfigData.Add(dt_TopTecTol);
                module.ConfigData.Add(TopTec_RqdTimeInToleranceWindow_s);

                module.ConfigData.Add(dt_BottomTecDegSet);
                module.ConfigData.Add(dt_BottomTecTimeout_s);
                module.ConfigData.Add(dt_BottomTecTstable_s);
                module.ConfigData.Add(BtmTec_TemperatureTolerance_C);

            }
            else  // Single Tec Control
            {
                DatumDouble dt_TargetTemp = new DatumDouble("TargetTemperature_C", temp_To);
                DatumDouble dt_Tstable_s = new DatumDouble("RqdStabilisationTime_s", t_points[0].DelayTime_s);
                DatumDouble dt_DegSet_C = new DatumDouble("SetPointTemperature_C", temp_setting);
                DatumDouble dt_DegTol = new DatumDouble("TemperatureTolerance_C", t_points[0].Tolerance_degC);
                dt_intval = new DatumSint32("TempTimeBtwReadings_ms",
                    InstrParamConfig.GetIntParam("TecCase_UpdateTime_mS"));

                module = engine.AddModuleRun(modName, "SimpleTempControl", "");
                module.Instrs.Add("Controller", CoRxDCTestInstruments.TopCaseTec );

                module.ConfigData.Add(dt_TargetTemp);
                module.ConfigData.Add(dt_Tstable_s);
                module.ConfigData.Add(dt_DegSet_C);
                module.ConfigData.Add(dt_DegTol);
            }
            module.ConfigData.Add(dt_timeout_s);
            module.ConfigData.Add(dt_intval);

            if (isSaveReslt)
            {
                string tempStr = temp_To.ToString("#").Replace('-', 'M');
                string paramName = string.Format("TEMPERATURE_ROOM_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "ActualTempReading");
                tempStr = temp_To.ToString("#").Replace('-', 'M');
                paramName = string.Format("TEMPERATURE_DUT_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "TargetTemp");
                paramName = string.Format("TEMPERATURE_SET_{0}C", tempStr);
                module.Limits.AddParameter(MainSpec, paramName, "IsTemperatureSetOK");
            }
        }
       

        /// <summary>
        /// Initialise modules to verify Splitters' ratio
        /// </summary>
        /// <param name="engine"></param>
        void InitSRVerifyModules(ITestEngineInit engine)
        {
            engine.SendToGui("Initialising splitter ratio verify module ...");
            if (FreqArr2VerifySR== null  || FreqArr2VerifySR .Length <1) return;

            #region Sig path verify module
            ModuleRun module = engine.AddModuleRun("Verify Signal Path Splitter Ratio", "TM_CoRxSRVerify", "");
            module.ConfigData.AddString("GuiTitle", "Verify Signal Path Splitter Ratio");
            module.ConfigData.AddDouble("LaserSourcePower_dBm", InstrParamConfig.GetDoubleParam("SignalSourcePower@SRVerify_dBm"));
            module.ConfigData.AddDouble("VoaAtten_dB", InstrParamConfig.GetDoubleParam("Att_Sig@SRVerify_dB"));
            module.ConfigData.AddDouble("MaxDeltaSR_dB", InstrParamConfig.GetDoubleParam("MaxDeltaSR_Sig@SRVerify_dB"));
            module.ConfigData.AddSint32("MaxRetryTimes", TestParmConfig.GetIntParam("MaxRetryTimes@SRVerify"));
            
            // Add this param to config data will save data to plot file
            module.ConfigData.AddString("FileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));

            module.ConfigData.AddReference("OpcChainInstrs", CoRxDCTestInstruments.SignalInputInstrs);

            // Add frequency message  to verify according to its setting mode
            string freqParamName;
            if (FreqSetMode == EnumFrequencySetMode.ByChan)
                freqParamName = "ChanArr2Verify";
            else if (FreqSetMode == EnumFrequencySetMode .ByWavelen )
                freqParamName = "WavelenArr2Verify" ;
            else 
                freqParamName = "FreqArr2Verify" ;

            module.ConfigData.AddDoubleArray(freqParamName, FreqArr2VerifySR);
            #endregion

            #region Loc path verify module
            module = engine.AddModuleRun("Verify Local Osc Path Splitter Ratio", "TM_CoRxSRVerify", "");
            module.ConfigData.AddString("GuiTitle", "Verify Local Osc Path Splitter Ratio");
            module.ConfigData.AddDouble("LaserSourcePower_dBm", InstrParamConfig.GetDoubleParam("LocSourcePower@SRVerify_dBm"));
            module.ConfigData.AddDouble("VoaAtten_dB", InstrParamConfig.GetDoubleParam("Att_Loc@SRVerify_dB"));
            module.ConfigData.AddDouble("MaxDeltaSR_dB", InstrParamConfig.GetDoubleParam("MaxDeltaSR_Loc@SRVerify_dB"));
            module.ConfigData.AddSint32("MaxRetryTimes", TestParmConfig.GetIntParam("MaxRetryTimes@SRVerify"));
            module.ConfigData.AddReference("OpcChainInstrs", CoRxDCTestInstruments.LocalOscInputInstrs);

            module.ConfigData.AddString("FileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));
            module.ConfigData.AddDoubleArray(freqParamName, FreqArr2VerifySR);
            #endregion
        }

        /// <summary>
        /// Initialise module for darkcurrent test
        /// </summary>
        /// <param name="engine"></param>
        void InitDarkCurrentModule(ITestEngineInit engine)
        {
            ModuleRun module = null;

  
            // Auxilary pd dark current test 
            if (TestParmConfig.GetBoolParam("IsTestAux"))
            {
                // Only run this test is Aux are required so don't add it to the Config data
                engine.SendToGui("Initialising auxilary pd Dark current test modules ...");
                module = engine.AddModuleRun("Auxilary pd Dark Current Test", "TM_AuxPdDarkCurrentTest", "");

                module.ConfigData.AddDouble("AuxPDBias_X_V", VauxPd_X_V);
                module.ConfigData.AddDouble("AuxPDBias_Y_V", VauxPd_Y_V);

                module.ConfigData.AddDouble("AuxPDCompCurrent_X_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_X@DarkCurrent_A"));
                module.ConfigData.AddDouble("AuxPDCompCurrent_Y_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_Y@DarkCurrent_A"));


                module.Limits.AddParameter(MainSpec, "DARKCURRENT_SIG_X_35C", "IauxPD_XL_nA");
                module.Limits.AddParameter(MainSpec, "DARKCURRENT_LO_X_35C", "IauxPD_XR_nA");
                module.Limits.AddParameter(MainSpec, "DARKCURRENT_SIG_Y_35C", "IauxPD_YL_nA");
                module.Limits.AddParameter(MainSpec, "DARKCURRENT_LO_Y_35C", "IauxPD_YR_nA");
            }

            // PD Dark current test 
            string moduleName = "";
            string paramName = "";
            string modname = "";


            for (int ind = 0; ind < TestTemperatures.Length; ind++)
            {
                engine.SendToGui("Initialising pd dark current test modules ...");

                string temp = TestTemperatures[ind].ToString().Replace('-', 'M');
                
                if (TestParmConfig.GetBoolParam("Is6464"))
                {
                    modname = "TM_PdDarkCurrentTest6464";
                                    }
                else
                {
                    modname = "TM_PdDarkCurrentTest";
                }
                moduleName = string.Format("PD Dark Current Test {0}C", temp);
                module = engine.AddModuleRun(moduleName, modname, "");
                //module = engine.AddModuleRun(moduleName, "TM_PdDarkCurrentTest", "");
                module.ConfigData.AddDouble("PDBias_V", TestParmConfig.GetDoubleParam("Vpd@DarkCurrent_V"));
                module.ConfigData.AddDouble("PDCompCurrent_A", TestParmConfig.GetDoubleParam("IpdCompliant@DarkCurrent_A"));
                module.ConfigData.AddDouble("TiaBias_V", TestParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", TestParmConfig.GetDoubleParam("IccComliant@DarkCurrent_A"));
                module.ConfigData.AddDouble("SignalSourcePower", InstrParamConfig.GetDoubleParam("SignalSourcePower@Test_dBm"));
                module.ConfigData.AddDouble("LocSourcePower", InstrParamConfig.GetDoubleParam("LocSourcePower@Test_dBm"));

                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    module.ConfigData.AddDouble("PDTapBias_V", TestParmConfig.GetDoubleParam("VPdTap_V"));
                    module.ConfigData.AddDouble("PDTapCompCurrent_A", TestParmConfig.GetDoubleParam("IPdTapCompliance_A"));

                    paramName = string.Format("DARKCURRENT_TAP_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "IPdTap_nA"); 
                }

                if (TestParmConfig.GetBoolParam("Is6464"))
                {
                    paramName = string.Format("DARKCURRENT_XI_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_XI1_nA");
                     paramName = string.Format("DARKCURRENT_XQ_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_XQ1_nA");
                    paramName = string.Format("DARKCURRENT_YI_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_YI1_nA");
                    paramName = string.Format("DARKCURRENT_YQ_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_YQ1_nA");
                    paramName = string.Format("ICC_TIA_LOW_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Icc_X_mA");
                    paramName = string.Format("ICC_TIA_HIGH_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Icc_Y_mA");  
                }
                else
                {
                    paramName = string.Format("DARKCURRENT_XIP_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_XI1_nA");
                    paramName = string.Format("DARKCURRENT_XIN_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_XI2_nA");
                    paramName = string.Format("DARKCURRENT_XQP_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_XQ1_nA");
                    paramName = string.Format("DARKCURRENT_XQN_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_XQ2_nA");
                    paramName = string.Format("DARKCURRENT_YIP_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_YI1_nA");
                    paramName = string.Format("DARKCURRENT_YIN_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_YI2_nA");
                    paramName = string.Format("DARKCURRENT_YQP_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_YQ1_nA");
                    paramName = string.Format("DARKCURRENT_YQN_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Ipd_YQ2_nA");
                    paramName = string.Format("ICC_TIA_X_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Icc_X_mA");
                    paramName = string.Format("ICC_TIA_Y_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "Icc_Y_mA");  
                }

                paramName = string.Format("MAX_LANE_DARKCURRENT_{0}C", temp);
                if ( MainSpec .ParamLimitExists( paramName ))
                    module .Limits .AddParameter ( MainSpec , paramName , "Ipd_Max_nA");         

            }
        }

        /// <summary>
        ///  Initialise Coupling eff modules test under all temperatures
        /// </summary>
        /// <param name="engine"></param>
        ///<param name="dutObject"></param>
        void InitCouplieEffModules(ITestEngineInit engine, DUTObject dutObject)
        {
            engine.SendToGui("Initialising Coupling Eff test modules ...");
            double wavelen_Ref_nm = TestParmConfig.GetDoubleParam("Wavelen4TempTest_nm");
            double freq_Ref_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelen_Ref_nm);
            for (int idx = 0; idx < TestTemperatures.Length; idx++)
            {

                string temp = TestTemperatures[idx].ToString().Replace('-', 'M');
                string moduleName = string.Format("Coupling Efficiency test @{0:##}C", temp);
                ModuleRun module = engine.AddModuleRun(moduleName, "TM_CoRxMultiCoupleEff", "");
                module.ConfigData.AddDouble("TiaBias_V", TestParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", TestParmConfig.GetDoubleParam("IccComliant_A"));

                module.ConfigData.AddDouble("AuxPDBias_X_V", VauxPd_X_V);
                module.ConfigData.AddDouble("AuxPDBias_Y_V", VauxPd_Y_V);

                module.ConfigData.AddDouble("AuxPDCompCurrent_X_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_X@CoupleEff_A"));
                module.ConfigData.AddDouble("AuxPDCompCurrent_Y_A", TestParmConfig.GetDoubleParam("IauxPdCompliant_Y@CoupleEff_A"));

                module.ConfigData.AddDouble("ReferenceWavelen_nM", wavelen_Ref_nm);
                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    module.ConfigData.AddDouble("PDTapBias_V", TestParmConfig.GetDoubleParam("VPdTap_V"));
                    module.ConfigData.AddDouble("PDTapCompCurrent_A", TestParmConfig.GetDoubleParam("IPdTapCompliance_A"));
                }

                string paramName = "";
                if (idx ==0) // If  normal temp test, test series frequencies and statistical
                {
                    if (MainSpec.ParamLimitExists("AUX_BIAS_LOC_X"))
                        module.Limits.AddParameter(MainSpec, "AUX_BIAS_LOC_X", "VauxPD_XR_V");
                    if (MainSpec.ParamLimitExists("AUX_BIAS_LOC_Y"))
                        module.Limits.AddParameter(MainSpec, "AUX_BIAS_LOC_Y", "VauxPD_YR_V");

                    if (MainSpec.ParamLimitExists("AUX_BIAS_SIG_X"))
                        module.Limits.AddParameter(MainSpec, "AUX_BIAS_SIG_X", "VauxPD_XL_V");
                    if (MainSpec.ParamLimitExists("AUX_BIAS_SIG_Y"))
                        module.Limits.AddParameter(MainSpec, "AUX_BIAS_SIG_Y", "VauxPD_YL_V");

                    module.ConfigData.AddBool("SaveDataToFile", true);
                    module.ConfigData.AddString("BlobFileDirectory",  TestParmConfig.GetStringParam("PlotDataTempDirectory"));
                    module.ConfigData.AddString("FileSufix4SigPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_CoupleEff4SigPath"));
                    module.ConfigData.AddString("FileSufix4LocPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_CoupleEff4LocPath"));

                    module.ConfigData.AddString("FilePrefix4RespStatatics", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_AuxPdRespStat"));
                    module.ConfigData.AddString("FilePrefix4PdRespSigPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_AuxPdResp_SigPath"));
                    module.ConfigData.AddString("FilePrefix4PdRespLocPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_AuxPdResp_LocPath"));


                    module.ConfigData.AddDouble("Freq_Start_GHz", TestParmConfig.GetDoubleParam("StartFreq@CoupleEff_GHz"));
                    module.ConfigData.AddDouble("Freq_Stop_GHz", TestParmConfig.GetDoubleParam("StopFreq@CoupleEff_GHz"));

                    paramName = string.Format("AUX_SIG_{0}C_FILE", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathDataFile");
                    paramName = string.Format("AUX_LO_{0}C_FILE", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathDataFile");

                    paramName = string.Format("PLOT_RESP_AUX_SIG_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathPdRespFile");
                    paramName = string.Format("PLOT_RESP_AUX_LO_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "LocPathPdRespFile");
                    paramName = string.Format("PLOT_RESP_AUX_STAT_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "PdRespStatiscsFile");


                    paramName = string.Format("RESP_LO_XL_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL_Avg");
                    paramName = string.Format("RESP_LO_XL_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL_Max");
                    paramName = string.Format("RESP_LO_XL_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL_Min");

                    paramName = string.Format("RESP_SIG_XL_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL_Avg");
                    paramName = string.Format("RESP_SIG_XL_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL_Max");
                    paramName = string.Format("RESP_SIG_XL_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL_Min");

                    paramName = string.Format("RESP_LO_XR_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR_Avg");
                    paramName = string.Format("RESP_LO_XR_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR_Max");
                    paramName = string.Format("RESP_LO_XR_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR_Min");

                    paramName = string.Format("RESP_SIG_XR_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR_Avg");
                    paramName = string.Format("RESP_SIG_XR_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR_Max");
                    paramName = string.Format("RESP_SIG_XR_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR_Min");

                    paramName = string.Format("RESP_LO_YL_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL_Avg");
                    paramName = string.Format("RESP_LO_YL_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL_Max");
                    paramName = string.Format("RESP_LO_YL_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL_Min");

                    paramName = string.Format("RESP_SIG_YL_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL_Avg");
                    paramName = string.Format("RESP_SIG_YL_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL_Max");
                    paramName = string.Format("RESP_SIG_YL_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL_Min");

                    paramName = string.Format("RESP_LO_YR_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR_Avg");
                    paramName = string.Format("RESP_LO_YR_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR_Max");
                    paramName = string.Format("RESP_LO_YR_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR_Min");

                    paramName = string.Format("RESP_SIG_YR_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR_Avg");
                    paramName = string.Format("RESP_SIG_YR_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR_Max");
                    paramName = string.Format("RESP_SIG_YR_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR_Min");

                }
                else // If not normal temp test, only the ref wavelen is tested
                {
                    module.ConfigData.AddBool("SaveDataToFile", false);
                    module.ConfigData.AddDouble("Freq_Start_GHz", freq_Ref_GHz);
                    module.ConfigData.AddDouble("Freq_Stop_GHz", freq_Ref_GHz);
                }
                module.ConfigData.AddDouble("Freq_Step_GHz", TestParmConfig.GetDoubleParam("StepFreq@CoupleEff_GHz"));
                module.ConfigData.AddBool("IsAdjustSigOpcPower", TestParmConfig.GetBoolParam("IsAdjustSigPower@CoupleEff"));
                module.ConfigData.AddDouble("RqsSigOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Sig@CoupleEff"));
                module.ConfigData.AddBool("IsAdjustLocOpcPower", TestParmConfig.GetBoolParam("IsAdjustLocPower@CoupleEff"));
                module.ConfigData.AddDouble("RqsLocOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Loc@CoupleEff"));

                paramName = string.Format("RESP_LO_XL_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XL");

                paramName = string.Format("RESP_SIG_XL_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XL");

                paramName = string.Format("RESP_LO_XR_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_XR");

                paramName = string.Format("RESP_SIG_XR_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_XR");

                paramName = string.Format("RESP_LO_YL_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YL");

                paramName = string.Format("RESP_SIG_YL_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YL");

                paramName = string.Format("RESP_LO_YR_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathRes_Aux_YR");

                paramName = string.Format("RESP_SIG_YR_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Aux_YR");

            }
        }

        /// <summary>
        /// Initialise responsivity modules under all temperature
        /// </summary>
        /// <param name="engine"></param>
        ///<param name="dutObject"></param>
        ///<param name="masterSource"> Source to use as master trigger source </param>
        void InitResponsivityModules(ITestEngineInit engine, DUTObject dutObject, Inst_Ke24xx masterSource)
        {
            string paramName;

            engine.SendToGui("Initialising Responsivity test modules ...");
            double wavelen_Ref_nm = TestParmConfig.GetDoubleParam("Wavelen4TempTest_nm");
            double freq_Ref_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelen_Ref_nm);

            for (int idx = 0; idx < TestTemperatures.Length; idx++)
            {

                string temp = TestTemperatures[idx].ToString().Replace('-','M');
                string moduleName = string.Format("Responsivity test @{0}C", temp);
                string modname = "";
                if (TestParmConfig.GetBoolParam("Is6464"))
                {
                    modname = "TM_CoRxResponsivity6464";
                }
                else
                {
                    modname = "TM_CoRxResponsivity";
                }
                
                ModuleRun module = engine.AddModuleRun(moduleName, modname, "");

                
                module.ConfigData.AddOrUpdateString("DutSerialNumber", dutObject.SerialNumber);
                module.ConfigData.AddOrUpdateString("DutTestStage", dutObject.TestStage);
                module.ConfigData.AddOrUpdateString("Temperature", temp);

                module.ConfigData.AddDouble("TiaBias_V", TestParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", TestParmConfig.GetDoubleParam("IccComliant@Resp_A"));
                module.ConfigData.AddDouble("PDBias_V", TestParmConfig.GetDoubleParam("Vpd_V"));
                module.ConfigData.AddDouble("PDCompCurrent_A", TestParmConfig.GetDoubleParam("IpdCompliant@Resp_A"));


                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    module.ConfigData.AddDouble("PDTapBias_V", TestParmConfig.GetDoubleParam("VPdTap_V"));
                    module.ConfigData.AddDouble("PDTapCompCurrent_A", TestParmConfig.GetDoubleParam("IPdTapCompliance_A"));
                                       
                    module.ConfigData.AddString("FilePrefix4TapRespSigPath",dutObject .SerialNumber + "_" +  TestParmConfig.GetStringParam("FilePrefix_TapResp_SigPath"));
                    module.ConfigData.AddString("FilePrefix4TapSigPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_TapPd_SigPath"));
                    
                    paramName = string.Format("PLOT_RESP_TAP_SIG_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigTapRespDataFile");
                    paramName = string.Format("TAP_SIG_{0}C_FILE", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigTapDataFile");
                    
                    paramName = string.Format("RESP_TAP_SIG_{0}C_AVG", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap_Avg");
                    paramName = string.Format("RESP_TAP_SIG_{0}C_MAX", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap_Max");
                    paramName = string.Format("RESP_TAP_SIG_{0}C_MIN", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathRes_Tap_Min");

                    paramName = string.Format("RESP_TAP_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_Tap@Ref");

                    //paramName = string.Format("RESP_TAP_LOC_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    //if (MainSpec.ParamLimitExists(paramName))
                    //    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_Tap@Ref");

                    //paramName = string.Format("TAP_ISOLATION_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    //if (MainSpec.ParamLimitExists(paramName))
                    //    module.Limits.AddParameter(MainSpec, paramName, "Isolation@Ref"); 

                    paramName = string.Format("MPD_LO_ISOLATION_MIN_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "IsolationMin"); 

                }

                module.ConfigData.AddDouble("ReferenceWavelen_nM", wavelen_Ref_nm);

                module.ConfigData.AddBool("IsPdSourceUseTrigger", InstrParamConfig.GetBoolParam("IsPdSourceUseTriggerLink"));
                module.Instrs.Add("Master Source", masterSource);

                module.ConfigData.AddString("BlobFileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));
                module.ConfigData.AddString("FileSufix4SigPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_SigPathData"));
                module.ConfigData.AddString("FileSufix4LocPath", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_LocPathData"));

                module.ConfigData.AddString("FileSufix4SigPathResp", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_SigPathResp"));
                module.ConfigData.AddString("FileSufix4LocPathResp", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_LocPathResp"));
                module.ConfigData.AddString("FileSufix4RespStat", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_RespStat"));
                module.ConfigData.AddString("FileSufix4LocPathRipple", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_LocPathRipple"));

                module.ConfigData.AddString("FileSufix4SigPathDevResp", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_SigPathDevResp"));
                module.ConfigData.AddString("FileSufix4LocPathDevResp", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_LocPathDevResp"));
                module.ConfigData.AddString("FileSufix4DevRespStat", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_DevRespStat"));

                module.ConfigData.AddString("FileSufix4XchipDevResp@Sig", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_XchipDevResp@Sig"));
                module.ConfigData.AddString("FileSufix4XchipDevResp@Loc", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_XchipDevResp@Loc"));
                module.ConfigData.AddString("FileSufix4YchipDevResp@Sig", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_YchipDevResp@Sig"));
                module.ConfigData.AddString("FileSufix4YchipDevResp@Loc", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_YchipDevResp@Loc"));

                module.ConfigData.AddString("FileSufix4SigPathCMRR", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_SigPathCMRR"));
                module.ConfigData.AddString("FileSufix4LocPathCMRR", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_LocPathCMRR"));
                module.ConfigData.AddString("FileSufix4CMRRStat", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_CmrrStat"));

                string Freq_Range_Str = TestParmConfig.GetStringParam("RangeFreq@Resp_GHz");
                module.ConfigData.AddOrUpdateReference("Freq_Range", GetListOfFrequencies(Freq_Range_Str));

                module.ConfigData.AddBool("IsAdjustSigOpcPower", TestParmConfig.GetBoolParam("IsAdjustSigPower@Resp"));
                module.ConfigData.AddDouble("RqsSigOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Sig@Resp"));
                module.ConfigData.AddBool("IsAdjustLocOpcPower", TestParmConfig.GetBoolParam("IsAdjustLocPower@Resp"));
                module.ConfigData.AddDouble("RqsLocOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Loc@Resp"));

                #region Add parameters to limit
                paramName = string.Format("MAX_LANE_IMBALANCE_LO_{0}C", temp);                
                if ( MainSpec .ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "maxRespImbalance_Loc");


                paramName = string.Format("MAX_LANE_IMBALANCE_SIG_{0}C", temp);                
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "maxRespImbalance_Sig");

                paramName = string.Format("MAX_LANE_IMBALANCE_X_LO_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "maxRespImbBalance_X_Loc");


                paramName = string.Format("MAX_LANE_IMBALANCE_Y_LO_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "maxRespImbBalance_Y_Loc");

                paramName = string.Format("MAX_LANE_IMBALANCE_X_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "maxRespImbBalance_X_Sig");


                paramName = string.Format("MAX_LANE_IMBALANCE_Y_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "maxRespImbBalance_Y_Sig");


                paramName = string.Format("MIN_LANE_RESP_LO_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "MinPdResp_Loc");

                paramName = string.Format("MIN_LANE_RESP_SIG_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "MinPdResp_Sig");

                paramName = string.Format("MAX_LANE_DEV_LO_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "MaxPdDevResp_Loc");

                paramName = string.Format("MAX_LANE_DEV_SIG_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "MaxPdDevResp_Sig");





                // if 6464 then skip this 
                if (!TestParmConfig.GetBoolParam("Is6464"))
                {
                    paramName = string.Format("RESP_XIP_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_XI1@Ref");
                    paramName = string.Format("RESP_XIP_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_XI1@Ref");

                    paramName = string.Format("RESP_XIN_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_XI2@Ref");
                    paramName = string.Format("RESP_XIN_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_XI2@Ref");

                    paramName = string.Format("RESP_XQP_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_XQ1@Ref");
                    paramName = string.Format("RESP_XQP_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_XQ1@Ref");

                    paramName = string.Format("RESP_XQN_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_XQ2@Ref");
                    paramName = string.Format("RESP_XQN_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_XQ2@Ref");

                    paramName = string.Format("RESP_YIP_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_YI1@Ref");
                    paramName = string.Format("RESP_YIP_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_YI1@Ref");

                    paramName = string.Format("RESP_YIN_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_YI2@Ref");
                    paramName = string.Format("RESP_YIN_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_YI2@Ref");

                    paramName = string.Format("RESP_YQP_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_YQ1@Ref");
                    paramName = string.Format("RESP_YQP_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_YQ1@Ref");

                    paramName = string.Format("RESP_YQN_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_YQ2@Ref");
                    paramName = string.Format("RESP_YQN_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_YQ2@Ref");

                    paramName = string.Format("MAX_LANE_CMRR_LO_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "MaxPdCMRR_Loc");

                    paramName = string.Format("MAX_LANE_CMRR_SIG_{0}C", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "MaxPdCMRR_Sig");

                    paramName = string.Format("CMRR_XI_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XI_Avg");
                    paramName = string.Format("CMRR_XI_LO_{0:}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XI_Max");
                    paramName = string.Format("CMRR_XI_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XI_Min");

                    paramName = string.Format("CMRR_XI_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XI_Avg");
                    paramName = string.Format("CMRR_XI_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XI_Max");
                    paramName = string.Format("CMRR_XI_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XI_Min");

                    paramName = string.Format("CMRR_XQ_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XQ_Avg");
                    paramName = string.Format("CMRR_XQ_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XQ_Max");
                    paramName = string.Format("CMRR_XQ_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_XQ_Min");

                    paramName = string.Format("CMRR_XQ_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XQ_Avg");
                    paramName = string.Format("CMRR_XQ_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XQ_Max");
                    paramName = string.Format("CMRR_XQ_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_XQ_Min");

                    paramName = string.Format("CMRR_YI_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YI_Avg");
                    paramName = string.Format("CMRR_YI_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YI_Max");
                    paramName = string.Format("CMRR_YI_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YI_Min");

                    paramName = string.Format("CMRR_YI_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YI_Avg");
                    paramName = string.Format("CMRR_YI_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YI_Max");
                    paramName = string.Format("CMRR_YI_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YI_Min");

                    paramName = string.Format("CMRR_YQ_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YQ_Avg");
                    paramName = string.Format("CMRR_YQ_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YQ_Max");
                    paramName = string.Format("CMRR_YQ_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathCMRR_YQ_Min");

                    paramName = string.Format("CMRR_YQ_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YQ_Avg");
                    paramName = string.Format("CMRR_YQ_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YQ_Max");
                    paramName = string.Format("CMRR_YQ_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathCMRR_YQ_Min");

                    paramName = string.Format("PLOT_CMRR_LO_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "LocCMRRDataFile");
                    paramName = string.Format("PLOT_CMRR_SIG_{0}C", temp);
                    if (MainSpec.ParamLimitExists(paramName))
                        module.Limits.AddParameter(MainSpec, paramName, "SigCMRRDataFile");


                    paramName = string.Format("RESP_XIP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Avg");
                    paramName = string.Format("RESP_XIP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Max");
                    paramName = string.Format("RESP_XIP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Min");

                    paramName = string.Format("RESP_XIP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Avg");
                    paramName = string.Format("RESP_XIP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Max");
                    paramName = string.Format("RESP_XIP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Min");

                    paramName = string.Format("RESP_XIN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI2_Avg");
                    paramName = string.Format("RESP_XIN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI2_Max");
                    paramName = string.Format("RESP_XIN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI2_Min");

                    paramName = string.Format("RESP_XIN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI2_Avg");
                    paramName = string.Format("RESP_XIN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI2_Max");
                    paramName = string.Format("RESP_XIN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI2_Min");

                    paramName = string.Format("RESP_XQP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Avg");
                    paramName = string.Format("RESP_XQP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Max");
                    paramName = string.Format("RESP_XQP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Min");

                    paramName = string.Format("RESP_XQP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Avg");
                    paramName = string.Format("RESP_XQP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Max");
                    paramName = string.Format("RESP_XQP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Min");

                    paramName = string.Format("RESP_XQN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ2_Avg");
                    paramName = string.Format("RESP_XQN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ2_Max");
                    paramName = string.Format("RESP_XQN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ2_Min");

                    paramName = string.Format("RESP_XQN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ2_Avg");
                    paramName = string.Format("RESP_XQN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ2_Max");
                    paramName = string.Format("RESP_XQN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ2_Min");

                    paramName = string.Format("RESP_YIP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Avg");
                    paramName = string.Format("RESP_YIP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Max");
                    paramName = string.Format("RESP_YIP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Min");

                    paramName = string.Format("RESP_YIP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Avg");
                    paramName = string.Format("RESP_YIP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Max");
                    paramName = string.Format("RESP_YIP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Min");

                    paramName = string.Format("RESP_YIN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI2_Avg");
                    paramName = string.Format("RESP_YIN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI2_Max");
                    paramName = string.Format("RESP_YIN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI2_Min");

                    paramName = string.Format("RESP_YIN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI2_Avg");
                    paramName = string.Format("RESP_YIN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI2_Max");
                    paramName = string.Format("RESP_YIN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI2_Min");

                    paramName = string.Format("RESP_YQP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Avg");
                    paramName = string.Format("RESP_YQP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Max");
                    paramName = string.Format("RESP_YQP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Min");

                    paramName = string.Format("RESP_YQP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Avg");
                    paramName = string.Format("RESP_YQP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Max");
                    paramName = string.Format("RESP_YQP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Min");

                    paramName = string.Format("RESP_YQN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ2_Avg");
                    paramName = string.Format("RESP_YQN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ2_Max");
                    paramName = string.Format("RESP_YQN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ2_Min");

                    paramName = string.Format("RESP_YQN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ2_Avg");
                    paramName = string.Format("RESP_YQN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ2_Max");
                    paramName = string.Format("RESP_YQN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ2_Min");

                    paramName = string.Format("DEV_XIP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Avg");
                    paramName = string.Format("DEV_XIP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Max");
                    paramName = string.Format("DEV_XIP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Min");

                    paramName = string.Format("DEV_XIP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Avg");
                    paramName = string.Format("DEV_XIP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Max");
                    paramName = string.Format("DEV_XIP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Min");

                    paramName = string.Format("DEV_XIN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI2_Avg");
                    paramName = string.Format("DEV_XIN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI2_Max");
                    paramName = string.Format("DEV_XIN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI2_Min");

                    paramName = string.Format("DEV_XIN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI2_Avg");
                    paramName = string.Format("DEV_XIN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI2_Max");
                    paramName = string.Format("DEV_XIN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI2_Min");

                    paramName = string.Format("DEV_XQP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Avg");
                    paramName = string.Format("DEV_XQP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Max");
                    paramName = string.Format("DEV_XQP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Min");

                    paramName = string.Format("DEV_XQP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Avg");
                    paramName = string.Format("DEV_XQP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Max");
                    paramName = string.Format("DEV_XQP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Min");

                    paramName = string.Format("DEV_XQN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ2_Avg");
                    paramName = string.Format("DEV_XQN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ2_Max");
                    paramName = string.Format("DEV_XQN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ2_Min");

                    paramName = string.Format("DEV_XQN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ2_Avg");
                    paramName = string.Format("DEV_XQN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ2_Max");
                    paramName = string.Format("DEV_XQN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ2_Min");

                    paramName = string.Format("DEV_YIP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Avg");
                    paramName = string.Format("DEV_YIP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Max");
                    paramName = string.Format("DEV_YIP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Min");

                    paramName = string.Format("DEV_YIP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Avg");
                    paramName = string.Format("DEV_YIP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Max");
                    paramName = string.Format("DEV_YIP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Min");

                    paramName = string.Format("DEV_YIN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI2_Avg");
                    paramName = string.Format("DEV_YIN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI2_Max");
                    paramName = string.Format("DEV_YIN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI2_Min");

                    paramName = string.Format("DEV_YIN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI2_Avg");
                    paramName = string.Format("DEV_YIN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI2_Max");
                    paramName = string.Format("DEV_YIN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI2_Min");

                    paramName = string.Format("DEV_YQP_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Avg");
                    paramName = string.Format("DEV_YQP_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Max");
                    paramName = string.Format("DEV_YQP_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Min");

                    paramName = string.Format("DEV_YQP_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Avg");
                    paramName = string.Format("DEV_YQP_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Max");
                    paramName = string.Format("DEV_YQP_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Min");

                    paramName = string.Format("DEV_YQN_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ2_Avg");
                    paramName = string.Format("DEV_YQN_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ2_Max");
                    paramName = string.Format("DEV_YQN_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ2_Min");

                    paramName = string.Format("DEV_YQN_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ2_Avg");
                    paramName = string.Format("DEV_YQN_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ2_Max");
                    paramName = string.Format("DEV_YQN_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ2_Min");

                }
                else
                {
                    paramName = string.Format("RESP_XI_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_XI1@Ref");
                    paramName = string.Format("RESP_XI_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_XI1@Ref");

                    paramName = string.Format("RESP_XQ_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_XQ1@Ref");
                    paramName = string.Format("RESP_XQ_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_XQ1@Ref");

                    paramName = string.Format("RESP_YI_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_YI1@Ref");
                    paramName = string.Format("RESP_YI_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_YI1@Ref");

                    paramName = string.Format("RESP_YQ_LO_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "LocPathResp_YQ1@Ref");
                    paramName = string.Format("RESP_YQ_SIG_{0}C_{1:####}NM", temp, wavelen_Ref_nm);
                    module.Limits.AddParameter(MainSpec, paramName, "SigPathResp_YQ1@Ref");

                    paramName = string.Format("RESP_XI_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Avg");
                    paramName = string.Format("RESP_XI_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Max");
                    paramName = string.Format("RESP_XI_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XI1_Min");

                    paramName = string.Format("RESP_XI_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Avg");
                    paramName = string.Format("RESP_XI_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Max");
                    paramName = string.Format("RESP_XI_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XI1_Min");

                    paramName = string.Format("RESP_XQ_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Avg");
                    paramName = string.Format("RESP_XQ_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Max");
                    paramName = string.Format("RESP_XQ_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_XQ1_Min");

                    paramName = string.Format("RESP_XQ_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Avg");
                    paramName = string.Format("RESP_XQ_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Max");
                    paramName = string.Format("RESP_XQ_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_XQ1_Min");

                    paramName = string.Format("RESP_YI_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Avg");
                    paramName = string.Format("RESP_YI_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Max");
                    paramName = string.Format("RESP_YI_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YI1_Min");

                    paramName = string.Format("RESP_YI_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Avg");
                    paramName = string.Format("RESP_YI_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Max");
                    paramName = string.Format("RESP_YI_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YI1_Min");

                    paramName = string.Format("RESP_YQ_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Avg");
                    paramName = string.Format("RESP_YQ_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Max");
                    paramName = string.Format("RESP_YQ_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathRes_YQ1_Min");

                    paramName = string.Format("RESP_YQ_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Avg");
                    paramName = string.Format("RESP_YQ_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Max");
                    paramName = string.Format("RESP_YQ_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathRes_YQ1_Min");

                    paramName = string.Format("DEV_XI_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Avg");
                    paramName = string.Format("DEV_XI_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Max");
                    paramName = string.Format("DEV_XI_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XI1_Min");

                    paramName = string.Format("DEV_XI_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Avg");
                    paramName = string.Format("DEV_XI_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Max");
                    paramName = string.Format("DEV_XI_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XI1_Min");

                    paramName = string.Format("DEV_XQ_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Avg");
                    paramName = string.Format("DEV_XQ_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Max");
                    paramName = string.Format("DEV_XQ_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_XQ1_Min");

                    paramName = string.Format("DEV_XQ_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Avg");
                    paramName = string.Format("DEV_XQ_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Max");
                    paramName = string.Format("DEV_XQ_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_XQ1_Min");

                    paramName = string.Format("DEV_YI_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Avg");
                    paramName = string.Format("DEV_YI_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Max");
                    paramName = string.Format("DEV_YI_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YI1_Min");

                    paramName = string.Format("DEV_YI_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Avg");
                    paramName = string.Format("DEV_YI_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Max");
                    paramName = string.Format("DEV_YI_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YI1_Min");

                    paramName = string.Format("DEV_YQ_LO_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Avg");
                    paramName = string.Format("DEV_YQ_LO_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Max");
                    paramName = string.Format("DEV_YQ_LO_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "locPathDeltaRes_YQ1_Min");

                    paramName = string.Format("DEV_YQ_SIG_{0}C_AVG", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Avg");
                    paramName = string.Format("DEV_YQ_SIG_{0}C_MAX", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Max");
                    paramName = string.Format("DEV_YQ_SIG_{0}C_MIN", temp);
                    module.Limits.AddParameter(MainSpec, paramName, "sigPathDeltaRes_YQ1_Min");

                }




                paramName = string.Format("PD_LO_{0}C_FILE", temp);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathDataFile");
                paramName = string.Format("PD_SIG_{0}C_FILE", temp);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathDataFile");

                paramName = string.Format("PLOT_PD_LO_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "LocPathDataPlot");
                paramName = string.Format("PLOT_PD_SIG_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "SigPathDataPlot");

           
                paramName = string.Format("PLOT_DEV_LO_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "LocDevRespDataFile");
                paramName = string.Format("PLOT_DEV_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "SigDevRespDataFile");
                

                paramName = string.Format("PLOT_RESP_LO_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "LocRespDataFile");
                paramName = string.Format("PLOT_RESP_SIG_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "SigRespDataFile");
                paramName = string.Format("PLOT_STAT_{0}C", temp);
                if (MainSpec.ParamLimitExists(paramName))
                    module.Limits.AddParameter(MainSpec, paramName, "RespStatFile");


                //add max lane ripple local% by chaoqun
                paramName = string.Format("MAX_LANE_RIPPLE_LOC_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "locLaneRipple_Max");

                paramName = string.Format("RIPPLE_LO_{0}C_FILE",temp);
                module.Limits.AddParameter(MainSpec ,paramName,"LocRippleDataFile");

                paramName = string.Format("MAX_X_DEV_LO_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "XChipMaxPdDevResp_Loc");
                paramName = string.Format("MAX_X_DEV_SIG_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "XChipMaxPdDevResp_Sig");
                paramName = string.Format("MAX_Y_DEV_LO_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "YChipMaxPdDevResp_Loc");
                paramName = string.Format("MAX_Y_DEV_SIG_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "YChipMaxPdDevResp_Sig");

                paramName = string.Format("DEV_X_LO_{0}C_FILE", temp);
                module.Limits.AddParameter(MainSpec, paramName, "XChipDevRespDataFile@Loc");
                paramName = string.Format("DEV_X_SIG_{0}C_FILE", temp);
                module.Limits.AddParameter(MainSpec, paramName, "XChipDevRespDataFile@Sig");
                paramName = string.Format("DEV_Y_LO_{0}C_FILE", temp);
                module.Limits.AddParameter(MainSpec, paramName, "YChipDevRespDataFile@Loc");
                paramName = string.Format("DEV_Y_SIG_{0}C_FILE", temp);
                module.Limits.AddParameter(MainSpec, paramName, "YChipDevRespDataFile@Sig");

                #endregion
            } // End for loop
        }
        /// <summary>
        /// Initialise the VOA test modules for each temperature
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="dutObject"></param>
        void InitVOATestModules(ITestEngineInit engine, DUTObject dutObject, Inst_Ke24xx masterSource)
        {
            string paramName;
            const double speedoflight = 299792458.0;
            engine.SendToGui("Initialising VOA test modules ...");

            for (int idx = 0; idx < TestTemperatures.Length; idx++)
            {
                
                  

                string temp = TestTemperatures[idx].ToString().Replace('-', 'M');
                string moduleName = string.Format("VOA test @{0}C", temp);
                string modname = "";
                if (TestParmConfig.GetBoolParam("Is6464"))
                {
                    modname = "TM_VOATests6464";
                }
                else
                {
                    modname = "TM_VOATests";
                }

                double temperature = 0;
                double.TryParse(temp, out temperature);
                //if (dutObject.TestStage.ToLower().Contains("spc") && (NormalTemperature != temperature))
                //    continue;

                ModuleRun module = engine.AddModuleRun(moduleName, modname, "");
                // ModuleRun module = engine.AddModuleRun(moduleName, "TM_VOATests", "");

                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    module.ConfigData.AddDouble("PDTapBias_V", TestParmConfig.GetDoubleParam("VPdTap_V"));
                    module.ConfigData.AddDouble("PDTapCompCurrent_A", TestParmConfig.GetDoubleParam("IPdTapCompliance_A"));
                }

                // Set-up attenuation values for VOA Gradient Calculation.
                // High value is either 80% of MAx attenuation or some fixed attenuation value defined in Config file

                bool UseFixedAttenForGradientCalc = TestParmConfig.GetBoolParam("UseFixedAttenForGradientMax");
                module.ConfigData.AddBool("UseFixedAttenForGradientMax", UseFixedAttenForGradientCalc); 
                if (UseFixedAttenForGradientCalc)
                    module.ConfigData.AddDouble("AttenForGradientCalc",TestParmConfig.GetDoubleParam("AttenForGradientCalc"));

                module.Instrs.Add("Master Source", masterSource);
                module.ConfigData.AddDouble("TiaBias_V", TestParmConfig.GetDoubleParam("Vcc_V"));
                module.ConfigData.AddDouble("TIACompCurrent_A", TestParmConfig.GetDoubleParam("IccComliant@Resp_A"));
                module.ConfigData.AddDouble("PDBias_V", TestParmConfig.GetDoubleParam("Vpd_V"));
                module.ConfigData.AddDouble("PDCompCurrent_A", TestParmConfig.GetDoubleParam("IpdCompliant@Resp_A"));

                module.ConfigData.AddDouble("Freq_Start_GHz", TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.ConfigData.AddDouble("Freq_Stop_GHz", TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.ConfigData.AddDouble("Freq_Step_GHz", TestParmConfig.GetDoubleParam("StepFreq@VOA_GHz"));

                module.ConfigData.AddDouble("Temp", TestTemperatures[idx]);

                module.ConfigData.AddDouble("VOACompCurrent_A", TestParmConfig.GetDoubleParam("VOAComplianceCurrent_A"));
                module.ConfigData.AddDouble("VOASweepStartVoltage_V", TestParmConfig.GetDoubleParam("VOASweepStartVolts_V"));
                module.ConfigData.AddDouble("VOASweepStopVoltage_V", TestParmConfig.GetDoubleParam("VOASweepStopVolts_V"));
                module.ConfigData.AddDouble("VOASweepStepVoltage_V", TestParmConfig.GetDoubleParam("VOASweepStepVolts_V"));

                module.ConfigData.AddBool("IsAdjustSigOpcPower", TestParmConfig.GetBoolParam("IsAdjustSigPower@Resp"));
                module.ConfigData.AddDouble("RqsSigOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Sig@Resp"));
                module.ConfigData.AddBool("IsAdjustLocOpcPower", TestParmConfig.GetBoolParam("IsAdjustLocPower@Resp"));
                module.ConfigData.AddDouble("RqsLocOpcPower_DBm", TestParmConfig.GetDoubleParam("Power_Loc@Resp"));

                module.ConfigData.AddString("BlobFileDirectory", TestParmConfig.GetStringParam("PlotDataTempDirectory"));
                module.ConfigData.AddString("FilePrefix4VOA", dutObject.SerialNumber + "_" + TestParmConfig.GetStringParam("FilePrefix_VOA_Sweep"));
                module.ConfigData.AddString("FilePrefix4PDL", dutObject.SerialNumber + "_Plot_VOA_PDL_");
                module.ConfigData.AddString("FilePrefix4VOATempComp", dutObject.SerialNumber + "_VOA_Temp_Comp");

                if (TestTemperatures[idx] != NormalTemperature)
                {
                    string ParamNameTemp = (NormalTemperature < TestTemperatures[idx]) ? string.Format("{0}C_{1}C", NormalTemperature, temp) : string.Format("{0}C_{1}C", temp, NormalTemperature);
                    paramName = string.Format("VOA_HSPIN_TEMP_COMP_{0}", ParamNameTemp);
                    module.Limits.AddParameter(MainSpec, paramName, "VOA_HSPIN_TempComp");
                    paramName = string.Format("VOA_MPD_TEMP_COMP_{0}", ParamNameTemp);
                    module.Limits.AddParameter(MainSpec, paramName, "VOA_MPD_TempComp");
                    //paramName = string.Format("VOA_TEMP_COMP_FILE_{0}", ParamNameTemp);
                    //module.Limits.AddParameter(MainSpec, paramName, "VOA_TEMP_COMP_FILE");
                    

                }

                paramName = string.Format("VOA_CURRENT_MAX_{0}C", temp);
                module.Limits.AddParameter(MainSpec, paramName, "MaxVoaCurrent");

                paramName = string.Format("PLOT_VOA_SIG_{0}C_{1:####}NM", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "VOASweepDataFileStart");

                paramName = string.Format("PLOT_VOA_SIG_{0}C_{1:####}NM", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "VOASweepDataFileStop");

                paramName = string.Format("VOLTAGE_VOA10DB_X_{0}C_{1:####}NM", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "VOAVoltage10DB_X_StartFreq");

                paramName = string.Format("VOLTAGE_VOA10DB_X_{0}C_{1:####}NM", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "VOAVoltage10DB_X_StopFreq");

                paramName = string.Format("VOLTAGE_VOA10DB_Y_{0}C_{1:####}NM", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "VOAVoltage10DB_Y_StartFreq");

                paramName = string.Format("VOLTAGE_VOA10DB_Y_{0}C_{1:####}NM", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "VOAVoltage10DB_Y_StopFreq");

                paramName = string.Format("ATTEN_X_SIG_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "MaxAtten_X_StartFreq");

                paramName = string.Format("ATTEN_X_SIG_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "MaxAtten_X_StopFreq");

                paramName = string.Format("ATTEN_X_SIG_{0}C_{1:####}NM_GRAD", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "AttenGradient_X_StartFreq");

                paramName = string.Format("ATTEN_X_SIG_{0}C_{1:####}NM_GRAD", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "AttenGradient_X_StopFreq");

                paramName = string.Format("ATTEN_Y_SIG_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "MaxAtten_Y_StartFreq");

                paramName = string.Format("ATTEN_Y_SIG_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "MaxAtten_Y_StopFreq");

                paramName = string.Format("ATTEN_Y_SIG_{0}C_{1:####}NM_GRAD", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "AttenGradient_Y_StartFreq");

                paramName = string.Format("ATTEN_Y_SIG_{0}C_{1:####}NM_GRAD", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                module.Limits.AddParameter(MainSpec, paramName, "AttenGradient_Y_StopFreq");

                double VOA_Volts_Step = 0;
                if (dutObject.TestStage.ToLower().Contains("spc"))
                {
                    VOA_Volts_Step = TestParmConfig.GetDoubleParam("VOASweepStepVolts_V");
                }
                else
                {
                    VOA_Volts_Step = TestParmConfig.GetDoubleParam("VOASweepStepVolts_V") * 10;
                }

                for (double AttenVolts = 0; AttenVolts <= TestParmConfig.GetDoubleParam("VOASweepStopVolts_V"); AttenVolts++)
                {
                    paramName = string.Format("ATTEN_SIG_{0}C_{1:####}NM_MPD_{2:G}V", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"), AttenVolts).Replace(".", "_");
                    if (MainSpec.ParamLimitExists(paramName))
                    {
                    module.Limits.AddParameter(MainSpec, paramName, string.Format("AttenMPD{0:0}VStartFreq", AttenVolts));
                    }
                    paramName = string.Format("ATTEN_SIG_{0}C_{1:####}NM_MPD_{2:G}V", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"), AttenVolts).Replace(".", "_");
                    if (MainSpec.ParamLimitExists(paramName))
                    {
                        module.Limits.AddParameter(MainSpec, paramName, string.Format("AttenMPD{0:0}VStopFreq", AttenVolts));
                    }
 
                }

                for (double AttenVolts = 0; AttenVolts <= TestParmConfig.GetDoubleParam("VOASweepStopVolts_V"); AttenVolts = AttenVolts + VOA_Volts_Step)
                {
                    paramName = string.Format("VOA_WDL_X_SIG_{0}C_{1:G}V", temp, AttenVolts).Replace(".", "_");
                    module.Limits.AddParameter(MainSpec, paramName, string.Format("VOA_WDL_X_{0:G}V", AttenVolts).Replace(".", "_"));
                    paramName = string.Format("VOA_WDL_Y_SIG_{0}C_{1:G}V", temp, AttenVolts).Replace(".", "_");
                    module.Limits.AddParameter(MainSpec, paramName, string.Format("VOA_WDL_Y_{0:G}V", AttenVolts).Replace(".", "_"));

                    paramName = string.Format("ATTEN_SIG_{0}C_{1:####}NM_AVE_{2:G}V", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"), AttenVolts).Replace(".", "_");
                    module.Limits.AddParameter(MainSpec, paramName, string.Format("AttenAverage{0:G}VStartFreq", AttenVolts).Replace(".", "_"));
                    paramName = string.Format("ATTEN_SIG_{0}C_{1:####}NM_AVE_{2:G}V", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"), AttenVolts).Replace(".", "_");
                    module.Limits.AddParameter(MainSpec, paramName, string.Format("AttenAverage{0:G}VStopFreq", AttenVolts).Replace(".", "_"));

                }



                if (CoRxDCTestInstruments.Is6464)
                {
                    double[] PdlLevels = new double[] { 10, 15 };

                    foreach (double level in PdlLevels)
                    {
                        string strLevel = level.ToString();

                        paramName = string.Format("VOA_PDL_SIG_{0}C_{1:####}NM_{2}_MX", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"), strLevel);
                        module.Limits.AddParameter(MainSpec, paramName, "VOAMaxPDL_StartFreq" + strLevel);

                        paramName = string.Format("VOA_PDL_SIG_{0}C_{1:####}NM_{2}_MX", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"), strLevel);
                        module.Limits.AddParameter(MainSpec, paramName, "VOAMaxPDL_StopFreq" + strLevel);

                        

                    }
                }
                else {


                    paramName = string.Format("VOA_PDL_SIG_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                    module.Limits.AddParameter(MainSpec, paramName, "VOAMaxPDL_StartFreq");

                    paramName = string.Format("VOA_PDL_SIG_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                    module.Limits.AddParameter(MainSpec, paramName, "VOAMaxPDL_StopFreq");

                    paramName = string.Format("VOA_PDL_POLYDEV_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                    module.Limits.AddParameter(MainSpec, paramName, "PDLMaxPolyFitDeviation_StartFreq");

                    paramName = string.Format("VOA_PDL_POLYDEV_{0}C_{1:####}NM_MAX", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                    module.Limits.AddParameter(MainSpec, paramName, "PDLMaxPolyFitDeviation_StopFreq");

                    paramName = string.Format("VOA_PDL_PFIT_{0}C_{1:####}NM_FILE", temp, speedoflight / TestParmConfig.GetDoubleParam("StartFreq@VOA_GHz"));
                    module.Limits.AddParameter(MainSpec, paramName, "PDLSweepDataFileStart");

                    paramName = string.Format("VOA_PDL_PFIT_{0}C_{1:####}NM_FILE", temp, speedoflight / TestParmConfig.GetDoubleParam("StopFreq@VOA_GHz"));
                    module.Limits.AddParameter(MainSpec, paramName, "PDLSweepDataFileStop");

                
                }
            }
        }


        #endregion



        #region Helper functions

        /// <summary>
        /// Convert a string to an array of integers representing the frequencies
        /// </summary>
        /// <exception cref="ArgumentException">when the input string is not in a valid format</exception>
        /// <param name="rangeStr">Example: "191100:50:191550,193400,196100:-50:195650,193550"</param>
        /// <returns>Array of integers with different frequencies</returns>
        private int[] GetListOfFrequencies(string rangeStr)
        {
            Exception ArgEx = new ArgumentException("Could not convert string below to a valid list of numbers:\n" + rangeStr);
            List<int> freqList = new List<int>();

            string[] temp = rangeStr.Split(',');
            foreach (string FreqLine in temp)
            {

                int freq = 0;
                if (Int32.TryParse(FreqLine, out freq))
                {
                    freqList.Add(freq);
                }
                else if (FreqLine.IndexOf(":") != -1)
                {
                    string[] range = FreqLine.Split(':');
                    if (range.Length != 3)
                        throw ArgEx; //when the are not 3 numbers

                    int StartFreq = 0, StepFreq = 0, LastFreq = 0;
                    if (int.TryParse(range[0], out StartFreq) && int.TryParse(range[1], out StepFreq) && int.TryParse(range[2], out LastFreq))
                    {   //reach here means found 3 integers seperated by two colons (:)
                        //check for valid numbers
                        if (StepFreq < 0)
                        {
                            if (LastFreq >= StartFreq)
                                throw ArgEx;
                            for (int i = StartFreq; i >= LastFreq; i = i + StepFreq)
                            {
                                freqList.Add(i);
                            }
                        }
                        else if (StepFreq > 0)
                        {

                            if (LastFreq <= StartFreq)
                                throw ArgEx;
                            for (int i = StartFreq; i <= LastFreq; i = i + StepFreq)
                            {
                                freqList.Add(i);
                            }
                        }
                        else throw ArgEx; // when step is 0

                    }

                    else throw ArgEx; // when not valid 3 integers in the x:y:z range format

                }
                else throw ArgEx; // when one of the comma seperated items is neither a valid range format (x:y:x) nor a number

            }

            int[] freqArray = new int[freqList.Count];
            freqList.CopyTo(freqArray);
            return freqArray;


        }

        /// <summary>
        /// Read instruments calibration data
        /// </summary>
        /// <param name="InstrumentName"></param>
        /// <param name="InstrumentSerialNo"></param>
        /// <param name="ParameterName"></param>
        /// <param name="CalFactor"></param>
        /// <param name="CalOffset"></param>
        private void ReadCalData(string InstrumentName, string InstrumentSerialNo,
            string ParameterName, out double CalFactor, out double CalOffset)
        {
            StringDictionary keys = new StringDictionary();
            keys.Add("Instrument_name", InstrumentName);
            keys.Add("Parameter", ParameterName);
            if (InstrumentSerialNo != null) // if instrument sn is provided, get cal data by sn
            {
                keys.Add("Instrument_SerialNo", InstrumentSerialNo);
            }
            DatumList calParams = InstrumentsCalData.GetData(keys, false);

            CalFactor = calParams.ReadDouble("CalFactor");
            CalOffset = calParams.ReadDouble("CalOffset");
        }

        /// <summary>
        /// Run at the end of test to archive results for this device
        /// </summary>
        private string ArchiveBlobfiles(DUTObject dutObject)
        {
            string zipFileName = Util_GenerateFileName.GenWithTimestamp(
                TestParmConfig.GetStringParam("ResultsArchiveDirectory"),
                "BlobFiles", dutObject.SerialNumber, "zip");

            if (!Directory.Exists(Path.GetDirectoryName(zipFileName)))
            {
                Directory.CreateDirectory(TestParmConfig.GetStringParam("ResultsArchiveDirectory"));
            }

            if (Directory.Exists(TestParmConfig.GetStringParam("PlotDataTempDirectory")))
            {
                string[] csvFilesToAdd = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.csv", SearchOption.TopDirectoryOnly);
                string[] zipFilesToAdd = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.zip", SearchOption.TopDirectoryOnly);
                string[] txtFilesToAdd = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"),
                    "*" + dutObject.SerialNumber + "*.txt", SearchOption.TopDirectoryOnly);

                if (csvFilesToAdd.Length > 0 || zipFilesToAdd.Length > 0 || txtFilesToAdd.Length > 0)
                {
                    Util_ZipFile zipFile = new Util_ZipFile(zipFileName);
                    
                    using (zipFile)
                    {
                        if (csvFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in csvFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (zipFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in zipFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }

                        if (txtFilesToAdd.Length > 0)
                        {
                            foreach (string fileToAdd in txtFilesToAdd)
                            {
                                zipFile.AddFileToZip(fileToAdd);
                            }
                        }
                    }
                    return zipFileName;
                }
            }
            return null;
        }

        /// <summary>
        /// Run at the start of test to delete any old CSV data
        /// </summary>
        protected void CleanUpBlobFiles()
        {
            if (!Directory.Exists(TestParmConfig.GetStringParam("PlotDataTempDirectory")))
                return;

            string[] filesToRemove = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"), "*.csv");
            string[] zipFilesToRemove = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"), "*.zip");
            string[] txtFilesToRemove = Directory.GetFiles(
                    TestParmConfig.GetStringParam("PlotDataTempDirectory"), "*.txt");

            foreach (string fileToRemove in filesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in zipFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }

            foreach (string fileToRemove in txtFilesToRemove)
            {
                try
                {
                    File.Delete(fileToRemove);
                }
                catch (Exception)
                {
                    // Ignore exceptions on individual files
                }
            }
        }

        /// <summary>
        /// To decide the bias applying to auxilary pd
        ///   the chip wafer no.>=CQRT0072A, then the bias is 14v
        ///   the chip wafer no.< CQRT0072A, then the bias is 10V
        /// </summary>
        /// <param name="waferID"></param>
        /// <returns>Auxilary pd bias level in volt </returns>
        public double GetAuxPdBias_V(string waferID)
        {
            string[] ArrayWaferID = waferID.Split('.');
            string[] ArrayWaferStander = TestParmConfig.GetStringParam("LeastWfId2useHighAuxPdBias").Split('.');

            string TempWaferID = "";
            string WaferStander = "";

            if (ArrayWaferID[0].Length > 8)
            {
                TempWaferID = ArrayWaferID[0].Substring(4, 4);
            }
            else
            {
                TempWaferID = ArrayWaferID[0].Substring(3, 4);
            }

            if (ArrayWaferStander[0].Length > 8)
            {
                WaferStander = ArrayWaferStander[0].Substring(4, 4);
            }
            else
            {
                WaferStander = ArrayWaferStander[0].Substring(3, 4);
            }

            if (string.Compare(TempWaferID, WaferStander, true) >= 0)
            {
                double Vbias = double.Parse(MainSpec.GetParamLimit("TC2_AUX_PD_REVERSE_BIAS").HighLimit.ValueToString()); //TestParmConfig.GetDoubleParam("VauxPD_High_Full_V");
                if ( Vbias >0) Vbias = -Vbias ;
                return Vbias;
            }
            else
            {
                double Vbias = double.Parse(MainSpec.GetParamLimit("TC1_AUX_PD_REVERSE_BIAS").HighLimit.ValueToString());//TestParmConfig.GetDoubleParam("VauxPD_Low_Full_V");
                if (Vbias > 0) Vbias = -Vbias;
                return Vbias;
            }
        }
        #endregion
    }
}
