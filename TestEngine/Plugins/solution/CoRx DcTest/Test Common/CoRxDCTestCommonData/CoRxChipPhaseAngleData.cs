using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Algorithms;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Reflection;

namespace Bookham.TestSolution.CoRxDCTestCommonData
{
    /// <summary>
    /// Class to process phase angle test data with sinusoidal fited
    /// </summary>
    public class CoRxChipPhaseAngleData
    {
        #region data
        /// <summary>
        /// XI waveform data
        /// </summary>
        public readonly Inst_Tds3034.ClsWaveformData WfData_XI;

        /// <summary>
        /// XQ waveform data
        /// </summary>
        public readonly Inst_Tds3034.ClsWaveformData WfData_XQ;

        /// <summary>
        /// YI waveform data
        /// </summary>
        public readonly Inst_Tds3034.ClsWaveformData WfData_YI;

        /// <summary>
        /// YQ waveform data
        /// </summary>
        public readonly Inst_Tds3034.ClsWaveformData WfData_YQ;

        /// <summary>
        /// XI Sinusoidal fit data
        /// </summary>
        public SinusoidalFit SinFit_XI;
        /// <summary>
        ///  XQ Sinusoidal fit data
        /// </summary>
        public SinusoidalFit SinFit_XQ;
        /// <summary>
        ///  YI Sinusoidal fit data
        /// </summary>
        public SinusoidalFit SinFit_YI;
        /// <summary>
        ///  YQ Sinusoidal fit data
        /// </summary>
        public SinusoidalFit SinFit_YQ;

        /// <summary>
        /// An array of coefficients for XI to control the following parameters in the model function : offset, amplitude, power, phase, period
        /// </summary>
        private readonly double[] InitCoeff_XI;
        /// <summary>
        /// An array of coefficients for XQ to control the following parameters in the model function : offset, amplitude, power, phase, period
        /// </summary>
        private readonly double[] InitCoeff_XQ;
        /// <summary>
        /// An array of coefficients for YI to control the following parameters in the model function : offset, amplitude, power, phase, period
        /// </summary>
        private readonly double[] InitCoeff_YI;
        /// <summary>
        /// An array of coefficients for YQ to control the following parameters in the model function : offset, amplitude, power, phase, period
        /// </summary>
        private readonly double[] InitCoeff_YQ;

        private readonly StrcSinusoidalCoefficientsLimit Coeff_Limit;

        /// <summary>
        /// XI initial phase angle in degree in sinusoidal fit
        /// </summary>
        public readonly double Phase_XI_Deg = 999;
        /// <summary>
        /// XQ initial phase angle in degree in sinusoidal fit
        /// </summary>
        public readonly double Phase_XQ_Deg = 999;
        /// <summary>
        /// YI initial phase angle in degree in sinusoidal fit
        /// </summary>
        public readonly double Phase_YI_Deg = 999;
        /// <summary>
        /// YQ initial phase angle in degree in sinusoidal fit
        /// </summary>
        public readonly double Phase_YQ_Deg = 999;

        /// <summary>
        /// Corelation coefficiency between raw data and sinusoidal fit data for XI
        /// </summary>
        public readonly double FitedCorelation_XI;
        /// <summary>
        /// Corelation coefficiency between raw data and sinusoidal fit data for XQ
        /// </summary>
        public readonly double FitedCorelation_XQ;

        /// <summary>
        /// Corelation coefficiency between raw data and sinusoidal fit data for YI
        /// </summary>
        public readonly double FitedCorelation_YI;

        /// <summary>
        /// Correlation coefficiency between raw data and sinusoidal fit data for YQ
        /// </summary>
        public readonly double FitedCorelation_YQ;

        /// <summary>
        /// Max intial phase in sinusodal expression
        /// </summary>
        public const double MaxPhase_Deg = 9999;
        /// <summary>
        /// If the waveform data should be smoothed before fited.
        /// </summary>
        public static bool IsSmoothWaveform = false;
        private static int waveformSmoothFactor = 5;
        /// <summary>
        /// If the waveform data sinusoidal fit should be process in an Excel template or not
        /// </summary>
        public static bool IsFitWithTemplate;
        private static string sinusoidalFitTemplatePath;

        #endregion

        /// <summary>
        /// Constructor, waveform data is fited with sinusoidal  algorithism to get initial phase
        /// </summary>
        /// <param name="wfData_XI"> XI waveform data </param>
        /// <param name="wfData_XQ"> XQ waveform data </param>
        /// <param name="wfData_YI"> YI waveform data </param>
        /// <param name="wfData_YQ"> YQ waveform data </param>
        /// <param name="coeff_Limit"> Limit for sinusoidal fited coeffiencies </param>
        public CoRxChipPhaseAngleData(Inst_Tds3034.ClsWaveformData wfData_XI, Inst_Tds3034.ClsWaveformData wfData_XQ,
            Inst_Tds3034.ClsWaveformData wfData_YI, Inst_Tds3034.ClsWaveformData wfData_YQ,
            StrcSinusoidalCoefficientsLimit coeff_Limit)
        {
            if (wfData_XI.Xdata == null || wfData_XI.Ydata == null)
                throw new Exception(" the X Data or Y Data in XI waveform is null, NOT available for sinusoidal fit");
            if (wfData_XQ.Xdata == null || wfData_XQ.Ydata == null)
                throw new Exception(" the X Data or Y Data in XQ waveform is null, NOT available for sinusoidal fit");
            if (wfData_YI.Xdata == null || wfData_YI.Ydata == null)
                throw new Exception(" the X Data or Y Data in YI waveform is null, NOT available for sinusoidal fit");
            if (wfData_YQ.Xdata == null || wfData_YQ.Ydata == null)
                throw new Exception(" the X Data or Y Data in YQ waveform is null, NOT available for sinusoidal fit");


            if (wfData_XI.Xdata.Length != wfData_XI.Ydata.Length)
                throw new Exception("the length of X data and Ydata in XI waveform is not equal, the waveform data is not available for sinusoidal fit");
            if (wfData_XQ.Xdata.Length != wfData_XQ.Ydata.Length)
                throw new Exception("the length of X data and Ydata in XQ waveform is not equal, the waveform data is not available for sinusoidal fit");
            if (wfData_YI.Xdata.Length != wfData_YI.Ydata.Length)
                throw new Exception("the length of X data and Ydata in YI waveform is not equal, the waveform data is not available for sinusoidal fit");
            if (wfData_YQ.Xdata.Length != wfData_YQ.Ydata.Length)
                throw new Exception("the length of X data and Ydata in YQ waveform is not equal, the waveform data is not available for sinusoidal fit");

            WfData_XI = wfData_XI;
            WfData_XQ = wfData_XQ;
            WfData_YI = wfData_YI;
            WfData_YQ = wfData_YQ;

            if (IsSmoothWaveform)
            {
                double[] smoothData;

                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_XI.Ydata, waveformSmoothFactor);
                    WfData_XI.Ydata = smoothData;
                }
                catch   // the exception caused by the smoothing doesn't need to handle as to ensure the fit can be process in any case
                {
                }
                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_XQ.Ydata, waveformSmoothFactor);
                    WfData_XQ.Ydata = smoothData;
                }
                catch
                { }

                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_YI.Ydata, waveformSmoothFactor);
                    WfData_YI.Ydata = smoothData;
                }
                catch
                { }

                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_YQ.Ydata, waveformSmoothFactor);
                    WfData_YQ.Ydata = smoothData;
                }
                catch
                { }


            }
            // Copy the limits for sinusoidal fited coefficients to be initial fited values
            if (coeff_Limit.Coeff_XI != null)
            {
                InitCoeff_XI = new double[coeff_Limit.Coeff_XI.Length];
                coeff_Limit.Coeff_XI.CopyTo(InitCoeff_XI, 0);
            }
            else
                InitCoeff_XI = null;

            if (coeff_Limit.Coeff_XQ != null)
            {
                InitCoeff_XQ = new double[coeff_Limit.Coeff_XQ.Length];
                coeff_Limit.Coeff_XQ.CopyTo(InitCoeff_XQ, 0);
            }
            else
                InitCoeff_XQ = null;

            if (coeff_Limit.Coeff_YI != null)
            {
                InitCoeff_YI = new double[coeff_Limit.Coeff_YI.Length];
                coeff_Limit.Coeff_YI.CopyTo(InitCoeff_YI, 0);
            }
            else
                InitCoeff_YI = null;

            if (coeff_Limit.Coeff_YQ != null)
            {
                InitCoeff_YQ = new double[coeff_Limit.Coeff_YQ.Length];
                coeff_Limit.Coeff_YQ.CopyTo(InitCoeff_YQ, 0);
            }
            else
                InitCoeff_YQ = null;

            Coeff_Limit = coeff_Limit;

            // fitd Coeff expression as : coeff[1] * Sin( ( x-coeff[3]) * 2* Pi / coeff[4] ) + coeff[0]
            try
            {
                SinFitData();
            }
            catch (Exception ex)   
            {
                
                throw new Exception("This error could occur when Visual C++ 2005 Express has expired. Original error message:" + Environment.NewLine + ex.ToString());
           
            }

            // A* Sin( w*t + q) + offset
            // phase is coeff[3] in radian, have to transfer into degree
            if (SinFit_XI.FittedYArray != null && SinFit_XI.Coeffs[4] != 0)
            {
                Phase_XI_Deg = (-SinFit_XI.Coeffs[3] / SinFit_XI.Coeffs[4] * 360);
                if (SinFit_XI.Coeffs[1] < 0)  // keep the amplitude as positive, if it is negtive, the initial phase should *(-1)
                {
                    SinFit_XI.Coeffs[1] = -SinFit_XI.Coeffs[1];
                    Phase_XI_Deg += 180;
                }

                Phase_XI_Deg %= 360;
                if (Math.Abs(Phase_XI_Deg) > 180)  // adjust the initial phase in range of -180 ~ 180
                {
                    Phase_XI_Deg += (Math.Sign(Phase_XI_Deg) * (-1) * 360);                    
                }
                
            }
            else
                Phase_XI_Deg = MaxPhase_Deg;

            if (SinFit_XQ.FittedYArray != null && SinFit_XQ.Coeffs[4] != 0)
            {
                Phase_XQ_Deg = (-SinFit_XQ.Coeffs[3] / SinFit_XQ.Coeffs[4] * 360);
                if (SinFit_XQ.Coeffs[1] < 0)  // keep the amplitude as positive, if it is negtive, the initial phase should *(-1)
                {
                    SinFit_XQ.Coeffs[1] = -SinFit_XQ.Coeffs[1];
                    Phase_XQ_Deg += 180;
                }

                Phase_XQ_Deg %= 360;
                if (Math.Abs(Phase_XQ_Deg) > 180)   // adjust the initial phase in range of -180 ~ 180
                {
                    Phase_XQ_Deg += (Math.Sign(Phase_XQ_Deg) * (-1) * 360);
                }
               
            }
            else
                Phase_XQ_Deg = MaxPhase_Deg;

            if (SinFit_YI.FittedYArray != null && SinFit_YI.Coeffs[4] != 0)
            {
                Phase_YI_Deg = (-SinFit_YI.Coeffs[3] / SinFit_YI.Coeffs[4] * 360);
                if (SinFit_YI.Coeffs[1] < 0)  // keep the amplitude as positive, if it is negtive, the initial phase should *(-1)
                {
                    SinFit_YI.Coeffs[1] = -SinFit_YI.Coeffs[1];
                    Phase_YI_Deg += 180;
                }

                Phase_YI_Deg %= 360;
                if (Math.Abs(Phase_YI_Deg) > 180)   // adjust the initial phase in range of -180 ~ 180
                {
                    Phase_YI_Deg += (Math.Sign(Phase_YI_Deg) * (-1) * 360);                   
                }
                
            }
            else

                Phase_YI_Deg = MaxPhase_Deg;

            if (SinFit_YQ.FittedYArray != null && SinFit_YQ.Coeffs[4] != 0)
            {
                Phase_YQ_Deg = (-SinFit_YQ.Coeffs[3] / SinFit_YQ.Coeffs[4] * 360);
                if (SinFit_YQ.Coeffs[1] < 0)  // keep the amplitude as positive, if it is negtive, the initial phase should *(-1)
                {
                    SinFit_YQ.Coeffs[1] = -SinFit_YQ.Coeffs[1];
                    Phase_YQ_Deg += 180;
                }

                Phase_YQ_Deg %= 360;
                if (Math.Abs(Phase_YQ_Deg) > 180)  // adjust the initial phase in range of -180 ~ 180
                {
                    Phase_YQ_Deg += (Math.Sign(Phase_YQ_Deg) * (-1) * 360);                    
                }                

            }
            else
                Phase_YQ_Deg = MaxPhase_Deg;

            FitedCorelation_XI = 0;  // if can't cal correlation, set the coeff as 0
            try
            {
                FitedCorelation_XI = GetCorrelationCoeff(wfData_XI, SinFit_XI);
            }
            catch
            { }

            FitedCorelation_XQ = 0;
            try
            {
                FitedCorelation_XQ = GetCorrelationCoeff(wfData_XQ, SinFit_XQ);
            }
            catch
            { }

            FitedCorelation_YI = 0;
            try
            {
                FitedCorelation_YI = GetCorrelationCoeff(wfData_YI, SinFit_YI);
            }
            catch
            { }

            FitedCorelation_YQ = 0;
            try
            {
                FitedCorelation_YQ = GetCorrelationCoeff(wfData_YQ, SinFit_YQ);
            }
            catch
            { }
        }

        /// <summary>
        /// Conatructor, waveform data is process with excel template to get initial phase
        /// </summary>
        /// <param name="wfData_XI"> XI Raw waveform data </param>
        /// <param name="wfData_XQ"> XQ Raw waveform data</param>
        /// <param name="wfData_YI"> YI Raw waveform data</param>
        /// <param name="wfData_YQ"> YQ Raw waveform data</param>
        public CoRxChipPhaseAngleData(Inst_Tds3034.ClsWaveformData wfData_XI, Inst_Tds3034.ClsWaveformData wfData_XQ,
            Inst_Tds3034.ClsWaveformData wfData_YI, Inst_Tds3034.ClsWaveformData wfData_YQ)
        {
            if (wfData_XI.Xdata == null || wfData_XI.Ydata == null)
                throw new Exception(" the X Data or Y Data in XI waveform is null, NOT available for sinusoidal fit");
            if (wfData_XQ.Xdata == null || wfData_XQ.Ydata == null)
                throw new Exception(" the X Data or Y Data in XQ waveform is null, NOT available for sinusoidal fit");
            if (wfData_YI.Xdata == null || wfData_YI.Ydata == null)
                throw new Exception(" the X Data or Y Data in YI waveform is null, NOT available for sinusoidal fit");
            if (wfData_YQ.Xdata == null || wfData_YQ.Ydata == null)
                throw new Exception(" the X Data or Y Data in YQ waveform is null, NOT available for sinusoidal fit");


            if (wfData_XI.Xdata.Length != wfData_XI.Ydata.Length)
                throw new Exception("the length of X data and Ydata in XI waveform is not equal, the waveform data is not available for sinusoidal fit");
            if (wfData_XQ.Xdata.Length != wfData_XQ.Ydata.Length)
                throw new Exception("the length of X data and Ydata in XQ waveform is not equal, the waveform data is not available for sinusoidal fit");
            if (wfData_YI.Xdata.Length != wfData_YI.Ydata.Length)
                throw new Exception("the length of X data and Ydata in YI waveform is not equal, the waveform data is not available for sinusoidal fit");
            if (wfData_YQ.Xdata.Length != wfData_YQ.Ydata.Length)
                throw new Exception("the length of X data and Ydata in YQ waveform is not equal, the waveform data is not available for sinusoidal fit");

            WfData_XI = wfData_XI;
            WfData_XQ = wfData_XQ;
            WfData_YI = wfData_YI;
            WfData_YQ = wfData_YQ;

            #region Smooth y data
            if (IsSmoothWaveform)
            {
                double[] smoothData;

                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_XI.Ydata, waveformSmoothFactor);
                    WfData_XI.Ydata = smoothData;
                }
                catch
                {
                }
                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_XQ.Ydata, waveformSmoothFactor);
                    WfData_XQ.Ydata = smoothData;
                }
                catch
                { }

                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_YI.Ydata, waveformSmoothFactor);
                    WfData_YI.Ydata = smoothData;
                }
                catch
                { }

                try
                {
                    smoothData = Alg_Smoothing.PascalTriangleSmooth(wfData_YQ.Ydata, waveformSmoothFactor);
                    WfData_YQ.Ydata = smoothData;
                }
                catch
                { }

            }

            #endregion

            #region sinusoidal fit with template

            SinFit_XI = FitWaveformWithSinusoidalTemplate(wfData_XI, out Phase_XI_Deg, out FitedCorelation_XI);
            SinFit_XQ = FitWaveformWithSinusoidalTemplate(wfData_XQ, out Phase_XQ_Deg, out FitedCorelation_XQ);
            SinFit_YI = FitWaveformWithSinusoidalTemplate(wfData_YI, out Phase_YI_Deg, out FitedCorelation_YI);
            SinFit_YQ = FitWaveformWithSinusoidalTemplate(wfData_YQ, out Phase_YQ_Deg, out FitedCorelation_YQ);
            
            #endregion

        }

        /// <summary>
        /// Get/Set waveform y data smoothing factor
        /// </summary>
        public static int WaveformSmoothFactor
        {
            get
            {
                return waveformSmoothFactor;

            }
            set
            {
                if (value < 3 || (value % 2) == 0)
                {
                    throw new ArgumentException("Smoothing factor must be an ODD integer greater than 3");
                }
                waveformSmoothFactor = value;
            }
        }

        /// <summary>
        /// Get/Set sinusoidal fit template file path
        /// </summary>
        public static string SinusoidalFitTemplatePath
        {
            get
            {
                return sinusoidalFitTemplatePath;

            }
            set
            {
                if (!System.IO.File.Exists(value))
                {
                    throw new ArgumentException("Sinusoidal fit template does not exist @ " + value +
                        "Please confirm the template's file path and correct in config file");
                }
                sinusoidalFitTemplatePath = value;
            }
        }

        /// <summary>
        /// Fit all wafeworm data with sinusoidal 
        /// </summary>
        private void SinFitData()
        {

            SinFit_XI = Alg_SinusoidalFit.PerformFit(WfData_XI.Xdata, WfData_XI.Ydata, InitCoeff_XI);
            SinFit_XQ = Alg_SinusoidalFit.PerformFit(WfData_XQ.Xdata, WfData_XQ.Ydata, InitCoeff_XQ);
            SinFit_YI = Alg_SinusoidalFit.PerformFit(WfData_YI.Xdata, WfData_YI.Ydata, InitCoeff_YI);
            SinFit_YQ = Alg_SinusoidalFit.PerformFit(WfData_YQ.Xdata, WfData_YQ.Ydata, InitCoeff_YQ);

        }

        /// <summary>
        ///  Calculate the correlation coefficient between the wafeform data and sinusoidal fited data
        /// </summary>
        /// <param name="wfData"> Raw waveform data </param>
        /// <param name="sinFitData"> sinusoidal fited wavefrom data </param>
        /// <returns></returns>
        private double GetCorrelationCoeff(Inst_Tds3034.ClsWaveformData wfData, SinusoidalFit sinFitData)
        {
            if (sinFitData.FittedYArray == null || sinFitData.FittedYArray.Length < 1) return 0;
            if (sinFitData.FittedYArray.Length != wfData.Ydata.Length)
                throw new ArgumentException(" The fited data array length can't match that on Y data in raw, can't calculate the correlation");
            double meanRawData = 0;
            double meanFitData = 0;

            for (int i = 0; i < sinFitData.FittedYArray.Length; i++)
            {
                meanFitData += sinFitData.FittedYArray[i];
                meanRawData += wfData.Ydata[i];
            }
            meanRawData /= wfData.Ydata.Length;
            meanFitData /= sinFitData.FittedYArray.Length;

            double MSE_Raw = 0;  //sum((x-x_mean)*(x-x_mean))
            double MSE_Fit = 0; // sum((y-y_mean) * (y-y_mean))

            double sum_Col = 0;  //sum( (x-x_meean) * ( y-y_mean))

            for (int i = 0; i < sinFitData.FittedYArray.Length; i++)
            {
                MSE_Fit += Math.Pow(sinFitData.FittedYArray[i]-meanFitData, 2);
                MSE_Raw += Math.Pow(wfData.Ydata[i]-meanRawData, 2);

                sum_Col += ((wfData.Ydata[i] - meanRawData) * (sinFitData.FittedYArray[i] - meanFitData));
            }

            if (MSE_Raw == 0 || MSE_Fit == 0)
                throw new ArgumentException(" Mean Square Error for  either the fit data or raw data could be ZERO!");
            // corelation = ( sum( (x-x_meean) * ( y-y_mean))/sqrt( sum((x-x_mean)*(x-x_mean)) * sum((y-y_mean) * (y-y_mean))) )
            double correlation = sum_Col / Math.Sqrt(MSE_Fit * MSE_Raw);
            correlation = Math.Log10(1 / (1 - correlation));  // corelaition eff = log10(1/(1-corelation) )
            return correlation;
        }

        /// <summary>
        /// Fit waveform data into sinusoidal expression, get fited data and Coefficiencies
        /// </summary>
        /// <param name="WfData"> Raw waveform data </param>
        /// <param name="Phase_Deg"> initial phase(deg) in fited sinusoidal expression  </param>
        /// <param name="FitedCorrelationEff"> correlation Coefficiency between raw data and fited data </param>
        /// <returns> Sinusoidal fited waveform expression </returns>
        private SinusoidalFit FitWaveformWithSinusoidalTemplate(Inst_Tds3034.ClsWaveformData WfData,
            out double Phase_Deg, out double FitedCorrelationEff)
        {
            Application excel = null;
            Workbook excWorkBook = null;
            Worksheet excSheet = null;

            try     // open the template and use sheet[0]
            {
                excel = new Application();
                excel.Visible = true;
                if (!(sinusoidalFitTemplatePath.Contains("\\") || sinusoidalFitTemplatePath.Contains("/")))
                    sinusoidalFitTemplatePath = Path.Combine(@"configuration/CoRxDcTest/", sinusoidalFitTemplatePath);
                sinusoidalFitTemplatePath = Path.GetFullPath(sinusoidalFitTemplatePath);

                excWorkBook = excel.Workbooks.Open(sinusoidalFitTemplatePath, Type.Missing, false, Type.Missing, Type.Missing, Type.Missing, true,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                excSheet = excWorkBook.Worksheets[1] as Worksheet;
            }
            catch (Exception ex)
            {
                if (excSheet != null) excSheet = null;
                if (excWorkBook != null) excWorkBook.Close(false, false, Type.Missing);
                if (excel != null) excel.Quit();
                throw ex;
            }

            SinusoidalFit sinFit;
            Phase_Deg = MaxPhase_Deg;
            FitedCorrelationEff = 0;
            if (excSheet != null)
            {
                int row_Start = 21;
                string col_Ydata1 = "AZ";
                string col_Ydata2 = "BA";

                string col_coeff = "AT"; // 46;
                string col_fitData = "Q"; // 14;
                int fit_Length = 141;

                int waveformDataPoint = WfData.Ydata.Length;

                for (int idx = 0; idx < waveformDataPoint; idx++) // fill waveform data to raw data cells area       
                {
                    excSheet.Cells[row_Start + idx, col_Ydata1] = WfData.Ydata[idx];
                    excSheet.Cells[row_Start + idx, col_Ydata2] = WfData.Ydata[idx];

                }
                Range range;
                for (int ii = 19; ii <= 22; ii++)  // check raw data
                {

                    range = excel.get_Range("AS" + ii.ToString(), Missing.Value);
                    
                }

                try  // Run macro in template to get sinusoidal fited efficiencies
                {
                    excel.Run("PhaseFits", Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                      Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                    sinFit = new SinusoidalFit();

                    sinFit.Coeffs = new double[5];
                    sinFit.Coeffs[0] = double.Parse(excSheet.get_Range(excSheet.Cells["22", col_coeff], excSheet.Cells["22", col_coeff]).Text.ToString());
                    sinFit.Coeffs[1] = double.Parse(excSheet.get_Range(excSheet.Cells[21, col_coeff], excSheet.Cells[21, col_coeff]).Text.ToString());
                    sinFit.Coeffs[2] = 1;
                    sinFit.Coeffs[3] = double.Parse(excSheet.get_Range(excSheet.Cells[36, col_coeff], excSheet.Cells[36, col_coeff]).Text.ToString());
                    sinFit.Coeffs[4] = double.Parse(excSheet.get_Range(excSheet.Cells[19, col_coeff], excSheet.Cells[19, col_coeff]).Text.ToString());

                    Phase_Deg = sinFit.Coeffs[3];
                    FitedCorrelationEff = double.Parse(excSheet.get_Range(excSheet.Cells[17, "AQ"], excSheet.Cells[17, "AQ"]).Text.ToString());

                    sinFit.FittedYArray = new double[waveformDataPoint];

                    for (int idx = 0; idx < fit_Length; idx++)
                    {
                        sinFit.FittedYArray[idx] = double.Parse(excSheet.get_Range(excSheet.Cells[21 + idx, col_fitData], excSheet.Cells[21 + idx, col_fitData]).Text.ToString());
                    }
                    for (int idx = fit_Length; idx < waveformDataPoint; idx++)
                    {
                        sinFit.FittedYArray[idx] = 0;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally  // to ensure template is close at any case
                {
                    excSheet = null;
                    if (excWorkBook != null) excWorkBook.Close(false, false, Type.Missing);
                    if (excel != null) excel.Quit();
                }

            }
            else
            {
                throw new Exception("The sinusoidal fit template is not an available templated, please check it @" + sinusoidalFitTemplatePath);
            }
            return sinFit;

        }
    }

    /// <summary>
    /// Structure data of low and upper limit to all waveform's sinusoidal fit
    /// </summary>
    public struct StrcSinusoidalCoefficientsLimit
    { 
        
        /// <summary>
        /// Original   offset, amplitude, power, phase, period for XI data
        /// </summary>
        public double[] Coeff_XI;
        /// <summary>
        /// Original  offset, amplitude, power, phase, period for XQ data
        /// </summary>
        public double[] Coeff_XQ;
        /// <summary>
        /// Original  offset, amplitude, power, phase, period for YI data
        /// </summary>
        public double[] Coeff_YI;
        /// <summary>
        /// Original offset, amplitude, power, phase, period for YQ data
        /// </summary>
        public double[] Coeff_YQ;

    }
}
