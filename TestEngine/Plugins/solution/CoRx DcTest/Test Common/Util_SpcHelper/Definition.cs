﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPC.Rule
{
    public class Definition
    {
        public static double CalculateStdev(List<double> array)
        {
            double stdev = 0;
            double ave = 0;
            double sSum = 0;

            ave = CalculateMean(array);
            array.ForEach(b => sSum += Math.Pow(b - ave, 2));
            stdev = Math.Sqrt(sSum / (array.Count - 1));

            return stdev;
        }

        public static double CalculateMean(List<double> array)
        {
            double sum = 0;
            double ave = 0;

            array.ForEach(b => sum += b);
            ave = sum / array.Count;

            return ave;
        }

        public static void CalculateStdev(string file,string node, string param , out double stdev,out double mean)
        {

            stdev = mean = 0;
            List<string> limitDefine = new List<string>();

            limitDefine = FileAccess.ReadTxtByLine(file);
            int k = 0;
            string[] h = limitDefine[0].Split(',');
            for (int i = 0; i < h.Length; i++)
            {
                if (h[i] == "param")
                {
                    k = i; break;
                }
            }

            foreach (string item in limitDefine)
            {
                string[] line = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (line[0] != node) continue;

                if (line[1] == "Mean")
                {
                    if (!double.TryParse(line[k], out mean))
                        mean = 0;
                }
                else if (line[1] == "Range")
                {
                    if (!double.TryParse(line[k], out stdev))
                        stdev = 999999;
                }
            }
        }
    }
}
