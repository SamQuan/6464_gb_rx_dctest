using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Data ;
using System.Data .OleDb ;
using System.IO;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Config;

namespace Bookham.TestSolution.CoRxDCTestCommonData
{
    /// <summary>
    /// Class to process Splitter Ratio calibration data reading, save , check and so on
    /// </summary>
    public class RxSplitterRatioCalData:IDisposable 
    {
        public const double InfinteRatio = 999; // set the infinitly ratia to no limit in limit file
        private List<SplitterRatioDataUnit> splitterRatioList;
        private SplitterRatioDataUnit[] arrSplitterRatio;
        
        private double frequencyTolerance_Ghz;
        private readonly string splitterName;
         
        /// <summary>
        /// Constructor, to read splitter ratio data from database
        /// </summary>
        /// <param name="splitterName"></param>
        ///<param name="configFilePath"></param>
        ///<param name="dataTableName"> </param>
        public  RxSplitterRatioCalData(string splitterName, string configFilePath, string dataTableName)
        {
            this.splitterName = splitterName;
            GetDatabaseDef(configFilePath, dataTableName);
            splitterRatioList = null; ;
            if (IsDBConfigOK )  // if there is available information to access database, get data from database
                try
                {
                    GetSplitterRatioCalData();
                }
                // not process the exception because in first running, the database is empty, and cause too much exception 
                    // but we still allow it to do splitter calibration
                catch //(Exception ex)   
                {

                }          
            
        }

        #region Database Define and operation

        string dbConfigFilePath;
        string dbConfigTableName;

        /// <summary>
        /// Flag to indicate if available splitter ratio database information is read from config file or not
        /// </summary>
        public bool IsDBConfigOK;
        /// <summary>
        /// Flag to indicate if availble splitter data is retrieved from database
        /// </summary>
        public bool IsDBCalDataOk;
        /// <summary>
        /// Splitter ratio database information
        /// </summary>
        SRCalDatabaseDef databaseDef;
        public string DataSourceStr;
        public string GetSRDataStr;

        private OleDbConnection connDatabase;
        private OleDbDataAdapter adpGetSRData;
        private DataSet dsCalDatabase;
        private OleDbCommand cmdGetSRData;
        private ConfigDataAccessor dbSettingConfig;

        /// <summary>
        /// Update this splitter's last cal time to present
        /// </summary>
        public void UpdateCalDate()
        {
            try
            {   // Read all data from config file
                dbSettingConfig = new ConfigDataAccessor(dbConfigFilePath, dbConfigTableName); 
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Can't update {0)'s last calibarate time" +
                    " to current time with exception of {1}", this.splitterName, ex.Message));
            }

            if (dbSettingConfig == null) return;

            StringDictionary dataKey = new StringDictionary();
            dataKey.Add("SplitterName", this.splitterName);
            dataKey.Add("ParameaterName", "LastCalDateTime");
            DatumList dl = dbSettingConfig.GetData(dataKey, false);
            if (dl == null) // if the file is empty, created information and save it into file
            {
                dl = new DatumList();
                dl.AddString("ParameaterName", "LastCalDateTime");
                dl.AddString("Value", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"));
                dl.AddString("SplitterName", this.splitterName);
                dbSettingConfig.AddData(dl);
            }
            else  // if there is old record data , update the date
            {
                DatumList newDl = (DatumList)dl.Clone();
                newDl.UpdateString("Value", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"));
                dbSettingConfig.UpdateData(dl, newDl);
            }
            dbSettingConfig.SaveChangesToFile();
            dbSettingConfig = null;
        }

        /// <summary>
        /// Get spliter ratio database access infromation from config file
        /// </summary>
        /// <param name="configFilePath"> Config file path </param>
        /// <param name="dataTableName"> data table name in config file </param>
        /// <returns></returns>
        public bool GetDatabaseDef(string configFilePath, string dataTableName)
        {
            if (!File.Exists(configFilePath)) return false;
            IsDBConfigOK = false;
            try
            {
                #region Read database definition from config file
                dbSettingConfig = new ConfigDataAccessor(configFilePath, dataTableName);
                if (dbSettingConfig == null) return false;

                databaseDef = new SRCalDatabaseDef();
                DatumList dataRow = null;
                // Get database informations
                StringDictionary dataKey = new StringDictionary();
                dataKey.Add("SplitterName", this.splitterName);

                dataKey.Add("ParameaterName", "DatabasePath");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    if (!File.Exists(dataRow.ReadString("Value")))
                    {
                        throw new Exception("File doesn't exist @ " + dataRow.ReadString("Value") +
                            this.splitterName + "'s splitter ratio calibration data is not available");
                        //return false;
                    }
                    databaseDef.DatabasePath = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SplitterRatioCalTable");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.DataTableName = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SplitterNameField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.SplitterNameFieldName = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRFrequencyField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.FrequencyFieldName = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRWavelenField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.WavelenFieldName = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRChanNumField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.ChanNumFieldName = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                dataKey.Add("ParameaterName", "SRRatioField");
                dataRow = dbSettingConfig.GetData(dataKey, false);
                if (dataRow != null)
                {
                    databaseDef.RatioFieldName = dataRow.ReadString("Value");
                }
                else  // if no this row, the database definition information is not avaailable
                    return false;
                dataKey.Remove("ParameaterName");

                
                #endregion
            }
            catch
            { return false; }

            finally
            {
                dbSettingConfig = null;
            }
            IsDBConfigOK = true;
            dbConfigTableName = dataTableName;  // Table name in config file
            dbConfigFilePath = configFilePath;
            return true;

        }

        /// <summary>
        /// Get splitter ratio data from database
        /// </summary>
        /// <returns> If get available splitter data from database, return true; or return false </returns>
        public bool GetSplitterRatioCalData()
        {
            // if database define is not correct, return false
            if (!IsDBConfigOK) return false;

            IsDBCalDataOk = false;
            #region Database initialise
            DataSourceStr = @" Provider = Microsoft.Jet.OLEDB.4.0 ; Data Source = " + databaseDef.DatabasePath;
            
            GetSRDataStr = "SELECT DISTINCT * FROM [" + databaseDef.DataTableName +
                "] WHERE [" + databaseDef.SplitterNameFieldName + "] = " +
                "\'" + this.splitterName + "\'";
            try
            {
                connDatabase = new OleDbConnection(DataSourceStr);

                cmdGetSRData = new OleDbCommand();
                cmdGetSRData.Connection = connDatabase;
                cmdGetSRData.CommandText = GetSRDataStr;

                adpGetSRData = new OleDbDataAdapter(cmdGetSRData);
                OleDbCommandBuilder cbInsertData = new OleDbCommandBuilder(adpGetSRData);
                cbInsertData.QuotePrefix = "[";
                cbInsertData.QuoteSuffix = "]";
                adpGetSRData.InsertCommand = cbInsertData.GetInsertCommand();

                // Define Delete SQL command
                string cmdStr = string.Format("DELETE * FROM [{0}] WHERE [{1}] = '{2}'",
                    databaseDef.DataTableName, databaseDef.SplitterNameFieldName, splitterName);
                adpGetSRData.DeleteCommand = new OleDbCommand(cmdStr, connDatabase);
                string chanParamStr = "@" + databaseDef.ChanNumFieldName;
                string srParamStr = "@" + databaseDef.RatioFieldName;
                
                string freqParamStr = "@" + databaseDef.FrequencyFieldName;
                string wavelenParamStr = "@" + databaseDef.WavelenFieldName;
                // Define update Sql command
                cmdStr = string.Format("UPDATE [{0}] \n" +
                    "SET [{1}] =  {2}, [{3}] = {4}\n" +
                    "WHERE {5} = {6} AND {7}={8}", databaseDef.DataTableName,
                    databaseDef.ChanNumFieldName, chanParamStr, databaseDef.RatioFieldName, srParamStr,
                    databaseDef.SplitterNameFieldName, splitterName, databaseDef.FrequencyFieldName, freqParamStr);
                adpGetSRData.UpdateCommand = new OleDbCommand(cmdStr);


            }
            catch (Exception ex)
            {
                IsDBConfigOK = false;
                throw ex;
            }
            finally
            {
                if (connDatabase.State != ConnectionState.Closed) connDatabase.Close();
            }
            dsCalDatabase = new DataSet();


            #endregion

            #region Get splitter ratio cal data from database
            try
            {
                connDatabase.Open();
                dsCalDatabase.Tables.Add(databaseDef.DataTableName);
                adpGetSRData.Fill(dsCalDatabase, databaseDef.DataTableName);
                DataTable dbTable = dsCalDatabase.Tables[databaseDef.DataTableName];
                if (dbTable == null || dbTable.Rows.Count == 0 || dbTable.Columns.Count == 0)   // no this table in database or the data table is empty
                {
                    throw new Exception("RxSplitterRatioCalData.GetSplitterRationCalData: there is no record in the database with connection of " +
                        connDatabase.DataSource + " with sql " + cmdGetSRData.CommandText);
                }
                if (splitterRatioList == null)
                    splitterRatioList = new List<SplitterRatioDataUnit>();
                else
                    splitterRatioList.Clear();      // clear list to ensure all data get from database reacord
                foreach (DataRow row in dbTable.Rows)
                {
                    SplitterRatioDataUnit dataUnit = new SplitterRatioDataUnit();
                    dataUnit.ChanNum = int.Parse(row[databaseDef.ChanNumFieldName].ToString());
                    dataUnit.Frequency_Ghz = double.Parse(row[databaseDef.FrequencyFieldName].ToString());
                    dataUnit.Ratio = double.Parse(row[databaseDef.RatioFieldName].ToString());
                    dataUnit.Wavelen_nM = double.Parse(row[databaseDef.WavelenFieldName].ToString());
                    if (dataUnit.Frequency_Ghz == 0 && dataUnit.Wavelen_nM != 0)  // translate wavelen to frequency
                        dataUnit.Frequency_Ghz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(dataUnit.Wavelen_nM);
                    if (dataUnit.Wavelen_nM == 0 && dataUnit.Frequency_Ghz != 0)  // translate freq to wavelen
                        dataUnit.Wavelen_nM = Math .Round ( Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(dataUnit.Frequency_Ghz),3);
                    splitterRatioList.Add(dataUnit);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connDatabase.State != ConnectionState.Closed) connDatabase.Close();
            }
            // sorted the cal data by frequency to ensure the data in array is sequencialy in frequency
            if (splitterRatioList != null)
            {
                splitterRatioList.Sort();
                arrSplitterRatio = splitterRatioList.ToArray();
            }
            IsDBCalDataOk = (arrSplitterRatio.Length > 0);  // if SR cal data record count > 0, there is available cal data in database
            #endregion

            return true;            
        }

        /// <summary>
        /// Save the available splitter ratio data to file, the previous data will be clear
        /// </summary>
        /// <returns> if data save sucessfully, return true </returns>
        public bool SaveSplitterRatioCalData()
        {
            bool bDataSaved = false;
            // if no cal data, return false
            if (splitterRatioList == null || splitterRatioList.Count == 0)
                return bDataSaved;
            
            if (dsCalDatabase == null) dsCalDatabase = new DataSet();

            try  // save data to database
            {
                connDatabase.Open();
                if (!dsCalDatabase.Tables.Contains(databaseDef.DataTableName))  // If the required table doesn't exist, create it and add it to database
                {
                    dsCalDatabase.Tables.Add(databaseDef.DataTableName);
                    adpGetSRData.Fill(dsCalDatabase, databaseDef.DataTableName);
                }
                // Clear all old record and update to database before save new data
                int num = adpGetSRData.DeleteCommand.ExecuteNonQuery(); 
                // Get data table in database
                DataTable dbTable = dsCalDatabase.Tables[databaseDef.DataTableName];
                
                // Add new cal data to database
                foreach (SplitterRatioDataUnit data in splitterRatioList)
                {
                    DataRow newRow = dbTable.NewRow();
                    newRow[databaseDef.SplitterNameFieldName] = this.splitterName;
                    newRow[databaseDef.FrequencyFieldName] = data.Frequency_Ghz;
                    newRow[databaseDef.WavelenFieldName] = data.Wavelen_nM;
                    newRow[databaseDef.ChanNumFieldName] = data.ChanNum;
                    newRow[databaseDef.RatioFieldName] = data.Ratio;
                    dbTable.Rows.Add(newRow);
                }
                DataSet newDataDS = dsCalDatabase.GetChanges(DataRowState.Added);  // Get added records to updated to data base
                if (newDataDS != null)
                {

                    adpGetSRData.Update(dbTable.Select(null, null, DataViewRowState.Added));
                }

                // Modify last cal date  in cinfigureation file to current time
                UpdateCalDate();
                bDataSaved = true;
            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (System.InvalidOperationException ex)
            {
                throw ex;
            }
            catch (System.Data.DBConcurrencyException ex)
            {
                throw ex;
            }
            finally
            {
                if (connDatabase.State != ConnectionState.Closed) connDatabase.Close();
            }
            return bDataSaved;
        }

        #endregion

        /// <summary>
        /// if ( Freq_Spec - FrequencyTolerance_Ghz <= Freq_toset <= Freq_Spec + FrequencyTolerance_Ghz),
        /// choose splitter ratio at Freq_spec ( splitter ratio won't measure all data at CW,
        /// it just measured at some specified frequencies, so choose a specified sr with frequency shift tolerance 
        /// within range of FrequencyTolerance_Ghz )
        /// </summary>
        public double FrequencyTolerance_Ghz
        {
            get { return frequencyTolerance_Ghz; }
            set { frequencyTolerance_Ghz = value; }
        }
        
        /// <summary>
        /// Get the splitter name
        /// </summary>
        public string SplitterName
        {
            get { return splitterName; }
        } 

        /// <summary>
        /// Get splitter ratio at most approach specified frequency
        /// </summary>
        /// <param name="freq_Ghz"></param>
        /// <returns> splitter ratio at most approach specified frequency </returns>
        public double GetSrByFrequency(double freq_Ghz)
        {
            if (arrSplitterRatio == null || arrSplitterRatio.Length < 1) return InfinteRatio;
            
            int dataIndx = -1;
            for (int indx = 0; indx < arrSplitterRatio.Length; indx++)
            {
                if (Math.Abs(arrSplitterRatio[indx].Frequency_Ghz - freq_Ghz) <= frequencyTolerance_Ghz)
                {
                    dataIndx = indx;
                    break;
                }
            }

            if (dataIndx == -1)  // If can't find the chan's data , set the ratis as "InfinteRatio"
                return InfinteRatio;
            else
                return arrSplitterRatio[dataIndx].Ratio;
        }
        /// <summary>
        /// Get splitter ratio at most approach specified wavelen
        /// </summary>
        /// <param name="wavelen_nM"></param>
        /// <returns> splitter ratio at most approach specified wavelen </returns>
        public double GetSrByWavelen(double wavelen_nM)
        {
            double freq_Ghz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(wavelen_nM);
            return GetSrByFrequency(freq_Ghz);
        }
        /// <summary>
        /// Get aplitter ratio by channel number
        /// </summary>
        /// <param name="chan"></param>
        /// <returns> splitter ratio @ this channnel </returns>
        public double GetSrByChan(int chan)
        {
            int dataIndx = -1;
            for (int indx = 0; indx < arrSplitterRatio.Length ; indx++)
            {
                if (Math.Abs(arrSplitterRatio[indx].ChanNum) == chan)
                {
                    dataIndx = indx;
                    break;
                }
            }

            if (dataIndx == -1)
                return InfinteRatio;
            else
                return arrSplitterRatio[dataIndx].Ratio;

        }

        /// <summary>
        /// Add a new  splitter ratio point
        /// </summary>
        /// <param name="newCalDataPoint"></param>
        public void AddCalData(SplitterRatioDataUnit newCalDataPoint)
        {
            if (splitterRatioList == null) splitterRatioList = new List<SplitterRatioDataUnit>();
            if (!splitterRatioList.Contains(newCalDataPoint))
            { 
                splitterRatioList.Add(newCalDataPoint);
                splitterRatioList.Sort();
                arrSplitterRatio = splitterRatioList.ToArray();
            }
        }

        /// <summary>
        /// Clear splitter ratio data
        /// </summary>
        public void ClearCalData()
        {
            if (splitterRatioList != null)
            {
                splitterRatioList.Clear();
                arrSplitterRatio = null;
            }
        }

        #region IDisposable Members
        /// <summary>
        ///  Dispose all resource
        /// </summary>
        public void Dispose()
        {
            splitterRatioList=null;
            arrSplitterRatio = null;
            connDatabase.Dispose();
            adpGetSRData.Dispose() ; 
            dsCalDatabase.Dispose();
            cmdGetSRData.Dispose();
        }

        #endregion
    }
    /// <summary>
    /// Splitter ratio @ frequency or wavelength
    /// </summary>
    public struct SplitterRatioDataUnit:IComparable 
    {
        public int ChanNum;
        public double Frequency_Ghz;
        public double Wavelen_nM;
        public double Ratio;
        
        #region IComparable Members
        /// <summary>
        /// Caompare data by the frequency
        /// </summary>
        /// <param name="obj">SplitterRatioDataUnit data</param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            SplitterRatioDataUnit data = (SplitterRatioDataUnit)obj;
            
            if (Frequency_Ghz > data.Frequency_Ghz)
                return 1;
            else if (Frequency_Ghz == data.Frequency_Ghz)
                return 0;
            else
                return -1;
        }

        #endregion
    }

    /// <summary>
    /// Define splitter ratio cal data's database
    /// </summary>
    public struct SRCalDatabaseDef
    {
        /// <summary>
        /// Database file path to get splitter ratio, it should be an access file
        /// </summary>
        public string DatabasePath;
        /// <summary>
        /// Data table name inthe data file
        /// </summary>
        public string DataTableName;
        /// <summary>
        /// Field name for Splitter name in data table
        /// </summary>
        public string SplitterNameFieldName;
        /// <summary>
        /// Field name for frequency in data table
        /// </summary>
        public string FrequencyFieldName;
        /// <summary>
        /// Field name for wavelength in data table
        /// </summary>
        public string WavelenFieldName;
        /// <summary>
        /// Field name for channel number in data table
        /// </summary>
        public string ChanNumFieldName;
        /// <summary>
        /// Field name for split ratio in data table
        /// </summary>
        public string RatioFieldName;       
    }
}
