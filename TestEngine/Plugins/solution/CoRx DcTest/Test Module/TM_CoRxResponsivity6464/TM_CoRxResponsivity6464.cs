// [Copyright]
//
// Bookham [Coherent RX DC Test]
// Bookham.TestSolution.TestModules
//
// TM_CoRxResponsivity.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.Instruments;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    internal struct OpticalProperty {

        internal double OpcPower_DBm; 
        internal double Wavelen_nm ;
        internal double ChainFrequency_GHz;
        internal int Chan ;


    }
    /// <summary>
    /// CoRx Dc test, PD responsivity test module
    /// </summary>
    public class TM_CoRxResponsivity6464
        : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// Test PD Responsivity
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, 
            DatumList calData, DatumList previousTestData)
        {
            engine.GuiShow();
            engine.GuiToFront();
            Inst_Ke24xx masterSource =(Inst_Ke24xx) instruments ["Master Source"];
            GuiMsg.TestSweepProgress ShowProgress = new GuiMsg.TestSweepProgress();
            double Vpd_V = configData.ReadDouble("PDBias_V");
            double Ipd_Compliance_A = configData.ReadDouble("PDCompCurrent_A");
            double Vcc_V = configData.ReadDouble("TiaBias_V");
            double Icc_Compliance_A = configData.ReadDouble("TIACompCurrent_A");
            bool isPdSourceUseTrigger = configData.ReadBool("IsPdSourceUseTrigger");
            double wavelen_ref_nm = configData.ReadDouble("ReferenceWavelen_nM");

            #region Get Control inputs settings
            double V_MC_V;
            double Icomp_MC_A;
            double V_GC_V;
            double Icomp_GC_A;
            double V_OC_V;
            double Icomp_OC_A;
            double V_OA_V;
            double Icomp_OA_A;
            double I_ThermPhase_A;
            double Vcomp_ThermPhase_V;
            // gets drive conditions or sets to defaults
            if (configData.IsPresent("MCBias_V"))
            {
                V_MC_V = configData.ReadDouble("MCBias_V");
            }
            else 
            {
                V_MC_V = double.NaN;
            }
            if (configData.IsPresent("MCCurComp_A"))
            {
                Icomp_MC_A = configData .ReadDouble ("MCCurComp_A");
            }
            else 
            {
                Icomp_MC_A =0.01;
            }

            if (configData.IsPresent("GCBias_V"))
            {
                V_GC_V = configData.ReadDouble("GCBias_V");
            }
            else 
            {
                V_GC_V = 2.3;
            }
            if (configData.IsPresent("GCCurComp_A"))
            {
                Icomp_GC_A = configData.ReadDouble("GCCurComp_A");
            }
            else 
            {
                Icomp_GC_A = 0.01;
            }

            if (configData.IsPresent("OCBias_V"))
            {
                V_OC_V = configData.ReadDouble("OCBias_V");
            }
            else 
            {
                V_OC_V = double.NaN;
            }
            if (configData.IsPresent("OCCurComp_A"))
            {
                Icomp_OC_A = configData.ReadDouble("OCCurComp_A");
            }
            else 
            {
                Icomp_OC_A = 0.01;
            }

            if (configData.IsPresent("OABias_V"))
            {
                V_OA_V = configData.ReadDouble("OABias_V");
            }
            else 
            {
                V_OA_V = double.NaN;
            }
            if (configData.IsPresent("OACurComp_A"))
            {
                Icomp_OA_A = configData.ReadDouble("OACurComp_A");
            }
            else 
            {
                Icomp_OA_A = 0.01;
            }

            if (configData.IsPresent("ThermPhaseBias_A"))
            {
                I_ThermPhase_A = configData.ReadDouble("ThermPhaseBias_A");
            }
            else
            {
                I_ThermPhase_A = double.NaN;
            }
            if (configData.IsPresent("ThermPhaseVoltComp_A"))
            {
                Vcomp_ThermPhase_V = configData.ReadDouble("ThermPhaseVoltComp_A");
            }
            else
            {
                Vcomp_ThermPhase_V = 1;
            }

            #endregion

            #region Get plot file name imformation

            string fileDirectory = configData.ReadString("BlobFileDirectory");
            string filePrefix4SigPath = configData.ReadString("FileSufix4SigPath");
            string filePrefix4LocPath = configData.ReadString("FileSufix4LocPath");
                        

            string filePrefix_Resp_sig = configData.ReadString("FileSufix4SigPathResp");
            string filePrefix_DevResp_Sig = configData.ReadString("FileSufix4SigPathDevResp");
            string filePrefix_Resp_loc = configData.ReadString("FileSufix4LocPathResp");
            string filePrefix_DevResp_loc = configData.ReadString("FileSufix4LocPathDevResp");
            string filePrefix_Ripple_loc = configData.ReadString("FileSufix4LocPathRipple");

            // file to record deviation inside single chip
            string filePrefix_XchipDevResp_Sig = configData.ReadString("FileSufix4XchipDevResp@Sig");
            string filePrefix_XchipDevResp_loc = configData.ReadString("FileSufix4XchipDevResp@Loc");
            string filePrefix_YchipDevResp_Sig = configData.ReadString("FileSufix4YchipDevResp@Sig");
            string filePrefix_YchipDevResp_loc = configData.ReadString("FileSufix4YchipDevResp@Loc");

            string filePrefix4RespStat = "";
            try
            {
                filePrefix4RespStat = configData.ReadString("FileSufix4RespStat");
            }
            catch
            { }


            string filePrefix4DevRespStat = "";
            try
            {
                filePrefix4DevRespStat = configData.ReadString("FileSufix4CMRRStat");
            }
            catch
            { }

            string filePrefixTapRespSig = "";
            string filePrefixTapDataSig = "";
            //string filePrefixTapRespLoc = "";
            //string filePrefixTapDataLoc = "";

            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                try
                {
                    filePrefixTapDataSig = configData.ReadString("FilePrefix4TapSigPath");
                }
                catch
                { }

                try
                {
                    filePrefixTapRespSig = configData.ReadString("FilePrefix4TapRespSigPath");
                }
                catch
                { }

            }
            #endregion
           
            #region Get frequency information
            EnumFrequencySetMode freqSetMode;
            int[] freq_Range;
            if (configData.IsPresent("Freq_Range"))
            {
                freq_Range = (int [])configData.ReadReference("Freq_Range");
                freqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            else throw new ArgumentException("Configuration information for the list of frequencies to sweep is not passed to the test module");


            #endregion

            #region Get opc power information
            bool isAdjustSigOpcPower = configData.ReadBool("IsAdjustSigOpcPower");
            double rqsSigOpcPower_DBm;
            if (isAdjustSigOpcPower)
                rqsSigOpcPower_DBm = configData.ReadDouble("RqsSigOpcPower_DBm");
            else
                rqsSigOpcPower_DBm = double.PositiveInfinity;

            bool isAdjustLocOpcPower = configData.ReadBool("IsAdjustLocOpcPower");
            double rqsLocOpcPower_DBm;
            if (isAdjustLocOpcPower)
                rqsLocOpcPower_DBm = configData.ReadDouble("RqsLocOpcPower_DBm");
            else
                rqsLocOpcPower_DBm = double.PositiveInfinity;

            #endregion

            #region Get referenced wavelen's frequency for each laser source
            double freq_sig_Ref_GHz = -1;
            double freq_loc_Ref_GHz = -1;

            try
            {
                CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm = wavelen_ref_nm;
                freq_sig_Ref_GHz = CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz;
            }
            catch
            { }

            try
            {
                CoRxDCTestInstruments.LocalOscInputInstrs.ChainWaveLen_nm = wavelen_ref_nm;
                freq_loc_Ref_GHz = CoRxDCTestInstruments.LocalOscInputInstrs.ChainFrequency_GHz;
            }
            catch
            { }
            #endregion

            #region Test Pd responsivity


            Inst_CoRxITLALaserSource itlaSource_Sig = null;
            Inst_CoRxITLALaserSource itlaSource_Loc = null;

            try
            {
                itlaSource_Sig = (Inst_CoRxITLALaserSource)CoRxDCTestInstruments.SignalInputInstrs.LaserSource;
            }
            catch
            {
            }
            try
            {
                itlaSource_Loc = (Inst_CoRxITLALaserSource)CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource;
            }
            catch
            { }

            if (itlaSource_Loc != null ) CoRxDCTestInstruments.SelectItla(itlaSource_Loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            if (itlaSource_Sig != null )  CoRxDCTestInstruments.SelectItla(itlaSource_Sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            double VPdTap_V =0;
            if (CoRxDCTestInstruments.VOASource != null)
            {
                CoRxDCTestInstruments.VOASource.InitSourceVMeasureI_MeasurementAccuracy(
                    0.01, 0.01, false, 0, 1, 1, true);
                CoRxDCTestInstruments.VOASource.VoltageRange_Volt = 1;
                CoRxDCTestInstruments.VOASource.VoltageSetPoint_Volt = 0;
                CoRxDCTestInstruments.VOASource.OutputEnabled = true;
                System.Threading.Thread.Sleep(100);

            }
            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                double IPdTap_Comp_A = configData.ReadDouble("PDTapCompCurrent_A");
                VPdTap_V = configData.ReadDouble("PDTapBias_V");
                CoRxDCTestInstruments.PdTapSource.InitSourceVMeasureI_MeasurementAccuracy(
                    IPdTap_Comp_A, IPdTap_Comp_A, false, 0, 1, 1, true);
                CoRxDCTestInstruments.PdTapSource.VoltageRange_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.VoltageSetPoint_Volt = VPdTap_V;
                CoRxDCTestInstruments.PdTapSource.OutputEnabled = true;
                System.Threading.Thread.Sleep(100);

            }
            CoRxDCTestInstruments.SetAllAuxPdSourceOutput(false);
            CoRxDCTestInstruments.SetSource2AuxPD(false);
            CoRxDCTestInstruments.InitAllPdSourcesAsVoltSource(Vpd_V, Ipd_Compliance_A);

            CoRxDCTestInstruments.SetAllPdBias(Vpd_V, Ipd_Compliance_A);
            CoRxDCTestInstruments.SetAllPDSourceOutput(true);
            CoRxDCTestInstruments.SetTIABias(Vcc_V, Icc_Compliance_A);
            CoRxDCTestInstruments.setAllTiaSourceOutput(true);
            CoRxDCTestInstruments.SetAllControlLevel(V_MC_V, Icomp_MC_A, V_GC_V, Icomp_GC_A,
                V_OC_V, Icomp_OC_A, V_OA_V, Icomp_OA_A, I_ThermPhase_A, Vcomp_ThermPhase_V);
            CoRxDCTestInstruments.SetupTIA ();
            double curTemp = CoRxDCTestInstruments.TiaSource_X.CurrentActual_amp;
            curTemp =  CoRxDCTestInstruments.TiaSource_Y.CurrentActual_amp;

            CoRxDCTestInstruments.SetupPdSourceTriggerLinkResponsivity(masterSource, Vpd_V , VPdTap_V , freq_Range.Length);
            
            List<CoRxPdRespData> sigPathResList = new List<CoRxPdRespData>();
            List<CoRxPdRespData> locPathResList = new List<CoRxPdRespData>();
            if ( CoRxDCTestInstruments .IsBiasPDTap ) CoRxDCTestInstruments.PdTapSource.OutputEnabled = true;


            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
            engine.SendToGui(new DatumString("","Measuring PD Responsivity on signal path..."));

            if (itlaSource_Sig != null)  CoRxDCTestInstruments.SelectItla(itlaSource_Sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = true;
            System.Threading.Thread.Sleep(100);

            // Test responsivity by channel or frequency
            CoRxPdRespData sigResData_RefFreq = null ;

            CoRxDCTestInstruments.TriggerPdSourceChain(); // get them out of idle
            OpticalProperty[] opticalProperty = new OpticalProperty[freq_Range.Length];
            StrcCoRxPdRespData[] sigPathResReading = new StrcCoRxPdRespData[freq_Range.Length];


            for (int j = 0; j < freq_Range.Length; ++j)
            {
                ShowProgress.Progress = 100 * (double)j / (freq_Range.Length - 1);
                engine.SendToGui(ShowProgress);

                if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                    CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz = freq_Range[j];
                else throw new NotImplementedException("Only set by Frequency implemented so far");

                DateTime TimeToMeasure = DateTime.Now + new TimeSpan(0,0,0,0,100); // set the earliest time to measure to be 100ms from now
                                
                if (isAdjustSigOpcPower)
                    CoRxDCTestInstruments.SignalInputInstrs.SetOpticalPower(rqsSigOpcPower_DBm);

                opticalProperty[j].OpcPower_DBm = CoRxDCTestInstruments.SignalInputInstrs.GetOpticalPower_dBm();
                opticalProperty[j].Wavelen_nm = CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm;
                opticalProperty[j].Chan = CoRxDCTestInstruments.SignalInputInstrs.ChainChan;
                opticalProperty[j].ChainFrequency_GHz = CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz;

                int sleepDuration_ms = (TimeToMeasure - DateTime.Now).Milliseconds; 
                if (sleepDuration_ms > 0)
                    System.Threading.Thread.Sleep(sleepDuration_ms);

                masterSource.TriggerArm();
                System.Threading.Thread.Sleep(30); // wait for measurements to complete before changing wavelength
                
            }

            masterSource.WaitForSweepToFinish(100);
            sigPathResReading = CoRxDCTestInstruments.GetPdRespCurList(isPdSourceUseTrigger, freq_Range.Length);
            CoRxDCTestInstruments.ResetPdSourceTrigger();

            //populate data
            for (int j = 0; j < freq_Range.Length; ++j)
            {
                sigPathResReading[j].OpcPower_DBm = opticalProperty[j].OpcPower_DBm;
                sigPathResReading[j].Wavelen_nm = opticalProperty[j].Wavelen_nm;
                sigPathResReading[j].Chan = opticalProperty[j].Chan;
                // Half the PD results 
                sigPathResReading[j].Ipd_XI1_mA = sigPathResReading[j].Ipd_XI1_mA / 2;
                sigPathResReading[j].Ipd_XQ1_mA = sigPathResReading[j].Ipd_XQ1_mA / 2;
                sigPathResReading[j].Ipd_YI1_mA = sigPathResReading[j].Ipd_YI1_mA / 2;
                sigPathResReading[j].Ipd_YQ1_mA = sigPathResReading[j].Ipd_YQ1_mA / 2;

                CoRxPdRespData sigResDataTemp = new CoRxPdRespData(sigPathResReading[j], true);
                sigPathResList.Add(sigResDataTemp);

                if (freq_sig_Ref_GHz == opticalProperty[j].ChainFrequency_GHz)
                    sigResData_RefFreq = sigResDataTemp;
            }


            ShowProgress.Progress = 0;                          // Clear progress bar ready for LO sweep
            engine.SendToGui(ShowProgress);

            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();
            
            // Local oscilator Path Coupling Eff
            engine.SendToGui(new DatumString("","Measuring PD Responsivity on local osc path..."));
            if (itlaSource_Loc != null ) CoRxDCTestInstruments.SelectItla(itlaSource_Loc);
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = true;
            System.Threading.Thread.Sleep(200);

            // Set lower measuremet range for MPD when measuring the LO
            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                CoRxDCTestInstruments.PdTapSource.InitSourceVMeasureI_MeasurementAccuracy(
                    1.05e-6, 1.05e-6, false, 0, 1, 1, true);
                System.Threading.Thread.Sleep(100);

            }

            CoRxPdRespData locResData_RefFreq = null ;

            CoRxDCTestInstruments.TriggerPdSourceChain(); // get them out of idle
            opticalProperty = new OpticalProperty[freq_Range.Length];
            StrcCoRxPdRespData[] locPathResReading = new StrcCoRxPdRespData[freq_Range.Length];
            for (int j = 0; j < freq_Range.Length; ++j)
            {

                ShowProgress.Progress = 100 * (double)j / (freq_Range.Length - 1);
                engine.SendToGui(ShowProgress);

                if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                    CoRxDCTestInstruments.LocalOscInputInstrs.ChainFrequency_GHz = freq_Range[j];
                else throw new NotImplementedException("Only set by Frequency implemented so far");

                DateTime TimeToMeasure = DateTime.Now + new TimeSpan(0, 0, 0, 0, 100); // set the earliest time to measure to be 100ms from now
                
                if (isAdjustLocOpcPower)
                    CoRxDCTestInstruments.LocalOscInputInstrs.SetOpticalPower(rqsLocOpcPower_DBm);

                opticalProperty[j].OpcPower_DBm = CoRxDCTestInstruments.LocalOscInputInstrs.GetOpticalPower_dBm();
                opticalProperty[j].Wavelen_nm = CoRxDCTestInstruments.LocalOscInputInstrs.ChainWaveLen_nm;
                opticalProperty[j].Chan = CoRxDCTestInstruments.LocalOscInputInstrs.ChainChan;
                opticalProperty[j].ChainFrequency_GHz = CoRxDCTestInstruments.LocalOscInputInstrs.ChainFrequency_GHz;

                int sleepDuration_ms = (TimeToMeasure - DateTime.Now).Milliseconds;
                if (sleepDuration_ms > 0)
                    System.Threading.Thread.Sleep(sleepDuration_ms);

                masterSource.TriggerArm();
                System.Threading.Thread.Sleep(30); // wait for measurements to complete before changing wavelength

            }
            masterSource.WaitForSweepToFinish(100);

            locPathResReading = CoRxDCTestInstruments.GetPdRespCurList(isPdSourceUseTrigger, freq_Range.Length);
            CoRxDCTestInstruments.ResetPdSourceTrigger();

            //populate data
            for (int j = 0; j < freq_Range.Length; ++j) {

                locPathResReading[j].OpcPower_DBm = opticalProperty[j].OpcPower_DBm;
                locPathResReading[j].Wavelen_nm = opticalProperty[j].Wavelen_nm;
                locPathResReading[j].Chan = opticalProperty[j].Chan;
                locPathResReading[j].Ipd_XI1_mA = locPathResReading[j].Ipd_XI1_mA / 2;
                locPathResReading[j].Ipd_XQ1_mA = locPathResReading[j].Ipd_XQ1_mA / 2;
                locPathResReading[j].Ipd_YI1_mA = locPathResReading[j].Ipd_YI1_mA / 2;
                locPathResReading[j].Ipd_YQ1_mA = locPathResReading[j].Ipd_YQ1_mA / 2;
                
                CoRxPdRespData locResDataTemp = new CoRxPdRespData(locPathResReading[j], true);
                locPathResList.Add(locResDataTemp);

                if (freq_loc_Ref_GHz == opticalProperty[j].ChainFrequency_GHz)
                    locResData_RefFreq = locResDataTemp;


            }

            
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(10);
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.ClearUpPdSourceTrigger();
            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            CoRxDCTestInstruments.SetAllPDSourceOutput(false);
            if (CoRxDCTestInstruments.IsBiasPDTap) CoRxDCTestInstruments.PdTapSource.OutputEnabled = false;
            if (CoRxDCTestInstruments.VOASource != null) CoRxDCTestInstruments.VOASource.OutputEnabled = false;
            

            #endregion

            // return data
            DatumList returnData = new DatumList();

            #region Save resp@ ref wavelen

            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                returnData.AddDouble("SigPathResp_Tap@Ref", sigResData_RefFreq.Res_Tap);
                returnData.AddDouble("LocPathResp_Tap@Ref", locResData_RefFreq.Res_Tap);
                // Old LO isolation test
                //double Isolation = 10*Math.Log10( locResData_RefFreq.Res_Tap / sigResData_RefFreq.Res_Tap);
                //returnData.AddDouble("Isolation@Ref", Isolation);

                double MinIsolation = -double.MaxValue;

                // Calculate minimum isolation
                for (int isoIndex = 0; isoIndex < sigPathResList.Count; ++isoIndex)
                {
                    double CalcIsolation = 10 * Math.Log10(locPathResList[isoIndex].Res_Tap  / sigPathResList[isoIndex].Res_Tap );
                    //double CalcIsolation = 10 * Math.Log10((locPathResList[isoIndex].Res_Tap / locPathResList[isoIndex].OpticalPower) / (sigPathResList[isoIndex].Res_Tap / sigPathResList[isoIndex].OpticalPower));
                    if (CalcIsolation > MinIsolation) MinIsolation = CalcIsolation;
                    
                }
                returnData.AddDouble("IsolationMin", Math.Abs( MinIsolation));
            }

            returnData.AddDouble("SigPathResp_XI1@Ref", sigResData_RefFreq.Res_XI1);
            returnData.AddDouble("LocPathResp_XI1@Ref", locResData_RefFreq.Res_XI1);

            returnData.AddDouble("SigPathResp_XQ1@Ref", sigResData_RefFreq.Res_XQ1);
            returnData.AddDouble("LocPathResp_XQ1@Ref", locResData_RefFreq.Res_XQ1);

            returnData.AddDouble("SigPathResp_YI1@Ref", sigResData_RefFreq.Res_YI1);
            returnData.AddDouble("LocPathResp_YI1@Ref", locResData_RefFreq.Res_YI1);

            returnData.AddDouble("SigPathResp_YQ1@Ref", sigResData_RefFreq.Res_YQ1);
            returnData.AddDouble("LocPathResp_YQ1@Ref", locResData_RefFreq.Res_YQ1);

            #endregion

            #region Save plot file
            List<string> sigPathDataStrs = new List<string>();
            List <string > locPathDataStrs = new List<string>();
            List<string> sigRespStrs = new List<string>();
            List<string> locRespStrs = new List<string>();
            List<string> sigDevStrs = new List<string>();
            List<string> locDevStrs = new List<string>();

            List<string> xChipDevRspStrs_Sig = new List<string>();
            List<string> xChipDevRspStrs_Loc = new List<string>();
            List<string> yChipDevRspStrs_Sig = new List<string>();
            List<string> yChipDevRspStrs_Loc = new List<string>();
          
            //chaoqun
            List<string> locRippleStrs = new List<string>();

            List<string> lstTapData_Sig = new List<string>();
            List<string> lstTapResp_Sig = new List<string>();
            List<string> lstTapData_Loc = new List<string>();
            List<string> lstTapResp_Loc = new List<string>();

            if (CoRxDCTestInstruments .IsBiasPDTap )
                sigPathDataStrs.Add(CoRxPdRespData.GetDataStringHeader6464());
            else 
                sigPathDataStrs .Add (CoRxPdRespData .GetPdDataStringHeader6464());
            sigRespStrs.Add(CoRxPdRespData.GetPdRespStringHeader6464());
            sigDevStrs.Add(CoRxPdRespData.GetDevRespStringHeader6464());
            xChipDevRspStrs_Sig .Add (CoRxPdRespData.GetChipDevRespStringHeader("X"));
            yChipDevRspStrs_Sig .Add (CoRxPdRespData .GetChipDevRespStringHeader ("Y"));

            lstTapResp_Sig .Add ( CoRxPdRespData.GetTapRespStringHeader());
            lstTapData_Sig .Add (CoRxPdRespData .GetTapDataStringHeader ());

            foreach (CoRxPdRespData dataTemp in sigPathResList)
            {
                
                sigDevStrs .Add (dataTemp .GetDevRespString6464() );
                sigRespStrs.Add(dataTemp .GetPdRespString6464());
                xChipDevRspStrs_Sig .Add ( dataTemp.GetChipDevRespString6464("X"));
                yChipDevRspStrs_Sig .Add (dataTemp .GetChipDevRespString6464 ("Y"));

                if ( CoRxDCTestInstruments .IsBiasPDTap )
                {
                    sigPathDataStrs .Add( dataTemp .DataToString6464());
                    lstTapData_Sig.Add(dataTemp.GetTapDataString());
                    lstTapResp_Sig.Add(dataTemp.GetTapRespString());
                }
                else 
                    sigPathDataStrs .Add( dataTemp .GetPdDataToString6464());
            }


            if (CoRxDCTestInstruments.IsBiasPDTap)
                locPathDataStrs.Add(CoRxPdRespData.GetDataStringHeader6464());
            else
                locPathDataStrs.Add(CoRxPdRespData.GetPdDataStringHeader6464());

            locDevStrs.Add(CoRxPdRespData.GetDevRespStringHeader6464());
            locRespStrs.Add(CoRxPdRespData.GetPdRespStringHeader6464());
            xChipDevRspStrs_Loc .Add (CoRxPdRespData .GetChipDevRespStringHeader ("X"));
            yChipDevRspStrs_Loc .Add (CoRxPdRespData .GetChipDevRespStringHeader ("Y"));

            // Fit the pd reap with 2 power poly fit
            locRippleStrs.Add(CoRxPdRespData.GetPdRippleStringHeader());

            double[] XI1 = new double[locPathResList.Count];
            double[] XQ1 = new double[locPathResList.Count];
            double[] YI1 = new double[locPathResList.Count];
            double[] YQ1 = new double[locPathResList.Count];
            double[] wavelength = new double[locPathResList.Count]; 
            int i=0;
            
            foreach (CoRxPdRespData datatemp in locPathResList)
            {
                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    locPathDataStrs.Add(datatemp.DataToString6464());
                    lstTapData_Loc.Add(datatemp.GetTapDataString());
                    lstTapResp_Loc.Add(datatemp.GetTapRespString());
                }
                else
                    locPathDataStrs.Add(datatemp.GetPdDataToString6464());

                locDevStrs.Add(datatemp.GetDevRespString6464());
                locRespStrs.Add(datatemp.GetPdRespString6464());

                xChipDevRspStrs_Loc.Add (datatemp .GetChipDevRespString6464("X"));
                yChipDevRspStrs_Loc .Add (datatemp .GetChipDevRespString6464 ("Y"));

                XI1[i] = datatemp.Res_XI1;
                XQ1[i] = datatemp.Res_XQ1;
                YI1[i] = datatemp.Res_YI1;
                YQ1[i] = datatemp.Res_YQ1;
                wavelength[i] = datatemp.TestData.Wavelen_nm;
                i++;
            }

            PolyFit yFitXI1 = Alg_PolyFit.PolynomialFit(wavelength, XI1, 2);
            PolyFit yFitXQ1 = Alg_PolyFit.PolynomialFit(wavelength, XQ1, 2);
            PolyFit yFitYI1 = Alg_PolyFit.PolynomialFit(wavelength, YI1, 2);
            PolyFit yFitYQ1 = Alg_PolyFit.PolynomialFit(wavelength, YQ1, 2);

            // calculate ripples' parameter
            i = 0;
            double maxRipple = 0;
            foreach (CoRxPdRespData datatemp in locPathResList)
            {
                datatemp.Ripple_XI1 = Math.Abs(yFitXI1.FittedYArray[i] - XI1[i]) / yFitXI1.FittedYArray[i] * 100;
                datatemp.Ripple_XQ1 = Math.Abs(yFitXQ1.FittedYArray[i] - XQ1[i]) / yFitXQ1.FittedYArray[i] * 100;
                datatemp.Ripple_YI1 = Math.Abs(yFitYI1.FittedYArray[i] - YI1[i]) / yFitYI1.FittedYArray[i] * 100;
                datatemp.Ripple_YQ1 = Math.Abs(yFitYQ1.FittedYArray[i] - YQ1[i]) / yFitYQ1.FittedYArray[i] * 100;
                locRippleStrs.Add(datatemp.GetPdRippleString());
                if (datatemp.Ripple_XI1 > maxRipple) { maxRipple = datatemp.Ripple_XI1; }
                if (datatemp.Ripple_XQ1 > maxRipple) { maxRipple = datatemp.Ripple_XQ1; }
                if (datatemp.Ripple_YI1 > maxRipple) { maxRipple = datatemp.Ripple_YI1; }
                if (datatemp.Ripple_YQ1 > maxRipple) { maxRipple = datatemp.Ripple_YQ1; }
                i++;
  
            }
           
            
            using (CsvWriter writer = new CsvWriter())
            {
                try
                {
                    // write plot files
                    string filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix4SigPath, "", "csv");
                    writer.WriteFile(filename, sigPathDataStrs);
                    returnData.AddFileLink("SigPathDataFile", filename);
                    returnData.AddFileLink("SigPathDataPlot", filename);

                    filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix4LocPath, "", "csv");
                    writer.WriteFile(filename, locPathDataStrs);
                    returnData.AddFileLink("LocPathDataFile", filename);
                    returnData.AddFileLink("LocPathDataPlot", filename);

                    filename = Util_GenerateFileName.GenWithTimestamp (fileDirectory, filePrefix_DevResp_loc ,"","csv");
                    writer .WriteFile(filename , locDevStrs);
                    returnData .AddFileLink ("LocDevRespDataFile", filename );
                    filename = Util_GenerateFileName .GenWithTimestamp (fileDirectory, filePrefix_DevResp_Sig , "","csv");
                    writer .WriteFile (filename , sigDevStrs );
                    returnData .AddFileLink ("SigDevRespDataFile", filename );
                    filename = Util_GenerateFileName .GenWithTimestamp (fileDirectory , filePrefix_Resp_loc ,"","csv");
                    writer .WriteFile (filename , locRespStrs );
                    returnData .AddFileLink ("LocRespDataFile", filename );
                    filename = Util_GenerateFileName .GenWithTimestamp (fileDirectory , filePrefix_Resp_sig, "","csv");
                    writer .WriteFile (filename , sigRespStrs );
                    returnData .AddFileLink ("SigRespDataFile", filename);

                   
                    //add ripple file by chaoqun
                    filename = Util_GenerateFileName .GenWithTimestamp (fileDirectory , filePrefix_Ripple_loc, "","csv");
                    writer .WriteFile (filename , locRippleStrs );
                    returnData .AddFileLink ("LocRippleDataFile", filename);

                    filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix_XchipDevResp_loc, "", "csv");
                    writer.WriteFile(filename, xChipDevRspStrs_Loc);
                    returnData.AddFileLink("XChipDevRespDataFile@Loc", filename);
                    filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix_XchipDevResp_Sig, "", "csv");
                    writer.WriteFile(filename, xChipDevRspStrs_Sig);
                    returnData.AddFileLink("XChipDevRespDataFile@Sig", filename);
                    filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix_YchipDevResp_loc, "", "csv");
                    writer.WriteFile(filename, yChipDevRspStrs_Loc);
                    returnData.AddFileLink("YChipDevRespDataFile@Loc", filename);
                    filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix_YchipDevResp_Sig, "", "csv");
                    writer.WriteFile(filename, yChipDevRspStrs_Sig);
                    returnData.AddFileLink("YChipDevRespDataFile@Sig", filename);

                    if (CoRxDCTestInstruments .IsBiasPDTap )
                    {
                        filename = Util_GenerateFileName .GenWithTimestamp (fileDirectory , filePrefixTapDataSig ,"","csv");
                        writer .WriteFile (filename , lstTapData_Sig );
                        returnData .AddFileLink ("SigTapDataFile", filename );
                        filename = Util_GenerateFileName .GenWithTimestamp (fileDirectory , filePrefixTapRespSig, "","csv");
                        writer .WriteFile (filename , lstTapResp_Sig );
                        returnData .AddFileLink ("SigTapRespDataFile", filename );


                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion

            #region statistic for Min, Max and Avg

            #region Signal path param
            double sigPathRes_XI1_Min = double .PositiveInfinity;
            double sigPathRes_XI1_Max = double .NegativeInfinity;
            double sigPathRes_XI1_Sum = 0;

            double sigPathRes_XI2_Min = double .PositiveInfinity;
            double sigPathRes_XI2_Max = double .NegativeInfinity;
            double sigPathRes_XI2_Sum = 0;

            double sigPathRes_XQ2_Min = double .PositiveInfinity;
            double sigPathRes_XQ2_Max = double .NegativeInfinity;
            double sigPathRes_XQ2_Sum = 0;

            double sigPathRes_XQ1_Min = double .PositiveInfinity;
            double sigPathRes_XQ1_Max = double .NegativeInfinity;
            double sigPathRes_XQ1_Sum=0;

            double sigPathDeltaRes_XI1_Min = double .PositiveInfinity;
            double sigPathDeltaRes_XI1_Max = 0;
            double sigPathDeltaRes_XI1_Sum = 0;

            double sigPathDeltaRes_XI2_Max = 0;
            double sigPathDeltaRes_XI2_Min = double .PositiveInfinity;
            double sigPathDeltaRes_XI2_Sum = 0;

            double sigPathDeltaRes_XQ2_Max = 0;
            double sigPathDeltaRes_XQ2_Min = double .PositiveInfinity;
            double sigPathDeltaRes_XQ2_Sum = 0;

            double sigPathDeltaRes_XQ1_Max = 0;
            double sigPathDeltaRes_XQ1_Min = double .PositiveInfinity;
            double sigPathDeltaRes_XQ1_Sum = 0;

            double xDeltaRes_Sig_XI1_Min = double .PositiveInfinity;
            double xDeltaRes_Sig_XI1_Max = 0;
            double xDeltaRes_Sig_XI1_Sum = 0;

            double xDeltaRes_Sig_XI2_Max = 0;
            double xDeltaRes_Sig_XI2_Min = double .PositiveInfinity;
            double xDeltaRes_Sig_XI2_Sum = 0;

            double xDeltaRes_Sig_XQ2_Max = 0;
            double xDeltaRes_Sig_XQ2_Min = double .PositiveInfinity;
            double xDeltaRes_Sig_XQ2_Sum = 0;

            double xDeltaRes_Sig_XQ1_Max = 0;
            double xDeltaRes_Sig_XQ1_Min = double .PositiveInfinity;
            double xDeltaRes_Sig_XQ1_Sum = 0;

            double sigPathCMRR_XI_Max = double.NegativeInfinity ;
            double sigPathCMRR_XI_Min= double .PositiveInfinity ; 
            double sigPathCMRR_XI_Sum = 0;

            double sigPathCMRR_XQ_Max = double .NegativeInfinity;
            double sigPathCMRR_XQ_Min = double .PositiveInfinity;
            double sigPathCMRR_XQ_Sum = 0;

            double sigPathRes_YI1_Min = double .PositiveInfinity;
            double sigPathRes_YI1_Max = double .NegativeInfinity;
            double sigPathRes_YI1_Sum = 0;

            double sigPathRes_YI2_Min = double .PositiveInfinity;
            double sigPathRes_YI2_Max = double .NegativeInfinity;
            double sigPathRes_YI2_Sum = 0;

            double sigPathRes_YQ2_Min = double .PositiveInfinity;
            double sigPathRes_YQ2_Max = double .NegativeInfinity;
            double sigPathRes_YQ2_Sum = 0;

            double sigPathRes_YQ1_Min = double .PositiveInfinity;
            double sigPathRes_YQ1_Max = double .NegativeInfinity;
            double sigPathRes_YQ1_Sum = 0;

            double sigPathDeltaRes_YI1_Min = double .PositiveInfinity;
            double sigPathDeltaRes_YI1_Max = 0;
            double sigPathDeltaRes_YI1_Sum = 0;

            double sigPathDeltaRes_YI2_Max = 0;
            double sigPathDeltaRes_YI2_Min = double .PositiveInfinity;
            double sigPathDeltaRes_YI2_Sum = 0;

            double sigPathDeltaRes_YQ2_Max = 0;
            double sigPathDeltaRes_YQ2_Min = double .PositiveInfinity;
            double sigPathDeltaRes_YQ2_Sum = 0;

            double sigPathDeltaRes_YQ1_Max = 0;
            double sigPathDeltaRes_YQ1_Min = double .PositiveInfinity;            
            double sigPathDeltaRes_YQ1_Sum = 0;

            double yDeltaRes_Sig_YI1_Min = double .PositiveInfinity;
            double yDeltaRes_Sig_YI1_Max = 0;
            double yDeltaRes_Sig_YI1_Sum = 0;

            double yDeltaRes_Sig_YI2_Max = 0;
            double yDeltaRes_Sig_YI2_Min = double .PositiveInfinity;
            double yDeltaRes_Sig_YI2_Sum = 0;

            double yDeltaRes_Sig_YQ2_Max = 0;
            double yDeltaRes_Sig_YQ2_Min = double .PositiveInfinity;
            double yDeltaRes_Sig_YQ2_Sum = 0;

            double yDeltaRes_Sig_YQ1_Max = 0;
            double yDeltaRes_Sig_YQ1_Min = double .PositiveInfinity;
            double yDeltaRes_Sig_YQ1_Sum = 0;

            double sigPathCMRR_YI_Max = double .NegativeInfinity;
            double sigPathCMRR_YI_Min = double .PositiveInfinity;
            double sigPathCMRR_YI_Sum=0;

            double sigPathCMRR_YQ_Max = double .NegativeInfinity;
            double sigPathCMRR_YQ_Min = double .PositiveInfinity;
            double sigPathCMRR_YQ_Sum=0;

            double sigPathRes_Tap_Min = double.PositiveInfinity;
            double sigPathRes_Tap_Max = double.NegativeInfinity;
            double sigPathRes_Tap_Sum = 0;

            double locLaneRipple_Max = double.PositiveInfinity;
            double maxRespImbalance_Sig = 0;
            double maxRespImbBalance_X_Sig = 0;
            double maxRespImbBalance_Y_Sig = 0;


            double minPdResp_Sig = double.PositiveInfinity;

            foreach (CoRxPdRespData dataTemp in sigPathResList)
            {
                sigPathCMRR_XI_Max = Math.Max(dataTemp.CMRR_XI, sigPathCMRR_XI_Max);
                sigPathCMRR_XI_Min = Math.Min(dataTemp.CMRR_XI, sigPathCMRR_XI_Min);
                sigPathCMRR_XI_Sum += dataTemp.CMRR_XI;

                sigPathCMRR_XQ_Max = Math.Max(dataTemp.CMRR_XQ, sigPathCMRR_XQ_Max);
                sigPathCMRR_XQ_Min = Math.Min(dataTemp.CMRR_XQ, sigPathCMRR_XQ_Min);
                sigPathCMRR_XQ_Sum += dataTemp.CMRR_XQ;

                sigPathCMRR_YI_Max = Math.Max(dataTemp.CMRR_YI, sigPathCMRR_YI_Max);
                sigPathCMRR_YI_Min = Math.Min(dataTemp.CMRR_YI, sigPathCMRR_YI_Min);
                sigPathCMRR_YI_Sum += dataTemp.CMRR_YI;

                sigPathCMRR_YQ_Max = Math.Max(dataTemp.CMRR_YQ, sigPathCMRR_YQ_Max);
                sigPathCMRR_YQ_Min = Math.Min(dataTemp.CMRR_YQ, sigPathCMRR_YQ_Min);
                sigPathCMRR_YQ_Sum += dataTemp.CMRR_YQ;

                sigPathDeltaRes_XI1_Max = Math.Abs(dataTemp.DeltaRes_XI1) > Math .Abs ( sigPathDeltaRes_XI1_Max)? dataTemp.DeltaRes_XI1: sigPathDeltaRes_XI1_Max;
                sigPathDeltaRes_XI1_Min = Math.Abs (dataTemp.DeltaRes_XI1)< Math .Abs ( sigPathDeltaRes_XI1_Min)? dataTemp.DeltaRes_XI1: sigPathDeltaRes_XI1_Min;
                sigPathDeltaRes_XI1_Sum += dataTemp.DeltaRes_XI1;

                sigPathDeltaRes_XI2_Max = Math.Abs(dataTemp.DeltaRes_XI2)> Math .Abs ( sigPathDeltaRes_XI2_Max)? dataTemp.DeltaRes_XI2 : sigPathDeltaRes_XI2_Max;
                sigPathDeltaRes_XI2_Min = Math.Abs(dataTemp.DeltaRes_XI2)< Math .Abs( sigPathDeltaRes_XI2_Min) ? dataTemp.DeltaRes_XI2 : sigPathDeltaRes_XI2_Min;
                sigPathDeltaRes_XI2_Sum += dataTemp.DeltaRes_XI2;

                sigPathDeltaRes_XQ1_Max = Math.Abs(dataTemp.DeltaRes_XQ1) > Math.Abs(sigPathDeltaRes_XQ1_Max) ? dataTemp.DeltaRes_XQ1 : sigPathDeltaRes_XQ1_Max;
                sigPathDeltaRes_XQ1_Min = Math.Abs(dataTemp.DeltaRes_XQ1) < Math.Abs(sigPathDeltaRes_XQ1_Min) ? dataTemp.DeltaRes_XQ1 : sigPathDeltaRes_XQ1_Min;
                sigPathDeltaRes_XQ1_Sum += dataTemp.DeltaRes_XQ1;

                sigPathDeltaRes_XQ2_Max = Math.Abs(dataTemp.DeltaRes_XQ2) > Math.Abs(sigPathDeltaRes_XQ2_Max) ? dataTemp.DeltaRes_XQ2 : sigPathDeltaRes_XQ2_Max;
                sigPathDeltaRes_XQ2_Min = Math.Abs(dataTemp.DeltaRes_XQ2) < Math.Abs(sigPathDeltaRes_XQ2_Min) ? dataTemp.DeltaRes_XQ2 : sigPathDeltaRes_XQ2_Min;
                sigPathDeltaRes_XQ2_Sum += dataTemp.DeltaRes_XQ2;

                sigPathDeltaRes_YI1_Max = Math.Abs(dataTemp.DeltaRes_YI1) > Math.Abs(sigPathDeltaRes_YI1_Max) ? dataTemp.DeltaRes_YI1 : sigPathDeltaRes_YI1_Max;
                sigPathDeltaRes_YI1_Min = Math.Abs(dataTemp.DeltaRes_YI1) < Math.Abs(sigPathDeltaRes_YI1_Min) ? dataTemp.DeltaRes_YI1 : sigPathDeltaRes_YI1_Min;
                sigPathDeltaRes_YI1_Sum += dataTemp .DeltaRes_YI1 ;

                sigPathDeltaRes_YI2_Max = Math.Abs(dataTemp.DeltaRes_YI2) > Math.Abs(sigPathDeltaRes_YI2_Max) ? dataTemp.DeltaRes_YI2 : sigPathDeltaRes_YI2_Max;
                sigPathDeltaRes_YI2_Min = Math.Abs(dataTemp.DeltaRes_YI2) < Math.Abs(sigPathDeltaRes_YI2_Min) ? dataTemp.DeltaRes_YI2 : sigPathDeltaRes_YI2_Min;
                sigPathDeltaRes_YI2_Sum += dataTemp.DeltaRes_YI2;

                sigPathDeltaRes_YQ1_Max = Math.Abs(dataTemp.DeltaRes_YQ1) > Math.Abs(sigPathDeltaRes_YQ1_Max) ? dataTemp.DeltaRes_YQ1 : sigPathDeltaRes_YQ1_Max;
                sigPathDeltaRes_YQ1_Min = Math.Abs(dataTemp.DeltaRes_YQ1) < Math.Abs(sigPathDeltaRes_YQ1_Min) ? dataTemp.DeltaRes_YQ1 : sigPathDeltaRes_YQ1_Min;
                sigPathDeltaRes_YQ1_Sum += dataTemp.DeltaRes_YQ1;

                sigPathDeltaRes_YQ2_Max = Math.Abs(dataTemp.DeltaRes_YQ2) > Math.Abs(sigPathDeltaRes_YQ2_Max) ? dataTemp.DeltaRes_YQ2 : sigPathDeltaRes_YQ2_Max;
                sigPathDeltaRes_YQ2_Min = Math.Abs(dataTemp.DeltaRes_YQ2) < Math.Abs(sigPathDeltaRes_YQ2_Min) ? dataTemp.DeltaRes_YQ2 : sigPathDeltaRes_YQ2_Min;
                sigPathDeltaRes_YQ2_Sum += dataTemp.DeltaRes_YQ2;

                xDeltaRes_Sig_XI1_Max = Math.Abs(dataTemp.XDeltaRes_XI1) > Math .Abs( xDeltaRes_Sig_XI1_Max)? dataTemp.XDeltaRes_XI1: xDeltaRes_Sig_XI1_Max;
                xDeltaRes_Sig_XI1_Min = Math.Abs(dataTemp.XDeltaRes_XI1) < Math .Abs( xDeltaRes_Sig_XI1_Min)? dataTemp.XDeltaRes_XI1: xDeltaRes_Sig_XI1_Min;
                xDeltaRes_Sig_XI1_Sum += dataTemp.XDeltaRes_XI1;

                xDeltaRes_Sig_XI2_Max = Math.Abs(dataTemp.XDeltaRes_XI2) > Math .Abs(xDeltaRes_Sig_XI2_Max)? dataTemp.XDeltaRes_XI2 : xDeltaRes_Sig_XI2_Max;
                xDeltaRes_Sig_XI2_Min = Math.Abs(dataTemp.XDeltaRes_XI2) < Math .Abs(xDeltaRes_Sig_XI2_Min)? dataTemp.XDeltaRes_XI2 : xDeltaRes_Sig_XI2_Min;
                xDeltaRes_Sig_XI2_Sum += dataTemp.XDeltaRes_XI2;

                xDeltaRes_Sig_XQ1_Max = Math.Abs(dataTemp.XDeltaRes_XQ1) > Math.Abs(xDeltaRes_Sig_XQ1_Max) ? dataTemp.XDeltaRes_XQ1 : xDeltaRes_Sig_XQ1_Max;
                xDeltaRes_Sig_XQ1_Min = Math.Abs(dataTemp.XDeltaRes_XQ1) < Math.Abs(xDeltaRes_Sig_XQ1_Min) ? dataTemp.XDeltaRes_XQ1 : xDeltaRes_Sig_XQ1_Min;
                xDeltaRes_Sig_XQ1_Sum += dataTemp.XDeltaRes_XQ1;

                xDeltaRes_Sig_XQ2_Max = Math.Abs(dataTemp.XDeltaRes_XQ2) > Math.Abs(xDeltaRes_Sig_XQ2_Max) ? dataTemp.XDeltaRes_XQ2 : xDeltaRes_Sig_XQ2_Max;
                xDeltaRes_Sig_XQ2_Min = Math.Abs(dataTemp.XDeltaRes_XQ2) < Math.Abs(xDeltaRes_Sig_XQ2_Min) ? dataTemp.XDeltaRes_XQ2 : xDeltaRes_Sig_XQ2_Min;
                xDeltaRes_Sig_XQ2_Sum += dataTemp.XDeltaRes_XQ2;

                yDeltaRes_Sig_YI1_Max = Math.Abs(dataTemp.YDeltaRes_YI1) > Math.Abs(yDeltaRes_Sig_YI1_Max) ? dataTemp.YDeltaRes_YI1 : yDeltaRes_Sig_YI1_Max;
                yDeltaRes_Sig_YI1_Min = Math.Abs(dataTemp.YDeltaRes_YI1) < Math.Abs(yDeltaRes_Sig_YI1_Min) ? dataTemp.YDeltaRes_YI1 : yDeltaRes_Sig_YI1_Min;
                yDeltaRes_Sig_YI1_Sum += dataTemp.YDeltaRes_YI1 ;

                yDeltaRes_Sig_YI2_Max = Math.Abs(dataTemp.YDeltaRes_YI2) > Math.Abs(yDeltaRes_Sig_YI2_Max) ? dataTemp.YDeltaRes_YI2 : yDeltaRes_Sig_YI2_Max;
                yDeltaRes_Sig_YI2_Min = Math.Abs(dataTemp.YDeltaRes_YI2) < Math.Abs(yDeltaRes_Sig_YI2_Min) ? dataTemp.YDeltaRes_YI2 : yDeltaRes_Sig_YI2_Min;
                yDeltaRes_Sig_YI2_Sum += dataTemp.YDeltaRes_YI2;

                yDeltaRes_Sig_YQ1_Max = Math.Abs(dataTemp.YDeltaRes_YQ1) > Math.Abs(yDeltaRes_Sig_YQ1_Max) ? dataTemp.YDeltaRes_YQ1 : yDeltaRes_Sig_YQ1_Max;
                yDeltaRes_Sig_YQ1_Min = Math.Abs(dataTemp.YDeltaRes_YQ1) < Math.Abs(yDeltaRes_Sig_YQ1_Min) ? dataTemp.YDeltaRes_YQ1 : yDeltaRes_Sig_YQ1_Min;
                yDeltaRes_Sig_YQ1_Sum += dataTemp.YDeltaRes_YQ1;

                yDeltaRes_Sig_YQ2_Max = Math.Abs(dataTemp.YDeltaRes_YQ2) > Math.Abs(yDeltaRes_Sig_YQ2_Max) ? dataTemp.YDeltaRes_YQ2 : yDeltaRes_Sig_YQ2_Max;
                yDeltaRes_Sig_YQ2_Min = Math.Abs(dataTemp.YDeltaRes_YQ2) < Math.Abs(yDeltaRes_Sig_YQ2_Min) ? dataTemp.YDeltaRes_YQ2 : yDeltaRes_Sig_YQ2_Min;
                yDeltaRes_Sig_YQ2_Sum += dataTemp.YDeltaRes_YQ2;

                sigPathRes_XI1_Max = Math.Max(dataTemp.Res_XI1, sigPathRes_XI1_Max);
                sigPathRes_XI1_Min = Math.Min(dataTemp.Res_XI1, sigPathRes_XI1_Min);
                sigPathRes_XI1_Sum += dataTemp.Res_XI1;

                sigPathRes_XI2_Max = Math.Max(dataTemp.Res_XI2, sigPathRes_XI2_Max);
                sigPathRes_XI2_Min = Math.Min(dataTemp.Res_XI2, sigPathRes_XI2_Min);
                sigPathRes_XI2_Sum += dataTemp.Res_XI2;

                sigPathRes_XQ1_Max = Math.Max(dataTemp.Res_XQ1, sigPathRes_XQ1_Max);
                sigPathRes_XQ1_Min = Math.Min(dataTemp.Res_XQ1, sigPathRes_XQ1_Min);
                sigPathRes_XQ1_Sum += dataTemp.Res_XQ1;

                sigPathRes_XQ2_Max = Math.Max(dataTemp.Res_XQ2, sigPathRes_XQ2_Max);
                sigPathRes_XQ2_Min = Math.Min(dataTemp.Res_XQ2, sigPathRes_XQ2_Min);
                sigPathRes_XQ2_Sum += dataTemp.Res_XQ2;

                sigPathRes_YI1_Max = Math.Max(dataTemp.Res_YI1, sigPathRes_YI1_Max);
                sigPathRes_YI1_Min = Math.Min(dataTemp.Res_YI1, sigPathRes_YI1_Min);
                sigPathRes_YI1_Sum += dataTemp.Res_YI1;

                sigPathRes_YI2_Max = Math.Max(dataTemp.Res_YI2, sigPathRes_YI2_Max);
                sigPathRes_YI2_Min = Math.Min(dataTemp.Res_YI2, sigPathRes_YI2_Min);
                sigPathRes_YI2_Sum += dataTemp.Res_YI2;

                sigPathRes_YQ1_Max = Math.Max(dataTemp.Res_YQ1, sigPathRes_YQ1_Max);
                sigPathRes_YQ1_Min = Math.Min(dataTemp.Res_YQ1, sigPathRes_YQ1_Min);
                sigPathRes_YQ1_Sum += dataTemp.Res_YQ1;

                sigPathRes_YQ2_Max = Math.Max(dataTemp.Res_YQ2, sigPathRes_YQ2_Max);
                sigPathRes_YQ2_Min = Math.Min(dataTemp.Res_YQ2, sigPathRes_YQ2_Min);
                sigPathRes_YQ2_Sum += dataTemp.Res_YQ2;

                // Now do the Min Responsivity      /Added 9-Apr-2015 DLS
                // This will return the minimum Average PhotoCurrents for the 8PDs across the test wavelengths
                minPdResp_Sig = Math.Min(minPdResp_Sig, dataTemp.Avg_Res);

                // doubles min pd resp to allow for only 4 pds rather than 8
                //minPdResp_Sig = minPdResp_Sig * 2; *removed by AF 26.09.2017 because now we have corrected the average to be the average of the 4 pds instead of 8
                
                if ( CoRxDCTestInstruments .IsBiasPDTap )
                {
                    sigPathRes_Tap_Max = Math.Max(dataTemp.Res_Tap, sigPathRes_Tap_Max);
                    sigPathRes_Tap_Min = Math.Min(dataTemp.Res_Tap, sigPathRes_Tap_Min);
                    sigPathRes_Tap_Sum += dataTemp.Res_Tap;
                }
                maxRespImbalance_Sig = Math.Max(dataTemp.RespImbalanceRatio, maxRespImbalance_Sig);
                maxRespImbBalance_X_Sig = Math.Max(dataTemp.RespImbalance_X, maxRespImbBalance_X_Sig);
                maxRespImbBalance_Y_Sig = Math.Max(dataTemp.RespImbalance_Y, maxRespImbBalance_Y_Sig);
            }
            
            //add by chaoqun
            locLaneRipple_Max = maxRipple;

            returnData.AddDouble("sigPathCMRR_XI_Max", sigPathCMRR_XI_Max);
            returnData.AddDouble("sigPathCMRR_XI_Min", sigPathCMRR_XI_Min);
            returnData.AddDouble("sigPathCMRR_XI_Avg", sigPathCMRR_XI_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathCMRR_XQ_Max", sigPathCMRR_XQ_Max);
            returnData.AddDouble("sigPathCMRR_XQ_Min", sigPathCMRR_XQ_Min);
            returnData.AddDouble("sigPathCMRR_XQ_Avg", sigPathCMRR_XQ_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathCMRR_YI_Max", sigPathCMRR_YI_Max);
            returnData.AddDouble("sigPathCMRR_YI_Min", sigPathCMRR_YI_Min);
            returnData.AddDouble("sigPathCMRR_YI_Avg", sigPathCMRR_YI_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathCMRR_YQ_Max", sigPathCMRR_YQ_Max);
            returnData.AddDouble("sigPathCMRR_YQ_Min", sigPathCMRR_YQ_Min);
            returnData.AddDouble("sigPathCMRR_YQ_Avg", sigPathCMRR_YQ_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_XI1_Max", sigPathDeltaRes_XI1_Max);
            returnData.AddDouble("sigPathDeltaRes_XI1_Min", sigPathDeltaRes_XI1_Min);
            returnData.AddDouble("sigPathDeltaRes_XI1_Avg", sigPathDeltaRes_XI1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_XI2_Max", sigPathDeltaRes_XI2_Max);
            returnData.AddDouble("sigPathDeltaRes_XI2_Min", sigPathDeltaRes_XI2_Min);
            returnData.AddDouble("sigPathDeltaRes_XI2_Avg", sigPathDeltaRes_XI2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_XQ1_Max", sigPathDeltaRes_XQ1_Max);
            returnData.AddDouble("sigPathDeltaRes_XQ1_Min", sigPathDeltaRes_XQ1_Min);
            returnData.AddDouble("sigPathDeltaRes_XQ1_Avg", sigPathDeltaRes_XQ1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_XQ2_Max", sigPathDeltaRes_XQ2_Max);
            returnData.AddDouble("sigPathDeltaRes_XQ2_Min", sigPathDeltaRes_XQ2_Min);
            returnData.AddDouble("sigPathDeltaRes_XQ2_Avg", sigPathDeltaRes_XQ2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_YI1_Max", sigPathDeltaRes_YI1_Max);
            returnData.AddDouble("sigPathDeltaRes_YI1_Min", sigPathDeltaRes_YI1_Min);
            returnData.AddDouble("sigPathDeltaRes_YI1_Avg", sigPathDeltaRes_YI1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_YI2_Max", sigPathDeltaRes_YI2_Max);
            returnData.AddDouble("sigPathDeltaRes_YI2_Min", sigPathDeltaRes_YI2_Min);
            returnData.AddDouble("sigPathDeltaRes_YI2_Avg", sigPathDeltaRes_YI2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_YQ1_Max", sigPathDeltaRes_YQ1_Max);
            returnData.AddDouble("sigPathDeltaRes_YQ1_Min", sigPathDeltaRes_YQ1_Min);
            returnData.AddDouble("sigPathDeltaRes_YQ1_Avg", sigPathDeltaRes_YQ1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathDeltaRes_YQ2_Max", sigPathDeltaRes_YQ2_Max);
            returnData.AddDouble("sigPathDeltaRes_YQ2_Min", sigPathDeltaRes_YQ2_Min);
            returnData.AddDouble("sigPathDeltaRes_YQ2_Avg", sigPathDeltaRes_YQ2_Sum / sigPathResList.Count);

            returnData.AddDouble("xDeltaRes_Sig_XI1_Max", xDeltaRes_Sig_XI1_Max);
            returnData.AddDouble("xDeltaRes_Sig_XI1_Min", xDeltaRes_Sig_XI1_Min);
            returnData.AddDouble("xDeltaRes_Sig_XI1_Avg", xDeltaRes_Sig_XI1_Sum / sigPathResList.Count);

            returnData.AddDouble("xDeltaRes_Sig_XI2_Max", xDeltaRes_Sig_XI2_Max);
            returnData.AddDouble("xDeltaRes_Sig_XI2_Min", xDeltaRes_Sig_XI2_Min);
            returnData.AddDouble("xDeltaRes_Sig_XI2_Avg", xDeltaRes_Sig_XI2_Sum / sigPathResList.Count);

            returnData.AddDouble("xDeltaRes_Sig_XQ1_Max", xDeltaRes_Sig_XQ1_Max);
            returnData.AddDouble("xDeltaRes_Sig_XQ1_Min", xDeltaRes_Sig_XQ1_Min);
            returnData.AddDouble("xDeltaRes_Sig_XQ1_Avg", xDeltaRes_Sig_XQ1_Sum / sigPathResList.Count);

            returnData.AddDouble("xDeltaRes_Sig_XQ2_Max", xDeltaRes_Sig_XQ2_Max);
            returnData.AddDouble("xDeltaRes_Sig_XQ2_Min", xDeltaRes_Sig_XQ2_Min);
            returnData.AddDouble("xDeltaRes_Sig_XQ2_Avg", xDeltaRes_Sig_XQ2_Sum / sigPathResList.Count);

            returnData.AddDouble("yDeltaRes_Sig_YI1_Max", yDeltaRes_Sig_YI1_Max);
            returnData.AddDouble("yDeltaRes_Sig_YI1_Min", yDeltaRes_Sig_YI1_Min);
            returnData.AddDouble("yDeltaRes_Sig_YI1_Avg", yDeltaRes_Sig_YI1_Sum / sigPathResList.Count);

            returnData.AddDouble("yDeltaRes_Sig_YI2_Max", yDeltaRes_Sig_YI2_Max);
            returnData.AddDouble("yDeltaRes_Sig_YI2_Min", yDeltaRes_Sig_YI2_Min);
            returnData.AddDouble("yDeltaRes_Sig_YI2_Avg", yDeltaRes_Sig_YI2_Sum / sigPathResList.Count);

            returnData.AddDouble("yDeltaRes_Sig_YQ1_Max", yDeltaRes_Sig_YQ1_Max);
            returnData.AddDouble("yDeltaRes_Sig_YQ1_Min", yDeltaRes_Sig_YQ1_Min);
            returnData.AddDouble("yDeltaRes_Sig_YQ1_Avg", yDeltaRes_Sig_YQ1_Sum / sigPathResList.Count);

            returnData.AddDouble("yDeltaRes_Sig_YQ2_Max", yDeltaRes_Sig_YQ2_Max);
            returnData.AddDouble("yDeltaRes_Sig_YQ2_Min", yDeltaRes_Sig_YQ2_Min);
            returnData.AddDouble("yDeltaRes_Sig_YQ2_Avg", yDeltaRes_Sig_YQ2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_XI1_Max", sigPathRes_XI1_Max);
            returnData.AddDouble("sigPathRes_XI1_Min", sigPathRes_XI1_Min);
            returnData.AddDouble("sigPathRes_XI1_Avg", sigPathRes_XI1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_XI2_Max", sigPathRes_XI2_Max);
            returnData.AddDouble("sigPathRes_XI2_Min", sigPathRes_XI2_Min);
            returnData.AddDouble("sigPathRes_XI2_Avg", sigPathRes_XI2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_XQ1_Max", sigPathRes_XQ1_Max);
            returnData.AddDouble("sigPathRes_XQ1_Min", sigPathRes_XQ1_Min);
            returnData.AddDouble("sigPathRes_XQ1_Avg", sigPathRes_XQ1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_XQ2_Max", sigPathRes_XQ2_Max);
            returnData.AddDouble("sigPathRes_XQ2_Min", sigPathRes_XQ2_Min);
            returnData.AddDouble("sigPathRes_XQ2_Avg", sigPathRes_XQ2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_YI1_Max", sigPathRes_YI1_Max);
            returnData.AddDouble("sigPathRes_YI1_Min", sigPathRes_YI1_Min);
            returnData.AddDouble("sigPathRes_YI1_Avg", sigPathRes_YI1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_YI2_Max", sigPathRes_YI2_Max);
            returnData.AddDouble("sigPathRes_YI2_Min", sigPathRes_YI2_Min);
            returnData.AddDouble("sigPathRes_YI2_Avg", sigPathRes_YI2_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_YQ1_Max", sigPathRes_YQ1_Max);
            returnData.AddDouble("sigPathRes_YQ1_Min", sigPathRes_YQ1_Min);
            returnData.AddDouble("sigPathRes_YQ1_Avg", sigPathRes_YQ1_Sum / sigPathResList.Count);

            returnData.AddDouble("sigPathRes_YQ2_Max", sigPathRes_YQ2_Max);
            returnData.AddDouble("sigPathRes_YQ2_Min", sigPathRes_YQ2_Min);
            returnData.AddDouble("sigPathRes_YQ2_Avg", sigPathRes_YQ2_Sum / sigPathResList.Count);

            if (CoRxDCTestInstruments .IsBiasPDTap )
            {
                returnData.AddDouble("SigPathRes_Tap_Max", sigPathRes_Tap_Max);
                returnData.AddDouble("SigPathRes_Tap_Min", sigPathRes_Tap_Min);
                returnData.AddDouble("SigPathRes_Tap_Avg", sigPathRes_Tap_Sum / sigPathResList.Count);
            }
            returnData.AddDouble("maxRespImbalance_Sig", maxRespImbalance_Sig);
            returnData.AddDouble("maxRespImbBalance_X_Sig", maxRespImbBalance_X_Sig);
            returnData.AddDouble("maxRespImbBalance_Y_Sig", maxRespImbBalance_Y_Sig);

            #region Get the worst result among 8 pds

//            double minPdResp_Sig = double.PositiveInfinity;       // Moved 9-Apr-2015 DLS
            double maxDevResp_Sig = 0;
            double xMaxDevResp_Sig = 0;
            double yMaxDevResp_Sig = 0;
     

            maxDevResp_Sig = Math.Abs(sigPathDeltaRes_XI1_Max) > Math.Abs(sigPathDeltaRes_XQ1_Max) ? sigPathDeltaRes_XI1_Max : sigPathDeltaRes_XQ1_Max;
            maxDevResp_Sig = Math.Abs(maxDevResp_Sig) > Math.Abs(sigPathDeltaRes_YI1_Max) ? maxDevResp_Sig : sigPathDeltaRes_YI1_Max;
            maxDevResp_Sig = Math.Abs(maxDevResp_Sig) > Math.Abs(sigPathDeltaRes_YQ1_Max) ? maxDevResp_Sig : sigPathDeltaRes_YQ1_Max;

            xMaxDevResp_Sig = Math.Abs(xDeltaRes_Sig_XI1_Max) > Math.Abs(xDeltaRes_Sig_XQ1_Max) ? xDeltaRes_Sig_XI1_Max : xDeltaRes_Sig_XQ1_Max;
                 
            yMaxDevResp_Sig = Math.Abs(yDeltaRes_Sig_YI1_Max) > Math.Abs(yDeltaRes_Sig_YQ1_Max) ? yDeltaRes_Sig_YI1_Max : yDeltaRes_Sig_YQ1_Max;
     
            //minPdResp_Sig = Math.Min(sigPathRes_XI1_Min, sigPathRes_XI2_Min);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_XQ1_Min);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_XQ2_Min);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YI1_Min);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YI2_Min);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YQ1_Min);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YQ2_Min);

            // Replaced by Min of Average 8PD at each wavelength calculation.

            //minPdResp_Sig = Math.Min(sigPathRes_XI1_Sum / sigPathResList.Count, sigPathRes_XI2_Sum / sigPathResList.Count);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_XQ1_Sum / sigPathResList.Count);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_XQ2_Sum / sigPathResList.Count);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YI1_Sum / sigPathResList.Count);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YI2_Sum / sigPathResList.Count);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YQ1_Sum / sigPathResList.Count);
            //minPdResp_Sig = Math.Min(minPdResp_Sig, sigPathRes_YQ2_Sum / sigPathResList.Count);


            returnData.AddDouble("MaxPdDevResp_Sig", maxDevResp_Sig);
            returnData.AddDouble("XChipMaxPdDevResp_Sig", xMaxDevResp_Sig);
            returnData.AddDouble("YChipMaxPdDevResp_Sig", yMaxDevResp_Sig);
            returnData.AddDouble("MinPdResp_Sig", minPdResp_Sig);
            #endregion

            #endregion

            #region Local oscilator path param
            double locPathRes_XI1_Min = double.PositiveInfinity;
            double locPathRes_XI1_Max = double.NegativeInfinity;
            double locPathRes_XI1_Sum = 0;

            double locPathRes_XQ1_Min = double.PositiveInfinity;
            double locPathRes_XQ1_Max = double.NegativeInfinity;
            double locPathRes_XQ1_Sum = 0;

            double locPathDeltaRes_XI1_Min = double.PositiveInfinity;
            double locPathDeltaRes_XI1_Max = 0;
            double locPathDeltaRes_XI1_Sum = 0;

            double locPathDeltaRes_XI2_Max = 0;
            double locPathDeltaRes_XI2_Min = double.PositiveInfinity;
            double locPathDeltaRes_XI2_Sum = 0;

            double locPathDeltaRes_XQ2_Max = 0;
            double locPathDeltaRes_XQ2_Min = double.PositiveInfinity;
            double locPathDeltaRes_XQ2_Sum = 0;

            double locPathDeltaRes_XQ1_Max = 0;
            double locPathDeltaRes_XQ1_Min = double.PositiveInfinity;
            double locPathDeltaRes_XQ1_Sum = 0;
            
            double xDeltaRes_Loc_XI1_Min = double.PositiveInfinity;
            double xDeltaRes_Loc_XI1_Max = 0;
            double xDeltaRes_Loc_XI1_Sum = 0;

            double xDeltaRes_Loc_XQ1_Max = 0;
            double xDeltaRes_Loc_XQ1_Min = double.PositiveInfinity;
            double xDeltaRes_Loc_XQ1_Sum = 0;

            double locPathCMRR_XI_Max = double .NegativeInfinity;
            double locPathCMRR_XI_Min = double .PositiveInfinity;
            double locPathCMRR_XI_Sum = 0;

            double locPathCMRR_XQ_Max = double .NegativeInfinity;
            double locPathCMRR_XQ_Min = double .PositiveInfinity;
            double locPathCMRR_XQ_Sum = 0;

            double locPathRes_YI1_Min = double.PositiveInfinity;
            double locPathRes_YI1_Max = double.NegativeInfinity;
            double locPathRes_YI1_Sum = 0;

            double locPathRes_YI2_Min = double.PositiveInfinity;
            double locPathRes_YI2_Max = double.NegativeInfinity;
            double locPathRes_YI2_Sum = 0;

            double locPathRes_YQ2_Min = double.PositiveInfinity;
            double locPathRes_YQ2_Max = double.NegativeInfinity;
            double locPathRes_YQ2_Sum = 0;

            double locPathRes_YQ1_Min = double.PositiveInfinity;
            double locPathRes_YQ1_Max = double.NegativeInfinity;
            double locPathRes_YQ1_Sum = 0;

            double locPathDeltaRes_YI1_Min = double.PositiveInfinity;
            double locPathDeltaRes_YI1_Max = 0;
            double locPathDeltaRes_YI1_Sum = 0;

            double locPathDeltaRes_YI2_Max = 0;
            double locPathDeltaRes_YI2_Min = double.PositiveInfinity;
            double locPathDeltaRes_YI2_Sum = 0;

            double locPathDeltaRes_YQ2_Max = 0;
            double locPathDeltaRes_YQ2_Min = double.PositiveInfinity;
            double locPathDeltaRes_YQ2_Sum = 0;

            double locPathDeltaRes_YQ1_Max = 0;
            double locPathDeltaRes_YQ1_Min = double.PositiveInfinity;
            double locPathDeltaRes_YQ1_Sum = 0;

            double yDeltaRes_Loc_YI1_Min = double.PositiveInfinity;
            double yDeltaRes_Loc_YI1_Max = 0;
            double yDeltaRes_Loc_YI1_Sum = 0;

            double yDeltaRes_Loc_YQ1_Max = 0;
            double yDeltaRes_Loc_YQ1_Min = double.PositiveInfinity;
            double yDeltaRes_Loc_YQ1_Sum = 0;

            double locPathCMRR_YI_Max = double .NegativeInfinity;
            double locPathCMRR_YI_Min = double .PositiveInfinity;
            double locPathCMRR_YI_Sum = 0;

            double locPathCMRR_YQ_Max = double .NegativeInfinity;
            double locPathCMRR_YQ_Min = double .PositiveInfinity;
            double locPathCMRR_YQ_Sum = 0;

            double locPathRes_Tap_Min = double.PositiveInfinity;
            double locPathRes_Tap_Max = double.NegativeInfinity;
            double locPathRes_Tap_Sum = 0;

            double maxRespImbalance_Loc = 0;
            double maxRespImbBalance_X_Loc = 0;
            double maxRespImbBalance_Y_Loc = 0;

            double minPdResp_Loc = double.PositiveInfinity;                             // Added 9-Apr-2015

            //debug
            //System.IO.StreamWriter file = new System.IO.StreamWriter("c:\\debug.csv");
            //file.WriteLine("Res_XI1,Res_XI2,Res_XQ1,Res_XQ2,Res_YI1,Res_YI2,Res_YQ1,Res_YQ2,Avg_Res,DeltaRes_XI1"); //header



            foreach (CoRxPdRespData dataTemp in locPathResList)
            {
              
//file.WriteLine(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", dataTemp.Res_XI1, dataTemp.Res_XI2, dataTemp.Res_XQ1, dataTemp.Res_XQ2, dataTemp.Res_YI1, dataTemp.Res_YI2, dataTemp.Res_YQ1, dataTemp.Res_YQ2, dataTemp.Avg_Res, dataTemp.DeltaRes_XI1));


                locPathCMRR_XI_Max = Math.Max(dataTemp.CMRR_XI, locPathCMRR_XI_Max);
                locPathCMRR_XI_Min = Math.Min(dataTemp.CMRR_XI, locPathCMRR_XI_Min);
                locPathCMRR_XI_Sum += dataTemp.CMRR_XI;

                locPathCMRR_XQ_Max = Math.Max(dataTemp.CMRR_XQ, locPathCMRR_XQ_Max);
                locPathCMRR_XQ_Min = Math.Min(dataTemp.CMRR_XQ, locPathCMRR_XQ_Min);
                locPathCMRR_XQ_Sum += dataTemp.CMRR_XQ;

                locPathCMRR_YI_Max = Math.Max(dataTemp.CMRR_YI, locPathCMRR_YI_Max);
                locPathCMRR_YI_Min = Math.Min(dataTemp.CMRR_YI, locPathCMRR_YI_Min);
                locPathCMRR_YI_Sum += dataTemp.CMRR_YI;

                locPathCMRR_YQ_Max = Math.Max(dataTemp.CMRR_YQ, locPathCMRR_YQ_Max);
                locPathCMRR_YQ_Min = Math.Min(dataTemp.CMRR_YQ, locPathCMRR_YQ_Min);
                locPathCMRR_YQ_Sum += dataTemp.CMRR_YQ;

                locPathDeltaRes_XI1_Max = Math.Abs(dataTemp.DeltaRes_XI1) > Math.Abs(locPathDeltaRes_XI1_Max) ? dataTemp.DeltaRes_XI1 : locPathDeltaRes_XI1_Max;
                locPathDeltaRes_XI1_Min = Math.Abs(dataTemp.DeltaRes_XI1) < Math.Abs ( locPathDeltaRes_XI1_Min)? dataTemp.DeltaRes_XI1: locPathDeltaRes_XI1_Min;
                locPathDeltaRes_XI1_Sum += dataTemp.DeltaRes_XI1;

                locPathDeltaRes_XQ1_Max = Math.Abs(dataTemp.DeltaRes_XQ1) > Math.Abs(locPathDeltaRes_XQ1_Max)? dataTemp.DeltaRes_XQ1 : locPathDeltaRes_XQ1_Max;
                locPathDeltaRes_XQ1_Min = Math.Abs(dataTemp.DeltaRes_XQ1) < Math.Abs(locPathDeltaRes_XQ1_Min) ? dataTemp.DeltaRes_XQ1 : locPathDeltaRes_XQ1_Min;
                locPathDeltaRes_XQ1_Sum += dataTemp.DeltaRes_XQ1;

                locPathDeltaRes_YI1_Max = Math.Abs(dataTemp.DeltaRes_YI1) > Math.Abs(locPathDeltaRes_YI1_Max) ? dataTemp.DeltaRes_YI1 : locPathDeltaRes_YI1_Max;
                locPathDeltaRes_YI1_Min = Math.Abs(dataTemp.DeltaRes_YI1) < Math.Abs(locPathDeltaRes_YI1_Min) ? dataTemp.DeltaRes_YI1 : locPathDeltaRes_YI1_Min;
                locPathDeltaRes_YI1_Sum += dataTemp.DeltaRes_YI1;

                locPathDeltaRes_YQ1_Max = Math.Abs(dataTemp.DeltaRes_YQ1) > Math.Abs(locPathDeltaRes_YQ1_Max) ? dataTemp.DeltaRes_YQ1 : locPathDeltaRes_YQ1_Max;
                locPathDeltaRes_YQ1_Min = Math.Abs(dataTemp.DeltaRes_YQ1) < Math.Abs(locPathDeltaRes_YQ1_Min) ? dataTemp.DeltaRes_YQ1 : locPathDeltaRes_YQ1_Min;
                locPathDeltaRes_YQ1_Sum += dataTemp.DeltaRes_YQ1;

                locPathDeltaRes_YQ2_Max = Math.Abs(dataTemp.DeltaRes_YQ2) > Math.Abs(locPathDeltaRes_YQ2_Max) ? dataTemp.DeltaRes_YQ2 : locPathDeltaRes_YQ2_Max;
                locPathDeltaRes_YQ2_Min = Math.Abs(dataTemp.DeltaRes_YQ2) < Math.Abs(locPathDeltaRes_YQ2_Min) ? dataTemp.DeltaRes_YQ2 : locPathDeltaRes_YQ2_Min;
                locPathDeltaRes_YQ2_Sum += dataTemp.DeltaRes_YQ2;

                xDeltaRes_Loc_XI1_Max = Math.Abs(dataTemp.XDeltaRes_XI1) > Math.Abs(xDeltaRes_Loc_XI1_Max) ? dataTemp.XDeltaRes_XI1 : xDeltaRes_Loc_XI1_Max;
                xDeltaRes_Loc_XI1_Min = Math.Abs(dataTemp.XDeltaRes_XI1) < Math.Abs(xDeltaRes_Loc_XI1_Min) ? dataTemp.XDeltaRes_XI1 : xDeltaRes_Loc_XI1_Min;
                xDeltaRes_Loc_XI1_Sum += dataTemp.XDeltaRes_XI1;

                xDeltaRes_Loc_XQ1_Max = Math.Abs(dataTemp.XDeltaRes_XQ1) > Math.Abs(xDeltaRes_Loc_XQ1_Max) ? dataTemp.XDeltaRes_XQ1 : xDeltaRes_Loc_XQ1_Max;
                xDeltaRes_Loc_XQ1_Min = Math.Abs(dataTemp.XDeltaRes_XQ1) < Math.Abs(xDeltaRes_Loc_XQ1_Min) ? dataTemp.XDeltaRes_XQ1 : xDeltaRes_Loc_XQ1_Min;
                xDeltaRes_Loc_XQ1_Sum += dataTemp.XDeltaRes_XQ1;

                yDeltaRes_Loc_YI1_Max = Math.Abs(dataTemp.YDeltaRes_YI1) > Math.Abs(yDeltaRes_Loc_YI1_Max) ? dataTemp.YDeltaRes_YI1 : yDeltaRes_Loc_YI1_Max;
                yDeltaRes_Loc_YI1_Min = Math.Abs(dataTemp.YDeltaRes_YI1) < Math.Abs(yDeltaRes_Loc_YI1_Min) ? dataTemp.YDeltaRes_YI1 : yDeltaRes_Loc_YI1_Min;
                yDeltaRes_Loc_YI1_Sum += dataTemp.YDeltaRes_YI1;

                yDeltaRes_Loc_YQ1_Max = Math.Abs(dataTemp.YDeltaRes_YQ1) > Math.Abs(yDeltaRes_Loc_YQ1_Max) ? dataTemp.YDeltaRes_YQ1 : yDeltaRes_Loc_YQ1_Max;
                yDeltaRes_Loc_YQ1_Min = Math.Abs(dataTemp.YDeltaRes_YQ1) < Math.Abs(yDeltaRes_Loc_YQ1_Min) ? dataTemp.YDeltaRes_YQ1 : yDeltaRes_Loc_YQ1_Min;
                yDeltaRes_Loc_YQ1_Sum += dataTemp.YDeltaRes_YQ1;

                locPathRes_XI1_Max = Math.Max(dataTemp.Res_XI1, locPathRes_XI1_Max);
                locPathRes_XI1_Min = Math.Min(dataTemp.Res_XI1, locPathRes_XI1_Min);
                locPathRes_XI1_Sum += dataTemp.Res_XI1;

                locPathRes_XQ1_Max = Math.Max(dataTemp.Res_XQ1, locPathRes_XQ1_Max);
                locPathRes_XQ1_Min = Math.Min(dataTemp.Res_XQ1, locPathRes_XQ1_Min);
                locPathRes_XQ1_Sum += dataTemp.Res_XQ1;

                locPathRes_YI1_Max = Math.Max(dataTemp.Res_YI1, locPathRes_YI1_Max);
                locPathRes_YI1_Min = Math.Min(dataTemp.Res_YI1, locPathRes_YI1_Min);
                locPathRes_YI1_Sum += dataTemp.Res_YI1;

                locPathRes_YQ1_Max = Math.Max(dataTemp.Res_YQ1, locPathRes_YQ1_Max);
                locPathRes_YQ1_Min = Math.Min(dataTemp.Res_YQ1, locPathRes_YQ1_Min);
                locPathRes_YQ1_Sum += dataTemp.Res_YQ1;

                // Now do the Min Responsivity      /Added 9-Apr-2015 DLS
                // This will return the minimum Average PhotoCurrents for the 8PDs across the test wavelengths
                minPdResp_Loc = Math.Min(minPdResp_Loc, dataTemp.Avg_Res);
                // double minpdresp to allow for only using 4 pds instead of 8
                //minPdResp_Loc = minPdResp_Loc * 2; *removed by AF 26.09.2017 because now we have corrected the average to be the average of the 4 pds instead of 8
                
                if (CoRxDCTestInstruments.IsBiasPDTap)
                {
                    locPathRes_Tap_Max = Math.Max(dataTemp.Res_Tap, locPathRes_Tap_Max);
                    locPathRes_Tap_Min = Math.Min(dataTemp.Res_Tap, locPathRes_Tap_Min);
                    locPathRes_Tap_Sum += dataTemp.Res_Tap;
                }
                maxRespImbalance_Loc = Math.Max(dataTemp.RespImbalanceRatio, maxRespImbalance_Loc);
                maxRespImbBalance_X_Loc = Math.Max(dataTemp.RespImbalance_X, maxRespImbBalance_X_Loc);
                maxRespImbBalance_Y_Loc = Math.Max(dataTemp.RespImbalance_Y, maxRespImbBalance_Y_Loc);
            }


            //file.Close();//debbug

            returnData.AddDouble("locPathCMRR_XI_Max", locPathCMRR_XI_Max);
            returnData.AddDouble("locPathCMRR_XI_Min", locPathCMRR_XI_Min);
            returnData.AddDouble("locPathCMRR_XI_Avg", locPathCMRR_XI_Sum / locPathResList.Count);

            returnData.AddDouble("locPathCMRR_XQ_Max", locPathCMRR_XQ_Max);
            returnData.AddDouble("locPathCMRR_XQ_Min", locPathCMRR_XQ_Min);
            returnData.AddDouble("locPathCMRR_XQ_Avg", locPathCMRR_XQ_Sum / locPathResList.Count);

            returnData.AddDouble("locPathCMRR_YI_Max", locPathCMRR_YI_Max);
            returnData.AddDouble("locPathCMRR_YI_Min", locPathCMRR_YI_Min);
            returnData.AddDouble("locPathCMRR_YI_Avg", locPathCMRR_YI_Sum / locPathResList.Count);

            returnData.AddDouble("locPathCMRR_YQ_Max", locPathCMRR_YQ_Max);
            returnData.AddDouble("locPathCMRR_YQ_Min", locPathCMRR_YQ_Min);
            returnData.AddDouble("locPathCMRR_YQ_Avg", locPathCMRR_YQ_Sum / locPathResList.Count);

            returnData.AddDouble("locPathDeltaRes_XI1_Max", locPathDeltaRes_XI1_Max);
            returnData.AddDouble("locPathDeltaRes_XI1_Min", locPathDeltaRes_XI1_Min);
            returnData.AddDouble("locPathDeltaRes_XI1_Avg", locPathDeltaRes_XI1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathDeltaRes_XQ1_Max", locPathDeltaRes_XQ1_Max);
            returnData.AddDouble("locPathDeltaRes_XQ1_Min", locPathDeltaRes_XQ1_Min);
            returnData.AddDouble("locPathDeltaRes_XQ1_Avg", locPathDeltaRes_XQ1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathDeltaRes_YI1_Max", locPathDeltaRes_YI1_Max);
            returnData.AddDouble("locPathDeltaRes_YI1_Min", locPathDeltaRes_YI1_Min);
            returnData.AddDouble("locPathDeltaRes_YI1_Avg", locPathDeltaRes_YI1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathDeltaRes_YQ1_Max", locPathDeltaRes_YQ1_Max);
            returnData.AddDouble("locPathDeltaRes_YQ1_Min", locPathDeltaRes_YQ1_Min);
            returnData.AddDouble("locPathDeltaRes_YQ1_Avg", locPathDeltaRes_YQ1_Sum / locPathResList.Count);

            returnData.AddDouble("xDeltaRes_Loc_XI1_Max", xDeltaRes_Loc_XI1_Max);
            returnData.AddDouble("xDeltaRes_Loc_XI1_Min", xDeltaRes_Loc_XI1_Min);
            returnData.AddDouble("xDeltaRes_Loc_XI1_Avg", xDeltaRes_Loc_XI1_Sum / locPathResList.Count);

            returnData.AddDouble("xDeltaRes_Loc_XQ1_Max", xDeltaRes_Loc_XQ1_Max);
            returnData.AddDouble("xDeltaRes_Loc_XQ1_Min", xDeltaRes_Loc_XQ1_Min);
            returnData.AddDouble("xDeltaRes_Loc_XQ1_Avg", xDeltaRes_Loc_XQ1_Sum / locPathResList.Count);

            returnData.AddDouble("yDeltaRes_Loc_YI1_Max", yDeltaRes_Loc_YI1_Max);
            returnData.AddDouble("yDeltaRes_Loc_YI1_Min", yDeltaRes_Loc_YI1_Min);
            returnData.AddDouble("yDeltaRes_Loc_YI1_Avg", yDeltaRes_Loc_YI1_Sum / locPathResList.Count);

            returnData.AddDouble("yDeltaRes_Loc_YQ1_Max", yDeltaRes_Loc_YQ1_Max);
            returnData.AddDouble("yDeltaRes_Loc_YQ1_Min", yDeltaRes_Loc_YQ1_Min);
            returnData.AddDouble("yDeltaRes_Loc_YQ1_Avg", yDeltaRes_Loc_YQ1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathRes_XI1_Max", locPathRes_XI1_Max);
            returnData.AddDouble("locPathRes_XI1_Min", locPathRes_XI1_Min);
            returnData.AddDouble("locPathRes_XI1_Avg", locPathRes_XI1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathRes_XQ1_Max", locPathRes_XQ1_Max);
            returnData.AddDouble("locPathRes_XQ1_Min", locPathRes_XQ1_Min);
            returnData.AddDouble("locPathRes_XQ1_Avg", locPathRes_XQ1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathRes_YI1_Max", locPathRes_YI1_Max);
            returnData.AddDouble("locPathRes_YI1_Min", locPathRes_YI1_Min);
            returnData.AddDouble("locPathRes_YI1_Avg", locPathRes_YI1_Sum / locPathResList.Count);

            returnData.AddDouble("locPathRes_YQ1_Max", locPathRes_YQ1_Max);
            returnData.AddDouble("locPathRes_YQ1_Min", locPathRes_YQ1_Min);
            returnData.AddDouble("locPathRes_YQ1_Avg", locPathRes_YQ1_Sum / locPathResList.Count);
            //add chaoqun
            returnData.AddDouble("locLaneRipple_Max", locLaneRipple_Max);
           
            if (CoRxDCTestInstruments.IsBiasPDTap)
            {
                returnData.AddDouble("LocPathRes_Tap_Max", locPathRes_Tap_Max);
                returnData.AddDouble("LocPathRes_Tap_Min", locPathRes_Tap_Min);
                returnData.AddDouble("LocPathRes_Tap_Avg", locPathRes_Tap_Sum / sigPathResList.Count);
            }
            returnData.AddDouble("maxRespImbalance_Loc", maxRespImbalance_Loc);
            returnData.AddDouble("maxRespImbBalance_X_Loc", maxRespImbBalance_X_Loc);
            returnData.AddDouble("maxRespImbBalance_Y_Loc", maxRespImbBalance_Y_Loc);

            #region Get the worst result among 8 pds

            // double minPdResp_Loc = double.PositiveInfinity;   // Remove 9-Apr-2015 DLS
            double maxDevResp_Loc = 0;
            double xMaxDevResp_Loc = 0;
            double yMaxDevResp_Loc = 0;
            double maxCMRR_Loc = double .NegativeInfinity;

            maxDevResp_Loc = Math.Abs(locPathDeltaRes_XI1_Max) > Math.Abs(locPathDeltaRes_XQ1_Max) ? locPathDeltaRes_XI1_Max : locPathDeltaRes_XQ1_Max;
            maxDevResp_Loc = Math.Abs(maxDevResp_Loc) > Math.Abs(locPathDeltaRes_YI1_Max) ? maxDevResp_Loc : locPathDeltaRes_YI1_Max;
            maxDevResp_Loc = Math.Abs(maxDevResp_Loc) > Math.Abs(locPathDeltaRes_YQ1_Max) ? maxDevResp_Loc : locPathDeltaRes_YQ1_Max;
            

            xMaxDevResp_Loc = Math.Abs(xDeltaRes_Loc_XI1_Max) > Math.Abs(xDeltaRes_Loc_XQ1_Max) ? xDeltaRes_Loc_XI1_Max : xDeltaRes_Loc_XQ1_Max;

            
            yMaxDevResp_Loc = Math.Abs(yDeltaRes_Loc_YI1_Max) > Math.Abs(yDeltaRes_Loc_YQ1_Max) ? yDeltaRes_Loc_YI1_Max : yDeltaRes_Loc_YQ1_Max;
 
            returnData.AddDouble("MaxPdCMRR_Loc", maxCMRR_Loc);
            returnData.AddDouble("MaxPdDevResp_Loc", maxDevResp_Loc);
            returnData.AddDouble("XChipMaxPdDevResp_Loc", xMaxDevResp_Loc);
            returnData.AddDouble("YChipMaxPdDevResp_Loc", yMaxDevResp_Loc);
            returnData.AddDouble("MinPdResp_Loc", minPdResp_Loc);
            #endregion
            #endregion

            

            #region Write data to file

            List<string> dataStatList = new List<string> ();
            dataStatList .Add ( "");

            string  strData= "";

            strData = string .Format("{0},{1},{2},{3},", "RESP_XIP_SIG", 
                sigPathRes_XI1_Sum/ sigPathResList.Count, sigPathRes_XI1_Min ,sigPathRes_XI1_Max);            
            dataStatList .Add (strData );
                       
            strData = string .Format("{0},{1},{2},{3},", "RESP_XQP_SIG", 
                sigPathRes_XQ1_Sum/sigPathResList .Count , sigPathRes_XQ1_Min , sigPathRes_XQ1_Max);
            dataStatList .Add (strData );

            strData = string .Format ("{0},{1},{2},{3},", "DEV_XIP_SIG",
                sigPathDeltaRes_XI1_Sum/ sigPathResList .Count ,sigPathDeltaRes_XI1_Min ,sigPathDeltaRes_XI1_Max);
            dataStatList .Add (strData );
            
            strData = string .Format ("{0},{1},{2},{3},", "DEV_XIN_SIG", 
                sigPathDeltaRes_XI2_Sum/sigPathResList .Count, sigPathDeltaRes_XI2_Min,sigPathDeltaRes_XI2_Max);
            dataStatList .Add (strData ); 
            
            strData = string .Format ("{0},{1},{2},{3},", "DEV_XQN_SIG", 
                sigPathDeltaRes_XQ2_Sum/sigPathResList .Count, sigPathDeltaRes_XQ2_Min,sigPathDeltaRes_XQ2_Max);
            dataStatList .Add (strData ); 
            
            strData = string .Format ("{0},{1},{2},{3},", "DEV_XQP_SIG", 
                sigPathDeltaRes_XQ1_Sum/sigPathResList .Count, sigPathDeltaRes_XQ1_Min,sigPathDeltaRes_XQ1_Max);
            dataStatList .Add (strData );  
            
            strData = string .Format ("{0},{1},{2},{3},", "CMRR_XI_SIG", 
                sigPathCMRR_XI_Sum/sigPathResList .Count, sigPathCMRR_XI_Min,sigPathCMRR_XI_Max);
            dataStatList .Add (strData ); 
            
            strData = string .Format ("{0},{1},{2},{3},", "CMRR_XQ_SIG", 
                sigPathCMRR_XQ_Sum/sigPathResList .Count, sigPathCMRR_XQ_Min,sigPathCMRR_XQ_Max);
            dataStatList .Add (strData );  
            
            strData = string .Format("{0},{1},{2},{3},", "RESP_YIP_SIG", 
                sigPathRes_YI1_Sum/ sigPathResList.Count, sigPathRes_YI1_Min ,sigPathRes_YI1_Max);            
            dataStatList .Add (strData );
            
            strData =string .Format("{0},{1},{2},{3},", "RESP_YIN_SIG",
                sigPathRes_YI2_Sum/ sigPathResList.Count ,sigPathRes_YI2_Min ,sigPathRes_YI2_Max);
            dataStatList .Add(strData );

            strData =string .Format("{0},{1},{2},{3},", "RESP_YQN_SIG", 
                sigPathRes_YQ2_Sum/sigPathResList .Count ,sigPathRes_YQ2_Min,sigPathRes_YQ2_Max );
            dataStatList .Add (strData );
            
            strData = string .Format("{0},{1},{2},{3},", "RESP_YQP_SIG", 
                sigPathRes_YQ1_Sum/sigPathResList .Count , sigPathRes_YQ1_Min , sigPathRes_YQ1_Max);
            dataStatList .Add (strData );

            strData = string .Format ("{0},{1},{2},{3},", "DEV_YIP_SIG",
                sigPathDeltaRes_YI1_Sum/ sigPathResList .Count ,sigPathDeltaRes_YI1_Min ,sigPathDeltaRes_YI1_Max);
            dataStatList .Add (strData );
            
            strData = string .Format ("{0},{1},{2},{3},", "DEV_YIN_SIG", 
                sigPathDeltaRes_YI2_Sum/sigPathResList .Count, sigPathDeltaRes_YI2_Min,sigPathDeltaRes_YI2_Max);
            dataStatList .Add (strData ); 
            
            strData = string .Format ("{0},{1},{2},{3},", "DEV_YQN_SIG", 
                sigPathDeltaRes_YQ2_Sum/sigPathResList .Count, sigPathDeltaRes_YQ2_Min,sigPathDeltaRes_YQ2_Max);
            dataStatList .Add (strData ); 
            
            strData = string .Format ("{0},{1},{2},{3},", "DEV_YQP_SIG", 
                sigPathDeltaRes_YQ1_Sum/sigPathResList .Count, sigPathDeltaRes_YQ1_Min,sigPathDeltaRes_YQ1_Max);
            dataStatList .Add (strData );  
            
            strData = string .Format ("{0},{1},{2},{3},", "CMRR_YI_SIG", 
                sigPathCMRR_YI_Sum/sigPathResList .Count, sigPathCMRR_YI_Min,sigPathCMRR_YI_Max);
            dataStatList .Add (strData ); 
            
            strData = string .Format ("{0},{1},{2},{3},", "CMRR_YQ_SIG", 
                sigPathCMRR_YQ_Sum/sigPathResList .Count, sigPathCMRR_YQ_Min,sigPathCMRR_YQ_Max);
            dataStatList .Add (strData );

            strData = string.Format("{0},{1},{2},{3},", "RESP_XIP_LOC",
                locPathRes_XI1_Sum / locPathResList.Count, locPathRes_XI1_Min, locPathRes_XI1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "RESP_XQP_LOC",
                locPathRes_XQ1_Sum / locPathResList.Count, locPathRes_XQ1_Min, locPathRes_XQ1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_XIP_LOC",
                locPathDeltaRes_XI1_Sum / locPathResList.Count, locPathDeltaRes_XI1_Min, locPathDeltaRes_XI1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_XIN_LOC",
                locPathDeltaRes_XI2_Sum / locPathResList.Count, locPathDeltaRes_XI2_Min, locPathDeltaRes_XI2_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_XQN_LOC",
                locPathDeltaRes_XQ2_Sum / locPathResList.Count, locPathDeltaRes_XQ2_Min, locPathDeltaRes_XQ2_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_XQP_LOC",
                locPathDeltaRes_XQ1_Sum / locPathResList.Count, locPathDeltaRes_XQ1_Min, locPathDeltaRes_XQ1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "RESP_YIP_LOC",
                locPathRes_YI1_Sum / locPathResList.Count, locPathRes_YI1_Min, locPathRes_YI1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "RESP_YIN_LOC",
                locPathRes_YI2_Sum / locPathResList.Count, locPathRes_YI2_Min, locPathRes_YI2_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "RESP_YQN_LOC",
                locPathRes_YQ2_Sum / locPathResList.Count, locPathRes_YQ2_Min, locPathRes_YQ2_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "RESP_YQP_LOC",
                locPathRes_YQ1_Sum / locPathResList.Count, locPathRes_YQ1_Min, locPathRes_YQ1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_YIP_LOC",
                locPathDeltaRes_YI1_Sum / locPathResList.Count, locPathDeltaRes_YI1_Min, locPathDeltaRes_YI1_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_YIN_LOC",
                locPathDeltaRes_YI2_Sum / locPathResList.Count, locPathDeltaRes_YI2_Min, locPathDeltaRes_YI2_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_YQN_LOC",
                locPathDeltaRes_YQ2_Sum / locPathResList.Count, locPathDeltaRes_YQ2_Min, locPathDeltaRes_YQ2_Max);
            dataStatList.Add(strData);

            strData = string.Format("{0},{1},{2},{3},", "DEV_YQP_LOC",
                locPathDeltaRes_YQ1_Sum / locPathResList.Count, locPathDeltaRes_YQ1_Min, locPathDeltaRes_YQ1_Max);
            dataStatList.Add(strData);

            if ( CoRxDCTestInstruments .IsBiasPDTap )
            {

                strData = string.Format("{0},{1},{2},{3},", "RES_TAP_SIG", sigPathRes_Tap_Sum / sigPathResList.Count,
                    sigPathRes_Tap_Min, sigPathRes_Tap_Max);
                dataStatList.Add(strData);
            }
            dataStatList.Sort();
            dataStatList.Insert(0, "Parameter,Average,Minimun,Maximun");

            using (CsvWriter writer = new CsvWriter())
            {
                try
                {
                    string filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix4RespStat, "", "csv");
                    writer.WriteFile(filename, dataStatList);
                    returnData.AddFileLink("RespStatFile", filename);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion

            #endregion
            return returnData;

        }

        /// <summary>
        /// User Gui
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(TM_CoRxResponsivityGui)); }
        }

        #endregion
    }
}
