namespace Bookham.TestLibrary.Instruments
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxChDisplay4 = new System.Windows.Forms.CheckBox();
            this.btnStartAcquisition = new System.Windows.Forms.Button();
            this.cbxChDisplay3 = new System.Windows.Forms.CheckBox();
            this.cbxChDisplay2 = new System.Windows.Forms.CheckBox();
            this.cbxChDisplay1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnGetData = new System.Windows.Forms.Button();
            this.cbxSaveDataFile = new System.Windows.Forms.CheckBox();
            this.cbxChDataEnable4 = new System.Windows.Forms.CheckBox();
            this.cbxChDataEnable3 = new System.Windows.Forms.CheckBox();
            this.cbxChDataEnable2 = new System.Windows.Forms.CheckBox();
            this.cbxChDataEnable1 = new System.Windows.Forms.CheckBox();
            this.btnSetup = new System.Windows.Forms.Button();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxChDisplay4);
            this.groupBox1.Controls.Add(this.btnStartAcquisition);
            this.groupBox1.Controls.Add(this.cbxChDisplay3);
            this.groupBox1.Controls.Add(this.cbxChDisplay2);
            this.groupBox1.Controls.Add(this.cbxChDisplay1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(163, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Display Channel";
            // 
            // cbxChDisplay4
            // 
            this.cbxChDisplay4.AutoSize = true;
            this.cbxChDisplay4.Location = new System.Drawing.Point(303, 25);
            this.cbxChDisplay4.Name = "cbxChDisplay4";
            this.cbxChDisplay4.Size = new System.Drawing.Size(65, 24);
            this.cbxChDisplay4.TabIndex = 5;
            this.cbxChDisplay4.Text = "Ch 4";
            this.cbxChDisplay4.UseVisualStyleBackColor = true;
            this.cbxChDisplay4.CheckedChanged += new System.EventHandler(this.cbxChDisplay4_CheckedChanged);
            // 
            // btnStartAcquisition
            // 
            this.btnStartAcquisition.Location = new System.Drawing.Point(406, 17);
            this.btnStartAcquisition.Name = "btnStartAcquisition";
            this.btnStartAcquisition.Size = new System.Drawing.Size(105, 39);
            this.btnStartAcquisition.TabIndex = 4;
            this.btnStartAcquisition.Text = "Start Acquisition";
            this.btnStartAcquisition.UseVisualStyleBackColor = true;
            this.btnStartAcquisition.Click += new System.EventHandler(this.btnStartAcquisition_Click);
            // 
            // cbxChDisplay3
            // 
            this.cbxChDisplay3.AutoSize = true;
            this.cbxChDisplay3.Location = new System.Drawing.Point(204, 25);
            this.cbxChDisplay3.Name = "cbxChDisplay3";
            this.cbxChDisplay3.Size = new System.Drawing.Size(65, 24);
            this.cbxChDisplay3.TabIndex = 2;
            this.cbxChDisplay3.Text = "Ch 3";
            this.cbxChDisplay3.UseVisualStyleBackColor = true;
            this.cbxChDisplay3.CheckedChanged += new System.EventHandler(this.cbxChDisplay3_CheckedChanged);
            // 
            // cbxChDisplay2
            // 
            this.cbxChDisplay2.AutoSize = true;
            this.cbxChDisplay2.Location = new System.Drawing.Point(105, 25);
            this.cbxChDisplay2.Name = "cbxChDisplay2";
            this.cbxChDisplay2.Size = new System.Drawing.Size(65, 24);
            this.cbxChDisplay2.TabIndex = 1;
            this.cbxChDisplay2.Text = "Ch 2";
            this.cbxChDisplay2.UseVisualStyleBackColor = true;
            this.cbxChDisplay2.CheckedChanged += new System.EventHandler(this.cbxChDisplay2_CheckedChanged);
            // 
            // cbxChDisplay1
            // 
            this.cbxChDisplay1.AutoSize = true;
            this.cbxChDisplay1.Location = new System.Drawing.Point(6, 25);
            this.cbxChDisplay1.Name = "cbxChDisplay1";
            this.cbxChDisplay1.Size = new System.Drawing.Size(65, 24);
            this.cbxChDisplay1.TabIndex = 0;
            this.cbxChDisplay1.Text = "Ch 1";
            this.cbxChDisplay1.UseVisualStyleBackColor = true;
            this.cbxChDisplay1.CheckedChanged += new System.EventHandler(this.cbxChDisplay1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnGetData);
            this.groupBox2.Controls.Add(this.cbxSaveDataFile);
            this.groupBox2.Controls.Add(this.cbxChDataEnable4);
            this.groupBox2.Controls.Add(this.cbxChDataEnable3);
            this.groupBox2.Controls.Add(this.cbxChDataEnable2);
            this.groupBox2.Controls.Add(this.cbxChDataEnable1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 117);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(148, 330);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Get Data";
            // 
            // btnGetData
            // 
            this.btnGetData.Location = new System.Drawing.Point(19, 275);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(105, 39);
            this.btnGetData.TabIndex = 5;
            this.btnGetData.Text = "Get Data";
            this.btnGetData.UseVisualStyleBackColor = true;
            this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
            // 
            // cbxSaveDataFile
            // 
            this.cbxSaveDataFile.AutoSize = true;
            this.cbxSaveDataFile.Location = new System.Drawing.Point(19, 216);
            this.cbxSaveDataFile.Name = "cbxSaveDataFile";
            this.cbxSaveDataFile.Size = new System.Drawing.Size(123, 24);
            this.cbxSaveDataFile.TabIndex = 4;
            this.cbxSaveDataFile.Text = "Save to File";
            this.cbxSaveDataFile.UseVisualStyleBackColor = true;
            this.cbxSaveDataFile.CheckedChanged += new System.EventHandler(this.cbxSaveDataFile_CheckedChanged);
            // 
            // cbxChDataEnable4
            // 
            this.cbxChDataEnable4.AutoSize = true;
            this.cbxChDataEnable4.Location = new System.Drawing.Point(19, 171);
            this.cbxChDataEnable4.Name = "cbxChDataEnable4";
            this.cbxChDataEnable4.Size = new System.Drawing.Size(65, 24);
            this.cbxChDataEnable4.TabIndex = 3;
            this.cbxChDataEnable4.Text = "Ch 4";
            this.cbxChDataEnable4.UseVisualStyleBackColor = true;
            this.cbxChDataEnable4.CheckedChanged += new System.EventHandler(this.cbxChDataEnable4_CheckedChanged);
            // 
            // cbxChDataEnable3
            // 
            this.cbxChDataEnable3.AutoSize = true;
            this.cbxChDataEnable3.Location = new System.Drawing.Point(19, 126);
            this.cbxChDataEnable3.Name = "cbxChDataEnable3";
            this.cbxChDataEnable3.Size = new System.Drawing.Size(65, 24);
            this.cbxChDataEnable3.TabIndex = 2;
            this.cbxChDataEnable3.Text = "Ch 3";
            this.cbxChDataEnable3.UseVisualStyleBackColor = true;
            this.cbxChDataEnable3.CheckedChanged += new System.EventHandler(this.cbxChDataEnable3_CheckedChanged);
            // 
            // cbxChDataEnable2
            // 
            this.cbxChDataEnable2.AutoSize = true;
            this.cbxChDataEnable2.Location = new System.Drawing.Point(19, 81);
            this.cbxChDataEnable2.Name = "cbxChDataEnable2";
            this.cbxChDataEnable2.Size = new System.Drawing.Size(65, 24);
            this.cbxChDataEnable2.TabIndex = 1;
            this.cbxChDataEnable2.Text = "Ch 2";
            this.cbxChDataEnable2.UseVisualStyleBackColor = true;
            this.cbxChDataEnable2.CheckedChanged += new System.EventHandler(this.cbxChDataEnable2_CheckedChanged);
            // 
            // cbxChDataEnable1
            // 
            this.cbxChDataEnable1.AutoSize = true;
            this.cbxChDataEnable1.Location = new System.Drawing.Point(19, 36);
            this.cbxChDataEnable1.Name = "cbxChDataEnable1";
            this.cbxChDataEnable1.Size = new System.Drawing.Size(65, 24);
            this.cbxChDataEnable1.TabIndex = 0;
            this.cbxChDataEnable1.Text = "Ch 1";
            this.cbxChDataEnable1.UseVisualStyleBackColor = true;
            this.cbxChDataEnable1.CheckedChanged += new System.EventHandler(this.cbxChDataEnable1_CheckedChanged);
            // 
            // btnSetup
            // 
            this.btnSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetup.Location = new System.Drawing.Point(12, 37);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(105, 59);
            this.btnSetup.TabIndex = 4;
            this.btnSetup.Text = "Setup Instrument";
            this.btnSetup.UseVisualStyleBackColor = true;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(201, 125);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInfo.Size = new System.Drawing.Size(490, 321);
            this.txtInfo.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(718, 496);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.btnSetup);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbxChDisplay3;
        private System.Windows.Forms.CheckBox cbxChDisplay2;
        private System.Windows.Forms.CheckBox cbxChDisplay1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbxChDataEnable4;
        private System.Windows.Forms.CheckBox cbxChDataEnable3;
        private System.Windows.Forms.CheckBox cbxChDataEnable2;
        private System.Windows.Forms.CheckBox cbxChDataEnable1;
        private System.Windows.Forms.CheckBox cbxSaveDataFile;
        private System.Windows.Forms.Button btnStartAcquisition;
        private System.Windows.Forms.Button btnGetData;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.CheckBox cbxChDisplay4;
        private System.Windows.Forms.TextBox txtInfo;
    }
}

