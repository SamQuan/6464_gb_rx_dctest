// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Inst_SMUTI_TriggeredSMU.cs
//
// Author: tommy.yu, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using System.ComponentModel;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Inst_SMUTI_TriggeredSMU
    /// </summary>
    public class Inst_SMUTI_TriggeredSMU : Instrument
    {
        private Chassis_SMUTI instChassis;

        #region some enum variable define
        /// <summary>
        /// channel output state
        /// </summary>
        public enum OutputState
        {
            //output off
            OFF,
            //output on
            ON,
            //output connect gnd
            GND
        }

        /// <summary>
        /// Front section name
        /// </summary>
        public enum FrontSectionName
        {
            FRONt1,
            FRONt2,
            FRONt3,
            FRONt4,
            FRONt5,
            FRONt6,
            FRONt7,
            FRONt8
        }

        /// <summary>
        /// over all mapping measure channel
        /// </summary>
        public enum OverallMapMeaChan
        {
            //Reference channel
            REFerence,

            //Filter channel
            FILTer

        }

        /// <summary>
        /// enum of super mode mapping measure channel,Tx or Rx
        /// </summary>
        public enum SupModMapMeaChan
        {
            //Associate to Tx channel
            TMEAS,

            //Associate to Rx channel
            RMEAS
        }

        /// <summary>
        /// sweep ,overall map or super mode map
        /// </summary>
        public enum Sweep
        {
            OverallMap,
            SuperMap
        }

        /// <summary>
        /// measure type,voltage or current measure
        /// </summary>
        public enum MeasureType
        { 
            //measure voltage
            Voltage,

            //measure current
            Current
        }

        /// <summary>
        /// sweep mode
        /// </summary>
        public enum SweepModes
        {  
            //can only use as power supply
            ONLYPOWER,
            //can only use as Multimeter
            ONLYMETER,
            //can use as power supply and multimeter
            BOTH
        }

        /// <summary>
        /// sweep mode, voltage sweep(LV) or current sweep(LI)
        /// </summary>
        public enum SweepType
        {
            //voltage sweep(LV)
            Voltage,
            //current sweep(LI)
            Current
        }
        #endregion

        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_SMUTI_TriggeredSMU(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 

            // 8 Channel High Current Low Voltage card (DSDR High Current )
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord HighCurrentLowVoltageCard = new InstrumentDataRecord(
                "Hardware_Unknown",				// hardware name 
                "Firmware_Unknown",  			// minimum valid firmware version 
                "Firmware_Unknown");			// maximum valid firmware version 
            ValidHardwareData.Add("HighCurrentLowVoltageCard", HighCurrentLowVoltageCard);

            //TODO: add other type card

            // Configure valid chassis driver information
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Hardware_Unknown",								// chassis driver name  
                "Hardware_Unknown",									// minimum valid chassis driver version  
                "Hardware_Unknown");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_SMUTI", chassisInfo);
            instChassis = (Chassis_SMUTI)chassisInit;
        }

        #region Inst_SMUTI_TriggeredSMU function
        /// <summary>
        /// Cleans up a sweep, aborts it and clears the triggering and traces
        /// </summary>
        public void CleanUpSweep()
        {
            instChassis.Clear();
            OutputEnable = OutputState.OFF;
            DisableTriggering();
            ClearSweepSetting();


        }
       
        /// <summary>
        /// clear sweep sweep memory settings
        /// </summary>
        public void ClearSweepSetting()
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:SWE:MEM:CLE ALL", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }
       
        /// <summary>
        /// Configure Triggerlines
        /// </summary>
        /// <param name="inputTriggerLine"></param>
        /// <param name="outputTriggerLine"></param>
        public void ConfigureTriggerlines(int inputTriggerLine, int outputTriggerLine)
        {

            TriggerInLineNumber = inputTriggerLine;
            TriggerOutLineNumber = outputTriggerLine;
        }
       
        /// <summary>
        /// disable Triigger setting
        /// </summary>
        public void DisableTriggering()
        {
            this.TriggerInLineNumber = 0;
            this.TriggerOutLineNumber = 0;
        }
       
        /// <summary>
        /// InitSourceIMeasureV_CurrentSweep
        /// </summary>
        /// <param name="Istart_A"></param>
        /// <param name="Istop_A"></param>
        /// <param name="Istep_A"></param>
        /// <param name="Vcompliance_V"></param>
        /// <param name="inputTriggerLine"></param>
        /// <param name="outputTriggerLine"></param>
        public void InitSourceIMeasureV_CurrentSweep(double Istart_A, double Istop_A, double Istep_A, double Vcompliance_V, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartCurrent_Amp = Istart_A;
            this.SweepStopCurrent_Amp = Istop_A;
            this.SweepStepCurrent_Amp = Istep_A;
            this.VoltageComplianceSetPoint_Volt = Vcompliance_V;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);
        }
       
        /// <summary>
        /// InitSourceVMeasureI_VoltageSweep,when Triggerin parameter > 0,configure Trigger line signal
        /// </summary>
        /// <param name="Vstart_V">sweep start voltage at V</param>
        /// <param name="Vstop_V">sweep stop voltage at V</param>
        /// <param name="Vstep_V">sweep step voltage at V</param>
        /// <param name="Icompliance_A">compliance current at amp</param>
        /// <param name="TrigInLine">trigger in line number,when this param > 0,set trigger in</param>
        /// <param name="TrigOutLine">trigger out line number</param>
        public void InitSourceVMeasureI_VoltageSweep(double Vstart_V, double Vstop_V, double Vstep_V, double Icompliance_A, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartVoltage_Volt = Vstart_V;
            this.SweepStopVoltage_Volt = Vstop_V;
            this.SweepStepVoltage_Volt = Vstep_V;
            this.CurrentComplianceSetPoint_Amp = Icompliance_A;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);
            
        }
       
        /// <summary>
        /// InitSourceVMeasureI_VoltageSweepFixedCurrent
        /// </summary>
        /// <param name="Vcompliance_V"></param>
        /// <param name="numPts"></param>
        /// <param name="inputTriggerLine"></param>
        /// <param name="outputTriggerLine"></param>
        public void InitSourceIMeasureV_TriggeredFixedCurrent(double iFixed_A, double Vcompliance_V, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartCurrent_Amp = iFixed_A;
            this.SweepStopCurrent_Amp = iFixed_A;
            this.SweepStepCurrent_Amp = 0;
            this.VoltageComplianceSetPoint_Volt = Vcompliance_V;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);
        }
       
        /// <summary>
        /// Init Source V Measure I ,Triggered at Fixed Voltage
        /// </summary>
        /// <param name="vFixed_V"></param>
        /// <param name="iCompliance_v"></param>
        /// <param name="inputTriggerLine"></param>
        /// <param name="outputTriggerLine"></param>
        public void InitSourceVMeasureI_TriggeredFixedVoltage(double vFixed_V, double Icompliance_A, int TrigInLine, int TrigOutLine)
        {
            this.SweepStartVoltage_Volt = vFixed_V;
            this.SweepStopVoltage_Volt = vFixed_V;
            this.SweepStepVoltage_Volt = 0;
            this.CurrentComplianceSetPoint_Amp = Icompliance_A;
            ConfigureTriggerlines(TrigInLine, TrigOutLine);

        }

        #endregion

        #region Instrument override
        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            //TODO:Parse string according return format string
            get
            {
                return instChassis.FirmwareVersion;
            }
        }
        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            //TODO:Parse string according return format string
            get 
            {
                return instChassis.HardwareIdentity;
            }
        }
        /// <summary>
        /// this function can't not support,not support reset single port statues
        /// </summary>
        public override void SetDefaultState()
        {
            this.DisableTriggering();
            this.OutputEnable = OutputState.OFF;
        }

        /// <summary>
        ///  Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                //base.IsOnline = value;
                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Basic Operation Function

        /// <summary>
        /// set the channel sweepMode,determine if source or sense or both is swept
        /// </summary>
        public SweepModes SweepMode
        {
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:MODE " + Enum.GetName(typeof(SweepModes), value), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set channel to single sweep or continuous sweep,
        /// true: set to single sweep,
        /// false: set to continuous sweep,
        /// single sweep: no need Trigger in signal but can provide Trigger out singal,
        /// continuous sweep: need Trigger in signal and can provide Trigger out singal,like work on Trigger links mode
        /// </summary>
        public bool TriggerInEnable
        {
            set
            { 
                string enableState="OFF";
                if (value)
                    enableState = "ON";
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:SINGl " + enableState, this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep delay time ,minimum delay time need to >2mS
        /// </summary>
        public double SweepDelayTime_mS
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:DEL?", this.Slot, this.SubSlot);
                double sweepDelayTime = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII)) * 1000;
                return sweepDelayTime;
            }
            set
            {
                string command;
                double delay_s = value / 1000;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:DEL " + delay_s.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep start voltage
        /// </summary>
        public double SweepStartVoltage_Volt
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STAR?", this.Slot, this.SubSlot);
                double sweepStartVoltage = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStartVoltage;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STAR " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// get/set sweep start current,0--20mA
        /// </summary>
        public double SweepStartCurrent_Amp
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STAR?", this.Slot, this.SubSlot);
                double sweepStartCurrent = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStartCurrent;
            }
            set
            {
                string command;
                double sweepStartCurrent = value;
                if (sweepStartCurrent > 20.0/1000 || sweepStartCurrent < 0)
                    throw new Exception("the current need to be set between 0 to 20 mA");
                else
                    command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STAR " + sweepStartCurrent.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep stop voltage
        /// </summary>
        public double SweepStopVoltage_Volt
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STOP?", this.Slot, this.SubSlot);
                double sweepStopVoltage = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStopVoltage;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STOP " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep stop current,0--20mA
        /// </summary>
        public double SweepStopCurrent_Amp
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STOP?", this.Slot, this.SubSlot);
                double sweepStopCurrent = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStopCurrent;
            }
            set
            {
                string command;
                double sweepStopCurrent = value;
                if (sweepStopCurrent > 20.0/1000 || sweepStopCurrent < 0)
                    throw new Exception("the current need to be set between 0 to 20 mA");
                else
                    command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STOP " + sweepStopCurrent.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep step voltage 
        /// </summary>
        public double SweepStepVoltage_Volt
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STEP?", this.Slot, this.SubSlot);
                double sweepStepVoltage = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStepVoltage;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:VOLT:STEP " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sweep step current
        /// </summary>
        public double SweepStepCurrent_Amp
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STEP?", this.Slot, this.SubSlot);
                double sweepStepCurrent = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return sweepStepCurrent;
            }
            set
            {
                string command;
                double sweepStepCurrent = value;
                if (sweepStepCurrent > 20.0/1000 || sweepStepCurrent < 0.0)
                    throw new Exception("the current need to be set between 0 to 20 mA");
                else
                    command = string.Format(":SOUR{0}:CHAN{1}:SWE:CURR:STEP " + sweepStepCurrent.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// Starts the sweep for the specify memory location
        /// </summary>
        /// <param name="memoryNumber">memory number</param>
        public void StartSweep(int memoryNumber)
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:SWE:MEM:IMM " + memoryNumber.ToString(), this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

        }

        /// <summary>
        /// Clear Up all Source Sweep Setting
        /// </summary>
        public void ClearUpSourSweepSetting()
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:SWE:CLE ALL", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// Clear up the specify source sweep settings
        /// </summary>
        /// <param name="memoryNumber">memory number</param>
        public void ClearUpSourSweepSetting(int MemoryNumber)
        {
            string command;
            if (MemoryNumber > 8 || MemoryNumber < 1)
                throw new Exception("invalid Memory Number, the Sweep Memory Number should be between 1 to 5 ");
            else
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:CLE " + MemoryNumber.ToString(), this.Slot, this.SubSlot);

            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        ///  Save the cached sweep settings to the source sweep list,1--8
        /// </summary>
        /// <param name="MemoryNumber">memory number,this number also is save sweep result memory number</param>
        public void SaveSweepSetting(int MemoryNumber, SweepType sweepType)
        {
            string command;
            if (MemoryNumber > 8 || MemoryNumber < 1)
            {
                throw new Exception("invalid Memory Number, the Memory Number should be between 1 to 8 ");
            }
            else
            {
                switch (sweepType)
                {
                    case SweepType.Voltage:
                        command = string.Format(":SOUR{0}:CHAN{1}:SWE:Volt:SAV " + MemoryNumber.ToString(), this.Slot, this.SubSlot);
                        break;
                    case SweepType.Current:
                        command = string.Format(":SOUR{0}:CHAN{1}:SWE:Curr:SAV " + MemoryNumber.ToString(), this.Slot, this.SubSlot);

                        break;
                    default:
                        throw new Exception("Invalid parameter");
                        break;
                }
            }
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// get MZ sweep result
        /// </summary>
        /// <param name="MemoryNumber">memory number that saved sweep result</param>
        /// <param name="measureData">sweep result</param>
        public void GetMzSweepDataSet(int MemoryNumber, out double[] measureData)
        {
            string command;
            command = string.Format(":SENS{0}:CHAN{1}:SWE:MEM " + MemoryNumber.ToString(), this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
            StringBuilder sb = new StringBuilder();
            string readData;
            string strReadData="";
            do
            {
                readData = instChassis.serialPort_FCUII.ReadLine();

                if (readData.Contains("STAR")) continue;
                if (readData.Contains("END")) break;
                strReadData = readData;

            } while (true);
            instChassis.serialPort_FCUII.ReadLine();
            instChassis.serialPort_FCUII.ReadLine();

            string[] tempStr = strReadData.Split(',');
            measureData = new Double[tempStr.Length];

            for (int i = 0; i < tempStr.Length; i++)
            {
                measureData[i] = double.Parse(tempStr[i].Trim());
            }

        }

        /// <summary>
        /// set/get which trigger-out line is associated with which channel 
        /// </summary>
        private int TriggerOutLineNumber
        {
            get
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:TRIG?", this.Slot, this.SubSlot);
                int triggerOutLineNumber = int.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return triggerOutLineNumber;
            }
            set
            {
                string command;
                command = string.Format(":SOUR{0}:CHAN{1}:SWE:TRIG " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set/get sence trigger in line 
        /// </summary>
        private int TriggerInLineNumber
        {
            get
            {
                string command;
                command = string.Format(":SENS{0}:CHAN{1}:SWE:TRIG?", this.Slot, this.SubSlot);
                string number = instChassis.Query(command, instChassis.serialPort_FCUII);
                int triggerInLineNumber = int.Parse(number);
                return triggerInLineNumber;
            }
            set
            {
                string command;
                command = string.Format(":SENS{0}:CHAN{1}:SWE:TRIG " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// set channel associated to Rear,when do mapping,need to use this function
        /// </summary>
        public void AssociatedRear()
        {

            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:CURR REAR", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

        }

        /// <summary>
        /// set channel associated to Phase
        /// </summary>
        public void AssociatedPhase()
        {

            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:CURR PHAS", this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);

        }

        /// <summary>
        /// set channel associated to FrontSection
        /// </summary>
        /// <param name="FrontSectionNumber"> front section name</param>
        public void AssociatedFrontSection(FrontSectionName FrontSection)
        {
            string command;
            command = string.Format(":SOUR{0}:CHAN{1}:CURR " + FrontSection.ToString(), this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// when do super mode mapping,set channel associate to Tx or Rx
        /// </summary>
        /// <param name="measrueType">measure Tpye,voltage or current</param>
        /// <param name="measureChan">measure channel,Tx or Rx</param>
        public void AssociateSupModeMeasChan(MeasureType measrueType,SupModMapMeaChan measureChan)
        {
            string command;
            string meaType = Enum.GetName(typeof(MeasureType), measrueType);
            string meaChan=Enum.GetName(typeof(SupModMapMeaChan),measureChan);
            command = string.Format(":SENS{0}:CHAN{1}:" + meaType + " " + meaChan, this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// when do overall mapping,set channel associated to Reference measure or filter measure channel
        /// </summary>
        /// <param name="OverallMapMeasureChan"> measure channel</param>
        public void AssociateOverMapMeasChan(OverallMapMeaChan overallMapMeasureType)
        {
            string command;
            string meaType = Enum.GetName(typeof(OverallMapMeaChan), overallMapMeasureType);
            command = string.Format(":SENS{0}:CHAN{1}:CURRent " + meaType, this.Slot, this.SubSlot);
            instChassis.Write(command, this.instChassis.serialPort_FCUII);
        }

        /// <summary>
        /// sense current,return current value
        /// </summary>
        /// <returns>return current value at amps</returns>
        public double SenseCurrent()
        {
            string command;
            command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
            string current_A = instChassis.Query(command, this.instChassis.serialPort_FCUII);
            return double.Parse(current_A);
        }

        /// <summary>
        /// sense current,output ADC vuale
        /// </summary>
        /// <param name="adc">ADC value of current</param>
        public void SenseCurrent(out int adc)
        {
            string command;
            command = string.Format(":SOURce{0}:CHANnel{1}:CURRent?ADC", this.Slot, this.SubSlot);
            adc = int.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));

        }

        /// <summary>
        /// sense voltage,return voltage
        /// </summary>
        /// <param name="senseCompliance_current"></param>
        /// <returns>voltage_Volt</returns>
        public double SenseVoltage()
        {
            string command;
            //get sense current
            command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
            string voltage_V = instChassis.Query(command, this.instChassis.serialPort_FCUII);
            return double.Parse(voltage_V);
        }

        /// <summary>
        /// sense voltage,out put ADC Value
        /// </summary>
        /// <param name="adc">ADC value of voltage</param>
        public void SenseVoltage(out int adc)
        {
            //get sense current
            string command;
            command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?adc", this.Slot, this.SubSlot);
            adc = int.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));

        }

        /// <summary>
        /// get the real current output
        /// </summary>
        public double CurrentActual_Amp
        {
            get
            {
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                string current_A = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                return double.Parse(current_A);
            }
        }

        /// <summary>
        ///  Properties of compliance current value 
        /// </summary>
        public double CurrentComplianceSetPoint_Amp
        {
            get
            {
                try
                {
                    double currentCompliance;
                    string command;
                    command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                    string current = instChassis.Query(command, instChassis.serialPort_FCUII);
                    currentCompliance = double.Parse(current);
                    return currentCompliance;
                }
                catch (Exception e)
                {
                    double returnData = 1.0;
                    return returnData;
                }
            }
            set
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:CURRent " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// Properties of current value of set point 
        /// </summary>
        public double CurrentSetPoint_Amp
        {
            get
            {
                double currentSetPoint;
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:CURRent?", this.Slot, this.SubSlot);
                string current = instChassis.Query(command, instChassis.serialPort_FCUII);
                currentSetPoint = double.Parse(current);
                return currentSetPoint;
            }
            set
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:CURRent " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// get/set outputState ,GND/ON/OFF
        /// </summary>
        public OutputState OutputEnable
        {
            get
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut?", this.Slot, this.SubSlot);
                string outputStatus = instChassis.Query(command, instChassis.serialPort_FCUII);
                switch (outputStatus)
                {
                    case "LOW or OFF":
                        outputStatus = "OFF";
                        break;
                    case "HIGH or ON":
                        outputStatus = "ON";
                        break;
                    default:
                        break;
                }
                OutputState outputEnable = (OutputState)Enum.Parse(typeof(OutputState), outputStatus);
                return outputEnable;
            }
            set
            {
                string command;
                command = string.Format(":SOURce{0}:CHANnel{1}:OUTPut " + Enum.GetName(typeof(OutputState), value), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);

            }
        }

        /// <summary>
        /// get the rear voltage output
        /// </summary>
        public double VoltageActual_Volt
        {
            get
            {
                string command;
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                string voltage_V = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                return double.Parse(voltage_V);
            }
        }

        /// <summary>
        /// set/get compliance voltage
        /// </summary>
        public double VoltageComplianceSetPoint_Volt
        {
            get
            {
                double voltageCompliance;
                string command;
                instChassis.serialPort_FCUII.DiscardInBuffer();
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                string voltAge = instChassis.Query(command, this.instChassis.serialPort_FCUII);
                voltageCompliance = double.Parse(voltAge);
                return voltageCompliance;
            }
            set
            {
                string command;
                instChassis.serialPort_FCUII.DiscardOutBuffer();
                command = string.Format(":SOURce{0}:CHANnel{1}:VOLTage " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        /// <summary>
        /// get/set voltage set point
        /// </summary>
        public double VoltageSetPoint_Volt
        {
            get
            {
                double voltageSetPoint;
                string command;
                instChassis.serialPort_FCUII.DiscardOutBuffer();
                command = string.Format(":SENSe{0}:CHANne{1}:VOLTage?", this.Slot, this.SubSlot);
                voltageSetPoint = double.Parse(instChassis.Query(command, this.instChassis.serialPort_FCUII));
                return voltageSetPoint;
            }
            set
            {
                string command;
                instChassis.serialPort_FCUII.DiscardOutBuffer();
                command = string.Format(":SOURce{0}:CHANnel{1}:VOLTage " + value.ToString(), this.Slot, this.SubSlot);
                instChassis.Write(command, this.instChassis.serialPort_FCUII);
            }
        }

        #endregion

  
    }
}
