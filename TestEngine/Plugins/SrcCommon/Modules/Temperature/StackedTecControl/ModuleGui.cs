// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// Bookham.TestLibrary.TestModules/ModuleGui.cs
// 
// Author: paul.annetts
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// Stacked TEC Control GUI
    /// </summary>
    public partial class ModuleGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Stacked TEC Control GUI
        /// </summary>
        public ModuleGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        private void ModuleMeasureCtrl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(DatumDouble))
            {
                DatumDouble datumDbl = (DatumDouble)payload;

                switch (datumDbl.Name)
                {
                    case "SetPointTemp1":
                        this.Sp1Value.Text = String.Format("{0}", datumDbl.Value);
                        Sp1Value.Refresh();
                        break;
                    case "SetPointTemp2":
                        this.SP2Value.Text = String.Format("{0}", datumDbl.Value);
                        SP2Value.Refresh();
                        break;
                    case "ActualTemp1":
                        this.ActualValue1.Text = String.Format("{0}", datumDbl.Value);
                        ActualValue1.Refresh();
                        break;
                    case "ActualTemp2":
                        this.ActualValue2.Text = String.Format("{0}", datumDbl.Value);
                        ActualValue2.Refresh();
                        break;
                   
                    default:
                        throw new ArgumentException("Invalid datum received: " + datumDbl.Name);
                }
            }

        }

      
    }
}
