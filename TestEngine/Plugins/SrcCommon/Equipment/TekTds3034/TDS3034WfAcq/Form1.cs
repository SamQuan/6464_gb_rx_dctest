using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bookham.TestLibrary.ChassisNS;


namespace Bookham.TestLibrary.Instruments
{
    public partial class Form1 : Form
    {
        private Chassis_TDS3034 tds420Chassis;
        Inst_Tds3034 oscCh1;
        Inst_Tds3034 oscCh2;
        Inst_Tds3034 oscCh3;
        Inst_Tds3034 oscCh4;
        
        Tds3034Channel chan1;
        Tds3034Channel chan2;
        Tds3034Channel chan3;
        Tds3034Channel chan4;

        List<Tds3034Channel> lstChannel2GetData = null;

        bool flgSaveData2File;

        public Form1()
        {
            InitializeComponent();

            oscCh1 = null;
            oscCh2 = null;
            oscCh3 = null;
            oscCh4 = null;

            tds420Chassis = null;

            chan1 = null;
            chan2 = null;
            chan3 = null;
            chan4 = null;

            txtInfo.Text = "";
            groupBox1.Enabled = false;
            groupBox2.Enabled = false;
            lstChannel2GetData = new List<Tds3034Channel>();

        }
                
        private void cbxChDataEnable1_CheckedChanged(object sender, EventArgs e)
        {
            if (lstChannel2GetData.Contains(chan1) && !cbxChDataEnable1.Checked)
            {
                lstChannel2GetData.Remove(chan1 );
            }
            else if ( !lstChannel2GetData .Contains(chan1 ) && cbxChDataEnable1.Checked )
            {
                lstChannel2GetData .Add(chan1 );
            }
        }

        private void cbxChDataEnable2_CheckedChanged(object sender, EventArgs e)
        {
            if (lstChannel2GetData.Contains(chan2) && !cbxChDataEnable2.Checked)
            {
                lstChannel2GetData.Remove(chan2 );
            }
            else if ( !lstChannel2GetData .Contains(chan2 ) && cbxChDataEnable2.Checked )
            {
                lstChannel2GetData .Add(chan2 );
            }

        }

        private void cbxChDataEnable3_CheckedChanged(object sender, EventArgs e)
        {
            if (lstChannel2GetData.Contains(chan3) && !cbxChDataEnable3.Checked)
            {
                lstChannel2GetData.Remove(chan3 );
            }
            else if ( !lstChannel2GetData .Contains(chan3 ) && cbxChDataEnable3.Checked )
            {
                lstChannel2GetData .Add(chan3 );
            }

        }

        private void cbxChDataEnable4_CheckedChanged(object sender, EventArgs e)
        {
            if (lstChannel2GetData.Contains(chan4) && !cbxChDataEnable4.Checked)
            {
                lstChannel2GetData.Remove(chan4 );
            }
            else if ( !lstChannel2GetData .Contains(chan4 ) && cbxChDataEnable4.Checked )
            {
                lstChannel2GetData .Add(chan4 );
            }

        }

        private void cbxSaveDataFile_CheckedChanged(object sender, EventArgs e)
        {
            flgSaveData2File = cbxSaveDataFile.Checked;
        }

        private void btnStartAcquisition_Click(object sender, EventArgs e)
        {
            btnGetData.Enabled = false;
            btnStartAcquisition.Enabled = false;
            chan1.StartWavefromAcquisition();
            btnStartAcquisition.Enabled = true;
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            btnStartAcquisition.Enabled = false;
            foreach ( Tds3034Channel chan in lstChannel2GetData )
            {
                chan.ReadWaveformData(flgSaveData2File);
            }
            btnStartAcquisition.Enabled = true;
        }

        private void cbxChDisplay4_CheckedChanged(object sender, EventArgs e)
        {
            bool status = cbxChDisplay4.Checked;
            chan4.SetChannelDisplayStatus(status);
        }

        private void cbxChDisplay3_CheckedChanged(object sender, EventArgs e)
        {
            bool status = cbxChDisplay3.Checked;
            chan3.SetChannelDisplayStatus(status);
        }

        private void cbxChDisplay2_CheckedChanged(object sender, EventArgs e)
        {
            bool status = cbxChDisplay2.Checked;
            chan2.SetChannelDisplayStatus(status);

        }

        private void cbxChDisplay1_CheckedChanged(object sender, EventArgs e)
        {
            bool status = cbxChDisplay1.Checked;
            chan1.SetChannelDisplayStatus(status);
        }       

        private void btnSetup_Click(object sender, EventArgs e)
        {
            if (tds420Chassis == null)
            {
                tds420Chassis = new Chassis_TDS3034("Chassis_TDS3034", "Chassis_TDS3034", "//szn-dt-jackxue/GPIB0::28::INSTR");
                oscCh1 = new Inst_Tds3034("TdsOsc ch1", "Inst_Tds3034", "1", "", tds420Chassis);
                oscCh2 = new Inst_Tds3034("TdsOsc ch2", "Inst_Tds3034", "2", "", tds420Chassis);
                oscCh3 = new Inst_Tds3034("TdsOsc ch3", "Inst_Tds3034", "3", "", tds420Chassis);
                oscCh4 = new Inst_Tds3034("TdsOsc ch4", "Inst_Tds3034", "4", "", tds420Chassis);

                tds420Chassis.IsOnline = true;
                oscCh1.IsOnline = true;
                oscCh2.IsOnline = true;
                oscCh3.IsOnline = true;
                oscCh4.IsOnline = true;

                //oscCh1.IsRepeatAcquireModeOn = false;
                oscCh1.DataPoints = 2500;
                oscCh1.WavformDataStartPoint = 1;
                oscCh1.WaveformDataStopPoint = oscCh1.DataPoints;


                chan1 = new Tds3034Channel(oscCh1 );
                chan2 = new Tds3034Channel(oscCh2);
                chan3 = new Tds3034Channel(oscCh3);
                chan4 = new Tds3034Channel(oscCh4);

                chan1.GetChannelDisplayStatus += new EventHandler<OscChannelDisplayStatusEventArgs>(ChannelDisplayChange);
                chan2.GetChannelDisplayStatus += new EventHandler<OscChannelDisplayStatusEventArgs>(ChannelDisplayChange);
                chan3.GetChannelDisplayStatus += new EventHandler<OscChannelDisplayStatusEventArgs>(ChannelDisplayChange);
                chan4.GetChannelDisplayStatus += new EventHandler<OscChannelDisplayStatusEventArgs>(ChannelDisplayChange);

                chan1.GetWaveformData += new EventHandler<OscWaveformDataEventArgs>(GetWaveformData);
                chan2.GetWaveformData += new EventHandler<OscWaveformDataEventArgs>(GetWaveformData);
                chan3.GetWaveformData += new EventHandler<OscWaveformDataEventArgs>(GetWaveformData);
                chan4.GetWaveformData += new EventHandler<OscWaveformDataEventArgs>(GetWaveformData);

                chan1.GetWaveformAcquisitionStatus += new EventHandler<oscAcqusitionCompletedEventArgs>(GetAcquisitonStatus);
                chan2.GetWaveformAcquisitionStatus += new EventHandler<oscAcqusitionCompletedEventArgs>(GetAcquisitonStatus);
                chan3.GetWaveformAcquisitionStatus += new EventHandler<oscAcqusitionCompletedEventArgs>(GetAcquisitonStatus);
                chan4.GetWaveformAcquisitionStatus += new EventHandler<oscAcqusitionCompletedEventArgs>(GetAcquisitonStatus);

                groupBox1.Enabled = true;
                groupBox2.Enabled = true;

                txtInfo.Text += "\n Instrument setup ok!";

            }


        }

        void chan1_GetWaveformAcquisitionStatus(object sender, oscAcqusitionCompletedEventArgs e)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        private void ChannelDisplayChange(object sender, OscChannelDisplayStatusEventArgs e)
        {
            int chan = 0;

            if (object.ReferenceEquals(sender, chan1))
                chan = 1;
            else if (object.ReferenceEquals(sender, chan2))
                chan = 2;
            else if (object.ReferenceEquals(sender, chan3))
                chan = 3;
            else if (object.ReferenceEquals(sender, chan4))
                chan = 4;
            ChangeChannelAcqEnableStatus(chan, e.DisplayStatus);

        }

        private void ChangeChannelAcqEnableStatus(int chan, bool enableStatus)
        {
            //Tds420AChannel chanTemp = null;
            CheckBox cbxTemp = null;

            switch (chan)
            {
                case 1:
                    //chanTemp = chan1;
                    cbxTemp = cbxChDataEnable1;
                    break;
                case 2:
                    //chanTemp = chan2;
                    cbxTemp = cbxChDataEnable2;
                    break;
                case 3:
                    //chanTemp = chan3;
                    cbxTemp = cbxChDataEnable3;
                    break;
                case 4:
                    //chanTemp = chan4;
                    cbxTemp = cbxChDataEnable4;
                    break;

            }

            if (cbxTemp != null)
            {
                string info;
                if (enableStatus)
                {
                    cbxTemp.Enabled = true;
                    info = string.Format("\n Enable channel {0} waveform to be acquisition ", chan);
                }
                else
                {
                    cbxTemp.Checked = false;
                    cbxTemp.Enabled = false;
                    info = string.Format("\n Disable channel {0} waveform to be acquisition ", chan);
                }

                txtInfo.Text += info;
            }
        }

        private void GetWaveformData(object sender, OscWaveformDataEventArgs e)
        {
            txtInfo .Text += string .Format ("\n read {0} waveform data!\n data file is : {1}", sender .ToString(), e .DataFileName );
        }

        private void GetAcquisitonStatus(object sender, oscAcqusitionCompletedEventArgs e)
        {
            string info = "";

            if (e.AcquisitionSatus)
                info = "\n Acquisite waveform data ...";
            else
            {
                info = "\n Waveform data acquisition completed!";
                btnGetData.Enabled = true;
            }

            txtInfo.Text += info;
        }
    }
}