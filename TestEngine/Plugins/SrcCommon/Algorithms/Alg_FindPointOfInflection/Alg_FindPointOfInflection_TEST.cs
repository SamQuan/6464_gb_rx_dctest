// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_FindPointOfInflection_TEST.cs
//
// Author: Mark Fullalove


using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using Bookham.TestLibrary.Algorithms;

// Disable missing XML comment warnings for this
#pragma warning disable 1591

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// Test harness for Alg_FindPointOfInflection
    /// </summary>
    [TestFixture]
    public class Alg_FindPointOfInflection_TEST
    {
        [Test]
        public void Valid_POS_POI()
        {
            double[] xData = xData0to30();
            double[] yData = yDataPosTP();
            int startPos = 1;
            int stopPos = 29;
            Alg_FindPointOfInflection.PointOfInflection poi = Alg_FindPointOfInflection.FindPointOfInflection(xData, yData, startPos, stopPos);
            Assert.IsTrue(poi.Found);
            Assert.AreEqual(15, Math.Round(poi.XValue, 8));
            Assert.AreEqual(0, Math.Round(poi.YValue, 8));
        }

        [Test]
        public void Valid_NEG_POI()
        {
            double[] xData = xData0to30();
            double[] yData = yDataNegTP();
            int startPos = 1;
            int stopPos = 29;
            Alg_FindPointOfInflection.PointOfInflection poi = Alg_FindPointOfInflection.FindPointOfInflection(xData, yData, startPos, stopPos);
            Assert.IsTrue(poi.Found);
            Assert.AreEqual(15, Math.Round(poi.XValue, 8));
            Assert.AreEqual(0, Math.Round(poi.YValue, 8));
        }

        [Test]
        public void Linear_NoPOI()
        {
            double[] xData = xData0to30();
            double[] yData = xData0to30();
            int startPos = 1;
            int stopPos = 29;
            Alg_FindPointOfInflection.PointOfInflection poi = Alg_FindPointOfInflection.FindPointOfInflection(xData, yData, startPos, stopPos);
            Assert.IsFalse(poi.Found);
        }

        [Test]
        public void XYswap_POI()
        {
            double[] xData = yDataNegTP();
            double[] yData = xData0to30();
            int startPos = 1;
            int stopPos = 29;
            Alg_FindPointOfInflection.PointOfInflection poi = Alg_FindPointOfInflection.FindPointOfInflection(xData, yData, startPos, stopPos);
            Assert.IsTrue(poi.Found);
            Assert.AreEqual(15, Math.Round(poi.YValue, 8));
            Assert.AreEqual(0, Math.Round(poi.XValue, 8));
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void MismatchedArrays_POI()
        {
            double[] xData = xData0to30();
            double[] yData = { 1, 2, 3, 4 };
            int startPos = 1;
            int stopPos = 29;
            Alg_FindPointOfInflection.PointOfInflection poi = Alg_FindPointOfInflection.FindPointOfInflection(xData, yData, startPos, stopPos);
        }

        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void EmptyArrays_POI()
        {
            double[] xData = { };
            double[] yData = { };
            int startPos = 1;
            int stopPos = 29;
            Alg_FindPointOfInflection.PointOfInflection poi = Alg_FindPointOfInflection.FindPointOfInflection(xData, yData, startPos, stopPos);
        }

        #region testData
        /// <summary>
        /// Integers between 0 and 30
        /// </summary>
        /// <returns>A list of integers from 0 to 30</returns>
        private double[] xData0to30()
        {
            double[] xData = new double[31];
            for (int i = 0; i < 31; i++ )
            {
                xData[i] = i;
            }
            return xData;
        }

        /// <summary>
        /// A set of data whose differential has a positive going turning point
        ///   31 points , COS(X*Pi/30)
        ///   Symmetrical about a turning point at 15
        /// </summary>
        /// 
        private double[] yDataPosTP()
        {
            double[] yData = new double[31];
            for (int i = 0; i < 31; i++)
            {
                yData[i] = Math.Cos( i * Math.PI / 30);
            }
            return yData;
        }

        /// <summary>
        /// A set of data whose differential has a negative going turning point
        ///   31 points, COS(X*PI/30 + PI)
        ///   Symmetrical about a turning point at 15
        /// </summary>
        private double[] yDataNegTP()
        {
            double[] yData = new double[31];
            for (int i = 0; i < 31; i++)
            {
                yData[i] = Math.Cos( Math.PI + (i * Math.PI / 30));
            }
            return yData;
        }
        #endregion

    }
}