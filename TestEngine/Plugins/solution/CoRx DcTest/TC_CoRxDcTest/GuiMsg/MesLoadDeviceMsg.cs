using System;
using System.Collections.Generic;
using System.Text;



namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// this struct data is ue to carry message to LoadBatchDialogueCtl 
    /// to show mes feed back message and batch load button's enable
    /// </summary>
    internal struct mesLoadInfoRsp
    {
        /// <summary>
        /// Flag to indicate if dut information can be enter or not
        /// </summary>
        public bool allowLoadMsg;
        /// <summary>
        /// Flag to indicate if user need to enter dut information
        /// </summary>
        public bool needDutInfoInput;
        /// <summary>
        /// information to show what's MES doing
        /// </summary>
        public string mesMessage;
    }
    /// <summary>
    /// this class is a meassage class sent by worker to LoadBatchDialogueCtl 
    /// to show Mes Batch feed back meassage and control load batch's button's enable state
    /// </summary>
    internal class MesLoadDeviceMsg
    {
        private mesLoadInfoRsp loadDutInfo;

        public MesLoadDeviceMsg(string mesmsg, bool dutInfoLoadEnable,bool inputDutInfo)
        {
            loadDutInfo = new mesLoadInfoRsp();
            loadDutInfo .allowLoadMsg = dutInfoLoadEnable ;
            loadDutInfo.mesMessage = mesmsg ;
            loadDutInfo.needDutInfoInput = inputDutInfo;
        }   

        public string MesMessage
        {
            get { return loadDutInfo.mesMessage; }
            set { loadDutInfo.mesMessage = value; }
        }
        /// <summary>
        /// disable the button when Mes is retrieving dut info
        /// and enable the button when the Mes is idle
        /// </summary>
        public bool EnableLoadDutInformation
        {
            get { return loadDutInfo.allowLoadMsg; }
            set { loadDutInfo .allowLoadMsg = value; }
        }
        /// <summary>
        ///  to decide the dut information show on gui need to clear or not
        /// </summary>
        public bool NeedDutInfoInput
        {
            get { return loadDutInfo.needDutInfoInput; }
            set { loadDutInfo.needDutInfoInput = value; }
        }
    }
}
