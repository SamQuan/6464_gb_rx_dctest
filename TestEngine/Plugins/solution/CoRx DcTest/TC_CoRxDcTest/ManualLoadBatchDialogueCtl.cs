// [Copyright]
//
// Bookham [Coherence RX DC Test]
// ManualLoadBatchDialogueCtl
//
// TC_CoRxDcTest/ManualLoadBatchDialogueCtl.cs
// 
// Author: alice.huang 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;


namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Gui to entry Dut information manually
    /// </summary>
    internal partial class ManualLoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// constructor
        /// </summary>
        public ManualLoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();

        }
        /// <summary>
        /// initialise controls on gui when load
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            txtSN.Text = "";
            txtSN.Focus();            
        }
        /// <summary>
        /// Process maunaly entry information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            if (txtSN.Text.Trim().Length < 4)  // i'm not sure of how many chars the SN should contain
            {
                MessageBox.Show("incorrect DUT SN Input!\n please re-input Dut's SN:");
                txtSN.SelectAll();
                txtSN.Focus();
                return;
            }

            if (cmbPartcode.Text.Trim().Length < 2)// i'm not sure how many chars the device types should contain
            {
                MessageBox.Show(" Please entry Generic code.", "Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                cmbPartcode.Focus();
                return;
            }

            if (!cmbStage.Items.Contains(cmbStage.Text.Trim()))
            {
                MessageBox.Show(" Please select a stage to process.", "Message", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                cmbStage.Focus();
                return;
            }
            //DeviceTypeLookupItem item= (DeviceTypeLookupItem ) this .lvGenericCode.SelectedItems[0].Tag ;
            
            string sn = txtSN.Text.Trim();
            string partCode = cmbPartcode.Text.Trim();//item.GenericCode;
            CoRxDCDutLoadInfo dutInfo = new CoRxDCDutLoadInfo(sn, partCode);            
            dutInfo.TestStage = cmbStage.Text.Trim();
            sendToWorker(dutInfo);
        }
        
        /// <summary>
        /// Process message from worker
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="inMsgSeq"></param>
        /// <param name="respSeq"></param>
        private void ManualLoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if(payload.GetType()== typeof(DatumStringArray))
            {
                DatumStringArray datum = (DatumStringArray)payload;
                string[] datumValue = datum .ValueCopy();

                if (datum.Name == "StageInfo")
                {
                    cmbStage.Items.Clear();
                    foreach (string stage in datumValue)
                    {
                        cmbStage.Items.Add(stage);
                    }
                    cmbStage.Update();
                }
                else if (datum.Name == "PartCodeInfo")
                {
                    cmbPartcode.Items.Clear();
                    foreach (string code in datumValue)
                    {
                        cmbPartcode.Items.Add(code);
                    }
                    cmbPartcode.Update();
                }
            }
        }
 
    }
}
