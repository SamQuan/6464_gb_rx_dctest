// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestControl
//
// BatchViewCtl.Designer.cs
//
// Author: alice.huang, 2009
// Design: [Reference design documentation]

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    partial class BatchViewCtl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDutInfo = new System.Windows.Forms.DataGridView();
            this.cmnDutInformation = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmnValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnReloadDut = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNodeID = new System.Windows.Forms.Label();
            this.lblTestSet = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgDutInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgDutInfo
            // 
            this.dgDutInfo.AllowUserToDeleteRows = false;
            this.dgDutInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDutInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cmnDutInformation,
            this.cmnValue});
            this.dgDutInfo.Location = new System.Drawing.Point(37, 33);
            this.dgDutInfo.Name = "dgDutInfo";
            this.dgDutInfo.ReadOnly = true;
            this.dgDutInfo.Size = new System.Drawing.Size(375, 227);
            this.dgDutInfo.TabIndex = 0;
            // 
            // cmnDutInformation
            // 
            this.cmnDutInformation.HeaderText = "DutInformation";
            this.cmnDutInformation.Name = "cmnDutInformation";
            this.cmnDutInformation.ReadOnly = true;
            this.cmnDutInformation.Width = 150;
            // 
            // cmnValue
            // 
            this.cmnValue.HeaderText = "Value";
            this.cmnValue.Name = "cmnValue";
            this.cmnValue.ReadOnly = true;
            this.cmnValue.Width = 150;
            // 
            // btnTest
            // 
            this.btnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTest.Location = new System.Drawing.Point(476, 217);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(139, 43);
            this.btnTest.TabIndex = 0;
            this.btnTest.Text = "&Continue";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnReloadDut
            // 
            this.btnReloadDut.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReloadDut.Location = new System.Drawing.Point(660, 215);
            this.btnReloadDut.Name = "btnReloadDut";
            this.btnReloadDut.Size = new System.Drawing.Size(139, 45);
            this.btnReloadDut.TabIndex = 1;
            this.btnReloadDut.Text = "&Reload Dut";
            this.btnReloadDut.UseVisualStyleBackColor = true;
            this.btnReloadDut.Click += new System.EventHandler(this.btnReloadDut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(480, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Node Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(480, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Test Set";
            // 
            // lblNodeID
            // 
            this.lblNodeID.BackColor = System.Drawing.SystemColors.Window;
            this.lblNodeID.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNodeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNodeID.Location = new System.Drawing.Point(586, 55);
            this.lblNodeID.Name = "lblNodeID";
            this.lblNodeID.Size = new System.Drawing.Size(238, 32);
            this.lblNodeID.TabIndex = 5;
            // 
            // lblTestSet
            // 
            this.lblTestSet.BackColor = System.Drawing.SystemColors.Window;
            this.lblTestSet.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTestSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestSet.Location = new System.Drawing.Point(586, 112);
            this.lblTestSet.Name = "lblTestSet";
            this.lblTestSet.Size = new System.Drawing.Size(238, 32);
            this.lblTestSet.TabIndex = 6;
            // 
            // BatchViewCtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblTestSet);
            this.Controls.Add(this.lblNodeID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnReloadDut);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.dgDutInfo);
            this.Name = "BatchViewCtl";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.BatchViewCtl_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.dgDutInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgDutInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmnDutInformation;
        private System.Windows.Forms.DataGridViewTextBoxColumn cmnValue;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnReloadDut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNodeID;
        private System.Windows.Forms.Label lblTestSet;

    }
}
