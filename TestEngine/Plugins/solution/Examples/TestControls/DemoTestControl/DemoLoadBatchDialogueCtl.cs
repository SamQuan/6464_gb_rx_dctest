// [Copyright]
//
// Bookham Modular Test Engine
// Test Control Plug-in
//
// DemoLoadBatchDialogue.cs
// 
// Author: Joseph Olajubu
// Design: Test Control DD

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Bookham.DemoTestControlPlugin.GuiMessages;

namespace Bookham.DemoTestControlPlugin
{
	/// <summary>
	/// Demo Test Control's Load Batch Tab.
	/// </summary>
	public class DemoLoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
	{
		private System.Windows.Forms.TextBox loadBatchTextBox;
		private System.Windows.Forms.Button loadBatchButton;
		private System.Windows.Forms.Label messageLabel;
		private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
		private System.Windows.Forms.Label loadBatchScreenLabel;
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		public DemoLoadBatchDialogueCtl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			this.loadBatchTextBox.CharacterCasing = CharacterCasing.Upper;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoLoadBatchDialogueCtl));
            this.loadBatchTextBox = new System.Windows.Forms.TextBox();
            this.loadBatchButton = new System.Windows.Forms.Button();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            this.messageLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // loadBatchTextBox
            // 
            this.loadBatchTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchTextBox.Location = new System.Drawing.Point(8, 80);
            this.loadBatchTextBox.Name = "loadBatchTextBox";
            this.loadBatchTextBox.Size = new System.Drawing.Size(480, 29);
            this.loadBatchTextBox.TabIndex = 0;
            // 
            // loadBatchButton
            // 
            this.loadBatchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchButton.Location = new System.Drawing.Point(8, 120);
            this.loadBatchButton.Name = "loadBatchButton";
            this.loadBatchButton.Size = new System.Drawing.Size(192, 32);
            this.loadBatchButton.TabIndex = 1;
            this.loadBatchButton.Text = "Load Batch";
            this.loadBatchButton.Click += new System.EventHandler(this.loadBatchButton_Click);
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(8, 8);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(136, 50);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bookhamLogoPictureBox.TabIndex = 2;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(144, 16);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(320, 24);
            this.loadBatchScreenLabel.TabIndex = 3;
            this.loadBatchScreenLabel.Text = "Test Engine Demo: Load Batch Screen";
            // 
            // messageLabel
            // 
            this.messageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.messageLabel.ForeColor = System.Drawing.Color.Red;
            this.messageLabel.Location = new System.Drawing.Point(16, 168);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(448, 23);
            this.messageLabel.TabIndex = 4;
            // 
            // DemoLoadBatchDialogueCtl
            // 
            this.Controls.Add(this.messageLabel);
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.loadBatchButton);
            this.Controls.Add(this.loadBatchTextBox);
            this.Name = "DemoLoadBatchDialogueCtl";
            this.Size = new System.Drawing.Size(840, 208);
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.DemoLoadBatchDialogue_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		/// <summary>
		/// Process recieved messages from the worker thread.
		/// </summary>
		/// <param name="payload">Message Payload</param>
		/// <param name="inMsgSeq">Incoming sequence number</param>
		/// <param name="respSeq">Response sequence number</param>
		private void DemoLoadBatchDialogue_MsgReceived(object payload, long inMsgSeq, long respSeq)
		{
			// Find out what type of message has arrived, and process it appropriately.
			Type messageType = payload.GetType();

			if (messageType == typeof (LoadBatchRequest))
			{
				// The Test Control Plug-in wants to load a batch. 
				// Display the GUI controls and prompt the user.
				this.messageLabel.Text = "Please Select A Batch To Load";
				
				//Enable the Load Batch dialogue.
				this.loadBatchTextBox.Text = "";
				displayControls();
				this.loadBatchButton.Enabled = true;
			}
			else if (messageType == typeof (LoadBatchComplete))
			{
				// The batch has been loaded. Hide the controls and prepare for the 
				// next Load Batch operation.
				this.loadBatchTextBox.Text = "";
				this.messageLabel.Text= "";
				hideControls();
			}
			else if (messageType == typeof (string))
			{
				// Display the string contained in the message. 
				this.messageLabel.Text = (string)payload;
			}
			else
			{
				//Error - invalid message.

			}
		}

		/// <summary>
		/// The User clicked the load batch button. Send a message to the worker thread, with the 
		/// user's desired Batch ID.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">event parameters.</param>
		private void loadBatchButton_Click(object sender, System.EventArgs e)
		{
			// Do not allow empty BatchIDs.
			if (this.loadBatchTextBox.Text.Length != 0)
			{
				// Disable the button
				this.loadBatchButton.Enabled = false;
				// Grab the Text from the load batch text box and message the worker with it's contents.
				LoadBatchResponse message = new LoadBatchResponse (this.loadBatchTextBox.Text);
				this.sendToWorker (message);
			}
		}

		/// <summary>
		/// Dispay normal load batch screen controls.
		/// </summary>
		private void displayControls()
		{
			this.loadBatchScreenLabel.Show();
			this.bookhamLogoPictureBox.Show();
			showLoadBatchControls ();
		}

		/// <summary>
		/// Hide All  Controls
		/// </summary>
		private void hideControls()
		{
			this.loadBatchScreenLabel.Hide();
			this.bookhamLogoPictureBox.Hide();
			hideLoadBatchControls();
		}

		/// <summary>
		/// Hide Load Batch operation associated controls.
		/// </summary>
		private void hideLoadBatchControls()
		{
			this.loadBatchTextBox.Hide();
			this.loadBatchButton.Hide();
			this.messageLabel.Hide();
		}

		/// <summary>
		/// Show Load Batch operation associated controls.
		/// </summary>
		private void showLoadBatchControls ()
		{
			this.loadBatchTextBox.Show();
			this.loadBatchButton.Show();
			this.messageLabel.Show();
		}

		/// <summary>
		/// Disable Load Batch operation associated controls.
		/// </summary>
		private void disableLoadBatchControls()
		{
			this.loadBatchTextBox.Enabled = false;
			this.loadBatchButton.Enabled = false;
		}

		/// <summary>
		/// Enable Load Batch operation associated controls.
		/// </summary>
		private void enableLoadBatchControls ()
		{
			this.loadBatchTextBox.Enabled = true;
			this.loadBatchButton.Enabled = true;
		}

	}

}

