// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_Tds420.cs
//
// Author: alice.huang, 2011
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
    // TODO: 
    // 1. Change the base class to the type of chassis you are implementing 
    //   (ChassisType_Visa488_2 for IEEE 488.2 compatible chassis, ChassisType_Visa for other VISA message
    //    based types).
    // 2. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 3. Fill in the gaps.
    public class Chassis_TDS3034 : ChassisType_Visa488_2
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="chassisNameInit">Chassis name</param>
        /// <param name="driverNameInit">Chassis driver name</param>
        /// <param name="resourceString">Resource string for communicating with the chassis (for VISA
        /// chassis, this is the VISA resource string)</param>
        public Chassis_TDS3034(string chassisNameInit, string driverNameInit,
            string resourceString)
            : base(chassisNameInit, driverNameInit, resourceString)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            ChassisDataRecord chassis_Tds3034C = new ChassisDataRecord(
                "TEKTRONIX TDS 3034C",			// hardware name 
                "0",			// minimum valid firmware version 
                "v9.99");		// maximum valid firmware version 
            ValidHardwareData.Add("chassis_Tds3034C", chassis_Tds3034C);

            this.Timeout_ms = 100;
        }
        #endregion

        #region Chassis overrides
        /// <summary>
        /// Firmware version of this chassis.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string command = "*IDN?";
                string res = this.Query_Unchecked(command, null);
                // TEKTRONIX,TDS 3034C,0,CF:91.1CT FV:v4.05 TDS3GV:v1.00 TDS3FFT:v1.00 TDS3TRG:v1.00

                string[] idn = res.Split(',');
                string[] verStr = idn[3].Split(' ');
                return verStr[1].Trim().TrimStart("FV:".ToCharArray());
            }
        }

        /// <summary>
        /// Hardware Identity of this chassis.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TEKTRONIX,TDS 3034C,0,CF:91.1CT FV:v4.05 TDS3GV:v1.00 TDS3FFT:v1.00 TDS3TRG:v1.00
                string command = "*IDN?";
                string res = this.Query_Unchecked(command, null);
                string[] idn = res.Split(',');
                return idn[0] + " " + idn[1];
            }
        }

        /// <summary>
        /// Setup the chassis as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                if (value) // if setting online                
                {
                    
                }

                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // Sets the Response Header Enable State that sets the oscilloscope to omit headers on query responses.
                    this.Write_Unchecked("HEAD OFF", null);

                    //// 4 = Query error, 8 = Device error, 16 = execution error, 32 = command error
                    //this.StandardEventRegisterMask = 4 + 8 + 16 + 32;
                    //// clear the status registers
                    //this.Write("*CLS", null);                    
                }
            }
        }
        #endregion

        /// <summary>
        /// Reset chassis
        /// </summary>
        public void Reset()
        {
            this.Write_Unchecked("*RST",null);
        }
    }
}
