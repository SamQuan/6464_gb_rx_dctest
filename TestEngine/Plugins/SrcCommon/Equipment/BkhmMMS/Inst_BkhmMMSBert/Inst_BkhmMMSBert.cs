// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_BkhmBert.cs
//
// Author: Paul.Annetts, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// Bookham BERT instrument
	/// </summary>
	public class Inst_BkhmMMSBert : Instrument,
		IInstType_RfClockSource, IInstType_BertPatternGen, IInstType_BertErrorAnalyser
	{
		/// <summary>
		/// Supported Bit rates
		/// </summary>
		internal enum BkhmBertBitRate
		{
			Mpbs_9953 = 3,
			Mpbs_10313 = 0,
			Mpbs_10703 = 2,
			Mpbs_11096 = 1
		}

		/// <summary>
		/// Supported Patterns
		/// </summary>
		internal enum BkhmBertPatternMode
		{
			Prbs7 = 0,
			Prbs23 = 2,
			Prbs31 = 3,
			User = 1
		}

		/// <summary>
		/// Meaning of the bits in the BERT control register
		/// </summary>
		[Flags]
		internal enum BertControlStatus
		{
			RateBit0 = 0x0001,
			RateBit1 = 0x0002,
			Pattern0 = 0x0004,
			Pattern1 = 0x0008,
			TxRunning = 0x0010,
			RxRunning = 0x0020,
			ResetCounters = 0x0040,
			//0x0080
			DelayEn = 0x0100,
			GenErrors = 0x0200,
			BeepEn = 0x0400,
			//0x0800        
			//0x1000        
			//0x2000        
			ScrambleEn = 0x4000,
			SonetEn = 0x8000
		}

		/// <summary>
		/// Meaning of the bits in the BERT status register
		/// </summary>
		[Flags]
		internal enum BertStatus
		{
			AquiringLock = 0x0001,
			PattLock = 0x0002,
			LockLost = 0x0004,
			Receiving = 0x0008,
			Unused1 = 0x0010,
			Unused2 = 0x0020,
			Unused3 = 0x0040,
			OscCal = 0x0080
		}


		#region Constructor
		/// <summary>
		/// Instrument Constructor.
		/// </summary>
		/// <param name="instrumentNameInit">Instrument name</param>
		/// <param name="driverNameInit">Instrument driver name</param>
		/// <param name="slotInit">Slot ID for the instrument</param>
		/// <param name="subSlotInit">Sub Slot ID for the instrument</param>
		/// <param name="chassisInit">Chassis through which the instrument communicates</param>
		public Inst_BkhmMMSBert(string instrumentNameInit, string driverNameInit,
			string slotInit, string subSlotInit, Chassis chassisInit)
			: base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
			chassisInit)
		{
			// Setup expected valid hardware variants 
			// TODO: Update with useful names, real values, create multiple copies as required.
			InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
				"Bookham Bert",				// hardware name 
				"0.1",  			// minimum valid firmware version 
				"1.0");			// maximum valid firmware version 
			ValidHardwareData.Add("InstrVariant1", instrVariant1);

			// Configure valid chassis driver information
			// TODO: Update with useful key name, real chassis name and versions
			InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
				"Chassis_BkhmMMS",								// chassis driver name  
				"0.0.0.0",									// minimum valid chassis driver version  
				"2.0.0.0");									// maximum valid chassis driver version
			ValidChassisDrivers.Add("Chassis_BkhmMMS", chassisInfo);

			// recall the card ID
			this.cardId = byte.Parse(slotInit);
			// initialise this instrument's chassis
			this.instrumentChassis = (Chassis_BkhmMMS)chassisInit;
		}
		#endregion

		#region Instrument overrides
		/// <summary>
		/// Is online?
		/// </summary>
		public override bool IsOnline
		{
			get
			{
				return base.IsOnline;
			}
			set
			{
				// try a simple command twice to ensure that comms is working
				int counter = 0;
				bool commsOk = false;
				do
				{
					try
					{
						instrumentChassis.CardFirmwareVersion(cardId, this);
						commsOk = true;
					}
					catch (ChassisException e)
					{
						if (counter >= 2) throw new InstrumentException("Couldn't connect!", e);
					}
				}
				while ((counter < 2) && (!commsOk));

				base.IsOnline = value;
			}
		}

		/// <summary>
		/// Firmware version of this instrument.
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				return instrumentChassis.CardFirmwareVersion(cardId, this);
			}
		}

		/// <summary>
		/// Hardware Identity of this instrument.
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				return instrumentChassis.CardHardwareVersion(cardId, this);
			}
		}

		/// <summary>
		/// Set instrument to default state
		/// </summary>
		public override void SetDefaultState()
		{

		}
		#endregion

		#region Private data and helper functions
		/// <summary>
		/// Instrument's chassis
		/// </summary>
		private Chassis_BkhmMMS instrumentChassis;

		/// <summary>
		/// Card ID in the Bookham chassis (RS-485 address)
		/// </summary>
		private byte cardId;

		/// <summary>
		/// Get BERT Status register
		/// </summary>
		/// <returns>Register values</returns>
		private BertStatus getBertStatus()
		{
			// get bytes
			byte[] resp = instrumentChassis.DoCommand(cardId, 0x516, this);
			// convert to PC's local bit-ordering
			Int32 statusInt = Alg_BigEndianConvert.BigEndianBytesToInt32(resp, false);
			// what's up?
			BertStatus status = (BertStatus)statusInt;
			return status;
		}

		/// <summary>
		/// Get BERT control register
		/// </summary>
		/// <returns>Register values</returns>
		private BertControlStatus getBertControlStatus()
		{
			// get bytes
			byte[] resp = instrumentChassis.DoCommand(cardId, 0x517, this);
			// convert to PC's local bit-ordering
			Int32 statusInt = Alg_BigEndianConvert.BigEndianBytesToInt32(resp, false);
			// what's up?
			BertControlStatus status = (BertControlStatus)statusInt;
			return status;
		}

		/// <summary>
		/// Get Pattern Generator mode
		/// </summary>
		/// <returns>Current mode</returns>
		private BkhmBertPatternMode getBertPatternMode()
		{
			byte[] resp = instrumentChassis.DoCommand(cardId, 0x513, this);
			int pattModeInt = resp[0];
			BkhmBertPatternMode patternMode = (BkhmBertPatternMode)pattModeInt;
			return patternMode;
		}

		/// <summary>
		/// Set Pattern Generator mode
		/// </summary>
		/// <param name="patternMode">New mode</param>
		private void setBertPatternMode(BkhmBertPatternMode patternMode)
		{
			instrumentChassis.DoCommand(cardId, 0x512, (byte)patternMode, this);
		}

		#endregion


		#region IInstType_RfClockSource Members
		/// <summary>
		/// Get/set clock frequency in MHz
		/// </summary>
		public double Frequency_MHz
		{
			get
			{
				byte[] resp =
					instrumentChassis.DoCommand(cardId, 0x511, this);
				BkhmBertBitRate bitRate = (BkhmBertBitRate)resp[0];
				switch (bitRate)
				{
					case BkhmBertBitRate.Mpbs_9953:
						return 9953.0;
					case BkhmBertBitRate.Mpbs_10703:
						return 10703.0;
					case BkhmBertBitRate.Mpbs_11096:
						return 11096.0;
					default:
						throw new InstrumentException("Unrecognised bitrate " + bitRate.ToString());
				}
			}
			set
			{
				int freqMHz = (int)value;
				BkhmBertBitRate bitRate;
				switch (freqMHz)
				{
					case 9953:
						bitRate = BkhmBertBitRate.Mpbs_9953;
						break;
					case 10703:
						bitRate = BkhmBertBitRate.Mpbs_10703;
						break;
					case 10313:
						bitRate = BkhmBertBitRate.Mpbs_10313;
						break;
					case 11096:
						bitRate = BkhmBertBitRate.Mpbs_11096;
						break;
					default:
						throw new InstrumentException("Unsupported clock frequency: " + value.ToString());
				}
				instrumentChassis.DoCommand(cardId, 0x510, (byte)bitRate, this);
			}
		}

		/// <summary>
		/// Get/set the clock signal amplitude
		/// </summary>
		public double Amplitude_Vrms
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
			set
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		/// <summary>
		/// Get/set the clock signal amplitude
		/// </summary>
		public double Amplitude_dBm
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
			set
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		/// <summary>
		/// Get/set Clock Source state (true:enabled, false:disabled)
		/// </summary>
		/// <remarks>Can't switch it off!</remarks>
		bool IInstType_RfClockSource.Enabled
		{
			get
			{
				return true;
			}
			set
			{
				if (!value) throw new Exception("The method or operation is not supported.");
			}
		}

		#endregion

		#region IInstType_BertErrorAnalyser

		/// <summary>
		/// Get/set the Pattern type
		/// </summary>
		public InstType_BertDataPatternType PatternType
		{
			get
			{
				BkhmBertPatternMode patternMode = getBertPatternMode();
				switch (patternMode)
				{
					case BkhmBertPatternMode.Prbs7:
					case BkhmBertPatternMode.Prbs23:
					case BkhmBertPatternMode.Prbs31:
						return InstType_BertDataPatternType.Prbs;
					case BkhmBertPatternMode.User:
						return InstType_BertDataPatternType.User;
					default:
						throw new BkhmMMSException("Invalid BkhmBertPatternMode: " + patternMode.ToString());
				}
			}
			set
			{
				InstType_BertDataPatternType currPattern = PatternType;
				if (currPattern != value)
				{
					BkhmBertPatternMode newPatternMode;
					switch (value)
					{
						case InstType_BertDataPatternType.Prbs:
							newPatternMode = BkhmBertPatternMode.Prbs31;
							break;
						case InstType_BertDataPatternType.User:
							newPatternMode = BkhmBertPatternMode.User;
							break;
						default:
							throw new BkhmMMSException("Invalid BkhmBertPatternMode: " + value.ToString());
					}
					setBertPatternMode(newPatternMode);
				}

			}
		}

		/// <summary>
		/// Get/set the PRBS length 2^N-1 (i.e. this is the "N")
		/// </summary>
		public int PrbsLength
		{
			get
			{
				BkhmBertPatternMode patternMode = getBertPatternMode();
				switch (patternMode)
				{
					case BkhmBertPatternMode.Prbs7:
						return 7;
					case BkhmBertPatternMode.Prbs23:
						return 23;
					case BkhmBertPatternMode.Prbs31:
						return 31;
					case BkhmBertPatternMode.User:
					default:
						throw new BkhmMMSException("Not in PRBS mode - can't access pattern length");
				}
			}
			set
			{
				int currPattern = PrbsLength;
				if (currPattern != value)
				{
					BkhmBertPatternMode newPatternMode;
					switch (value)
					{
						case 7:
							newPatternMode = BkhmBertPatternMode.Prbs7;
							break;
						case 23:
							newPatternMode = BkhmBertPatternMode.Prbs23;
							break;
						case 31:
							newPatternMode = BkhmBertPatternMode.Prbs31;
							break;
						default:
							throw new BkhmMMSException("PRBS pattern length not supported: " + value.ToString());
					}
					setBertPatternMode(newPatternMode);
				}
			}
		}

		/// <summary>
		/// Get the User Pattern Length
		/// </summary>
		public int UserPatternLength
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		/// <summary>
		/// Get/set the User Pattern itself
		/// </summary>
		public bool[] UserPattern
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
			set
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		/// <summary>
		/// Get/set whether the data outputs track
		/// </summary>
		public bool DataOutputsTrack
		{
			get
			{
				throw new Exception("The method or operation is not supported.");
			}
			set
			{
				throw new Exception("The method or operation is not supported.");
			}
		}

		/// <summary>
		/// Get/set whether the clock outputs track
		/// </summary>
		public bool ClockOutputsTrack
		{
			get
			{
				throw new Exception("The method or operation is not supported.");
			}
			set
			{
				throw new Exception("The method or operation is not supported.");
			}
		}

		/// <summary>
		/// Set Output RF coupling mode for the specified output
		/// </summary>
		/// <param name="output">Which output</param>
		/// <param name="mode">Which mode</param>
		public void SetOutputCoupling(InstType_BertPattGenOutput output, InstType_BertRfCouplingMode mode)
		{
			throw new Exception("The method or operation is not supported.");
		}

		/// <summary>
		/// Get Output RF coupling mode for the specified output
		/// </summary>
		/// <param name="output">Which output</param>
		/// <returns>Which mode</returns>
		public InstType_BertRfCouplingMode GetOutputCoupling(InstType_BertPattGenOutput output)
		{
			throw new Exception("The method or operation is not supported.");
		}

		/// <summary>
		/// Set Output voltage for the specified output
		/// </summary>
		/// <param name="output">Which output</param>
		/// <param name="vDef">Voltage setup structure</param>
		public void SetOutputVoltage(InstType_BertPattGenOutput output, InstType_BertOutputVoltageDef vDef)
		{
			throw new Exception("The method or operation is not supported.");
		}

		/// <summary>
		/// Get Output voltage for the specified output
		/// </summary>
		/// <param name="output">Which output</param>
		/// <returns>Voltage setup structure</returns>
		public InstType_BertOutputVoltageDef GetOutputVoltage(InstType_BertPattGenOutput output)
		{
			throw new Exception("The method or operation is not supported.");
		}

		/// <summary>
		/// Set Crossing Point percentage for the specified output
		/// </summary>
		/// <param name="output">Which output</param>
		/// <param name="percent">Crossing Point percentage</param>
		public void SetOutputCrossingPt_Percent(InstType_BertPattGenOutput output, double percent)
		{
			throw new Exception("The method or operation is not supported.");
		}

		/// <summary>
		/// Get Crossing Point percentage for the specified output
		/// </summary>
		/// <param name="output">Which output</param>
		/// <returns>Crossing Point percentage</returns>
		public double GetOutputCrossingPt_Percent(InstType_BertPattGenOutput output)
		{
			throw new Exception("The method or operation is not supported.");
		}

		/// <summary>
		/// Get/set Pattern Generator state (true:enabled, false:disabled)
		/// </summary>
		public bool PattGenEnabled
		{
			get
			{
				BertControlStatus status = getBertControlStatus();
				return ((status & BertControlStatus.TxRunning) != 0);
			}
			set
			{
				bool enabled = PattGenEnabled;
				if (value != enabled)
				{
					if (value)
						instrumentChassis.DoCommand(this.cardId, 0x541, this);
					else
						instrumentChassis.DoCommand(this.cardId, 0x542, this);
				}
			}
		}

		#endregion

		#region IInstType_DataErrorAnalyser Members

		/// <summary>
		/// The error ratio accumulated since the start of the gating period,
		/// where each error is a true data zero received as a data one.
		/// </summary>
		public double ErrorRatioOfZerosRecievedAsOnes
		{
			get
			{
				throw new Exception("The method or operation is not supported.");
			}
		}


		/// <summary>
		/// The error ratio accumulated since the start of the gating period,
		/// where each error is a true data one received as a data zero
		/// </summary>
		public double ErrorRatioOfOnesRecievedAsZeros
		{
			get
			{
				throw new Exception("The method or operation is not supported.");
			}
		}

		/// <summary>
		/// Get/set the Data Polarity of the input
		/// </summary>
		public InstType_BertDataPolarity Polarity
		{
			get
			{
				throw new Exception("The method or operation is not supported.");
			}
			set
			{
				throw new Exception("The method or operation is not supported.");
			}
		}

		/// <summary>
		/// Automatically setup the receiver in terms of eye delay and threshold, and 
		/// also pattern synchronisation
		/// </summary>
		public void AutoSetup()
		{
			// does this automatically, I think...
		}

		/// <summary>
		/// Get/set the Eye Delay
		/// </summary>
		public double MeasPhaseDelay_ps
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
			set
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		/// <summary>
		/// Get/set the Eye threshold voltage
		/// </summary>
		public double MeasThreshold_V
		{
			get
			{
				throw new Exception("The method or operation is not implemented.");
			}
			set
			{
				throw new Exception("The method or operation is not implemented.");
			}
		}

		/// <summary>
		/// Start the Analyser
		/// </summary>
		public void AnalyserStart()
		{
			if (this.AnalyserEnabled) AnalyserStop();
			// start analyser
			instrumentChassis.DoCommand(this.cardId, 0x581, this);
			// reset registers (do this after start as there's always a startup glitch!)
			instrumentChassis.DoCommand(this.cardId, 0x583, this);
		}

		/// <summary>
		/// Stop the Analyser
		/// </summary>
		public void AnalyserStop()
		{
			if (this.AnalyserEnabled)
			{
				instrumentChassis.DoCommand(this.cardId, 0x582, this);
			}
		}

		/// <summary>
		/// Get the analyser state (true: running, false: not running)
		/// </summary>
		public bool AnalyserEnabled
		{
			get
			{
				BertControlStatus status = getBertControlStatus();
				return ((status & BertControlStatus.RxRunning) != 0);
			}
		}

		/// <summary>
		/// How many errored bits
		/// </summary>
		public Int64 ErroredBits
		{
			get
			{
				byte[] bytes =
					instrumentChassis.DoCommand(cardId, 0x584, this);
				Int32 val = Alg_BigEndianConvert.BigEndianBytesToInt32(bytes, false);
				Int64 errBits = (Int64)val;
				return errBits;
			}
		}

		/// <summary>
		/// Ratio of errored bits to total bit count
		/// </summary>
		public double ErrorRatio
		{
			get
			{
				byte[] bytes =
					instrumentChassis.DoCommand(cardId, 0x584, this);
				Int32 errBits = Alg_BigEndianConvert.BigEndianBytesToInt32(bytes, false);
				bytes =
					instrumentChassis.DoCommand(cardId, 0x543, this);
				Int64 txBitsDiv64 = Alg_BigEndianConvert.BigEndianBytesToInt64(bytes, false);
				Int64 txBits = txBitsDiv64 * 64;

				double val;
				if (txBits > 0) val = (double)errBits / txBits;
				else val = 0.0;

				return val;
			}
		}

		/// The number of errors
		/// accumulated since the start of the gating period, where
		/// each error is a true data zero received as a data one.
		/// </summary>
		public long ErroredBitsZerosRecievedAsOnes
		{
			get
			{
				throw new InstrumentException("ErroredBitsZerosRecievedAsOnes: This function is not yet implemented");
			}
		}

		/// <summary>
		///The number of errors
		///accumulated since the start of the gating period, where
		///each error is a true data one received as a data zero
		/// </summary>
		public long ErroredBitsOnesRecievedAsZeros
		{
			get
			{
				throw new InstrumentException("ErroredBitsOnesRecievedAsZeros: This function is not yet implemented");
			}
		}


		/// <summary>
		/// Has there been a sync loss since the analyser was last enabled?
		/// </summary>
		public bool SyncLossDetected
		{
			get
			{
				BertStatus status = getBertStatus();
				//     return ((status & BertStatus.RxRunning) != 0);

				if (((status & BertStatus.LockLost) > 0) || ((status & BertStatus.PattLock) == 0))
				{
					return true;
				}
				else
				{
					return false;
				}

				//byte[] bytes =
				//    instrumentChassis.DoCommand(cardId, 0x585, this);
				//Int32 lostBits = Alg_BigEndianConvert.BigEndianBytesToInt32(bytes, false);
				//if (lostBits > 0) return true;
				//else return false;
			}
		}

		#endregion
	}
}
