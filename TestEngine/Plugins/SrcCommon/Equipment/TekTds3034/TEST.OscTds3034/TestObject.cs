using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.Utilities;

namespace TEST.OscTds420
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_TDS3034 testChassis;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
        private Inst_Tds3034 testInstr;
        private Inst_Tds3034 testInstr_2; 
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_TDS3034("Chassis_TDS3034", "Chassis_TDS3034", "//szn-dt-jackxue/GPIB0::28::INSTR");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Inst_Tds3034("TdsOsc ch1", "Inst_Tds3034", "1", "1", testChassis);
            testInstr_2 = new Inst_Tds3034("TdsOsc ch2", "Inst_Tds3034", "2", "1", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;
            TestOutput(testChassis, "IsOnline set true OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            testInstr_2.IsOnline = true;
            testInstr_2.EnableLogging = true;
            TestOutput(testInstr, "IsOnline set true OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            testInstr.EnableFrontPannel();
            TestOutput("Don't forget to take the chassis offline!");
            testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_ClearWaveForm()
        {
            TestOutput("\n\n*** T01_ClearWaveForm ***");
            testInstr.ClearWaveformStorage();
            testInstr.ClearAllWaveformStorage();
            
        }

        [Test]
        public void T02_ChannelDisplay()
        {
            TestOutput("\n\n*** T02_ChannelDisplay***");

            TestOutput("\n ********** Turn off channel display *************");
            if (testInstr.IsChannelWaveformDisplay)
                testInstr.IsChannelWaveformDisplay = false;
            if (testInstr.IsDisplaySavedWaveform)
                testInstr.IsDisplaySavedWaveform = false;
            if (testInstr_2.IsChannelWaveformDisplay)
                testInstr_2.IsChannelWaveformDisplay = false;
            if (testInstr_2.IsDisplaySavedWaveform)
                testInstr_2.IsDisplaySavedWaveform = false;

            TestOutput("\n ********** Turn on channel display *************");            
            testInstr.IsChannelWaveformDisplay = true;            
            testInstr_2.IsChannelWaveformDisplay = true;

            TestOutput("\n *********** Test display format *************");
            testInstr.DisplayFormat = Inst_Tds3034.EnumDisplayFormat.XY;
            System .Threading .Thread .Sleep(1000);
            Assert.AreEqual(testInstr.DisplayFormat, Inst_Tds3034.EnumDisplayFormat.XY);
            System .Threading .Thread .Sleep(1000);
            testInstr .DisplayFormat = Inst_Tds3034.EnumDisplayFormat .YT ;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumDisplayFormat.YT, testInstr.DisplayFormat);

        }

        [Test]
        public void T03_AcquisitionMode()
        {
            TestOutput("\n\n *************** Testacquisition mode *************");
            testInstr.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.AVE;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumAcquisitionMode.AVE, testInstr.AcquisitionMode);
            testInstr_2.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.PEAK;
            System.Threading.Thread.Sleep(1000);            
            Assert.AreEqual(Inst_Tds3034.EnumAcquisitionMode.PEAK, testInstr_2.AcquisitionMode);

            testInstr_2.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.ENV;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumAcquisitionMode.ENV, testInstr_2.AcquisitionMode);
            //testInstr.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.HIR;
            //System.Threading.Thread.Sleep(1000);
            //            Assert.AreEqual(Inst_Tds3034.EnumAcquisitionMode.HIR, testInstr.AcquisitionMode);

            testInstr.AcquisitionMode = Inst_Tds3034.EnumAcquisitionMode.SAM;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumAcquisitionMode.SAM, testInstr_2.AcquisitionMode);

        }

        [Test]
        public void T04_WaveformSetting()
        {
            TestOutput("\n\n ****************** Setting waveform setting **************");

            TestOutput("\n****************** Setting waveform autosaving **************");

            // NOT SUPPORT BY TDS420A
            //testInstr.AutoSaveAcquisition = false;            
            //System.Threading.Thread.Sleep(1000);
            //Assert.AreEqual(false, testInstr.AutoSaveAcquisition);   
            //testInstr.AutoSaveAcquisition = true;

            //TestOutput("\n**************Setting waveform storage size *************");
            int dataPoints = 500;
            //testInstr.WaveformStorageSize = 2 * dataPoints;
            //testInstr_2.WaveformStorageSize = dataPoints;
            //System.Threading.Thread.Sleep(1000);
            //Assert.AreNotEqual(2 * dataPoints, testInstr.WaveformStorageSize);
            //Assert.AreEqual(dataPoints, testInstr_2.WaveformStorageSize);

            TestOutput("\n *************** Setting waveform sample points ***************");
            testInstr.DataPoints = dataPoints;
            //testInstr_2.DataPoints = dataPoints;
            System.Threading.Thread.Sleep(1000);
            //Assert.AreEqual(dataPoints, testInstr.DataPoints);
            Assert.AreEqual(dataPoints, testInstr_2.DataPoints);

            TestOutput("\n ******************Setting Channel bandwidth ******************");
            testInstr.BandWidth = Inst_Tds3034.EnumBandWidth.ONEF;
            testInstr_2.BandWidth = Inst_Tds3034.EnumBandWidth.FUL;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumBandWidth.FUL, testInstr_2.BandWidth);
            Assert.AreEqual(Inst_Tds3034.EnumBandWidth.ONEF, testInstr.BandWidth);

            TestOutput("\n ******************** Setting channel coupling ***************");
            testInstr.Coupling = Inst_Tds3034.EnumCouplingMode.DC;
            testInstr_2.Coupling = Inst_Tds3034.EnumCouplingMode.AC;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumCouplingMode.DC, testInstr.Coupling);
            Assert.AreEqual(Inst_Tds3034.EnumCouplingMode.AC, testInstr_2.Coupling);

            TestOutput("\n ******************** Setting channel impedance **************");
            testInstr.Impedance = Inst_Tds3034.EnumImpedance.FIF;
            testInstr_2.Impedance = Inst_Tds3034.EnumImpedance.MEG;
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(Inst_Tds3034.EnumImpedance.FIF, testInstr.Impedance);
            Assert.AreEqual(Inst_Tds3034.EnumImpedance.MEG, testInstr_2.Impedance);
             
            TestOutput("\n **************** Setting waveform position *************");
            testInstr.VerticalPosition_Div = -4;
            testInstr_2.VerticalPosition_Div = -4;
            Assert.AreEqual(-4, testInstr.VerticalPosition_Div);
            Assert.AreEqual(-4, testInstr_2.VerticalPosition_Div);

            TestOutput("\n *************** Setting  acquisition stop condition ****************");
            testInstr.AcquisitionStopCondition = Inst_Tds3034.EnumAcquisitionStopCondition.SEQ;
            //testInstr_2.AcquisitionStopCondition = Inst_Tds3034.EnumAcquisitionStopCondition.SEQuence;


        }

        [Test]
        public void T05_GetWaveform()
        {
            TestOutput("\n\n ****************** Start waform acquisiton *****************");
            testInstr.EnableServiceRequest();
            testInstr.AcquisitionState = true;
            //testInstr_2.AutoSet();
            testInstr_2.Trigger();
            bool status = testInstr.WaitForAcquisitionToFinish();
            Assert.AreEqual(true, status);

            TestOutput("\n ***************** Getting waveform ***********************");
            Inst_Tds3034.ClsWaveformData wfData;
            //Inst_Tds3034.ClsWaveformDataScale scaleInfo;
            wfData =  testInstr.GetWaveform();
            TestOutput(" \n the first channel's scale information is \t " + wfData.ScaleInfomation .ScaleInfromationString);
            wfData = testInstr_2.GetWaveform();
            TestOutput(" \n the second channel's scale information is \t " + wfData .ScaleInfomation .ScaleInfromationString);
            testInstr.SaveWaveform();
            testInstr.WaitForOperationToComplete();
            testInstr_2.SaveWaveform();
            testInstr_2.WaitForOperationToComplete();

            TestOutput("\n ***************** Getting saved waveform data **************");
            testInstr.IsDisplaySavedWaveform = true;
            testInstr_2.IsDisplaySavedWaveform = true;
            Inst_Tds3034 .ClsWaveformData chanWaveformData = testInstr.GetSavedWaveForm();
            TestOutput(" \n the first channel's saved scale information is \t " + chanWaveformData .ScaleInfomation.ScaleInfromationString);
            Inst_Tds3034.ClsWaveformData savedWaveformData = testInstr_2.GetSavedWaveForm();
            TestOutput(" \n the second channel's saved  scale information is \t " + savedWaveformData .ScaleInfomation .ScaleInfromationString );

        }

        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        private void saveWaveform(Inst_Tds3034.StrcWaveform wfData)
        {
            List<string> dataList = new List<string>();
            StringBuilder sb ;

            for (int i = 0; i < wfData.Xdata.Length; i++)
            {                
                sb= new StringBuilder();
                sb.Append(wfData.Xdata[i]);
                sb.Append(",");
                sb.Append(wfData.Ydata[i]);
                sb.Append("\n");
                dataList .Add( sb .ToString());
            }

            string fileName = string .Format ("wfdata_{0}.csv", DateTime.Now.ToLongTimeString());
            using (CsvWriter writer = new CsvWriter())
            {
                writer.WriteFile(fileName, dataList);
            }
            
            
            
        }
        #endregion


    }

}
