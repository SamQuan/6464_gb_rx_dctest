using System;
using System.Diagnostics;
using NUnit.Framework;


namespace Bookham.TestLibrary.Algorithms
{
	/// <summary>
	/// Test harness for XatY
	/// </summary>
	[TestFixture]
	public class XatYTEST
	{
        /// <exclude/>
        [Test]
        public void Ok()
		{
			double[] xArray = { 10, 20 };
			double[] yArray = { 1, 2 };
			double x = XatYAlgorithm.Calculate( xArray, yArray, 1.5 );
			Debug.Assert( x == 15 );
		}

        /// <exclude/>
        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void ValueOutsideArrayBounds()
		{
			double[] xArray = { 10, 20 };
			double[] yArray = { 1, 2 };
			double x = XatYAlgorithm.Calculate( xArray, yArray, 3 );
		}

        /// <exclude/>
        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void EmptyArray()
        {
            double[] emptyArray = { };
            double[] yArray = { 1, 2 };
            double x = XatYAlgorithm.Calculate(emptyArray, yArray, 1.5);
        }

        /// <exclude/>
        [Test]
        [ExpectedException(typeof(AlgorithmException))]
        public void DifferentArrayLengths()
        {
            double[] xArray = { 10, 20 };
            double[] yArray = { 1, 2, 3 };
            double x = XatYAlgorithm.Calculate(xArray, yArray, 2.5);
        }
	}
}
