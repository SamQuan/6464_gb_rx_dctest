// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Chassis
//
// Chassis_BkhmBert.cs
//
// Author: Paul.Annetts, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;
using NationalInstruments.VisaNS;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.ChassisNS
{
	/// <summary>
	/// Chassis for Bookham MMS (Bert etc) class
	/// </summary>
	public class Chassis_BkhmMMS : ChassisType_Visa
	{
		/// <summary>
		/// Module type
		/// </summary>
		public enum ModuleType
		{
			///<summary>XFP host card</summary>
			XfpHost = 0,
			///<summary>PSU card</summary>
			Psu = 1,
			///<summary>BERT card</summary>
			Bert = 2
		}

		/// <summary>
		/// System commands
		/// </summary>
		public enum SystemCommands
		{
			///<summary>Get Module Type</summary>
			GetModuleType = 2,
			///<summary>Get Module Revision</summary>
			GetModuleRev = 3,
			///<summary>Module reset</summary>
			ModuleReset = 4
		}

		/// <summary>
		/// Revision structure
		/// </summary>
		public struct Revision
		{
			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="data">byte array from received message</param>
			public Revision(byte[] data)
			{
				VerMajor = data[0];
				VerMinor = data[1];
				BuildYear = data[2];
				BuildMonth = data[3];
			}

			/// <summary>
			/// Return a firmware version string
			/// </summary>
			/// <returns>Value</returns>
			public string FirmwareVersion()
			{ return VerMajor.ToString() + "." + VerMinor.ToString(); }

			/// <summary>
			/// Return a Hardware version string
			/// </summary>
			/// <returns>Value</returns>
			public string HardwareVersion()
			{ return BuildYear.ToString() + "." + BuildMonth.ToString(); }

			///<summary>Major firmware version</summary>
			public int VerMajor;
			///<summary>Minor firmware version</summary>
			public int VerMinor;

			///<summary>Build year</summary>            
			public int BuildYear;
			///<summary>Build month</summary>            
			public int BuildMonth;
			//public int BuildDay;
			//public int BuildHour; 
			//public int BuildMin;
			//public int BuildSec;           

		}

		#region Constructor
		/// <summary>
		/// Instrument Constructor.
		/// </summary>
		/// <param name="chassisNameInit">Chassis name</param>
		/// <param name="driverNameInit">Chassis driver name</param>
		/// <param name="resourceString">Resource string for communicating with the chassis (for VISA
		/// chassis, this is the VISA resource string)</param>
		public Chassis_BkhmMMS(string chassisNameInit, string driverNameInit,
			string resourceString)
			: base(chassisNameInit, driverNameInit, resourceString)
		{
			// Setup expected valid hardware variants 
			ChassisDataRecord chassisData = new ChassisDataRecord(
				"Bookham MMS Chassis",			// hardware name 
				"N/A",			// minimum valid firmware version 
				"N/A");		// maximum valid firmware version 
			ValidHardwareData.Add("Bookham MMS Chassis", chassisData);
		}
		#endregion

		#region Chassis overrides
		/// <summary>
		/// Firmware version of this chassis.
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				return "N/A";
			}
		}

		/// <summary>
		/// Hardware Identity of this chassis.
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				return "Bookham MMS Chassis";
			}
		}

		/// <summary>
		/// Setup the chassis as it goes online
		/// </summary>
		public override bool IsOnline
		{
			get
			{
				return base.IsOnline;
			}
			set
			{
				// setup base class
				base.IsOnline = value;

				if (value) // if setting online                
				{
					// get the visa session that's been created
					this.visaSession = (SerialSession)this.VisaSession;
					visaSession.BaudRate = 115200;
					visaSession.DataBits = 8;
					visaSession.StopBits = StopBitType.One;
				}
			}
		}
		#endregion
		/// <summary>
		/// Do Command - send a command to a BERT chassis based card and process the response.
		/// No data to send.
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="cmdNum">Which command number</param>
		/// <param name="i">Instrument</param>
		/// <returns>Received payload - byte array</returns>
		public byte[] DoCommand(byte cardId, int cmdNum, Instrument i)
		{
			byte[] emptyArray = new byte[0];
			return DoCommand(cardId, cmdNum, emptyArray, i);
		}

		/// <summary>
		/// Do Command - send a command to a BERT chassis based card and process the response.
		/// Single byte as transmitted data
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="cmdNum">Which command number</param>
		/// <param name="data">Data byte to send</param>
		/// <param name="i">Instrument</param>
		/// <returns>Received payload - byte array</returns>
		public byte[] DoCommand(byte cardId, int cmdNum, byte data, Instrument i)
		{
			byte[] dataArray = new byte[] { data };
			return DoCommand(cardId, cmdNum, dataArray, i);
		}

		/// <summary>
		/// Do Command - send a command to a BERT chassis based card and process the response.
		/// Byte array as transmitted data
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="cmdNum">Which command number</param>
		/// <param name="txData">Data byte array to send</param>
		/// <param name="i">Instrument</param>
		/// <returns>Received payload - byte array</returns>
		public byte[] DoCommand(byte cardId, int cmdNum, byte[] txData, Instrument i)
		{
			int txDataLen = txData.Length;
			byte[] header = new byte[8];
			header[0] = 0xFE;
			header[1] = 0xE1;
			header[2] = cardId;
			header[3] = msgId;
			header[4] = checked((byte)(9 + txDataLen));
			header[5] = 0;
			header[6] = checked((byte)(cmdNum / 256));
			header[7] = (byte)(cmdNum % 256);

			byte[] fullTxBytes = new byte[9 + txDataLen];
			Array.Copy(header, fullTxBytes, 8);
			Array.Copy(txData, 0, fullTxBytes, 8, txDataLen);

			byte[] fullRxBytes = sendReceive(fullTxBytes, i, cardId, msgId);
			// increment message ID
			msgId++;

			if (msgId > 255) msgId = 0;

			int nbrPayloadBytes = fullRxBytes.Length - 9;
			byte status = fullRxBytes[5];
			if (status != 0)
			{
				throw new BkhmMMSStatusException(status, fullTxBytes, fullRxBytes);
			}
			byte[] rxData = new byte[nbrPayloadBytes];

			Array.Copy(fullRxBytes, 8, rxData, 0, nbrPayloadBytes);
			return rxData;
		}

		private byte calcChecksum(byte[] data)
		{
			byte sum = 0;
			foreach (byte b in data)
			{
				sum += b;
			}
			byte csum = (byte)(0xFF - sum);
			return csum;
		}

		private byte[] sendReceive(byte[] fullTxBytes, Instrument i, byte cardId, byte msgId)
		{
			byte csumSend = calcChecksum(fullTxBytes);
			fullTxBytes[fullTxBytes.Length - 1] = csumSend;
			visaSession.Write(fullTxBytes);
			this.LogEvent("Sent: " + BkhmMMSException.ByteArrayToString(fullTxBytes));

			DateTime start = DateTime.Now;
			int timeout_ms = 1000;
			TimeSpan timeout = new TimeSpan(0, 0, 0, 0, timeout_ms);

			List<byte> bytesReceived = new List<byte>();
			List<byte> receivedMessage = new List<byte>();

			bool messageComplete = false;
			int expectedMsgLen = -1;
			int rxMsgBytes = 0;

			// continue until timeout or message is completed
			while ((DateTime.Now - start < timeout) && (!messageComplete))
			{
				System.Threading.Thread.Sleep(1);
				// how many bytes are ready to read...
				int availBytes = visaSession.AvailableNumber;

				while ((availBytes-- > 0) && (!messageComplete))
				{
					// read a bytes
					byte[] bytes = visaSession.ReadByteArray(1);
					byte b = bytes[0];
					bytesReceived.Add(b);

					rxMsgBytes = receivedMessage.Count;

					switch (rxMsgBytes)
					{
						// header bytes
						case 0:
							if (b != 0xFE) continue;
							break;
						case 1:
							if (b != 0xE1) receivedMessage.Clear();
							break;
						// don't check card Id
						case 2:
							break;
						// check message Id
						case 3:
							if (b != msgId)
							{
								string errStr = String.Format("Bad MsgID: expected {0}, got {1}",
									msgId, b);
								throw new ChassisException(errStr);
							}
							break;
						// get message length
						case 4:
							expectedMsgLen = b;
							break;
						// no special action
						default:
							break;
					}
					receivedMessage.Add(b);
					// do we now have the whole message?  
					if (receivedMessage.Count == expectedMsgLen) messageComplete = true;
				}
			}
			this.LogEvent("Received: " + BkhmMMSException.ByteArrayToString(bytesReceived));

			// check timeout condition
			if (!messageComplete)
			{
				throw new BkhmMMSException("Didn't receive valid return message",
					fullTxBytes, bytesReceived.ToArray());
			}

			// validate checksum
			byte csumRx = calcChecksum(receivedMessage.ToArray());
			if (csumRx != 0x0)
			{
				throw new BkhmMMSException("Invalid checksum received",
					fullTxBytes, bytesReceived.ToArray());
			}

			return receivedMessage.ToArray();
		}

		/// <summary>
		/// Get the module type
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="i">Instrument</param>
		/// <returns>ModuleType</returns>
		public ModuleType GetModuleType(byte cardId, Instrument i)
		{
			byte[] resp =
				DoCommand(cardId, (int)SystemCommands.GetModuleType, i);
			ModuleType modType = (ModuleType)resp[0];
			return modType;
		}

		/// <summary>
		/// Get the module revision
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="i">Instrument</param>
		/// <returns>Revision structure</returns>
		public Revision GetModuleRevision(byte cardId, Instrument i)
		{
			byte[] resp =
				DoCommand(cardId, (int)SystemCommands.GetModuleRev, i);
			Revision rev = new Revision(resp);
			return rev;
		}

		/// <summary>
		/// Get card hardware version - as used in standard Instrument functions
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="i">Instrument</param>
		/// <returns>Hardware version string</returns>
		public string CardHardwareVersion(byte cardId, Instrument i)
		{
			ModuleType type = GetModuleType(cardId, i);
			return "Bookham " + type.ToString();
		}

		/// <summary>
		/// Get card firmware version - as used in standard Instrument functions
		/// </summary>
		/// <param name="cardId">Card ID</param>
		/// <param name="i">Instrument</param>
		/// <returns>Firmware version string</returns>
		public string CardFirmwareVersion(byte cardId, Instrument i)
		{
			Revision rev = GetModuleRevision(cardId, i);
			return rev.FirmwareVersion();
		}

		#region Private data
		/// <summary>Sequence number of messages</summary>
		private byte msgId = 0;

		///<summary>Communication session - VISA session cast to a SerialSession</summary>
		SerialSession visaSession;
		#endregion
	}
}
