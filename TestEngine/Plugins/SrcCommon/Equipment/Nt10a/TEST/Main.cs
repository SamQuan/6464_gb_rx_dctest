using System;

namespace EquipmentTest_Inst_Nt10a
{
	public class TestWrapper
	{
		public static int Main(string[] args)
		{
			// Init tester 
			Inst_Nt10a_Test test = new  Inst_Nt10a_Test();

			// Run tests
			test.Setup();

			// Create chassis
            test.T01_CreateInst("/GPIB0::10::INSTR");

			test.T02_TryCommsNotOnline();

			// set online
			test.T03_SetOnline();

			test.T04_DriverVersion();

			test.T05_FirmwareVersion();
			
			test.T06_HardwareID();

			test.T07_SetDefaultState();

			test.T08_EnableOutput();

            test.T09_ControlConstants();

            test.T10_OperatingMode();

            test.T11_SensorType();

            test.T12_TemperatureAndResistance();

            test.T13_TecCurrent();

            test.T14_TecVoltage();

            test.T15_TecCurrentCompliance();

            test.T16_TimingComparision(true);

            test.T16_TimingComparision(false);

			// Shutdown
			test.ShutDown();

			// End
			return 0;
		}
	}
}
