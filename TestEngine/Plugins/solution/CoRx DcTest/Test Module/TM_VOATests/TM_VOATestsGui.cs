// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.TestModules
//
// TM_VOATestsGui.cs
//
// Author: dave.l.smith, 2014
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class TM_VOATestsGui6464 : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TM_VOATestsGui6464()
        {
            /* Call designer generated code. */
            InitializeComponent();
            lblMessage.Text = "";
            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Incoming message event handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ModuleGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(GuiMsg.CautionOfLaserRqst))
            {
                gbCaution.Visible = true;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(GuiMsg.CancelOflLaserCautionRqst))
            {
                gbCaution.Visible = false;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(DatumString))
            {
                DatumString msg = payload as DatumString;
                lblMessage.Text = msg.Value;
                lblMessage.Refresh();
            }
            else if (payload.GetType() == typeof(GuiMsg.VOASweepProgress))
            {
                GuiMsg.VOASweepProgress ProgressInfo = (GuiMsg.VOASweepProgress)payload;
                if (ProgressInfo.Progress == -1)
                {
                    VoltageProgress.Visible = false;
                    lblMessage.Text = "";
                }
                else
                {
                    if (!VoltageProgress.Visible)
                    {
                        VoltageProgress.Visible = true;
                        lblMessage.Text = ProgressInfo.SweepMsg;
                    }

                    VoltageProgress.Value = Math.Min(100, (int)ProgressInfo.Progress);
                }
            }

        }
    }
}
