﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Collections;

namespace SPC.Rule
{
    public class Identifier
    {
        public string SerialNumber;
        public string TestStage;
        public string DeviceType;
        public string NodeId;

        #region Properties
        private string time_from = DateTime.Now.ToString("yyyyMMdd000000");
        private string time_to = DateTime.Now.ToString("yyyyMMddHHmmss");


        public string TimeForm
        {
            get { return time_from; }
            set { if (CheckTimeValid(value)) time_from = value; }
        }

        public string TimeTo
        {
            get { return time_to; }
            set { if (CheckTimeValid(value))time_to = value; }
        }
        #endregion


        private bool CheckTimeValid(string str_time)
        {
            Regex reg = new Regex("^[0-9]{14}$");

            return reg.IsMatch(str_time);
        }
    }

    public class Element
    {
        public Element(string name, double value)
        {
            Name = name;
            Value = value;
        }
        public Element(string name, double value,string time)
        {
            Name = name;
            Value = value;
            this.TimeDate = time;
        }
        public Element(Element e)
        {
            Name = e.Name;
            Value = e.Value;
            TimeDate = e.TimeDate;
            OverRange = e.OverRange;
            Continuous = e.Continuous;
        }
        public string Name;
        public double Value;
        public bool OverRange;
        public bool Continuous;
        public string TimeDate;
    }

    public class ElementCollection : List<Element>, IComparable
    {
        private List<string> parameters;

        public ElementCollection() : base() { }
        public ElementCollection(int capacity) : base(capacity) { }
        public ElementCollection(IEnumerable<Element> collection) : base(collection) { }

        public string TimeDate { get; set; }

        public List<string> Parameters
        {
            get
            {
                if (parameters == null || parameters.Count != this.Count)
                {
                    parameters=new List<string> ();
                    this.ForEach(e => parameters.Add(e.Name));
                }

                return parameters; 
            }
        }

        #region Private Methods

        

        #endregion

        public int CompareTo(object obj)
        {
            long ths, lhs;

            if (long.TryParse(TimeDate, out ths) && long.TryParse((obj as ElementCollection).TimeDate, out lhs))
            {
                return -ths.CompareTo(lhs);
            }
            return 0;
        }
    }

    public class Resource
    {
        public ushort SIGMA_LEVEL = 3;

        private LimitDefine limits = null;

        public Resource(string name, List<ElementCollection> src, LimitDefine limits)
        {
            if (src == null || src.Count < 1) return;
            if(!src[0].Exists(e=>string.Equals(name,e.Name)))return;

            TestData = new Dictionary<string, Element>();
            src.ForEach(delegate(ElementCollection ec)
            {
                Element em = new Element(ec.Find(e => string.Equals(name, e.Name)));
                em.TimeDate = ec.TimeDate;
                TestData[ec.TimeDate] = em;

            });

            this.Name = name;

            this.limits = limits;


            UpdateData();
        }

        public string Name { get; set; }
        public Dictionary<string, Element> TestData { get; set; }

        public List<double> Values { get; private set; }
        public List<string> Times { get; private set; }

        #region Parameters helpful

        private double mean;
        private double upper;
        private double lower;


        public double Mean
        {
            get { return mean; }
            set { mean = value; }
        }
        public double UpperLimit
        {
            get { return upper; }
            set { upper = value; }
        }
        public double LowerLimit
        {
            get { return lower; }
            set { lower = value; }
        }

        #endregion


        #region Public Methods
        public void UpdateData()
        {
            if (Values == null) Values = new List<double>();
            if (Times == null) Times = new List<string>();

            Values.Clear();

            foreach (string item in TestData.Keys)
            {
                Times.Add(item);
                Values.Add(TestData[item].Value);
            }

           // Values = this.TestData.Values.GetEnumerator().ConvertAll(e => { return e.Value; });

            //this.mean = Definition.CalculateMean(Values);

            //double stdev = Definition.CalculateStdev(Values);

            //this.upper = mean + SIGMA_LEVEL * stdev;
            //this.lower = mean - SIGMA_LEVEL * stdev;

            this.mean = this.limits.Mean;
            this.lower = this.mean - this.limits.Stdev;
            this.upper = this.mean + this.limits.Stdev;
          
            //this.TestData.Values.ToList().ForEach(e => SetValueAttr(e));

            int i = 0;
            foreach (string item in TestData.Keys)
            {
                SetValueAttr(TestData[item], i++);
            }
        }



        #endregion


        private void SetValueAttr(Element e, int index)
        {
            e.OverRange = e.Value > upper || e.Value < lower;

            if (TestData.Count - index >= 7)
            {
                int rt = 0,lt = 0;
                for (int i = index; i < index + 7; i++)
                {
                    if (Values[i] > mean) rt++;
                    else if (Values[i] < mean) lt++;
                }

                e.Continuous = rt == 7 || lt == 7;
            }
        }

        

    }

    public class ResourceCollection : List<Resource>
    {
        public ResourceCollection() : base() { }
        public ResourceCollection(int capacity) : base(capacity) { }
        public ResourceCollection(IEnumerable<Resource> collection) : base(collection) { }

        public ResourceCollection(List<ElementCollection> src, List<LimitDefine> limits)
            : base()
        {
            if(src==null || src.Count<1)return;

            foreach (string item in src[0].Parameters)
            {
                this.Add(new Resource(item, src, limits.Find(delegate(LimitDefine def) { return def.Name == item; })));
            }
        }



    }


    public class Limits
    {
        public Hashtable LimitsDef
        {
            get;
            private set;
        }
        public Limits(string file)
        {
            List<String> content = FileAccess.ReadTxtByLine(file);
            if (content == null) return;

            LimitsDef = new   Hashtable  ();

            string[] headers = content[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string[] lines = new string[headers.Length];

            foreach (string item in content)
            {
                lines = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                Hashtable table = new Hashtable();
                int node = 0;
                if (int.TryParse(lines[0], out node))
                {
                    if (!LimitsDef.ContainsKey(node))
                        LimitsDef[node] = new List<LimitDefine>();

                    if (lines[1] == "Mean")
                    {
                        for (int i = 2; i < lines.Length; i++)
                        {
                            bool newLimit = false;
                            string name = headers[i];

                            LimitDefine def = (LimitsDef[node] as List<LimitDefine>). Find(delegate(LimitDefine d)
                            {
                                return d.Name == name;
                            });
                            if (def == null)
                            {
                                newLimit = true;
                                def  = new LimitDefine();
                                def.Node = node.ToString();
                                def.Name = name;
                            }

                            if (!double.TryParse(lines[i], out def.Mean))
                                def.Mean = 0;
                            if (newLimit)
                                (LimitsDef[node] as List<LimitDefine>).Add(def);

                        }
                    }
                    else if (lines[1] == "Range")
                    {
                        for (int i = 2; i < lines.Length; i++)
                        {
                            bool newLimit = false;
                            string name = headers[i];

                            LimitDefine def = (LimitsDef[node] as List<LimitDefine>).Find(delegate(LimitDefine d)
                            {
                                return d.Name == name;
                            });
                            if (def == null)
                            {
                                newLimit = true;
                                def = new LimitDefine();
                                def.Node = node.ToString();
                                def.Name = name;
                            }

                            if (!double.TryParse(lines[i], out def.Stdev))
                                def.Stdev = 999999;

                            if (newLimit)
                                (LimitsDef[node] as List<LimitDefine>).Add(def);
                        }
                    }
                }

            }

        }
    }

    public class LimitDefine
    {
        public double Mean;
        public double Stdev;
        public string Name;
        public string Node;
    }
}
