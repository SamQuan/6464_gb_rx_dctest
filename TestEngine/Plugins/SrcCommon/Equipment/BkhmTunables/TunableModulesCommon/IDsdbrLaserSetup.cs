using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.BlackBoxes
{
    /// <summary>
    /// Wrapper functions for the OIF Vendor registers common to FCU, WBTT and ITLA.
    /// See...
    /// "WBTT Release 3 OIF Regsiters"
    /// "FCU OIF Registers" by Richard Barlow.
    /// "ITLA OIF Registers"
    /// </summary>
    public interface IDsdbrLaserSetup
    {
        /// <summary>
        /// Initialise this instrument for calibration.
        /// Call before using other functions in this
        /// interface.
        /// </summary>
        void EnterLaserSetupMode();

        /// <summary>
        /// Get/set Settling time for settings
        /// </summary>
        int SettlingTime_ms
        {
            get; 
            set;
        }

        /// <summary>
        /// Gets the current SOA DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetSOADAC();

        /// <summary>
        /// Sets the SOA DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetSOADAC(int dac);

        /// <summary>
        /// Gets the current Gain DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetGainDAC();

        /// <summary>
        /// Sets the SOA DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetGainDAC(int dac);

        /// <summary>
        /// Gets the current front section pair (i and i+1) setting.
        /// </summary>
        /// <returns>Front section pair.</returns>
        int GetFrontSectionPair();

        /// <summary>
        /// Sets the current front section par (i and i+1) setting.
        /// </summary>
        /// <param name="i">New front section pair.</param>
        void SetFrontSectionPair(int i);

        /// <summary>
        /// Gets the current Front Even DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetFrontEvenDAC();

        /// <summary>
        /// Sets the Front Even DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetFrontEvenDAC(int dac);

        /// <summary>
        /// Gets the current Front Odd DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetFrontOddDAC();

        /// <summary>
        /// Sets the Front Odd DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetFrontOddDAC(int dac);

        /// <summary>
        /// Gets the current Rear DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetRearDAC();

        /// <summary>
        /// Sets the Rear DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetRearDAC(int dac);

        /// <summary>
        /// Gets the current Phase DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetPhaseDAC();

        /// <summary>
        /// Sets the Phase DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetPhaseDAC(int dac);

        /// <summary>
        /// Gets the current Mode Centre Phase DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetModeCentrePhaseDAC();

        /// <summary>
        /// Sets the Mode Centre Phase DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetModeCentrePhaseDAC(int dac);

        /// <summary>
        /// Gets the current BOL Phase DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetBOLPhaseDAC();

        /// <summary>
        /// Sets the BOL Phase DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetBOLPhaseDAC(int dac);

        /// <summary>
        /// Gets the current Dither DAC reading.
        /// </summary>
        /// <returns>DAC value.</returns>
        int GetDitherDAC();

        /// <summary>
        /// Sets the Dither DAC.
        /// </summary>
        /// <param name="dac">New DAC value.</param>
        void SetDitherDAC(int dac);

        /// <summary>
        /// Save channel settings.
        /// </summary>
        /// <param name="chan">Channel number.</param>
        void SaveChannel(int chan);

        /// <summary>
        /// Start the AutoLocker.
        /// </summary>
        void DoAutoLockerSetup();

        /// <summary>
        /// Get locker slope state. +ve or -ve.
        /// </summary>
        /// <returns>True if +ve. False if -ve.</returns>
        bool GetLockerSlopePositive();

        /// <summary>
        /// Set locker slope state.
        /// </summary>
        /// <param name="positive">True for +ve. False for -ve.</param>
        void SetLockerSlopePositive(bool positive);

        /// <summary>
        /// Gets Tx Coarse Pot value.
        /// </summary>
        /// <returns>Pot value.</returns>
        int GetTxCoarsePot();

        /// <summary>
        /// Set Tx Coarse Pot value.
        /// </summary>
        /// <param name="pot">New Pot value.</param>
        void SetTxCoarsePot(int pot);

        /// <summary>
        /// Gets Ref Coarse Pot value.
        /// </summary>
        /// <returns>Pot value.</returns>
        int GetRefCoarsePot();

        /// <summary>
        /// Set Ref Coarse Pot value.
        /// </summary>
        /// <param name="rcp">New Pot value.</param>
        void SetRefCoarsePot(int rcp);

        /// <summary>
        /// Get power mon pot.
        /// </summary>
        /// <returns>Pot value.</returns>
        int GetPowerMonPot();

        /// <summary>
        /// Set power mon pot.
        /// </summary>
        /// <param name="pot">New Pot value.</param>
        void SetPowerMonPot(int pot);


        /// <summary>
        /// Get locker error factor.
        /// </summary>
        /// <returns>Factor.</returns>
        int GetLockerErrorFactor();

        /// <summary>
        /// Set locker error factor.
        /// </summary>
        /// <param name="lef">New factor.</param>
        void SetLockerErrorFactor(int lef);

        /// <summary>
        /// Gets locker on/off state.
        /// </summary>
        /// <returns>True if on, false if off.</returns>
        bool GetLockerOn();
        
        /// <summary>
        /// Sets locker on/off state.
        /// </summary>
        /// <param name="on">True for on, false for off.</param>
        void SetLockerOn(bool on);

        /// <summary>
        /// Gets Locker Tx Mon ADC.
        /// </summary>
        /// <returns>ADC value.</returns>
        int GetLockerTxMon_ADC();

        /// <summary>
        /// Gets Locker Ref Mon ADC.
        /// </summary>
        /// <returns>ADC value.</returns>
        int GetLockerRefMon_ADC();

        /// <summary>
        /// Gets laser Power Mon ADC.
        /// </summary>
        /// <param name="n">Number of times to repeat request.</param>
        /// <returns>Average ADC value.</returns>
        int GetLaserPowerMon_ADC(int n);

        /// <summary>
        /// Get Ls Phase Sense value.
        /// </summary>
        /// <returns>Read value.</returns>
        int GetLsPhaseSense();

        /// <summary>
        /// Gets minimum ADC for the EOL Phase.
        /// </summary>
        /// <returns>ADC limit.</returns>
        int	GetEOLPhaseADC_Min();

        /// <summary>
        /// Sets minimum ADC for the EOL Phase.
        /// </summary>
        /// <param name="pmin">ADC limit.</param>
    	void SetEOLPhaseADC_Min(int pmin);

        /// <summary>
        /// Gets maximum ADC for the EOL Phase.
        /// </summary>
        /// <returns>ADC limit.</returns>
        int GetEOLPhaseADC_Max();

        /// <summary>
        /// Sets maximum ADC for the EOL Phase.
        /// </summary>
        /// <param name="pmin">ADC limit.</param>
        void SetEOLPhaseADC_Max(int pmax);

        /// <summary>
        /// Get Power Mon Factor.
        /// </summary>
        /// <returns>Power Mon Factor.</returns>
        int GetPowerMonFactor();

        /// <summary>
        /// Set Power Mon Factor.
        /// </summary>
        /// <param name="pmf">Power Mon Factor.</param>
        void SetPowerMonFactor(int pmf);

        /// <summary>
        /// Get Power Mon Offset.
        /// </summary>
        /// <returns>Power Mon Offset.</returns>
        int GetPowerMonOffset();

        /// <summary>
        /// Set Power Mon Offset.
        /// </summary>
        /// <param name="pmo">Power Mon Offset.</param>
        void SetPowerMonOffset(int pmo);

        /// <summary>
        /// Get SBS Dither multiplier.
        /// </summary>
        /// <returns>SBS Dither multiplier.</returns>
        int GetSBSDitherMultiplier();

        /// <summary>
        /// Set SBS Dither multiplier.
        /// </summary>
        /// <param name="mult">SBS Dither multiplier.</param>
        void SetSBSDitherMultiplier(int mult);

        /// <summary>
        /// Gets SBS Dither state.
        /// </summary>
        /// <returns>True if on, false if off.</returns>
        bool GetSBSDitherOn();

        /// <summary>
        /// Sets SBS Dither state.
        /// </summary>
        /// <param name="on">True for on, false for off.</param>
        void SetSBSDitherOn(bool on);

        /// <summary>
        /// Gets Power Profiling state.
        /// </summary>
        /// <returns>True for on, false for off.</returns>
        bool GetPowerProfilingOn();

        /// <summary>
        /// Set Power Profiling state.
        /// </summary>
        /// <param name="on">True for on, false for off.</param>
	    void SetPowerProfilingOn(bool on);

        /// <summary>
        /// Gets Power Control Loop state.
        /// </summary>
        /// <returns>True for on, false for off.</returns>
        bool GetPowerControlLoopOn();

        /// <summary>
        /// Set Power Control Loop state.
        /// </summary>
        /// <param name="on">True for on, false for off.</param>
        void SetPowerControlLoopOn(bool enabled);
    }
}
