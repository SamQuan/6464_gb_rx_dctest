// [Copyright]
//
// Bookham Modular Test Engine
// FactoryWorks MES Plug-in
//
// Library/MES/FactoryWorks/FactoryWorksConfig.cs
// 
// Author: Blair Harris
// Design: As specified in FactoryWorks MES Plug-In DD

using System;
using System.Xml;
using System.Data;
using Bookham.TestEngine.PluginInterfaces.MES;
using Bookham.TestEngine.FactoryWorks;



namespace Bookham.TestLibrary.MES
{

	/// <summary>
	/// Class to read and access config for the FactoryWorks plug-in
	/// </summary>
	internal sealed class FactoryWorksConfig
	{

		#region public methods
		/// <summary>
		/// Default constructor
		/// </summary>
		internal FactoryWorksConfig()
		{
			this.configFilePath = "./Configuration/FactoryWorksConfig.xml";
		}

		/// <summary>
		/// Constructor specifying a path to the config file
		/// </summary>
		/// <param name="configFilePath"></param>
		internal FactoryWorksConfig( string configFilePath )
		{
			this.configFilePath = configFilePath;
		}

		/// <summary>
		/// Reads the FactoryWorks Configuration from the file FactoryWorksConfig.xml
		/// </summary>
		internal void LoadConfiguration()
		{
			// Instantiate the FactoryWorks Configuration typed dataset, and then populate it from
			// the FactoryWorksConfig.xml configuration file, which is required to be stored in the ./Configuration
			// directory, relative to the TestEngine executable.
			FactoryWorksConfigSchema factoryWorksConfigDataSet = new FactoryWorksConfigSchema();

			// Catch any exceptions that may be generated due to being unable to access
			// the FactoryWorksConfig.xml file.
			try 
			{
				factoryWorksConfigDataSet.ReadXml( this.configFilePath, XmlReadMode.ReadSchema );

				this.node = factoryWorksConfigDataSet.FactoryWorksConfig[0].Node;
				if( this.node < 0 ) throw new MESFatalErrorException( "Node must be positive" );
				this.requestDir = factoryWorksConfigDataSet.FactoryWorksConfig[0].RequestDir;
				this.responseDir = factoryWorksConfigDataSet.FactoryWorksConfig[0].ResponseDir;

				FactoryWorksConfigSchema.LoadBatchRow[] loadBatchRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetLoadBatchRows();
				this.loadBatch_Response.WaitForResponse = true;
				this.loadBatch_Response.Timeout_s = loadBatchRow[0].Timeout_s;
				if( this.loadBatch_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );

				FactoryWorksConfigSchema.TrackInRow[] trackInRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetTrackInRows();
				this.trackIn_Response.WaitForResponse = trackInRow[0].WaitForResponse;
				this.trackIn_Response.Timeout_s = trackInRow[0].Timeout_s;
				if( this.trackIn_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );

				FactoryWorksConfigSchema.SetAttributeRow[] setAttribRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetSetAttributeRows();
				this.setAttribute_Response.WaitForResponse = setAttribRow[0].WaitForResponse;
				this.setAttribute_Response.Timeout_s = setAttribRow[0].Timeout_s;
				if( this.setAttribute_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );

				FactoryWorksConfigSchema.MarkAsDefectiveRow[] markAsDefRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetMarkAsDefectiveRows();
				this.markAsDefective_Response.WaitForResponse = markAsDefRow[0].WaitForResponse;
				this.markAsDefective_Response.Timeout_s = markAsDefRow[0].Timeout_s;
				if( this.markAsDefective_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );

				FactoryWorksConfigSchema.TrackOutComponentPassReworkOnHoldRow[] trackOutCompRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetTrackOutComponentPassReworkOnHoldRows();
				this.trackOutComponent_PassReworkOnHold_Response.WaitForResponse = trackOutCompRow[0].WaitForResponse;
				this.trackOutComponent_PassReworkOnHold_Response.Timeout_s = trackOutCompRow[0].Timeout_s;
				if( this.trackOutComponent_PassReworkOnHold_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );

				FactoryWorksConfigSchema.TrackOutComponentFailRow[] trackOutCompFailRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetTrackOutComponentFailRows();
				this.trackOutComponent_Fail_Response.WaitForResponse = trackOutCompFailRow[0].WaitForResponse;
				this.trackOutComponent_Fail_Response.Timeout_s = trackOutCompFailRow[0].Timeout_s;
				if( this.trackOutComponent_Fail_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );

				FactoryWorksConfigSchema.TrackOutBatchPassOnholdRow[] trackOutBatchRow = factoryWorksConfigDataSet.FactoryWorksConfig[0].GetTrackOutBatchPassOnholdRows();
				this.trackOutBatch_PassOnHold_Response.WaitForResponse = trackOutBatchRow[0].WaitForResponse;
				this.trackOutBatch_PassOnHold_Response.Timeout_s = trackOutBatchRow[0].Timeout_s;
				if( this.trackOutBatch_PassOnHold_Response.Timeout_s < 0 ) throw new MESFatalErrorException( "All timeouts must be positive" );			
			}
			catch( System.Exception e )
			{
				throw new MESFatalErrorException( "Unable to open FactoryWorks Configuration file: " + this.configFilePath, e );
			}
		}
		#endregion
		
		#region Public Properties
		/// <summary>
		/// Node number
		/// </summary>
		internal int Node
		{
			set
			{
				this.node = value;
			}
			get
			{
				return this.node;
			}
		}

		/// <summary>
		/// Request drop folder
		/// </summary>
		internal string RequestDir
		{
			get
			{
				return this.requestDir;
			}
		}

		/// <summary>
		/// Response folder
		/// </summary>
		internal string ResponseDir
		{
			get
			{
				return this.responseDir;
			}
		}




		/// <summary>
		/// Load batch response config
		/// </summary>
		internal ResponseConfig LoadBatchResponse
		{
			get
			{
				return this.loadBatch_Response;
			}
		}

		/// <summary>
		/// Track-in response config
		/// </summary>
		internal ResponseConfig TrackInResponse
		{
			get
			{
				return this.trackIn_Response;
			}
		}

		/// <summary>
		/// Set attribute response config
		/// </summary>
		internal ResponseConfig SetAttributeResponse
		{
			get
			{
				return this.setAttribute_Response;
			}
		}

		/// <summary>
		/// Mark as defective response config
		/// </summary>
		internal ResponseConfig MarkAsDefectiveResponse
		{
			get
			{
				return this.markAsDefective_Response;
			}
		}

		/// <summary>
		/// Track-out component for pass/rework/onhold response config
		/// </summary>
		internal ResponseConfig TrackOutComponentPassReworkOnHoldResponse
		{
			get
			{
				return this.trackOutComponent_PassReworkOnHold_Response;
			}
		}

		/// <summary>
		/// Track-out component fail response config
		/// </summary>
		internal ResponseConfig TrackOutComponentFailResponse
		{
			get
			{
				return this.trackOutComponent_Fail_Response;
			}
		}

		/// <summary>
		/// Track-out batch response config
		/// </summary>
		internal ResponseConfig TrackOutBatchResponse
		{
			get
			{
				return this.trackOutBatch_PassOnHold_Response;
			}
		}
		#endregion

		#region Private Data
		/// <summary>Config file pathname</summary>
		private string configFilePath;

		/// <summary>Node</summary>
		private int node;
		/// <summary>Request drop folder</summary>
		private string requestDir;
		/// <summary>Response folder</summary>
		private string responseDir;

		private ResponseConfig loadBatch_Response;
		private ResponseConfig trackIn_Response;
		private ResponseConfig setAttribute_Response;
		private ResponseConfig markAsDefective_Response;
		private ResponseConfig trackOutComponent_PassReworkOnHold_Response;
		private ResponseConfig trackOutComponent_Fail_Response;
		private ResponseConfig trackOutBatch_PassOnHold_Response;
		#endregion
	}

}
