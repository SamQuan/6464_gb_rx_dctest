// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Inst_Ke24xx.cs
//
// Author: T Foster
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisTypes;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Algorithms;
using System.Collections.Generic;
using System.Threading;



namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument driver for Keithley 24xx SMU
    /// Provides basic source functions only for 'ElectricalSource' instrument type
    /// </summary>    
    public class Inst_Ke24xx : InstType_TriggeredElectricalSource, IInstType_DigiIOCollection
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="instrumentName">Instrument name</param>
        /// <param name="driverName">Instrument driver name</param>
        /// <param name="slotId">Slot ID for the instrument</param>
        /// <param name="subSlotId">Sub Slot ID for the instrument</param>
        /// <param name="chassis">Chassis through which the instrument communicates</param>
        public Inst_Ke24xx(string instrumentName, string driverName, string slotId, string subSlotId, Chassis chassis)
            : base(instrumentName, driverName, slotId, subSlotId, chassis)
        {
            // Configure valid hardware information

            // Add Ke2400 details
            InstrumentDataRecord ke2400Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2400",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C50");											// maximum valid firmware version 
            ke2400Data.Add("MaxCurrentLimitAmp", "1.05");		// maximum current limit
            ke2400Data.Add("MaxVoltageLimit", "210.0");			// maximum voltage limit
            ke2400Data.Add("MaxVoltageLimitLV", "21.0");		// maximum voltage limit if LV 
            ke2400Data.Add("IsLVUnit", "true");					// Indicates this is an LV (low voltage) unit
            // Set this parameter false for standard units
            ke2400Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2400Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2400Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2400Data.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2400", ke2400Data);

            // Add Ke2400C details
            InstrumentDataRecord ke2400CData = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2400-C",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C39");											// maximum valid firmware version 
            ke2400CData.Add("MaxCurrentLimitAmp", "1.05");		// maximum current limit
            ke2400CData.Add("MaxVoltageLimit", "210.0");			// maximum voltage limit
            ke2400CData.Add("MaxVoltageLimitLV", "21.0");		// maximum voltage limit if LV 
            ke2400CData.Add("IsLVUnit", "true");					// Indicates this is an LV (low voltage) unit
            // Set this parameter false for standard units
            ke2400CData.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2400CData.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2400CData.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2400CData.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2400C", ke2400CData);


            // Add Ke2410 details
            InstrumentDataRecord ke2410Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2410",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C22");											// maximum valid firmware version 
            ke2410Data.Add("MaxCurrentLimitAmp", "1.05");		// maximum current limit
            ke2410Data.Add("MaxVoltageLimit", "1100.0");		// maximum voltage limit
            ke2410Data.Add("MaxVoltageLimitLV", "1100.0");		// LV mode not supported
            ke2410Data.Add("IsLVUnit", "false");
            ke2410Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2410Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2410Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2410Data.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2410", ke2410Data);

            // Add Ke2420 details
            InstrumentDataRecord ke2420Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2420",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C22");											// maximum valid firmware version 
            ke2420Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
            ke2420Data.Add("MaxVoltageLimit", "63.0");			// maximum voltage limit
            ke2420Data.Add("MaxVoltageLimitLV", "63.0");		// LV mode not supported
            ke2420Data.Add("IsLVUnit", "false");
            ke2420Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2420Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2420Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2420Data.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2420", ke2420Data);

            // Add Ke2430 details
            InstrumentDataRecord ke2430Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2430",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C22");											// maximum valid firmware version 
            ke2430Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
            ke2430Data.Add("MaxVoltageLimit", "105.0");		// maximum voltage limit
            ke2430Data.Add("MaxVoltageLimitLV", "105.0");		// LV mode not supported
            ke2430Data.Add("IsLVUnit", "false");
            ke2430Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2430Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2430Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2430Data.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2430", ke2430Data);

            // Add Ke2440 details
            InstrumentDataRecord ke2440Data = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2440",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C30");											// maximum valid firmware version 
            ke2440Data.Add("MaxCurrentLimitAmp", "3.15");		// maximum current limit
            ke2440Data.Add("MaxVoltageLimit", "105.0");		// maximum voltage limit
            ke2440Data.Add("MaxVoltageLimitLV", "105.0");		// LV mode not supported
            ke2440Data.Add("IsLVUnit", "false");
            ke2440Data.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2440Data.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2440Data.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2440Data.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2440", ke2440Data);

            // Add Ke2440c details
            InstrumentDataRecord ke2440cData = new InstrumentDataRecord(
                "KEITHLEY INSTRUMENTS INC. MODEL 2440-C",			// hardware name 
                "C00",											// minimum valid firmware version 
                "C30");											// maximum valid firmware version 
            ke2440cData.Add("MaxCurrentLimitAmp", "5.25");		// maximum current limit
            ke2440cData.Add("MaxVoltageLimit", "105.0");		// maximum voltage limit
            ke2440cData.Add("MaxVoltageLimitLV", "105.0");		// LV mode not supported
            ke2440cData.Add("IsLVUnit", "false");
            ke2440cData.Add("LineFrequency_Hz", "50");			// line frequency. default 50Hz
            ke2440cData.Add("OutputOffState", "norm");			// Output off state : norm, himp, zero, guard
            ke2440cData.Add("TerminalsFrontRear", "rear");		// Terminal connections front, rear
            ke2440cData.Add("4wireSense", "true");				// Use 4 wire sensing false (default) or true 
            ValidHardwareData.Add("Ke2440c", ke2440cData);


            // Configure valid chassis information
            // Add 24xx chassis details
            InstrumentDataRecord ke24xxChassisData = new InstrumentDataRecord(
                "Chassis_Ke24xx",								// chassis driver name  
                "0",											// minimum valid chassis driver version  
                "10.0.0.0");										// maximum valid chassis driver version
            ValidChassisDrivers.Add("Chassis_Ke24xx", ke24xxChassisData);

            // Initialise the local chassis reference cast to the actual chassis type
            instrumentChassis = (Chassis_Ke24xx)base.InstrumentChassis;

            this.Timeout_ms = 2000;
            instrumentChassis.Timeout_ms = 2000;
            // Set to a known state
            //instrumentChassis.Clear();

            // Add digital IO objects
            digiOutLines = new List<IInstType_DigitalIO>(4);
            for (int ii = 1; ii <= 4; ii++)
            {
                digiOutLines.Add(new Inst_Ke24xx_DigiOutLine(instrumentName + "_D" + ii, instrumentChassis, ii));
            }
        }


        #region Public Instrument Methods and Properties


        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                string hid = instrumentChassis.HardwareIdentity;
                // Log event 
                LogEvent("'HardwareIdentity' returned : " + hid);

                return hid;
            }
        }

        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                string fv = instrumentChassis.FirmwareVersion;
                // Log event 
                LogEvent("'FirmwareVersion' returned : " + fv);

                // Return 
                return fv;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            // Clear any pending operations
            instrumentChassis.Clear();
        }


        /// <summary>
        /// Get/Set instrument online status
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                base.IsOnline = value;
                foreach (IInstType_DigitalIO dio in digiOutLines)
                {
                    dio.IsOnline = value;
                }
            }
        }

        private void Beep(int tone_Hz, int duration_ms)
        {
            if (tone_Hz != 0)
                instrumentChassis.Write(":SYST:BEEP:IMM " + tone_Hz + "," + (Single)(duration_ms / 100), this);
            System.Threading.Thread.Sleep(duration_ms);
        }

        /// <summary>
        /// Configures the instrument into a default state.
        /// 
        /// </summary>
        public override void SetDefaultState()
        {
            // Clear any pending operations
            instrumentChassis.Clear();

            // Set factory default
            instrumentChassis.Write("*RST", this);

            // Set line frequency
            instrumentChassis.Write(":SYST:LFR " + HardwareData["LineFrequency_Hz"], this);

            // Set line output off state
            instrumentChassis.Write(":OUTP:SMOD " + HardwareData["OutputOffState"].ToUpper(), this);

            // Set line output terminals (front or rear)
            instrumentChassis.Write(":ROUT:TERM " + HardwareData["TerminalsFrontRear"].ToUpper(), this);

            // Set 4 wire sense
            if (HardwareData["4wireSense"].ToLower() == "true")
            {
                instrumentChassis.Write(":SYST:RSEN 1", this);
            }

            // Beeper off
            instrumentChassis.Write(":SYST:BEEP:STAT OFF", this);
        }

        #endregion

        #region Public ElectricalSource InstrumentType Methods and Properties

        /// <summary>
        /// Sets/returns the output state
        /// </summary>
        public override bool OutputEnabled
        {
            get
            {
                // Query the output state
                string rtn = instrumentChassis.Query(":OUTP:STAT?", this);

                // Return bool value
                return (rtn == trueStr);
            }
            set
            {
                // Convert bool value to string
                string boolVal = value ? trueStr : falseStr;

                // Set output
                instrumentChassis.Write(":*CLS;:OUTP:STAT " + boolVal, this);
            }
        }
        /// <summary>
        /// select rear or front panel
        /// </summary>
        public bool SelectRearTerminals
        {
            set
            {
                if (value)
                    instrumentChassis.Write_Unchecked(":ROUT:TERM REAR", this);
                else
                    instrumentChassis.Write_Unchecked(":ROUT:TERM FRONT", this);
            }

            get
            {
                string rsp = instrumentChassis.Query_Unchecked(":ROUT:TERM?", this);
                return rsp.Contains("REAR");
            }
        }

        /// <summary>
        ///  Sets/returns the current level set point
        /// </summary>
        public override double CurrentSetPoint_amp
        {
            get
            {
                // Make sure we are in current mode
                string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
                if (mode.ToLower() != "curr")
                {
                    throw new InstrumentException("Attempting to read output current set point when not in Current Mode.");
                }

                // Get current source set level in amps
                //string rtn = instrumentChassis.Query(":SENS:CURR:LEV?", this); // Incorrect - MF
                string rtn = instrumentChassis.Query(":SOUR:CURR:LEV:AMPL?", this);


                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Make sure we are in current mode
                string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
                if (mode.ToLower() != "curr")
                {
                    // Is output on?
                    bool outputEnabled = OutputEnabled;

                    // Set function to current. this disabled the output if it is on
                    instrumentChassis.Write(":SOUR:FUNC CURR", this);

                    // set output back on if needed
                    if (outputEnabled) OutputEnabled = true;
                }

                // Set current source level in amps
                //instrumentChassis.Write(":SOUR:CURR:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:CURR:LEV:IMM:AMPL " + value.ToString().Trim(), this);    // IMMEDIATE
            }
        }

        /// <summary>
        /// Returns the actual measured current level 
        /// </summary>
        public override double CurrentActual_amp
        {
            get
            {
                //instrumentChassis.Write_Unchecked(":SYST:AZER:STAT ON", this);
                //instrumentChassis.Write_Unchecked(":FORM ASC", this);
                instrumentChassis.Write_Unchecked("*CLS", this);
                // Read current in amps
                //instrumentChassis.Write_Unchecked(":ARM:COUNT 1", this);

                // Replaced Query with Write/Wait/Read 
                //instrumentChassis.Write_Unchecked(":MEAS:CURR?", this);
                //System.Threading.Thread.Sleep(30);
                //string[] meas = instrumentChassis.Read_Unchecked(this).Split(',');
                string[] meas = instrumentChassis.Query(":MEAS:CURR?", this).Split(',');
                // If only a single element is returned then use it, otherwire CURR will be the second. 
                int index = meas.Length == 1 ? 0 : 1;
                double rtn = 0;

                try
                {
                    rtn = Convert.ToDouble(meas[index]);
                }
                catch (Exception ex)
                {
                    if (!meas[index].StartsWith("#"))
                        throw ex;
                }

                //if ( ! m_bAutoZero )
                //instrumentChassis.Write_Unchecked(":SYST:AZER:STAT OFF",this);

                // Return
                return rtn;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dSenseCurrentRange"></param>
        public void Set_Isense_Range(double dSenseCurrentRange)
        {
            if (dSenseCurrentRange < -1.05 || dSenseCurrentRange > 1.05)
            {
                throw new InstrumentException("K2400::Set_Isense_Range - Supplied Value of Sense Current is Out of Range");
            }
            instrumentChassis.Write_Unchecked(string.Format(":CURR:RANG:AUTO OFF;:CURR:RANG {0}", dSenseCurrentRange), this);
        }
        /// <summary>
        /// Sets/returns the compliance current setpoint
        /// Compliance current being the maximum positive or negative current the instrument will supply
        /// </summary>
        public override double CurrentComplianceSetPoint_Amp
        {
            get
            {
                // Read compliance current in amps
                string rtn = instrumentChassis.Query(":SENS:CURR:PROT:LEV?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Check value with compliance
                double compCurr = Convert.ToDouble(HardwareData["MaxCurrentLimitAmp"]);
                if (Math.Abs(value) > compCurr)
                {
                    throw new InstrumentException("Set value level '" + value.ToString() +
                        "' is greater than maximum current '" + compCurr.ToString() + "'");
                }

                // Set current in amps
                instrumentChassis.Write(":SENS:CURR:PROT:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:CURR:RANG " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/returns the voltage setpoint level
        /// </summary>
        public override double VoltageSetPoint_Volt
        {
            get
            {
                // Make sure we are in voltage mode
                string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
                if (mode.ToLower() != "volt")
                {
                    throw new InstrumentException("Attempting to read output voltage set point when not in Voltage Mode.");
                }

                // Get voltage source set level in volts
                //string rtn = instrumentChassis.Query(":SENS:VOLT:LEV?", this); // This is incorrect - MF
                string rtn = instrumentChassis.Query(":SOUR:VOLT:LEV:AMPL?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Make sure we are in voltage mode
                string mode = instrumentChassis.Query(":SOUR:FUNC?", this);
                if (mode.ToLower() != "volt")
                {
                    // Is output on?
                    bool outputEnabled = OutputEnabled;

                    // Set function to volt. this disabled the output if it is on
                    instrumentChassis.Write(":SOUR:FUNC VOLT", this);

                    // set output back on if needed
                    if (outputEnabled) OutputEnabled = true;
                }

                // Set voltage level
                //instrumentChassis.Write(":SOUR:VOLT:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:VOLT:LEV:IMM:AMPL " + value.ToString().Trim(), this); // apply immediately
            }
        }


        /// <summary>
        ///  Returns the measured voltage output level
        /// </summary>
        public override double VoltageActual_Volt
        {
            get
            {
                // Read voltage in volts
                string[] meas = instrumentChassis.Query(":MEAS:VOLT?", this).Split(',');

                // Get the 1st value and convert to double
                double rtn = Convert.ToDouble(meas[0]);

                // Return
                return rtn;
            }
        }


        /// <summary>
        /// Sets/returns the compliance voltage set point
        /// Compliance voltage being the maximum positive or negative voltage the instrument will supply
        /// </summary>
        public override double VoltageComplianceSetPoint_Volt
        {
            get
            {
                // Read current in volts
                string rtn = instrumentChassis.Query(":SENS:VOLT:PROT:LEV?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }

            set
            {
                // Determine compliance voltage
                // Some units are low voltage (LV) which have their top range disabled
                double compVolt;
                if (HardwareData["IsLVUnit"].ToLower() == "true")
                {
                    compVolt = Convert.ToDouble(HardwareData["MaxVoltageLimitLV"]);
                }
                else
                {
                    compVolt = Convert.ToDouble(HardwareData["MaxVoltageLimit"]);
                }

                if (Math.Abs(value) > compVolt)
                {
                    throw new InstrumentException("Set value level '" + value.ToString() +
                        "' is greater than maximum voltage possible '" + compVolt.ToString() + "'");
                }

                // Set voltage
                instrumentChassis.Write(":SENS:FUNC 'VOLT:DC'", this);
                instrumentChassis.Write(":SENS:VOLT:PROT:LEV " + value.ToString().Trim(), this);
                instrumentChassis.Write(":SOUR:VOLT:RANG " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Set the source mode to be fixed current 
        /// </summary>
        public override void SourceFixedCurrent()
        {
            instrumentChassis.Write(":SOUR:FUNC CURR", this);
            instrumentChassis.Write(":SOUR:CURR:MODE FIX", this);
        }

        /// <summary>
        /// Set the source mode to be fixed voltage 
        /// </summary>
        public override void SourceFixedVoltage()
        {
            instrumentChassis.Write(":SOUR:FUNC VOLT", this);
            instrumentChassis.Write(":SOUR:VOLT:MODE FIX", this);
        }

        /// <summary>
        /// Set the source mode to sweep voltage
        /// </summary>
        public void SourceSweptVoltage()
        {

            instrumentChassis.Write(":SOUR:FUNC VOLT", this);
            instrumentChassis.Write(":SOUR:VOLT:MODE SWE", this);

        }
        ///<summary>
        /// Configure sweep reading parameters
        /// </summary>

        public override void ConfigureCurrentSweepReadings()
        {
            instrumentChassis.Write(":SOUR:FUNC CURR", this);
            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
        }

        /// <summary>
        /// Set / Get to indicate if we are using 4 wire sense
        /// </summary>
        public bool FourWireSense
        {
            get
            {
                string rtn = instrumentChassis.Query(":SYST:RSEN?", this);
                // Return bool value
                return (rtn == trueStr);
            }
            set
            {
                // Convert bool value to string
                string boolVal = value ? trueStr : falseStr;

                // Set FourWireSense
                instrumentChassis.Write(":SYST:RSEN " + boolVal, this);
            }
        }

 


        /// <summary>
        /// Set / Get to indicate if we are using the front terminals,
        /// (of course if we aren't then we must be using the rear terminals)
        /// </summary>
        public bool UseFrontTerminals
        {
            get
            {
                string rtn = instrumentChassis.Query(":ROUT:TERM?", this);
                // Return bool value
                return (rtn == "FRON");
            }
            set
            {
                string terminals = value ? "FRON" : "REAR";
                instrumentChassis.Write(":ROUT:TERM " + terminals, this);
            }
        }


        /// <summary>
        /// Source a swept current
        /// </summary>
        /// <param name="Imin_amp">The value we are seeping from</param>
        /// <param name="Imax_amp">The value we are seeping to</param>
        /// <param name="numPts">The number of points we will be sweeping over</param>
        public override void SourceSweptCurrent(double Imin_amp, double Imax_amp, int numPts)
        {
            double Istep_A = (double)(Imax_amp - Imin_amp) / (numPts - 1);
            instrumentChassis.Write_Unchecked("*CLS", this);
            instrumentChassis.Write_Unchecked(":SOUR:CURR:STAR " + Imin_amp, this);
            instrumentChassis.Write_Unchecked(":SOUR:CURR:STOP " + Imax_amp, this);
            instrumentChassis.Write_Unchecked(":SOUR:CURR:STEP " + Istep_A, this);
            instrumentChassis.Write_Unchecked(":SOUR:SWE:POIN " + numPts, this);
            instrumentChassis.Write_Unchecked(":SOUR:CURR:MODE SWE", this);
            // Force autozero off for a big speedup
            instrumentChassis.Write_Unchecked(":SYST:AZER:STAT OFF", this);
        }
        /// <summary>
        /// Sets/Gets the mode of Source Function
        /// </summary>
        public SourceFunctionItem SourceFunction
        {
            get
            {
                string rtnString = instrumentChassis.Query_Unchecked(":SOUR:FUNC:MODE?", this);
                SourceFunctionItem rtnFunction;
                switch (rtnString)
                {
                    case "VOLT":
                        rtnFunction = SourceFunctionItem.Voltage;
                        break;
                    case "CURR":
                        rtnFunction = SourceFunctionItem.Current;
                        break;
                    case "MEM":
                        rtnFunction = SourceFunctionItem.Memory;
                        break;
                    default:
                        throw new InstrumentException("Unknown Source Function Type for KE24xx!");
                }

                return rtnFunction;
            }
            set
            {
                string command;
                switch (value)
                {
                    case SourceFunctionItem.Voltage:
                        command = "VOLT";
                        break;
                    case SourceFunctionItem.Current:
                        command = "CURR";
                        break;
                    case SourceFunctionItem.Memory:
                        command = "MEM";
                        break;
                    default:
                        throw new InstrumentException("Unknown Source Function Type for KE24xx!");
                }
                //send(":SOUR:FUNC VOLT");
                instrumentChassis.Write(":SOUR:FUNC " + command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public SenseFunctionItem SenseFunction
        {
            get
            {
                string rtnString = instrumentChassis.Query_Unchecked(":SENS:FUNC:ON?", this);
                SenseFunctionItem rtnFunction;
                switch (rtnString)
                {
                    case "\"CURR:DC\"":
                        rtnFunction = SenseFunctionItem.Current;
                        break;
                    case "\"VOLT:DC\"":
                        rtnFunction = SenseFunctionItem.Voltage;
                        break;
                    case "\"RES\"":
                        rtnFunction = SenseFunctionItem.Resistance;
                        break;
                    case "\"VOLT:DC\",\"CURR:DC\"":
                        rtnFunction = SenseFunctionItem.VoltageCurrent;
                        break;
                    case "\"VOLT:DC\",\"RES\"":
                        rtnFunction = SenseFunctionItem.VoltageResistance;
                        break;
                    case "\"CURR:DC\",\"RES\"":
                        rtnFunction = SenseFunctionItem.CurrentResistance;
                        break;
                    case "\"VOLT:DC\",\"CURR:DC\",\"RES\"":
                        rtnFunction = SenseFunctionItem.VoltageCurrentResistance;
                        break;
                    default:
                        throw new InstrumentException("Unknown Sense Function Type of KE24xx!!!");
                }

                return rtnFunction;
            }
            set
            {
                string command;
                switch (value)
                {
                    case SenseFunctionItem.Voltage:
                        command = "'VOLT:DC'";
                        break;
                    case SenseFunctionItem.Current:
                        command = "'CURR:DC'";
                        break;
                    case SenseFunctionItem.Resistance:
                        command = "'RES'";
                        break;
                    case SenseFunctionItem.VoltageCurrent:
                        command = "'VOLT','CURR'";
                        break;
                    case SenseFunctionItem.CurrentResistance:
                        command = "'CURR','RES'";
                        break;
                    case SenseFunctionItem.VoltageResistance:
                        command = "'VOLT','RES'";
                        break;
                    case SenseFunctionItem.VoltageCurrentResistance:
                        command = "'VOLT','CURR','RES'";
                        break;
                    default:
                        throw new InstrumentException("Set the Valid Sense Function Type to KE24xx!!!");
                }
                //send(":SENS:FUNC 'CURR'");
                //send(":SENS:CURR:PROT %lf", compliance);
                instrumentChassis.Write(string.Concat(":SENS:FUNC:ON ", command), this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public enum SourceFunctionItem
        {
            /// <summary>
            /// 
            /// </summary>
            Voltage = 1,
            /// <summary>
            /// 
            /// </summary>
            Current = 2,
            /// <summary>
            /// 
            /// </summary>
            Memory = 3
        }
        /// <summary>
        /// 
        /// </summary>
        public enum SenseFunctionItem
        {
            /// <summary>
            /// 
            /// </summary>
            Voltage,
            /// <summary>
            /// 
            /// </summary>
            Current,
            /// <summary>
            /// 
            /// </summary>
            Resistance,
            /// <summary>
            /// 
            /// </summary>
            VoltageCurrent,
            /// <summary>
            /// 
            /// </summary>
            VoltageResistance,
            /// <summary>
            /// 
            /// </summary>
            CurrentResistance,
            /// <summary>
            /// 
            /// </summary>
            VoltageCurrentResistance,

        }
        /// <summary>
        /// Set up both the sense voltage compliance and the sense voltage range
        /// </summary>
        /// <param name="senseCompliance_volt">sense voltage compliance</param>
        /// <param name="senseRange_volt">sense voltage range</param>
        public override void SenseVoltage(double senseCompliance_volt, double senseRange_volt)
        {
            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);

            instrumentChassis.Write(":SENS:FUNC 'VOLT:DC'", this);
            instrumentChassis.Write(":SENS:VOLT:PROT " + senseCompliance_volt, this);
            SenseVoltageRange = senseRange_volt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="senseCompliance_volt"></param>
        /// <param name="senseRange_volt"></param>
        /// <param name="integrationPeriod"></param>
        public virtual void SenseVoltage(double senseCompliance_volt,
            double senseRange_volt, double integrationPeriod)
        {
            instrumentChassis.Write(":SENS:FUNC 'VOLT:DC'", this);
            SenseVoltage(senseCompliance_volt, senseRange_volt);
            instrumentChassis.Write(":SENS:VOLT:NPLC " + integrationPeriod.ToString(), this);
        }

        /// <summary>
        /// Set the voltage sense range
        /// </summary>
        public double SenseVoltageRange
        {
            set
            {
                if (value != 0 && value != double.NaN)
                {
                    instrumentChassis.Write_Unchecked(":SENS:VOLT:RANG:AUTO 0", this);
                    instrumentChassis.Write(":SENS:VOLT:RANG " + value, this);
                }
                else
                {
                    instrumentChassis.Write_Unchecked(":SENS:VOLT:RANG:AUTO 1", this);
                }
            }
        }

        /// <summary>
        /// Set the current sense range
        /// </summary>
        public double SenseCurrentRange
        {
            set
            {
                if (value != 0 && value != double.NaN)
                {
                    CurrentRange_Amp = value;
                }
                else
                {
                    instrumentChassis.Write_Unchecked(":SENS:CURR:RANG:AUTO 1", this);
                }

            }
        }

        /// <summary>
        /// Get/Set the Source voltage range.
        /// </summary>
        public double SourceVoltageRange
        {
            get
            {
                string r = instrumentChassis.Query(":SOUR:VOLT:RANG?", this);
                return double.Parse(r);
            }
            set
            {
                instrumentChassis.Write(":SOUR:VOLT:RANG " + value, this);
            }
        }
        /// <summary>
        /// Set the source current value immediately
        /// </summary>
        /// <param name="value_amp"></param>
        public override void SetCurrent(double value_amp)
        {
            instrumentChassis.Write(":SOUR:CURR:LEV:IMM:AMPL " + value_amp, this);
        }

        /// <summary>
        /// Get  / Set specified current amplitude when triggered
        /// </summary>
        public double TriggerAmplitude_amp
        {
            set
            {
                instrumentChassis.Write(":SOUR:CURR:TRIG " + value, this);
            }
            get
            {
                // Read the current trigger amplitude back
                string rtn = instrumentChassis.Query(":SOUR:CURR:TRIG?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }
        }

        /// <summary>
        /// Get  / Set specified voltage amplitude when triggered
        /// </summary>
        public double TriggerAmplitude_volt
        {
            set
            {
                instrumentChassis.Write(":SOUR:VOLT:TRIG  " + value, this);
            }
            get
            {
                // Read the current trigger amplitude back
                string rtn = instrumentChassis.Query(":SOUR:VOLT:TRIG?", this);

                // Convert and return
                return Convert.ToDouble(rtn);
            }
        }

        /// <summary>
        /// Set up measurement for sourcing current and measuring voltage
        /// </summary>
        /// <param name="voltageCompliance_V">the voltage compliance figure you want</param>
        /// <param name="voltageRange_V">the voltage range you want</param>
        /// <param name="sourceMeasAutoDelay">whether we use an automaic delay or not</param>
        /// <param name="sourceMeasSpecificDelay_s">in the case of not using automatic delay we need to specify a delay time</param>
        /// <param name="numAverages">the number of averages as required for averaging</param>
        /// <param name="integrationRate">the integration rate</param>
        /// <param name="AutoZeroState">Autozero false, NPLC cache ON, Autozero true, NPLC cache OFF</param>
        public override void InitSourceIMeasureV_MeasurementAccuracy(double voltageCompliance_V,
            double voltageRange_V, bool sourceMeasAutoDelay, double sourceMeasSpecificDelay_s,
            int numAverages, double integrationRate, bool AutoZeroState)
        {
            SourceFixedCurrent();
            SenseVoltage(voltageCompliance_V, voltageRange_V);

            SetMeasurementAccuracy(sourceMeasAutoDelay, sourceMeasSpecificDelay_s, numAverages, integrationRate, AutoZeroState);
        }

        /// <summary>
        /// Setting our measurment accuracy up
        /// </summary>
        /// <param name="sourceMeasAutoDelay">whether we use an automaic delay or not</param>
        /// <param name="sourceMeasSpecificDelay_s">in the case of not using automatic delay we need to specify a delay time</param>
        /// <param name="numAverages">the number of averages as required for averaging</param>
        /// <param name="integrationRate">the integration rate</param>
        /// <param name="autoZeroState">Autozero false, NPLC cache ON, Autozero true, NPLC cache OFF</param>
        public override void SetMeasurementAccuracy(bool sourceMeasAutoDelay,
            double sourceMeasSpecificDelay_s, int numAverages,
            double integrationRate, bool autoZeroState)
        {
            if (!sourceMeasAutoDelay)
            {
                SetSourceDelayMeasureManual(sourceMeasSpecificDelay_s);
            }
            else
            {
                SetSourceDelayMeasureAuto();
            }

            AveragingCount = numAverages;
            IntegrationRate_SenseVoltage = integrationRate;
            AutoZero = autoZeroState;
        }

        /// <summary>
        /// Set measurement accuracy
        /// </summary>
        /// <param name="sourceMeasAutoDelay">Auto delay on/off</param>
        /// <param name="sourceMeasSpecificDelay_s">Source measure delay S</param>
        /// <param name="numAverages">Number of averages per measurement</param>
        /// <param name="integrationRate">Integration rate</param>
        public override void SetMeasurementAccuracy(bool sourceMeasAutoDelay,
            double sourceMeasSpecificDelay_s, int numAverages, double integrationRate)
        {
            // TODO - check last param
            SetMeasurementAccuracy(sourceMeasAutoDelay,
                sourceMeasSpecificDelay_s, numAverages, integrationRate, false);
        }

        /// <summary>
        /// Get / set the averaging count
        /// </summary>
        public Int32 AveragingCount
        {
            set
            {
                // Averaging
                if (value < 2)
                    instrumentChassis.Write(":AVER OFF", this);
                else
                {
                    instrumentChassis.Write(":AVER ON", this);
                    instrumentChassis.Write(":AVER:TCON REP", this);
                    instrumentChassis.Write(":AVER:COUN " + value, this);
                }
            }
            get
            {
                // Read the averaging count
                string meas = instrumentChassis.Query(":AVER:COUN?", this);
                Int32 rtn = Convert.ToInt32(meas);
                return (rtn);
            }
        }

        /// <summary>
        /// Getting  / Setting the sense voltage integration rate
        /// </summary>
        public double IntegrationRate_SenseVoltage
        {
            get
            {
                // Read current in amps
                string meas = instrumentChassis.Query("SENS:VOLT:NPLC?", this);

                // Get the 2nd value and convert to double
                double rtn = Convert.ToDouble(meas);

                return (rtn);
            }
            set
            {
                double setRate = value;

                if (value > 10.0)
                    setRate = 10.0;
                if (value < 0.01)
                    setRate = 0.01;

                // Set Integration rate - not that it is set for all measurement types
                instrumentChassis.Write("SENS:VOLT:NPLC " + setRate, this);
            }
        }

        /// <summary>
        /// Getting  / Setting the sense current integration rate
        /// </summary>
        public double IntegrationRate_SenseCurrent
        {
            get
            {
                // Read current in amps
                string meas = instrumentChassis.Query("SENS:CURR:NPLC?", this);

                // Get the 2nd value and convert to double
                double rtn = Convert.ToDouble(meas);

                return (rtn);
            }
            set
            {
                double setRate = value;

                if (value > 10.0)
                    setRate = 10.0;
                if (value < 0.01)
                    setRate = 0.01;

                // Set Integration rate - not that it is set for all measurement types
                instrumentChassis.Write("SENS:CURR:NPLC " + setRate, this);
            }
        }

        /// <summary>
        /// Autozero the meter
        /// </summary>
        public bool AutoZero
        {
            set
            {
                if (value == false)
                {
                    // Autozero OFF, NPLC cache ON
                    instrumentChassis.Write(":SYST:AZER:STAT OFF", this);		// Turn off Autozero
                    instrumentChassis.Write(":SYST:AZER ONCE", this);			// Force an immediate update - Helps to prevent drift			 
                    instrumentChassis.Write(":SYST:AZER:CACH:STAT 1", this);	// Enable NPLC caching
                    instrumentChassis.Write(":SYST:AZER:CACH:REFR", this);	// Update the NPLC cache values

                    // Used in the sweep to see whether we send the AutoZeroRefresh or not
                    m_bAutoZero = false;
                }
                else
                {
                    // Autozero ON, NPLC cache OFF
                    instrumentChassis.Write(":SYST:AZER 1", this);   // Turn on autozero

                    m_bAutoZero = true;
                }
            }
        }


        /// <summary>
        /// Force an immediate autozero update, function only used if Autozero is turned off
        /// </summary>
        public override void AutoZeroRefresh()
        {
            if (!m_bAutoZero)
            {
                instrumentChassis.Write(":SYST:AZER ONCE", this);		// Force an immediate update - Helps to prevent drift			 
                instrumentChassis.Write(":SYST:AZER:CACH:REFR", this);	// Update the NPLC cache values
            }
        }

        /// <summary>
        /// Set it to a manual delay time and specify the time reqd
        /// </summary>
        /// <param name="specificDelay_s">specific time period for delays</param>
        public void SetSourceDelayMeasureManual(double specificDelay_s)
        {
            instrumentChassis.Write(":SOUR:DEL:AUTO OFF", this);
            instrumentChassis.Write(":SOUR:DEL " + specificDelay_s, this);
        }

        /// <summary>
        /// Set it to an automatic delay time
        /// </summary>
        public void SetSourceDelayMeasureAuto()
        {
            instrumentChassis.Write(":SOUR:DEL:AUTO ON", this);
        }

        /// <summary>
        /// Set source delay 
        /// </summary>
        /// <param name="autoDelay">Auto delay on/off</param>
        /// <param name="specificDelay_s">Source delay S</param>
        public override void SetSourceDelayMeasure(bool autoDelay, double specificDelay_s)
        {
            if (autoDelay)
            {
                SetSourceDelayMeasureAuto();
            }
            else
            {
                SetSourceDelayMeasureManual(specificDelay_s);
            }
        }


        /// <summary>
        /// Sets up the meter ready for doing a current sweep, sourcing current and measuring voltage
        /// </summary>
        /// <param name="Imin_amp">The value to sweep from</param>
        /// <param name="Imax_amp">The value to sweep to</param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceIMeasureV_CurrentSweep(double Imin_amp, double Imax_amp,
            int numPts, int inputTriggerLine, int outputTriggerLine)
        {

            // Define sweep
            SourceSweptCurrent(Imin_amp, Imax_amp, numPts);

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Readings are always returned in this order
            ConfigureCurrentSweepReadings();

        }

        /// <summary>
        /// Sets up the meter ready for doing a single fixed value current, sourcing current and measuring voltage
        /// </summary>
        /// <param name="I_amp">the fixed current you wish to set</param>
        /// <param name="numPts">Perform count source-measure operation in the trigger layer, and this is the count </param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceIMeasureV_TriggeredFixedCurrent(double I_amp, int numPts,
            int inputTriggerLine, int outputTriggerLine)
        {
            SetCurrent(I_amp);				// This applies the current immediately.

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Readings are always returned in this order
            ConfigureCurrentSweepReadings();
            // MF - check
            SourceFixedCurrent();
        }

        /// <summary>
        /// Setup for doing a untriggered spot measurment
        /// </summary>
        public override void InitSourceIMeasureV_UntriggeredSpotMeas()
        {
            SourceFixedCurrent();
            DisableTriggering();

            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
            instrumentChassis.Write("FORM ASCII", this);
        }

        //
        // Source voltage, measure current
        //

        /// <summary>
        /// Set up measurement for sourcing voltage and measuring current
        /// </summary>
        /// <param name="currentCompliance_A"></param>
        /// <param name="currentRange_A"></param>
        /// <param name="sourceMeasAutoDelay"></param>
        /// <param name="sourceMeasSpecificDelay_s">in the case of not using automatic delay we need to specify a delay time</param>
        /// <param name="numAverages">the number of averages as required for averaging</param>
        /// <param name="integrationRate">the integration rate</param>
        /// <param name="AutoZeroState">Autozero false, NPLC cache ON, Autozero true, NPLC cache OFF</param>
        public override void InitSourceVMeasureI_MeasurementAccuracy(
            double currentCompliance_A, double currentRange_A,
            bool sourceMeasAutoDelay, double sourceMeasSpecificDelay_s,
            int numAverages, double integrationRate, bool AutoZeroState)
        {
            SourceFixedVoltage();
            SenseCurrent(currentCompliance_A, currentRange_A);

            SetMeasurementAccuracy(sourceMeasAutoDelay, sourceMeasSpecificDelay_s,
                numAverages, integrationRate, AutoZeroState);
            //DataFormat = DataFormatOption.VoltageCurrent;
        }

        /// <summary>
        /// Set up the current compliance and range
        /// </summary>
        /// <param name="IsenseCompliance_amp">compliance  value</param>
        /// <param name="IsenseRange_amp">range value</param>
        /// <returns>a bool to indicate that both compliance and range have been set</returns>
        public override bool SenseCurrent(double IsenseCompliance_amp, double IsenseRange_amp)
        {
            
            instrumentChassis.Write_Unchecked(":FORM:ELEM VOLT,CURR", this);

            instrumentChassis.Write_Unchecked(":SENS:FUNC 'CURR:DC'", this);

            // Range is set to the lowest range containing the compliance value
            bool complianceSet = SetIsenseCompliance(IsenseCompliance_amp);

            // Should now be able to set a higher range if required
            bool rangeSet = false;
            //double Value = IsenseRange_amp;

            SenseCurrentRange = IsenseRange_amp;
            rangeSet = true;

            return (complianceSet && rangeSet);
        }

        /// <summary>
        /// Setting the current sense compliance value up based on knowledge of the ke2400 allowable values, thather than just throwing
        /// exceptions
        /// </summary>
        /// <param name="reqCompliance_amp"></param>
        /// <returns>bool to indicate that was able to set up compliance ok</returns>
        public bool SetIsenseCompliance(double reqCompliance_amp)
        {
            bool status = true;

            double[] ranges =  {	1.05e-6,
								        10.5e-6,
								        105.0e-6,
								        1.05e-3,
								        10.5e-3,
								        105.0e-3,
								        1.050		};

            // range max is 1.0 each step down divide by 10

            double range_now = IsenseRange_amp;
            double compliance_now = CurrentComplianceSetPoint_Amp;
            uint index_now;
            // what is current range index?
            for (index_now = 0; (index_now < 7) && (range_now != ranges[index_now]); index_now++) ;

            uint index_reqd;
            // What should new range be ?
            for (index_reqd = 0; (index_reqd < 7) && (Math.Abs(reqCompliance_amp) >= ranges[index_reqd]); index_reqd++) ;

            if (index_reqd >= index_now) // if increase range
            {
                if (index_now == 6) // can't increase_range any more
                {	// Compliance to max
                    instrumentChassis.Write(":SENS:CURR:PROT " + ranges[6], this);
                    status = false;
                }
                else
                {
                    // Step up range while it is less than reqd
                    for (; index_reqd >= index_now; index_now++)
                    {
                        // Compliance
                        double myVal = ranges[index_now] / 1.05;
                        instrumentChassis.Write(":SENS:CURR:PROT " + myVal, this);


                        // Range

                        instrumentChassis.Write(":SENS:CURR:RANG " + ranges[index_now], this);
                    }

                    // Set final Compliance
                    instrumentChassis.Write(":SENS:CURR:PROT " + reqCompliance_amp, this);
                }
            }
            else
            {
                if (index_now == 0)
                {
                    instrumentChassis.Write(":SENS:CURR:PROT " + ranges[0], this);
                    status = false;
                }
                else
                {
                    // Step it down while it is more than reqd
                    for (; index_reqd <= index_now; index_now--)
                    {
                        // Range
                        instrumentChassis.Write(":SENS:CURR:RANG " + ranges[index_now], this);

                        // Compliance
                        double myVal = ranges[index_now] / 1.05;
                        instrumentChassis.Write(":SENS:CURR:PROT " + myVal, this);
                    }

                    // Set final Compliance
                    if (ranges[index_now + 1] * 0.01 > reqCompliance_amp)
                        instrumentChassis.Write(":SENS:CURR:PROT " + reqCompliance_amp, this);
                    else
                        instrumentChassis.Write(":SENS:CURR:PROT " + ranges[index_now + 1], this);
                }
            }

            return status;
        }

        /// <summary>
        /// Get the sense range (current)
        /// </summary>
        /// <returns>the sense range value</returns>
        public double IsenseRange_amp
        {
            get
            {
                string rtn = instrumentChassis.Query(":SENS:CURR:RANG:UPP?", this);
                return Convert.ToDouble(rtn);
            }
        }

        /// <summary>
        /// Set up for sourcing voltage and measuring current, 
        /// and possibly doing a triggered current sweep
        /// </summary>
        /// <param name="Vmin_volt">minimum voltage for sweep</param>
        /// <param name="Vmax_volt">maximum voltage for sweep</param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceVMeasureI_VoltageSweep(double Vmin_volt, double Vmax_volt,
            int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            // Define sweep
            SourceSweptVoltage(Vmin_volt, Vmax_volt, numPts);

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Always returns in the order SENSE, SOURCE
            ConfigureVoltageSweepReadings();
        }

        // sourceImeasureV

        /// <summary>
        /// configure so it will provide data readings in the order Voltage, Current
        /// </summary>
        public override void ConfigureVoltageSweepReadings()
        {
            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
        }

        /// <summary>
        /// set up for a swept voltage sweep (doesn't use triggerlink calls)
        /// </summary>
        /// <param name="Vmin_volt">start voltage</param>
        /// <param name="Vmax_volt">finish voltage</param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        public override void SourceSweptVoltage(double Vmin_volt, double Vmax_volt, int numPts)
        {
            double stepsize = (double)(Vmax_volt - Vmin_volt) / (numPts - 1);

            instrumentChassis.Write(":SOUR:VOLT:START " + Vmin_volt, this);
            instrumentChassis.Write(":SOUR:VOLT:STOP " + Vmax_volt, this);
            instrumentChassis.Write(":SOUR:VOLT:STEP " + stepsize, this);
            instrumentChassis.Write(":SOUR:SWE:POIN " + numPts, this);
            instrumentChassis.Write(":SOUR:VOLT:MODE SWE", this);
            // Force autozero off for a big speedup
            instrumentChassis.Write(":SYST:AZER:STAT OFF", this);

            //LINEAR STAIRCASE SWEEP
            instrumentChassis.Write(":SOUR:SWE:SPAC LIN", this);
            //sweep direction.Sweep from start to stop (UP) or from stop to start (DOWN).
            instrumentChassis.Write(":SOUR:SWE:DIR DOWN", this);
            //Select source ranging mode (BEST,AUTO, or FIXed).
            instrumentChassis.Write(":SOUR:SWE:RANG BEST", this);
            //Arm Layer:
            instrumentChassis.Write(":ARM:DIR ACC", this);
            instrumentChassis.Write(":ARM:SOUR IMM", this);
            instrumentChassis.Write(":ARM:COUN 1", this);
            instrumentChassis.Write(":ARM:OUTP NONE", this);

            //Trigger Layer:
            //Query number of points in sweep.
            int iSweepCount = 1 + Convert.ToInt32(instrumentChassis.Query_Unchecked(":SOUR:SWE:POIN?", this));
            instrumentChassis.Write(":TRIG:COUN " + iSweepCount.ToString(), this);
            //Specify trigger delay: 0 to 999.9999 (sec).
            //instrumentChassis.Write(":TRIG:DEL 0.1", this);
            //Specify control source (IMMediate or TLINk).
            instrumentChassis.Write(":TRIG:SOUR IMM", this);
            instrumentChassis.Write(":TRIG:DIR ACC", this);
            //Select input trigger line (1, 2, 3, or 4).
            instrumentChassis.Write(":TRIG:ILIN 2", this);
            //Select output trigger line (1, 2, 3, or 4).
            instrumentChassis.Write(":TRIG:OLIN 1", this);
            //Enable input event detectors (SOURce,DELay, SENSe, or NONE).

            // In some low version 24xx, the command"TRIG:INP SOUR" can't be recognized, so we "try" to do this command , Echo,31/july/09

            try
            {
                instrumentChassis.Write(":TRIG:INP SOUR", this);
            }
            catch
            {
                throw new Exception(" This Device can't recognize this command!");
            }
            //Output trigger after SOURce, DELay, SENSe or not (NONE) at all.
            //instrumentChassis.Write(":TRIG:OUTP DEL", this);
            instrumentChassis.Write(":TRIG:OUTP SOUR", this);

        }

        /// <summary>
        /// Setup for sourcing a fixed voltage and measuring I (using triggers)
        /// </summary>
        /// <param name="setPoint_volt"></param>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        public override void InitSourceVMeasureI_TriggeredFixedVoltage(double setPoint_volt,
            int numPts, int inputTriggerLine, int outputTriggerLine)
        {
            VoltageSetPoint_Volt = setPoint_volt;

            // All things to do with triggers
            ConfigureTriggering(numPts, inputTriggerLine, outputTriggerLine, true);

            // Data stored from sweep
            // Readings are always returned in this order
            ConfigureVoltageSweepReadings();
            // MF - check
            SourceFixedVoltage();
        }

        /// <summary>
        /// Switches back from triggered type, and sets up for sourcing voltage whilst measuring current
        /// </summary>
        public override void InitSourceVMeasureI_UntriggeredSpotMeas()
        {
            SourceFixedVoltage();
            DisableTriggering();

            instrumentChassis.Write(":FORM:ELEM VOLT,CURR", this);
            instrumentChassis.Write("FORM ASCII", this);
        }

        /// <summary>
        /// Configure your trigger linking stuff
        /// </summary>
        /// <param name="numPts">The number of points to take the sweep over</param>
        /// <param name="inputTriggerLine">the input trigger line</param>
        /// <param name="outputTriggerLine">the output trigger line</param>
        /// <param name="triggerViaTLinks">are we triggering via the tlinks or immediately</param>
        public override void ConfigureTriggering(int numPts,
            int inputTriggerLine, int outputTriggerLine, bool triggerViaTLinks)
        {
            if (triggerViaTLinks)
            {
                instrumentChassis.Write(":TRIG:SOUR TLINK", this);			// Expect external trigger
            }
            else
            {
                instrumentChassis.Write(":TRIG:SOUR IMM", this);
            }
            ConfigureTriggerlines(inputTriggerLine, outputTriggerLine);

            instrumentChassis.Write(":TRIG:TCON:DIR SOUR", this);		// Trigger bypass set to SOURce will bypass the control source once (other option -> ACCeptor )
            instrumentChassis.Write(":TRIG:COUN " + numPts, this);	// Perform count source-measure operation in the trigger layer
            instrumentChassis.Write(":TRIG:TCON:OUTP SOUR", this);		// Output trigger after each measurement
        }


        /// <summary>
        /// Allows you to configure triggerlines 
        /// </summary>
        /// <param name="inputTriggerLine">The input line you wish to select</param>
        /// <param name="outputTriggerLine">The output line you wish to associate with the input line</param>
        public override void ConfigureTriggerlines(int inputTriggerLine, int outputTriggerLine)
        {
            instrumentChassis.Write(":TRIG:ILIN " + inputTriggerLine, this);	// Default is 1
            instrumentChassis.Write(":TRIG:OLIN " + outputTriggerLine, this);	// Default is 2
        }

        /// <summary>
        /// Disable the triggering
        /// </summary>
        public override void DisableTriggering()
        {
            instrumentChassis.Write(":ABOR", this);
            instrumentChassis.Write(":TRIG:CLE", this);
            instrumentChassis.Write(":ARM:SOUR IMM", this);			// Source immediately. Dont wait for external trigger.
            instrumentChassis.Write(":TRIG:SOUR IMM", this);			// Source immediately. Dont wait for external trigger.
            instrumentChassis.Write(":TRIG:TCON:OUTP NONE", this);		// No trigger output.
        }



        /// <summary>
        /// Aborting a sweep 
        /// </summary>
        public override void AbortSweep()
        {
            instrumentChassis.Clear();
            //instrumentChassis.Write_Unchecked(":OUTPUT OFF", this);
            instrumentChassis.Write_Unchecked(":ABORT", this);
        }

        /// <summary>
        /// Cleans up a sweep, aborts it and clears the triggering and traces
        /// </summary>
        public override void CleanUpSweep()
        {
            // Abort the sweep
            // Abort any pending operations
            instrumentChassis.Clear();
            instrumentChassis.Write_Unchecked(":OUTPUT OFF", this);
            instrumentChassis.Write_Unchecked(":ABORT", this);
            ClearSweepTriggering();
            ClearSweepTraceData();
        }

        /// <summary>
        /// Clears the trigger
        /// </summary>
        public override void ClearSweepTriggering()
        {
            instrumentChassis.Write_Unchecked(":TRIG:CLEAR", this);
            // Autozero ON - prevents strange readings !
            instrumentChassis.Write_Unchecked(":SYST:AZER:STAT ON", this);
            // Output format = ASCII
            instrumentChassis.Write_Unchecked(":FORM ASC", this);
        }

        /// <summary>
        /// Clears the trace
        /// </summary>
        public override void ClearSweepTraceData()
        {
            instrumentChassis.Write_Unchecked(":TRAC:CLEAR", this);
        }

        /// <summary>
        /// Starts a sweep, this command uses a write unchecked... so you need to
        /// call a WaitForOperationToComplete() if you wish to ensure operation has finished.
        /// That, or GetESRStatus for yourself for your specific requirements
        /// </summary>
        public override void StartSweep()
        {
            //play();
            instrumentChassis.Write_Unchecked(":INIT", this);
        }

        /// <summary>
        /// Trigger measurement
        /// </summary>
        public override void Trigger()
        {
            instrumentChassis.Write_Unchecked(":INIT", this);
        }

        /// <summary>
        /// Trigger measurement out of arm layer using *TRG
        /// </summary>
        public void TriggerArm()
        {
            instrumentChassis.Write_Unchecked("*TRG", this);
        }


        /// <summary>
        /// Making use of the chassis OPC* command
        /// </summary>
        public void WaitForOperationToComplete()
        {
            instrumentChassis.WaitForOperationComplete();
        }

        /// <summary>
        /// Wait for sweep to finish
        /// </summary>
        /// <returns>True when sweep ends</returns>
        public override bool WaitForSweepToFinish()
        {
            //int oldTimeout = instrumentChassis.Timeout_ms;
            //instrumentChassis.Timeout_ms = this.Timeout_ms;
            instrumentChassis.WaitForOperationComplete();
            //instrumentChassis.Timeout_ms = oldTimeout;
            return true;
        }


        /// <summary>
        /// use *OPC? to wait for operation to complete
        /// </summary>
        /// <param name="timeout_ms">timeout in ms</param>
        public void WaitForSweepToFinish(int timeout_ms)
        {
            int oldTimeout = instrumentChassis.Timeout_ms;
            instrumentChassis.Timeout_ms = timeout_ms;
            string reply = instrumentChassis.Query_Unchecked("*OPC?", this);
            instrumentChassis.Timeout_ms = oldTimeout;
            if (reply != "1") throw new ChassisException("Unexpected reply format from the instrument while waiting for sweep to complete");

        }
        /// <summary>
        /// Gets the esr status, (hooking into the instrument chassis)
        /// </summary>
        /// <returns>the standard event register byte</returns>
        public byte GetESRStatus()
        {
            return (instrumentChassis.StandardEventRegister);
        }

        /// <summary>
        /// A public structure for the sweep data that will be returned
        /// </summary>
        public struct SweepData
        {
            /// <summary>
            /// An array of voltages
            /// </summary>
            public Single[] voltage_V;
            /// <summary>
            /// An array of currents
            /// </summary>
            public Single[] current_mA;
        }

        /// <summary>
        /// Returns the data from a sweep,
        /// </summary>
        /// <returns>a sweepdata structure containing an array of voltages and an array of currents</returns>
        public override void GetSweepDataSet(out Double[] VoltageData, out Double[] CurrentData)
        {
            //instrumentChassis.Write("FORM:BORD NORM", this ); // Normal byte order
            instrumentChassis.Write_Unchecked("FORM:BORD SWAP", this); // Big Endian byte order
            instrumentChassis.Write_Unchecked("FORM REAL, 32", this);
            byte[] buffer = instrumentChassis.QueryByteArray_Unchecked(":FETCH?", this);
            Single[] valueList = ParseBinaryData(buffer);

            VoltageData = new Double[valueList.Length / 2];
            CurrentData = new Double[valueList.Length / 2];

            int x = 0;

            for (int i = 0; i < valueList.Length; i += 2)
            {
                CurrentData[x] = (double)valueList[i + 1];
                VoltageData[x] = (double)valueList[i];
                x++;
            }
        }

        /// <summary>
        /// Takes the binary data (byte array), ignores the first 2 bytes, then
        /// converts groups of 4 bytes into singles and eventually returns an array of singles
        /// </summary>
        /// <param name="buffer">the bute array</param>
        /// <returns>an array of singles</returns>
        public override Single[] ParseBinaryData(byte[] buffer)
        {
            Single[] valueList;

            int buffSize = buffer.Length;
            int index = 0;
            valueList = new Single[(buffSize - 2) / 4];

            // Ignore the first 2 bytes as they contain header information
            for (int i = 2; i + 3 < buffer.Length; i += 4)
            {
                byte[] subArray = Alg_ArrayFunctions.ExtractSubArray(buffer, i, i + 4);

                valueList[index] = Alg_BigEndianConvert.BigEndianBytesToSingle(subArray);

                index++;
            }

            return valueList;
        }

        #endregion
        /// <summary>
        /// Get/Set to enable or disable the hardware interlock. 
        /// </summary>
        public bool EnableInterLock
        {
            get
            {
                string cmd = ":OUTP:INT:STAT?";
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                // "1": Enable interlock
                // "0": Disable interlock
                return rsp.Contains("1");
            }
            set
            {
                string cmd = ":OUTP:INT:STAT " + (value ? "1" : "0");
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        /// <summary>
        /// Get/Set output-off state of the SourceMeter.
        /// </summary>
        public EnumOutputOffType OutputOffState
        {
            get
            {
                string cmd = ":OUTP:SMOD?";
                string rsp = instrumentChassis.Query_Unchecked(cmd, this);
                switch (rsp)
                {
                    case "HIMP":
                        return EnumOutputOffType.HIMPedance;
                    case "NORM":
                        return EnumOutputOffType.NORMal;
                    case "ZERO":
                        return EnumOutputOffType.ZERO;
                    case "GUAR":
                        return EnumOutputOffType.GUARd;
                    default:
                        throw new InstrumentException("Unexpected response string getting with query \'" +
                            cmd + "\' : " + rsp);
                }
            }
            set
            {
                string cmdPrefix = ":OUTP:SMOD ";
                string valStr = "";

                switch (value)
                {
                    case EnumOutputOffType.HIMPedance:
                        valStr = "HIMP";
                        break;
                    case EnumOutputOffType.NORMal:
                        valStr = "NORM";
                        break;
                    case EnumOutputOffType.ZERO:
                        valStr = "ZERO";
                        break;
                    case EnumOutputOffType.GUARd:
                        valStr = "GUAR";
                        break;
                }
                this.instrumentChassis.Write(cmdPrefix + valStr, this);
            }
        }
        /// <summary>
        /// With auto output-off enabled, an :INITiate (or :READ? or MEASure?) will
        /// start source-measure operation. The output will turn on at the beginning
        /// of each SDM (source-delay-measure) cycle and turn off after each measurement is completed.
        /// </summary>
        public bool EnableAutoOutput
        {
            get
            {
                string cmd = ":SOUR:CLE:AUTO?";
                string rsp = this.instrumentChassis.Query_Unchecked(cmd, this);

                return (rsp.Contains("1"));
            }
            set
            {
                string cmd = ":SOUR:CLE:AUTO " + (value ? 1 : 0);
                this.instrumentChassis.Write(cmd, this);
            }
        }
        #region DioLine function

        /// <summary>
        /// Sets/Gets the status of Digital1 Output
        /// </summary>
        public bool DigitalOutput1Enabled
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this);
                int rtnData = Convert.ToInt32(rtn);
                int rtnBit = rtnData & 0x1;
                return (rtnBit.ToString() == falseStr ? false : true);
            }
            set
            {
                int previousData = Convert.ToInt32(instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this));
                int setData;
                if (value)
                {
                    setData = previousData | 0x1;
                }
                else
                {
                    setData = previousData & 0x14;
                }
                instrumentChassis.Write_Unchecked(":SOURCE2:TTL " + setData.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of Digital2 Output
        /// </summary>
        public bool DigitalOutput2Enabled
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this);
                int rtnData = Convert.ToInt32(rtn);
                int rtnBit = rtnData & 0x2;
                return (rtnBit.ToString() == falseStr ? false : true);
            }
            set
            {
                int previousData = Convert.ToInt32(instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this));
                int setData;
                if (value)
                {
                    setData = previousData | 0x2;
                }
                else
                {
                    setData = previousData & 0x13;
                }
                instrumentChassis.Write_Unchecked(":SOURCE2:TTL " + setData.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of Digital3 Output
        /// </summary>
        public bool DigitalOutput3Enabled
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this);
                int rtnData = Convert.ToInt32(rtn);
                int rtnBit = rtnData & 0x4;
                return (rtnBit.ToString() == falseStr ? false : true);
            }
            set
            {
                int previousData = Convert.ToInt32(instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this));
                int setData;
                if (value)
                {
                    setData = previousData | 0x4;
                }
                else
                {
                    setData = previousData & 0x11;
                }
                instrumentChassis.Write_Unchecked(":SOURCE2:TTL " + setData.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the status of Digital4 Output
        /// </summary>
        public bool DigitalOutput4Enabled
        {
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this);
                int rtnData = Convert.ToInt32(rtn);
                int rtnBit = rtnData & 0x8;
                return (rtnBit.ToString() == falseStr ? false : true);
            }
            set
            {
                int previousData = Convert.ToInt32(instrumentChassis.Query_Unchecked(":SOURCE2:TTL:LEVEL?", this));
                int setData;
                if (value)
                {
                    setData = previousData | 0x8;
                }
                else
                {
                    setData = previousData & 0x7;
                }
                instrumentChassis.Write_Unchecked(":SOURCE2:TTL " + setData.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the Voltage Range
        /// </summary>
        public double VoltageRange_Volt
        {
            get
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if(functionString != voltageFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't VOLT!!!");
                //}
                string rtn = instrumentChassis.Query_Unchecked(":SOUR:VOLT:RANG?", this);

                return Convert.ToDouble(rtn);
            }
            set
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != voltageFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't VOLT!!!");
                //}

                if (value < -210 || value > 210)
                {
                    throw new InstrumentException("The Value of Current Range is out of Limits!!!");
                }

                instrumentChassis.Write_Unchecked(":SOUR:VOLT:RANG:AUTO OFF", this);
                instrumentChassis.Write_Unchecked(":SOUR:VOLT:RANG " + value.ToString().Trim(), this);
            }
        }

        /// <summary>
        /// Sets/Gets the Current Range
        /// </summary>
        public double CurrentRange_Amp
        {
            get
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != currentFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't CURR!!!");
                //}
                string rtn = instrumentChassis.Query_Unchecked(":SOUR:CURR:RANG?", this);

                return Convert.ToDouble(rtn);
            }
            set
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != currentFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't CURR!!!");
                //}

                //if (value < -1.05 || value > 1.05)
                //{
                //    throw new InstrumentException("The Value of Current Range is out of Limits!!!");
                //}

                instrumentChassis.Write_Unchecked(":SOUR:CURR:RANG:AUTO OFF", this);
                instrumentChassis.Write_Unchecked(":SOUR:CURR:RANG " + value.ToString(), this);
            }
        }


        /// <summary>
        /// Set interval for arm layer timer
        /// </summary>
        public double TriggerTimer_s
        {
            set
            {
                instrumentChassis.Write_Unchecked(":TRIG:TIMer " + value.ToString(), this);
            }
            get
            {
                return double.Parse(instrumentChassis.Query_Unchecked(":TRIG:TIMer?", this));
            }
        }



        /// <summary>
        /// 
        /// </summary>
        public TriggerSourceType ArmInput
        {
            set
            {
                string command = ":ARM:SOURCE ";

                instrumentChassis.Write_Unchecked(command + value.ToString(), this);
            }
        }

        #endregion

        #region Private Data

        // Chassis reference
        private Chassis_Ke24xx instrumentChassis;

        // Bool values
        private const string falseStr = "0";
        private const string trueStr = "1";
        private bool m_bAutoZero;

        #endregion

        #region Enum Data


        /// <summary>
        /// 
        /// </summary>
        public enum ControlOption
        {
            /// <summary>
            /// Disables buffer storage
            /// </summary>
            NEVER,
            /// <summary>
            /// Fills buffer and stops
            /// </summary>
            NEXT
        }

        /// <summary>
        /// 
        /// </summary>
        public enum FeedOption
        {
            /// <summary>
            /// Put raw readings in buffer
            /// </summary>
            SENSE,
            /// <summary>
            /// Put Calc1 readings in buffer
            /// </summary>
            CALCULATE1,
            /// <summary>
            /// Put Calc2 readings in buffer
            /// </summary>
            CALCULATE2
        }

        /// <summary>
        /// 
        /// </summary>
        public enum DataFormatOption
        {
            /// <summary>
            /// 
            /// </summary>
            Voltage,
            /// <summary>
            /// 
            /// </summary>
            Current,
            /// <summary>
            /// 
            /// </summary>
            Resistance,
            /// <summary>
            /// 
            /// </summary>
            VoltageCurrent,
            /// <summary>
            /// 
            /// </summary>
            VoltageResistance,
            /// <summary>
            /// 
            /// </summary>
            CurrentResistance,
            /// <summary>
            /// 
            /// </summary>
            VoltageCurrentResistance
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TriggerDirectionType
        {
            /// <summary>
            /// 
            /// </summary>
            ACCEPTOR,
            /// <summary>
            /// 
            /// </summary>
            SOURCE
        }

        /// <summary>
        /// 
        /// </summary>
        public enum TriggerLayerType
        {
            /// <summary>
            /// 
            /// </summary>
            SOURCE,
            /// <summary>
            /// 
            /// </summary>
            DELAY,
            /// <summary>
            /// 
            /// </summary>
            SENSE,
            /// <summary>
            /// 
            /// </summary>
            SOURCEDELAY,
            /// <summary>
            /// 
            /// </summary>
            SOURCESENSE,
            /// <summary>
            /// 
            /// </summary>
            DELAYSENSE,
            /// <summary>
            /// 
            /// </summary>
            SOURCEDELAYSENSE,
            /// <summary>
            /// 
            /// </summary>
            NONE
        }

        /// <summary>
        /// Specify arm/trigger event control source
        /// </summary>
        public enum TriggerSourceType
        {
            /// <summary>
            /// Pass operation through immediately
            /// </summary>
            IMMediate,
            /// <summary>
            /// Select Trigger Link trigger as event
            /// </summary>
            TLINk,
            /// <summary>
            /// Select timer as event
            /// </summary>
            TIMer,
            /// <summary>
            /// Select manual event
            /// </summary>
            MANual,
            /// <summary>
            /// Select bus trigger as event
            /// </summary>
            BUS,
            /// <summary>
            /// Select low SOT pulse as event
            /// </summary>
            NSTest,
            /// <summary>
            /// Select high SOT pulse as event
            /// </summary>
            PSTest,
            /// <summary>
            /// Select high or low SOT pulse as event
            /// </summary>
            BSTest
        }

        /// <summary>
        /// Enum lines available for Tlink
        /// </summary>
        public enum TriggerLine
        {
            /// <summary>
            /// 
            /// </summary>
            Line1,
            /// <summary>
            /// 
            /// </summary>
            Line2,
            /// <summary>
            /// 
            /// </summary>
            Line3,
            /// <summary>
            /// 
            /// </summary>
            Line4
        }

        /// <summary>
        /// Enum ways to do sweep
        /// </summary>
        public enum SweepOptions
        {
            /// <summary>
            /// 
            /// </summary>
            UP,
            /// <summary>
            /// 
            /// </summary>
            DOWN,
            /// <summary>
            /// 
            /// </summary>
            LINEAR,
            /// <summary>
            /// 
            /// </summary>
            LOG
        }
        /// <summary>
        /// Enum the output-off state of the SourceMeter.
        /// </summary>
        public enum EnumOutputOffType
        {
            /// <summary>
            /// Disconnect Input/Output
            /// </summary>
            HIMPedance,
            /// <summary>
            /// Normal output-off state
            /// </summary>
            NORMal,
            /// <summary>
            /// Zero output-off state
            /// </summary>
            ZERO,
            /// <summary>
            /// Guard output-off state
            /// </summary>
            GUARd,
        }

        #endregion

        #region Tommy Region For Sweep Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nValue"></param>
        public void Set_DigitalIO_Value(int nValue)
        {
            if ((nValue < 0) || (nValue > 15))
            {
                throw new Exception("K2400::Set_DigitalIO_Value - Value Out of Range");
            }
            //send( "SOURCE2:TTL %d", nValue );
            instrumentChassis.Write("SOURCE2:TTL " + nValue.ToString(), this);
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetLocal()
        {
            //send(":SYST:LOC");
            instrumentChassis.Write_Unchecked(":SYST:LOC", this);
        }
        /// <summary>
        /// Enum source functions available for sweep
        /// </summary>
        public enum SweepType
        {
            /// <summary>
            /// Voltage sweep
            /// </summary>
            VOLT,
            /// <summary>
            /// Current sweep
            /// </summary>
            CURR,
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sweepType"></param>
        /// <param name="Start"></param>
        /// <param name="Stop"></param>
        /// <param name="Step"></param>
        /// <param name="Delay_S"></param>
        public void LinearStairCaseSweepSettings(SweepType sweepType,
            double Start, double Stop, double Step, double Delay_S)
        {
            string strHead = ":SOUR:" + sweepType.ToString();

            instrumentChassis.Write(strHead + ":START " + Start.ToString(), this);
            instrumentChassis.Write(strHead + ":STOP " + Stop.ToString(), this);
            instrumentChassis.Write(strHead + ":STEP " + Step.ToString(), this);
            instrumentChassis.Write(strHead + ":MODE SWE", this);
            // Force autozero off for a big speedup
            instrumentChassis.Write(":SYST:AZER:STAT OFF", this);

            //LINEAR STAIRCASE SWEEP
            instrumentChassis.Write(":SOUR:SWE:SPAC LIN", this);
            //sweep direction.Sweep from start to stop (UP) or from stop to start (DOWN).
            instrumentChassis.Write(":SOUR:SWE:DIR DOWN", this);
            //Select source ranging mode (BEST,AUTO, or FIXed).
            instrumentChassis.Write(":SOUR:SWE:RANG BEST", this);
            //Arm Layer:
            instrumentChassis.Write(":ARM:DIR ACC", this);
            instrumentChassis.Write(":ARM:SOUR IMM", this);
            instrumentChassis.Write(":ARM:COUN 1", this);
            instrumentChassis.Write(":ARM:OUTP NONE", this);

            //Trigger Layer:
            //Query number of points in sweep.
            int iTrigCount = 1 + Convert.ToInt32(instrumentChassis.Query_Unchecked(":SOUR:SWE:POIN?", this));
            //Specify trigger delay: 0 to 999.9999 (sec).
            instrumentChassis.Write(":TRIG:DEL " + Delay_S.ToString(), this);
            instrumentChassis.Write(":SOUR:DEL " + Delay_S.ToString(), this);
            instrumentChassis.Write(":TRIG:DIR ACC", this);
            //Specify control source (IMMediate or TLINk).
            instrumentChassis.Write(":TRIG:SOUR IMM", this);
            instrumentChassis.Write(":TRIG:COUN " + iTrigCount.ToString(), this);
            //Output trigger after SOURce, DELay, SENSe or not (NONE) at all.
            instrumentChassis.Write(":TRIG:OUTP DEL", this);
            //Enable input event detectors (SOURce,DELay, SENSe, or NONE).
            instrumentChassis.Write(":TRIG:INP SOUR", this);
            //Select input trigger line (1, 2, 3, or 4).
            instrumentChassis.Write(":TRIG:ILIN 2", this);
            //Select output trigger line (1, 2, 3, or 4).
            instrumentChassis.Write(":TRIG:OLIN 1", this);

        }

        /// <summary>
        /// 
        /// </summary>
        public double SweepStartCurrent_A
        {
            set
            {
                string command = String.Format(":*CLS;:SOUR:CURR:START {0}", value);
                //send(":*CLS;:SOUR:CURR:START %lf", value);
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double SweepStopCurrent_A
        {
            set
            {
                string command = String.Format(":*CLS;:SOUR:CURR:STOP {0}", value);
                //send(":*CLS;:SOUR:CURR:STOP %lf", value);
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double SweepCurrentStep_A
        {
            set
            {
                string command = String.Format(":*CLS;:SOUR:CURR:STE {0}", value);
                //send(":*CLS;:SOUR:CURR:STEP %lf", value);
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetSourceCurrentModeToSweep()
        {
            //send(":*CLS;:SOUR:CURR:MODE SWE");
            instrumentChassis.Write_Unchecked(":*CLS;:SOUR:CURR:MODE SWE", this);
        }

        /// <summary>
        /// Sets/Gets the Level of Current
        /// </summary>
        public double CurrentLevel_Amp
        {
            get
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != currentFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't CURR!!!");
                //}

                string rtn = instrumentChassis.Query_Unchecked(":SOUR:CURR:LEV:AMPL?", this);

                return Convert.ToDouble(rtn);
            }
            set
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != currentFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't CURR!!!");
                //}

                if (value < -1.05 || value > 1.05)
                {
                    throw new InstrumentException("The Value of Current Range is out of Limits!!!");
                }
                //send(":*CLS;:SOUR:CURR:LEV:IMM:AMPL %lf",dValue);
                instrumentChassis.Write_Unchecked(":*CLS;:SOUR:CURR:LEV:IMM:AMPL " + value.ToString(), this);
            }
        }
        /// <summary>
        /// Waiting for sweep completed with time out
        /// </summary>
        /// <param name="dTimeOut_s"></param>
        public void WaitForSweep(double dTimeOut_s)
        {
            DateTime tmStart, tmFinish;
            double dDifference = 0;
            bool bTimeOut = false;
            tmStart = DateTime.Now;

            while (!bTimeOut)
            {

                string strResp = "8";
                try
                {
                    WaitForSweepToFinish();
                    Thread.Sleep(500);
                    strResp = instrumentChassis.Query_Unchecked("STAT:OPER:EVEN?", this);
                }
                catch
                {
                    strResp = "8";
                }
                int R = Convert.ToInt32(strResp);
                if (!Convert.ToBoolean(R & 8)) break;
                tmFinish = DateTime.Now;
                dDifference = ((TimeSpan)(tmFinish - tmStart)).TotalSeconds;
                bTimeOut = dDifference > dTimeOut_s;
            }
        }

        // TODO - implement this using SRQ

        // TODO - check this

        /// <summary>
        /// 
        /// </summary>
        public TriggerSourceType ArmSource
        {
            set
            {
                //send(":ARM:SOURCE IMM");
                instrumentChassis.Write_Unchecked(":ARM:SOURCE " + value.ToString(), this);
            }
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":ARM:SOURce?", this);

                TriggerSourceType results = TriggerSourceType.IMMediate;

                switch (rtn.ToUpper())
                {
                    case "IMM":
                        results = TriggerSourceType.IMMediate;
                        break;
                    case "MAN":
                        results = TriggerSourceType.MANual;
                        break;
                    case "TLIN":
                        results = TriggerSourceType.TLINk;
                        break;
                    case "TIM":
                        results = TriggerSourceType.TIMer;
                        break;
                    case "BUS":
                        results = TriggerSourceType.BUS;
                        break;
                    case "BST":
                        results = TriggerSourceType.BSTest;
                        break;
                    case "NST":
                        results = TriggerSourceType.NSTest;
                        break;
                    case "PST":
                        results = TriggerSourceType.PSTest;
                        break;
                }

                return results;
            }
        }

        /// <summary>
        /// Specify trigger event control source
        /// </summary>
        public TriggerSourceType TriggerSource
        {
            set
            {
                //send(":*CLS;:TRIG:SOURCE IMM");
                instrumentChassis.Write_Unchecked(":*CLS;:TRIG:SOURCE " + value.ToString(), this);
            }
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:SOURce?", this);

                TriggerSourceType results = TriggerSourceType.IMMediate;

                switch (rtn.ToUpper())
                {
                    case "IMM":
                        results = TriggerSourceType.IMMediate;
                        break;
                    case "MAN":
                        results = TriggerSourceType.MANual;
                        break;
                    case "TLIN":
                        results = TriggerSourceType.TLINk;
                        break;
                    case "TIM":
                        results = TriggerSourceType.TIMer;
                        break;
                    case "BUS":
                        results = TriggerSourceType.BUS;
                        break;
                    case "BST":
                        results = TriggerSourceType.BSTest;
                        break;
                    case "NST":
                        results = TriggerSourceType.NSTest;
                        break;
                    case "PST":
                        results = TriggerSourceType.PSTest;
                        break;
                }

                return results;
            }
        }
        /// <summary>
        /// Set/Get trigger-in sources
        /// </summary>
        public TriggerLayerType TriggerInput
        {
            set
            {
                string command = ":TRIG:INPUT ";
                //send(":TRIG:INP SOUR");
                switch (value)
                {
                    case TriggerLayerType.SOURCE:
                        command += "SOUR";
                        break;
                    case TriggerLayerType.DELAY:
                        command += "DEL";
                        break;
                    case TriggerLayerType.SENSE:
                        command += "SENS";
                        break;
                    case TriggerLayerType.SOURCEDELAY:
                        command += "SOUR,DEL";
                        break;
                    case TriggerLayerType.SOURCESENSE:
                        command += "SOUR,SENS";
                        break;
                    case TriggerLayerType.DELAYSENSE:
                        command += "DEL,SENS";
                        break;
                    case TriggerLayerType.SOURCEDELAYSENSE:
                        command += "SOUR,DEL,SENS";
                        break;
                    case TriggerLayerType.NONE:
                        command += "NONE";
                        break;
                }

                instrumentChassis.Write_Unchecked(command, this);
            }
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:INPUT?", this);

                TriggerLayerType results = TriggerLayerType.NONE;

                switch (rtn.ToUpper())
                {
                    case "NONE":
                        results = TriggerLayerType.NONE;
                        break;
                    case "SENS":
                        results = TriggerLayerType.SENSE;
                        break;
                    case "SOUR":
                        results = TriggerLayerType.SOURCE;
                        break;
                    case "DEL":
                        results = TriggerLayerType.DELAY;
                        break;
                    case "SOUR,DEL":
                        results = TriggerLayerType.SOURCEDELAY;
                        break;
                    case "DEL,SENS":
                        results = TriggerLayerType.DELAYSENSE;
                        break;
                    case "SOUR,SENS":
                        results = TriggerLayerType.SOURCESENSE;
                        break;
                    case "SOUR,DEL,SENS":
                        results = TriggerLayerType.SOURCEDELAYSENSE;
                        break;
                }

                return results;
            }
        }
        /// <summary>
        /// Get/Set trigger-out source
        /// </summary>
        public TriggerLayerType TriggerOutput
        {
            set
            {
                string command = ":TRIG:OUTPUT ";
                //send(":TRIG:OUTPUT DEL");
                switch (value)
                {
                    case TriggerLayerType.SOURCE:
                        command += "SOUR";
                        break;
                    case TriggerLayerType.DELAY:
                        command += "DEL";
                        break;
                    case TriggerLayerType.SENSE:
                        command += "SENS";
                        break;
                    case TriggerLayerType.SOURCEDELAY:
                        command += "SOUR,DEL";
                        break;
                    case TriggerLayerType.SOURCESENSE:
                        command += "SOUR,SENS";
                        break;
                    case TriggerLayerType.DELAYSENSE:
                        command += "DEL,SENS";
                        break;
                    case TriggerLayerType.SOURCEDELAYSENSE:
                        command += "SOUR,DEL,SENS";
                        break;
                    case TriggerLayerType.NONE:
                        command += "NONE";
                        break;
                }

                instrumentChassis.Write_Unchecked(command, this);
            }
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:OUTPUT?", this);

                TriggerLayerType results = TriggerLayerType.NONE;

                switch (rtn.ToUpper())
                {
                    case "NONE":
                        results = TriggerLayerType.NONE;
                        break;
                    case "SENS":
                        results = TriggerLayerType.SENSE;
                        break;
                    case "SOUR":
                        results = TriggerLayerType.SOURCE;
                        break;
                    case "DEL":
                        results = TriggerLayerType.DELAY;
                        break;
                    case "SOUR,DEL":
                        results = TriggerLayerType.SOURCEDELAY;
                        break;
                    case "DEL,SENS":
                        results = TriggerLayerType.DELAYSENSE;
                        break;
                    case "SOUR,SENS":
                        results = TriggerLayerType.SOURCESENSE;
                        break;
                    case "SOUR,DEL,SENS":
                        results = TriggerLayerType.SOURCEDELAYSENSE;
                        break;
                }

                return results;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int TriggerCount
        {
            set
            {
                //send(":*CLS;:TRIG:COUN %d", nCount);
                instrumentChassis.Write_Unchecked(":*CLS;:TRIG:COUN " + value.ToString(), this);
            }
            get
            {
                return int.Parse(instrumentChassis.Query_Unchecked(":TRIG:COUN?", this));
            }
        }

        /// <summary>
        /// Set/Get arm layer trigger count
        /// </summary>
        public int ArmCount
        {
            get
            {
                return int.Parse(instrumentChassis.Query_Unchecked(":ARM:COUN?", this));
            }
            set
            {
                instrumentChassis.Write_Unchecked(":*CLS;:ARM:COUN " + value.ToString(), this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TriggerDirectionType ArmDirection
        {
            set
            {
                string command = ":ARM:DIR ";
                //send(":ARM:DIR ACC");
                switch (value)
                {
                    case TriggerDirectionType.ACCEPTOR:
                        command += "ACC";
                        break;
                    case TriggerDirectionType.SOURCE:
                        command += "SOURCE";
                        break;
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public TriggerDirectionType TriggerDirection
        {
            set
            {
                string command = ":TRIG:DIR ";
                //send(":TRIG:DIR ACC");
                switch (value)
                {
                    case TriggerDirectionType.ACCEPTOR:
                        command += "ACC";
                        break;
                    case TriggerDirectionType.SOURCE:
                        command += "SOURCE";
                        break;
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DataFormatOption DataFormat
        {
            set
            {
                string command = ":*CLS;:FORM:ELEM ";
                switch (value)
                {
                    case DataFormatOption.Voltage:
                        command += "VOLT";
                        break;
                    case DataFormatOption.Current:
                        command += "CURR";
                        break;
                    case DataFormatOption.Resistance:
                        command += "RES";
                        break;
                    case DataFormatOption.VoltageCurrent:
                        //VOLT,CURR
                        command += "VOLT,CURR";
                        break;
                    case DataFormatOption.VoltageResistance:
                        command += "VOLT,RES";
                        break;
                    case DataFormatOption.CurrentResistance:
                        command += "CURR,RES";
                        break;
                    case DataFormatOption.VoltageCurrentResistance:
                        command += "VOLT,CURR,RES";
                        break;
                }
                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="numberOfDataPoints"></param>
        /// <param name="cState"></param>
        /// <param name="fState"></param>
        public void InitialiseDataBuffer(int numberOfDataPoints, ControlOption cState, FeedOption fState)
        {
            //TestStand no send!
            string command = string.Format(":*CLS;:DATA:POINTS {0};:DATA:CLEAR;:DATA:FEED {1};:DATA:FEED:CONT {2}"
                , numberOfDataPoints, fState.ToString(), cState.ToString());
            instrumentChassis.Write_Unchecked(command, this);
        }

        /// <summary>
        /// The Number must be 2 ~ 2500
        /// </summary>
        public int SourceNumberOfSweepPoints
        {
            set
            {
                instrumentChassis.Write_Unchecked(":SOUR:SWEEP:POIN " + value.ToString(), this);
            }
            get
            {
                //send(":SOUR:SWEEP:POIN?");
                return int.Parse(instrumentChassis.Query_Unchecked(":SOUR:SWE:POIN?", this));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public SweepOptions SourceSweepOptions
        {
            set
            {
                string command = ":SOUR:SWE:";

                switch (value)
                {
                    case SweepOptions.UP:
                        command += "DIR UP";
                        break;
                    case SweepOptions.DOWN:
                        command += "DIR DOWN";
                        break;
                    case SweepOptions.LINEAR:
                        command += "SPAC LIN";
                        break;
                    case SweepOptions.LOG:
                        command += "SPAC LOG";
                        break;
                }

                instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetSourceVoltageModeToSweep()
        {
            //send(":*CLS;:SOUR:VOLT:MODE SWE");
            instrumentChassis.Write(":*CLS;:SOUR:VOLT:MODE SWE", this);
        }
        /// <summary>
        /// 
        /// </summary>
        public bool SenseFunctionConcurrent
        {
            get
            {
                bool results = false;

                string rtn = instrumentChassis.Query_Unchecked(":*CLS;:FUNC:CONC?", this);
                switch (rtn.ToUpper())
                {
                    case "OFF":
                    case "0":
                        results = false;
                        break;
                    case "ON":
                    case "1":
                        results = true;
                        break;
                }

                return results;
            }
            set
            {
                //send(":*CLS;:FUNC:CONC ON");
                instrumentChassis.Write(string.Format(":*CLS;:FUNC:CONC {0}", value ? "ON" : "OFF"), this);
            }
        }

        /// <summary>
        /// Select input line
        /// </summary>
        public TriggerLine TriggerInputLine
        {
            set
            {
                instrumentChassis.Write_Unchecked(string.Format(":TRIG:ILIN {0}", (int)value + 1), this);
            }
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:ILINe?", this);

                TriggerLine results = TriggerLine.Line1;

                switch (rtn)
                {
                    case "1":
                        results = TriggerLine.Line1;
                        break;
                    case "2":
                        results = TriggerLine.Line2;
                        break;
                    case "3":
                        results = TriggerLine.Line3;
                        break;
                    case "4":
                        results = TriggerLine.Line4;
                        break;
                }

                return results;
            }
        }
        /// <summary>
        /// Select output line
        /// </summary>
        public TriggerLine TriggerOutputLine
        {
            set
            {
                instrumentChassis.Write_Unchecked(string.Format(":TRIG:OLIN {0}", (int)value + 1), this);
            }
            get
            {
                string rtn = instrumentChassis.Query_Unchecked(":TRIG:OLINe?", this);

                TriggerLine results = TriggerLine.Line1;

                switch (rtn)
                {
                    case "1":
                        results = TriggerLine.Line1;
                        break;
                    case "2":
                        results = TriggerLine.Line2;
                        break;
                    case "3":
                        results = TriggerLine.Line3;
                        break;
                    case "4":
                        results = TriggerLine.Line4;
                        break;
                }

                return results;
            }
        }

        /// <summary>
        /// Set trigger layer delay: 0 ~ 999.9999 seconds
        /// </summary>
        public double TriggerDelay_s
        {
            get
            {
                return double.Parse(instrumentChassis.Query(":TRIGger:DELay?", this));
            }
            set
            {
                //string.Format(":*CLS;:TRIG:DELAY {0}", value);
                instrumentChassis.Write_Unchecked(":TRIG:DEL " + value.ToString(), this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double SourceDelay_s
        {
            set
            {
                //send(":*CLS;:SOUR:DELAY %lf", value);
                string command = String.Format(":SOUR:DEL {0}", value);
                instrumentChassis.Write_Unchecked(command, this);
            }
            get
            {
                return double.Parse(instrumentChassis.Query_Unchecked(":SOUR:DEL?", this));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double SweepStartVoltage_V
        {
            set
            {
                //send(":*CLS;:SOUR:VOLT:START %lf", value);
                instrumentChassis.Write_Unchecked(":*CLS;:SOUR:VOLT:START " + value, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public double SweepStopVoltage_V
        {
            set
            {
                //send(":*CLS;:SOUR:VOLT:STOP %lf", value);
                instrumentChassis.Write_Unchecked(":*CLS;:SOUR:VOLT:STOP " + value, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public double SweepVoltageStep_V
        {
            set
            {
                //send(":*CLS;:SOUR:VOLT:STEP %lf", value);
                instrumentChassis.Write_Unchecked(":*CLS;:SOUR:VOLT:STEP " + value, this);
            }
        }

        /// <summary>
        /// Sets/Gets the Level of Voltage 
        /// </summary>
        public double VoltageLevel_Volt
        {
            get
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != voltageFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't VOLT!!!");
                //}

                string rtn = instrumentChassis.Query_Unchecked(":SOUR:VOLT:LEV:AMPL?", this);

                return Convert.ToDouble(rtn);
            }
            set
            {
                //string functionString = instrumentChassis.Query_Unchecked(":SOUR:FUNC?", this);
                //if (functionString != currentFunctionString)
                //{
                //    throw new InstrumentException("The Function of KE2400 isn't CURR!!!");
                //}

                if (value < -1.05 || value > 1.05)
                {
                    throw new InstrumentException("The Value of Current Range is out of Limits!!!");
                }
                //send(":*CLS;:SOUR:VOLT:LEV:IMM:AMPL %lf", dValue);
                //instrumentChassis.Write_Unchecked(":*CLS;:SOUR:VOLT:LEV:IMM:AMPL " + value.ToString(), this);
                instrumentChassis.Write_Unchecked(":SOUR:VOLT:LEV:AMPL " + value.ToString(), this);
            }
        }
        /// <summary>
        /// Reset the Trigger
        /// </summary>
        public void ResetTrigger()
        {
            //":*CLS;:ABORT;:TRIG:CLE"
            instrumentChassis.Write_Unchecked(":*CLS;:ABORT;:TRIG:CLE", this);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ClearStatusRegister()
        {
            instrumentChassis.Write_Unchecked("*CLS", this);
        }
        /// <summary>
        /// Enable serial request on GPIB bus on operation completed
        /// </summary>
        public void EnableSRQonOPC()
        {
            instrumentChassis.Write_Unchecked("*ESE 1;*SRE 32", this);
        }

        #endregion

        #region IInstType_DigiIOCollection Members

        /// <summary>
        /// Get a Digital IO Line by line number
        /// </summary>
        /// <param name="lineNumber"></param>
        /// <returns></returns>
        public IInstType_DigitalIO GetDigiIoLine(int lineNumber)
        {
            return digiOutLines[lineNumber - 1];
        }

        /// <summary>
        /// Get enumerator for Digital IO Line
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IInstType_DigitalIO> DigiIoLines
        {
            get
            {
                return digiOutLines;
            }
        }

        internal List<IInstType_DigitalIO> digiOutLines;

        #endregion


    }
}
