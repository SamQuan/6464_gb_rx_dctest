// [Copyright]
//
// Bookham Modular Test Engine Library
// Login Manager
//
// Library/LoginManager/LoginManagerCtl.cs
// 
// Author: Bill Godfrey
// Design: Core DD version 1.0

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.Strings;
using Bookham.TestEngine.PluginInterfaces.Security;

namespace Bookham.TestLibrary.Security
{
	/// <summary>
	/// Default login manager plugin GUI class.
	/// </summary>
	public class LoginManagerCtl : Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase
	{
		#region Visual C# generated controls
		private System.Windows.Forms.Panel credentialsPanel;
		private System.Windows.Forms.Label userLabel;
		private System.Windows.Forms.Label passLabel;
		private System.Windows.Forms.TextBox userTextBox;
		private System.Windows.Forms.TextBox passTextBox;
		private System.Windows.Forms.Button loginButton;
		private System.Windows.Forms.Panel outerPanel;
		private System.Windows.Forms.StatusBar loginStatusBar;
		private System.ComponentModel.IContainer components = null;
		#endregion

		/// <summary>
		/// Construct custom login manager control class.
		/// </summary>
		public LoginManagerCtl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			/* Set password box to hide typed in text. */
			this.passTextBox.PasswordChar = '*';  // '\u2606';

			/* Load localised label and button text. */
			this.userLabel.Text = StringResource.Get("LoginManager.LabelText.User");
			this.passLabel.Text = StringResource.Get("LoginManager.LabelText.Pass");
			this.loginButton.Text = StringResource.Get("LoginManager.ButtonText.Login");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.credentialsPanel = new System.Windows.Forms.Panel();
			this.loginButton = new System.Windows.Forms.Button();
			this.passTextBox = new System.Windows.Forms.TextBox();
			this.userTextBox = new System.Windows.Forms.TextBox();
			this.passLabel = new System.Windows.Forms.Label();
			this.userLabel = new System.Windows.Forms.Label();
			this.outerPanel = new System.Windows.Forms.Panel();
			this.loginStatusBar = new System.Windows.Forms.StatusBar();
			this.credentialsPanel.SuspendLayout();
			this.outerPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// credentialsPanel
			// 
			this.credentialsPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.credentialsPanel.Controls.Add(this.loginButton);
			this.credentialsPanel.Controls.Add(this.passTextBox);
			this.credentialsPanel.Controls.Add(this.userTextBox);
			this.credentialsPanel.Controls.Add(this.passLabel);
			this.credentialsPanel.Controls.Add(this.userLabel);
			this.credentialsPanel.Location = new System.Drawing.Point(239, 95);
			this.credentialsPanel.Name = "credentialsPanel";
			this.credentialsPanel.Size = new System.Drawing.Size(328, 128);
			this.credentialsPanel.TabIndex = 0;
			// 
			// loginButton
			// 
			this.loginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.loginButton.Enabled = false;
			this.loginButton.Location = new System.Drawing.Point(240, 80);
			this.loginButton.Name = "loginButton";
			this.loginButton.Size = new System.Drawing.Size(75, 32);
			this.loginButton.TabIndex = 4;
			this.loginButton.Text = "<L>";
			this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
			// 
			// passTextBox
			// 
			this.passTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.passTextBox.Location = new System.Drawing.Point(56, 48);
			this.passTextBox.Name = "passTextBox";
			this.passTextBox.Size = new System.Drawing.Size(256, 20);
			this.passTextBox.TabIndex = 3;
			this.passTextBox.Text = "";
			this.passTextBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			// 
			// userTextBox
			// 
			this.userTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.userTextBox.Location = new System.Drawing.Point(56, 16);
			this.userTextBox.Name = "userTextBox";
			this.userTextBox.Size = new System.Drawing.Size(256, 20);
			this.userTextBox.TabIndex = 2;
			this.userTextBox.Text = "";
			this.userTextBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
			// 
			// passLabel
			// 
			this.passLabel.Location = new System.Drawing.Point(8, 40);
			this.passLabel.Name = "passLabel";
			this.passLabel.Size = new System.Drawing.Size(48, 32);
			this.passLabel.TabIndex = 1;
			this.passLabel.Text = "<P>";
			this.passLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// userLabel
			// 
			this.userLabel.Location = new System.Drawing.Point(8, 8);
			this.userLabel.Name = "userLabel";
			this.userLabel.Size = new System.Drawing.Size(48, 32);
			this.userLabel.TabIndex = 0;
			this.userLabel.Text = "<U>";
			this.userLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// outerPanel
			// 
			this.outerPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.outerPanel.Controls.Add(this.loginStatusBar);
			this.outerPanel.Controls.Add(this.credentialsPanel);
			this.outerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.outerPanel.Location = new System.Drawing.Point(0, 0);
			this.outerPanel.Name = "outerPanel";
			this.outerPanel.Size = new System.Drawing.Size(816, 360);
			this.outerPanel.TabIndex = 2;
			// 
			// loginStatusBar
			// 
			this.loginStatusBar.Location = new System.Drawing.Point(0, 336);
			this.loginStatusBar.Name = "loginStatusBar";
			this.loginStatusBar.Size = new System.Drawing.Size(814, 22);
			this.loginStatusBar.TabIndex = 1;
			// 
			// LoginManagerCtl
			// 
			this.Controls.Add(this.outerPanel);
			this.Name = "LoginManagerCtl";
			this.Size = new System.Drawing.Size(816, 360);
            this.ActOnIntoView += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.IntoViewDlg(this.LoginManagerCtl_ActOnIntoView);
			this.LoginResponse += new Bookham.TestEngine.PluginInterfaces.Security.SecurityCtlBase.LoginResponseDelegate(this.LoginManagerCtl_LoginResponse);
			this.credentialsPanel.ResumeLayout(false);
			this.outerPanel.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// Click handler for login button.
		/// </summary>
		/// <param name="sender">Ignored.</param>
		/// <param name="e">Ignored.</param>
		private void loginButton_Click(object sender, System.EventArgs e)
		{
			/* Send unauthenticated login request to security server. */
			this.SendLoginRequest(
				new LoginManagerLoginRequest(this.userTextBox.Text, this.passTextBox.Text));

			/* Clear password for next time. */
			this.passTextBox.Text = "";
		}

		/// <summary>
		/// Handle login response from security server.
		/// </summary>
		/// <param name="loginMsg">Login response message</param>
		private void LoginManagerCtl_LoginResponse(LoginResponseMsg loginMsg)
		{
			/* Set status bar text. */
            this.loginStatusBar.Text = 
				StringResource.Get("LoginManager.StatusText." + loginMsg.Status.ToString());

			/* Optionally clear out the user id box and focus, depending on these values. */
			switch (loginMsg.Status)
			{
				case LoginRequestStatus.UserDoesNotExist:
				{
					/* If user does not exist, clear out and position cursor. */
					this.userTextBox.Clear();
					this.userTextBox.Focus();
				} break;

				case LoginRequestStatus.InvalidPassword:
				{
					/* Right user, wrong password. */
					this.passTextBox.Focus();
				} break;
			}
		}

		/// <summary>
		/// Handle login control coming into view.
		/// </summary>
		/// <returns>Reference to login button.</returns>
		private System.Windows.Forms.IButtonControl LoginManagerCtl_ActOnIntoView()
		{
			this.userTextBox.Text = "";
			this.passTextBox.Text = "";
			this.loginStatusBar.Text = StringResource.Get("LoginManager.StatusText.LoginDotx3");
			this.userTextBox.Focus();
			return this.loginButton;
		}

		/// <summary>
		/// One of the text boxes has changed. Enable the login button if both have more then
		/// three characters.
		/// </summary>
		/// <param name="sender">Ignored.</param>
		/// <param name="e">Ignored.</param>
		private void textBox_TextChanged(object sender, EventArgs e)
		{
			this.loginButton.Enabled = 
				((this.userTextBox.Text.Length > 3) && (this.passTextBox.Text.Length > 3));
		}
	}
}

