// [Copyright]
//
// Bookham [Coherence Rx DC Test]
// Bookham.TestSolution.TestControl
//
// LoadBatchDialogueCtl.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;
using Bookham.TestEngine.PluginInterfaces.MES;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Test Control Plug-in Template Batch View User Control
    /// </summary>
    internal partial class LoadBatchDialogueCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public LoadBatchDialogueCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }
        /// <summary>
        ///  loading action: initialise Gui information
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lblMesMessage.Text = " Waiting Lot ID Input ...";
            txtSN.Focus();
        }

        /// <summary>
        /// Process recieved messages from the worker thread.
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming sequence number</param>
        /// <param name="respSeq">Response sequence number</param>
        private void LoadBatchDialogueCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
            if (payload.GetType() == typeof(MesLoadDeviceMsg))
            {
                MesLoadDeviceMsg batchLoadRsp = (MesLoadDeviceMsg)payload;

                lblMesMessage.Text = batchLoadRsp.MesMessage;
                btnLoadBatch.Enabled = batchLoadRsp.EnableLoadDutInformation;
                if (batchLoadRsp.NeedDutInfoInput)
                {
                    txtSN.SelectAll();
                    txtSN.Focus();
                }
            }
        }
        /// <summary>
        /// Load batch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            if (txtSN.Text.Trim().Length >= 6)
            {                
                sendToWorker(txtSN.Text.Trim());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == '\n' ) && (e .KeyChar =='\r') ) btnLoadBatch.Focus();
        }

       
    }
}
