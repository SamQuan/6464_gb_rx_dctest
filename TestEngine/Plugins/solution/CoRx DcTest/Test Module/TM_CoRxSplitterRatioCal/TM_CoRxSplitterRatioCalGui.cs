// [Copyright]
//
// Bookham [Coherence Rx DC Test]
// Bookham.TestSolution.TestModules
//
// TM_CoRxSplitterRatioCalGui.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G]

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.Framework.InternalData;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Module Template GUI User Control
    /// </summary>
    public partial class TM_CoRxSplitterRatioCalGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TM_CoRxSplitterRatioCalGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

        }
        /// <summary>
        /// Process message from worker
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="inMsgSeq"></param>
        /// <param name="respSeq"></param>
        private void TM_CoRxSplitterRatioCalGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            if (payload.GetType() == typeof(GuiMsg.CautionOfLaserRqst))
            {
                gbCaution.Visible = true;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(GuiMsg.CancelOflLaserCautionRqst))
            {
                gbCaution.Visible = false;
                this.Refresh();
            }
            else if (payload.GetType() == typeof(DatumString))
            {
                DatumString msg = payload as DatumString;

                if (msg.Name == "GuiTitle")
                {
                    lblTile.Text = msg.Value;
                    lblTile .Refresh();
                }
                else
                {
                    lblTestMsg.Text = msg.Value;
                    lblTestMsg .Refresh();
                }
            }
        }

      
    }
}
