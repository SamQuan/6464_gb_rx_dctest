// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Instr_Ag33120A.cs
//
// Author: wendy.wen, 2009
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.ChassisNS;
// TODO: Uncomment this once you refer to an instrument type
//using Bookham.TestLibrary.InstrTypes;

namespace Bookham.TestLibrary.Instruments
{
    // TODO: 
    // 1. Add reference to the Instrument type you are implementing.
    // 2. Add reference to the Chassis type you are connecting via.
    // 3. Change the base class to the type of instrument you are implementing.
    // 4. Visual Studio can be used to auto-generate function stubs (hover over the base class name).
    // 5. Fill in the gaps.
    /// <summary>
    /// Instrument for Function waveform generator: aglient 33120
    /// </summary>
    public class Instr_Ag33120A : Instrument
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Instr_Ag33120A(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr_Ag33120A = new InstrumentDataRecord(
                "HEWLETT-PACKARD 33120A",				// hardware name 
                "0",  			// minimum valid firmware version 
                "9.0-5.0-1.0");			// maximum valid firmware version 
            ValidHardwareData.Add("Ag33120A", instr_Ag33120A);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassis_33120A = new InstrumentDataRecord(
                "Chassis_Ag33120A",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("Ag33120A", chassis_33120A);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.instrumentChassis = (Chassis_Ag33120A)chassisInit;

            this.cmdFreq = "FREQ";
            this.cmdAmplitude = "VOLT";
            modulationType = EnumModulationType.None;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idnArray = res.Split(',');
                return idnArray[3].Trim();
               
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                // TODO: Update
                string command = "*IDN?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                string[] idnArray = res.Split(',');
                return idnArray[0] + " " + idnArray[1];
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {
            throw new InstrumentException("The method or operation is not implemented.");
        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }

        public void Reset()
        {
            this.instrumentChassis.Write_Unchecked("*CLS;*RST",this);
        }
        #endregion


        #region signal generater
        /// <summary>
        /// 
        /// </summary>
        /// <param name="waveform"></param>
        /// <param name="frequency_HZ"></param>
        /// <param name="amplitude_V"></param>
        /// <param name="Voffset_V"></param>
        public void SetWaveform(EnumWaveFormType waveform, 
            double frequency_HZ, double amplitude_V, double Voffset_V)
        {
            string cmd = "";

            switch (waveform )
            {
                case EnumWaveFormType .NOIS :
                    cmd = string.Format(":APPL:NOIS DEF,{0},{1}", amplitude_V, Voffset_V);
                    break;
                case EnumWaveFormType .RAMP :
                    cmd = string.Format(":APPL:RAMP {0},{1},{2}", frequency_HZ, amplitude_V, Voffset_V);
                    break;
                case EnumWaveFormType .SIN :
                    cmd = string.Format(":APPL:SIN {0},{1},{2}", frequency_HZ, amplitude_V, Voffset_V);
                    break;
                case EnumWaveFormType .SQU :
                    cmd = string.Format(":APPL:SQU {0},{1},{2}", frequency_HZ, amplitude_V, Voffset_V);
                    break;
                case EnumWaveFormType .TRI :
                    cmd = string.Format(":APPL:TRI {0},{1},{2}", frequency_HZ, amplitude_V, Voffset_V);
                    break;
                case EnumWaveFormType .USER :
                    cmd = string.Format(":APPL:USER {0},{1},{2}", frequency_HZ, amplitude_V, Voffset_V);
                    break;
                case EnumWaveFormType .DC :
                default:
                    cmd = ":APPL:DC DEF,DEF," + Voffset_V.ToString();
                    break ;
            }
            this.instrumentChassis.Write_Unchecked(cmd, this);
        }
                
        /// <summary>
        /// Get or Set the Frequency
        /// </summary>
        public double Frequency_Khz
        {
            get
            {
                string command = this.cmdFreq + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res.Replace("+", "");
                }
                double value = double.Parse(res);
                frequency = value / 1000;
                return frequency;
            }
            set
            {
                frequency = value;
                string command = this.cmdFreq + " " + frequency + "KHZ";
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }


        /// <summary>
        /// Get or Set the Peak-peak Amplitude
        /// </summary>
        public double Amplitude_V
        {
            get
            {
                string command = this.cmdAmplitude + "?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res.Replace("+", "");
                }
                amplitude = double.Parse(res); ;
                return amplitude;
            }
            set
            {
                
                //string command = this.cmdAmplitude + " " + amplitude + "MVPP";
                string cmd = ":SOUR:VOLT " + value.ToString()/* + "VPP"*/;
                this.instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        /// <summary>
        /// Get/Set voltage offset
        /// </summary>
        public double Voffset_V
        {
            get
            {
                string cmd = "SOUR:VOLT:OFFS?";
                string rspStr = instrumentChassis.Query_Unchecked(cmd, this);

                double offset = double.Parse(rspStr);
                return offset;
            }
            set
            {
                string cmd = ":SOUR:VOLT:OFFS " + value;
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool OutputEnableSysnc
        {
            get
            {
                string command = "OUTP:SYNC?";
                string res = this.instrumentChassis.Query_Unchecked(command, this);
                if (res.Contains("+"))
                {
                    res.Replace("+", "");
                }
                if (res.Trim() == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                int temp;
                if (value)
                {
                    temp = 1;
                }
                else
                {
                    temp = 0;
                }
                string command = "OUTP:SYNC " + temp.ToString();
                this.instrumentChassis.Write_Unchecked(command, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void OutputOff()
        {
            string cmd = ":FUNC:SHAP DC;:VOLT:OFFS 0";
            this.instrumentChassis.Write_Unchecked(cmd,this);
        }
        /// <summary>
        /// 
        /// </summary>
        public void SetTriggerToBus()
        {
            string command = "TRIG:SOUR BUS";
            this.instrumentChassis.Write_Unchecked(command, this);

        }

        /// <summary>
        /// 
        /// </summary>
        public EnumTriggerSource TriggerSource
        {
            get
            {
                string cmd = ":TRIG:SOUR?";
                string rspStr = instrumentChassis.Query_Unchecked(cmd, this);

                EnumTriggerSource source;
                try
                {
                    source = (EnumTriggerSource)Enum.Parse(typeof(EnumTriggerSource), rspStr, true);
                }
                catch
                {
                    throw new InstrumentException(this.Name +
                        " unkown trigger source response string with " + rspStr);
                }
                return source;
            }
            set
            {
                string cmd = ":TRIG:SOUR " + value .ToString("G");
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public EnumLoadType OutputLoad
        {
            get
            {
                string cmd = ":OUTP:LOAD?";
                string rspStr = instrumentChassis.Query_Unchecked(cmd,this );
                if ( rspStr.Contains( "50"))
                    return EnumLoadType .Ohm50 ;
                else if ( rspStr .Contains("9.9E+37"))
                    return EnumLoadType .INFinity ;
                else 
                    throw new InstrumentException( this .Name + 
                        " unkown output load response string with " + rspStr );
            }
            set
            {
                string cmdPrefix = ":OUTP:LOAD ";
                string cmd = cmdPrefix + (value == EnumLoadType.INFinity ? "MAX" : "MIN");
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EnumVoltUnit OutputUnit
        {
            get
            {
                string cmd = ":VOLT:UNIT?";
                string rspStr = instrumentChassis.Query_Unchecked(cmd, this);
                EnumVoltUnit unit;
                try
                {
                    unit = (EnumVoltUnit)Enum.Parse(typeof(EnumVoltUnit), rspStr);
                }
                catch
                {
                    throw new InstrumentException( this .Name + 
                        ": unkown voltage unit response string with " + rspStr);
                }
                return unit;
            }
            set
            {
                string cmdPrefix = ":VOLT:UNIT ";

                string cmd = cmdPrefix + value.ToString("G");
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public EnumWaveFormType WaveForm
        {
            get
            {
                string cmd = ":FUNC:SHAP?";
                string rspStr = instrumentChassis.Query_Unchecked(cmd,this);

                EnumWaveFormType waveForm;
                try
                {
                    waveForm = (EnumWaveFormType)Enum.Parse(typeof(EnumWaveFormType), rspStr, true);
                }
                catch
                {
                    throw new InstrumentException(this.Name + 
                        " : unkown waveform response from instrument with " + rspStr);
                }
                return waveForm;
            }
            set
            {
                string cmd = ":FUNC:SHAP " + value.ToString();
                instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// Only available to set modulation signal waveform when modulation type is AM & FM
        /// </summary>
        public EnumWaveFormType ModulationWaveForm
        {
            get
            {
                if (modulationType != EnumModulationType.AM && modulationType != EnumModulationType.FM)
                    throw new  InstrumentException( " Non modulation mode applying to the signal when modulation type is "+ modulationType);
                string cmd = " :SOUR:" + modulationType.ToString("G") + ":INT:FUNC?";
                string  rspStr = instrumentChassis.Query_Unchecked(cmd, this);

                EnumWaveFormType waveForm;
                try
                {
                    waveForm = (EnumWaveFormType)Enum.Parse(typeof(EnumWaveFormType), rspStr, true);
                }
                catch
                {
                    throw new InstrumentException(this.Name + 
                        ": Unkown modulating waveform response string with " + rspStr); 
                }
                return waveForm;
            }
            set
            {
                if (value == EnumWaveFormType.DC  )
                    throw new InstrumentException(this .Name +  
                        " illegal setting : Can't set dc signal to be modulation signal! ");
                //if ((value == EnumWaveFormType.NOIS) & 
                //    (modulationType == EnumModulationType .FSK ||
                //     modulationType == EnumModulationType .BM ))
                //    throw new InstrumentException(string .Format ( "{0} illegal setting : Can't set {1} to be modulation signal when ! "));
                if (modulationType != EnumModulationType.AM && modulationType != EnumModulationType.FM)
                    throw new InstrumentException("Can't set modulation signal waveform when modulation mode " + modulationType);

                String cmd = " :SOUR:" + modulationType.ToString("G") + ":INT:FUNC " + value.ToString("G");
                instrumentChassis.Write_Unchecked(cmd, this);

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EnumModulationType ModulationType
        {
            get
            {
                string cmd = ":SOUR:AM:STAT?";
                string rspStr = instrumentChassis.Query_Unchecked(cmd, this);
                if (rspStr.Contains("1"))
                {
                    modulationType = EnumModulationType.AM;
                    return modulationType;
                }

                cmd = ":SOUR:BM:STAT?";
                rspStr = instrumentChassis.Query_Unchecked(cmd,this);
                if ( rspStr .Contains("1"))
                {
                    modulationType = EnumModulationType .BM ;
                    return modulationType ;
                }

                cmd = ":SOUR:FM:STAT?";
                rspStr = instrumentChassis .Query_Unchecked (cmd ,this );
                if (rspStr.Contains("1"))
                {
                    modulationType = EnumModulationType.FM;
                    return modulationType;
                }

                cmd = ":SOUR:FSK:STAT?";
                rspStr = instrumentChassis.Query_Unchecked(cmd, this);
                if ( rspStr .Contains ("1"))
                {
                    modulationType = EnumModulationType .FSK ;
                    return modulationType ;
                }

                cmd = ":SOUR:SWE:STAT?";
                rspStr = instrumentChassis.Query_Unchecked(cmd, this);
                if ( rspStr .Contains ("1"))
                {
                    modulationType = EnumModulationType .SWE ;
                    return modulationType;
                }
                return EnumModulationType .None;
            }
            set
            {
                // turn on modulation of specified type
                if (value == EnumModulationType.None)
                {
                    DisableModulation();
                }
                else 
                {
                    string cmd = ":SOUR:" + value.ToString("G") + ":STAT ON";
                    instrumentChassis.Write_Unchecked(cmd, this);
                }
                // set the modulation type to variable for other use
                modulationType = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public EnumModulationSource ModulationSource
        {
            get
            {
                if (modulationType == EnumModulationType.None) 
                    return EnumModulationSource.None;
                else if ((modulationType == EnumModulationType.FM) ||
                    (modulationType == EnumModulationType.SWE))
                    return EnumModulationSource.INT;
                else
                {
                    string cmd = ":SOUR:" + modulationType.ToString() + ":SOUR?";
                    string rspStr = instrumentChassis.Query_Unchecked(cmd, this);

                    EnumModulationSource source;
                    try
                    {
                        source = (EnumModulationSource)Enum.Parse(
                            typeof(EnumModulationSource), rspStr, true);
                    }
                    catch
                    {
                        throw new InstrumentException(this.Name +
                            ": Unknown modulation source response string with : " + rspStr);
                    }
                    return source;
                }
                
            }
            set
            {
                if (modulationType == EnumModulationType.None ||
                    modulationType == EnumModulationType.FM ||
                    modulationType == EnumModulationType.SWE)
                    return;
                else
                {
                    if ((modulationType == EnumModulationType.AM &&
                                value == EnumModulationSource.INT) ||
                        (value == EnumModulationSource.BOTH &&
                        (modulationType == EnumModulationType.BM ||
                        modulationType == EnumModulationType.FSK)))
                        throw new InstrumentException(this.Name +
                            " can't set modulation source to be " + value +
                            " when modulation type is " + modulationType.ToString());
                    else
                    {
                        string cmd = ":SOUR:" + modulationType.ToString("G") + ":SOUR " + value.ToString("G");
                        instrumentChassis.Write_Unchecked(cmd, this);
                    }

                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void DisableModulation()
        {
            if (modulationType == EnumModulationType.None) return;
            // Turn off modulation
            string cmd = "SOUR:" + modulationType.ToString("G") + ":STAT OFF";
            instrumentChassis.Write_Unchecked(cmd, this);
            // set modulationType to be none
            modulationType = EnumModulationType.None;
        }

        public bool ModulationState
        {
            get
            {
                if (modulationType == EnumModulationType.None) return false;
                string cmd = ":SOUR:" + modulationType.ToString("G") + ":STAT?";
                string rsp = this .instrumentChassis .Query_Unchecked( cmd,this );
                return rsp.Contains("ON") || rsp.Contains("1");
            }
            set
            {
                if (modulationType == EnumModulationType.None) return;
                string cmd = ":SOUR:" + modulationType.ToString("G") + ":STAT " +
                    (value ? "ON" : "OFF");
                this.instrumentChassis.Write_Unchecked(cmd, this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="waveForm"> modulate the carrier with a sine, square, triangle,
        /// ramp, noise,or arbitrary waveform </param>
        /// <param name="modulationSource">accept an external modulating signal, 
        /// or both an internal and External modulating signal</param>
        /// <param name="frequency_Hz"> the modulating frequency , between 10 mHz and 20 kHz </param>
        /// <param name="modulationDepth">the modulation depth in percent, between 0% and 120%</param>
        public void SetAmModulation( EnumWaveFormType waveForm, 
            EnumModulationSource modulationSource, double frequency_Hz, double modulationDepth)
        {
            modulationType = EnumModulationType.AM;
            //AM:INTernal:FUNCtion {SINusoid|SQUare|TRIangle|RAMP|NOISe|USER}
            ModulationWaveForm = waveForm;
            ModulationSource = modulationSource;
            // AM:DEPTh {<depth in percent>|MINimum|MAXimum}
            string cmd = ":AM:DEPTh " + modulationDepth;
            this.instrumentChassis.Write_Unchecked(cmd,this);
            //AM:INTernal:FREQuency {<frequency>|MINimum|MAXimum}
            cmd = ":AM:INT:FREQ " + frequency_Hz;
            this.instrumentChassis.Write_Unchecked(cmd,this);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source">If you are using the external gated burst mode, 
        ///                         select the external gate source</param>
        /// <param name="count"> number of cycles per burst, 1 ~ 50,000 cycles</param>
        /// <param name="rate"> The frequency at which internally triggered bursts 
        /// are generated,10 mHz and 50 kHz</param>
        /// <param name="startPhase_Deg">starting phase of the burst to any value between 
        /// -360 degrees and +360 degrees</param>
        public void SetBmModulation(  EnumModulationSource source, 
            int count, double rate, double startPhase_Deg)
        {
            modulationType = EnumModulationType.BM;
            ModulationSource = source;
            //BM:NCYCles {<# cycles>|INFinity|MINimum|MAXimum}
            string cmd = ":BM:NCYC " + count;
            this.instrumentChassis.Write_Unchecked(cmd,this);
            //BM:PHASe {<degrees>|MINimum|MAXimum}
            cmd = ":BM:PHAS " + startPhase_Deg;
            this.instrumentChassis.Write_Unchecked(cmd,this);
            //BM:INTernal:RATE {<frequency>|MINimum|MAXimum}
            cmd = ":BM:INT:RATE " + rate;
            this.instrumentChassis.Write_Unchecked(cmd,this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="waveForm"> Modulate the carrier with a sine, square, 
        /// triangle, ramp, noise, or arbitrary waveform.</param>
        /// <param name="frequency_Hz"> Modulating frequency to any value between 10 mHz and 10 kHz</param>
        /// <param name="peakFreqDeviation_Hz"> Peak frequency deviation, between 10 mHz and 7.5 MHz </param>
        public void setFmModulation(EnumWaveFormType waveForm, 
            double frequency_Hz, double peakFreqDeviation_Hz)
        {
            modulationType = EnumModulationType.FM;
            //FM:INTernal:FUNCtion {SINusoid|SQUare|TRIangle|RAMP|NOISe|USER}
            ModulationWaveForm = waveForm;
            //FM:DEViation {<peak deviation in Hz>|MINimum|MAXimum}
            string cmd = ":FM:DEV " + peakFreqDeviation_Hz;
            this.instrumentChassis.Write_Unchecked(cmd, this);
            //FM:INTernal:FREQuency {<frequency>|MINimum|MAXimum}
            cmd = ":FM:INT:FREQ " + frequency_Hz;
            this .instrumentChassis .Write_Unchecked (cmd ,this );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"> Internal or external FSK source</param>
        /// <param name="hopFreq_Hz"> FSK ��hop�� frequency.</param>
        /// <param name="rate"> the output frequency ��shifts�� between the carrier frequency and the hop frequency</param>
        public void setFSKModulation( EnumModulationSource  source, double hopFreq_Hz, double rate)
        {
            ModulationType = EnumModulationType.FSK;
            ModulationSource = source;
            //FSKey:FREQuency {<frequency>|MINimum|MAXimum}
            string cmd = ":FSK:FREQ " + hopFreq_Hz;
            this.instrumentChassis.Write_Unchecked(cmd, this);
            //FSKey:INTernal:RATE {<rate in Hz>|MINimum|MAXimum}
            cmd = ":FSK:INT:RATE " + rate;
            this.instrumentChassis.Write_Unchecked(cmd, this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startFreq"></param>
        /// <param name="stopFreq"></param>
        /// <param name="Tsweep_S"> Seconds required to sweep from the start frequency to the stop frequency </param>
        /// <param name="sweepMode"> linear or logarithmic spacing frequency .</param>
        public void setFrequencySweep( double startFreq, double stopFreq,
                                double Tsweep_S, EnumSweepMode sweepMode)
        {
            modulationType = EnumModulationType.SWE;
            // FREQuency:STARt {<frequency>|MINimum|MAXimum}
            string cmd = ":FREQ:STAR " + startFreq;
            this.instrumentChassis.Write_Unchecked(cmd, this);
            // FREQuency:STOP {<frequency>|MINimum|MAXimum}
            cmd = ":FREQ:STOP " + stopFreq;
            this.instrumentChassis.Write_Unchecked(cmd, this);
            // SWEep:SPACing {LINear|LOGarithmic}
            cmd = ":SWE:SPAC " + sweepMode.ToString("G");
            this.instrumentChassis.Write_Unchecked(cmd, this);
            // SWEep:TIME {<seconds>|MINimum|MAXimum}
            cmd = ":SWE:TIME " + Tsweep_S;
            this.instrumentChassis.Write_Unchecked(cmd, this);
        }
        #endregion
        #region Define Enum data
        /// <summary>
        /// 
        /// </summary>
        public enum EnumWaveFormType
        {
            /// <summary>
            /// SINusoid
            /// </summary>
            SIN,
            /// <summary>
            /// SQUare
            /// </summary>
            SQU,
            /// <summary>
            /// TRIangle
            /// </summary>
            TRI,
            /// <summary>
            /// RAMP
            /// </summary>
            RAMP,
            /// <summary>
            /// NOISe
            /// </summary>
            NOIS,
            /// <summary>
            /// DC
            /// </summary>
            DC,
            /// <summary>
            /// USER
            /// </summary>
            USER,
        }
        /// <summary>
        /// 
        /// </summary>
        public enum EnumVoltUnit
        {
            /// <summary>
            /// peak - peak , which is the default setting
            /// </summary>
            VPP,
            /// <summary>
            /// RMS Value
            /// </summary>
            VRMS,
            /// <summary>
            /// 
            /// </summary>
            DBM,
            
        }
        /// <summary>
        /// 
        /// </summary>
        public enum EnumLoadType
        {
            /// <summary>
            /// 50 Ohms, which is the minimum value
            /// </summary>
            Ohm50,
            /// <summary>
            /// 9.9E+37, which is the maximum value
            /// </summary>
            INFinity,
            
        }
        /// <summary>
        /// 
        /// </summary>
        public enum EnumTriggerSource
        {
            /// <summary>
            /// IMMediate
            /// </summary>
            IMM,
            /// <summary>
            /// EXTernal
            /// </summary>
            EXT,
            /// <summary>
            /// BUS, Software trigger
            /// </summary>
            BUS,
        }
        /// <summary>
        /// Enum Modulation source
        /// </summary>
        public enum EnumModulationSource
        {
            /// <summary>
            /// INTernal
            /// </summary>
            INT,
            /// <summary>
            /// EXTernal
            /// </summary>
            EXT,
            /// <summary>
            /// BOTH
            /// </summary>
            BOTH,
            /// <summary>
            /// No modulation
            /// </summary>
            None,
        }
        /// <summary>
        /// Enum modulation type
        /// </summary>
        public enum EnumModulationType
        {
            /// <summary>
            /// Amplitude modulation
            /// </summary>
            AM,
            /// <summary>
            /// Bust Modulation
            /// </summary>
            BM,
            /// <summary>
            /// Frequency modulation
            /// </summary>
            FM,
            /// <summary>
            /// Frequency-shift keying
            /// </summary>
            FSK,
            /// <summary>
            /// Frequency Sweep 
            /// </summary>
            SWE,
            /// <summary>
            /// 
            /// </summary>
            None,
 
        }
        /// <summary>
        /// Enum voltage 
        /// </summary>
        public enum EnumSweepMode
        {
            /// <summary>
            /// LINear
            /// </summary>
            LIN,
            /// <summary>
            /// LOGarithmic
            /// </summary>
            LOG,
        }
        #endregion
        #region Private data
        // TODO: Change to the actual chassis type
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag33120A instrumentChassis;

        private double frequency;
        private double amplitude;
        private EnumModulationType modulationType;
        
        #endregion

        #region command area

        private string cmdFreq;
        private string cmdAmplitude;

        #endregion
    }
}
