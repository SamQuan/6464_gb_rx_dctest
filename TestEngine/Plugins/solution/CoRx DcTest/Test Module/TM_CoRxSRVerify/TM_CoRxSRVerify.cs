// [Copyright]
//
// Bookham [Coherent RX DC Test]
// Bookham.TestSolution.TestModules
//
// TM_CoRxSRVerify.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G]

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Utilities;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// Coherent RX DC Test, Verify the SR is within toerance with cal data
    /// </summary>
    public class TM_CoRxSRVerify : ITestModule
    {
        #region ITestModule Members
        /// <summary>
        /// Varify splitter ratio and cal data
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, 
            DatumList configData, InstrumentCollection instruments, 
            ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            // Show message on GUI to indicate in which optical splitter path is under verify
            engine.GuiShow();
            engine.GuiToFront();

            DatumString title = configData.GetDatumString("GuiTitle");
            engine.SendToGui(title);

            CoRxSourceInstrsChain OpcInstrs = (CoRxSourceInstrsChain)configData.ReadReference("OpcChainInstrs");
            double sourceOpcPower_dBm = configData.ReadDouble("LaserSourcePower_dBm");
            double voaAtten_dB = configData.ReadDouble("VoaAtten_dB");
            double maxSrTollerance_dB = configData.ReadDouble("MaxDeltaSR_dB");
            int maxRetryTimes = configData.ReadSint32("MaxRetryTimes");

            #region Get frequency information
            EnumFrequencySetMode freqSetMode  = EnumFrequencySetMode.ByChan;
            double[] freqArr_Verify=null;

            if (configData.IsPresent("FreqArr2Verify"))
            {
                freqArr_Verify = configData.ReadDoubleArray("FreqArr2Verify");                
                freqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            else if (configData.IsPresent("WavelenArr2Verify"))
            {
                freqArr_Verify = configData.ReadDoubleArray("WavelenArr2Verify");
                freqSetMode = EnumFrequencySetMode.ByWavelen;
            }
            else if (configData.IsPresent("ChanArr2Verify"))
            {
                freqArr_Verify = configData.ReadDoubleArray("ChanArr2Verify");
                freqSetMode = EnumFrequencySetMode.ByChan;
            }
            
            #endregion

            // switch optical off and indicate operator to insert VOA ouput to OPM head
           OpcInstrs.OpticalChainOutputEnable = false;
           System.Threading.Thread.Sleep(100);
           OpcInstrs.SafeShutDown();
                

            engine .SendToGui(new GuiMsg .CancelOflLaserCautionRqst() );
            
            engine.GuiUserAttention();
            
            engine.SendToGui(new DatumString("TestMsg",
                "Laser is on, don't move the fibre connector !"));
            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());

            OpcInstrs.LaserSource.Power_dBm = sourceOpcPower_dBm;
            OpcInstrs.OpticalChainOutputEnable = true;

            List<StrcSRVerifyDataUnit> srDataList = new List<StrcSRVerifyDataUnit>();
            bool isSrOk = true;

            double maxSr = double.NegativeInfinity;
            double minSr = double.PositiveInfinity;
            
            // verify splitter ratia at each freq point
            foreach (double freq in freqArr_Verify)
            {
                int loopCount = 0;
                double deltaSR = 999;

                StrcSRVerifyDataUnit srData = new StrcSRVerifyDataUnit();
                SplitterRatioDataUnit srReading;

                do   // if the error is too much, repeat verify for times
                {
                    if (freqSetMode == EnumFrequencySetMode.ByChan)
                        OpcInstrs.ChainChan = (int)freq;
                    else if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                        OpcInstrs.ChainFrequency_GHz = freq;
                    else
                        OpcInstrs.ChainWaveLen_nm = freq;

                    srData.FreqWavelenChan = freq;
                    srData.SR_Cal = OpcInstrs.GetCurrentSR();
                    srReading = OpcInstrs.MeasSplitterRatio(voaAtten_dB);
                    deltaSR = Math.Abs(srData.SR_Cal - srReading.Ratio);
                    // If SR is out of tollerance, promote the user choose to make action
                    if (deltaSR > maxSrTollerance_dB)
                    {
                        engine.SendToGui(new DatumString("TestMsg",
                            "Splitter ratio delta to the calibrated file is " +  deltaSR .ToString()));
                        ButtonId rsp = engine .ShowYesNoUserQuery ( string .Format( 
                            " the splitter ratio is clarified as {0} while the measured value is {1}\n"+
                            " would you like to check the VOA Output fibre?\n"+
                            " DO NOT MOVE THE FIBRE until you get the safe information promoted! ", 
                            srData .SR_Cal, srReading .Ratio ));
                        if (rsp == ButtonId.Yes)   // choose to check fibre and iTLA
                        {
                            OpcInstrs.OpticalChainOutputEnable = false;
                            System.Threading.Thread.Sleep(100);
                            OpcInstrs.SafeShutDown();  // To ensure the itla is shut down
                            
                            engine.GuiUserAttention();
                            engine .SendToGui( new GuiMsg .CancelOflLaserCautionRqst());
                            engine.ShowContinueUserQuery ( " The laser is shut down now,"+ 
                                " it is save to get the VOA Output fibre to check or Clean,\n"+
                                "DO NOT CHOOSE 'Continue' UNTILL YOU INSERT THE FIBRE TO power meter head");
                            
                            engine.SendToGui(new GuiMsg.CautionOfLaserRqst());
                            engine.SendToGui(new DatumString("TestMsg",
                                "Laser is on, don't move the fibre connector !"));
                            OpcInstrs.OpticalChainOutputEnable = true;
                            System.Threading.Thread.Sleep(100);
                        }
                    }
                }
                while (loopCount++ < maxRetryTimes && deltaSR > maxSrTollerance_dB);               
                
                srData.SRReading = srReading;
                srData.DeltaRatio = srReading .Ratio - srData .SR_Cal ;
                srDataList.Add(srData);
                if (deltaSR > maxSrTollerance_dB) isSrOk = false;

                maxSr = Math.Max(maxSr, srReading.Ratio);
                minSr = Math.Min(minSr, srReading.Ratio);

            }  // for

            OpcInstrs.OpticalChainOutputEnable = false;
            bool isCalDataSaveOK = false;
            isCalDataSaveOK = OpcInstrs.SplitterRatiosCalData.SaveSplitterRatioCalData();

            OpcInstrs.SafeShutDown();

            // return data
            DatumList returnData = new DatumList();
            returnData.AddSint32("IsSRVerifyOK", (isSrOk ? 1 : 0));
            returnData.AddDouble("MaxSR", maxSr);
            returnData.AddDouble("MinSR", minSr);
            // if configdata contains "FileDirectory" and the path is available, save all sr data to csv 
            try
            {
                string fileDirectory = configData.ReadString("FileDirectory");
                string filePath = "";
                if (Path.GetFullPath(fileDirectory) != "")   // if the folder exist, choose to save the verify data to file
                {
                    filePath = WriteSRDataToFile(fileDirectory, freqSetMode, srDataList);
                }
                returnData.AddFileLink("SRDataFilePath", filePath);
            }
            catch (Exception  ex)
            {
                throw ex;
            }
            return returnData;
        }
        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(TM_CoRxSRVerifyGui)); }
        }

        /// <summary>
        /// Write splitter datas to CSV file
        /// </summary>
        /// <param name="fileDirectory"> directory to save file</param>
        /// <param name="freqSetMode"> frequency setting mode, set by chan/frequency/wavelen </param>
        /// <param name="srDataList"> SR Data list </param>
        /// <returns></returns>
        internal string WriteSRDataToFile(String fileDirectory, 
            EnumFrequencySetMode freqSetMode,List<StrcSRVerifyDataUnit> srDataList)
        {
            List<string> srDataStrs = new List<string>();

            string header;
            if (freqSetMode == EnumFrequencySetMode.ByChan)
                header = "Verify Channel Number,";
            else if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                header = "Verify Frequency_GHz,";
            else
                header = "Verify Wavelen_nm,";
            header += "Channel, Freq_Ghz,Wvelen_nm,SplitterRatio,SR_Cal,DeltaSr_dB";

            // Build data to string
            srDataStrs.Add(header);
            foreach (StrcSRVerifyDataUnit dataTemp in srDataList)
            {
                StringBuilder dataStr = new StringBuilder();
                dataStr.Append(dataTemp.FreqWavelenChan);
                dataStr.Append(",");
                dataStr.Append(dataTemp.SRReading.ChanNum);
                dataStr.Append(",");
                dataStr.Append(dataTemp.SRReading.Frequency_Ghz);
                dataStr.Append(",");
                dataStr.Append(dataTemp.SRReading.Wavelen_nM);
                dataStr.Append(",");
                dataStr.Append(dataTemp.SRReading.Ratio);
                dataStr.Append(",");
                dataStr.Append(dataTemp.SR_Cal);
                dataStr.Append(",");
                dataStr.Append(dataTemp.DeltaRatio);
                dataStr.Append(",");
                srDataStrs.Add(dataStr.ToString ());
            }
            string filename = Util_GenerateFileName.GenWithTimestamp(fileDirectory, "SRVerify", "", ".csv");
            using (CsvWriter writer = new CsvWriter())
            {
                try
                {
                    writer.WriteFile(filename, srDataStrs);                    
                }
                catch (Exception ex)
                {
                    filename = "";
                    throw ex;
                }
            }
            return filename;
        }

        internal struct StrcSRVerifyDataUnit
        {
            /// <summary>
            /// Frequency/Wavelen/Chan at which to verify SR 
            /// </summary>
            public double FreqWavelenChan;
            /// <summary>
            /// delta
            /// </summary>
            public double DeltaRatio;
            public double SR_Cal;
            public SplitterRatioDataUnit SRReading;
        }
        #endregion
    }
}
