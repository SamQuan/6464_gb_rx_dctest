using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NUnit.Framework;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.Framework.Logging;

namespace EquipmentTest_Inst_Ag662x
{
	/// <exclude />	
	[TestFixture]
	public class Inst_Ag662x_Test
	{
		/// <exclude />
		/// <summary>
		/// Constructor 
		/// </summary>
		public Inst_Ag662x_Test()
		{ }

		/// <exclude />
		[TestFixtureSetUp]
		public void Setup()
		{
			// initialise logging domain			
			Initializer.Init();
			BugOut.SetOutputToConsole(true);
		}

		/// <exclude />
		[TestFixtureTearDown]
		public void ShutDown()
		{
			testInst.OutputEnabled = false;
			testChassis.IsOnline = false;

			// Test end
			LogWrite("Test Finished!");
		}

		[Test]
		public void T01_CreateInst(string visaResource)
		{
			testChassis = new Chassis_Ag662x("TestChassis", "Chassis_Ag662x", visaResource);
			LogWrite("Chassis created OK");
			testInst = new Inst_Ag662x("TestInst", "Inst_Ag662x", "1", "", testChassis);
			LogWrite("Instrument created OK");
		}

		/// <exclude />
		[Test]
		[ExpectedException(typeof(System.Exception))]
		public void T02_TryCommsNotOnline()
		{
			//Cannot try this as logging shuts down the app after the exception
			try
			{
				LogWrite(testInst.HardwareIdentity);
			}
			catch (ChassisException e)
			{
				LogWrite("Expected exception :" + e.Message);
			}
		}

		[Test]
		public void T03_SetOnline()
		{
			testChassis.IsOnline = true;
			LogWrite("Chassis IsOnline set true OK");
			testInst.IsOnline = true;
			LogWrite("Instrument IsOnline set true OK");
		}

		[Test]
		public void T04_DriverVersion()
		{
			LogWrite(testInst.DriverVersion);
		}

		[Test]
		public void T05_FirmwareVersion()
		{
			LogWrite(testInst.FirmwareVersion);
		}

		[Test]
		public void T06_HardwareID()
		{
			LogWrite(testInst.HardwareIdentity);
		}

		[Test]
		public void T07_SetDefaultState()
		{
			testInst.SetDefaultState();
		}

		[Test]
		public void T08_EnableOutput()
		{
			LogWrite("Output state : " + testInst.OutputEnabled.ToString());
			testInst.OutputEnabled = true;
			LogWrite("Output state set to 'true' OK");
			LogWrite("Output state : " + testInst.OutputEnabled.ToString());
		}

		[Test]
		public void T09_Current_amp()
		{
			LogWrite("Current_amp : " + testInst.CurrentSetPoint_amp);
			testInst.CurrentSetPoint_amp = 0.210;
			LogWrite("Current_amp set to 0.210");
			LogWrite("Current_amp : " + testInst.CurrentSetPoint_amp + " : "
				+ testInst.CurrentActual_amp);
			testInst.CurrentSetPoint_amp = 0.0;
		}

		[Test]
		public void T10_Voltage_volt()
		{
			LogWrite("Voltage_volt : " + testInst.VoltageSetPoint_Volt);
			testInst.VoltageSetPoint_Volt = 0.5;
			LogWrite("Voltage_volt set to 0.5");
			LogWrite("Voltage_volt : " + testInst.VoltageSetPoint_Volt + " : "
				+ testInst.VoltageActual_Volt);
			testInst.VoltageSetPoint_Volt = 0.0;
		}

		[Test]
		public void T11_Voltage_compliance()
		{
			LogWrite("Voltage_compliance : =  " + testInst.VoltageComplianceSetPoint_Volt);
			testInst.VoltageComplianceSetPoint_Volt = 1.5;
			LogWrite("Voltage_compliance set to 1.5");



			LogWrite("Voltage_compliance : " + testInst.VoltageComplianceSetPoint_Volt);
			testInst.VoltageComplianceSetPoint_Volt = 0.0;

		}

		[Test]
		public void T12_CheckSomeErrors()
		{
			LogWrite("setting Voltage_compliance to 200v ");
			try
			{
				testInst.VoltageComplianceSetPoint_Volt = 200;
			}
			catch (ChassisException e)
			{
				LogWrite("Caught expected exception: expect Error 5 : " + e.Message);
			}
			LogWrite("Voltage_compliance now reads = " + testInst.VoltageComplianceSetPoint_Volt);

			LogWrite("trying to set 200V on supply ");

			LogWrite("Voltage_volt = " + testInst.VoltageSetPoint_Volt);
			try
			{
				testInst.VoltageSetPoint_Volt = 200;
			}
			catch (ChassisException e)
			{
				LogWrite("Caught expected exception: expect Error 5 : " + e.Message);
			}
			LogWrite("VoltageSetPoint_Volt = " + testInst.VoltageSetPoint_Volt);


			LogWrite("trying to set 15V ");


			try
			{
				testInst.VoltageSetPoint_Volt = 15;
			}
			catch (InstrumentException e)
			{
				LogWrite("Caught expected exception: expect Error 8 : " + e.Message);
			}

			testInst.VoltageComplianceSetPoint_Volt = 16.0;

			testInst.VoltageSetPoint_Volt = 14;
			LogWrite("VoltageSetPoint_Volt = " + testInst.VoltageSetPoint_Volt);

			LogWrite("Setting current to : " + testInst.CurrentSetPoint_amp);
			testInst.CurrentSetPoint_amp = 0.210;


			//Cannot debug the range switching bits properly as i don't have a 6624x


		}

		[Test]
		public void T13_TimingComparision(bool Safemode)
		{
			int i;

			System.DateTime StartTime;
			System.DateTime EndTime;
			System.TimeSpan Duration;

			StartTime = System.DateTime.Now;
			double reading;

			LogWrite("Start Time: " + StartTime);
			int maxloops = 50;

			testInst.SafeModeOperation = Safemode;

			for (i = 0; i < maxloops; i++)
			{
				//doing 1 voltage compliance, 2 volt sets, 1 current set,
				// and 4 reads of set points.
				testInst.VoltageComplianceSetPoint_Volt = 16.0;
				reading = testInst.VoltageComplianceSetPoint_Volt;
				testInst.VoltageSetPoint_Volt = 14;
				reading = testInst.VoltageSetPoint_Volt;
				testInst.CurrentSetPoint_amp = 0.5;
				reading = testInst.CurrentSetPoint_amp;
				testInst.VoltageSetPoint_Volt = 14;
				reading = testInst.VoltageSetPoint_Volt;
			}
			EndTime = System.DateTime.Now;
			LogWrite("End Time: " + EndTime);
			Duration = EndTime - StartTime;
			LogWrite("SafeMode = " + Safemode + "\n " +
				"Did " + maxloops + " sets of operations in " + Duration.TotalSeconds + " seconds");

		}

		[Test]
		public void T14_MultiChannelReads()
		{
			testInst2 = new Inst_Ag662x("TestInst", "Inst_Ag662x", "2", "", testChassis);
			LogWrite("Instrument 2 created OK");
			// put online
			testInst2.IsOnline = true;

			testInst.OutputEnabled = false;
			testInst.VoltageSetPoint_Volt = 1.0;
			testInst.CurrentSetPoint_amp = 0.1;
			testInst.SafeModeOperation = true;

			testInst2.OutputEnabled = false;
			testInst2.VoltageSetPoint_Volt = 2.0;
			testInst2.CurrentSetPoint_amp = 0.2;
			testInst2.SafeModeOperation = true;

			testInst.OutputEnabled = true;
			testInst2.OutputEnabled = true;
			int counter = 0;
			while (true)
			{
				//double vset1 = testInst.VoltageSetPoint_Volt;
				//double vset2 = testInst2.VoltageSetPoint_Volt;
				double vact1 = testInst.VoltageActual_Volt;
				double vact2 = testInst2.VoltageActual_Volt;
				//double iset1 = testInst.CurrentSetPoint_amp;
				//double iset2 = testInst2.CurrentSetPoint_amp;
				double iact1 = testInst.CurrentActual_amp;
				double iact2 = testInst2.CurrentActual_amp;
				//System.Threading.Thread.Sleep(1000);
				counter++;
				LogWrite("Count=" + counter);
			}


		}

		private void LogWrite(string message)
		{
			BugOut.WriteLine(BugOut.WildcardLabel, message);
		}

		/// <summary>
		/// Chassis & Inst references
		/// </summary>
		private Chassis_Ag662x testChassis;
		private Inst_Ag662x testInst;
		private Inst_Ag662x testInst2;

	}



}
