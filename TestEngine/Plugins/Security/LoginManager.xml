<?xml version="1.0"?>
<doc>
    <assembly>
        <name>LoginManager</name>
    </assembly>
    <members>
        <member name="T:Bookham.TestLibrary.Security.LoginManager">
            <summary>
            Default Login Manager will simple User/Password login. 
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManager.#ctor">
            <summary>
            Construct login manager, called by plugin loader.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManager.LogoutNotification(System.String,Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel,Bookham.TestEngine.PluginInterfaces.Security.ILogWriter)">
            <summary>
            Process a logout notification.
            </summary>
            <remarks>This class cannot fail a logout request.</remarks>
            <param name="user">Logging out user.</param>
            <param name="priv">Former user's privilege.</param>
            <param name="logWriter">Reference to log writer object.</param>
            <returns>Success result.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManager.LoginRequest(Bookham.TestEngine.PluginInterfaces.Security.LoginRequestMsg,Bookham.TestEngine.PluginInterfaces.Security.ILogWriter)">
            <summary>
            Process a login request, scanning the XML file for the login details.
            </summary>
            <param name="msg">Unauthenticated credentials from the login GUI control.</param>
            <param name="logWriter">Reference to log writer object.</param>
            <returns>Success/failure result.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManager.refreshUserInfo(Bookham.TestEngine.PluginInterfaces.Security.ILogWriter)">
            <summary>
            Reload the UserInfoXml, but keep the old one if it throws anything. All
            exceptions thrown will be logged but otherwise ignored.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Security.LoginManager.userInfo">
            <summary>
            Stored user information.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Security.LoginManager.Control">
            <summary>
            Return type of GUI control to use with this plugin.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.Security.LoginManagerCtl">
            <summary>
            Default login manager plugin GUI class.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.#ctor">
            <summary>
            Construct custom login manager control class.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.Dispose(System.Boolean)">
            <summary>
            Clean up any resources being used.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.InitializeComponent">
            <summary>
            Required method for Designer support - do not modify
            the contents of this method with the code editor.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.loginButton_Click(System.Object,System.EventArgs)">
            <summary>
            Click handler for login button.
            </summary>
            <param name="sender">Ignored.</param>
            <param name="e">Ignored.</param>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.LoginManagerCtl_LoginResponse(Bookham.TestEngine.PluginInterfaces.Security.LoginResponseMsg)">
            <summary>
            Handle login response from security server.
            </summary>
            <param name="loginMsg">Login response message</param>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.LoginManagerCtl_ActOnIntoView">
            <summary>
            Handle login control coming into view.
            </summary>
            <returns>Reference to login button.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerCtl.textBox_TextChanged(System.Object,System.EventArgs)">
            <summary>
            One of the text boxes has changed. Enable the login button if both have more then
            three characters.
            </summary>
            <param name="sender">Ignored.</param>
            <param name="e">Ignored.</param>
        </member>
        <member name="T:Bookham.TestLibrary.Security.LoginManagerLoginRequest">
            <summary>
            Inherited message payload (login manager -> security server) for login credentials.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.LoginManagerLoginRequest.#ctor(System.String,System.String)">
            <summary>
            Constructs the login request message.
            </summary>
            <param name="userId">User id, passed into base class.</param>
            <param name="passWord">Password text.</param>
        </member>
        <member name="F:Bookham.TestLibrary.Security.LoginManagerLoginRequest.PassWord">
            <summary>
            Gets the login password.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.Security.PasswordHash">
            <summary>
            Utility class holding public static functions for hashing and checking passwords.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Security.PasswordHash.salt">
            <summary>
            Secret salt added to all hashes. Do not reveal to operators.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.PasswordHash.Hash(System.String,Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel,System.String)">
            <summary>
            Transform user information into a salted hash.
            </summary>
            <param name="user">User name.</param>
            <param name="priv">Privilege level.</param>
            <param name="pass">Plain text password.</param>
            <returns>Base64 string of hash.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Security.PasswordHash.TestPassword(System.String,Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel,System.String,System.String)">
            <summary>
            Test login credentials against a loaded hash.
            </summary>
            <param name="user">User name.</param>
            <param name="priv">User privilege.</param>
            <param name="pass">Typed in password plaintext.</param>
            <param name="hash">Hash loaded from user data.</param>
            <returns>True if password correct. False on no match.</returns>
        </member>
        <member name="M:Bookham.TestLibrary.Security.PasswordHash.#ctor">
            <summary>
            Private constructor to prevent construction.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.Security.UserInfoXml">
            <summary>
            Class holding cached user information and tools to load the file.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.userDataFileName">
            <summary>
            Location of username list.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.malformedPrivilegeFormat">
            <summary>
            Template for a Malformed privilege field error message.
            </summary>
            <remarks><list type="bullet">
            <item>{0} will be replaced with short file name.</item>
            <item>{1} will be replaced with line number where error was detected.</item>
            <item>{2} will be replaced with the poisition on the line where the error was detected.</item>
            <item>{3} will be replaved with the malformed data value.</item>
            </list></remarks>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.accessDeniedFormat">
            <summary>
            Template for a file access denied error.
            </summary>
            <remarks>{0} will be replaced with file name which failed to open for lack of privilege.</remarks>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.fileNotFoundFormat">
            <summary>
            Template for a file not found error message.
            </summary>
            <remarks>{0} will be replaced with full path of missing file.</remarks>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.generalIoFailureFormat">
            <summary>
            Template for a general IO failure message.
            </summary>
            <remarks><list type="bullet">
            <item>{0} will be replaced with failed filename.</item>
            <item>{1} Will be replaced with specific error message.</item>
            </list></remarks>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.malformedXmlFormat">
            <summary>
            Template for a malformed XML error message.
            </summary>
            <remarks><list type="bullet">
            <item>(0} will be replaced with the short filename of the malformed XML file.</item>
            <item> {1} will be replaced withe the error generated by the XML parser.</item>
            </list></remarks>
        </member>
        <member name="M:Bookham.TestLibrary.Security.UserInfoXml.#ctor">
            <summary>
            Load user info from file. Any exceptions thrown in the process will be allowed to
            fall.
            </summary>
            <exception cref="T:Bookham.TestLibrary.Security.UserInfoXmlException">
            Thrown on expected errors, including;
            <list type="bullet">
            <item>File not found,</item>
            <item>Access denied,</item>
            <item>IO failure,</item>
            <item>XML parser failure,</item>
            <item>Bad Privilege value,</item>
            </list>
            </exception>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserInfoXml.users">
            <summary>
            Collection of UserRecord instances, key'd by user name.
            </summary>
        </member>
        <member name="P:Bookham.TestLibrary.Security.UserInfoXml.Item(System.String)">
            <summary>
            Gets the loaded user record by user name key.
            </summary>
        </member>
        <member name="T:Bookham.TestLibrary.Security.UserInfoXmlException">
            <summary>
            Exception indicating a failure when the User Info file is scanned.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.UserInfoXmlException.#ctor(System.String)">
            <summary>
            Construct exception with message.
            </summary>
            <param name="message">Message text.</param>
        </member>
        <member name="M:Bookham.TestLibrary.Security.UserInfoXmlException.#ctor(System.String,System.Exception)">
            <summary>
            Construct exception with message and wrapped inner exception.
            </summary>
            <param name="message">Message text.</param>
            <param name="caughtException">Inner exception.</param>
        </member>
        <member name="T:Bookham.TestLibrary.Security.UserRecord">
            <summary>
            Class containing information about a single user.
            </summary>
        </member>
        <member name="M:Bookham.TestLibrary.Security.UserRecord.#ctor(System.String,System.String,Bookham.TestEngine.PluginInterfaces.Security.PrivilegeLevel)">
            <summary>
            Construct a single user record.
            </summary>
            <param name="user">User's name.</param>
            <param name="passHash">User's hashed password.</param>
            <param name="priv">User's privilege.</param>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserRecord.User">
            <summary>
            Gets the user's name.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserRecord.PassHash">
            <summary>
            Gets the user's password hash string.
            </summary>
        </member>
        <member name="F:Bookham.TestLibrary.Security.UserRecord.Priv">
            <summary>
            Gets the user's privilege setting.
            </summary>
        </member>
    </members>
</doc>
