using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Instruments;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestSolution.Instruments
{
    /// <summary>
    /// Instruments on optical insput  chain, it will includes:
    /// 1) laser source, IInstType_RxTunableLaserSource
    /// 2) VOA
    /// 3) OPM to monitor laser into Rx
    /// 4) OPM to monitor the laser on the other splitter output
    /// 5) Splitter ratio data for the splitter for the two splitter output that cal
    /// on each frequencies ( Wavenlen or channels) that may used during the whole test
    /// </summary>
    public class CoRxSourceInstrsChain
    {
        /// <summary>
        /// Laser Source 
        /// </summary>
        public Inst_CoRxITLALaserSource LaserSource;
        /// <summary>
        /// Voa in optical input path, if the power is not adjust, set it to null
        /// </summary>
        public InstType_OpticalAttenuator Voa;

        /// <summary>
        /// OPM monitoring the lsaer power from one of the splitter, we get get the laser into RX by reference to it, 
        /// </summary>
        public InstType_OpticalPowerMeter OPM_Ref;
        /// <summary>
        /// OPM to measure the direct laser input to Rx from the spllitter  
        /// </summary>
        public InstType_OpticalPowerMeter OPM_Mon;

        /// <summary>
        /// splitter ratio data for splitter in the specified optical path 
        /// </summary>
        public RxSplitterRatioCalData SplitterRatiosCalData;
        /// <summary>
        /// Flag to indicate if the splitter ratio data is available
        /// </summary>
        public bool IsSplitterRatioCalOK;
        /// <summary>
        /// power reading tolerance in input power adjust
        /// </summary>
        public double PowerSetTolerance_dB;  
        /// <summary>
        ///  Voa's default settings
        /// </summary>
        public double sngVoaDefAtt_dB=1;
        /// <summary>
        /// Max time to adjust voa to meet required input power
        /// </summary>
        public double MaxPowerTuningCount;
        /// <summary>
        /// Max asttenuation allowed by VOA
        /// </summary>
        public double MaxAllowedAtt=99;
        // Max power meter reading when laser source shut down, define it for shut down justify
        public double MaxShutDownPower_dBm = -40;

        /// <summary>
        /// Read splitter ratio value at all frequency from database,
        /// the database informatin contains in the .xml configuration file       
        /// </summary>
        /// <param name="splitterName"></param>
        /// <param name="configFilePath"> database configuration file's full path, it should be XML file</param>
        /// <param name="dataTableName"> data table name in XML file</param>        
        public void  SetupSplitterRatioData(string splitterName, 
            string configFilePath, string dataTableName)
        {            
            SplitterRatiosCalData = new RxSplitterRatioCalData(splitterName, configFilePath, dataTableName);
            if (!SplitterRatiosCalData.IsDBCalDataOk)
                IsSplitterRatioCalOK = false;
            else
                IsSplitterRatioCalOK = true;
        }
        /// <summary>
        /// Measure optical path's splitter ratio with series frequency
        /// if each splitter ratio at each frequency are within max and min limit, return true
        /// else return false
        /// </summary>
        /// <param name="frequency_Start_GHz"></param>
        /// <param name="frequency_Stop_GHz"></param>
        /// <param name="frequency_Step_GHz"></param>
        /// <param name="voa_Att"></param>
        /// <param name="sr_Max"></param>
        /// <param name="sr_Min"></param>
        public bool MeasSplitterRatioByFrequency(double frequency_Start_GHz, double frequency_Stop_GHz, 
            double frequency_Step_GHz, double voa_Att, double sr_Max, double sr_Min )
        {
            InstType_OpticalPowerMeter.MeterMode mode_Mon = OPM_Mon.Mode;  // read it for recover power meter
            InstType_OpticalPowerMeter.MeterMode mode_Ref = OPM_Ref.Mode;
            OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;

            OpticalChainOutputEnable = true;
            SplitterRatiosCalData.ClearCalData ();
            bool isDataOk = true;
            for (double freq = frequency_Start_GHz; 
                freq <= frequency_Stop_GHz; freq += frequency_Step_GHz)
            {
                ChainFrequency_GHz = freq;
                SplitterRatioDataUnit newData = MeasSplitterRatio(voa_Att);
                SplitterRatiosCalData.AddCalData(newData);
                if (newData.Ratio > sr_Max || newData.Ratio < sr_Min) isDataOk = false;
            }
            OpticalChainOutputEnable = false;
            OPM_Mon.Mode = mode_Mon;
            OPM_Ref.Mode = mode_Ref;
            return isDataOk;
        }
        
        /// <summary>
        /// Measure optical path's splitter ratio with series wavelen
        /// if each splitter ratio at each wavelen are within max and min limit, return true
        /// else return false
        /// </summary>
        /// <param name="wavelen_Start_nM"></param>
        /// <param name="wavelen_Stop_nM"></param>
        /// <param name="wavelen_Step_nM"></param>
        /// <param name="voa_Att"></param>
        /// <param name="sr_Max"> Maximun Splliter ratio allowed </param>
        /// <param name="sr_Min"> Minimun Splliter ratio allowed </param>
        public bool MeasSplitterRatioByWavelen(double wavelen_Start_nM, double wavelen_Stop_nM, 
            double wavelen_Step_nM, double voa_Att,double sr_Max, double sr_Min )
        {
            InstType_OpticalPowerMeter.MeterMode mode_Mon = OPM_Mon.Mode;
            InstType_OpticalPowerMeter.MeterMode mode_Ref = OPM_Ref.Mode;
            OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OpticalChainOutputEnable = true;

            SplitterRatiosCalData.ClearCalData ();
            bool isDataOk = true;
            for (double wavelen = wavelen_Start_nM; 
                wavelen <= wavelen_Stop_nM; wavelen += wavelen_Step_nM)
            {
                ChainWaveLen_nm= wavelen;
                SplitterRatioDataUnit newData = MeasSplitterRatio(voa_Att);
                SplitterRatiosCalData.AddCalData(newData);
                if (newData.Ratio > sr_Max || newData.Ratio < sr_Min) isDataOk = false;
            }

            OpticalChainOutputEnable = false;
            OPM_Mon.Mode = mode_Mon;
            OPM_Ref.Mode = mode_Ref;
            return isDataOk;
        }
        
        /// <summary>
        /// Measure optical path's splitter ratio with series channel number
        /// if each splitter ratio at each channel are within max and min limit, return true
        /// else return false
        /// </summary>
        /// <param name="chan_Start"></param>
        /// <param name="chan_Stop"></param>
        /// <param name="chan_Step"></param>
        /// <param name="voa_Att"></param>
        /// <param name="sr_Max"> Maximun Splliter ratio allowed </param>
        /// <param name="sr_Min"> Minimun Splliter ratio allowed </param>
        /// <returns></returns>
        public bool MeasSplitterRatioByChan(int chan_Start, int chan_Stop, int chan_Step, 
            double voa_Att,double sr_Max, double sr_Min )
        {
            InstType_OpticalPowerMeter.MeterMode mode_Mon = OPM_Mon.Mode;
            InstType_OpticalPowerMeter.MeterMode mode_Ref = OPM_Ref.Mode;
            OPM_Mon.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OPM_Ref.Mode = InstType_OpticalPowerMeter.MeterMode.Absolute_dBm;
            OpticalChainOutputEnable = true;

            SplitterRatiosCalData.ClearCalData ();
            bool isDataOk = true;            
            for (int chan = chan_Start; chan <= chan_Stop; chan += chan_Step)
            {
                ChainChan = chan ;
                SplitterRatioDataUnit newData = MeasSplitterRatio(voa_Att);
                SplitterRatiosCalData.AddCalData(newData);
                if (newData.Ratio > sr_Max || newData.Ratio < sr_Min) isDataOk = false;
            }

            OpticalChainOutputEnable = false;
            OPM_Mon.Mode = mode_Mon;
            OPM_Ref.Mode = mode_Ref;
            return isDataOk;
        }

        /// <summary>
        /// Get/Set wavelen apply to the whole instruments in optical chain that might involves wavelen setting
        /// </summary>
        public double  ChainWaveLen_nm
        {
            set 
            {
                LaserSource.WaveLen_nM = value;
                setPathWaveLen();
            }
            get
            {
                return LaserSource.WaveLen_nM;
            }
        }
        /// <summary>
        /// Get/Set frequency apply to the whole instruments in optical chain that might involves frequency setting
        /// </summary>
        public double ChainFrequency_GHz
        {
            set
            {
                LaserSource.Frequency_GHz = value;
                setPathWaveLen();
            }
            get
            {
                return LaserSource.Frequency_GHz;
            }
        }
        /// <summary>
        /// Get/Set the channel number of the otical instruments chain, 
        /// only available for instruemnts chain with laser source with ITU Channels such as  ITLA 
        /// </summary>
        public int ChainChan
        {
            set
            {
                LaserSource.CurrentChan =value;
                setPathWaveLen();
            }
            get
            {
                return LaserSource.CurrentChan;
            }
        }
        /// <summary>
        /// Get optical path's output power
        /// </summary>
        /// <returns></returns>
        public double GetOpticalPower_dBm()
        {
            double power =OPM_Ref.ReadPower() - GetCurrentSR();
            return power;
        }
        /// <summary>
        /// Get splitter ratio for currently wavelen in the optical instruemnt chain 
        /// </summary>
        /// <returns></returns>
        public double GetCurrentSR()
        {
            if ( SplitterRatiosCalData == null )
                return double.PositiveInfinity;

            double wavelen = LaserSource.WaveLen_nM;
            double sr = SplitterRatiosCalData.GetSrByWavelen(wavelen);
            if (sr == RxSplitterRatioCalData.InfinteRatio)
                throw new Exception("No calibrated splitter ratio available for " + wavelen + "nM");
            return sr;
        }
        /// <summary>
        /// Adjust optical instruments chain's output power to specified value
        /// if the output power is within tolerance with adjust times not exceed maximun adjust times,
        /// the function will return true, else, return false
        /// </summary>
        /// <param name="power_dBm"></param>
        /// <returns></returns>
        public bool SetOpticalPower(double power_dBm)
        {
            // if double.PositiveInfinity, don't need to adjust input power

            if (Voa == null) return true;
            if (power_dBm == double.PositiveInfinity) return true;

            if (power_dBm > LaserSource.MaxPower_dBm) return false;
           
            bool bPowerOk = false;
            Voa.Attenuation_dB = sngVoaDefAtt_dB;

            if (!OpticalChainOutputEnable) OpticalChainOutputEnable = true;
            int intLoopCounter = 0;
            double sngPower_Low = power_dBm - PowerSetTolerance_dB;
            double sngPower_Upper = power_dBm + PowerSetTolerance_dB;
            double powerReading = GetOpticalPower_dBm();

            double att_dB;
            double att2PowerEff = 1;
            while ((powerReading < sngPower_Low || powerReading > sngPower_Upper) &&
                intLoopCounter++ < MaxPowerTuningCount)
            {
                att_dB = Voa.Attenuation_dB;
                double deltaAtt = (powerReading - power_dBm) * att2PowerEff;
                att_dB += deltaAtt;
                if (att_dB < 0 || att_dB > MaxAllowedAtt) break;
                Voa.Attenuation_dB = att_dB;
                System.Threading.Thread.Sleep(100);
                double deltaPower = GetOpticalPower_dBm() - powerReading;
                powerReading += deltaPower;

                double newAtt2PowerEff = (deltaPower != 0 ? deltaAtt / deltaPower : att2PowerEff);
                att2PowerEff = (newAtt2PowerEff + att2PowerEff) / 2;
            }
            if (powerReading >= sngPower_Low && powerReading <= sngPower_Upper)
                bPowerOk = true;

            return bPowerOk;
        }
        
        /// <summary>
        /// Set/Get laser source and voa output status
        /// </summary>
        public bool OpticalChainOutputEnable
        {
            set
            {
                LaserSource.OutputEnable = value;
                
                if (Voa != null)
                {
                    try
                    {
                        Voa.OutputEnabled = value;
                    }
                    catch
                    { // if Voa module can't support this function, use "try-catch" to avoid exception
                    }
                }
                LaserSource.OutputEnable = value;
            }
            get
            {
                if (Voa != null)
                    return (Voa.OutputEnabled && LaserSource.OutputEnable);
                else
                    return LaserSource.OutputEnable;
            }
        }

        /// <summary>
        /// Measure splitter ratio at optical instruments chain's wavelen in effect
        /// </summary>
        /// <param name="voa_Att"></param>
        /// <returns></returns>
        public SplitterRatioDataUnit MeasSplitterRatio(double voa_Att)
        {
            double freq_Ghz = LaserSource.Frequency_GHz;
            double wavelen_nM = LaserSource.WaveLen_nM;
            int chan = LaserSource.CurrentChan;
            
            if (Voa != null )  Voa.Attenuation_dB = voa_Att;

            double pReading_Mon = OPM_Mon.ReadPower(); // *CalFactor_OpmMon - CalOffset_OpmMon;
            double pReading_Ref = OPM_Ref.ReadPower(); // *CalFactor_OpmRef - CalOffset_OpmRef;

            double ratio =  pReading_Ref - pReading_Mon;

            SplitterRatioDataUnit dataUnit = new SplitterRatioDataUnit();
            dataUnit.Ratio = ratio;
            dataUnit.ChanNum = chan;
            dataUnit.Frequency_Ghz = freq_Ghz;
            dataUnit.Wavelen_nM = wavelen_nM;
            
            return dataUnit;
        }
        /// <summary>
        /// Set laser source's wavelen to VOA, OPM, etc. such intruemnts might involves frequency in measurement
        /// </summary>
        void setPathWaveLen( )
        {
            double wavelen_nM  = LaserSource .WaveLen_nM ;
            if (Voa != null)
                Voa.Wavelength_nm = wavelen_nM;
            OPM_Mon.Wavelength_nm = wavelen_nM;
            OPM_Ref.Wavelength_nm = wavelen_nM;
        }

        /// <summary>
        /// Check if laser source is shut down or not
        /// </summary>
        /// <returns></returns>
        public bool IsShutDown()
        {
            double range = OPM_Ref.Range;
            OPM_Ref.Range = MaxShutDownPower_dBm;

            double power = OPM_Ref.ReadPower();

            OPM_Ref.Range = range;
            return (power < MaxShutDownPower_dBm || double.IsPositiveInfinity(power));
        }
        /// <summary>
        /// Confirm the laser source ouput is off to ensure safe operation
        /// </summary>
        public void SafeShutDown()
        {
            if (IsShutDown() ) return ;
            
            System.Threading.Thread.Sleep(100);
            ChainChan = 1;
            OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(100);
            if (!IsShutDown())
                throw new InstrumentException("Can't shut down Itla, Please reset the iTLA!");
        }
        
    }
    /// <summary>
    /// Enum way to set frequency
    /// </summary>
    public enum EnumFrequencySetMode
    {
        /// <summary>
        /// 
        /// </summary>
        ByChan,
        /// <summary>
        /// 
        /// </summary>
        ByFrequency,
        /// <summary>
        /// 
        /// </summary>
        ByWavelen,
    }
}
