// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// Bookham.TestLibrary.TestModules/ModuleGui.cs
// 
// Author: paul.annetts
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestLibrary.TestModules
{
    /// <summary>
    /// Empty GUI for this module.
    /// </summary>
    public partial class ModuleGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Empty GUI for this module.
        /// </summary>
        public ModuleGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }
    }
}
