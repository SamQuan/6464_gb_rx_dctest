Option Strict Off
Option Explicit On
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("MSAStandardCommands_NET.MSAStandardCommands")> Public Class MSAStandardCommands
	
	
	
	
	Public Function SetTxCommand(ByRef data1 As Byte, ByRef data2 As Byte, ByRef data3 As Byte) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		
		Send(1) = data1
		Send(2) = data2
		Send(3) = data3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(SET_TX_COMMAND, Send, 3, Receive, 0) Then
			SetTxCommand = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Tx Command Register.")
			SetTxCommand = False
		End If
	End Function
	
	
	
	Public Function ReadTxCommand(ByRef data1 As Byte, ByRef data2 As Byte, ByRef data3 As Byte) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_TX_COMMAND, Send, 0, Receive, 3) Then
			data1 = Receive(1)
			data2 = Receive(2)
			data3 = Receive(3)
			ReadTxCommand = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Tx Command Register.")
			ReadTxCommand = False
		End If
	End Function
	
	
	
	
	Public Function SetRxThreshold(ByRef rxthreshold As Double) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim RxDTV As Short
		
		If (rxthreshold < 0) Or (rxthreshold > 100) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Rx Threshold out-of-range in SetRxThreshold.")
			SetRxThreshold = False
		Else
			' Convert to 8.8 fixed point number.
			RxDTV = Int(rxthreshold * 256)
			Call ConvIntToBytes(Send, RxDTV)
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If Comms.SendMSA(SET_RX_THRESHOLD, Send, 2, Receive, 0) Then
				SetRxThreshold = True
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to set Rx Threshold.")
				SetRxThreshold = False
			End If
		End If
	End Function
	
	
	
	
	
	Public Function ReadRxThreshold() As Double
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim RxDTV As Short
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_RX_THRESHOLD, Send, 0, Receive, 2) Then
			RxDTV = ConvBytesToInt(Receive)
			ReadRxThreshold = CDbl(RxDTV / 256#)
		Else
			ReadRxThreshold = GENERAL_ERROR_CODE
		End If
	End Function
	
	
	
	
	Public Function SaveTxRegister() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SaveTxRegister = Comms.SendMSA(SAVE_TX_COMMAND, Send, 0, Receive, 0)
	End Function
	
	
	
	Public Function SaveRxRegister() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SaveRxRegister = Comms.SendMSA(SAVE_RX_COMMAND, Send, 0, Receive, 0)
	End Function
	
	
	
	Public Function RestoreTxRegister() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		RestoreTxRegister = Comms.SendMSA(RESTORE_TX_COMMAND, Send, 0, Receive, 0)
	End Function
	
	
	
	Public Function RestoreRxRegister() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		RestoreRxRegister = Comms.SendMSA(RESTORE_RX_COMMAND, Send, 0, Receive, 0)
	End Function
	
	
	
	
	Public Function EnterProtectMode() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		EnterProtectMode = Comms.SendMSA(ENTER_PROTECT_MODE, Send, 0, Receive, 0)
	End Function
	
	
	
	
	Public Function EnterVendorProtectMode(ByRef ModulePassword As String) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(10) As Byte
		Dim pwlen As Byte
		Dim indx As Short
		
		If Len(ModulePassword) > 8 Then
			pwlen = 8
		Else
			pwlen = Len(ModulePassword)
		End If
		
		For indx = 1 To pwlen
			Send(indx) = Asc(Mid(ModulePassword, indx, 1))
		Next indx
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(ENTER_PROTECT_MODE, Send, pwlen, Receive, 0) Then
			EnterVendorProtectMode = True
		Else
			EnterVendorProtectMode = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set module into Vendor Protect Mode.")
		End If
	End Function
	
	
	
	Public Function EnterSoftMode() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(SOFT_MODE, Send, 0, Receive, 0) Then
			EnterSoftMode = True
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set module into Soft Mode.")
			EnterSoftMode = False
		End If
	End Function
	
	
	
	Public Function EnterPinMode() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(PIN_CONTROL_MODE, Send, 0, Receive, 0) Then
			EnterPinMode = True
		Else
			EnterPinMode = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to Enter Pin Mode.")
		End If
	End Function
	
	
	
	Public Function ExitProtectMode() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(EXIT_PROTECT_MODE, Send, 0, Receive, 0) Then
			ExitProtectMode = True
		Else
			ExitProtectMode = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to Exit Protect Mode.")
		End If
	End Function
	
	
	
	Public Function ResetCPN() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(RESET_CPN, Send, 0, Receive, 0) Then
			ResetCPN = True
		Else
			ResetCPN = False
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to Reset CPN.")
		End If
	End Function
	
	
	
	Private Function Byte2Band(ByRef bandbyte As Byte) As String
		If bandbyte = &H43s Then
			Byte2Band = "C"
		ElseIf bandbyte = &H4Cs Then 
			Byte2Band = "L"
		ElseIf bandbyte = &H53s Then 
			Byte2Band = "S"
		Else
			Byte2Band = "E"
		End If
	End Function
	
	
	
	
	Private Function Bytes2Channel(ByRef highbyte As Byte, ByRef lowbyte As Byte) As Double
		' The bottom 4 bits of the lowbyte represent the fractional part to a resolution
		' of 0.0625 (i.e. 1 bit = 0.0625)
		Dim chan As Double
		chan = CDbl(highbyte) * 256# + CDbl(lowbyte)
		Bytes2Channel = chan / 16#
	End Function
	
	
	
	
	Public Function SelectITUChannel(ByRef band As String, ByRef ituchan As Double) As Boolean
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		Dim chan As Double
		
		On Error GoTo InternalErr
		
		chan = ituchan
		If band = "C" Then
			Send(1) = &H43s
		ElseIf band = "L" Then 
			Send(1) = &H4Cs
		ElseIf band = "S" Then 
			Send(1) = &H53s
		Else
			' Invalid band code.
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Invalid band code (" & band & ") in SelectITUChannel.")
			SelectITUChannel = False
			Exit Function
		End If
		
		If (chan < MIN_ITU_CHANNEL_NUMBER) Or (chan > MAX_ITU_CHANNEL_NUMBER) Or ((Int(chan / 0.0625) * 0.0625) <> chan) Then
			' Invalid channel number.
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Invalid channel number in SelectITUChannel (chan=" & CStr(chan) & ").")
			SelectITUChannel = False
			Exit Function
		Else
			chan = chan * 16
			'UPGRADE_WARNING: Mod has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
			Send(3) = CByte(chan Mod 256)
			Send(2) = CByte((chan - Send(3)) \ 256)
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SelectITUChannel = Comms.SendMSA(SET_ITU_CHANNEL, Send, 3, Receive, 0)
			If Not SelectITUChannel Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed channel change in SelectITUChannel (chan=" & CStr(ituchan) & ").")
			End If
		End If
		Exit Function
		
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SelectITUChannel")
		SelectITUChannel = False
	End Function
	
	
	
	
	
	Public Function GetFirstChannel(ByRef band As String, ByRef chan As Double) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(3) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetFirstChannel = Comms.SendMSA(READ_FIRST_ITU_CHAN, Send, 0, Receive, 3)
		
		If GetFirstChannel Then
			chan = Bytes2Channel(Receive(2), Receive(3))
			band = Byte2Band(Receive(1))
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read first ITU channel.")
			chan = GENERAL_ERROR_CODE
			band = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetFirstChannel")
		chan = GENERAL_ERROR_CODE
		band = strGENERAL_ERROR_CODE
		GetFirstChannel = False
	End Function
	
	
	
	
	Public Function GetLastChannel(ByRef band As String, ByRef chan As Double) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetLastChannel = Comms.SendMSA(READ_LAST_ITU_CHAN, Send, 0, Receive, 3)
		
		If GetLastChannel = True Then
			chan = Bytes2Channel(Receive(2), Receive(3))
			band = Byte2Band(Receive(1))
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read last ITU channel.")
			chan = GENERAL_ERROR_CODE
			band = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetLastChannel")
		chan = GENERAL_ERROR_CODE
		band = strGENERAL_ERROR_CODE
		GetLastChannel = False
	End Function
	
	
	
	
	Public Function GetITUChannel(ByRef band As String, ByRef chan As Double) As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetITUChannel = Comms.SendMSA(READ_ITU_CHANNEL, Send, 0, Receive, 3)
		
		If GetITUChannel = True Then
			chan = Bytes2Channel(Receive(2), Receive(3))
			band = Byte2Band(Receive(1))
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read current ITU channel.")
			chan = GENERAL_ERROR_CODE
			band = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetITUChannel")
		chan = GENERAL_ERROR_CODE
		band = strGENERAL_ERROR_CODE
		GetITUChannel = False
	End Function
	
	
	
	
	
	Public Function ChannelSpacing() As Double
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_ITU_CHAN_SPACING, Send, 0, Receive, 1) Then
			ChannelSpacing = CDbl(Receive(1)) * 3.125
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read ITU channel spacing.")
			ChannelSpacing = GENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		ChannelSpacing = GENERAL_ERROR_CODE
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ChannelSpacing")
	End Function
	
	
	
	
	Public Sub GetModuleRevisionCodes(ByRef hwrev As String, ByRef swrev As String)
        Dim strData As String = ""
		Dim ReturnCode As Byte
		Dim intIndex As Short
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(16) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_REVISION_CODES, Send, 0, Receive, 16) Then
			For intIndex = 1 To 16
				If Receive(intIndex) = 0 Then
					strData = strData & " "
				Else
					strData = strData & Chr(Receive(intIndex))
				End If
			Next intIndex
			hwrev = Left(strData, 8)
			swrev = Right(strData, 8)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read module revision codes.")
			hwrev = "Error"
			swrev = "Error"
		End If
		Exit Sub
		
InternalErr: 
		hwrev = "Error"
		swrev = "Error"
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetModuleRevisionCodes")
	End Sub
	
	
	
	
	
	Public Function GetUnitSerialNumber() As String
		Dim strData As String
		Dim ReturnCode As Byte
		Dim intIndex As Short
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(16) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_UNIT_SERIAL_NUMBER, Send, 0, Receive, 16) Then
			strData = ""
			For intIndex = 1 To 16
				If Receive(intIndex) = 0 Then
					strData = strData & " "
				Else
					strData = strData & Chr(Receive(intIndex))
				End If
			Next intIndex
			GetUnitSerialNumber = strData
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read unit serial number.")
			GetUnitSerialNumber = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetUnitSerialNumber")
		GetUnitSerialNumber = strGENERAL_ERROR_CODE
	End Function
	
	
	
	
	
	Public Function GetUnitPartNumber() As String
		Dim strData As String
		Dim ReturnCode As Byte
		Dim intIndex As Short
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(16) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_UNIT_PART_NUMBER, Send, 0, Receive, 16) Then
			strData = ""
			For intIndex = 1 To 16
				If Receive(intIndex) = 0 Then
					strData = strData & " "
				Else
					strData = strData & Chr(Receive(intIndex))
				End If
			Next intIndex
			GetUnitPartNumber = strData
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read unit part number.")
			GetUnitPartNumber = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetUnitPartNumber")
		GetUnitPartNumber = strGENERAL_ERROR_CODE
	End Function
	
	
	
	
	Public Function GetManufactureDate() As String
		Dim strData As String
		Dim ReturnCode As Byte
		Dim intIndex As Short
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(8) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_MANUFACTURE_DATE, Send, 0, Receive, 8) Then
			strData = Chr(Receive(1)) & Chr(Receive(2)) & Chr(Receive(3)) & Chr(Receive(4))
			strData = strData & "-" & Chr(Receive(5)) & Chr(Receive(6))
			strData = strData & "-" & Chr(Receive(7)) & Chr(Receive(8))
			GetManufactureDate = strData
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read manufacture date.")
			GetManufactureDate = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		GetManufactureDate = strGENERAL_ERROR_CODE
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetManufactureDate")
	End Function
	
	
	
	
	Public Function GetSupplier() As String
		Dim ReturnCode As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_SUPPLIER_CODE, Send, 0, Receive, 1) Then
			GetSupplier = Hex(Receive(1))
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read supplier id.")
			GetSupplier = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		GetSupplier = strGENERAL_ERROR_CODE
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetSupplier")
	End Function
	
	
	
	
	Public Function GetModuleType() As String
		Dim ReturnCode As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_MODULE_TYPE, Send, 0, Receive, 1) Then
			GetModuleType = Hex(Receive(1))
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read module type.")
			GetModuleType = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		GetModuleType = strGENERAL_ERROR_CODE
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetModuleType")
	End Function
	
	
	
	
	
	Private Function Get3ByteDouble(ByRef code As Byte) As Double
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(code, Send, 0, Receive, 3) Then
			Get3ByteDouble = CDbl(Conv3BytesToLong(Receive))
		Else
			Get3ByteDouble = GENERAL_ERROR_CODE
		End If
		Exit Function
		
InternalErr: 
		Get3ByteDouble = GENERAL_ERROR_CODE
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "Get3ByteDouble")
	End Function
	
	
	
	
	
	Public Function LaserBiasCurrent() As Double
		LaserBiasCurrent = Get3ByteDouble(READ_LASER_BIAS_CURRENT)
		If LaserBiasCurrent <> GENERAL_ERROR_CODE Then
			LaserBiasCurrent = LaserBiasCurrent / 1000# ' Convert to mA
		End If
	End Function
	
	
	
	
	Public Function LaserOutputPower() As Double
		LaserOutputPower = Get3ByteDouble(READ_LASER_OUTPUT_POWER)
	End Function
	
	
	
	
	Public Function SubMountTemp() As Double
		SubMountTemp = Get3ByteDouble(READ_SUBMOUNT_TEMP)
		If SubMountTemp <> GENERAL_ERROR_CODE Then
			SubMountTemp = SubMountTemp / 1000# ' Convert to degC
		End If
	End Function
	
	
	
	
	Public Function RxACPower() As Double
		RxACPower = Get3ByteDouble(READ_RX_AC_POWER)
	End Function
	
	
	
	
	Public Function RxAvgPower() As Double
		RxAvgPower = Get3ByteDouble(READ_RX_AVG_POWER)
	End Function
	
	
	
	
	Public Function LaserWavelength() As Double
		LaserWavelength = Get3ByteDouble(READ_LASER_WAVELENGTH)
	End Function
	
	
	
	
	Public Function ModuleTemp() As Double
		ModuleTemp = Get3ByteDouble(READ_MODULE_TEMP)
		If ModuleTemp <> GENERAL_ERROR_CODE Then
			ModuleTemp = ModuleTemp / 1000# ' Convert to degC
		End If
	End Function
	
	
	
	
	Public Function APDTemp() As Double
		APDTemp = Get3ByteDouble(READ_APD_TEMP)
		If APDTemp <> GENERAL_ERROR_CODE Then
			APDTemp = APDTemp / 1000# ' Convert to degC
		End If
	End Function
	
	
	
	
	Public Function LaserOutputPowerDbm() As Double
		LaserOutputPowerDbm = Get3ByteDouble(READ_LASER_OUTPUT_POWER_DBM)
		If LaserOutputPowerDbm <> GENERAL_ERROR_CODE Then
			LaserOutputPowerDbm = LaserOutputPowerDbm / 100#
		End If
	End Function
	
	
	
	
	Public Function RxAvgPowerDbm() As Double
		RxAvgPowerDbm = Get3ByteDouble(READ_RX_AVG_POWER_DBM)
		If RxAvgPowerDbm <> GENERAL_ERROR_CODE Then
			RxAvgPowerDbm = RxAvgPowerDbm / 100#
		End If
	End Function
	
	
	
	
	Public Function LaserAbsoluteTemperature() As Double
		LaserAbsoluteTemperature = Get3ByteDouble(READ_LASER_ABSOLUTE_TEMP)
		If LaserAbsoluteTemperature <> GENERAL_ERROR_CODE Then
			LaserAbsoluteTemperature = LaserAbsoluteTemperature / 1000#
		End If
	End Function
	
	
	
	
	
	Public Function ModBias() As Double
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_MOD_BIAS, Send, 0, Receive, 2) Then
			ModBias = CDbl(Receive(1)) * 256# + CDbl(Receive(2)) ' Unsigned integer.
		Else
			ModBias = GENERAL_ERROR_CODE
		End If
		Exit Function
		
InternalErr: 
		ModBias = GENERAL_ERROR_CODE
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ModBias")
	End Function
	
	
	
	
	
	Public Function LinkStatus() As Boolean
		Dim ReturnCode As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		
		On Error GoTo LinkStatusErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_LINK_STATUS, Send, 0, Receive, 2) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.DeviceAddress. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If (Receive(1) = READ_LINK_STATUS) And (Receive(2) = Comms.DeviceAddress) Then
				LinkStatus = True
			Else
				LinkStatus = False
			End If
		Else
			LinkStatus = False
		End If
		Exit Function
		
LinkStatusErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "LinkStatus")
		LinkStatus = False
	End Function
	
	
	
	
	Public Function ReadMaxI2CRate() As String
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim ReturnCode As Byte
		
		On Error GoTo InternalErr
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(READ_MAX_I2C_RATE, Send, 0, Receive, 1) Then
			Select Case Receive(1)
				Case &H1s
					ReadMaxI2CRate = "100kbps"
				Case &H4s
					ReadMaxI2CRate = "400kbps"
				Case Else
					ReadMaxI2CRate = strGENERAL_ERROR_CODE
			End Select
		Else
			ReadMaxI2CRate = strGENERAL_ERROR_CODE
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ReadMaxI2CRate")
		ReadMaxI2CRate = strGENERAL_ERROR_CODE
	End Function
	
	
	
	
	Public Function Loopback() As Boolean
		'UPGRADE_WARNING: Lower bound of array Receive was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Receive(4) As Byte
		'UPGRADE_WARNING: Lower bound of array Send was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Send(4) As Byte
		Dim ReturnCode As Byte
		Dim dataOK As Boolean
		Dim indx As Byte
		
		On Error GoTo InternalErr
		Send(1) = Asc("A")
		Send(2) = Asc("B")
		Send(3) = Asc("C")
		Send(4) = Asc("D")
		dataOK = True
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.SendMSA. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.SendMSA(DATA_LOOPBACK, Send, 4, Receive, 4) Then
			For indx = 1 To 4
				If Send(indx) <> Receive(indx) Then
					dataOK = False
				End If
			Next indx
			Loopback = dataOK
		Else
			Loopback = False
		End If
		Exit Function
InternalErr: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "Loopback")
		Loopback = False
	End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class