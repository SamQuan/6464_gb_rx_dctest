using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Bookham.DemoTestControlPlugin.GuiMessages;

namespace Bookham.DemoTestControlPlugin
{
	/// <summary>
	/// Abort/Retry Dialogue User Control definition.
	/// </summary>
	public class DemoRetryAbortDialogue : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
	{
		private System.Windows.Forms.Label userInstructionLabel;
		private System.Windows.Forms.Button retryButton;
		private System.Windows.Forms.Button abortButton;
		private System.Windows.Forms.PictureBox bookhamLogoPictureBox;
		private System.Windows.Forms.Label loadBatchScreenLabel;
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Constructor
		/// </summary>
		public DemoRetryAbortDialogue()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemoRetryAbortDialogue));
            this.userInstructionLabel = new System.Windows.Forms.Label();
            this.retryButton = new System.Windows.Forms.Button();
            this.abortButton = new System.Windows.Forms.Button();
            this.bookhamLogoPictureBox = new System.Windows.Forms.PictureBox();
            this.loadBatchScreenLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // userInstructionLabel
            // 
            this.userInstructionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userInstructionLabel.ForeColor = System.Drawing.Color.Red;
            this.userInstructionLabel.Location = new System.Drawing.Point(32, 120);
            this.userInstructionLabel.Name = "userInstructionLabel";
            this.userInstructionLabel.Size = new System.Drawing.Size(800, 32);
            this.userInstructionLabel.TabIndex = 0;
            this.userInstructionLabel.Text = "Load Batch Operation Failed. Please Decide what action to take: ";
            // 
            // retryButton
            // 
            this.retryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.retryButton.Location = new System.Drawing.Point(32, 176);
            this.retryButton.Name = "retryButton";
            this.retryButton.Size = new System.Drawing.Size(192, 48);
            this.retryButton.TabIndex = 1;
            this.retryButton.Text = "Retry";
            this.retryButton.Click += new System.EventHandler(this.retryButton_Click);
            // 
            // abortButton
            // 
            this.abortButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abortButton.Location = new System.Drawing.Point(232, 176);
            this.abortButton.Name = "abortButton";
            this.abortButton.Size = new System.Drawing.Size(200, 48);
            this.abortButton.TabIndex = 2;
            this.abortButton.Text = "Abort";
            this.abortButton.Click += new System.EventHandler(this.abortButton_Click);
            // 
            // bookhamLogoPictureBox
            // 
            this.bookhamLogoPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("bookhamLogoPictureBox.Image")));
            this.bookhamLogoPictureBox.Location = new System.Drawing.Point(16, 8);
            this.bookhamLogoPictureBox.Name = "bookhamLogoPictureBox";
            this.bookhamLogoPictureBox.Size = new System.Drawing.Size(136, 50);
            this.bookhamLogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bookhamLogoPictureBox.TabIndex = 3;
            this.bookhamLogoPictureBox.TabStop = false;
            // 
            // loadBatchScreenLabel
            // 
            this.loadBatchScreenLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadBatchScreenLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.loadBatchScreenLabel.Location = new System.Drawing.Point(152, 16);
            this.loadBatchScreenLabel.Name = "loadBatchScreenLabel";
            this.loadBatchScreenLabel.Size = new System.Drawing.Size(320, 24);
            this.loadBatchScreenLabel.TabIndex = 4;
            this.loadBatchScreenLabel.Text = "Test Engine Demo: Load Batch Screen";
            // 
            // DemoRetryAbortDialogue
            // 
            this.Controls.Add(this.loadBatchScreenLabel);
            this.Controls.Add(this.bookhamLogoPictureBox);
            this.Controls.Add(this.abortButton);
            this.Controls.Add(this.retryButton);
            this.Controls.Add(this.userInstructionLabel);
            this.Name = "DemoRetryAbortDialogue";
            this.MsgReceived += new Bookham.TestEngine.Framework.Messages.ManagedCtlBase.MsgReceivedDlg(this.DemoRetryAbortDialogue_MsgReceived);
            ((System.ComponentModel.ISupportInitialize)(this.bookhamLogoPictureBox)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// User clicks the retry button. Send a retry message to the worker thread and resume 
		/// displaying the normal load batch screen.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event parameters</param>
		private void retryButton_Click(object sender, System.EventArgs e)
		{
			// Message the worker thread.
			this.sendToWorker( new QueryRetryAbortResponse(QueryRetryAbort.Retry ));
			
			//Disable buttons
			this.abortButton.Enabled = false;
			this.retryButton.Enabled = false;
		}

				
		/// <summary>
		/// User clicks the abort button. Send a abort message to the worker thread and resume 
		/// displaying the normal load batch screen.
		/// </summary>
		/// <param name="sender">Sender</param>
		/// <param name="e">Event parameters</param>
		private void abortButton_Click(object sender, System.EventArgs e)
		{
			// Message the worker thread.
			this.sendToWorker( new QueryRetryAbortResponse(QueryRetryAbort.Abort) );

			//Disable buttons
			this.abortButton.Enabled = false;
			this.retryButton.Enabled = false;
		}

		private void DemoRetryAbortDialogue_MsgReceived(object payload, long inMsgSeq, long respSeq)
		{
			// Find out what type of message has arrived, and process it appropriately.
			Type messageType = payload.GetType();

			if (messageType == typeof (QueryRetryAbortRequest))
			{

				this.abortButton.Enabled = true;
				this.retryButton.Enabled = true;

				// Display the error message.
				QueryRetryAbortRequest error = (QueryRetryAbortRequest)payload;
				this.userInstructionLabel.Text = "Load Batch Operation Error: " + error.message + "  Please Decide what action to take:";

			}
		}
	}
}

