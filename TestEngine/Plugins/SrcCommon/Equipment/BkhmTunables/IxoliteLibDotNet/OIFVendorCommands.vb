Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
'UPGRADE_WARNING: Class instancing was changed to public. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="ED41034B-3890-49FC-8076-BD6FC2F42A85"'
<System.Runtime.InteropServices.ProgId("OIFVendorCommands_NET.OIFVendorCommands")> Public Class OIFVendorCommands
	
	
	
	Public Function GetPCBTemp() As Double
		' 81
		' Returns in 100th degC from the TMP121 sensor.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetPCBTemp = Comms.ReadOIF(PCBTEMPERATURE, 0, 100)
		If GetPCBTemp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read PCB temperature.")
		End If
	End Function
	
	
	
	
	Public Function GetPCBTemperature(ByRef temp As Double) As Boolean
		' 81
		' Returns in 100th degC from the TMP121 sensor.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		temp = Comms.ReadOIF(PCBTEMPERATURE, 0, 100)
		If temp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read PCB temperature.")
			GetPCBTemperature = False
		Else
			GetPCBTemperature = True
		End If
	End Function
	
	
	
	Public Function SetOffGridTuning(ByRef offgrid As Double) As Boolean
		' 82
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetOffGridTuning = Comms.WriteOIF(OFFGRIDTUNING, offgrid, 10)
		If Not SetOffGridTuning Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Off Grid Tuning.")
		End If
	End Function
	
	
	
	Public Function GetOffGridTuning(ByRef offgrid As Double) As Boolean
		' 82
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		offgrid = Comms.ReadOIF(OFFGRIDTUNING, 0, 10)
		If offgrid = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Off Grid Tuning.")
			GetOffGridTuning = False
		Else
			GetOffGridTuning = True
		End If
	End Function
	
	
	
	
	Public Function SetIVSource(ByRef which As String, ByRef msbyte As Byte, ByRef lsbyte As Byte) As Boolean
		' 87 (LSW) + 88 (MSW)
		Dim what As Byte
		If which = "LSW" Then what = IVSOURCELSW Else what = IVSOURCEMSW
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetIVSource = Comms.WriteByteOIF(what, msbyte, lsbyte)
		If Not SetIVSource Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to SetIVSource " & which & ".")
		End If
	End Function
	
	
	
	
	Public Function GetIVSource(ByRef which As String, ByRef msbyte As Byte, ByRef lsbyte As Byte) As Boolean
		' 87 (LSW) + 88 (MSW)
		Dim what As Byte
		If which = "LSW" Then what = IVSOURCELSW Else what = IVSOURCEMSW
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetIVSource = Comms.ReadByteOIF(what, msbyte, lsbyte)
		If Not GetIVSource Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to GetIVSource " & which & ".")
		End If
	End Function
	
	
	
	
	Public Function SetIVSourceIndex(ByRef srcAsInt As Integer) As Boolean
		' 89
		' BPG 15-Feb-2006 - Changed parameter from IVSourceIndexNumbers to Integer, because enums are
		' not support as parameters on COM objects.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetIVSourceIndex = Comms.WriteOIF(IVSOURCEINDEX, CDbl(srcAsInt), 1)
		If Not SetIVSourceIndex Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to SetIVSourceIndex " & CStr(srcAsInt) & ".")
		End If
	End Function
	
	
	
	
	Public Function SetIVSourceDAC() As Boolean
		' 8A
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetIVSourceDAC = Comms.WriteOIF(WRITESOURCETODAC, 1, 1)
		If Not SetIVSourceDAC Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to SetIVSourceDAC.")
		End If
	End Function
	
	
	
	
	Public Function GetWaveCode() As Double
		' 90
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetWaveCode = Comms.ReadOIF(BKHMWAVECODE, 0, 1)
		If GetWaveCode = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read wavecode.")
		End If
	End Function
	
	
	
	Public Function SetWaveCode(ByRef wc As Short) As Boolean
		' 90
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetWaveCode = Comms.WriteOIF(BKHMWAVECODE, CDbl(wc), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetWaveCode Then Logger.logFATAL("Error: Failed to set wavecode.")
	End Function
	
	
	
	Public Function SaveChannel(ByRef chan As Short) As Boolean
		' 91
		' Note: This channel number must go from 1 to N with channel 1 as the lowest frequency.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(STORECHANNEL, CDbl(chan), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to save channel.")
			SaveChannel = False
		Else
			SaveChannel = True
		End If
	End Function
	
	
	
	
	Public Function GetSOADAC(ByRef dac As Integer) As Boolean
		' 92
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(SOADACREG, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read SOA dac.")
			GetSOADAC = False
		Else
			GetSOADAC = True
		End If
	End Function
	
	
	
	Public Function SetSOADAC(ByRef dac As Integer) As Boolean
		' 92
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(SOADACREG, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set SOA dac.")
			SetSOADAC = False
		Else
			SetSOADAC = True
		End If
		msDelay((10))
	End Function
	
	
	
	Public Function GetGainDAC(ByRef dac As Integer) As Boolean
		' 93
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(GAINDACREG, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Gain dac.")
			GetGainDAC = False
		Else
			GetGainDAC = True
		End If
	End Function
	
	
	
	Public Function SetGainDAC(ByRef dac As Integer) As Boolean
		' 93
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetGainDAC = Comms.WriteOIF(GAINDACREG, CDbl(dac), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetGainDAC Then Logger.logFATAL("Error: Failed to set Gain dac.")
	End Function
	
	
	
	
	Public Function GetFrontEvenDAC(ByRef dac As Integer) As Boolean
		' 94
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(FRONTEVENDACREG, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Front Even dac.")
			GetFrontEvenDAC = False
		Else
			GetFrontEvenDAC = True
		End If
	End Function
	
	
	
	Public Function SetFrontEvenDAC(ByRef dac As Integer) As Boolean
		' 94
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(FRONTEVENDACREG, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Front Even dac.")
			SetFrontEvenDAC = False
		Else
			SetFrontEvenDAC = True
		End If
		msDelay((10))
	End Function
	
	
	
	
	Public Function GetFrontOddDAC(ByRef dac As Integer) As Boolean
		' 95
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(FRONTODDDACREG, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Front Odd dac.")
			GetFrontOddDAC = False
		Else
			GetFrontOddDAC = True
		End If
	End Function
	
	
	
	Public Function SetFrontOddDAC(ByRef dac As Integer) As Boolean
		' 95
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(FRONTODDDACREG, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Front Odd dac.")
			SetFrontOddDAC = False
		Else
			SetFrontOddDAC = True
		End If
		msDelay((10))
	End Function
	
	
	
	
	Public Function GetRearDAC(ByRef dac As Integer) As Boolean
		' 96
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(REARDACREG, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Rear dac.")
			GetRearDAC = False
		Else
			GetRearDAC = True
		End If
	End Function
	
	
	
	Public Function SetRearDAC(ByRef dac As Integer) As Boolean
		' 96
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(REARDACREG, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Rear dac.")
			SetRearDAC = False
		Else
			SetRearDAC = True
		End If
		msDelay((10))
	End Function
	
	
	
	
	Public Function GetPhaseDAC(ByRef phdac As Integer) As Boolean
		' 97
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		phdac = CInt(Comms.ReadOIF(PHASEDACREG, 0, 1))
		If phdac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Phase dac.")
			GetPhaseDAC = False
		Else
			GetPhaseDAC = True
		End If
	End Function
	
	
	
	Public Function SetPhaseDAC(ByRef dac As Integer) As Boolean
		' 97
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(PHASEDACREG, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Phase dac.")
			SetPhaseDAC = False
		Else
			SetPhaseDAC = True
		End If
		msDelay((10))
	End Function
	
	
	
	
	Public Function GetDitherDAC(ByRef dac As Short) As Boolean
		' 98
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CShort(Comms.ReadOIF(DITHERDACREG, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Dither dac.")
			GetDitherDAC = False
		Else
			GetDitherDAC = True
		End If
	End Function
	
	
	
	Public Function SetDitherDAC(ByRef dac As Short) As Boolean
		' 98
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(DITHERDACREG, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Dither dac.")
			SetDitherDAC = False
		Else
			SetDitherDAC = True
		End If
		msDelay((10))
	End Function
	
	
	
	Public Function GetPCBTempSensor() As Double
		' 99
		' Returns in 16th degC from the raw TMP121 sensor.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetPCBTempSensor = Comms.ReadOIF(PCBTEMPSENSOR, 0, 16)
		If GetPCBTempSensor = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read PCB temperature sensor.")
		End If
	End Function
	
	
	
	Public Function GetTECADC(ByRef tadc As Short) As Boolean
		' 9A
		' Raw reading of ADC on TEMP_OUT signal (tile temp), 0-1023.
        'Dim returnval As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		tadc = CShort(Comms.ReadOIF(TEMPOUTLEVEL, 0, 1))
		If tadc = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read TEC ADC.")
			GetTECADC = False
		Else
			GetTECADC = True
		End If
	End Function
	
	
	
	Public Function GetModItecADC(ByRef tadc As Short) As Boolean
		' 9A
        'Dim returnval As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		tadc = CShort(Comms.ReadOIF(MODITEC, 0, 1))
		If tadc = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read ModItec ADC.")
			GetModItecADC = False
		Else
			GetModItecADC = True
		End If
	End Function
	
	
	
	Public Function GetVTECLevel() As Short
		' 9B
		' Raw reading of ADC on VTEC signal, 0-1023.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetVTECLevel = Comms.ReadOIF(VTECLEVEL, 0, 1)
	End Function
	
	
	
	Public Function GetLaserItecADC(ByRef tadc As Short) As Boolean
		' 9B
        'Dim returnval As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		tadc = CShort(Comms.ReadOIF(LASERITEC, 0, 1))
		If tadc = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read LaserItec ADC.")
			GetLaserItecADC = False
		Else
			GetLaserItecADC = True
		End If
	End Function
	
	
	
	
	Public Function GetLsBiasSense(ByRef lsbs As Short) As Boolean
		' 9C
		' 0-1023
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lsbs = CShort(Comms.ReadOIF(LSBIASSENSELEVEL, 0, 1))
		If lsbs = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Ls Bias Sense.")
			GetLsBiasSense = False
		Else
			GetLsBiasSense = True
		End If
	End Function
	
	
	
	
	Public Function GetLockerTxMon(ByRef ltxmon As Integer) As Boolean
		' 9E
		' 0-65535
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		ltxmon = CInt(Comms.ReadOIF(LOCKERTRANSMITMON, 0, 1))
		If ltxmon = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Locker Transmit Monitor.")
			GetLockerTxMon = False
		Else
			GetLockerTxMon = True
		End If
	End Function
	
	
	
	Public Function GetLockerRefMon(ByRef lrefmon As Integer) As Boolean
		' 9F
		' 0-65535
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lrefmon = CInt(Comms.ReadOIF(LOCKERREFLECTMON, 0, 1))
		If lrefmon = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Locker Reflect Monitor.")
			GetLockerRefMon = False
		Else
			GetLockerRefMon = True
		End If
	End Function
	
	
	
	Public Function GetLockerErrorMon(ByRef errmon As Integer) As Boolean
		' A0
		' 0-65535
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		errmon = CInt(Comms.ReadOIF(LOCKERERRORMON, 0, 1))
		If errmon = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Locker Error Monitor.")
			GetLockerErrorMon = False
		Else
			GetLockerErrorMon = True
		End If
	End Function
	
	
	
	Public Function GetLaserPowerMon(ByRef lpm As Integer, Optional ByRef avgcnt As Short = -999) As Boolean
		' A1
		' 0-65535. Can be noisy so may need to average it.
		Dim indx As Short
		Dim cnt As Short
		Dim thislpm As Integer
		On Error GoTo ErrorHandler
		If avgcnt = -999 Then
			cnt = 1
		Else
			If (avgcnt <= 0) Or (avgcnt >= 200) Then cnt = 1 Else cnt = avgcnt
		End If
		
		lpm = 0
		For indx = 1 To cnt
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			thislpm = CInt(Comms.ReadOIF(LASERPOWERMON, 0, 1))
			If thislpm = GENERAL_ERROR_CODE Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				Logger.logFATAL("Error: Failed to read Laser Power Monitor.")
				GoTo Aborted
			Else
				lpm = lpm + thislpm
			End If
		Next indx
		
		lpm = lpm / cnt
		GetLaserPowerMon = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetLaserPowerMon")
Aborted: 
		GetLaserPowerMon = False
	End Function
	
	
	
	
	Public Function GetLsPhaseSense(ByRef lsps As Integer) As Boolean
		' A2
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lsps = CInt(Comms.ReadOIF(LSPHASESENSE, 0, 1))
		If lsps = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Ls Phase Sense.")
			GetLsPhaseSense = False
		Else
			GetLsPhaseSense = True
		End If
	End Function
	
	
	
	
	Public Function GetLockerReference(ByRef lckref As Integer) As Boolean
		' A3
		' 0-65535
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lckref = CInt(Comms.ReadOIF(LOCKERREFERENCE, 0, 1))
		If lckref = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Locker Reference.")
			GetLockerReference = False
		Else
			GetLockerReference = True
		End If
	End Function
	
	
	
	
	Public Function GetRawThermisterBridge(ByRef therm As Integer) As Boolean
		' A4
		' 0-65535
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		therm = CInt(Comms.ReadOIF(RAWTHERMBRIDGE, 0, 1))
		If therm = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Raw Thermister Bridge (laser / IC715).")
			GetRawThermisterBridge = False
		Else
			GetRawThermisterBridge = True
		End If
	End Function
	
	
	
	
	Public Function GetRawThermisterBridgeAlt(ByRef therm As Integer) As Boolean
		' A5
		' 0-65535
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		therm = CInt(Comms.ReadOIF(RAWTHERMBRIDGEALT, 0, 1))
		If therm = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Raw Thermister Bridge (laser alt / IC2).")
			GetRawThermisterBridgeAlt = False
		Else
			GetRawThermisterBridgeAlt = True
		End If
	End Function
	
	
	
	Public Function GetTxFinePot(ByRef tfp As Short) As Boolean
		' A6
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		tfp = CShort(Comms.ReadOIF(TRANSMITFINEPOT, 0, 1))
		If tfp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Transmit Fine Pot.")
			GetTxFinePot = False
		Else
			GetTxFinePot = True
		End If
	End Function
	
	
	
	
	Public Function SetTxFinePot(ByRef tfp As Short, ByRef pot As Short) As Boolean
		' A6
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetTxFinePot = Comms.WriteOIF(TRANSMITFINEPOT, CDbl(pot), 1)
		If Not SetTxFinePot Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Transmit Fine Pot.")
		Else
			tfp = pot
		End If
	End Function
	
	
	
	Public Function GetTxCoarsePot(ByRef tcp As Short) As Boolean
		' A7
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		tcp = CShort(Comms.ReadOIF(TRANSMITCOARSEPOT, 0, 1))
		If tcp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Transmit Coarse Pot.")
			GetTxCoarsePot = False
		Else
			GetTxCoarsePot = True
		End If
	End Function
	
	
	
	
	Public Function SetTxCoarsePot(ByRef tcp As Short, ByRef pot As Short) As Boolean
		' A7
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetTxCoarsePot = Comms.WriteOIF(TRANSMITCOARSEPOT, CDbl(pot), 1)
		If Not SetTxCoarsePot Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Transmit Coarse Pot.")
		Else
			tcp = pot
		End If
	End Function
	
	
	
	Public Function GetRefCoarsePot(ByRef rcp As Short) As Boolean
		' A8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		rcp = CShort(Comms.ReadOIF(REFLECTCOARSEPOT, 0, 1))
		If rcp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Reflect Coarse Pot.")
			GetRefCoarsePot = False
		Else
			GetRefCoarsePot = True
		End If
	End Function
	
	
	
	Public Function SetRefCoarsePot(ByRef rcp As Short, ByRef pot As Short) As Boolean
		' A8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetRefCoarsePot = Comms.WriteOIF(REFLECTCOARSEPOT, CDbl(pot), 1)
		If Not SetRefCoarsePot Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Reflect Coarse Pot.")
		Else
			rcp = pot
		End If
	End Function
	
	
	
	Public Function GetLockerRangePot(ByRef pot As Short) As Boolean
		' A9
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(LOCKERRANGEPOT, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Locker Range Pot.")
			GetLockerRangePot = False
		Else
			GetLockerRangePot = True
		End If
	End Function
	
	
	
	Public Function SetLockerRangePot(ByRef pot As Short) As Boolean
		' A9
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(LOCKERRANGEPOT, CDbl(pot), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Locker Range Pot.")
			SetLockerRangePot = False
		Else
			SetLockerRangePot = True
		End If
	End Function
	
	
	
	Public Function GetPowerMonPot(ByRef pot As Short) As Boolean
		' AA
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(POWERMONPOT, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Power Monitor Pot.")
			GetPowerMonPot = False
		Else
			GetPowerMonPot = True
		End If
	End Function
	
	
	
	Public Function SetPowerMonPot(ByRef pot As Short) As Boolean
		' AA
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(POWERMONPOT, CDbl(pot), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Power Monitor Pot.")
			SetPowerMonPot = False
		Else
			SetPowerMonPot = True
		End If
	End Function
	
	
	
	
	Public Function GetPhaseMin(ByRef pmin As Integer) As Boolean
		' AB
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pmin = CInt(Comms.ReadOIF(PHASEMINSETPOINT, 0, 1))
		If pmin = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read PhaseMin.")
			GetPhaseMin = False
		Else
			GetPhaseMin = True
		End If
	End Function
	
	
	
	Public Function SetPhaseMin(ByRef pmin As Integer) As Boolean
		' AB
		' Note that this will be set to default a value unless PhaseRangeCalc has been run.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetPhaseMin = Comms.WriteOIF(PHASEMINSETPOINT, CDbl(pmin), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPhaseMin Then Logger.logFATAL("Error: Failed to set PhaseMin.")
	End Function
	
	
	
	Public Function GetPhaseMax(ByRef pmax As Integer) As Boolean
		' AC
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pmax = CInt(Comms.ReadOIF(PHASEMAXSETPOINT, 0, 1))
		If pmax = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read PhaseMax.")
			GetPhaseMax = False
		Else
			GetPhaseMax = True
		End If
	End Function
	
	
	
	Public Function SetPhaseMax(ByRef pmax As Integer) As Boolean
		' AC
		' Note that this will be set to default a value unless PhaseRangeCalc has been run.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetPhaseMax = Comms.WriteOIF(PHASEMAXSETPOINT, CDbl(pmax), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPhaseMax Then Logger.logFATAL("Error: Failed to set PhaseMax.")
	End Function
	
	
	
	Public Function GetPowerMonFactor(ByRef pmf As Double) As Boolean
		' AD
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pmf = Comms.ReadOIF(POWERMONFACTOR, 0, 1)
		If pmf = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Power Monitor Factor.")
			GetPowerMonFactor = False
		Else
			GetPowerMonFactor = True
		End If
	End Function
	
	
	
	Public Function SetPowerMonFactor(ByRef pmf As Double) As Boolean
		' AD
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetPowerMonFactor = Comms.WriteOIF(POWERMONFACTOR, pmf, 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPowerMonFactor Then Logger.logFATAL("Error: Failed to set Power Monitor Factor.")
	End Function
	
	
	
	Public Function GetLockerErrorFactor(ByRef lef As Double) As Boolean
		' AE
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		lef = Comms.ReadOIF(LOCKERERRORFACTOR, 0, 1)
		If lef = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Locker Error Factor.")
			GetLockerErrorFactor = False
		Else
			GetLockerErrorFactor = True
		End If
	End Function
	
	
	
	Public Function SetLockerErrorFactor(ByRef lef As Double) As Boolean
		' AE
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetLockerErrorFactor = Comms.WriteOIF(LOCKERERRORFACTOR, lef, 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetLockerErrorFactor Then Logger.logFATAL("Error: Failed to set Locker Error Factor.")
	End Function
	
	
	
	Public Function GetTileTempSetpoint(ByRef osa As Double) As Boolean
		' B0
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		osa = Comms.ReadOIF(TILETEMPSETPOINT, 0, 1)
		If osa = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Tile Temp Setpoint.")
			GetTileTempSetpoint = False
		Else
			GetTileTempSetpoint = True
		End If
	End Function
	
	
	
	Public Function GetAtmelReference() As Short
		' B2
        'WBTT specific. Register B2 has now been reallocated on ITLA/TTA
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetAtmelReference = CShort(Comms.ReadOIF(ATMELREFERENCE, 0, 1))
	End Function
	
    Public Function GetLowPowerEtalonSlope(ByRef lef As Double) As Boolean
        ' B2
        lef = Comms.ReadOIF(LOWPOWERETALONSLOPE, 0, 1)
        If lef = GENERAL_ERROR_CODE Then
            Logger.logFATAL("Error: Failed to read Low Power Etalon Slope.")
            GetLowPowerEtalonSlope = False
        Else
            GetLowPowerEtalonSlope = True
        End If
    End Function



    Public Function SetLowPowerEtalonSlope(ByVal lef As Double) As Boolean
        ' B2
        SetLowPowerEtalonSlope = Comms.WriteOIF(LOWPOWERETALONSLOPE, lef, 1)
        If Not SetLowPowerEtalonSlope Then Logger.logFATAL("Error: Failed to set Low Power Etalon Slope.")
    End Function
	
	Public Function SelectChannel(ByRef chan As Short) As Boolean
		' B3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SelectChannel = Comms.WriteOIF(LASERCHANNEL, CDbl(chan), 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SelectChannel Then Logger.logFATAL("Error: Failed change to channel " & CStr(chan) & ".")
	End Function
	
	
	
	Public Function GetHexCtrlReg() As String
		' B4
		Dim upper As Byte
		Dim lower As Byte
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then
			GetHexCtrlReg = Hex(upper) & Hex(lower)
		Else
			GetHexCtrlReg = strGENERAL_ERROR_CODE
		End If
	End Function
	
	
	
	
	Public Function SetControlBitUpper(ByRef ctrlbit As Byte, ByRef what As String, Optional ByRef msg As String = "defaultvalue") As Boolean
		' B4
		' Sets/unsets bits in the upper byte of the control register B4.
		' Sets the specified bit "ctrlbit" either "on" or "off" according to parameter "what".
		Dim upper As Byte
		Dim lower As Byte
		Dim emsg As String
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then GoTo Aborted
		
		upper = upper And Not ctrlbit
		If what = "on" Then upper = upper Or ctrlbit
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteByteOIF(CALCONTROLREG, upper, lower) Then GoTo Aborted
		SetControlBitUpper = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SetControlBitUpper")
Aborted: 
		SetControlBitUpper = False
		If msg = "defaultvalue" Then emsg = " " & what Else emsg = " " & what & " (" & msg & ")"
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to set upper control bit 0x" & Hex(ctrlbit) & emsg)
	End Function
	
	
	
	Public Function SetControlBitLower(ByRef ctrlbit As Byte, ByRef what As String, Optional ByRef msg As String = "defaultvalue") As Boolean
		' B4
		' Sets/unsets bits in the lower byte of the control register B4.
		' Sets the specified bit "ctrlbit" either "on" or "off" according to parameter "what".
		Dim upper As Byte
		Dim lower As Byte
		Dim emsg As String
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then GoTo Aborted
		
		lower = lower And Not ctrlbit
		If what = "on" Then lower = lower Or ctrlbit
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteByteOIF(CALCONTROLREG, upper, lower) Then GoTo Aborted
		SetControlBitLower = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SetControlBitLower")
Aborted: 
		SetControlBitLower = False
		If msg = "defaultvalue" Then emsg = " " & what Else emsg = " " & what & " (" & msg & ")"
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to set lower control bit 0x" & Hex(ctrlbit) & emsg)
	End Function
	
	
	
	
	Public Function ControlBitUpper(ByRef ctrlbit As Byte) As String
		' B4
		' Reads the specified bit "ctrlbit" in the upper byte of the control register B4,
		' returning either "on" or "off" according to whether the bit is set or clear.
		Dim upper As Byte
		Dim lower As Byte
		Dim resultb As Byte
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then
			resultb = upper And ctrlbit
			If resultb = 0 Then
				ControlBitUpper = "off"
			Else
				ControlBitUpper = "on"
			End If
		Else
			ControlBitUpper = "error"
		End If
		
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ControlBitUpper")
		ControlBitUpper = "error"
	End Function
	
	
	
	
	Public Function ControlBitLower(ByRef ctrlbit As Byte) As String
		' B4
		' Reads the specified bit "ctrlbit" in the lower byte of the control register B4,
		' returning either "on" or "off" according to whether the bit is set or clear.
		Dim upper As Byte
		Dim lower As Byte
		Dim resultb As Byte
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then
			resultb = lower And ctrlbit
			If resultb = 0 Then
				ControlBitLower = "off"
			Else
				ControlBitLower = "on"
			End If
		Else
			ControlBitLower = "error"
		End If
		
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "ControlBitLower")
		ControlBitLower = "error"
	End Function
	
	
	Public Function GetRawControlRegister(ByRef upper As Byte, ByRef lower As Byte) As Boolean
		' B4
		' Gets the raw control register, returning false on error.
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetRawControlRegister = Comms.ReadByteOIF(CALCONTROLREG, upper, lower)
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetRawControlRegister")
		GetRawControlRegister = False
	End Function
	
	Public Function SetRawControlRegister(ByVal upper As Byte, ByVal lower As Byte) As Boolean
		' B4
		' Writes the control register B4, returning false on error.
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetRawControlRegister = Comms.WriteByteOIF(CALCONTROLREG, upper, lower)
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SetRawControlRegister")
		SetRawControlRegister = False
	End Function
	
	
	
	Public Function FrontEven() As Short
		' B4
		' Reads the control register and returns the Front Even setting as an integer in the range 0-3.
		Dim upper As Byte
		Dim lower As Byte
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then
			FrontEven = upper And 3
		Else
			FrontEven = GENERAL_ERROR_CODE
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "FrontEven")
		FrontEven = GENERAL_ERROR_CODE
	End Function
	
	
	
	Public Function FrontOdd() As Short
		' B4
		' Reads the control register and returns the Front Odd setting as an integer in the range 0-3.
		Dim upper As Byte
		Dim lower As Byte
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then
			FrontOdd = (lower \ 64) And 3
		Else
			FrontOdd = GENERAL_ERROR_CODE
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "FrontOdd")
		FrontOdd = GENERAL_ERROR_CODE
	End Function
	
	
	
	Public Function FrontSectionSelected() As Short
		' B4
		' Reads the control register and returns the Front setting as an integer in the range 1-7.
		Dim upper As Byte
		Dim lower As Byte
		Dim front As Byte
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then
			front = (CShort(upper And 3) * 4) + CShort((lower \ 64) And 3)
			Select Case front
				Case 0 : FrontSectionSelected = 1
				Case 1 : FrontSectionSelected = 2
				Case 5 : FrontSectionSelected = 3
				Case 6 : FrontSectionSelected = 4
				Case 10 : FrontSectionSelected = 5
				Case 11 : FrontSectionSelected = 6
				Case 15 : FrontSectionSelected = 7
				Case Else : FrontSectionSelected = GENERAL_ERROR_CODE
			End Select
		Else
			FrontSectionSelected = GENERAL_ERROR_CODE
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "FrontSectionSelected")
		FrontSectionSelected = GENERAL_ERROR_CODE
	End Function
	
	
	
	
	Public Function SelectFrontSection(ByRef sec As Short) As Boolean
		' B4
		Dim upper As Byte
		Dim lower As Byte
		
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.ReadByteOIF(CALCONTROLREG, upper, lower) Then GoTo Aborted
		upper = upper And Not 3
		lower = lower And Not &HC0s
		
		Select Case sec
			Case 1
				' EVEN1 EVEN0 ODD1 ODD0 = 0000
			Case 2
				' EVEN1 EVEN0 ODD1 ODD0 = 0001
				lower = lower Or ODD0
			Case 3
				' EVEN1 EVEN0 ODD1 ODD0 = 0101
				upper = upper Or EVEN0
				lower = lower Or ODD0
			Case 4
				' EVEN1 EVEN0 ODD1 ODD0 = 0110
				upper = upper Or EVEN0
				lower = lower Or ODD1
			Case 5
				' EVEN1 EVEN0 ODD1 ODD0 = 1010
				upper = upper Or EVEN1
				lower = lower Or ODD1
			Case 6
				' EVEN1 EVEN0 ODD1 ODD0 = 1011
				upper = upper Or EVEN1
				lower = lower Or ODD1
				lower = lower Or ODD0
			Case 7
				' EVEN1 EVEN0 ODD1 ODD0 = 1111
				upper = upper Or EVEN1
				upper = upper Or EVEN0
				lower = lower Or ODD1
				lower = lower Or ODD0
		End Select
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteByteOIF(CALCONTROLREG, upper, lower) Then GoTo Aborted
		
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SelectFrontSection")
Aborted: 
		SelectFrontSection = GENERAL_ERROR_CODE
	End Function
	
	
	
	Public Function SetRearSource(ByRef what As String) As Boolean
		If what = "low" Then
			SetRearSource = SetControlBitLower(REARSOURCE, "on", "RearSrc")
		Else
			SetRearSource = SetControlBitLower(REARSOURCE, "off", "RearSrc")
		End If
	End Function
	
	
	
	Public Function SetLocker(ByRef what As String) As Boolean
		If what = "on" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLocker = DBR.SetControlBitUpper(LOCKER, "on", "Locker")
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLocker = DBR.SetControlBitUpper(LOCKER, "off", "Locker")
		End If
	End Function
	
	
	
	Public Function SetLockerSlope(ByRef what As String) As Boolean
		If what = "pos" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLockerSlope = DBR.SetControlBitUpper(LOCKERSLOPE, "on", "LockerSlope")
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLockerSlope = DBR.SetControlBitUpper(LOCKERSLOPE, "off", "LockerSlope")
		End If
	End Function
	
	
	
	Public Function SetLaserPSU(ByRef what As String) As Boolean
		If what = "on" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLaserPSU = DBR.SetControlBitUpper(LASERPSU, "on", "LaserPSU")
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetLaserPSU = DBR.SetControlBitUpper(LASERPSU, "off", "LaserPSU")
		End If
	End Function
	
	
	
	
	Public Function CheckControlBits() As String
		' B4
		' These bits must be in the correct state before starting calibration.
		' We can wait for up to 10 seconds for TEMPLOCK to clear.
		' Not applicable to the FCU
		Dim indx As Short
		On Error GoTo ErrorHandler
		
		'UPGRADE_WARNING: Couldn't resolve default property of object DBR.ControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If DBR.ControlBitUpper(THERMISTERFAULT) = "on" Then
			CheckControlBits = "Error: Thermister Fault bit set."
		Else
			indx = 0
			'UPGRADE_WARNING: Couldn't resolve default property of object DBR.ControlBitUpper. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Do While (DBR.ControlBitUpper(TEMPLOCK) = "on") And (indx < 10)
				indx = 1
				sDelay((1))
			Loop 
			If indx >= 10 Then
				CheckControlBits = "Error: Temp Lock bit set."
			Else
				'UPGRADE_WARNING: Couldn't resolve default property of object DBR.SetControlBitLower. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				If DBR.SetControlBitLower(DITHER, "off", "Dither") = False Then
					CheckControlBits = "Error: Failed to clear Dither bit."
				Else
					CheckControlBits = ""
				End If
			End If
		End If
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "CheckControlBits")
		CheckControlBits = "CheckControlBits: Internal error"
	End Function
	
	
	
	
	
	
	Public Function GetLaserPowerEstimate() As Double
		' B5
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetLaserPowerEstimate = Comms.ReadOIF(LASERPOWERESTIMATE, 0, 1)
	End Function
	
	
	
	
	Public Function GetPowerMonOffset(ByRef pmo As Double) As Boolean
		' B6
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pmo = Comms.ReadOIF(POWERMONITOROFFSET, 0, 1)
		If pmo = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read Power Monitor Offset.")
			GetPowerMonOffset = False
		Else
			GetPowerMonOffset = True
		End If
	End Function
	
	
	
	Public Function SetPowerMonOffset(ByRef pmo As Double) As Boolean
		' B6
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetPowerMonOffset = Comms.WriteOIF(POWERMONITOROFFSET, pmo, 1)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPowerMonOffset Then Logger.logFATAL("Error: Failed to set Power Monitor Offset.")
	End Function
	
	
	
	Public Function GetSrcNumber(ByRef which As String, ByRef pot As Short) As Boolean
		' B7 = Current
		' BC = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCENUMBER Else what = VOLTAGESOURCENUMBER
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(what, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Source Number.")
			GetSrcNumber = False
		Else
			GetSrcNumber = True
		End If
	End Function
	
	
	
	Public Function SetSrcNumber(ByRef which As String, ByRef pot As Short) As Boolean
		' B7 = Current
		' BC = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCENUMBER Else what = VOLTAGESOURCENUMBER
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(what, CDbl(pot), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Source Number.")
			SetSrcNumber = False
		Else
			SetSrcNumber = True
		End If
	End Function
	
	
	
	Public Function GetSrcIndex(ByRef which As String, ByRef pot As Short) As Boolean
		' B8 = Current
		' BD = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCEINDEX Else what = VOLTAGESOURCEINDEX
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(what, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Current Source Index.")
			GetSrcIndex = False
		Else
			GetSrcIndex = True
		End If
	End Function
	
	
	
	Public Function SetSrcIndex(ByRef which As String, ByRef pot As Short) As Boolean
		' B8 = Current
		' BD = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCEINDEX Else what = VOLTAGESOURCEINDEX
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(what, CDbl(pot), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Source Index.")
			SetSrcIndex = False
		Else
			SetSrcIndex = True
		End If
	End Function
	
	
	
	Public Function GetSrcLSW(ByRef which As String, ByRef msbyte As Byte, ByRef lsbyte As Byte) As Boolean
		' B9 = Current
		' BE = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCELSW Else what = VOLTAGESOURCELSW
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetSrcLSW = Comms.ReadByteOIF(what, msbyte, lsbyte)
		If Not GetSrcLSW Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Source LSW.")
		End If
	End Function
	
	
	Public Function SetSrcLSW(ByRef which As String, ByRef msbyte As Byte, ByRef lsbyte As Byte) As Boolean
		' B9 = Current
		' BE = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCELSW Else what = VOLTAGESOURCELSW
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSrcLSW = Comms.WriteByteOIF(what, msbyte, lsbyte)
		If Not SetSrcLSW Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Source LSW.")
		End If
	End Function
	
	
	
	Public Function GetSrcMSW(ByRef which As String, ByRef msbyte As Byte, ByRef lsbyte As Byte) As Boolean
		' BA = Current
		' DF = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCEMSW Else what = VOLTAGESOURCEMSW
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetSrcMSW = Comms.ReadByteOIF(what, msbyte, lsbyte)
		If Not GetSrcMSW Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Source MSW.")
		End If
	End Function
	
	
	
	Public Function SetSrcMSW(ByRef which As String, ByRef msbyte As Byte, ByRef lsbyte As Byte) As Boolean
		' BA = Current
		' DF = Voltage
		Dim what As Byte
		If which = "I" Then what = CURRENTSOURCEMSW Else what = VOLTAGESOURCEMSW
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSrcMSW = Comms.WriteByteOIF(what, msbyte, lsbyte)
		If Not SetSrcMSW Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Source MSW.")
		End If
	End Function
	
	
	
	Public Function GetSBSDitherMultiplier(ByRef mult As Short) As Boolean
		' BB
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		mult = CShort(Comms.ReadOIF(SBSDITHERMULTIPLIER, 0, 1))
		If mult = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get SBS Dither Multipler.")
			GetSBSDitherMultiplier = False
		Else
			GetSBSDitherMultiplier = True
		End If
	End Function
	
	
	
	Public Function SetSBSDitherMultiplier(ByRef mult As Byte) As Boolean
		' BB
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetSBSDitherMultiplier = Comms.WriteOIF(SBSDITHERMULTIPLIER, CDbl(mult), 1)
		If Not SetSBSDitherMultiplier Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set SBS Dither Multipler.")
		End If
	End Function
	
	
	
	Public Function GetWavelengthError() As Double
		' BF
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetWavelengthError = Comms.ReadOIF(WAVELENGTHERROR, 0, 1)
	End Function
	
	
	
	Public Function GetBaseCamp(ByRef dac As Integer) As Boolean
		' C0
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(BASECAMPDAC, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Basecamp dac.")
			GetBaseCamp = False
		Else
			GetBaseCamp = True
		End If
	End Function
	
	
	
	Public Function SetBaseCamp(ByRef dac As Integer) As Boolean
		' C0
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(BASECAMPDAC, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Basecamp dac.")
			SetBaseCamp = False
		Else
			SetBaseCamp = True
		End If
	End Function
	
	
	
	Public Function GetBaseCamp2(ByRef dac As Integer) As Boolean
		' C1
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		dac = CInt(Comms.ReadOIF(BASECAMP2DAC, 0, 1))
		If dac = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Basecamp2 dac.")
			GetBaseCamp2 = False
		Else
			GetBaseCamp2 = True
		End If
	End Function
	
	
	
	Public Function SetBaseCamp2(ByRef dac As Integer) As Boolean
		' C1
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(BASECAMP2DAC, CDbl(dac), 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Basecamp2 dac.")
			SetBaseCamp2 = False
		Else
			SetBaseCamp2 = True
		End If
	End Function
	
	
	
	Public Function GetPlateauPower(ByRef pwr As Double) As Boolean
		' C2
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pwr = Comms.ReadOIF(PLATEAUPOWER, 0, 1)
		If pwr = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Plateau Power.")
			GetPlateauPower = False
		Else
			GetPlateauPower = True
		End If
	End Function
	
	
	
	Public Function SetPlateauPower(ByRef pwr As Double) As Boolean
		' C2
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(PLATEAUPOWER, pwr, 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Plateau Power.")
			SetPlateauPower = False
		Else
			SetPlateauPower = True
		End If
	End Function
	
	
	
	Public Function GetSinkSlope(ByRef slp As Double) As Boolean
		' C3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		slp = Comms.ReadOIF(SINKSLOPE, 0, 1)
		If slp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Sink Slope.")
			GetSinkSlope = False
		Else
			GetSinkSlope = True
		End If
	End Function
	
	
	
	Public Function SetSinkSlope(ByRef slp As Double) As Boolean
		' C3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(SINKSLOPE, slp, 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Sink Slope.")
			SetSinkSlope = False
		Else
			SetSinkSlope = True
		End If
	End Function
	
	
	
	Public Function GetSourceSlope(ByRef slp As Double) As Boolean
		' C4
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		slp = Comms.ReadOIF(SOURCESLOPE, 0, 1)
		If slp = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Source Slope.")
			GetSourceSlope = False
		Else
			GetSourceSlope = True
		End If
	End Function
	
	
	
	Public Function SetSourceSlope(ByRef slp As Double) As Boolean
		' C4
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not Comms.WriteOIF(SOURCESLOPE, slp, 1) Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Source Slope.")
			SetSourceSlope = False
		Else
			SetSourceSlope = True
		End If
	End Function
	
	
	
	
	Public Function GetUnitSerialNumber() As String
		' C5
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetUnitSerialNumber = Comms.ReadOIFString(UNITSERIALNUMBER)
		If GetUnitSerialNumber = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get unit serial number.")
		End If
	End Function
	
	
	
	
	Public Function SetUnitSerialNumber(ByRef serno As String) As Boolean
		' C5
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetUnitSerialNumber = Comms.WriteOIFString(UNITSERIALNUMBER, serno, Len(serno) + 1)
		If Not SetUnitSerialNumber Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to send unit serial number.")
		End If
	End Function
	
	
	
	
	Public Function SetUnitBuildDate(ByRef bdate As String) As Boolean
		' C6
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetUnitBuildDate = Comms.WriteOIFString(UNITBUILDDATE, bdate, Len(bdate) + 1)
		If Not SetUnitBuildDate Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to send unit build date.")
		End If
	End Function
	
	
	
	
	Public Function SetUnitManufacturer(ByRef manuf As String) As Boolean
		' C7
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetUnitManufacturer = Comms.WriteOIFString(UNITMANUFACTURER, manuf, Len(manuf) + 1)
		If Not SetUnitManufacturer Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to send unit manufacturer.")
		End If
	End Function
	
	
	
	
	Public Function UnlockModule() As Boolean
		' C8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		UnlockModule = Comms.WriteOIFString(MODULEUNLOCK, "DSDBR123", 8)
		If Not UnlockModule Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to send module unlock code.")
		End If
	End Function
	
	
	
	Public Function GetOffGridPositive(ByRef pot As Short) As Boolean
		' C9
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(OFFGRIDPOSITIVE, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Off Grid Positive.")
			GetOffGridPositive = False
		Else
			GetOffGridPositive = True
		End If
	End Function
	
	
	
	
	Public Function SetOffGridPositive(ByRef pot As Short) As Boolean
		' C9
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetOffGridPositive = Comms.WriteOIF(OFFGRIDPOSITIVE, CDbl(pot), 1)
		If Not SetOffGridPositive Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Off Grid Positive.")
		End If
	End Function
	
	
	
	Public Function GetOffGridNegative(ByRef pot As Short) As Boolean
		' CA
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(OFFGRIDNEGATIVE, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Off Grid Negative.")
			GetOffGridNegative = False
		Else
			GetOffGridNegative = True
		End If
	End Function
	
	
	
	
	Public Function SetOffGridNegative(ByRef pot As Short) As Boolean
		' CA
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetOffGridNegative = Comms.WriteOIF(OFFGRIDNEGATIVE, CDbl(pot), 1)
		If Not SetOffGridNegative Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Off Grid Negative.")
		End If
	End Function
	
	
	
	Public Function GetChannelCentre(ByRef pot As Short) As Boolean
		' CB
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(CHANNELCENTRE, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Channel Centre.")
			GetChannelCentre = False
		Else
			GetChannelCentre = True
		End If
	End Function
	
	
	
	
	Public Function SetChannelCentre(ByRef pot As Short) As Boolean
		' CB
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetChannelCentre = Comms.WriteOIF(CHANNELCENTRE, CDbl(pot), 1)
		If Not SetChannelCentre Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Channel Centre.")
		End If
	End Function
	
	
	
	Public Function GetPowerControlState(ByRef state As String) As Boolean
		' CF (ITLA)
		Dim pc As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pc = Comms.ReadOIF(POWERCONTROLLOOP, 0, 1)
		If pc = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to read power control state.")
			GetPowerControlState = False
		Else
			GetPowerControlState = True
			If pc = 0 Then state = "off" Else state = "on"
		End If
	End Function
	
	
	
	Public Function SetPowerControlState(ByRef state As String) As Boolean
		' CF (ITLA)
		If state = "on" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetPowerControlState = Comms.WriteOIF(POWERCONTROLLOOP, 1, 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetPowerControlState = Comms.WriteOIF(POWERCONTROLLOOP, 0, 1)
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetPowerControlState Then Logger.logFATAL("Error: Failed to set power control state.")
	End Function
	
	
	
	Public Function GetClockSelectState(ByRef state As String) As Boolean
		' D0 bit 0
		Dim reg As Short
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		reg = CShort(Comms.ReadOIF(MODDRIVERCONTROL, 0, 1))
		If reg = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get clock select state.")
			GetClockSelectState = False
		Else
			If (reg And CLOCKSELECT) = 0 Then state = "off" Else state = "on"
		End If
	End Function
	
	
	
	Public Function SetClockSelectState(ByRef state As String) As Boolean
		' D0 bit 0
		If state = "on" Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetClockSelectState = Comms.WriteOIF(MODDRIVERCONTROL, CLOCKSELECT, 1)
		Else
			'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			SetClockSelectState = Comms.WriteOIF(MODDRIVERCONTROL, 0, 1)
		End If
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetClockSelectState Then Logger.logFATAL("Error: Failed to set clock select state.")
	End Function
	
	
	
	Public Function GetLaserSerialNumber() As String
		' D1
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetLaserSerialNumber = Comms.ReadOIFString(LASERSERIALNUMBER)
		If GetLaserSerialNumber = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get laser serial number.")
		End If
	End Function
	
	
	
	
	Public Function SetLaserSerialNumber(ByRef serno As String) As Boolean
		' D1
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        SetLaserSerialNumber = Comms.WriteOIFString(LASERSERIALNUMBER, serno, Len(serno) + 1)
		If Not SetLaserSerialNumber Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to send laser serial number.")
		End If
	End Function
	
	
	
	
	Public Function GetBoardSerialNumber() As String
		' D2
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetBoardSerialNumber = Comms.ReadOIFString(BOARDSERIALNUMBER)
		If GetBoardSerialNumber = strGENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get board serial number.")
		End If
	End Function
	
	
	
	
	Public Function SetBoardSerialNumber(ByRef serno As String) As Boolean
		' D2
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIFString. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetBoardSerialNumber = Comms.WriteOIFString(BOARDSERIALNUMBER, serno, Len(serno) + 1)
		If Not SetBoardSerialNumber Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to send board serial number.")
		End If
	End Function
	
	
	
	Public Function GetLeftMZBias(ByRef pot As Short) As Boolean
		' D3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(LEFTMZBIAS, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Left MZ Bias.")
			GetLeftMZBias = False
		Else
			GetLeftMZBias = True
		End If
	End Function
	
	
	
	Public Function SetLeftMZBias(ByRef pot As Short) As Boolean
		' D3
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetLeftMZBias = Comms.WriteOIF(LEFTMZBIAS, CDbl(pot), 1)
		If Not SetLeftMZBias Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Left MZ Bias.")
		End If
	End Function
	
	
	
	Public Function GetRightMZBias(ByRef pot As Short) As Boolean
		' D4
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(RIGHTMZBIAS, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Right MZ Bias.")
			GetRightMZBias = False
		Else
			GetRightMZBias = True
		End If
	End Function
	
	
	
	Public Function SetRightMZBias(ByRef pot As Short) As Boolean
		' D4
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetRightMZBias = Comms.WriteOIF(RIGHTMZBIAS, CDbl(pot), 1)
		If Not SetRightMZBias Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Right MZ Bias.")
		End If
	End Function
	
	
	
	Public Function GetMZModulation(ByRef pot As Short) As Boolean
		' D5
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(MZMODULATION, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get MZ Modulation.")
			GetMZModulation = False
		Else
			GetMZModulation = True
		End If
	End Function
	
	
	
	Public Function SetMZModulation(ByRef pot As Short) As Boolean
		' D5
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetMZModulation = Comms.WriteOIF(MZMODULATION, CDbl(pot), 1)
		If Not SetMZModulation Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set MZ Modulation.")
		End If
	End Function
	
	
	
	Public Function GetLeftImbalance(ByRef pot As Short) As Boolean
		' D7
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(LEFTIMBALANCE, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Left MZ Imbalance.")
			GetLeftImbalance = False
		Else
			GetLeftImbalance = True
		End If
	End Function
	
	
	
	Public Function SetLeftImbalance(ByRef pot As Short) As Boolean
		' D7
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetLeftImbalance = Comms.WriteOIF(LEFTIMBALANCE, CDbl(pot), 1)
		If Not SetLeftImbalance Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Left MZ Imbalance.")
		End If
	End Function
	
	
	
	Public Function GetRightImbalance(ByRef pot As Short) As Boolean
		' D8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		pot = CShort(Comms.ReadOIF(RIGHTIMBALANCE, 0, 1))
		If pot = GENERAL_ERROR_CODE Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to get Right MZ Imbalance.")
			GetRightImbalance = False
		Else
			GetRightImbalance = True
		End If
	End Function
	
	
	
	Public Function SetRightImbalance(ByRef pot As Short) As Boolean
		' D8
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetRightImbalance = Comms.WriteOIF(RIGHTIMBALANCE, CDbl(pot), 1)
		If Not SetRightImbalance Then
			'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Logger.logFATAL("Error: Failed to set Right MZ Imbalance.")
		End If
	End Function
	
	
	
	Public Function SetMinPowerSetpoint(ByRef dbm As Double) As Boolean
		' DA (ITLA)
		' Sets the min power setpoint in 100th dBm.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetMinPowerSetpoint = Comms.WriteOIF(MINPOWERSETPOINT, dbm, 100)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetMinPowerSetpoint Then Logger.logFATAL("Error: Failed to write Min Power Setpoint.")
	End Function
	
	Public Function SetMaxPowerSetpoint(ByRef dbm As Double) As Boolean
		' DB (ITLA)
		' Sets the max power setpoint in 100th dBm.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetMaxPowerSetpoint = Comms.WriteOIF(MAXPOWERSETPOINT, dbm, 100)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetMaxPowerSetpoint Then Logger.logFATAL("Error: Failed to write Max Power Setpoint.")
	End Function
	
	Public Function SetOifRegisterAsBytes(ByVal regNum As Byte, ByVal upper As Byte, ByVal lower As Byte) As Boolean
		' Set any OIF register as raw bytes.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.WriteByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		SetOifRegisterAsBytes = Comms.WriteByteOIF(regNum, upper, lower)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not SetOifRegisterAsBytes Then Logger.logFATAL("Error: Failed to Set Oif Register " & regNum)
	End Function
	
	Public Function GetOifRegisterAsBytes(ByVal regNum As Byte, ByRef upper As Byte, ByRef lower As Byte) As Boolean
		' Get any OIF register as raw bytes.
		'UPGRADE_WARNING: Couldn't resolve default property of object Comms.ReadByteOIF. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		GetOifRegisterAsBytes = Comms.ReadByteOIF(regNum, upper, lower)
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		If Not GetOifRegisterAsBytes Then Logger.logFATAL("Error: Failed to Get Oif Register " & regNum)
	End Function
	
    Public Function GetOifRegisterAsString(ByVal regNum As Byte, ByRef response As String) As Boolean
        response = Comms.ReadOIFString(regNum)
        If response = strGENERAL_ERROR_CODE Then
            Logger.logFATAL("Error: Failed to set OIF register as string")
            GetOifRegisterAsString = False
        Else
            GetOifRegisterAsString = True
        End If
    End Function


    Public Function SetOifRegisterAsString(ByVal regNum As Byte, ByRef stringArg As String) As Boolean
        SetOifRegisterAsString = Comms.WriteOIFString(regNum, stringArg, Len(stringArg))
        If Not SetOifRegisterAsString Then
            Logger.logFATAL("Error: Failed to set OIF Register as string")
        End If
    End Function
	
	
	Public Function SetSource(ByRef cnumAsInt As Integer, ByRef iv As Single) As Boolean
		' This routine performs the text equivalent of "seti/setv" using the standard current/voltage
		' source numbers which are then converted to the Atmel internal table numbers.
		' BPG 15-Feb-2006 - Changed cnum from CurrentNames to Long, because enums are not
		' supported for COM object parameters.
		Dim regNum As GlobalData.IVSourceIndexNumbers
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(4) As Byte
		
		On Error GoTo ErrorHandler
		
		Select Case cnumAsInt
			Case GlobalData.CurrentNames.FRONTODDCURRENT : regNum = GlobalData.IVSourceIndexNumbers.FRONTODDSRCINDEX
			Case GlobalData.CurrentNames.FRONTEVENCURRENT : regNum = GlobalData.IVSourceIndexNumbers.FRONTEVENSRCINDEX
			Case GlobalData.CurrentNames.PHASECURRENT : regNum = GlobalData.IVSourceIndexNumbers.PHASESRCINDEX
			Case GlobalData.CurrentNames.REARLOWCURRENT : regNum = GlobalData.IVSourceIndexNumbers.REARSRCINDEX
			Case GlobalData.CurrentNames.REARHIGHCURRENT : regNum = GlobalData.IVSourceIndexNumbers.REARSRCINDEX
			Case GlobalData.CurrentNames.GAINCURRENT : regNum = GlobalData.IVSourceIndexNumbers.GAINSRCINDEX
			Case GlobalData.CurrentNames.SOAFORWARDCURRENT : regNum = GlobalData.IVSourceIndexNumbers.SOASRCINDEX
			Case GlobalData.CurrentNames.SOAREVERSECURRENT : regNum = GlobalData.IVSourceIndexNumbers.SOASRCINDEX
			Case GlobalData.CurrentNames.CMZLEFTBIASVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.LEFTSRCINDEX
			Case GlobalData.CurrentNames.CMZRIGHTBIASVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.RIGHTSRCINDEX
			Case GlobalData.CurrentNames.CMZLEFTIMBALVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.LEFTIMBALSRCINDEX
			Case GlobalData.CurrentNames.CMZRIGHTIMBALVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.RIGHTIMBALSRCINDEX
		End Select
		
		Call ConvSingleToBytes(temp, iv)
		If Not SetIVSource("LSW", temp(3), temp(4)) Then GoTo Aborted
		If Not SetIVSource("MSW", temp(1), temp(2)) Then GoTo Aborted
		If Not SetIVSourceIndex(CInt(regNum)) Then GoTo Aborted
		If Not SetIVSourceDAC Then GoTo Aborted
		SetSource = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "SetSource")
Aborted: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to set current/voltage source.")
		SetSource = False
	End Function
	
	
	
	Public Function GetSource(ByRef cnumAsInt As Short, ByRef iv As Single) As Boolean
		' This routine performs the text equivalent of "geti/getv" using the standard current/voltage
		' source numbers which are then converted to the Atmel internal table numbers.
		' BPG 15-Feb-2006 - Changed cnum from CurrentNames to Int
		
		Dim regNum As GlobalData.IVSourceIndexNumbers
		'UPGRADE_WARNING: Lower bound of array temp was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim temp(4) As Byte
		
		On Error GoTo ErrorHandler
		
		Select Case cnumAsInt
			Case GlobalData.CurrentNames.FRONTODDCURRENT : regNum = GlobalData.IVSourceIndexNumbers.FRONTODDSRCINDEX
			Case GlobalData.CurrentNames.FRONTEVENCURRENT : regNum = GlobalData.IVSourceIndexNumbers.FRONTEVENSRCINDEX
			Case GlobalData.CurrentNames.PHASECURRENT : regNum = GlobalData.IVSourceIndexNumbers.PHASESRCINDEX
			Case GlobalData.CurrentNames.REARLOWCURRENT : regNum = GlobalData.IVSourceIndexNumbers.REARSRCINDEX
			Case GlobalData.CurrentNames.REARHIGHCURRENT : regNum = GlobalData.IVSourceIndexNumbers.REARSRCINDEX
			Case GlobalData.CurrentNames.GAINCURRENT : regNum = GlobalData.IVSourceIndexNumbers.GAINSRCINDEX
			Case GlobalData.CurrentNames.SOAFORWARDCURRENT : regNum = GlobalData.IVSourceIndexNumbers.SOASRCINDEX
			Case GlobalData.CurrentNames.SOAREVERSECURRENT : regNum = GlobalData.IVSourceIndexNumbers.SOASRCINDEX
			Case GlobalData.CurrentNames.CMZLEFTBIASVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.LEFTSRCINDEX
			Case GlobalData.CurrentNames.CMZRIGHTBIASVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.RIGHTSRCINDEX
			Case GlobalData.CurrentNames.CMZLEFTIMBALVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.LEFTIMBALSRCINDEX
			Case GlobalData.CurrentNames.CMZRIGHTIMBALVOLTAGE : regNum = GlobalData.IVSourceIndexNumbers.RIGHTIMBALSRCINDEX
		End Select
		
		If Not SetIVSourceIndex(regNum) Then GoTo Aborted
		If Not GetIVSource("LSW", temp(3), temp(4)) Then GoTo Aborted
		If Not GetIVSource("MSW", temp(1), temp(2)) Then GoTo Aborted
		iv = Conv4BytesToSingle(temp)
		GetSource = True
		Exit Function
		
ErrorHandler: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.LogINTERNAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Call Logger.LogINTERNAL(Err.Number, Err.Source, Err.Description, "GetSource")
Aborted: 
		'UPGRADE_WARNING: Couldn't resolve default property of object Logger.logFATAL. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Logger.logFATAL("Error: Failed to get current/voltage source.")
		GetSource = False
	End Function
End Class