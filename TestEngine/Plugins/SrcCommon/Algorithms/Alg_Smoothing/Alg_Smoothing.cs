// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_Smoothing.cs
//
// Author: Mark Fullalove
// Design: From Bookham "PcLeslie" software

using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.Algorithms
{
    /// <summary>
    /// An implementation of the algorithm for smoothing data
    /// </summary>
    public sealed class Alg_Smoothing
    {
        private Alg_Smoothing()
        {
            // Empty private constructor
        }

        /// <summary>
        /// An implementation of the pascal triangle smoothing algorithm
        /// </summary>
        /// <param name="rawData">raw data</param>
        /// <param name="smoothingFactor">smoothing factor</param>
        /// <returns>return smoothed data</returns>
        public static double[] PascalTriangleSmooth(double[] rawData, int smoothingFactor)
        {
            if (rawData.Length == 0)
            {
                throw new AlgorithmException("Empty raw data array");
            }

            if (smoothingFactor < 3 || (smoothingFactor % 2) == 0)
            {
                throw new AlgorithmException("Smoothing factor must be an ODD integer greater than 3");
            }

            if (rawData.Length < smoothingFactor)
            {
                throw new AlgorithmException("Smoothing factor cannot be greater than the number of data values");
            }

            List<double> smoothedData = new List<double>(rawData.Length);

            int DataArrayIndex = 0;
            int EndOfDataArray = rawData.Length;

            int DataArrayCounter = DataArrayIndex;
            int DataArrayCurrentPosition = DataArrayIndex;

            int StartSmoothPosition = DataArrayIndex;
            int FinishSmoothPosition = EndOfDataArray;

            int LowerDataArrayBoundary = DataArrayIndex;
            // FinishSmoothPosition points to just beyond the last element
            int UpperDataArrayBoundary = --FinishSmoothPosition;

            int smoothArea = smoothingFactor / 2;

            int cnt = 0, counter = 0;
            int LowerIndex = 0, UpperIndex = 0;

            double Weighting = 0;
            double Value = 0;

            //bool HitBoundaryLowerLimit = false;
            bool HitBoundaryUpperLimit = false;

            while (DataArrayIndex != EndOfDataArray)
            {
                // Initialise our counters and markers
                DataArrayCounter = DataArrayIndex;
                StartSmoothPosition = DataArrayIndex;
                FinishSmoothPosition = DataArrayIndex;

                // Test lower boundary
                for (cnt = 0, LowerIndex = smoothArea;
                    cnt < smoothArea; cnt++)
                {
                    if (DataArrayCounter != LowerDataArrayBoundary)
                    {
                        DataArrayCounter--;
                        StartSmoothPosition--;
                        LowerIndex--;
                    }
                    //else
                    //    HitBoundaryLowerLimit = true;
                }

                // reset counter and test upper boundary
                DataArrayCounter = DataArrayIndex;
                for (HitBoundaryUpperLimit = false, cnt = 0, UpperIndex = smoothArea;
                    cnt < smoothArea; cnt++)
                {
                    if (DataArrayCounter != UpperDataArrayBoundary)
                    {
                        DataArrayCounter++;
                        FinishSmoothPosition++;
                        UpperIndex--;
                    }
                    else
                        HitBoundaryUpperLimit = true;
                }


                DataArrayCounter = StartSmoothPosition;
                Weighting = 0;
                Value = 0;

                if (HitBoundaryUpperLimit)
                    counter = 0;
                else
                    counter = LowerIndex;

                // Hurrah, we can now start to smooth
                while (DataArrayCounter <= FinishSmoothPosition)
                {
                    Value += (rawData[DataArrayCounter] * PascalTriangle(smoothingFactor - 1, counter));
                    Weighting += PascalTriangle(smoothingFactor - 1, counter);
                    DataArrayCounter++;
                    counter++;
                }

                smoothedData.Add(Value / Weighting);

                DataArrayIndex++;

            } // End of while

            return smoothedData.ToArray();
        }

        /// <summary>
        /// smoothing a line by a moving window which each point is the average of points in the window
        /// </summary>
        /// <param name="array">input array</param>
        /// <param name="pointNumberOfWindow">the point number of half of the window</param>
        /// <returns>smoothed array</returns>
        public static double[] AveragingSmoothing(double[] array, int pointNumberOfWindow)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }

            if (pointNumberOfWindow < 2)
            {
                throw new ArgumentOutOfRangeException("pointNumberOfWindow", pointNumberOfWindow, "Point number of window must >=2");
            }

            //Point number of the whole window=2*pointNumberOfHalfWindow+1
            int pointNumberOfHalfWindow = (pointNumberOfWindow - 1) / 2;

            double[] returnArray = new double[array.Length];

            //calculate the begining points
            for (int i = 0; i < pointNumberOfHalfWindow; i++)
            {
                double sum = 0;
                int count = 0;

                for (int j = 0; j < i + pointNumberOfHalfWindow + 1; j++)
                {
                    count++;
                    sum += array[j];
                }

                returnArray[i] = sum / count;

            }

            //calculate middle points
            for (int i = pointNumberOfHalfWindow; i < array.Length - pointNumberOfHalfWindow; i++)
            {
                double sum = 0;

                for (int j = i - pointNumberOfHalfWindow; j < i + pointNumberOfHalfWindow + 1; j++)
                {
                    sum += array[j];
                }

                returnArray[i] = sum / (2 * pointNumberOfHalfWindow + 1);
            }


            //calculate the ending points
            for (int i = array.Length - pointNumberOfHalfWindow; i < array.Length; i++)
            {
                double sum = 0;
                int count = 0;

                for (int j = i - pointNumberOfHalfWindow; j < array.Length; j++)
                {
                    count++;
                    sum += array[j];
                }

                returnArray[i] = sum / count;

            }


            return returnArray;
        }

        /// <summary>
        /// smoothing a line by a moving window which each point is the average of points in the window
        /// </summary>
        /// <param name="array">input array</param>
        /// <returns>smoothed array</returns>
        public static double[] AveragingSmoothingWithThreePointWindow(double[] array)
        {
            return AveragingSmoothing(array, 3);
        }

        /// <summary>
        /// smoothing a line by a moving window which each point is the average of points in the window
        /// </summary>
        /// <param name="array">input array</param>
        /// <returns>smoothed array</returns>
        public static double[] AverageingSmoothingWithFivePointWindow(double[] array)
        {
            return AveragingSmoothing(array, 5);
        }

        static double PascalTriangle(int RowNumber, int Element)
        {
            double Value = 0;

            // preconditions
            if (RowNumber == 1 || RowNumber == 2)
            {
                Value = 1;

                return (Value);
            }

            double rowMinusElementFactorial = factorial(RowNumber - Element);
            double elementFactorial = factorial(Element);

            Value = factorial(RowNumber) / (elementFactorial * rowMinusElementFactorial);

            return (Value);
        }

        static long factorial(int n)
        {
            long product = 1;
            //Console.WriteLine("N = " + n.ToString());

            if (n <= 1)
                return (product);

            for (; n > 1; --n)
            {
                product *= n;
                //Console.WriteLine("N = " + n.ToString() + ", Product = " + product.ToString());
            }
            return product;
        }
    }
}
