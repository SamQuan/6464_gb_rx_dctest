// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestEngine.Equipment
//
// Chassis_Nt10a.cs
//
// Author: K Pillar
// Design: As specified in Test Module/Program DD 

using System;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisTypes;

namespace Bookham.TestLibrary.ChassisNS
{
	#region Public Functions
	
	/// <summary>
    /// Chassis_Nt10a driver class.
	/// </summary>
	public class Chassis_Nt10a : ChassisType_Visa
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chassisName">Chassis name</param>
		/// <param name="driverName">Chassis driver name</param>
		/// <param name="resourceStringId">Resource data</param>
        public Chassis_Nt10a(string chassisName, string driverName, string resourceStringId)
			: base (chassisName, driverName, resourceStringId)
		{
			// Configure valid hardware information

            // Add details of Nt10a chassis
            ChassisDataRecord Nt10aData = new ChassisDataRecord(
                "Nt10a",		// hardware name 
				"NOT SUPPORTED",										// minimum valid firmware version 
                "NOT SUPPORTED");										// maximum valid firmware version 
            ValidHardwareData.Add("Nt10a", Nt10aData);
            
        }

		/// <summary>
		/// Firmware version
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				//cannot ask chassis for firmware version: Not supported
	            //by instrument
				return ("NOT SUPPORTED");
			}
		}

		/// <summary>
		/// Hardware identity string
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
                //cannot ask chassis for Hardware Identity: Not supported
                //by instrument
                return ("Nt10a");
			}
		}


        

       

		#endregion

	}
	
}
