// [Copyright]
//
// Bookham Test Engine Equipment Driver Library
// Bookham.TestSolution.Instruments
//
// Inst_Ag86060_SimpleSwitch.cs
//
// Author: joseph.olajubu, 2007
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.ChassisNS;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Instrument to control one Layer of an Agilent 86060C 1 x N Optical Switch
    /// </summary>
    public class Inst_Ag86060_SimpleSwitch : InstType_SimpleSwitch, IInstType_SimpleSwitch
    {
        #region Constructor
        /// <summary>
        /// Instrument Constructor.
        /// </summary>
        /// <param name="instrumentNameInit">Instrument name</param>
        /// <param name="driverNameInit">Instrument driver name</param>
        /// <param name="slotInit">Slot ID for the instrument</param>
        /// <param name="subSlotInit">Sub Slot ID for the instrument</param>
        /// <param name="chassisInit">Chassis through which the instrument communicates</param>
        public Inst_Ag86060_SimpleSwitch(string instrumentNameInit, string driverNameInit,
            string slotInit, string subSlotInit, Chassis chassisInit)
            : base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
            chassisInit)
        {
            // Setup expected valid hardware variants 
            // TODO: Update with useful names, real values, create multiple copies as required.
            InstrumentDataRecord instr86060C = new InstrumentDataRecord(
                "HEWLETT-PACKARD 86060C",				// hardware name 
                "0",  			// minimum valid firmware version 
                "2.00");			// maximum valid firmware version 
            ValidHardwareData.Add("86060C", instr86060C);

            InstrumentDataRecord instr86064 = new InstrumentDataRecord(
                "HEWLETT-PACKARD 86064",				// hardware name 
                "0",  			// minimum valid firmware version 
                "2.00");			// maximum valid firmware version 
            ValidHardwareData.Add("86064", instr86064);

            // Configure valid chassis driver information
            // TODO: Update with useful key name, real chassis name and versions
            InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
                "Chassis_Ag86060",								// chassis driver name  
                "0.0.0.0",									// minimum valid chassis driver version  
                "2.0.0.0");									// maximum valid chassis driver version
            ValidChassisDrivers.Add("86060C", chassisInfo);

            // initialise this instrument's chassis
            // TODO - cast to the appropriate type.
            // don't forget to change the private variable type too.
            this.ag86060Chassis = (Chassis_Ag86060)chassisInit;
        }
        #endregion

        #region Instrument overrides

        /// <summary>
        /// Firmware version of this instrument.
        /// </summary>
        public override string FirmwareVersion
        {
            get
            {
                return ag86060Chassis.FirmwareVersion;
            }
        }

        /// <summary>
        /// Hardware Identity of this instrument.
        /// </summary>
        public override string HardwareIdentity
        {
            get
            {
                return ag86060Chassis.HardwareIdentity;
            }
        }

        /// <summary>
        /// Set instrument to default state
        /// </summary>
        public override void SetDefaultState()
        {

            /* Send a reset command. */
            ag86060Chassis.Write("*RST", this);
            ag86060Chassis.Write_Unchecked("*WAI", this);

        }

        /// <summary>
        /// Setup the instrument as it goes online
        /// </summary>
        public override bool IsOnline
        {
            get
            {
                return base.IsOnline;
            }
            set
            {
                // setup base class
                base.IsOnline = value;

                if (value) // if setting online                
                {
                    // TODO: custom setup for this instrument after it goes online                    
                }
            }
        }
        #endregion

        #region Private data
        /// <summary>
        /// Instrument's chassis
        /// </summary>
        private Chassis_Ag86060 ag86060Chassis;
        #endregion

        /// <summary>
        /// Maximum switch state
        /// </summary>
        public override int MaximumSwitchState
        {
            get
            {
                //Query the instrument.

                /* Get the Switch Configuration
                   Each Layer of this device shall be treated as a simple 1 x N Switch. To find the number of layers, and the 
                   number of output ports per layer, Query the instrument. The response is of the following form:
                   Returned Format: <config> = �L<i>A<j1>A<k1>B<l1>B<m1>A<j2>A<k2>B<l2>B<m2>...?
                   This driver only supports Layer 1.
                  
                  Where: <i> = number of layers on switch
                        <j1> = minimum available channel on port A, layer 1
                        <k1> = maximum available channel on port A, layer 1
                        <l1> = minimum available channel on port B, layer 1
                        <m1> = maximum available channel on port B, layer 1
                        <j2> = minimum available channel on port A, layer 2
                        <k2> = maximum available channel on port A, layer 2
                        <l2> = minimum available channel on port B, layer 2
                        <m2> = maximum available channel on port B, layer 2
                  */

                if (this.FirmwareVersion.Trim().StartsWith("2"))
                {
                    string resp = ag86060Chassis.Query("SYST:CONF?", this);

                    int numPortInfoFields = resp.Length / 2;
                    string[] info = new string[numPortInfoFields];

                    for (int i = 0; i < numPortInfoFields; i++)
                    {
                        info[i] = resp.Substring(i * 2, 2);
                    }

                    return Convert.ToInt32(info[4].Remove(0, 1));
                }
                else
                {
                    string resp = ag86060Chassis.Query("SYST:CONF?", this).Replace("\"", "");
                    string[] info = resp.Split(' ');
                    return Convert.ToInt32(info[4].Remove(0, 1));
                }
            }
        }


        /// <summary>
        /// Minimum Switch State
        /// </summary>
        public override int MinimumSwitchState
        {
            get
            {
                //Query the instrument.

                /* Get the Switch Configuration
                   Each Layer of this device shall be treated as a simple 1 x N Switch. To find the number of layers, and the 
                   number of output ports per layer, Query the instrument. The response is of the following form:
                   Returned Format: <config> = �L<i>A<j1>A<k1>B<l1>B<m1>A<j2>A<k2>B<l2>B<m2>...?
                   This driver only supports Layer 1.
                 
                  An output of 0 means no output. 
                  
                  Where: <i> = number of layers on switch
                        <j1> = minimum available channel on port A, layer 1
                        <k1> = maximum available channel on port A, layer 1
                        <l1> = minimum available channel on port B, layer 1
                        <m1> = maximum available channel on port B, layer 1
                        <j2> = minimum available channel on port A, layer 2
                        <k2> = maximum available channel on port A, layer 2
                        <l2> = minimum available channel on port B, layer 2
                        <m2> = maximum available channel on port B, layer 2
                  */
                if (this.FirmwareVersion.Trim().StartsWith("2"))
                {
                    string resp = ag86060Chassis.Query("SYST:CONF?", this);
                    int numPortInfoFields = resp.Length / 2;
                    string[] info = new string[numPortInfoFields];

                    for (int i = 0; i < numPortInfoFields; i++)
                    {
                        info[i] = resp.Substring(i * 2, 2);
                    }

                    return Convert.ToInt32(info[3].Remove(0, 1));
                }
                else
                {
                    string resp = ag86060Chassis.Query("SYST:CONF?", this).Replace("\"", "");
                    string[] info = resp.Split(' ');
                    return Convert.ToInt32(info[3].Remove(0, 1));
                }
            }
        }

        /// <summary>
        /// Set/Get The Switch State.
        /// </summary>
        public override int SwitchState
        {
            get
            {
                //Query the instrument.
                string resp = ag86060Chassis.Query("ROUTE:LAYER1:CHANNEL?", this);
                //Response is of form A<channel>,B<channel>, where <channel> = 0 - N
                string[] switchMap = resp.Split(',');

                char[] portId = { 'B' };
                int outputPort = Convert.ToInt32(switchMap[1].TrimStart(portId));
                return outputPort;
            }
            set
            {
                //Validate valid switch position.
                if ((value >= this.MinimumSwitchState) && (value <= this.MaximumSwitchState))
                {
                    //Build cmd string
                    string gpibCmd = "ROUTE:LAYER1:CHANNEL A1,B" + value;
                    ag86060Chassis.Write(gpibCmd, this);
                }
                else
                {
                    throw new InstrumentException("Invalid switch Position Specified: " + value);
                }
            }
        }
    }
}
