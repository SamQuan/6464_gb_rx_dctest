using System;
using System.Collections.Generic;
using System.Text;

namespace Bookham.TestLibrary.ChassisNS
{
	/// <summary>
	/// Bookham Bert Status
	/// </summary>
	public enum BkhmMMSStatus
	{
		///<summary>OK</summary>
		Ok = 0,
		///<summary>Port not open</summary>
		PortNotOpen = 1,
		///<summary>Invalid Tx data</summary>
		InvalidTxData = 2,
		///<summary>Port not available</summary>
		PortNotAvail = 3
	}

	/// <summary>
	/// Bookham BERT exception
	/// </summary>
	public class BkhmMMSException : Exception
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message">Exception message</param>
		public BkhmMMSException(string message)
			: base(message)
		{
			TxData = null;
			RxData = null;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="message">Exception message</param>
		/// <param name="txData">Transmit data</param>
		/// <param name="rxData">Rx data</param>
		public BkhmMMSException(string message, byte[] txData, byte[] rxData)
			: base(message)
		{
			TxData = txData;
			RxData = rxData;
		}

		/// <summary>
		/// Transmit data
		/// </summary>
		public readonly byte[] TxData;

		/// <summary>
		/// Received data
		/// </summary>
		public readonly byte[] RxData;

		/// <summary>
		/// Override ToString method
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			string errMsg = "";
			// if there's binary info - add to exception so it is in logs
			if ((TxData != null) && (RxData != null))
			{
				string txDataStr = ByteArrayToString(TxData);
				string rxDataStr = ByteArrayToString(RxData);
				errMsg = String.Format("\nSent:{0}\nReceived:{1}",
					txDataStr, rxDataStr);
			}

			return errMsg + base.ToString();
		}

		/// <summary>
		/// Method to convert byte array to string
		/// </summary>
		/// <param name="bArray">Byte array or List of bytes</param>
		/// <returns>String representation</returns>
		public static string ByteArrayToString(IEnumerable<byte> bArray)
		{
			StringBuilder s = new StringBuilder();
			s.Append("[ ");
			foreach (byte b in bArray)
			{
				string byteString = String.Format("{0:X2} ", b);
				s.Append(byteString);
			}
			s.Append("]");
			return s.ToString();
		}

	}

	/// <summary>
	/// Bookham status exception
	/// </summary>
	public class BkhmMMSStatusException : BkhmMMSException
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="status">Status</param>
		/// <param name="txData">Transmit data</param>
		/// <param name="rxData">Received data</param>
		public BkhmMMSStatusException(byte status, byte[] txData, byte[] rxData)
			: base("BERT Status=" + status.ToString(), txData, rxData)
		{
			Status = status;
		}

		/// <summary>
		/// Status
		/// </summary>
		public readonly byte Status;
	}
}
