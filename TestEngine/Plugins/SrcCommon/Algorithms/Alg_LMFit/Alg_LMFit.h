// [Copyright]
//
// Bookham Test Engine Algorithms
// Bookham.TestLibrary.Algorithms
//
// Alg_LMFit.h
// A managed wrapper for a Levenberq-Marquardt fit routine
//
// Author: Mark Fullalove

#pragma once
#include "lmmin.h"
#include "lm_eval.h"
#include <stdlib.h>
#include <stdio.h>	// only for printf in PerformFit_InternalFunction


using namespace System;
using namespace System::Runtime::InteropServices;

namespace Bookham
{
	namespace TestLibrary
	{	
		namespace Algorithms
		{
			public delegate double ModelFunc(double x_point, double* coeffs);

			public ref class Alg_LMFit
			{
				public:
					static double PerformFit(array<double> ^ xData, array<double> ^ yData, array<double> ^% coeffs, ModelFunc^ modelFunction);
						
				private:
					Alg_LMFit(void);
					typedef double (__stdcall *user_func) (double user_x_point, double * coeffs);
					static double PerformFit_InternalFunction(array<double> ^ xData, array<double> ^ yData, array<double> ^% coeffs);
			};
		}
	}
}
