// [Copyright]
//
// Bookham Test Engine
// TODO - Full name of assembly
//
// TP_CoRxDcTest/ProgramGui.cs
// 
// Author: paul.annetts
// Design: TODO Title of design document

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bookham.TestSolution.TestPrograms
{
    /// <summary>
    /// Gui for phase angle test
    /// </summary>
    public partial class TP_CoRxPATestGui : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TP_CoRxPATestGui()
        {
            /* Call designer generated code. */
            InitializeComponent();

            /* TODO - Write constructor code here. */
        }

        /// <summary>
        /// Message Received Event Handler
        /// </summary>
        /// <param name="payload">Message Payload</param>
        /// <param name="inMsgSeq">Incoming message sequence number</param>
        /// <param name="respSeq">Outgoing message sequence number</param>
        private void ProgramGui_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            Type t = payload.GetType();


            if (t == typeof(GuiMsg.CoRxConnectorSwitchRqst))
            {
                label1.Text = ((GuiMsg.CoRxConnectorSwitchRqst)payload).Message;
                SwitchFibreConnector switchForm = new SwitchFibreConnector(this);
                //if (gbCaution.Visible) gbCaution.Visible = false;
                panel1.Controls.Clear();
                panel1.Controls.Add(switchForm);
                panel1.Controls[0].Dock = DockStyle.Fill;
                this.Refresh();
                // load picture to show how to connect fibre connectors
            }
            else if (t == typeof(string))
                label1.Text = payload.ToString();
            else if (t == typeof(GuiMsg.CautionOfLaserRqst))
            {
                gbCaution.Visible = true;
                this.Refresh();
            }
            else if (t == typeof(GuiMsg.CancelOflLaserCautionRqst))
            {
                gbCaution.Visible = false;
                this.Refresh();
            }
            else if (t == typeof(GuiMsg .GetChipInfoRqst))
            {
                gbCaution.Visible = false;
                this.Refresh();
                GetChipInfoGui getInfoGui = new GetChipInfoGui(this);
                panel1.Controls.Clear();
                panel1.Controls.Add(getInfoGui);
                panel1.Controls[0].Dock = DockStyle.Fill;
                this.Refresh();
            }
        }

        /// <summary>
        /// Expose the protected send to worker to allow other controls to use it!
        /// </summary>
        /// <param name="payload"></param>
        internal void SendToWorker(object payload)
        {
            this.sendToWorker(payload);
        }

        /// <summary>
        /// Indicate that the control has finished
        /// </summary>
        internal void CtrlFinished()
        {
            panel1.Controls.Clear();
            this.Refresh();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            gbCaution.Visible = false;
        }
    }
}
