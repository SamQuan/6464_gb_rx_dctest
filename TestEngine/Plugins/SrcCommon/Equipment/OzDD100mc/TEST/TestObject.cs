using System;
using System.Collections.Generic;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
//using Bookham.TestEngine.PluginInterfaces.Instrument;
//using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Instruments;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.InstrTypes;

namespace Ag8156A_Harness
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region References to the objects to test - Chassis and Instruments
        // TODO - PUT YOUR CHASSIS REFERENCE HERE
        private Chassis_OzDd100mc testChassis1;
        private Chassis_OzDd100mc testChassis2;

        // TODO - PUT YOUR INSTRUMENT REFERENCES HERE
       
        string fileCalibraForAttenuSig = @"C:\ConherentRx_RfTest_Lab\TestEngine\Plugins\SrcCommon\Equipment\OzDD100mc\stepForAttenuSig.csv";
        string fileCalibraForAttenuLoc = @"C:\ConherentRx_RfTest_Lab\TestEngine\Plugins\SrcCommon\Equipment\OzDD100mc\stepForAttenuLoc.csv";

        private Inst_OzDd100mc_Attenuator AttenuatorSig;
        private InstType_OpticalAttenuator testAttenuatorSig;
        private Inst_OzDd100mc_Attenuator AttenuatorLoc;
        private InstType_OpticalAttenuator testAttenuatorLoc;
        #endregion


        #region Constants for use during test.
        // VISA Chassis for where to find the instrument
        const string visaResource1 = "COM1";
        const string chassisName1 = "Chassis_OzDd100mc1";
        const string instr1Name1 = "ATT1";

        //const string visaResource2 = "COM2";
        //const string chassisName2 = "Chassis_OzDd100mc2";
        //const string instr1Name2 = "ATT2";
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis object
            // TODO...
            TestOutput("Don't forget to create chassis objects");
            testChassis1 = new Chassis_OzDd100mc(chassisName1, "Chassis_OzDd100mc", visaResource1);
            //TestOutput(chassisName1, "Created OK");
            //testChassis2 = new Chassis_OzDd100mc(chassisName2, "Chassis_OzDd100mc", visaResource2);
            //TestOutput(chassisName2, "Created OK");

            // create instrument objects            
            // TODO...
            TestOutput("Don't forget to create instrument objects");
            AttenuatorSig = new Inst_OzDd100mc_Attenuator(instr1Name1, "Inst_OzDd100mc_Attenuator", "", "", testChassis1);
            TestOutput(instr1Name1, "Created OK");
            //AttenuatorLoc = new Inst_OzDd100mc_Attenuator(instr1Name2, "Inst_OzDd100mc_Attenuator", "", "", testChassis2);
            //TestOutput(instr1Name2, "Created OK");

            // put them online
            // TODO...
            TestOutput("Don't forget to put equipment objects online");
            testChassis1.IsOnline = true;
            TestOutput(chassisName1, "IsOnline set true OK");
            AttenuatorSig.IsOnline = true;
            TestOutput(instr1Name1, "IsOnline set true OK");

            //testChassis2.IsOnline = true;
            //TestOutput(chassisName2, "IsOnline set true OK");
            //AttenuatorLoc.IsOnline = true;
            //TestOutput(instr1Name2, "IsOnline set true OK");

            AttenuatorSig.InitPowerCalibraData(fileCalibraForAttenuSig);
           // AttenuatorLoc.InitPowerCalibraData(fileCalibraForAttenuLoc);

            testAttenuatorSig = (InstType_OpticalAttenuator)AttenuatorSig;
            //testAttenuatorLoc = (InstType_OpticalAttenuator)AttenuatorLoc;
            
            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {            
            TestOutput("\n\n*** T01_FirstTest ***");
            testAttenuatorSig.Attenuation_dB = 30;
           // testAttenuatorLoc.Attenuation_dB = 40;
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            //Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(string objStr, string output)
        {
            string outputStr = String.Format("{0}: {1}", objStr, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}
