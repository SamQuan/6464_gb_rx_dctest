using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Utilities;
using Microsoft.Office.Interop.Excel;

namespace SinusoidalFitTest
{
    public partial class FrmSinusoidalFit : Form
    {
        private string  sinusoidalFitTemplatePath;
        public FrmSinusoidalFit()
        {
            InitializeComponent();
            dlgGetFile.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            sinusoidalFitTemplatePath = @"Fit try2.xls";
        }

        private void btnGetFile_Click(object sender, EventArgs e)
        {            
            dlgGetFile.ShowDialog(this);
            txtFileName.Text = dlgGetFile.FileName;

            btnGetFile.Enabled = false;

            double[] xData = null;
            double[] yData = null ;
            string fitDataFilePrefix = "";
            string fileName;
            bool flgGetFile = GetWaveFormData(txtFileName.Text.Trim(), out xData ,out yData , out fitDataFilePrefix);

            if (flgGetFile && xData != null && yData != null & xData .Length >1)
            {

                //  offset, amplitude, power, phase, period
                double[] initCoeff = new double[5];

                initCoeff[0] = 0;
                initCoeff[1] = 0.05;
                initCoeff[2] = 1;
                initCoeff[3] = 0;
                initCoeff[4] = 0.00000002;

                double[] upperCoeff = new double[5];
                upperCoeff[0] = 0.003;
                upperCoeff[1] = 0.065;
                upperCoeff[2] = 1;
                upperCoeff[3] = 0.000000012;
                upperCoeff[4] = 0.000000022;

                double[] lowCoeff = new double[5];
                lowCoeff[0] = -0.003;
                lowCoeff[1] = 0.004;
                lowCoeff[2] = 1;
                lowCoeff[3] = -0.000000012;
                lowCoeff[4] = 0.000000018;


                SinusoidalFit sinFit = Alg_SinusoidalFit.PerformFit(xData, yData, initCoeff); // , upperCoeff ,lowCoeff);

                txtOffset.Text = sinFit.Coeffs[0].ToString ("0.00000");
                txtOffset.Refresh();
                txtAmp.Text = sinFit.Coeffs[1].ToString("0.00000");
                txtAmp.Refresh();
                txtFreq.Text = ((double)(1.0 / sinFit.Coeffs[4])).ToString("0.000");
                txtFreq.Refresh();
                double phase =(sinFit.Coeffs[3] / sinFit.Coeffs[4]);
                phase *= 360;
                phase %= 360;
                phase = -phase;
                txtPhase.Text = phase.ToString("0.000");
                txtPhase.Refresh();
                txtMse.Text = sinFit.MeanSquaredError.ToString("0.000000");
                txtMse.Refresh();

                bool flgFileOk = WriteFile(xData, yData, sinFit, fitDataFilePrefix, out fileName);
                if (flgFileOk)
                {
                    textBox1.Text = fileName;
                    textBox1.Refresh();
                }

            }
            btnGetFile.Enabled = true;
        }

        internal bool GetWaveFormData(string fileName, out double[] Xdata, out double[] Ydata, out string WaveformName)
        {
            CsvReader reader = new CsvReader();

            bool flgGetFile = false;

            Xdata = null;
            Ydata = null;
            WaveformName = "";

            List<string[]> dataList =null ;
            try
            {
                dataList = reader.ReadFile(fileName);
                flgGetFile = true;
            }
            catch
            {
 
            }
            Xdata = new double[dataList.Count - 1];
            Ydata = new double[dataList.Count - 1];

            for (int indx = 1; indx < dataList.Count; indx++)
            {
                Xdata[indx -1] = double .Parse ( dataList[indx][0]);
                Ydata[indx - 1] = double.Parse( dataList[indx][1]);
            }

            WaveformName = dataList[0][1];
            return flgGetFile;
        }

        internal bool WriteFile(double[] xData, double[] yData,SinusoidalFit sinFit, string filePrefix, out string fileName)
        {
            bool flgFileOk = false;

            List<string > lstData = new List<string> ();
            lstData.Add ("X data,Y data, Fit data,Fited Error, ");
            
            StringBuilder dataStr ;
            
            for ( int ind = 0; ind < xData.Length ; ind++)
            {
                dataStr = new StringBuilder ();
                dataStr.Append(xData[ind]);
                dataStr.Append(",");
                dataStr.Append(yData[ind]);
                dataStr.Append(",");
                dataStr.Append(sinFit.FittedYArray[ind]);
                dataStr.Append(",");
                dataStr .Append ( Math .Pow ( ( yData[ind]- sinFit.FittedYArray[ind]), 2));
                dataStr .Append (",");
                lstData.Add(dataStr.ToString());

 
            }

            lstData .Add ( string .Format ("Offset, {0},", sinFit .Coeffs[0]));
            lstData .Add (string.Format ("Amplitude,{0},", sinFit .Coeffs [1]));
            lstData.Add(string.Format("Phase,{0},", sinFit.Coeffs[3]));
            lstData .Add (string .Format ("Period, {0}", sinFit .Coeffs [4]));
            lstData .Add (string .Format ("Mean Square Fit, {0}", sinFit .MeanSquaredError ));

            fileName = string.Format("{0}_{1}.csv", filePrefix, DateTime.Now.ToString("yyyyMMddHHmmssfff"));

            fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);

            using ( CsvWriter writer = new CsvWriter ())
            {
                try
                {               
                    writer .WriteFile ( fileName ,lstData);
                    flgFileOk = true ;                    
                }
                catch 
                {
                    return flgFileOk ;
                }
            }
            return flgFileOk ;
        }

        private void btnFitTmp_Click(object sender, EventArgs e)
        {
            dlgGetFile.ShowDialog(this);
            txtFileName.Text = dlgGetFile.FileName;

            btnFitTmp.Enabled = false;

            double[] xData = null;
            double[] yData = null ;
            string fitDataFilePrefix = "";
            string fileName;
            bool flgGetFile = GetWaveFormData(txtFileName.Text.Trim(), out xData ,out yData , out fitDataFilePrefix);

            Microsoft.Office.Interop.Excel.Application excel = null;

            Workbook excWorkBook = null;
            Worksheet excSheet = null;

            try
            {
                excel =
                    new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = true;
                if (!sinusoidalFitTemplatePath.Contains("\\"))
                    sinusoidalFitTemplatePath = Path.Combine(System.Windows.Forms.Application.StartupPath, sinusoidalFitTemplatePath);

                excWorkBook = excel.Workbooks.Open(sinusoidalFitTemplatePath, Type.Missing, false, Type.Missing, Type.Missing, Type.Missing, true,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                excSheet = excWorkBook.Worksheets[1] as Worksheet;


                if (excSheet != null)
                {
                    int row_Start = 21;
                    string col_1 = "AZ";
                    string col_2 = "BA";

                    for (int idx = 0; idx < yData.Length; idx++)
                    {
                        excSheet.Cells[row_Start + idx, col_1] = yData[idx];
                        excSheet.Cells[row_Start + idx, col_2] = yData[idx];

                    }
                    bool status = true;
                    Range range;
                    for (int ii = 19; ii <= 22; ii++)
                    {

                        range = excel.get_Range("AS" + ii.ToString(), Missing.Value);
                        if ((double)range.Value2 == -2146826246.0)
                            status = false;
                    }

                    excel.Run("PhaseFits", Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                      Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value
                    , Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);

                    SinusoidalFit cosFit = new SinusoidalFit();
                    string col_coeff = "AT"; // 46;
                    string val = excSheet.get_Range(excSheet.Cells["22", col_coeff], excSheet.Cells["22", col_coeff]).Text.ToString();
                    cosFit.Coeffs = new double[5];
                    cosFit.Coeffs[0] = double.Parse(val);
                    cosFit.Coeffs[1] = double.Parse(excSheet.get_Range(excSheet.Cells[21, col_coeff], excSheet.Cells[21, col_coeff]).Text.ToString());
                    cosFit.Coeffs[2] = 1;
                    cosFit.Coeffs[3] = double.Parse(excSheet.get_Range(excSheet.Cells[36, col_coeff], excSheet.Cells[36, col_coeff]).Text.ToString());
                    cosFit.Coeffs[4] = double.Parse(excSheet.get_Range(excSheet.Cells[19, col_coeff], excSheet.Cells[19, col_coeff]).Text.ToString());

                    double fit_Corel = double.Parse(excSheet.get_Range(excSheet.Cells[17, "AQ"], excSheet.Cells[17, "AQ"]).Text.ToString());
                    cosFit.FittedYArray = new double[yData.Length];
                    string  col_fitData = "Q";
                    int fit_Length = 141;
                    cosFit.FittedYArray = new double[yData.Length];

                    for (int idx = 0; idx < fit_Length; idx++)
                    {
                        cosFit.FittedYArray[idx] = double.Parse(excSheet.get_Range(excSheet.Cells[21+idx, col_fitData], excSheet.Cells[21+idx, col_fitData]).Text.ToString());
                    }
                    for (int idx = fit_Length; idx < yData.Length; idx++)
                    {
                        cosFit.FittedYArray[idx] = 0;
                    }

                    txtOffset.Text = cosFit.Coeffs[0].ToString("0.00000");
                    txtOffset.Refresh();
                    txtAmp.Text = cosFit.Coeffs[1].ToString("0.00000");
                    txtAmp.Refresh();
                    txtFreq.Text = cosFit.Coeffs[4].ToString("0.000");
                    txtFreq.Refresh();
                    double phase = cosFit.Coeffs[3];                    
                    txtPhase.Text = phase.ToString("0.000");
                    txtPhase.Refresh();
                    txtMse.Text = cosFit.MeanSquaredError.ToString("0.000000");
                    txtMse.Refresh();

                    bool flgFileOk = WriteFile(xData, yData, cosFit, fitDataFilePrefix, out fileName);
                    if (flgFileOk)
                    {
                        textBox1.Text = fileName;
                        textBox1.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (excSheet != null) excSheet = null;
                if (excWorkBook != null) excWorkBook.Close(false, false, Type.Missing);
                if (excel != null) excel.Quit();
            }

            btnFitTmp.Enabled = true;
        }

    }
}