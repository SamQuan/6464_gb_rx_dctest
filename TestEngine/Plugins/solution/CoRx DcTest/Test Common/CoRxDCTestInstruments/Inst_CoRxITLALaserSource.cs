using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestLibrary.InstrTypes;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestLibrary.Instruments;
using Bookham.TestEngine.PluginInterfaces.Instrument;

namespace Bookham.TestLibrary.Instruments
{
    /// <summary>
    /// Class to use iTLA  as tunale laser source 
    /// </summary>
    public  class Inst_CoRxITLALaserSource : IInstType_TunableLaserSource, IInstType_RxTunableLaserSource 
    {
        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="instrName">  instrument name </param>
        /// <param name="iTLASource"> iTLA </param>
        public Inst_CoRxITLALaserSource(string instrName, Instr_ITLA iTLASource)
        {

            iTLA = iTLASource;
            iTLA.ixoliteChassis.SelectMyPort();   // To ensure the com can be switch to the right port so that the right iTLA character could be got
            
            if (!iTLA.IsOnline)   // so as to simulate
            {
                minFreq_GHz = 191700;
                maxFreq_GHz = 196100;

                minPower_dBm = 7;
                maxPower_dBm = 11;
                iTLAFreqGrid_GHz = 50;
            }
            else
            {
                iTLA.GetNOP();
                minFreq_GHz = iTLA.GetLaserFirstFreq_GHz();
                maxFreq_GHz = iTLA.GetLaserLastFreq_GHz();
                
                minPower_dBm = iTLA.GetOpticalPowerMin_dBm();
                maxPower_dBm = iTLA.GetOpticalPowerMax_dBm();
                iTLAFreqGrid_GHz = iTLA.GetFrequencyGrid_GHz();
            }
            minWaveLen_nM = Math .Round ( Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(maxFreq_GHz),3);
            maxWaveLen_nM = Math .Round ( Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(minFreq_GHz),3);
            chanNum = (int)((maxFreq_GHz - minFreq_GHz) / iTLAFreqGrid_GHz) +1;
            
            name = instrName;
        }

        private readonly Instr_ITLA iTLA;
        private readonly double iTLAFreqGrid_GHz;
        private readonly string name="";
        
        /// <summary>
        /// Get Freq inteval btw channels
        /// </summary>
        public double FreqGrid_GHz
        {
            get { return iTLAFreqGrid_GHz; }
        } 


        /// <summary>
        /// Get iTLA
        /// </summary>
        /// <returns></returns>
        public Instr_ITLA GetItla()
        { return iTLA; }
    
        #region Min-Max Rating
        private readonly double minFreq_GHz;
        private readonly double maxFreq_GHz;
        private readonly double minWaveLen_nM;
        private readonly double maxWaveLen_nM;
        private readonly double minPower_dBm;
        private readonly double maxPower_dBm;
        private readonly int chanNum;
        
        /// <summary>
        /// Get total ITU channnel count available for this ITLA
        /// </summary>
        public int ChanNum
        {
            get { return chanNum; }
        } 
               
        #endregion

        #region IInstType_TunableLaserSource Members
        /// <summary>
        /// Get/Set optical output enable status
        /// </summary>
        public bool BeamEnable
        {
            get
            {
                return iTLA .GetSoftwareEnableOutput();
            }
            set
            {
                iTLA.GetNOP();
                iTLA.SoftwareEnableOutput(value);
            }
        }
        
        /// <summary>
        /// Maximun output power in dBm
        /// </summary>
        public double MaximumOuputPower
        {
            get { return maxPower_dBm; }
        }
        /// <summary>
        /// Maximun wavelength in nm
        /// </summary>
        public double MaximumWavelengthINnm
        {
            get { return maxWaveLen_nM; }
        }
        /// <summary>
        /// Minimun output power in dBm
        /// </summary>
        public double MinimumOutputPower
        {
            get { return minPower_dBm; }
        }
        /// <summary>
        /// Minimun wavelength in nm
        /// </summary>
        public double MinimumWavelengthINnm
        {
            get { return minWaveLen_nM; }
        }
        /// <summary>
        /// Set/Get Power output in dBm
        /// </summary>
        public double Power
        {
            get
            {
                return iTLA .GetLaserOutputPower_dBm();
            }
            set
            {
                iTLA.GetNOP();
                double power_dBm;
                // Clamp setting value to iTLA's output power range
                if (value > maxPower_dBm)
                { power_dBm = maxPower_dBm; }
                else if( value < minPower_dBm )
                { power_dBm = minPower_dBm; }
                else 
                {
                    power_dBm = value ;
                }

                iTLA .SetPowerSetpoint_dBm(power_dBm );
                iTLA.GetNOP();
            }
        }
        /// <summary>
        /// Power Unit
        /// </summary>
        public InstType_TunableLaserSource.EnPowerUnits PowerUnits
        {
            get
            {
                return InstType_TunableLaserSource.EnPowerUnits .dBm;
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// set/Get  WaveLen in nM
        /// </summary>
        public double WavelengthINnm
        {
            get
            {
                double freq_GHz= iTLA .GetLaserFrequency_GHz();
                return Math .Round ( Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(freq_GHz),3);
            }
            set
            {
                iTLA.GetNOP();
                double tempFreq_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(value);
                if (tempFreq_GHz > maxFreq_GHz || tempFreq_GHz < minFreq_GHz)
                    throw new InstrumentException(string.Format(
                        " The frequency is out of laser source's range, " +
                        "It should be set to {0} ~ {1} nM", minWaveLen_nM, maxWaveLen_nM));

                double Deltafreq_Ghz = tempFreq_GHz - minFreq_GHz;
                
                int chanNumber =(int ) Math.Round(Deltafreq_Ghz / FreqGrid_GHz) +1;                
                iTLA.SelectChannel(chanNumber);

                iTLA.GetNOP();

                int chan = iTLA.GetChannel();
                if (chan != chanNumber) throw new InstrumentException("Itla fails to set channel to " + chanNumber .ToString ());
            }
        }

        #endregion

        #region IInstrument Members

        /// <summary>
        /// Details of chassis driver for this instance of the instrument
        /// </summary>
        public InstrumentDataRecord ChassisDriverData
        {
            get { return null; }
        }
        /// <summary>
        /// Instruments chassis name
        /// </summary>
        public string ChassisName
        {
            get { return ""; }
        }
        /// <summary>
        /// Instrument driver name
        /// </summary>
        public string DriverName
        {
            get { return "Inst_CoRxITLALaserSource"; }
        }
        
        /// <summary>
        /// Instrument driver version
        /// </summary>
        public string DriverVersion
        {
            get { return System .Reflection .Assembly .GetAssembly ( this .GetType()).GetName().Version.ToString(); }
        }
        /// <summary>
        /// Logging enable flag False by default at startup
        /// </summary>
        public bool EnableLogging
        {
            get
            {
                return  iTLA .EnableLogging;
            }
            set
            {
                iTLA .EnableLogging = value;
            }
        }
        /// <summary>
        /// State caching enable flag.  State caching is optional and functionality is
        //     dependent on driver.  Should not be enabled with shared instruments.  False
        //     by default at startup.
        /// </summary>
        public bool EnableStateCaching
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Hardware firmware version
        /// </summary>
        public string FirmwareVersion
        {
            get { return "Unknown"; }
        }
        /// <summary>
        /// Details of hardware for this instance of the driver
        /// </summary>
        public InstrumentDataRecord HardwareData
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }
        /// <summary>
        /// Unique hardware identification string
        /// </summary>
        public string HardwareIdentity
        {
            get { return "CoRxITLALaserSource"; }
        }
        /// <summary>
        /// Get/Set instrument online status
        /// </summary>
        public bool IsOnline
        {
            get
            {
                return iTLA .IsOnline;
            }
            set
            {
                iTLA .IsOnline = value;
            }
        }
        /// <summary>
        /// Instrument name property
        /// </summary>
        public string Name
        {
            get { return name; }
        }
        /// <summary>
        /// Configures the instrument into a default state.
        /// </summary>
        public void SetDefaultState()
        {
            iTLA.SoftwareEnableOutput(false); ;
        }
        /// <summary>
        /// Slot ID
        /// </summary>
        public string Slot
        {
            get { return ""; }
        }
        /// <summary>
        /// Subslot ID
        /// </summary>
        public string SubSlot
        {
            get { return ""; }
        }
    /// <summary>
    /// Communications timeout in milliseconds
    /// </summary>
        public int Timeout_ms
        {
            get
            {
                return iTLA .Timeout_ms;
            }
            set
            {
                iTLA .Timeout_ms = value;
            }
        }
        /// <summary>
        /// Detail of chassis drivers valid to use with this driver
        /// </summary>
        public InstrumentData ValidChassisDrivers
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }
        /// <summary>
        /// Detail of hardware valid to use with this driver
        /// </summary>
        public InstrumentData ValidHardwareData
        {
            get
            {
                return iTLA .ValidHardwareData;
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        #endregion

        #region IInstType_RxTunableLaserSource Members
        
        /// <summary>
        /// Get/Set iTLA's exist channal num
        /// </summary>
        public int CurrentChan
        {
            set
            {
                iTLA.GetNOP();
                iTLA.SelectChannel(value);
                iTLA.GetNOP();
                int chan = iTLA.GetChannel();
                if (chan != value) throw new InstrumentException("Itla fails to set channel to " + value.ToString());
            }
            get
            {
                return iTLA.GetChannel();
            }
        }
 
        /// <summary>
        /// Set/Get  WaveLen in nM
        /// </summary>
        public double WaveLen_nM
        {
            get
            {
                double freq_GHz = iTLA.GetLaserFrequency_GHz();
                return Math .Round ( Alg_FreqWlConvert.Freq_GHz_TO_Wave_nm(freq_GHz),3);
            }
            set
            {
                iTLA.GetNOP();
                double tempFreq_GHz = Alg_FreqWlConvert.Wave_nm_TO_Freq_GHz(value);
                if (tempFreq_GHz > maxFreq_GHz || tempFreq_GHz < minFreq_GHz)
                    throw new InstrumentException(string.Format(
                        " The Wavelength is out of laser source's range, " +
                        "It should be set to {0} ~ {1} nM", minWaveLen_nM, maxWaveLen_nM));

                double Deltafreq_Ghz = tempFreq_GHz - minFreq_GHz;

                int chanNumber = (int)Math.Round(Deltafreq_Ghz / FreqGrid_GHz) +1;
                iTLA.SelectChannel(chanNumber);
                iTLA.GetNOP();
                
                int chan = iTLA.GetChannel();
                if (chan != chanNumber) throw new InstrumentException("Itla fails to set channel to " + chanNumber.ToString());

            }
        }
        /// <summary>
        /// Set/Get Frequency in GHz
        /// </summary>
        public double Frequency_GHz
        {
            get
            {
                double freq_GHz = iTLA.GetLaserFrequency_GHz();
                return freq_GHz;
            }
            set
            {
                iTLA.GetNOP();
                double tempFreq_GHz = value;
                if (tempFreq_GHz > maxFreq_GHz || tempFreq_GHz < minFreq_GHz)
                    throw new InstrumentException(string.Format(
                        " The frequency is out of laser source's range, " +
                        "It should be set to {0} ~ {1} GHz", minFreq_GHz , maxFreq_GHz));

                double Deltafreq_Ghz = tempFreq_GHz - minFreq_GHz;

                int chanNumber = (int)Math.Round(Deltafreq_Ghz / FreqGrid_GHz) +1;
                iTLA.SelectChannel(chanNumber);
                iTLA.GetNOP();

                int chan = iTLA.GetChannel();
                if (chan != chanNumber) throw new InstrumentException("Itla fails to set channel to " + chanNumber.ToString());

            }
        }
        /// <summary>
        /// Set/Get Power output in dBm
        /// </summary>
        public double Power_dBm
        {
            get
            {
                double power_dBm = iTLA.GetLaserOutputPower_dBm();
                return power_dBm;
            }
            set
            {
                iTLA.GetNOP();
                double power_dBm;
                // Clamp setting value within iTLA's power range
                if (value > maxPower_dBm)
                { power_dBm = maxPower_dBm; }
                else if( value < minPower_dBm )
                { power_dBm = minPower_dBm; }
                else 
                {
                    power_dBm = value ;
                }

                iTLA .SetPowerSetpoint_dBm(power_dBm );
                iTLA.GetNOP();
            }
        }
        /// <summary>
        /// Set/Get OPTICAL  Output status
        /// </summary>
        public bool OutputEnable
        {
            get
            {
                return iTLA .GetSoftwareEnableOutput();
            }
            set
            {
                iTLA.GetNOP();

                iTLA .SoftwareEnableOutput(value );
                iTLA.GetNOP();

                bool state = iTLA.GetSoftwareEnableOutput();

                if (state != value) throw new InstrumentException("Itla fails to set enable status to be " + value.ToString());
            }
        }

        /// <summary>
        /// Get minimun available frequency in Ghz
        /// </summary>
        public double MinFreq_GHz
        {
            get { return minFreq_GHz; }
        }

        /// <summary>
        /// Get maximun available frequency in Ghz
        /// </summary>
        public double MaxFreq_GHz
        {
            get { return maxFreq_GHz; }
        }

        /// <summary>
        /// Get minimun wavelength available in nm
        /// </summary>
        public double MinWaveLen_nM
        {
            get { return minWaveLen_nM; }
        }

        /// <summary>
        /// Get maximun wavelength available in nm
        /// </summary>
        public double MaxWaveLen_nM
        {
            get { return maxWaveLen_nM; }
        }

        /// <summary>
        /// Get Minimun output power in dBm
        /// </summary>
        public double MinPower_dBm
        {
            get { return minPower_dBm; }
        }

        /// <summary>
        /// Get maximun output power in dBm
        /// </summary>
        public double MaxPower_dBm
        {
            get { return maxPower_dBm; }
        }
        #endregion

        #region
        public void SetGainDAC(int value)
        {
            iTLA.UnlockModule();
            System.Threading.Thread.Sleep(100);
            iTLA.SetGainDAC(value);
        }
        public int gainDac
        {
            get { return iTLA.GetGainDAC(); }
        }

        public void unlock()
        {
            iTLA.UnlockModule();
        }

        #endregion
    }
}
