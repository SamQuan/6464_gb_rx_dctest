// [Copyright]
//
// Bookham [Full Project Name]
// Bookham.TestSolution.Instruments
//
// Inst_BkhmBertXfpHost.cs
//
// Author: Paul.Annetts, 2006
// Design: [Reference design documentation]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.ChassisNS;
using Bookham.TestLibrary.Algorithms;

namespace Bookham.TestLibrary.Instruments
{
	/// <summary>
	/// XFP Host IO bits
	/// </summary>
	[Flags]
	public enum XfpHostIoOutBits
	{
		/// <summary>ModDesel bit</summary>
		ModDesel = 0x01,
		/// <summary>PDown bit</summary>
		PDown = 0x02,
		/// <summary>TxDisabled bit</summary>
		TxDisabled = 0x04
	}

	/// <summary>
	/// XFP Host card on Bookham BERT
	/// </summary>
	public class Inst_BkhmMMSXfpHost : Instrument
	{
		#region Constructor
		/// <summary>
		/// Instrument Constructor.
		/// </summary>
		/// <param name="instrumentNameInit">Instrument name</param>
		/// <param name="driverNameInit">Instrument driver name</param>
		/// <param name="slotInit">Slot ID for the instrument</param>
		/// <param name="subSlotInit">Sub Slot ID for the instrument</param>
		/// <param name="chassisInit">Chassis through which the instrument communicates</param>
		public Inst_BkhmMMSXfpHost(string instrumentNameInit, string driverNameInit,
			string slotInit, string subSlotInit, Chassis chassisInit)
			: base(instrumentNameInit, driverNameInit, slotInit, subSlotInit,
			chassisInit)
		{
			// Setup expected valid hardware variants 
			// TODO: Update with useful names, real values, create multiple copies as required.
			InstrumentDataRecord instrVariant1 = new InstrumentDataRecord(
				"Bookham XfpHost",				// hardware name 
				"0.1",  			// minimum valid firmware version 
				"1.0");			// maximum valid firmware version 
			ValidHardwareData.Add("InstrVariant1", instrVariant1);

			// Configure valid chassis driver information
			// TODO: Update with useful key name, real chassis name and versions
			InstrumentDataRecord chassisInfo = new InstrumentDataRecord(
				"Chassis_BkhmMMS",								// chassis driver name  
				"0.0.0.0",									// minimum valid chassis driver version  
				"2.0.0.0");									// maximum valid chassis driver version
			ValidChassisDrivers.Add("Chassis_BkhmMMS", chassisInfo);

			// recall the card ID
			this.cardId = byte.Parse(slotInit);
			// initialise this instrument's chassis
			this.instrumentChassis = (Chassis_BkhmMMS)chassisInit;
		}
		#endregion

		#region Instrument overrides

		/// <summary>
		/// Firmware version of this instrument.
		/// </summary>
		public override string FirmwareVersion
		{
			get
			{
				return instrumentChassis.CardFirmwareVersion(cardId, this);
			}
		}

		/// <summary>
		/// Hardware Identity of this instrument.
		/// </summary>
		public override string HardwareIdentity
		{
			get
			{
				return instrumentChassis.CardHardwareVersion(cardId, this);
			}
		}

		/// <summary>
		/// Set instrument to default state
		/// </summary>
		public override void SetDefaultState()
		{
			throw new InstrumentException("The method or operation is not implemented.");
		}
		#endregion

		#region Private data
		/// <summary>
		/// Instrument's chassis
		/// </summary>
		private Chassis_BkhmMMS instrumentChassis;

		/// <summary>
		/// Card Id
		/// </summary>
		private byte cardId;
		#endregion

		private void SetHostIoBit(XfpHostIoOutBits bitId, bool bitState)
		{
			Int32 bitIdInt = (Int32)bitId;
			// check that this is a power of 2
			if ((bitIdInt & (bitIdInt - 1)) != 0)
			{
				throw new ArgumentException("One bit at a time please! " + bitId.ToString("X"));
			}
			// divide by two to find what bit number this is!
			int val = bitIdInt;
			byte counter = 0;
			while (val > 1)
			{
				val = val >> 1;
				counter++;
			}
			if (val != 1)
			{
				throw new ArgumentException("No bits set! " + bitId.ToString("X"));
			}

			byte[] data = new byte[2];
			data[0] = counter;
			if (bitState) data[1] = 1;
			else data[1] = 0;

			instrumentChassis.DoCommand(cardId, 0x102, data, this);
		}

		private XfpHostIoOutBits GetHostIoBits()
		{
			byte[] resp = instrumentChassis.DoCommand(cardId, 0x100, this);
			byte bits = resp[0];
			return (XfpHostIoOutBits)bits;
		}

		/// <summary>
		/// Get/Set the XFP enabled
		/// </summary>
		public bool XfpEnabled
		{
			get
			{
				XfpHostIoOutBits ioBits = GetHostIoBits();
				bool disabled = ((ioBits & XfpHostIoOutBits.TxDisabled) != 0);
				return (!disabled);
			}
			set
			{
				SetHostIoBit(XfpHostIoOutBits.TxDisabled, !value);
			}
		}

		/// <summary>
		/// Get/Set the Device power supply state
		/// </summary>
		public bool PowerEnabled
		{
			get
			{
				byte[] resp = instrumentChassis.DoCommand(cardId, 0x231, this);

				if (resp[0] == 1)
				{
					return (true);
				}
				else if (resp[0] == 0)
				{
					return (false);
				}
				else
				{
					// throw exception!!
					return (false);
				}
			}
			set
			{
				byte TxBuff = 0;

				if (value) TxBuff = 1;
				instrumentChassis.DoCommand(cardId, 0x230, TxBuff, this);

			}
		}
	}
}
