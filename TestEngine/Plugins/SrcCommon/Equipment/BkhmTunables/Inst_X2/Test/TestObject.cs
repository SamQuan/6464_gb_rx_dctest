using System;
using System.Collections.Generic;
// TODO: You'll probably need these includes once you add your references!
//using Bookham.TestLibrary.ChassisNS;
//using Bookham.TestLibrary.Instruments;
//using Bookham.TestLibrary.InstrTypes;
using Bookham.TestEngine.Framework.Logging;
using System.Text;
using NUnit.Framework;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestSolution.ChassisNS;
using Bookham.TestSolution.Instruments;

namespace Test.X2
{
    /// <exclude />	
    [TestFixture]
    public class Test_Object
    {
        #region Private data (incl equipment references)
        private Chassis_X2 testChassis;

        private Inst_X2 testInstr;        
        #endregion

        /// <exclude />
        [TestFixtureSetUp]
        public void Setup()
        {
            // initialise Test Engine logging domain
            Bookham.TestEngine.Framework.Logging.Initializer.Init();
            Bookham.TestEngine.Framework.Logging.BugOut.SetOutputToConsole(true);
            TestOutput("*** Test Harness Initialising ***");

            // create chassis objects
            TestOutput("Don't forget to create chassis objects");
            testChassis = new Chassis_X2("Chassis_X2", "Chassis_X2", "COM3");
            TestOutput(testChassis, "Created OK");

            // create instrument objects            
            TestOutput("Don't forget to create instrument objects");
            testInstr = new Inst_X2("Inst_X2", "Inst_X2", "1", "2", testChassis);
            TestOutput(testInstr, "Created OK");

            // put them online
            TestOutput("Don't forget to put equipment objects online");
            testChassis.IsOnline = true;
            testChassis.EnableLogging = true;            
            TestOutput(testChassis, "Chassis set online OK");

            testInstr.IsOnline = true;
            testInstr.EnableLogging = true;
            TestOutput(testInstr, "Inst set online OK");            
        }

        /// <exclude />
        [TestFixtureTearDown]
        public void ShutDown()
        {
            // TODO...
            TestOutput("Don't forget to take the chassis offline!");
            //testChassis.IsOnline = false;

            // Test end
            TestOutput("Test Finished!");
        }

        [Test]
        public void T01_FirstTest()
        {
            TestOutput("\n\n*** T01_FirstTest ***");
            // You can use Assert to check return values from tests
            Assert.AreEqual(1, 1); // this will succeed
            // Don't forget to use special functions for Double metavalues...
            Assert.IsNaN(Double.NaN);
            int gain = testInstr.GetGainDAC();
            BugOut.WriteLine(BugOut.WildcardLabel, "Gain is " + gain.ToString());
        }

        [Test]
        public void T02_SecondTest()
        {
            TestOutput("\n\n*** T02_SecondTest***");
            // Give up the test - bad thing happened
            Assert.Fail();
        }


        #region Private helper fns
        private void TestOutput(string output)
        {
            BugOut.WriteLine(BugOut.WildcardLabel, output);
        }

        private void TestOutput(IInstrument i, string output)
        {
            string outputStr = String.Format("{0}: {1}", i.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }

        private void TestOutput(IChassis c, string output)
        {
            string outputStr = String.Format("{0}: {1}", c.Name, output);
            BugOut.WriteLine(BugOut.WildcardLabel, outputStr);
        }
        #endregion


    }

}
