// [Copyright]
//
// Bookham [Coherent RX DC Test]
// Bookham.TestSolution.TestModules
//
// TM_CoRxMultiCoupleEff.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G ]

using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine.PluginInterfaces.Module;
using Bookham.TestEngine.Framework.InternalData;
using Bookham.TestEngine.Equipment;
using Bookham.TestEngine.PluginInterfaces.Instrument;
using Bookham.TestEngine.PluginInterfaces.Chassis;
using Bookham.TestLibrary.Algorithms;
using Bookham.TestSolution.Instruments;
using Bookham.TestLibrary.Utilities;
using Bookham.TestSolution.CoRxDCTestCommonData;
using Bookham.TestLibrary.Instruments;

namespace Bookham.TestSolution.TestModules
{
    /// <summary>
    /// CoRx's multiple frequencies coupling efficiency Test Module.
    /// </summary>
    public class TM_CoRxMultiCoupleEff : ITestModule
    {
        #region ITestModule Members

        /// <summary>
        /// Coupling efficiency test module
        /// </summary>
        /// <param name="engine"></param>
        /// <param name="userType"></param>
        /// <param name="configData"></param>
        /// <param name="instruments"></param>
        /// <param name="chassis"></param>
        /// <param name="calData"></param>
        /// <param name="previousTestData"></param>
        /// <returns></returns>
        public DatumList DoTest(ITestEngine engine, ModulePrivilegeLevel userType, DatumList configData, InstrumentCollection instruments, ChassisCollection chassis, DatumList calData, DatumList previousTestData)
        {
            engine.GuiShow();
            engine.GuiToFront();
            engine .SendToGui(new GuiMsg.CautionOfLaserRqst() );

            double VauxPD_X_V = configData.ReadDouble("AuxPDBias_X_V");
            double IauxPD_Compliance_X_A = configData.ReadDouble("AuxPDCompCurrent_X_A");
            double VauxPD_Y_V = configData.ReadDouble("AuxPDBias_Y_V");
            double IauxPD_Compliance_Y_A = configData.ReadDouble("AuxPDCompCurrent_Y_A");
            double wavelen_ref_nm = configData.ReadDouble("ReferenceWavelen_nM");

            string fileSufix4SigPath="";
            string fileSufix4LocPath="";

            string filePrefix4PdRespSig = "";
            string filePrefix4PdRespLoc = "";
            string filePrefixPdRespStat = "";
            
            #region Get plot file name imformation
            string fileDirectory="";
            bool isSaveDataToFile = configData.ReadBool("SaveDataToFile");
            if (isSaveDataToFile )
            {
                fileDirectory = configData .ReadString ("BlobFileDirectory");
                fileSufix4SigPath = configData.ReadString("FileSufix4SigPath");
                fileSufix4LocPath = configData.ReadString("FileSufix4LocPath");

                if (configData .IsPresent ("FilePrefix4PdRespLocPath"))
                    filePrefix4PdRespLoc = configData.ReadString("FilePrefix4PdRespLocPath");
                   
                if (configData .IsPresent ("FilePrefix4PdRespSigPath"))
                    filePrefix4PdRespSig = configData.ReadString("FilePrefix4PdRespSigPath");
                
                if (configData .IsPresent ("FilePrefix4RespStatatics"))
                    filePrefixPdRespStat = configData.ReadString("FilePrefix4RespStatatics");
                
            }

            #endregion
            #region Get frequency information
            EnumFrequencySetMode freqSetMode = EnumFrequencySetMode.ByChan;
            double freq_Start=0;
            double freq_Stop=0;
            double freq_Step=0;
            if (configData.IsPresent("Freq_Start_GHz") && configData.IsPresent("Freq_Stop_GHz") 
                && configData.IsPresent("Freq_Step_GHz"))
            {
                freq_Start = configData.ReadDouble("Freq_Start_GHz");
                freq_Stop = configData.ReadDouble("Freq_Stop_GHz");
                freq_Step = configData.ReadDouble("Freq_Step_GHz");
                freqSetMode = EnumFrequencySetMode.ByFrequency;
            }
            else if (configData.IsPresent("Wavelen_Start_nm") && configData.IsPresent("Wavelen_Stop_nm") &&
                configData.IsPresent("wavelen_Step_nm"))
            {
                freq_Start = configData.ReadDouble("Wavelen_Start_nm");
                freq_Stop = configData.ReadDouble("Wavelen_Stop_nm");
                freq_Step = configData.ReadDouble("wavelen_Step_nm");
                freqSetMode = EnumFrequencySetMode.ByWavelen;
            }
            else if (configData.IsPresent("Chan_Start") && configData.IsPresent("Chan_Stop") &&
                configData.IsPresent("Chan_Step"))
            {
                freq_Start = configData.ReadDouble("Chan_Start");
                freq_Stop = configData.ReadDouble("Chan_Stop");
                freq_Step = configData.ReadDouble("Chan_Step");
                freqSetMode = EnumFrequencySetMode.ByChan;
            }
            
            #endregion
            
            #region Get opc power information
            bool isAdjustSigOpcPower = configData.ReadBool("IsAdjustSigOpcPower");
            double rqsSigOpcPower_DBm;
            if (isAdjustSigOpcPower)
                rqsSigOpcPower_DBm = configData.ReadDouble("RqsSigOpcPower_DBm");
            else
                rqsSigOpcPower_DBm = double.PositiveInfinity;

            bool isAdjustLocOpcPower = configData.ReadBool("IsAdjustLocOpcPower");
            double rqsLocOpcPower_DBm;
            if (isAdjustLocOpcPower)
                rqsLocOpcPower_DBm = configData.ReadDouble("RqsLocOpcPower_DBm");
            else
                rqsLocOpcPower_DBm = double.PositiveInfinity;

            #endregion

            #region Get referenced wavelen's frequency for each laser source
            double freq_sig_Ref_GHz = -1;
            double freq_loc_Ref_GHz = -1;

            
            CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm = wavelen_ref_nm;
            freq_sig_Ref_GHz = CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz;
        
            CoRxDCTestInstruments.LocalOscInputInstrs.ChainWaveLen_nm = wavelen_ref_nm;
            freq_loc_Ref_GHz = CoRxDCTestInstruments.LocalOscInputInstrs.ChainFrequency_GHz;
        
            #endregion

            #region Test Coupling Efficiency
            
            CoRxDCTestInstruments.SetSource2AuxPD(true);
            CoRxDCTestInstruments.InitAllAuxPDSourcesAsVoltSource(VauxPD_X_V,VauxPD_Y_V , IauxPD_Compliance_X_A,IauxPD_Compliance_Y_A );
            
            Inst_CoRxITLALaserSource itlaSource_Sig = null;
            Inst_CoRxITLALaserSource itlaSource_Loc = null ;

            try
            {
                itlaSource_Sig = (Inst_CoRxITLALaserSource)CoRxDCTestInstruments.SignalInputInstrs.LaserSource;
            }
            catch
            {                
            }
            try
            {
                itlaSource_Loc = (Inst_CoRxITLALaserSource)CoRxDCTestInstruments.LocalOscInputInstrs.LaserSource;
            }
            catch
            { }

            if (itlaSource_Loc != null)
            {
                CoRxDCTestInstruments.SelectItla(itlaSource_Loc);
                itlaSource_Loc.CurrentChan = 1;
            }
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            if (itlaSource_Sig != null)
            {
                CoRxDCTestInstruments.SelectItla(itlaSource_Sig);
                itlaSource_Sig.CurrentChan = 1;
            }
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();    

            CoRxDCTestInstruments.SetAllAuxPdBias(VauxPD_X_V,VauxPD_Y_V , IauxPD_Compliance_X_A,IauxPD_Compliance_Y_A );
            CoRxDCTestInstruments.SetAllAuxPdSourceOutput(true);
            
            double opcPower_mW ;
            double powerReading_dBm;
            double IauxPD_XR_A;
            double IauxPD_XL_A;
            double IauxPd_YR_A;
            double IauxPd_YL_A;
            //double ItapPd_A=0;
            double wavelen_nm;
            List<CoRxAuxPdResponsivityData> sigPathCouplingEff = new List<CoRxAuxPdResponsivityData>();
            List<CoRxAuxPdResponsivityData> locPathCouplingEff = new List<CoRxAuxPdResponsivityData>();
            
            CoRxAuxPdResponsivityData sigAuxRes_RefWavelen= null;
            CoRxAuxPdResponsivityData locAuxRes_RefWavelen= null;
            engine .SendToGui( new GuiMsg .CautionOfLaserRqst() );
            engine .SendToGui(new DatumString("","Measuring Coupling Efficiency on signal path..."));

            
            if (itlaSource_Sig != null ) CoRxDCTestInstruments.SelectItla(itlaSource_Sig);
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = true;

            System.Threading.Thread.Sleep(2000);
            // Test coupling eff by channel or frequency
            for (double freq = freq_Start; freq <= freq_Stop; freq += freq_Step)
            {
                // Signal Path Coupling Eff               
                if (freqSetMode == EnumFrequencySetMode.ByChan)
                    CoRxDCTestInstruments.SignalInputInstrs.ChainChan = (int)freq;
                else if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                    CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz = freq;
                else
                    CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm = freq;                
                
                System.Threading.Thread.Sleep(100);
                if (isAdjustSigOpcPower) 
                    CoRxDCTestInstruments.SignalInputInstrs.SetOpticalPower(rqsSigOpcPower_DBm);
                
                powerReading_dBm = CoRxDCTestInstruments .SignalInputInstrs .GetOpticalPower_dBm();
                opcPower_mW = Alg_PowConvert_dB .Convert_dBmtomW( powerReading_dBm );
                wavelen_nm = CoRxDCTestInstruments.SignalInputInstrs.ChainWaveLen_nm;

                IauxPD_XR_A = Math.Abs( CoRxDCTestInstruments.PdSource_AUX_XR.CurrentActual_amp);
                IauxPD_XL_A = Math.Abs( CoRxDCTestInstruments.PdSource_AUX_XL.CurrentActual_amp);
                
                IauxPd_YR_A = Math.Abs( CoRxDCTestInstruments.PdSource_AUX_YR.CurrentActual_amp);
                IauxPd_YL_A = Math.Abs( CoRxDCTestInstruments.PdSource_AUX_YL.CurrentActual_amp);
                // if signal aux pd reading too low (<0.1mA ), it's abnormal, delay and read the current again
                if (IauxPD_XL_A < 0.0001 || IauxPd_YL_A < 0.0001)  
                {
                    System.Threading.Thread.Sleep(1000);
                    IauxPD_XR_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XR.CurrentActual_amp);
                    IauxPD_XL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XL.CurrentActual_amp);
                    IauxPd_YR_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_YR.CurrentActual_amp);
                    IauxPd_YL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_YL.CurrentActual_amp);
                }
                
                CoRxAuxPdResponsivityData newSigChanCouplingEff =
                    new CoRxAuxPdResponsivityData(powerReading_dBm, wavelen_nm,
                    IauxPD_XR_A, IauxPD_XL_A, IauxPd_YR_A, IauxPd_YL_A /*,ItapPd_A*/);
                sigPathCouplingEff.Add(newSigChanCouplingEff);
                if (freq_sig_Ref_GHz == CoRxDCTestInstruments.SignalInputInstrs.ChainFrequency_GHz)
                    sigAuxRes_RefWavelen = newSigChanCouplingEff;
            }  // end for loop 
            System.Threading.Thread.Sleep(100);
            
            CoRxDCTestInstruments.SignalInputInstrs.OpticalChainOutputEnable = false;
            System.Threading.Thread.Sleep(10);
            CoRxDCTestInstruments.SignalInputInstrs.SafeShutDown();

            // Local Osc path coupling Eff
            engine.SendToGui(new DatumString("","Measuring Coupling Efficiency on Local Osc path..."));

            if (itlaSource_Loc != null)
            {
                CoRxDCTestInstruments.SelectItla(itlaSource_Loc);
                itlaSource_Loc.CurrentChan = 1;
            }
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = true;
            System.Threading.Thread.Sleep(2000);
            // Seperated loc path and sig path to avoid frequetly switching btw 2 iTLAs to reduce error cause by it.
            for (double freq = freq_Start; freq <= freq_Stop; freq += freq_Step)
            {
                if (freqSetMode == EnumFrequencySetMode.ByChan)
                    CoRxDCTestInstruments.LocalOscInputInstrs.ChainChan = (int)freq;
                else if (freqSetMode == EnumFrequencySetMode.ByFrequency)
                    CoRxDCTestInstruments.LocalOscInputInstrs.ChainFrequency_GHz = freq;
                else
                    CoRxDCTestInstruments.LocalOscInputInstrs.ChainWaveLen_nm = freq;

                
                System.Threading.Thread.Sleep(100);
                if (isAdjustLocOpcPower)
                    CoRxDCTestInstruments.LocalOscInputInstrs.SetOpticalPower(rqsLocOpcPower_DBm);
                
                wavelen_nm = CoRxDCTestInstruments.SignalInputInstrs.GetOpticalPower_dBm();

                powerReading_dBm = CoRxDCTestInstruments.LocalOscInputInstrs.GetOpticalPower_dBm();
                opcPower_mW = Alg_PowConvert_dB.Convert_dBmtomW(powerReading_dBm);

                wavelen_nm = Math.Abs( CoRxDCTestInstruments.LocalOscInputInstrs.ChainWaveLen_nm);

                IauxPD_XR_A =Math.Abs( CoRxDCTestInstruments.PdSource_AUX_XR.CurrentActual_amp);
                IauxPD_XL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XL.CurrentActual_amp);
                IauxPd_YR_A = Math .Abs( CoRxDCTestInstruments.PdSource_AUX_YR.CurrentActual_amp);
                IauxPd_YL_A = Math .Abs ( CoRxDCTestInstruments.PdSource_AUX_YL.CurrentActual_amp);
                // if local aux pd reading too low (<0.1mA ), it's abnormal, delay and read the current again
                if (IauxPD_XR_A < 0.0001 || IauxPd_YR_A < 0.0001)
                {
                    System.Threading.Thread.Sleep(1000);
                    IauxPD_XR_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XR.CurrentActual_amp);
                    IauxPD_XL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_XL.CurrentActual_amp);
                    IauxPd_YR_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_YR.CurrentActual_amp);
                    IauxPd_YL_A = Math.Abs(CoRxDCTestInstruments.PdSource_AUX_YL.CurrentActual_amp);
                }
               
                CoRxAuxPdResponsivityData newLocChanCouplingEff =
                    new CoRxAuxPdResponsivityData(powerReading_dBm, wavelen_nm,
                    IauxPD_XR_A, IauxPD_XL_A, IauxPd_YR_A, IauxPd_YL_A /*,ItapPd_A*/);
                locPathCouplingEff.Add(newLocChanCouplingEff);
                if (freq_loc_Ref_GHz == CoRxDCTestInstruments.LocalOscInputInstrs.ChainFrequency_GHz)
                    locAuxRes_RefWavelen = newLocChanCouplingEff;                          
            }
            System.Threading.Thread.Sleep(100);
            //turn off 
            CoRxDCTestInstruments.LocalOscInputInstrs.OpticalChainOutputEnable = false;
            CoRxDCTestInstruments.LocalOscInputInstrs.SafeShutDown();

            CoRxDCTestInstruments.setAllTiaSourceOutput(false);
            CoRxDCTestInstruments.SetAllAuxPdSourceOutput(false);
            if (CoRxDCTestInstruments.IsBiasPDTap) CoRxDCTestInstruments.PdTapSource.OutputEnabled = false;
            engine.SendToGui(new GuiMsg.CancelOflLaserCautionRqst());
            #endregion

            // return data
            DatumList returnData = new DatumList();
                        
            returnData.AddDouble("VauxPD_XR_V", VauxPD_X_V);
            returnData.AddDouble("VauxPD_XL_V", VauxPD_X_V);
            returnData.AddDouble("VauxPD_YR_V", VauxPD_Y_V);
            returnData.AddDouble("VauxPD_YL_V", VauxPD_Y_V);
            #region Save data to plot fie
            
            string fileName = "";
            if (isSaveDataToFile)
            {
                
                List<string> sigPathDataStrs = new List<string>();
                List <string > locPathDataStrs = new List<string>();

                List<string> lstPdResp_Sig = new List<string>();
                List<string> lstPdResp_Loc = new List<string>();
                
                lstPdResp_Sig.Add(CoRxAuxPdResponsivityData.GetPdRespStringHeader());
                lstPdResp_Loc.Add(CoRxAuxPdResponsivityData.GetPdRespStringHeader());
                                
                sigPathDataStrs.Add(CoRxAuxPdResponsivityData.GetAuxPdDataStringHeader());
                foreach (CoRxAuxPdResponsivityData dataTemp in sigPathCouplingEff)
                {
                    sigPathDataStrs.Add(dataTemp.AuxPdDataToString());
                    lstPdResp_Sig.Add(dataTemp.GetPdRespString());
                }              
                
                using (CsvWriter writer = new CsvWriter())
                {
                    fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, fileSufix4SigPath, "", ".csv");
                    try 
                    {
                        writer .WriteFile(fileName, sigPathDataStrs); 
                        returnData .AddFileLink("SigPathDataFile", fileName );
                    }
                    catch (Exception ex)
                    {
                        throw ex ;
                    }
                    if (filePrefix4PdRespSig.Trim() != "")
                    {
                        fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix4PdRespSig, "", ".csv");
                        try
                        {
                            writer.WriteFile(fileName, lstPdResp_Sig);
                            returnData.AddFileLink("SigPathPdRespFile", fileName);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }

                locPathDataStrs.Add(CoRxAuxPdResponsivityData.GetAuxPdDataStringHeader());
                foreach (CoRxAuxPdResponsivityData datatemp in locPathCouplingEff)
                {
                    locPathDataStrs.Add(datatemp.AuxPdDataToString());
                    lstPdResp_Loc.Add(datatemp.GetPdRespString());
                }
               
                
                using (CsvWriter writer = new CsvWriter())
                {
                    fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, fileSufix4LocPath, "", ".csv");
                    try
                    {
                        writer.WriteFile(fileName, locPathDataStrs);
                        returnData .AddFileLink("LocPathDataFile", fileName );
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    if (filePrefix4PdRespLoc.Trim() != "")
                    {
                        fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefix4PdRespLoc, "", ".csv");
                        try
                        {
                            writer.WriteFile(fileName, lstPdResp_Loc);
                            returnData.AddFileLink("LocPathPdRespFile", fileName);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }
            #endregion

            #region Statistical Min, Max and Avg
            double sigPathRes_Aux_XR_Min = double .PositiveInfinity;
            double sigPathRes_Aux_XR_Max = double.NegativeInfinity;
            double sigPathRes_Aux_XR_Sum = 0;
            double sigPathRes_Aux_XL_Min = double.PositiveInfinity;
            double sigPathRes_Aux_XL_Max = double.NegativeInfinity;
            double sigPathRes_Aux_XL_Sum = 0;
            double sigPathRes_Aux_YR_Min = double.PositiveInfinity;
            double sigPathRes_Aux_YR_Max = double.NegativeInfinity;
            double sigPathRes_Aux_YR_Sum = 0;
            double sigPathRes_Aux_YL_Min = double.PositiveInfinity;
            double sigPathRes_Aux_YL_Max = double.NegativeInfinity;
            double sigPathRes_Aux_YL_Sum = 0;

            foreach (CoRxAuxPdResponsivityData datatemp in sigPathCouplingEff)
            {
                sigPathRes_Aux_XR_Max = Math.Max(sigPathRes_Aux_XR_Max, datatemp.Res_Aux_XR);
                sigPathRes_Aux_XR_Min = Math.Min(sigPathRes_Aux_XR_Min, datatemp.Res_Aux_XR);
                sigPathRes_Aux_XR_Sum += datatemp.Res_Aux_XR;

                sigPathRes_Aux_XL_Max = Math.Max(sigPathRes_Aux_XL_Max, datatemp.Res_Aux_XL);
                sigPathRes_Aux_XL_Min = Math.Min(sigPathRes_Aux_XL_Min, datatemp.Res_Aux_XL);
                sigPathRes_Aux_XL_Sum += datatemp.Res_Aux_XL;

                sigPathRes_Aux_YR_Max = Math.Max(sigPathRes_Aux_YR_Max, datatemp.Res_Aux_YR);
                sigPathRes_Aux_YR_Min = Math.Min(sigPathRes_Aux_YR_Min, datatemp.Res_Aux_YR);
                sigPathRes_Aux_YR_Sum += datatemp.Res_Aux_YR;

                sigPathRes_Aux_YL_Max = Math.Max(sigPathRes_Aux_YL_Max, datatemp.Res_Aux_YL);
                sigPathRes_Aux_YL_Min = Math.Min(sigPathRes_Aux_YL_Min, datatemp.Res_Aux_YL);
                sigPathRes_Aux_YL_Sum += datatemp.Res_Aux_YL;
                
            }

            List<string> lstRespStat = new List<string>();

            lstRespStat .Add ("Parameter,Average,Min,Max,");

            returnData.AddDouble("SigPathRes_Aux_XR_Min", sigPathRes_Aux_XR_Min);
            returnData.AddDouble("SigPathRes_Aux_XR_Max", sigPathRes_Aux_XR_Max);
            double avg = sigPathRes_Aux_XR_Sum/ sigPathCouplingEff.Count;
            returnData.AddDouble("SigPathRes_Aux_XR_Avg", avg);
            lstRespStat .Add ( string .Format ("Sig Aux XR, {0},{1},{2},", avg , sigPathRes_Aux_XR_Min,sigPathRes_Aux_XR_Max));

            returnData.AddDouble("SigPathRes_Aux_XL_Min", sigPathRes_Aux_XL_Min);
            returnData.AddDouble("SigPathRes_Aux_XL_Max", sigPathRes_Aux_XL_Max);
            avg = sigPathRes_Aux_XL_Sum/ sigPathCouplingEff.Count;
            returnData.AddDouble("SigPathRes_Aux_XL_Avg",avg );
            lstRespStat.Add(string.Format("Sig Aux XL, {0},{1},{2},", avg, sigPathRes_Aux_XL_Min, sigPathRes_Aux_XL_Max));

            returnData.AddDouble("SigPathRes_Aux_YR_Min", sigPathRes_Aux_YR_Min);
            returnData.AddDouble("SigPathRes_Aux_YR_Max", sigPathRes_Aux_YR_Max);
            avg = sigPathRes_Aux_YR_Sum/sigPathCouplingEff .Count;
            returnData.AddDouble("SigPathRes_Aux_YR_Avg",avg );
            lstRespStat.Add(string.Format("Sig Aux YR, {0},{1},{2},", avg, sigPathRes_Aux_YR_Min, sigPathRes_Aux_YR_Max));

            returnData.AddDouble("SigPathRes_Aux_YL_Min", sigPathRes_Aux_YL_Min);
            returnData.AddDouble("SigPathRes_Aux_YL_Max", sigPathRes_Aux_YL_Max);
            avg = sigPathRes_Aux_YL_Sum / sigPathCouplingEff.Count;
            returnData.AddDouble("SigPathRes_Aux_YL_Avg",avg );
            lstRespStat.Add(string.Format("Sig Aux YL, {0},{1},{2},", avg, sigPathRes_Aux_YL_Min, sigPathRes_Aux_YL_Max));

            double locPathRes_Aux_XR_Min = double.PositiveInfinity;
            double locPathRes_Aux_XR_Max = double.NegativeInfinity;
            double locPathRes_Aux_XR_Sum = 0;
            double locPathRes_Aux_XL_Min = double.PositiveInfinity;
            double locPathRes_Aux_XL_Max = double.NegativeInfinity;
            double locPathRes_Aux_XL_Sum = 0;
            double locPathRes_Aux_YR_Min = double.PositiveInfinity;
            double locPathRes_Aux_YR_Max = double.NegativeInfinity;
            double locPathRes_Aux_YR_Sum = 0;
            double locPathRes_Aux_YL_Min = double.PositiveInfinity;
            double locPathRes_Aux_YL_Max = double.NegativeInfinity;
            double locPathRes_Aux_YL_Sum = 0;

            foreach (CoRxAuxPdResponsivityData datatemp in locPathCouplingEff)
            {
                locPathRes_Aux_XR_Max = Math.Max(locPathRes_Aux_XR_Max, datatemp.Res_Aux_XR);
                locPathRes_Aux_XR_Min = Math.Min(locPathRes_Aux_XR_Min, datatemp.Res_Aux_XR);
                locPathRes_Aux_XR_Sum += datatemp.Res_Aux_XR;

                locPathRes_Aux_XL_Max = Math.Max(locPathRes_Aux_XL_Max, datatemp.Res_Aux_XL);
                locPathRes_Aux_XL_Min = Math.Min(locPathRes_Aux_XL_Min, datatemp.Res_Aux_XL);
                locPathRes_Aux_XL_Sum += datatemp.Res_Aux_XL;

                locPathRes_Aux_YR_Max = Math.Max(locPathRes_Aux_YR_Max, datatemp.Res_Aux_YR);
                locPathRes_Aux_YR_Min = Math.Min(locPathRes_Aux_YR_Min, datatemp.Res_Aux_YR);
                locPathRes_Aux_YR_Sum += datatemp.Res_Aux_YR;

                locPathRes_Aux_YL_Max = Math.Max(locPathRes_Aux_YL_Max, datatemp.Res_Aux_YL);
                locPathRes_Aux_YL_Min = Math.Min(locPathRes_Aux_YL_Min, datatemp.Res_Aux_YL);
                locPathRes_Aux_YL_Sum += datatemp.Res_Aux_YL;

            }

            returnData.AddDouble("LocPathRes_Aux_XR_Min", locPathRes_Aux_XR_Min);
            returnData.AddDouble("LocPathRes_Aux_XR_Max", locPathRes_Aux_XR_Max);
            avg = locPathRes_Aux_XR_Sum / locPathCouplingEff.Count;
            returnData.AddDouble("LocPathRes_Aux_XR_Avg", avg );
            lstRespStat.Add(string.Format("Loc Aux XR, {0},{1},{2},", avg, locPathRes_Aux_XR_Min, locPathRes_Aux_XR_Max));

            returnData.AddDouble("LocPathRes_Aux_XL_Min", locPathRes_Aux_XL_Min);
            returnData.AddDouble("LocPathRes_Aux_XL_Max", locPathRes_Aux_XL_Max);
            avg = locPathRes_Aux_XL_Sum / locPathCouplingEff.Count;
            returnData.AddDouble("LocPathRes_Aux_XL_Avg", avg );
            lstRespStat.Add( string .Format ("Loc Aux XL, {0},{1},{2},", avg, locPathRes_Aux_XL_Min, locPathRes_Aux_XL_Max));

            returnData.AddDouble("LocPathRes_Aux_YR_Min", locPathRes_Aux_YR_Min);
            returnData.AddDouble("LocPathRes_Aux_YR_Max", locPathRes_Aux_YR_Max);
            avg = locPathRes_Aux_YR_Sum / locPathCouplingEff.Count;
            returnData.AddDouble("LocPathRes_Aux_YR_Avg", avg );
            lstRespStat.Add(string .Format ( "Loc Aux YR, {0},{1},{2},", avg, locPathRes_Aux_YR_Min, locPathRes_Aux_YR_Max));

            returnData.AddDouble("LocPathRes_Aux_YL_Min", locPathRes_Aux_YL_Min);
            returnData.AddDouble("LocPathRes_Aux_YL_Max", locPathRes_Aux_YL_Max);
            avg = locPathRes_Aux_YL_Sum / locPathCouplingEff.Count;
            returnData.AddDouble("LocPathRes_Aux_YL_Avg", avg );
            lstRespStat.Add(string .Format ( "Loc Aux YL, {0},{1},{2},", avg, locPathRes_Aux_YL_Min, locPathRes_Aux_YL_Max));

            if (isSaveDataToFile && (filePrefixPdRespStat .Trim() != ""))
            {
                using (CsvWriter writer = new CsvWriter())
                {
                    fileName = Util_GenerateFileName.GenWithTimestamp(fileDirectory, filePrefixPdRespStat, "", ".csv");
                    try
                    {
                        writer.WriteFile(fileName, lstRespStat);
                        returnData.AddFileLink("PdRespStatiscsFile", fileName);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    
                }
            }
            #endregion

            // Save coupling Eff for reference wavelen
            if (freq_loc_Ref_GHz != -1 && locAuxRes_RefWavelen != null)
            {
                returnData.AddDouble("LocPathRes_Aux_XR", locAuxRes_RefWavelen.Res_Aux_XR);
                returnData.AddDouble("LocPathRes_Aux_XL", locAuxRes_RefWavelen.Res_Aux_XL);
                returnData.AddDouble("LocPathRes_Aux_YR", locAuxRes_RefWavelen.Res_Aux_YR);
                returnData.AddDouble("LocPathRes_Aux_YL", locAuxRes_RefWavelen.Res_Aux_YL);
                returnData.AddDouble("LocPathCouplingEff", locAuxRes_RefWavelen.CouplingEff);
            }

            if (freq_sig_Ref_GHz != -1 && sigAuxRes_RefWavelen != null)
            {
                returnData.AddDouble("SigPathRes_Aux_XR", sigAuxRes_RefWavelen.Res_Aux_XR);
                returnData.AddDouble("SigPathRes_Aux_XL", sigAuxRes_RefWavelen.Res_Aux_XL);
                returnData.AddDouble("SigPathRes_Aux_YR", sigAuxRes_RefWavelen.Res_Aux_YR);
                returnData.AddDouble("SigPathRes_Aux_YL", sigAuxRes_RefWavelen.Res_Aux_YL);
                returnData.AddDouble("SigPathCouplingEff", sigAuxRes_RefWavelen.CouplingEff);
            }
            return returnData;
        }
        
        /// <summary>
        /// User GUI
        /// </summary>
        public Type UserControl
        {
            get { return (typeof(TM_CoRxMultiCoupleEffGui)); }
        }

        #endregion


    }
}
