// [Copyright]
//
// Bookham [Coherence RX DC Test]
// Bookham.TestSolution.TestControl
//
// BatchViewCtl.cs
//
// Author: alice.huang, 2011
// Design: [Refence Doc.: Xs Test specification for coherent receiver 40G/100G 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Bookham.TestEngine.PluginInterfaces.Program;

namespace Bookham.TestSolution.TestControl
{
    /// <summary>
    /// Batch view GUI
    /// </summary>
    internal partial class BatchViewCtl : Bookham.TestEngine.Framework.Messages.ManagedCtlBase
    {

        /// <summary>
        /// Constructor
        /// </summary>
        public BatchViewCtl()
        {
            /* Call designer generated code. */
            InitializeComponent();
        }

        /// <summary>
        /// Handle incoming messages.
        /// </summary>
        /// <param name="payload">Message payload</param>
        /// <param name="inMsgSeq">Input message sequence number.</param>
        /// <param name="respSeq">Outgoing message sequence number.</param>
        private void BatchViewCtl_MsgReceived(object payload, long inMsgSeq, long respSeq)
        {
            //TODO: Process messages from the Test Control Plug-in worker thread.
            if (payload.GetType() == typeof(DUTObject))
            {
                DUTObject dutInfo = (DUTObject)payload;
                if (dutInfo.SerialNumber.Length > 1)
                {                    
                    dgDutInfo.Rows.Add("Serial Number",dutInfo .SerialNumber );
                }
                if (dutInfo.PartCode.Length >1)
                {
                    dgDutInfo.Rows.Add("Generic code", dutInfo.PartCode);
                }
                if (dutInfo.Attributes.ReadString("DeviceType").Length > 1)
                    dgDutInfo.Rows.Add("DeviceType", dutInfo.Attributes.ReadString("DeviceType"));

                if (dutInfo.TestStage.Length > 0) dgDutInfo.Rows.Add("Stage", dutInfo.TestStage);
                
                if (dutInfo.ProgramPluginName.Length >0 )
                {
                    dgDutInfo.Rows.Add("Program runing", dutInfo.ProgramPluginName);
                }
                
                lblNodeID.Text = dutInfo.NodeID.ToString();
                lblTestSet.Text = dutInfo.EquipmentID;
            }
        }

        /// <summary>
        /// Initialise controls on Gui when loading
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            dgDutInfo.Rows.Clear();
            btnTest.Focus();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTest_Click(object sender, EventArgs e)
        {
            sendToWorker(new DutTestOption(enmTestOption.GoOnTest));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReloadDut_Click(object sender, EventArgs e)
        {
            sendToWorker(new DutTestOption(enmTestOption.CancelTest));
        }
    }
}
