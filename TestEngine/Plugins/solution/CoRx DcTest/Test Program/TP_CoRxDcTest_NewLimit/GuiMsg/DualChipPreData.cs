using System;
using System.Collections.Generic;
using System.Text;
using Bookham.TestEngine .Framework .InternalData ;

namespace Bookham.TestSolution.TestPrograms.GuiMsg
{
    internal class DualChipPreData
    {
        public DualChipPreData(DatumList preData)
        { PreTestData = preData; }
        public readonly DatumList PreTestData;
    }
}
